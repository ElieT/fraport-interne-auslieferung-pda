﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;
using oxmc.IA_ce.ucFraport;
using oxmc.BusinessLogic.com.ox.helper;


namespace oxmc.IA_ce
{
    public partial class OxUserControlCe : UserControl
    {
        private readonly SyncManager _syncManager;
        private readonly SyncLogManager _syncLogManager;

        private OxUserControlCe _activeControl;
        private List<OxUserControlCe> _oxUserControls = new List<OxUserControlCe>();
        private FormMain FM;

        public OxUserControlCe()
        {
            InitializeComponent();

            // *** BEGIN CHANGE 2014-03-12 ***
            if (Screen.PrimaryScreen.Bounds.Height == 320)
            {
                AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                Size = new System.Drawing.Size(240, 268);
            }
            else
            {
                AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
                AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                Size = new System.Drawing.Size(480, 536);
            }
            // *** END CHANGE 2014-03-12 ***

            _syncManager = SyncManager.GetInstance();
            _syncLogManager = SyncLogManager.GetInstance();
        }
        public void ShowThisControl()
        {
            try
            {
                Visible = true;
                OnChangeUserControl(this, new EventArgs());
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void ShowThisControl(Control parent)
        {
            ShowThisControl(0, 0, parent);
        }

        public void ShowThisControl(int left, Control parent)
        {

            // BackColor = Color.FromArgb(AppConfig.Color2[0], AppConfig.Color2[1], AppConfig.Color2[2]);
            ShowThisControl(0, left, parent);
            // Height = Height 8; //???
        }
        public void ShowThisControl(int top, int left, Control parent)
        {

            // BackColor = Color.FromArgb(AppConfig.Color2[0], AppConfig.Color2[1], AppConfig.Color2[2]);
            Visible = true;
            Parent = parent;
            Top = top;
            Left = left;
            //Width = 240; 
            //Height = 267;

            // *** BEGIN CHANGE 2013-07-03 ***
            //if (AppConfig.DeviceId.Equals("MC9090")){
            //if (AppConfig.SVMClientOs.ToUpper().Equals("MC9090"))
            //{
            //    Width = 240; 
            //    Height = 267;
            //} else {
            //    Width = 480;
            //    Height = 534;
            //}
            // *** END CHANGE 2013-07-03 ***

            // *** BEGIN CHANGE 2014-03-12 ***
            if (Screen.PrimaryScreen.Bounds.Height == 320)
            {
                AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                Size = new System.Drawing.Size(240, 268);
            }
            else
            {
                AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
                AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                Size = new System.Drawing.Size(480, 536);
            }
            // *** END CHANGE 2014-03-12 ***

            BringToFront();
            // Height = Height 8; //???
        }

        public EventHandler ChangeUserControl;
        public virtual void OnChangeUserControl(object sender, EventArgs e)
        {
            try
            {
                if (ChangeUserControl != null)
                    ChangeUserControl(sender, e);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public virtual void btSave_Click(object sender, EventArgs e) { }

        public void DeleteThisControl()
        {
            Visible = false;
            Dispose();
        }

        public virtual void PageNavigate(Boolean forward) { }



        public EventHandler RfidObjectReadFinished;
        public EventHandler RfidObjectMessage;
        public EventHandler RfidObjectColor;
        public EventHandler RfidReaderTimeout;


        protected virtual void OnRfidObjectReadFinished(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("oxuc RFID ReadFinished: " + sender.ToString());
                if (RfidObjectReadFinished != null)
                    RfidObjectReadFinished(sender, e);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        protected virtual void OnRfidObjectMessage(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("oxuc RFID ObjectMessage: " + sender.ToString());
                if (RfidObjectMessage != null)
                    RfidObjectMessage(sender, e);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        protected virtual void OnRfidObjectColor(object sender, EventArgs e)
        {
            try
            {
                if (RfidObjectColor != null)
                    RfidObjectColor(sender, e);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        protected virtual void OnRfidReaderTimeout(object sender, EventArgs e)
        {
            try
            {
                System.Console.WriteLine("ReadFinished: " + sender.ToString());
                if (RfidReaderTimeout != null)
                    RfidReaderTimeout(sender, e);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        protected void ObjectReadFinished(object sender, EventArgs e)
        {
            OnRfidObjectReadFinished(sender, e);
        }

        protected void ObjectMessage(object sender, EventArgs e)
        {
            System.Console.WriteLine("ObjectMessage: " + sender.ToString());
            OnRfidObjectMessage(sender, e);
        }

        protected void ObjectColor(object sender, EventArgs e)
        {
            System.Console.WriteLine("ObjectColor: " + sender.ToString());
            OnRfidObjectColor(sender, e);
        }
        protected void ReaderTimeout(object sender, EventArgs e)
        {
            System.Console.WriteLine("RfidReaderTimeout: " + sender.ToString());
            OnRfidReaderTimeout(sender, e);
        }

        public virtual void HideThisControl()
        {
            try
            {
                if (this.Parent.GetType().Name.Equals("Panel"))
                {
                    ((OxUserControlCe)this).ShowThisControl();
                    return;
                }
                //((OxUserControlCe)this.Parent).ShowThisControl();
                ((FormMain)this.Parent).
                Visible = false;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        public void StartApplication()
        {
            OxStatus.GetInstance().SAPClientValue = AppConfig.Mandt;
            OxStatus.GetInstance().SAPUserValue = AppConfig.UserId;

            // Generate Device
            OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
            // Collect all db table names relevant for delta changes
            OxDbManager.GetInstance().CreateDeltaTableContent();

            //RecheckNetworkConnection(this, new EventArgs());

            //initialise caching objects
            CachingManager.UpdateNotifList();
            CachingManager.UpdateOrderList();
            CachingManager.UpdateTimeConfList();

            if (AppConfig.FlagSyncAtStartup.Equals("X"))
            {
                //OxStatus.GetInstance().ProgressBar = 0;
                /* var myThread = new Thread(ProcessSyncRoutine);
                myThread.Start(); */
                ProcessSyncRoutine();
            }
        }

        public void ProcessSyncRoutine()
        {
            try
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now, "Client-Reset nach Login wurde gestartet...", null, true, AppConfig.MessageTimeout);
                OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now, "Letzte Synchronisation: ......... läuft .........", null, true, AppConfig.MessageTimeout);
                OxStatus.GetInstance().SetStatusMessage("Letzte Synchronisation: ......... läuft .........", -1);

                if (AppConfig.FlagDocuments.Equals("X"))
                    _syncManager._reqDoc = true;
                else
                    _syncManager._reqDoc = false;
                if (AppConfig.FlagCustomizing.Equals("X"))
                    _syncManager._reqCust = true;
                else
                    _syncManager._reqCust = false;
                if (AppConfig.FlagMasterdata.Equals("X"))
                    _syncManager._reqMD = true;
                else
                    _syncManager._reqMD = false;
                if (AppConfig.FlagConfirmation.Equals("X"))
                    _syncManager._confRQ = true;
                else
                    _syncManager._confRQ = false;
                if (AppConfig.FlagMassdata.Equals("X"))
                    _syncManager._massData = true;
                else
                    _syncManager._massData = false;

                _syncManager.SynchronizeWithBackend();
                SyncLogWriteNewSyncData();
                _syncLogManager.SetLastSync("", DateTime.Now.ToString("yyyyMMddHHmmsss"));
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void SyncLogWriteNewSyncData()
        {
            try
            {
                //_syncEnd = DateTime.Now;
                TimeSpan ts = _syncManager.SyncEndTimeStamp - _syncManager.SyncStartTimeStamp;
                double millis = ts.TotalMilliseconds;
                int ss = Convert.ToInt32(Math.Round(millis / 1000));
                //int ss = System.Convert.ToInt32(millis);
                //int.Parse(ts.TotalMilliseconds.ToString());
                SyncLogManager.CreateSyncLog(new SyncLogData(_syncManager.SyncStartTimeStamp.ToString("yyyyMMdd"),
                                                             _syncManager.SyncStartTimeStamp.ToString("HHmmsss"),
                                                             _syncManager.SyncEndTimeStamp.ToString("yyyyMMdd"),
                                                             _syncManager.SyncEndTimeStamp.ToString("HHmmsss"),
                                                             _syncManager.StreamlengthOut.ToString(),
                                                             _syncManager.StreamlengthIn.ToString(),
                                                             _syncManager.RecordsUploadCount.ToString(),
                                                             _syncManager.RecordsDownloadCount.ToString(), "", "", "",
                                                             "", _syncManager.getTypeSummary()));

                // Order reset (nicht benötigt) aber Count New Order aus Schwarz setzen
                //formMain.RefreshSyncLogAfterOrders(syncLogData);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

    }
}
