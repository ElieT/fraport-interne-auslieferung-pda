﻿using System;
using System.Drawing.Imaging;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace oxmc.IA_ce.ucCommon
{
    public partial class TransparentPicturesBox : PictureBox
    {
        private Color _transparentColor = Color.White;
        public TransparentPicturesBox()
        {
            InitializeComponent();
        }

        public Color TransparentColor
        {
            get { return _transparentColor; }
            set { _transparentColor = value; }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if(Image != null)
            {
                var pictureBounds = new Rectangle(0, 0, Width, Height);
                var imageBounds = new Rectangle(0, 0, Image.Width, Image.Height);
                var attributes = new ImageAttributes();
                attributes.SetColorKey(TransparentColor, TransparentColor);
                e.Graphics.DrawImage(Image, pictureBounds, imageBounds.X, imageBounds.Y, imageBounds.Width, imageBounds.Height, GraphicsUnit.Pixel, attributes);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            var brush = new SolidBrush(Parent.BackColor);
            e.Graphics.FillRectangle(brush, ClientRectangle);
        }
    }
}
