﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.IA_ce;

namespace IA_ce.ucFraport 

{
    public partial class ucPickUpListItem : OxUserControlCe
    {
        private List<DeliveryCause> delivCauses = new List<DeliveryCause>();
        private string selectedText = "";
        private Delivery _deliv;

        public ucPickUpListItem(Delivery deliv)
        {
            _deliv = deliv;

            InitializeComponent();

            //lblAbt.Text = Deliv.Raum;
            lblAbt.Text = _deliv.Shortobj;
            lblAddressee.Text = _deliv.Empf;
            lblQuantity.Text = _deliv.Menge.TrimStart(' ');
            lblUnit.Text = _deliv.Meins;

            rbDelivered.Checked = false;
            rbFailed.Checked = false;

            cbCause.Enabled = false;
            delivCauses = DeliveryManager.GetDeliveryCauses();
            foreach (DeliveryCause delivCause in delivCauses)
            {
                cbCause.Items.Add(delivCause.Text);
            }

            try
            {
                if ((!_deliv.Status.Equals("A")) && (String.IsNullOrEmpty(_deliv.Status)))
                {
                    int selectedItem = 0;
                    var stop = false;
                    foreach (var cause in delivCauses)
                    {
                        if (cause.Grund.Equals(_deliv.Grund))
                        {
                            stop = true;
                            break;
                        }
                        selectedItem++;
                    }
                    if (stop)
                    {
                        cbCause.Select(selectedItem, 1);
                        cbCause.SelectedIndex = selectedItem;
                    }
                }
            }
            catch (Exception ex){
                FileManager.LogException(ex);       
            }

            if (("A").Equals(deliv.Status))
            {
                rbDelivered.Checked = true;
                rbFailed.Checked = false;
                cbCause.Enabled = false;
                // Selectbox
            }
            else if (String.IsNullOrEmpty(deliv.Status) || deliv.Status.Equals("L") || deliv.Status.Equals("O"))
            {
                rbDelivered.Checked = false;
                rbFailed.Checked = false;
                cbCause.Enabled = false;
            }
            else
            {
                rbDelivered.Checked = false;
                rbFailed.Checked = true;
                cbCause.Enabled = true;
            }
        }

        public Delivery Deliv
        {
            get { return _deliv; }
            set { _deliv = value; }
        }

        public String GetChangedText()
        {
            return selectedText;
        }

        private void cbCause_selectedValueChanged(object sender, EventArgs e)
        {
            if(sender.GetType().Name.Equals("ComboBox"))
            {
                if (cbCause.SelectedItem != null)
                {
                    selectedText = (string) cbCause.SelectedItem;
                }
            }
        }

        public EventHandler ClickEvent;
        /* private void pbIcon_Click(object sender, EventArgs e)
        {

        } */

        private void cbBoxes_clicked(object sender, EventArgs e)
        {
            if (this.GetValueRbDelivered())
            {
                cbCause.Enabled = false;
                cbCause.SelectedIndex = -1;
                //_deliv.Status = "J";
            } else if (this.GetValueRbFailed())
            {
                cbCause.Enabled = true;
                //_deliv.Status = "N";
            }
        }

        public bool GetValueRbDelivered()
        {
            return rbDelivered.Checked;
        }

        public bool GetValueRbFailed()
        {
            return rbFailed.Checked;
        }

        public void setStatusFetched()
        {
            try
            {
                if (_deliv.Status.Equals("O") || _deliv.Status.Equals("L"))
                {
                    //_deliv.Status = "A";
                    if (!rbFailed.Checked)
                    {
                        rbDelivered.Checked = true;

                        FileManager.LogMessage("Abholung " + _deliv.Belnr.Trim() + " abgeholt.");
                    }
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}
