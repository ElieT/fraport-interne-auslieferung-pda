﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using IA_ce;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucDispo : OxUserControlCe
    {
        private static ucDispo _instance;
        private List<String> selectedItems = new List<String>();
        private List<string> errorItems = new List<string>();
        private List<string> successItems = new List<string>();
        private List<string> checkedItems = new List<string>();

        private List<string[]> _statusOfItem = new List<string[]>();

        //private List<string[]> errorArrayItems = new List<string[]>();
        //private List<string[]> successArrayItems = new List<string[]>();

        private ucDispo()
        {
            InitializeComponent();
        }

        public static ucDispo GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ucDispo();
            }
            return _instance;
        }

        public void RefreshData()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                // *** BEGIN CHANGE 2013-07-08 ***
                tbItemNr.Focus();
                tbItemNr.Text = "";
                // *** END CHANGE 2013-07-08 ***

                pnItems.Controls.Clear();
                if (selectedItems != null && selectedItems.Count > 0 )
                {
                    var top = 0;
                    var index = 1;
                    foreach (var number in selectedItems)
                    {
                        // *** BEGIN CHANGE 2013-08-30 ***
                        var status = GetStatusFromList(number);
                        // *** END CHANGE 2013-08-30 ***

                        var isError = false;
                        if (errorItems != null && errorItems.Count > 0)
                        {
                            foreach (var e in errorItems)
                            {
                                if (e.TrimStart('0').Equals(number.TrimStart('0')))
                                    isError = true;
                            }
                        }

                        var isSuccess = false;
                        if (successItems != null && successItems.Count > 0)
                        {
                            foreach (var e in successItems)
                            {
                                if (e.TrimStart('0').Equals(number.TrimStart('0')))
                                    isSuccess = true;
                            }
                        }

                        //var isError = false;
                        //if (errorArrayItems != null && errorArrayItems.Count > 0)
                        //{
                        //    foreach (var e in errorArrayItems)
                        //    {
                        //        if (e[1].TrimStart('0').Equals(number.TrimStart('0')) && e[0].Equals(index+""))
                        //            isError = true;
                        //    }
                        //}

                        var isChecked = false;
                        if (checkedItems != null && checkedItems.Count > 0)
                        {
                            foreach (var c in checkedItems)
                            {
                                if (c.TrimStart('0').Equals(number.TrimStart('0')))
                                    isChecked = true;
                            }
                        }

                        //var ucDispoItem = new ucDispoItem(number);
                        var ucDispoItem = new ucDispoItem(number, status);
                        ucDispoItem.Top = top*ucDispoItem.Height + 2;
                        ucDispoItem.ChangeEvent += CbChangeEvent;
                        if (top%2 == 0)
                            ucDispoItem.BackColor = Color.LightGray;

                        // *** BEGIN CHANGE 2013-07-05 ***
                        if (isError)
                            ucDispoItem.SetVisible("error");

                        if (isSuccess)
                        {
                            ucDispoItem.SetVisible("success");
                            ucDispoItem.SetCheckBox(false);
                        }
                        else
                        {
                            ucDispoItem.SetCheckBox(isChecked);
                        }
                        pnItems.Controls.Add(ucDispoItem);
                        
                        top++;
                        index++;
                    }

                    
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                FileManager.LogException(ex);
            }
            Cursor.Current = Cursors.Default;
        }

        private void pbHome_Clicked(Object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (selectedItems != null && selectedItems.Count > 0)
                {
                    //var res = MessageBox.Show("Möchten Sie die Zuweisungen speichern?", "Vorgang speichern?",
                    //                          MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                    //                          MessageBoxDefaultButton.Button1);
                    var res = MessageBox.Show("Durch das Verlassen der Maske gehen Ihre abgegebenen Daten verloren. Wollen Sie die Maske wirklich verlassen?", "Vorgang speichern?",
                          MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                          MessageBoxDefaultButton.Button1);
                    if (res == DialogResult.No)
                    {

                        ////CallService(sender, e);
                        //errorItems = new List<string>();

                        //var keys = "&DATATYPE=IH_SELF_DISPO&UPSTREAM=SELFDISPO;I;";

                        //FileManager.LogMessage("Selbstdisposition für : " + keys);

                        //keys += GetSelectedKeysAsString();

                        //var retval = SyncManager.GetInstance().ProcessOnlineRequest(keys);

                        //FileManager.LogMessage("Returnmessage: " + retval);

                        //SplitReturnMessage(retval);

                        //DisplayReturnMessage();

                        ////if (!string.IsNullOrEmpty(retval))
                        ////    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, retval, null, true, 5000);

                        //FileManager.LogMessage("Server response: " + retval);

                        //if (!string.IsNullOrEmpty(retval) && retval.IndexOf("RETURNMESSAGE;E") == -1 &&
                        //    retval.IndexOf("Exception") == -1)
                        //{
                        //    Visible = false;
                        //    selectedItems = null;
                        //    errorItems = null;
                        //    successItems = null;
                        //    checkedItems = null;
                        //    NavigationManager.NavigateHome();
                        //}
                        //else
                        //{
                        //    RefreshData();
                        //    Cursor.Current = Cursors.Default;
                        //    return;
                        //}
                        return;

                    }
                    if (res == DialogResult.Yes)
                    {
                        Visible = false;
                        selectedItems = null;
                        errorItems = null;
                        successItems = null;
                        checkedItems = null;

                        // *** BEGIN CHANGE 2013-08-30 ***
                        _statusOfItem = null;
                        // *** END CHANGE 2013-08-30 ***
                        
                        NavigationManager.NavigateHome();
                    }
                    else
                    {
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                } else
                {
                    Visible = false;
                    selectedItems = null;
                    errorItems = null;
                    successItems = null;
                    checkedItems = null;

                    // *** BEGIN CHANGE 2013-08-30 ***
                    _statusOfItem = null;
                    // *** END CHANGE 2013-08-30 ***

                    Cursor.Current = Cursors.Default;
                    NavigationManager.NavigateHome();
                }
            } catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                FileManager.LogException(ex);
            }
            Cursor.Current = Cursors.Default;
        }

        private void AddScannedItem (object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (!String.IsNullOrEmpty(tbItemNr.Text))
                    {
                        AddtoList(tbItemNr.Text);
                    } else if (tbItemNr.Text.Length > 10)
                    {
                        OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Die eingescannte Belegnummer ist zu lang.", null, true, 5000);
                        tbItemNr.Text = "";
                    }
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private List<string> GetSeletedKeys()
        {
            var strArray = new List<string>();
            try
            {
                //strArray.AddRange(from object item in pnItems.Controls
                //                  where item.GetType().Equals("ucDispoItem")
                //                  select (ucDispoItem) item
                //                  into ucDispoItem where ucDispoItem.IsChecked() select ucDispoItem.Id);

                foreach (var control in pnItems.Controls)
                {
                    if (control.GetType().Name.Equals("ucDispoItem"))
                    {
                        var item = (ucDispoItem) control;
                        if (item.IsChecked())
                        {
                            strArray.Add(item.Id);
                        }
                    }
                }
            } catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return strArray;
        }

        private void CallService (object sender, EventArgs e)
        {
            try
            {
                //successItems = null;
                //checkedItems = null;
                //errorItems = null;

                // wenn keine Belege markiert oder gescannt wurden...
                if (checkedItems == null || checkedItems.Count == 0)
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Sie haben keinen Beleg selektiert!", null, true, 5000);
                    return;
                }

                errorItems = new List<string>();

                var res = MessageBox.Show("Möchten Sie den Scanvorgang wirklich abschließen?", "Vorgang beenden?",
                                                  MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (res != DialogResult.Yes)
                {
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                var keys = "&DATATYPE=IH_SELF_DISPO&UPSTREAM=SELFDISPO;I;";

                FileManager.LogMessage("Selbstdisposition für : " + keys);

                keys += GetSelectedKeysAsString();

                var retval = SyncManager.GetInstance().ProcessOnlineRequest(keys);
                //var retval = "|RETURNMESSAGE;E;1000000485";

                FileManager.LogMessage("Returnmessage: " + retval);

                SplitReturnMessage(retval);

                DisplayReturnMessage();

                //if (!string.IsNullOrEmpty(retval))
                //{
                //    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, retval, null, true, 5000);
                //}

                FileManager.LogMessage("Server response: " + retval);

                if (!string.IsNullOrEmpty(retval) && retval.IndexOf("RETURNMESSAGE;E") == -1 && retval.IndexOf("Exception") == -1)
                {
                    Visible = false;
                    selectedItems = null;
                    successItems = null;
                    checkedItems = null;
                    errorItems = null;

                    // *** BEGIN CHANGE 2013-08-30 ***
                    _statusOfItem = null;
                    // *** END CHANGE 2013-08-30 ***

                    // *** BEGIN CHANGE 2015-06-22 ***
                    startSync();
                    // *** END CHANGE 2015-06-2015 ***

                    NavigationManager.NavigateHome();
                } else
                {
                    RefreshData();

                    Cursor.Current = Cursors.Default;
                    return;
                }

            } catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                FileManager.LogException(ex);
            }
            Cursor.Current = Cursors.Default;
        }

        private string GetSelectedKeysAsString()
        {
            var keys = "";
            try
            {
                var selectedItemsList = GetSeletedKeys();
                if (selectedItemsList != null && selectedItemsList.Count > 0)
                {

                    if (selectedItemsList.Count == 1)
                        keys = "|SELFDISPO;I;" + AppConfig.UserId + ";" + selectedItemsList[0];
                    else
                    {
                        var ind = 1;
                        foreach (var key in selectedItemsList)
                        {
                            keys += "|SELFDISPO;I;" + AppConfig.UserId + ";" + key;
                            if (ind != selectedItemsList.Count)
                                keys += ";";
                            ind++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return "";
            }
            return keys;
        }

        private void AddItemByClick (object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (!String.IsNullOrEmpty(tbItemNr.Text))
                    AddtoList(tbItemNr.Text);
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
                Cursor.Current = Cursors.Default;
            }
            Cursor.Current = Cursors.Default;
        }

        private void AddtoList(string item) {

            try
            {
                if (!String.IsNullOrEmpty(item))
                {
                    if (item.TrimStart(' ').Length == 10)
                    {
                        if (selectedItems == null)
                            selectedItems = new List<string>();

                        if (checkedItems == null)
                            checkedItems = new List<string>();

                        foreach (var i in selectedItems)
                        {
                            if (item.Equals(i))
                            {
                                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Beleg wurde schon eingescannt.", null, true, 5000);
                                tbItemNr.Text = "";
                                return;
                            }
                        }

                        // *** BEGIN CHANGE 2013-08-30 ***
                        if (_statusOfItem == null)
                            _statusOfItem = new List<string[]>();

                        var status = GetStatus(item);

                        var itemArray = new string[] { item, status };
                        _statusOfItem.Add(itemArray);
                        // *** END CHANGE 2013-08-30 ***

                        selectedItems.Add(item);
                        checkedItems.Add(item);

                        RefreshData();

                        tbItemNr.Text = "";
                    }
                    else if (item.TrimStart(' ').Length > 10)
                    {
                        OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Die eingescannte Belegnummer ist zu lang.", null, true, 5000);
                        tbItemNr.Text = "";
                    }
                }
            }catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void SplitReturnMessage (string returnstream)
        {
            try
            {
                var index = 1;
                var retList = returnstream.Split('|');
                foreach (var item in retList)
                {
                    if (item.StartsWith("RETURNMESSAGE;"))
                    {
                        var belegnr = "";
                        var messageTyp = item.Substring(14, 1);

                        var ind = item.IndexOf(':', 14);

                        if (ind > -1)
                            belegnr = item.Substring(16, ind - 16);
                        else
                            belegnr = item.Substring(16);

                        if (!string.IsNullOrEmpty(belegnr) && messageTyp.Equals("E"))
                        {
                            if (errorItems == null)
                                errorItems = new List<string>();

                            errorItems.Add(belegnr);

                            //var data = new string[]{index + "", belegnr};
                            //errorArrayItems.Add(data);
                        }


                        if (!string.IsNullOrEmpty(belegnr) && messageTyp.Equals("S"))
                        {
                            if (successItems == null)
                                successItems = new List<string>();
                            
                            successItems.Add(belegnr);

                            //var data = new string[] { index + "", belegnr };
                            //successArrayItems.Add(data);
                        }

                        index++;
                    }
                }
            } catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void CbChangeEvent(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (sender.GetType().Name.Equals("CheckBox"))
                {
                    var cb = (CheckBox) sender;
                    var item = (ucDispoItem) cb.Parent;
                    if (item != null)
                    {
                        if (checkedItems == null)
                            checkedItems = new List<string>();

                        if (item.IsChecked())
                        {
                            var found = false;
                            foreach (var c in checkedItems)
                            {
                                if (item.Id.Equals(c))
                                    found = true;
                            }
                            if (!found)
                                checkedItems.Add(item.Id);
                        } else
                        {
                            checkedItems.Remove(item.Id);
                        }
                    }
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
                Cursor.Current = Cursors.Default;
            }
            Cursor.Current = Cursors.Default;
        }

        public void DisplayReturnMessage ()
        {
            var message = "";

            try
            {
                if (errorItems != null && errorItems.Count > 0)
                {
                    if (errorItems.Count == 1)
                        message = "Fehler bei der Verbuchung des Belegs " + errorItems[0] + "."; 
                    else
                    {
                        message = "Fehler bei der Verbuchung der Belege: ";
                        var items = "";
                        var count = 0;
                        foreach (var e in errorItems)
                        {
                            if (count < errorItems.Count-1)
                            {
                                items += e + ", ";
                            } else
                            {
                                items += e;
                            }
                            count++;
                        }
                        message += items;
                    }

                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, message, null, true, 5000);
                }

                if (successItems != null && successItems.Count > 0)
                {
                    if (successItems.Count == 1)
                        message = successItems.Count + " Beleg gebucht.";
                    else if (successItems.Count > 1)
                        message = successItems.Count + " Belege gebucht.";

                    OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now, message, null, true, 5000);
                }

            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        // *** TEST
        public static int ind;


        // *** BEGIN CHANGE 2013-08-29 ***
        private static string GetStatus(string item)
        {
            var status = "";
            try
            {
                if (!String.IsNullOrEmpty(item))
                {
                    if (item.TrimStart(' ').Length == 10)
                    {
                        var keys = "&DATATYPE=IH_DISPO_STATUS&UPSTREAM=DISPOSTATUS;";

                        keys += item;

                        FileManager.LogMessage("Status : " + keys);

                        var retval = SyncManager.GetInstance().ProcessOnlineRequest(keys);
                        //retval = "|STATUS;1000000485;0;L";

                        FileManager.LogMessage("Returnmessage: " + retval);

                        var retBelnr = "";
                        var messType = "";
                        var statshort = GetStatusShort(retval, out retBelnr, out messType);

                        if (retBelnr.Equals(item))
                        {
                            if (!messType.Equals("0"))
                                status = "Err:" + statshort;
                            else if (statshort.ToUpper().StartsWith("L"))
                                status = "offene Ausliefung";
                            else if (statshort.ToUpper().StartsWith("O"))
                                status = "offene Abholung";
                        } else
                        {
                            status = "";
                        }
                    }
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return status;
        }

        private static string GetStatusShort (string returnmessage, out string belnr, out string messType)
        {
            var statShort = "";
            messType = "";
            belnr = "";
            try
            {
                if (!string.IsNullOrEmpty(returnmessage))
                {
                    var statArray = returnmessage.Split('|');
                    foreach (var item in statArray)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var status = item.Split(';');

                            if (status != null)
                            {
                                belnr = status[1];
                                messType = status[2];
                                statShort = status[3];
                            }
                        }
                    }
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return statShort;
        }

        private string GetStatusFromList(string belnr)
        {
            var status = "";
            try
            {
                if (_statusOfItem != null && _statusOfItem.Count > 0)
                {
                    foreach (var item in _statusOfItem)
                    {
                       if (item[0].Equals(belnr))
                       {
                           status = item[1];
                           break;
                       }
                    }
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return status;
        }
        // *** END CHANGE 2013-08-29 ***

        // *** BEGIN CHANGE 2015-06-22 ***
        private void startSync ()
        {

            try
            {
                var t1 = DateTime.Now;
                Cursor.Current = Cursors.WaitCursor;
                var syncManager = SyncManager.GetInstance();
                if (AppConfig.FlagDocuments.Equals("X"))
                    syncManager._reqDoc = true;
                else
                    syncManager._reqDoc = false;
                if (AppConfig.FlagCustomizing.Equals("X"))
                    syncManager._reqCust = true;
                else
                    syncManager._reqCust = false;
                if (AppConfig.FlagMasterdata.Equals("X"))
                    syncManager._reqMD = true;
                else
                    syncManager._reqMD = false;
                if (AppConfig.FlagConfirmation.Equals("X"))
                    syncManager._confRQ = true;
                else
                    syncManager._confRQ = false;
                if (AppConfig.FlagMassdata.Equals("X"))
                    syncManager._massData = true;
                else
                    syncManager._massData = false;
                Cust_012.ClearInstance();
                syncManager.SynchronizeWithBackend(false);
                RefreshData();

                ClearOldFiles();
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                FileManager.LogMessage("Fehler beim Sync nach Dispo: ");
                FileManager.LogException(ex);
            }
        }

        private void ClearOldFiles()
        {
            //if(AppConfig.FileResidenceDays <= 0)
            if (AppConfig.FileResidenceDays < 0)
                return;
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
            var files = Directory.GetFiles(path);

            foreach (var file in files)
            {
                if (file.ToLower().EndsWith(".pdf"))
                {
                    var creationTime = File.GetCreationTime(file);
                    //if (creationTime <= DateTime.Now)
                    if (creationTime <= DateTime.Now.AddDays(-1 * AppConfig.FileResidenceDays))
                    {
                        var docs = DeliveryManager.GetDocForFile(file.Substring(file.LastIndexOf('\\')));
                        var delete = true;
                        foreach (var doc in docs)
                        {
                            if (!doc.Updflag.Equals("X"))
                            {
                                delete = false;
                                break;
                            }
                        }
                        if (delete)
                        {
                            FileManager.LogMessage("Delete file: " + file);
                            File.Delete(file);
                        }
                    }
                }
            }
        }

        // *** END CHANGE 2015-06-22 ***
    }
}
