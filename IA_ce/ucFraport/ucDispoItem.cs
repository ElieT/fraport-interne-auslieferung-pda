﻿using System;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucDispoItem : OxUserControlCe
    {
        private string _id;
        //public ucDispoItem(string belegnr)
        public ucDispoItem(string belegnr, string status)
        {
            InitializeComponent();
            Id = belegnr;
            lblNumber.Text = belegnr;
            //cbItem.Checked = true;
            pbError.Visible = false;
            pbSuccess.Visible = false;

            // *** BEGIN CHANGE 2013-08-30 ***
            lblStatus.Text = status;
            FileManager.LogMessage("Display Status: " + status + ";");
            // *** END CHANGE 2013-08-30 ***
        }

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool IsChecked()
        {
            if (cbItem.Checked)
                return true;
            return false;
        }

        public EventHandler ChangeEvent;
        private void ChangeBoxValue(object sender, EventArgs e)
        {
            if (ChangeEvent != null)
            {
                ChangeEvent(sender, e);
            }
        }

        public void SetVisible(string mode)
        {
            if (mode.ToLower().Equals("error"))
            {
                pbError.Visible = true;
                pbSuccess.Visible = false;
            } else
            {
                pbError.Visible = false;
                pbSuccess.Visible = true;
            }
        }

        public void SetCheckBox (bool check)
        {
            cbItem.Checked = check;
        }
    }
}
