﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;
using oxmc.IA_ce;
using oxmc.IA_ce.ucFraport;

namespace IA_ce.ucFraport
{
    public partial class ucPickUpList : OxUserControlCe
    {
        private static ucPickUpList _instance;
        private List<Delivery> _pickUpList;
        private List<Delivery> _changedDeliverys;

        private ucPickUpList()
        {
            InitializeComponent();
        }

        public static ucPickUpList getInstance()
        {
            if (_instance == null)
            {
                _instance = new ucPickUpList();              
            }
            return _instance;
        }

        public void refreshData ()
        {
            try
            {
                foreach (Control item in pnDelivItems.Controls)
                {
                    item.Visible = false;
                    item.Dispose();
                }
                pnDelivItems.Controls.Clear();

                int top = 0;

                _pickUpList = DeliveryManager.getAllPickUps();
                if (_pickUpList != null && _pickUpList.Count > 0)
                {
                    lblBuilding.Text = _pickUpList[0].Gebnr;

                    foreach (Delivery delivery in _pickUpList)
                    {
                        // Item ist eine Abholung
                        delivery.IsPickUp = "X";
                        DeliveryManager.setPickUpFlag(delivery);

                        if (String.IsNullOrEmpty(delivery.MobileKey))
                        {
                            ucPickUpListItem listItem = new ucPickUpListItem(delivery);
                            listItem.ClickEvent += ClickEvent;
                            listItem.Top = top * listItem.Height + 2;
                            if (top % 2 == 0)
                                listItem.BackColor = Color.LightGray;
                            pnDelivItems.Controls.Add(listItem);
                            top++;
                        }
                    }

                    pbSave.Visible = true;
                } 
                else
                {
                    lblBuilding.Text = "";
                    pbSave.Visible = false;
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                Cursor.Current = Cursors.WaitCursor;
            }
        }

        private void pbHomeClicked (object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                NavigationManager.NavigateHome();
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            } finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void pbBackClicked(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                NavigationManager.NavigateBack();
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            } finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        public EventHandler ClickEvent;
        public override void btSave_Click(object sender, EventArgs e)
        {
            _changedDeliverys = new List<Delivery>();

            Cursor.Current = Cursors.WaitCursor;
            // für jedes angezeigte Element im Panel
            foreach (ucPickUpListItem item in pnDelivItems.Controls)
            {
                // current delivery
                Delivery curDeliv = item.Deliv;

                // Holen der neuen Werte
                // Status: zugestellt
                if (item.GetValueRbDelivered())
                {
                    // Überprüfung des vorherigen Werts.
                    curDeliv.Status = "A";
                    curDeliv.Lidat = DateTime.Now.Date;
                    curDeliv.Gtext = "";
                    curDeliv.Updflag = "U";
                    curDeliv.Lfname = AppConfig.UserId;

                    // Hinzufügen zu Liste der übergebenen Deliverys
                    _changedDeliverys.Add(curDeliv);
                }
                // Status: fehlgeschlagen
                else if (item.GetValueRbFailed())
                {
                    if (!(item.GetChangedText().Equals("")))
                    {
                        curDeliv.Status = "F";

                        curDeliv.Grund = DeliveryManager.GetStatus(item.GetChangedText());
                        curDeliv.Gtext = item.GetChangedText();
                        // Updflag
                        curDeliv.Updflag = "U";

                        // Hinzufügen zu Liste der übergebenen Deliverys
                        _changedDeliverys.Add(curDeliv);
                    }
                    else
                    {
                        OxStatus.GetInstance().TriggerEventMessage(0, 'E', DateTime.Now, "Geben Sie für alle fehlgeschlagenen Auslieferbelege einen Grund an.", null, true, 1000);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
            }

            if (_changedDeliverys.Count > 0)
            {
                foreach (Delivery delivery in _changedDeliverys)
                {
                    if ((!delivery.Status.Equals("A")) && (!String.IsNullOrEmpty(delivery.Status)) && (String.IsNullOrEmpty(delivery.Gtext)))
                    {
                        OxStatus.GetInstance().TriggerEventMessage(0, 'E', DateTime.Now, "Geben Sie für alle fehlgeschlagenen Auslieferbelege einen Grund an.", null, true, 10);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }
                // Aufruf der nächsten UI
                ucDeliveryPdf pdf = new ucDeliveryPdf();
                pdf.ChangeUserControl += ChangeUserControl;
                pdf.RefreshData(_changedDeliverys);
                Cursor.Current = Cursors.Default;
                NavigationManager.NavigateForward(pdf);
            }
            else
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void setStatusFetched(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                foreach (ucPickUpListItem item in pnDelivItems.Controls)
                {
                    item.setStatusFetched();
                }

                // refreshData();

                Cursor.Current = Cursors.Default;
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
