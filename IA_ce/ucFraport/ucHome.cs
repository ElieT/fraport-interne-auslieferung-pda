﻿using System;
using System.Collections.Specialized;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using IA_ce;
using IA_ce.ucFraport;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;
//using oxmc.UI_ce.ucCommon;
//using oxmc.UI_ce.ucNotif;
//using oxmc.UI_ce.ucOrder;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucHome : OxUserControlCe
    {
        private static readonly object Padlock = new object();
        private static ucHome _instance;
        private SyncManager _syncManager;
        private Boolean _isNewUser = false;
        private OxStatus _oxStatus;

        public Boolean IsNewUser
        {
            get { return _isNewUser; }
            set { _isNewUser = value; }
        }

        private ucHome()
        {
            InitializeComponent();

            pbDelivery.Visible = true;
            pbSync.Visible = true;

            lblUser2.Text = AppConfig.UserId;
            lblUserVal.Text = AppConfig.UserId;
            _oxStatus = _oxStatus = OxStatus.GetInstance();
        }

        public static ucHome GetInstance()
        {
            lock (Padlock)
            {
                if (_instance == null || _instance.IsDisposed)
                {
                    /*Cursor.Current = Cursors.WaitCursor;
                    DBManager.ClearColumnOrder();
                    DBManager.CreateColumnOrder();
                    Cursor.Current = Cursors.Default;*/

                    _instance = new ucHome();
                    _instance._syncManager = SyncManager.GetInstance();
                    Cursor.Current = Cursors.WaitCursor;
                    String _sapUserDetails = "";
                    NameValueCollection parameter = new NameValueCollection();
                    parameter.Add("PDA", "PDA");
                    parameter.Add("PDA", "PDA");
                    parameter.Add("pa_ping", "ping");
                    parameter.Add("userid", AppConfig.UserId);
                    parameter.Add("pw", AppConfig.Pw);
                    _sapUserDetails = _instance._syncManager.GetUserName(parameter, "user_details.htm", true);

                    if (_sapUserDetails.IndexOf("ping") > -1)
                        SyncManager.iSAPAvailable = "Ja!";
                    _instance.UpdateAmpel(_sapUserDetails);
                    //_instance._rfidEnabled = AppConfig.RfidEnabled;

                    var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                    _instance.lblVersion.Text = version.ToString();
                    _instance.lblVersion2.Text = version.ToString();

                    Cursor.Current = Cursors.Default;
                }
            }
            _instance.RefreshData();

            return _instance;
        }

        public void RefreshData()
        {
            pbDelivery.Visible = true;
            lblDelivery.Visible = true;

            //FileManager.DeleteOldPdfFiles();

            //ClearOldFiles();
        }

        public void UpdateAmpel(String userDetails)
        {
            try
            {
                lblUser2.Text = AppConfig.UserId;
                lblUserVal.Text = AppConfig.UserId;
                lblMandtVal.Text = AppConfig.Mandt;

                if (SyncManager.iSAPAvailable.Equals("Nein!"))
                {
                    String lastSyncDB = SyncLogManager.GetLastSync();
                    if (lastSyncDB.Length > 11)
                    {
                        lblLastSyncVal.Text =
                            lastSyncDB.Substring(6, 2) + "." +
                            lastSyncDB.Substring(4, 2) + "." +
                            lastSyncDB.Substring(0, 4) + " " +
                            lastSyncDB.Substring(8, 2) + ":" +
                            lastSyncDB.Substring(10, 2) + ":" +
                            lastSyncDB.Substring(12, 2);
                    }
                }
                else
                {
                    if (userDetails != null && !userDetails.Equals("") &&
                        userDetails.IndexOf('|') > -1)
                    {
                        lblLastSyncVal.Text =
                            userDetails.Split('|')[2].Substring(6, 2) +
                            "." +
                            userDetails.Split('|')[2].Substring(4, 2) +
                            "." +
                            userDetails.Split('|')[2].Substring(0, 4) +
                            " " +
                            userDetails.Split('|')[2].Substring(8, 2) +
                            ":" +
                            userDetails.Split('|')[2].Substring(10, 2) +
                            ":" +
                            userDetails.Split('|')[2].Substring(12, 2);
                        lblUserVal.Text += " " + userDetails.Split('|')[0];
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                Cursor.Current = Cursors.Default;
            }
        }

        private void pbExit_Click(object sender, EventArgs e)
        {

            OxDbManager.GetInstance().CommitTransaction(); 
            Application.Exit();
        }

        private void pbSync_Click(object sender, EventArgs e)
        {
            try
            {
                var t1 = DateTime.Now;
                Cursor.Current = Cursors.WaitCursor;
                var syncManager = SyncManager.GetInstance();
                if (AppConfig.FlagDocuments.Equals("X"))
                    _syncManager._reqDoc = true;
                else
                    _syncManager._reqDoc = false;
                if (AppConfig.FlagCustomizing.Equals("X"))
                    _syncManager._reqCust = true;
                else
                    _syncManager._reqCust = false;
                if (AppConfig.FlagMasterdata.Equals("X"))
                    _syncManager._reqMD = true;
                else
                    _syncManager._reqMD = false;
                if (AppConfig.FlagConfirmation.Equals("X"))
                    _syncManager._confRQ = true;
                else
                    _syncManager._confRQ = false;
                if (AppConfig.FlagMassdata.Equals("X"))
                    _syncManager._massData = true;
                else
                    _syncManager._massData = false;
                Cust_012.ClearInstance();
                syncManager.SynchronizeWithBackend(false);
                RefreshData();

                ClearOldFiles();
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void ClearOldFiles()
        {
            //if(AppConfig.FileResidenceDays <= 0)
            if (AppConfig.FileResidenceDays < 0)
                return;
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
            var files = Directory.GetFiles(path);

            foreach(var file in files)
            {
                if(file.ToLower().EndsWith(".pdf"))
                {
                    var creationTime = File.GetCreationTime(file); 
                    //if (creationTime <= DateTime.Now)
                    if (creationTime <= DateTime.Now.AddDays(-1 * AppConfig.FileResidenceDays))
                    {
                        var docs = DeliveryManager.GetDocForFile(file.Substring(file.LastIndexOf('\\')));
                        var delete = true;
                        foreach(var doc in docs)
                        {
                            if(!doc.Updflag.Equals("X"))
                            {
                                delete = false;
                                break;
                            }
                        }
                        if (delete)
                        {
                            FileManager.LogMessage("Delete file: " + file);
                            File.Delete(file);
                        }
                    }
                }
            }
        }

        private void PbLogoutClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            OxDbManager.GetInstance().CommitTransaction();
            try
            {
                ucHome.GetInstance().Dispose();
                //ucUser.GetInstance().ShowThisControl(ucUser.GetInstance().Parent);
                NavigationManager.NavigateBack();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            Cursor.Current = Cursors.Default;
        }

        private void pbDelivery_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                //SendImage();

                ucBuildList.GetInstance().RefreshData();
                ChangeUserControl += ChangeUserControl;
                NavigationManager.NavigateForward(ucBuildList.GetInstance());
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            Cursor.Current = Cursors.Default;
        }

        private void SendImage()
        {
            String pdfFile = AppConfig.GetAppPath() + "\\Dok1.pdf";

            IA_Doch header = new IA_Doch();
            header.Description = "BILD";
            header.DocType = "MEB";
            header.Email = "";
            header.Filename = pdfFile;
            header.RefType = "OR";
            header.RefObject = "";
            header.Spras = "D";

            var hashString = AppConfig.UserId + AppConfig.Mandt +
                             DateTime.Now.ToString("yyyyMMddHHmmffff");
            var hash = OxCrypto.HashPasswordForStoringInConfigFile(hashString);
            header.MobileKey = hash;

            if (!FileManager.Processing)
            {
                //FileManager.IA_ReadBinFile(header);
                FileManager.IA_ReadBinFile(header);
            }
        }

        ///*public void SVMCheck()
        //{
        //    Cursor.Current = Cursors.WaitCursor;
        //    /* Version Check */
        //    _oxStatus = OxStatus.GetInstance();
        //    _syncManager = SyncManager.GetInstance();
        //    String currentVersion = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString().PadLeft(3, '0') + "." +
        //                            Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString().PadLeft(3, '0') + "." +
        //                            Assembly.GetExecutingAssembly().GetName().Version.Build.ToString().PadLeft(3, '0');

        //    var currMajor = Assembly.GetExecutingAssembly().GetName().Version.Major;
        //    var currMinor = Assembly.GetExecutingAssembly().GetName().Version.Minor;
        //    var currBuild = Assembly.GetExecutingAssembly().GetName().Version.Build;
        //    String SVMVersion = _oxStatus.SVMVersion;
        //    if (string.IsNullOrEmpty(SVMVersion))
        //        return;

        //    int svmMajor = 0, svmMinor = 0, svmBuild = 0;
        //    try
        //    {
        //        svmMajor = int.Parse(SVMVersion.Split('.')[0]);
        //        svmMinor = int.Parse(SVMVersion.Split('.')[1]);
        //        svmBuild = int.Parse(SVMVersion.Split('.')[2]);
        //    }
        //    catch (Exception ex)
        //    {
        //        Cursor.Current = Cursors.Default;
        //        FileManager.LogException(ex);
        //        return;
        //    }

        //    var newVersion = false;
        //    if (svmMajor > currMajor)
        //        newVersion = true;
        //    else
        //    {
        //        if (svmMajor < currMajor)
        //            newVersion = false;
        //        else
        //        {
        //            if (svmMinor > currMinor)
        //                newVersion = true;
        //            else
        //            {
        //                if (svmMinor < currMinor)
        //                    newVersion = false;
        //                else
        //                {
        //                    if (svmBuild > currBuild)
        //                        newVersion = true;
        //                }
        //            }
        //        }
        //    }

        //    if (newVersion)
        //    {
        //        NameValueCollection parameter = new NameValueCollection();
        //        parameter.Set("pa_version_new", SVMVersion);
        //        parameter.Set("pa_mobileuser", AppConfig.UserId);
        //        _oxStatus.SapUserDetails = _syncManager.GetUserName(parameter, "svm_get_meta.htm", true);
        //        //SyncManager.GetInstance().GetUserName()
        //        DialogResult res;

        //        if (UserManager.GetNumberOfRefusedUpdates(AppConfig.Mandt, AppConfig.UserId) >= 3)
        //            res = MessageBox.Show("Aktuelle Version " + currentVersion + ". Es liegt eine neue Version (" + SVMVersion + ") vor. Sie müssen diese Installieren bevor Sie mit der Arbeit fortfahren können.", "Softwareupdate",
        //              MessageBoxButtons.OK, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
        //        else
        //            res = MessageBox.Show("Aktuelle Version " + currentVersion + ". Es liegt eine neue Version (" + SVMVersion + ") vor, möchten Sie aktualisieren?", "Softwareupdate",
        //              MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

        //        if (res == DialogResult.Yes || res == DialogResult.OK)
        //        {
        //            if (!String.IsNullOrEmpty(AppConfig.Pw))
        //            {
        //                var sqlStmt =
        //                    "SELECT * FROM S_SYNCDELTA WHERE TABLENAME NOT LIKE 'D_SYNC_%' " +
        //                    "AND UPLOAD = 'X' AND USERID = '" + AppConfig.UserId + "' AND MANDT = '" + AppConfig.Mandt + "'";

        //                var cmd = new SQLiteCommand(sqlStmt);
        //                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
        //                if (result.Count != 0)
        //                {
        //                    ProcessSyncRoutine();
        //                }
        //            }

        //            //ProcessSyncRoutine();
        //            string AppPath =
        //                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
        //            if (AppPath.Length > 7 && AppPath.IndexOf("file:") != -1)
        //                AppPath = AppPath.Substring(6, AppPath.Length - 6);

        //            String svmConfigFile = AppPath + "\\..\\SVM\\svm.config";
        //            String svmApp = AppPath + "\\..\\SVM\\SVM.exe";
        //            String configMode = "app_prd";

        //            if (!Directory.Exists(svmConfigFile.Substring(0, svmConfigFile.LastIndexOf("\\"))))
        //            {
        //                _oxStatus.SetStatusMessage("Updater Verzeichnis nicht vorhanden", 5000);
        //                Cursor.Current = Cursors.Default;
        //                return;
        //            }
        //            using (var writer = new StreamWriter(svmConfigFile, false))
        //            {
        //                writer.WriteLine(configMode);
        //                writer.WriteLine(AppConfig.SVMApplicationId);
        //                writer.WriteLine(AppConfig.SVMClientOs);
        //                writer.WriteLine(AppConfig.UserId);
        //                writer.WriteLine(SVMVersion);
        //                writer.WriteLine(currentVersion);
        //                writer.WriteLine(_oxStatus.SVMFilename);
        //                writer.WriteLine(AppConfig.SVMTempDirectory);
        //                writer.WriteLine(AppPath);
        //                writer.WriteLine(_oxStatus.GetParameterValue("checksum"));
        //            }
        //            if (!File.Exists(svmApp))
        //            {
        //                _oxStatus.SetStatusMessage("Updater Programm nicht vorhanden", 5000);
        //                Cursor.Current = Cursors.Default;
        //                return;
        //            }
        //            Cursor.Current = Cursors.Default;

        //            System.Diagnostics.Process process = new System.Diagnostics.Process();
        //            process.StartInfo.FileName = svmApp;
        //            process.Start();

        //            //Process.Start(svmApp);
        //            Application.Exit();
        //        }
        //        else
        //        {
        //            UserManager.AddRefusedUpdate(AppConfig.Mandt, AppConfig.UserId);
        //        }
        //    }
        //    Cursor.Current = Cursors.Default;
        //}*/

        public void SVMCheck()
        {
            Cursor.Current = Cursors.WaitCursor;
            /* Version Check */
            _oxStatus = OxStatus.GetInstance();
            _syncManager = SyncManager.GetInstance();
            String currentVersion = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString().PadLeft(3, '0') + "." +
                                    Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString().PadLeft(3, '0') + "." +
                                    Assembly.GetExecutingAssembly().GetName().Version.Build.ToString().PadLeft(3, '0');

            var currMajor = Assembly.GetExecutingAssembly().GetName().Version.Major;
            var currMinor = Assembly.GetExecutingAssembly().GetName().Version.Minor;
            var currBuild = Assembly.GetExecutingAssembly().GetName().Version.Build;
            String SVMVersion = _oxStatus.SVMVersion;
            if (string.IsNullOrEmpty(SVMVersion))
                return;

            int svmMajor = 0, svmMinor = 0, svmBuild = 0;
            try
            {
                svmMajor = int.Parse(SVMVersion.Split('.')[0]);
                svmMinor = int.Parse(SVMVersion.Split('.')[1]);
                svmBuild = int.Parse(SVMVersion.Split('.')[2]);
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                FileManager.LogException(ex);
                return;
            }

            var newVersion = false;
            if (svmMajor > currMajor)
                newVersion = true;
            else
            {
                if (svmMajor < currMajor)
                    newVersion = false;
                else
                {
                    if (svmMinor > currMinor)
                        newVersion = true;
                    else
                    {
                        if (svmMinor < currMinor)
                            newVersion = false;
                        else
                        {
                            if (svmBuild > currBuild)
                                newVersion = true;
                        }
                    }
                }
            }

            if (newVersion)
            {
                NameValueCollection parameter = new NameValueCollection();
                parameter.Set("pa_version_new", SVMVersion);
                parameter.Set("pa_mobileuser", AppConfig.UserId);
                _oxStatus.SapUserDetails = _syncManager.GetUserName(parameter, "svm_get_meta.htm", true);
                //SyncManager.GetInstance().GetUserName()
                DialogResult res;

                if (UserManager.GetNumberOfRefusedUpdates(AppConfig.Mandt, AppConfig.UserId) >= 3)
                    res = MessageBox.Show("Aktuelle Version " + currentVersion + ". Es liegt eine neue Version (" + SVMVersion + ") vor. Sie müssen diese Installieren bevor Sie mit der Arbeit fortfahren können.", "Softwareupdate",
                      MessageBoxButtons.OK, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                else
                    res = MessageBox.Show("Aktuelle Version " + currentVersion + ". Es liegt eine neue Version (" + SVMVersion + ") vor, möchten Sie aktualisieren?", "Softwareupdate",
                      MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                if (res == DialogResult.Yes || res == DialogResult.OK)
                {
                    if (!String.IsNullOrEmpty(AppConfig.Pw))
                    {
                        var sqlStmt =
                            "SELECT * FROM S_SYNCDELTA WHERE TABLENAME NOT LIKE 'D_SYNC_%' " +
                            "AND UPLOAD = 'X'";

                        var cmd = new SQLiteCommand(sqlStmt);
                        var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                        if (result.Count != 0)
                        {
                            ProcessSyncRoutine();
                        }
                    }

                    //ProcessSyncRoutine();
                    string AppPath =
                        Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                    if (AppPath.Length > 7 && AppPath.IndexOf("file:") != -1)
                        AppPath = AppPath.Substring(6, AppPath.Length - 6);

                    String svmConfigFile = AppPath + "\\SVM\\svm.config";
                    String svmApp = AppPath + "\\SVM\\SVM.exe";
                    String configMode = "app_prd";
                    #if DEBUG 
                    svmConfigFile = AppPath + "\\..\\SVM\\svm.config";
                    svmApp = AppPath + "\\..\\SVM\\SVM.exe";
                    #endif

                    if (!Directory.Exists(svmConfigFile.Substring(0, svmConfigFile.LastIndexOf("\\"))))
                    {
                        _oxStatus.SetStatusMessage("Updater Verzeichnis nicht vorhanden", 5000);
                        _oxStatus.TriggerEventMessage(0, 'E', DateTime.Now, "Updater Verzeichnis nicht vorhanden", null, true);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                    using (var writer = new StreamWriter(svmConfigFile, false))
                    {
                        writer.WriteLine(configMode);
                        writer.WriteLine(AppConfig.SVMApplicationId);
                        writer.WriteLine(AppConfig.SVMClientOs);
                        writer.WriteLine(AppConfig.UserId);
                        writer.WriteLine(SVMVersion);
                        writer.WriteLine(currentVersion);
                        writer.WriteLine(_oxStatus.SVMFilename);
                        writer.WriteLine(AppConfig.SVMTempDirectory);
                        writer.WriteLine(AppPath);
                        writer.WriteLine(_oxStatus.GetParameterValue("checksum"));
                    }
                    if (!File.Exists(svmApp))
                    {
                        _oxStatus.SetStatusMessage("Updater Programm nicht vorhanden", 5000);
                        _oxStatus.TriggerEventMessage(0, 'E', DateTime.Now, "Updater Programm nicht vorhanden", null, true);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                    Cursor.Current = Cursors.Default;

                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = svmApp;
                    process.Start();

                    //Process.Start(svmApp);
                    Application.Exit();
                }
                else
                {
                    UserManager.AddRefusedUpdate(AppConfig.Mandt, AppConfig.UserId);
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void pbDispo_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ucDispo.GetInstance().RefreshData();
                NavigationManager.NavigateForward(ucDispo.GetInstance());
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                FileManager.LogException(ex);
            }   
        }

        private void pbPickUpClicked (object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                ucPickUpList.getInstance().refreshData();
                NavigationManager.NavigateForward(ucPickUpList.getInstance());
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            } finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        public void sendLogFiles (object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var mbRes = MessageBox.Show("Möchten Sie die Logdaten jetzt übertragen?",
                                            "Log-Files senden", MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (mbRes == DialogResult.Yes)
                {
                    Focus();
                    BringToFront();
                    OxDbManager.GetInstance().CommitTransaction();
                    Invoke((ThreadStart)delegate
                    {
                        HelpManager.TriggerDataSave(FormMain.Instance.WebServerHelpMeStarted,
                                                    FormMain.Instance.WebServerHelpMeEnded,
                                                    FormMain.Instance.WebServerHelpMeProgress);
                    });
                }
            }catch (Exception ex)
            {
                FileManager.LogException(ex);
            } finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
