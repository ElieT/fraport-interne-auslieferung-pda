﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using IA_ce;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucEventMessage : OxUserControlCe
    {
        private static ucEventMessage instance;
        private bool _maximized;
        private int _displayTime;
        private System.Threading.Timer _closePopupTimer;
        private bool mbIsPermanent;
        ListViewItem _lastItem = new ListViewItem();

        private ucEventMessage()
        {
            InitializeComponent();
            foreach (var message in PushMessageManager.GetPushMessages())
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'I', message.MessageDate, message.Message, null, false);
            }
        }


        public void ShowEvent(Control parent)
        {
            Refresh();
            Visible = true;
            BringToFront();

            //var height = 225;
            
            // *** BEGIN CHANGE 2013-07-03 ***
            var height = 113;
            //if (!AppConfig.SVMClientOs.ToUpper().Equals("MC75"))
            if (Screen.PrimaryScreen.Bounds.Height == 320)
                Width = 239;
            else
                Width = 480;
            // *** END CHANGE 2013-07-03 ***

            if (OxStatus.MessageList.Count > 0 && !OxStatus.MessageList[0]["Type"].Equals("E"))
            {
                //height = 40;
                height = 22;
                // *** BEGIN CHANGE 2013-07-03 ***
                //if (AppConfig.DeviceId.Equals("MC9090"))
                //    height = 44;
                // *** END CHANGE 2013-07-03 ***
            }
            else
                _maximized = true;

            ShowThisControl(parent.Height, 0, parent);
        }

        public void Refresh()
        {
            if (OxStatus.MessageList.Count > 0)
            {
                switch (OxStatus.MessageList[0]["Type"])
                {
                    case "E":
                        lblTitle.ForeColor = Color.Red;
                        lblTitle.Text = "Fehler!";
                        break;
                    case "S":
                        lblTitle.ForeColor = Color.Green;
                        lblTitle.Text = "Erfolg!";
                        break;
                    case "I":
                        lblTitle.ForeColor = Color.Black;
                        lblTitle.Text = "Hinweis";
                        break;
                    default:
                        lblTitle.ForeColor = Color.Black;
                        break;
                }
            }
            tbMessages.Text = "";
            foreach (var element in OxStatus.MessageList)
            {
                tbMessages.Text += element["Time"] + " - " + (element["Message"].Replace("\n", " ")).Replace("\r", "") + "\r\n";
            }

            if (OxStatus.MessageList.Count > 0 && OxStatus.MessageList[OxStatus.MessageList.Count - 1].ContainsKey("Timeout") && !OxStatus.MessageList[0]["Type"].Equals("E"))
            {
                _displayTime = int.Parse(OxStatus.MessageList[OxStatus.MessageList.Count - 1]["Timeout"]);
                //_closePopupTimer = new System.Threading.Timer(DisplayTimeEvent, null, _displayTime, 0);
            }
        }


        delegate void ClosePopup();

        public void DisplayTimeEvent(object source)
        {
            try
            {
                var cp = new ClosePopup(ClosePopupMethod);
                Invoke(cp);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static ucEventMessage GetInstance()
        {
            if (instance == null)
            {
                instance = new ucEventMessage();
            }

            return instance;
        }

        private void ClosePopupMethod()
        {
            if (!mbIsPermanent)
                Visible = false;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Visible = false;
            mbIsPermanent = false;
        }

        private void TbGotFocus(object sender, EventArgs e)
        {
            mbIsPermanent = true;
        }

        private void lblTitle_Click(object sender, EventArgs e)
        {
            mbIsPermanent = true;
            if (_maximized)
            {
                _maximized = false;
                Visible = false;
                Refresh();
                //ShowThisControl(40);

                // *** BEGIN CHANGE 2014-12-17 ***
                if (Screen.PrimaryScreen.Bounds.Height == 320)
                    ShowThisControl(20);
                else
                    ShowThisControl(40);
                // *** END CHANGE 2014-12-17 ***
            }
            else
            {
                _maximized = true;
                //Visible = false;
                Visible = true;
                Refresh();
                //ShowThisControl(225);
                //ShowThisControl(113);

                // *** BEGIN CHANGE 2014-12-17 ***
                if (Screen.PrimaryScreen.Bounds.Height == 320)
                    ShowThisControl(113);
                else
                    ShowThisControl(226);
                // *** END CHANGE 2014-12-17 ***
            }
        }

        public void BtMaximizeClick()
        {
            //var height = 255;
            var height = 113;
            if (Screen.PrimaryScreen.Bounds.Height != 320)
                height = 226;

            if (OxStatus.MessageList.Count > 0 && !OxStatus.MessageList[0]["Type"].Equals("E"))
            //if (OxStatus.MessageList.Count > 0)
            {
                //height = 40;
                if (Screen.PrimaryScreen.Bounds.Height == 320)
                    height = 22;
                else
                    height = 44;

                _maximized = false;
            }
            else
                _maximized = true;
            ShowThisControl(height);

        }

        public void ShowThisControl(int height)
        {

            // MessageBox.Show("ShowThisControl(int height) " + height);
            Refresh();
            Visible = true;
            Parent = FormMain.Instance;
            //Top = 267 - height;
            // *** BEGIN CHANGE 2013-07-03 ***
            //if (!AppConfig.SVMClientOs.ToUpper().Equals("MC75"))
            if (Screen.PrimaryScreen.Bounds.Height == 320)
                Top = 270 - height;
            else
            {
                // Top = 540 - height;
                var realHeight = Screen.PrimaryScreen.Bounds.Height;
                Top = 540 - height;
                //Top = 480 - height;
            }
            // *** END CHANGE 2013-07-03 ***
            //Top = 270 - height;
            Left = 0;
            
            // *** BEGIN CHANGE 2013-07-03 ***
            if (Screen.PrimaryScreen.Bounds.Height == 320)
                Width = 240;
            else
                Width = 480;
            // *** END CHANGE 2013-07-03 ***
            Height = height;
            BringToFront();

            //NavigationManager.NavigateForward(this);

        }
    }
}

