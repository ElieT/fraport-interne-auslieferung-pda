﻿namespace oxmc_ce.IA_ce.ucFraport
{
    partial class ucUser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucUser));
            this.lblPw2 = new System.Windows.Forms.Label();
            this.lblPwOld = new System.Windows.Forms.Label();
            this.lblPw = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblMandt = new System.Windows.Forms.Label();
            this.tbPw2 = new oxmc.Common.Controls.OxTextBox(this.components);
            this.tbPwOld = new oxmc.Common.Controls.OxTextBox(this.components);
            this.tbPw = new oxmc.Common.Controls.OxTextBox(this.components);
            this.tbUserid = new oxmc.Common.Controls.OxTextBox(this.components);
            this.tbMandt = new oxmc.Common.Controls.OxTextBox(this.components);
            this.lblMessage = new System.Windows.Forms.Label();
            this.pnlClient = new System.Windows.Forms.Panel();
            this.pnlUser = new System.Windows.Forms.Panel();
            this.pnlOldPwd = new System.Windows.Forms.Panel();
            this.pnlPwd = new System.Windows.Forms.Panel();
            this.pnlPwdRepeat = new System.Windows.Forms.Panel();
            this.inputPanel1 = new Microsoft.WindowsCE.Forms.InputPanel(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblClose = new System.Windows.Forms.Label();
            this.pbClose = new oxmc.Common.Controls.OxPictureBox(this.components);
            this.lblReset = new System.Windows.Forms.Label();
            this.btnReset = new oxmc.Common.Controls.OxPictureBox(this.components);
            this.lblChangePW = new System.Windows.Forms.Label();
            this.pbChangePW = new oxmc.Common.Controls.OxPictureBox(this.components);
            this.lblLogin = new System.Windows.Forms.Label();
            this.pbLogin = new oxmc.Common.Controls.OxPictureBox(this.components);
            this.lblVersion = new System.Windows.Forms.Label();
            this.pnlClient.SuspendLayout();
            this.pnlUser.SuspendLayout();
            this.pnlOldPwd.SuspendLayout();
            this.pnlPwd.SuspendLayout();
            this.pnlPwdRepeat.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChangePW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogin)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPw2
            // 
            this.lblPw2.Location = new System.Drawing.Point(3, 9);
            this.lblPw2.Name = "lblPw2";
            this.lblPw2.Size = new System.Drawing.Size(181, 62);
            this.lblPw2.Text = "Kennwort wiederholen";
            this.lblPw2.Visible = false;
            // 
            // lblPwOld
            // 
            this.lblPwOld.Location = new System.Drawing.Point(3, 10);
            this.lblPwOld.Name = "lblPwOld";
            this.lblPwOld.Size = new System.Drawing.Size(181, 35);
            this.lblPwOld.Text = "Altes Kennwort";
            this.lblPwOld.Visible = false;
            // 
            // lblPw
            // 
            this.lblPw.Location = new System.Drawing.Point(3, 12);
            this.lblPw.Name = "lblPw";
            this.lblPw.Size = new System.Drawing.Size(178, 33);
            this.lblPw.Text = "Kennwort";
            // 
            // lblUser
            // 
            this.lblUser.Location = new System.Drawing.Point(3, 14);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(181, 31);
            this.lblUser.Text = "Benutzer";
            // 
            // lblMandt
            // 
            this.lblMandt.Location = new System.Drawing.Point(3, 13);
            this.lblMandt.Name = "lblMandt";
            this.lblMandt.Size = new System.Drawing.Size(178, 31);
            this.lblMandt.Text = "Mandant";
            // 
            // tbPw2
            // 
            this.tbPw2.Location = new System.Drawing.Point(203, 10);
            this.tbPw2.MaxLength = 20;
            this.tbPw2.Name = "tbPw2";
            this.tbPw2.PasswordChar = '*';
            this.tbPw2.Size = new System.Drawing.Size(263, 41);
            this.tbPw2.TabIndex = 5;
            this.tbPw2.Visible = false;
            this.tbPw2.GotFocus += new System.EventHandler(this.tbPw2_GotFocus);
            this.tbPw2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyDown);
            this.tbPw2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyUp);
            this.tbPw2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ucUser_KeyPress);
            this.tbPw2.LostFocus += new System.EventHandler(this.tbPw2_LostFocus);
            // 
            // tbPwOld
            // 
            this.tbPwOld.Location = new System.Drawing.Point(203, 6);
            this.tbPwOld.MaxLength = 20;
            this.tbPwOld.Name = "tbPwOld";
            this.tbPwOld.PasswordChar = '*';
            this.tbPwOld.Size = new System.Drawing.Size(263, 41);
            this.tbPwOld.TabIndex = 3;
            this.tbPwOld.Visible = false;
            this.tbPwOld.GotFocus += new System.EventHandler(this.tbPwOld_GotFocus);
            this.tbPwOld.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyDown);
            this.tbPwOld.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyUp);
            this.tbPwOld.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ucUser_KeyPress);
            this.tbPwOld.LostFocus += new System.EventHandler(this.tbPwOld_LostFocus);
            // 
            // tbPw
            // 
            this.tbPw.Location = new System.Drawing.Point(203, 6);
            this.tbPw.MaxLength = 20;
            this.tbPw.Name = "tbPw";
            this.tbPw.PasswordChar = '*';
            this.tbPw.Size = new System.Drawing.Size(263, 41);
            this.tbPw.TabIndex = 4;
            this.tbPw.GotFocus += new System.EventHandler(this.tbPw_GotFocus);
            this.tbPw.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyDown);
            this.tbPw.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyUp);
            this.tbPw.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ucUser_KeyPress);
            this.tbPw.LostFocus += new System.EventHandler(this.tbPw_LostFocus);
            // 
            // tbUserid
            // 
            this.tbUserid.Location = new System.Drawing.Point(203, 6);
            this.tbUserid.MaxLength = 12;
            this.tbUserid.Name = "tbUserid";
            this.tbUserid.Size = new System.Drawing.Size(263, 41);
            this.tbUserid.TabIndex = 2;
            this.tbUserid.GotFocus += new System.EventHandler(this.tbUserid_GotFocus);
            this.tbUserid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyDown);
            this.tbUserid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyUp);
            this.tbUserid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ucUser_KeyPress);
            this.tbUserid.LostFocus += new System.EventHandler(this.tbUserid_LostFocus);
            // 
            // tbMandt
            // 
            this.tbMandt.Location = new System.Drawing.Point(203, 3);
            this.tbMandt.MaxLength = 3;
            this.tbMandt.Name = "tbMandt";
            this.tbMandt.Size = new System.Drawing.Size(263, 41);
            this.tbMandt.TabIndex = 1;
            this.tbMandt.GotFocus += new System.EventHandler(this.tbMandt_GotFocus);
            this.tbMandt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyDown);
            this.tbMandt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyUp);
            this.tbMandt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ucUser_KeyPress);
            this.tbMandt.LostFocus += new System.EventHandler(this.tbMandt_LostFocus);
            // 
            // lblMessage
            // 
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMessage.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblMessage.Location = new System.Drawing.Point(0, 260);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(480, 74);
            this.lblMessage.Text = "label1";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlClient
            // 
            this.pnlClient.Controls.Add(this.tbMandt);
            this.pnlClient.Controls.Add(this.lblMandt);
            this.pnlClient.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlClient.Location = new System.Drawing.Point(0, 0);
            this.pnlClient.Name = "pnlClient";
            this.pnlClient.Size = new System.Drawing.Size(480, 47);
            // 
            // pnlUser
            // 
            this.pnlUser.AutoScrollMargin = new System.Drawing.Size(10, 10);
            this.pnlUser.Controls.Add(this.lblUser);
            this.pnlUser.Controls.Add(this.tbUserid);
            this.pnlUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUser.Location = new System.Drawing.Point(0, 47);
            this.pnlUser.Name = "pnlUser";
            this.pnlUser.Size = new System.Drawing.Size(480, 47);
            // 
            // pnlOldPwd
            // 
            this.pnlOldPwd.Controls.Add(this.lblPwOld);
            this.pnlOldPwd.Controls.Add(this.tbPwOld);
            this.pnlOldPwd.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlOldPwd.Location = new System.Drawing.Point(0, 94);
            this.pnlOldPwd.Name = "pnlOldPwd";
            this.pnlOldPwd.Size = new System.Drawing.Size(480, 47);
            // 
            // pnlPwd
            // 
            this.pnlPwd.Controls.Add(this.lblPw);
            this.pnlPwd.Controls.Add(this.tbPw);
            this.pnlPwd.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPwd.Location = new System.Drawing.Point(0, 141);
            this.pnlPwd.Name = "pnlPwd";
            this.pnlPwd.Size = new System.Drawing.Size(480, 47);
            // 
            // pnlPwdRepeat
            // 
            this.pnlPwdRepeat.Controls.Add(this.lblPw2);
            this.pnlPwdRepeat.Controls.Add(this.tbPw2);
            this.pnlPwdRepeat.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPwdRepeat.Location = new System.Drawing.Point(0, 188);
            this.pnlPwdRepeat.Name = "pnlPwdRepeat";
            this.pnlPwdRepeat.Size = new System.Drawing.Size(480, 72);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblClose);
            this.panel1.Controls.Add(this.pbClose);
            this.panel1.Controls.Add(this.lblReset);
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.lblChangePW);
            this.panel1.Controls.Add(this.pbChangePW);
            this.panel1.Controls.Add(this.lblLogin);
            this.panel1.Controls.Add(this.pbLogin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 334);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(480, 176);
            // 
            // lblClose
            // 
            this.lblClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblClose.Location = new System.Drawing.Point(363, 108);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(103, 33);
            this.lblClose.Text = "Beenden";
            // 
            // pbClose
            // 
            this.pbClose.Image = ((System.Drawing.Image)(resources.GetObject("pbClose.Image")));
            this.pbClose.Location = new System.Drawing.Point(387, 65);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(40, 40);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbClose.Click += new System.EventHandler(this.PbExitClick);
            // 
            // lblReset
            // 
            this.lblReset.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblReset.Location = new System.Drawing.Point(208, 133);
            this.lblReset.Name = "lblReset";
            this.lblReset.Size = new System.Drawing.Size(73, 33);
            this.lblReset.Text = "Reset";
            // 
            // btnReset
            // 
            this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
            this.btnReset.Location = new System.Drawing.Point(219, 92);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(40, 40);
            this.btnReset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnReset.Click += new System.EventHandler(this.IconClick);
            // 
            // lblChangePW
            // 
            this.lblChangePW.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblChangePW.Location = new System.Drawing.Point(24, 108);
            this.lblChangePW.Name = "lblChangePW";
            this.lblChangePW.Size = new System.Drawing.Size(107, 61);
            this.lblChangePW.Text = "Kennwort ändern";
            // 
            // pbChangePW
            // 
            this.pbChangePW.Image = ((System.Drawing.Image)(resources.GetObject("pbChangePW.Image")));
            this.pbChangePW.Location = new System.Drawing.Point(52, 65);
            this.pbChangePW.Name = "pbChangePW";
            this.pbChangePW.Size = new System.Drawing.Size(40, 40);
            this.pbChangePW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbChangePW.Click += new System.EventHandler(this.IconClick);
            // 
            // lblLogin
            // 
            this.lblLogin.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblLogin.Location = new System.Drawing.Point(191, 65);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(127, 40);
            this.lblLogin.Text = "Anmelden";
            // 
            // pbLogin
            // 
            this.pbLogin.Image = ((System.Drawing.Image)(resources.GetObject("pbLogin.Image")));
            this.pbLogin.Location = new System.Drawing.Point(213, 12);
            this.pbLogin.Name = "pbLogin";
            this.pbLogin.Size = new System.Drawing.Size(50, 50);
            this.pbLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogin.Click += new System.EventHandler(this.IconClick);
            // 
            // lblVersion
            // 
            this.lblVersion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVersion.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblVersion.Location = new System.Drawing.Point(0, 537);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(480, 23);
            this.lblVersion.Text = "Version";
            // 
            // ucUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.pnlPwdRepeat);
            this.Controls.Add(this.pnlPwd);
            this.Controls.Add(this.pnlOldPwd);
            this.Controls.Add(this.pnlUser);
            this.Controls.Add(this.pnlClient);
            this.Name = "ucUser";
            this.Size = new System.Drawing.Size(480, 560);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ucUser_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ucUser_KeyUp);
            this.pnlClient.ResumeLayout(false);
            this.pnlUser.ResumeLayout(false);
            this.pnlOldPwd.ResumeLayout(false);
            this.pnlPwd.ResumeLayout(false);
            this.pnlPwdRepeat.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChangePW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPw2;
        private System.Windows.Forms.Label lblPwOld;
        private System.Windows.Forms.Label lblPw;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblMandt;
        public oxmc.Common.Controls.OxTextBox tbPw2;
        public oxmc.Common.Controls.OxTextBox tbPwOld;
        public oxmc.Common.Controls.OxTextBox tbPw;
        public oxmc.Common.Controls.OxTextBox tbUserid;
        public oxmc.Common.Controls.OxTextBox tbMandt;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Panel pnlClient;
        private System.Windows.Forms.Panel pnlUser;
        private System.Windows.Forms.Panel pnlOldPwd;
        private System.Windows.Forms.Panel pnlPwd;
        private System.Windows.Forms.Panel pnlPwdRepeat;
        private Microsoft.WindowsCE.Forms.InputPanel inputPanel1;
        private System.Windows.Forms.Panel panel1;
        private oxmc.Common.Controls.OxPictureBox pbLogin;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblChangePW;
        private oxmc.Common.Controls.OxPictureBox pbChangePW;
        private oxmc.Common.Controls.OxPictureBox btnReset;
        private System.Windows.Forms.Label lblReset;
        private oxmc.Common.Controls.OxPictureBox pbClose;
        private System.Windows.Forms.Label lblClose;
        private System.Windows.Forms.Label lblVersion;
    }
}
