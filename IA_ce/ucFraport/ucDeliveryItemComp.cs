﻿using System;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucDeliveryItemComp : OxUserControlCe
    {
        //private List<Delivery> allDeliv;

        public ucDeliveryItemComp(Delivery deliv)
        {
            try
            {
                InitializeComponent();
                lblRoom.Text = deliv.Raum;
                lblAddressee.Text = deliv.Empf;
                lblQuantity.Text = deliv.Menge;
                lblUnit.Text = deliv.Meins;
                //if (("Z").Equals(deliv.Status))
                //{
                //    lblStatus.Text = "Zugest.";
                //    lblCause.Text = "";
                //} else 
                //{
                //    lblStatus.Text = "Fehlg.";
                //    lblCause.Text = deliv.Gtext;
                //}
                switch (deliv.Status)
                {
                    case "Z":
                        lblStatus.Text = "Zugest.";
                        lblCause.Text = "";
                        break;
                    case "A":
                        lblStatus.Text = "Abgeh.";
                        lblCause.Text = "";
                        break;
                    case "F":
                        lblStatus.Text = "Fehlg.";
                        lblCause.Text = deliv.Gtext;
                        break;
                    default:
                        lblStatus.Text = "Fehlg.";
                        lblCause.Text = deliv.Gtext;
                        break;
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}
