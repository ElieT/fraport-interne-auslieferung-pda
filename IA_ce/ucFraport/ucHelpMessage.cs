﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using oxmc.IA_ce;

namespace IA_ce.ucFraport
{
    public partial class ucHelpMessage : OxUserControlCe
    {
        private static ucHelpMessage _instance;

        private ucHelpMessage()
        {
            InitializeComponent();

            // *** BEGIN CHANGE 2014-03-12 ***
            if (Screen.PrimaryScreen.Bounds.Height == 320)
            {
                //AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                //AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                //Size = new System.Drawing.Size(240, 268);

                label1.Width = 240; 
                label2.Width = 240;

            }
            else
            {
                AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
                AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                Size = new System.Drawing.Size(480, 536);

                label1.Width = 480;
                label2.Width = 480;
            }
            // *** END CHANGE 2014-03-12 ***
        }

        public static ucHelpMessage GetInstance()
        {
            if (_instance == null || _instance.IsDisposed)
                _instance = new ucHelpMessage();
            return _instance;
        }

        public void ShowMessage()
        {
            Visible = true;
            label1.Text = "Supportdatei wird erstellt und übertragen.";
            ShowThisControl(FormMain.Instance);
            BringToFront();
        }

        public void SetProgress(int progress)
        {
            label2.Text = "Fortschritt: " + progress + "%";
        }

        public void HideMessage()
        {
            Hide();
            Visible = false;
            FormMain.Instance.Controls.Remove(this);
            Dispose();
        }
    }
}
