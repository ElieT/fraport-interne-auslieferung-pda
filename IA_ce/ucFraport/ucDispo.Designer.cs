﻿namespace oxmc.IA_ce.ucFraport
{
    partial class ucDispo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDispo));
            this.pbHome = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnItems = new System.Windows.Forms.Panel();
            this.tbItemNr = new System.Windows.Forms.TextBox();
            this.pbSAve = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // pbHome
            // 
            this.pbHome.Image = ((System.Drawing.Image)(resources.GetObject("pbHome.Image")));
            this.pbHome.Location = new System.Drawing.Point(2, 2);
            this.pbHome.Name = "pbHome";
            this.pbHome.Size = new System.Drawing.Size(39, 18);
            this.pbHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHome.Click += new System.EventHandler(this.pbHome_Clicked);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Location = new System.Drawing.Point(50, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(164, 15);
            this.lblTitle.Text = "Transportdisposition";
            // 
            // pnItems
            // 
            this.pnItems.AutoScroll = true;
            this.pnItems.Location = new System.Drawing.Point(0, 50);
            this.pnItems.Name = "pnItems";
            this.pnItems.Size = new System.Drawing.Size(237, 217);
            // 
            // tbItemNr
            // 
            this.tbItemNr.Location = new System.Drawing.Point(2, 23);
            this.tbItemNr.Name = "tbItemNr";
            this.tbItemNr.Size = new System.Drawing.Size(205, 21);
            this.tbItemNr.TabIndex = 3;
            this.tbItemNr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddScannedItem);
            // 
            // pbSAve
            // 
            this.pbSAve.Image = ((System.Drawing.Image)(resources.GetObject("pbSAve.Image")));
            this.pbSAve.Location = new System.Drawing.Point(196, 2);
            this.pbSAve.Name = "pbSAve";
            this.pbSAve.Size = new System.Drawing.Size(38, 18);
            this.pbSAve.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSAve.Click += new System.EventHandler(this.CallService);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(213, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 21);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.Click += new System.EventHandler(this.AddItemByClick);
            // 
            // ucDispo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbSAve);
            this.Controls.Add(this.tbItemNr);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.pbHome);
            this.Controls.Add(this.pnItems);
            this.Name = "ucDispo";
            this.Size = new System.Drawing.Size(237, 267);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbHome;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnItems;
        private System.Windows.Forms.TextBox tbItemNr;
        private System.Windows.Forms.PictureBox pbSAve;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
