﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using IA_ce;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using System.Drawing;
//using oxmc.IA_ce.ucCommon;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucBuildList : OxUserControlCe
    {
        private ucDeliveryItemList _ucDeliveryItemList;
        private static ucBuildList _instance;
        private List<Delivery> allDelivs;
        
        private ucBuildList()
        {
            InitializeComponent();
        }

        public List<Delivery> AllDelivs
        {
            get { return allDelivs; }
            set { allDelivs = value; }
        }

        public static ucBuildList GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ucBuildList();
            }
            // _instance.RefreshData();
            return _instance;
        }

        public void RefreshData()
        {
            try
            {
                int top = 0;
                // Löschen der Einträge
                foreach (Control item in pnBuildItems.Controls)
                {
                    item.Visible = false;
                    item.Dispose();
                }
                pnBuildItems.Controls.Clear();

                AllDelivs = null;
                AllDelivs = DeliveryManager.GetAllDeliverys();

                FileManager.LogMessage("Count AllDelivs: " + AllDelivs.Count);

                if (AllDelivs.Count > 0)
                {
                    List<string> allBuildings = DeliveryManager.GetAllBuildings();
                    // Gehe durch Liste aller existierender Gebäude
                    foreach (string building in allBuildings)
                    {

                        List<Delivery> allDelivsInBuilding = new List<Delivery>();

                        // Schaue für jede Delivery,...
                        foreach (Delivery deliv in AllDelivs)
                        {
                            // ... ob sie das aktuelle Gebäude betrifft

                            // *** BEGIN CHANGE 07.11.2014
                            //if (building.Equals(deliv.Gebnr))
                            if (building.Equals(deliv.Gebnr)&& !deliv.IsPickUp.Equals("X"))
                            {
                                // Hänge Delivery an Liste an
                                allDelivsInBuilding.Add(deliv);
                            }
                        }

                        // Gebäude, zu denen es keine offenen 
                        int count = allDelivsInBuilding.Count;
                        int sumDelivered = 0;
                        // get number of deliverys which have not been finished
                        foreach (Delivery deliv in allDelivsInBuilding)
                        {
                            // if ("Z".Equals(deliv.Status))
                            if (!String.IsNullOrEmpty(deliv.MobileKey))
                            {
                                sumDelivered = sumDelivered + 1;
                            }
                        }

                        if (count > sumDelivered)
                        {
                            // Für jedes Gebäude wird ein ucBuildListItem angelegt
                            ucBuildListItem ucBuildListItem = new ucBuildListItem(allDelivsInBuilding);
                            ucBuildListItem.ClickEvent += ClickEvent;
                            ucBuildListItem.Top = top*ucBuildListItem.Height + 2;
                            if (top%2 == 0)
                                ucBuildListItem.BackColor = Color.LightGray;
                            pnBuildItems.Controls.Add(ucBuildListItem);
                            top++;
                        } 
                        else
                        {
                            FileManager.LogMessage("Gebäude " + building + " abgeschlossen.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void ClickEvent(object sender, EventArgs e)
        {
            Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            try
            {
                ucBuildListItem _ucBuildListItem = null;
                PictureBox _pictureBox = new PictureBox();
                Label _label1 = new Label();


                if (sender.GetType().Name.Equals("PictureBox"))
                {
                    _pictureBox = (PictureBox)sender;
                    if (_pictureBox.Parent.GetType().Name.Equals("ucBuildListItem"))
                    {
                        _ucBuildListItem = (ucBuildListItem)_pictureBox.Parent;
                        String gebnr = _ucBuildListItem.GetGebnr();

                        if (!_ucBuildListItem.allItemsDelivered())
                        {

                            if (_ucDeliveryItemList == null)
                            {
                                _ucDeliveryItemList = ucDeliveryItemList.GetInstance();
                                _ucDeliveryItemList.ChangeUserControl += ChangeUserControl;
                            }
                            _ucDeliveryItemList.RefreshData(gebnr);
                            //_ucDeliveryItemList.ShowThisControl(0, 0, this);
                            NavigationManager.NavigateForward(_ucDeliveryItemList);
                        } else
                        {
                            OxStatus.GetInstance().TriggerEventMessage(0, 'E', DateTime.Now, "Zum Gebäude existieren keine offenen Auslieferungen!", null, true, 1000);
                            Cursor.Current = System.Windows.Forms.Cursors.Default;
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            Cursor.Current = System.Windows.Forms.Cursors.Default;
        }

        private void pbHome_Clicked (Object sender, EventArgs e)
        {
            Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            this.Visible = false;
            NavigationManager.NavigateHome();
            Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }
}
