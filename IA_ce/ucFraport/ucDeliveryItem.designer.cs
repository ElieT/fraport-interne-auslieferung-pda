﻿namespace oxmc.IA_ce.ucFraport
{
    partial class ucDeliveryItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRoom = new System.Windows.Forms.Label();
            this.lblAddressee = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblUnit = new System.Windows.Forms.Label();
            this.rbDelivered = new System.Windows.Forms.RadioButton();
            this.rbFailed = new System.Windows.Forms.RadioButton();
            this.cbCause = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblRoom
            // 
            this.lblRoom.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblRoom.Location = new System.Drawing.Point(5, 4);
            this.lblRoom.Name = "lblRoom";
            this.lblRoom.Size = new System.Drawing.Size(67, 15);
            this.lblRoom.Text = "lblRoom";
            // 
            // lblAddressee
            // 
            this.lblAddressee.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblAddressee.Location = new System.Drawing.Point(75, 4);
            this.lblAddressee.Name = "lblAddressee";
            this.lblAddressee.Size = new System.Drawing.Size(102, 15);
            this.lblAddressee.Text = "lblAddressee";
            // 
            // lblQuantity
            // 
            this.lblQuantity.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblQuantity.Location = new System.Drawing.Point(180, 4);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(29, 15);
            this.lblQuantity.Text = "lblQuantity";
            this.lblQuantity.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblUnit
            // 
            this.lblUnit.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblUnit.Location = new System.Drawing.Point(212, 4);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(19, 15);
            this.lblUnit.Text = "lblUnit";
            // 
            // rbDelivered
            // 
            this.rbDelivered.Location = new System.Drawing.Point(12, 25);
            this.rbDelivered.Name = "rbDelivered";
            this.rbDelivered.Size = new System.Drawing.Size(15, 15);
            this.rbDelivered.TabIndex = 5;
            this.rbDelivered.Click += new System.EventHandler(this.cbBoxes_clicked);
            this.rbDelivered.CheckedChanged += new System.EventHandler(this.cbBoxes_clicked);
            // 
            // rbFailed
            // 
            this.rbFailed.Location = new System.Drawing.Point(45, 25);
            this.rbFailed.Name = "rbFailed";
            this.rbFailed.Size = new System.Drawing.Size(15, 15);
            this.rbFailed.TabIndex = 6;
            this.rbFailed.Click += new System.EventHandler(this.cbBoxes_clicked);
            this.rbFailed.CheckedChanged += new System.EventHandler(this.cbBoxes_clicked);
            // 
            // cbCause
            // 
            this.cbCause.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.cbCause.Location = new System.Drawing.Point(75, 23);
            this.cbCause.Name = "cbCause";
            this.cbCause.Size = new System.Drawing.Size(155, 20);
            this.cbCause.TabIndex = 7;
            this.cbCause.SelectedIndexChanged += new System.EventHandler(this.cbCause_selectedValueChanged);
            // 
            // ucDeliveryItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.cbCause);
            this.Controls.Add(this.rbFailed);
            this.Controls.Add(this.rbDelivered);
            this.Controls.Add(this.lblUnit);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.lblAddressee);
            this.Controls.Add(this.lblRoom);
            this.Name = "ucDeliveryItem";
            this.Size = new System.Drawing.Size(237, 45);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblRoom;
        private System.Windows.Forms.Label lblAddressee;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.RadioButton rbDelivered;
        private System.Windows.Forms.RadioButton rbFailed;
        private System.Windows.Forms.ComboBox cbCause;
    }
}
