﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.BusinessLogic.com.ox.RFID;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;
using oxmc.Common.Controls;
using oxmc.IA_ce;
using oxmc.IA_ce.ucFraport;

namespace oxmc_ce.IA_ce.ucFraport
{
    public partial class ucUser : OxUserControlCe
    {
        private Color color3 = Color.FromArgb(AppConfig.Color3[0], AppConfig.Color3[1], AppConfig.Color3[2]);

        const string PingString = "ping";
        private static ucUser instance;
        public EventHandler UserLogon;
        private Boolean _rfidEnabled = false;
        private OxStatus _oxStatus;
        private SyncManager _syncManager;
        private bool _isShiftDown;
        private bool _isCtrlDown;
        private bool _isT;

        private ucUser()
        {
            try
            {
                InitializeComponent();
                tbUserid.Text = AppConfig.UserId;
                tbMandt.Text = AppConfig.Mandt;
                //tbPw.Text = AppConfig.Pw;
                BackColor = color3;
                lblMessage.Text = "";
                lblPw2.Visible = false;
                tbPw2.Visible = false;
                lblPwOld.Visible = false;
                tbPwOld.Visible = false;
                pnlPwdRepeat.Visible = false;
                pnlOldPwd.Visible = false;

                ResetElements(LogonMode.Mode.CLogon);

                lblVersion.Text = "Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

                // RefreshData();

                _rfidEnabled = AppConfig.RfidEnabled;
                _oxStatus = OxStatus.GetInstance();
                _syncManager = SyncManager.GetInstance(); 
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static ucUser GetInstance()
        {
            if (instance == null || instance.IsDisposed)
                instance = new ucUser();
            return instance;
        }

        private void AutoLogon(object sender, EventArgs e)
        {
            if (AppConfig.FlagAutoLogon.Equals("X"))
                Logon(LogonMode.Mode.CLogon);
        }

        //private void BtNewUser_Click(object sender, EventArgs e)
        //{
        //    tbPw2.Visible = true;
        //    lblPw2.Visible = true;
        //    btPwReset.Enabled = false;
        //    pnlPwdRepeat.Visible = true;
        //}

        public void Logon(LogonMode.Mode mode)
        {
            try
            {
                var errormessage = "";

                OxStatus.GetInstance().SapLocnt = "0";
                tbUserid.Text = tbUserid.Text.Trim();
                tbMandt.Text = tbMandt.Text.Trim();
                tbPw.Text = tbPw.Text.Trim();
                tbPw2.Text = tbPw2.Text.Trim();
                tbPwOld.Text = tbPwOld.Text.Trim();
                OxDbManager.GetInstance().CommitTransaction();
                SyncManager.ClearInstance();
                OxDbManager.ClearInstance();
                Cursor.Current = Cursors.WaitCursor;
                ResetElements(mode);
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
                Byte[] hashedBytes;
                UTF8Encoding encoder = new UTF8Encoding();
                if (string.IsNullOrEmpty(tbPw.Text))
                {
                    lblMessage.Text = "Kennwort darf nicht leer sein";
                    OxStatus.GetInstance().TriggerEventMessage(2, 'E', DateTime.Now, "Kennwort darf nicht leer sein.", null, true, AppConfig.MessageTimeout);
                    return;
                }
                hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(tbPw.Text));
                Boolean validUser = false;
                Boolean userExists = UserManager.UserExists(tbMandt.Text, tbUserid.Text);
                if (mode.Equals(LogonMode.Mode.CPwChange)) // Change PW of an existing user
                {
                    AppConfig.UserId = tbUserid.Text;
                    AppConfig.Mandt = tbMandt.Text;
                    AppConfig.Pw = tbPw.Text;
                    String sapSuccessfull = "";
                    String sapMessage = "";
                    String sapUser = "";
                    String sapPing = "";
                    String message = _syncManager.ChangePw(tbUserid.Text, tbPwOld.Text, tbPw.Text, tbPw2.Text, PingString, true);
                    if (message.Split('|').Length > 0 && !string.IsNullOrEmpty(message.Split('|')[0]))
                    {
                        sapSuccessfull = message.Split('|')[0].Trim();
                    }
                    if (message.Split('|').Length > 1 && !string.IsNullOrEmpty(message.Split('|')[1]))
                    {
                        sapMessage = message.Split('|')[1].Trim();
                    }
                    if (message.Split('|').Length > 2 && !string.IsNullOrEmpty(message.Split('|')[2]))
                    {
                        sapUser = message.Split('|')[2].Trim();
                    }
                    if (message.Split('|').Length > 3 && !string.IsNullOrEmpty(message.Split('|')[3]))
                    {
                        sapPing = message.Split('|')[3].Trim();
                    }
                    if (sapPing.IndexOf(PingString) != -1 && sapSuccessfull.Equals("X"))
                    {
                        validUser = UserManager.CheckUser(tbMandt.Text, tbUserid.Text,
                                                          md5Hasher.ComputeHash(encoder.GetBytes(tbPwOld.Text)));
                        if (validUser)
                        {
                            validUser = UserManager.ChangePW(tbMandt.Text, tbUserid.Text, hashedBytes);
                            if (validUser)
                            {
                                AppConfig.UserId = tbUserid.Text.ToLower();
                                AppConfig.Mandt = tbMandt.Text;
                                AppConfig.Pw = tbPw.Text;
                                User.userName = tbUserid.Text; // Wird später mit dem Namen überschrieben
                                lblMessage.Text = "";
                                // Generate Device
                                //OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
                                // Collect all db table names relevant for delta changes// Generate Device
                                //OxDbManager.GetInstance().CreateDeviceGuid(true, AppConfig.Mandt, AppConfig.UserId);
                                OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
                                //OxDbManager.GetInstance().CreateDeltaTableContent(true, false);
                                OxDbManager.GetInstance().CreateDeltaTableContent();

                                OnUserLogon(new EventArgs());
                                OxDbManager.GetInstance().CommitTransaction();
                                //StartApplication();

                                OxStatus.GetInstance().TriggerEventMessage(2, 'S', DateTime.Now, "Passwort erfolgreich geändert.", null, true, AppConfig.MessageTimeout);

                                Visible = false;
                                ucHome.GetInstance().RefreshData();
                                //_ucHome.IsNewUser = true;
                                ucHome.GetInstance().ShowThisControl(0, 0, FormMain.Instance);
                                ucHome.GetInstance().SVMCheck();
                                GetInstance().Parent.Controls.Remove(GetInstance());
                                //if (OxStatus.GetInstance().SapAvailable)
                                //    BGSyncManager.GetInstance().StartBackgroundSync();
                                //NavigationManager.NavigateForward(_ucHome);
                            }
                            else
                            {
                                lblMessage.Text = "Passwort konnte nicht geändert werden.";
                            }
                        }
                        else
                        {
                            validUser = UserManager.CreateUser(tbMandt.Text, tbUserid.Text,
                                                               md5Hasher.ComputeHash(encoder.GetBytes(tbPw.Text)));
                            if (validUser)
                            {
                                UserManager.UpdateUser(tbMandt.Text, tbMandt.Text);
                                AppConfig.UserId = tbUserid.Text.ToLower();
                                AppConfig.Mandt = tbMandt.Text;
                                AppConfig.Pw = tbPw.Text;
                                User.userName = tbUserid.Text.ToLower(); // Wird später mit dem Namen überschrieben
                                lblMessage.Text = "";
                                // Generate Device
                                // OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
                                // Collect all db table names relevant for delta changes// Generate Device
                                //OxDbManager.GetInstance().CreateDeviceGuid(true, AppConfig.Mandt, AppConfig.UserId);
                                OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
                                //OxDbManager.GetInstance().CreateDeltaTableContent(true, false);
                                OxDbManager.GetInstance().CreateDeltaTableContent();

                                OnUserLogon(new EventArgs());
                                //StartApplication();
                                Visible = false;

                                ucHome.GetInstance().RefreshData();
                                //_ucHome.IsNewUser = true;
                                ucHome.GetInstance().ShowThisControl(0, 0, FormMain.Instance);
                                ucHome.GetInstance().SVMCheck();
                                GetInstance().Parent.Controls.Remove(GetInstance());
                                OxDbManager.GetInstance().CommitTransaction();
                                //if (OxStatus.GetInstance().SapAvailable)
                                //    BGSyncManager.GetInstance().StartBackgroundSync();
                                //NavigationManager.NavigateForward(_ucHome);

                                //Dispose();
                            }
                            else
                            {
                                lblMessage.Text = "Benutzer konnte nicht angelegt werden.";
                            }
                        }
                    }
                    else if (sapPing.IndexOf(PingString) != -1 && !sapSuccessfull.Equals("X"))
                    {
                        lblMessage.Text = "Fehler: " + sapMessage;
                    }
                    else
                    {
                        lblMessage.Text = "Kennwortänderungen sind nur im Online Modus möglich.";
                    }
                }
                else if (mode.Equals(LogonMode.Mode.CNewUser) || mode.Equals(LogonMode.Mode.CPwReset)) // Create a new local user or reset user
                {
                    /*
                    if (!tbPw.Text.Equals(tbPw2.Text) && !mode.Equals(LogonMode.Mode.CPwReset))
                    {
                        lblMessage.Text = "Die Kennwörter stimmen nicht überein.";
                    }
                    else if (tbPw2.Text.Length < 6 && !mode.Equals(LogonMode.Mode.CPwReset))
                    {
                        lblMessage.Text = "Die Länge des Passworts ist nicht ausreichend.";
                    }
                     * else if
                     */
                    if (tbUserid.Text.Length == 0 && !mode.Equals(LogonMode.Mode.CPwReset))
                    {
                        lblMessage.Text = "Bitte Benutzernamen angeben.";
                    }
                    else if (tbMandt.Text.Length != 3 && !mode.Equals(LogonMode.Mode.CPwReset))
                    {
                        lblMessage.Text = "Bitte Mandant angeben.";
                    }
                    else
                    {
                        Boolean pwChange, pwReset, allowLogin;
                        String valid_User = "";
                        AppConfig.UserId = tbUserid.Text.ToLower();
                        AppConfig.Mandt = tbMandt.Text;
                        AppConfig.Pw = tbPw.Text;
                        NameValueCollection parameter = new NameValueCollection();
                        parameter.Add("pa_ping", PingString);
                        parameter.Add("userid", AppConfig.UserId);
                        parameter.Add("pw", AppConfig.Pw);
                        _oxStatus.SapLocnt = "0";
                        //_oxStatus.SapUserDetails = _syncManager.GetUserName(SyncManager.GetInstance().DefaultGatewayConfig, parameter, "user_details.htm", true, false);
                        _oxStatus.SapUserDetails = _syncManager.GetUserName(parameter, "user_details.htm", true);

                        //_oxStatus.SapUserDetails = _syncManager.GetUserName(null, null, PingString, AppConfig.UserId, AppConfig.Pw, true);
                        if (_oxStatus.SapUserDetails.IndexOf(PingString) != -1)
                        {
                            _oxStatus.SapAvailable = true;
                        }
                        else
                        {
                            _oxStatus.SapAvailable = false;
                        }
                        if (_oxStatus.SapAvailable)
                        {
                            valid_User = _oxStatus.ValidateUser(out pwChange, out pwReset, out allowLogin);
                            if (mode.Equals(LogonMode.Mode.CPwReset) && allowLogin)
                            {
                                if (UserManager.ResetUser(tbMandt.Text, tbUserid.Text))
                                {
                                    lblMessage.Text =
                                        "Benutzerkonto wurde auf diesem Gerät zurückgesetzt. Es wird versucht ein neues Konto anzulegen.";
                                    validUser = UserManager.CreateUser(tbMandt.Text, tbUserid.Text, hashedBytes);
                                    if (validUser)
                                    {
                                        AppConfig.UserId = tbUserid.Text.ToLower();
                                        AppConfig.Mandt = tbMandt.Text;
                                        AppConfig.Pw = tbPw.Text;
                                        User.userName = tbUserid.Text; // Wird später mit dem Namen überschrieben
                                        lblMessage.Text = "";
                                        /*
                                        DBManager.ClearData();
                                        DBManager.ClearColumnOrder();
                                        DBManager.CreateColumnOrder();
                                        */
                                        _oxStatus.TriggerEventMessage(0, 'S', DateTime.Now, "Kennwort erfolgreich zurückgesetzt", null, true, 1000);// Generate Device
                                        //OxDbManager.GetInstance().CreateDeviceGuid(true, AppConfig.Mandt, AppConfig.UserId);
                                        OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);

                                        //OxDbManager.GetInstance().CreateDeltaTableContent(true, false);
                                        OxDbManager.GetInstance().CreateDeltaTableContent();

                                    }
                                    else
                                    {
                                        lblMessage.Text = "Benutzer konnte nicht angelegt werden (bereits lokal vorhanden?).";
                                        _oxStatus.TriggerEventMessage(0, 'E', DateTime.Now, lblMessage.Text, null, true, 10);
                                    }
                                }
                                else
                                {
                                    lblMessage.Text =
                                        "Benutzerkonto konnte nicht zurückgesetzt werden.";
                                    ResetElements(LogonMode.Mode.CLogon);
                                }
                            }


                            if (!String.IsNullOrEmpty(valid_User))
                            {
                                lblMessage.Text = valid_User;
                                if (pwReset && userExists)
                                    Logon(LogonMode.Mode.CPwReset);
                                else if (pwReset && !userExists)
                                {
                                    tbPwOld.Text = tbPw.Text;
                                    tbPw.Text = "";
                                    tbPw2.Text = "";
                                    ResetElements(LogonMode.Mode.CPwChange);
                                }
                                else if (pwChange)
                                {
                                    tbPwOld.Text = tbPw.Text;
                                    tbPw.Text = "";
                                    tbPw2.Text = "";
                                    ResetElements(LogonMode.Mode.CPwChange);
                                }
                                return;
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Keine Online-Verbindung verfügbar. Für diese Aktion muss eine Verbindung zum SAP System bestehen.";
                            if (!String.IsNullOrEmpty(_oxStatus.SapUserDetails))
                                lblMessage.Text += _oxStatus.SapUserDetails;
                            _oxStatus.TriggerEventMessage(0, 'E', DateTime.Now, lblMessage.Text, null, true, 10);
                            return;
                        }

                        if (userExists && !mode.Equals(LogonMode.Mode.CPwReset))
                        {
                            lblMessage.Text = "Ein Benutzerkonto unter diesem Namen ist bereits lokal vorhanden.";
                            _oxStatus.TriggerEventMessage(0, 'E', DateTime.Now, lblMessage.Text, null, true, 10);
                            return;
                        }
                        validUser = UserManager.CreateUser(tbMandt.Text, tbUserid.Text, hashedBytes);
                        if (validUser)
                        {
                            AppConfig.UserId = tbUserid.Text.ToLower();
                            AppConfig.Mandt = tbMandt.Text;
                            AppConfig.Pw = tbPw.Text;
                            User.userName = tbUserid.Text; // Wird später mit dem Namen überschrieben
                            lblMessage.Text = "";
                            // Generate Device
                            //OxDbManager.GetInstance().CreateDeviceGuid(true, AppConfig.Mandt, AppConfig.UserId);
                            OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);

                            //OxDbManager.GetInstance().CreateDeltaTableContent(true, false);
                            OxDbManager.GetInstance().CreateDeltaTableContent();

                            OnUserLogon(new EventArgs());
                            tbUserid.Text = "";
                            tbPw.Text = "";
                            //StartApplication();
                            ucHome.GetInstance().RefreshData();
                            ucHome.GetInstance().ShowThisControl(0, 0, FormMain.Instance);
                            ucHome.GetInstance().SVMCheck();
                            GetInstance().Parent.Controls.Remove(GetInstance());
                            OxDbManager.GetInstance().CommitTransaction();
                            //if (OxStatus.GetInstance().SapAvailable)
                            //    BGSyncManager.GetInstance().StartBackgroundSync();
                        }
                        else
                        {
                            lblMessage.Text = "Benutzer konnte nicht angelegt werden (bereits lokal vorhanden?).";
                            _oxStatus.TriggerEventMessage(0, 'E', DateTime.Now, lblMessage.Text, null, true, 10);
                        }
                    }
                }
                else if (mode.Equals(LogonMode.Mode.CLogon))              // Logon to an existing user account
                {
                    validUser = UserManager.CheckUser(tbMandt.Text, tbUserid.Text, hashedBytes);
                    if (validUser)
                    {
                        UserManager.UpdateUser(tbMandt.Text, tbUserid.Text);
                        AppConfig.UserId = tbUserid.Text.ToLower();
                        AppConfig.Mandt = tbMandt.Text;
                        AppConfig.Pw = tbPw.Text;
                        User.userName = tbUserid.Text; // Wird später mit dem Namen überschrieben

                        NameValueCollection parameter = new NameValueCollection();
                        parameter.Add("pa_ping", PingString);
                        parameter.Add("userid", AppConfig.UserId);
                        parameter.Add("pw", AppConfig.Pw);
                        _oxStatus.SapLocnt = "0";
                        //_oxStatus.SapUserDetails = _syncManager.GetUserName(SyncManager.GetInstance().DefaultGatewayConfig, parameter, "user_details.htm", true, false);
                        _oxStatus.SapUserDetails = _syncManager.GetUserName(parameter, "user_details.htm", true);

                        Boolean pwChange, pwReset, allowLogin;
                        String valid_User = _oxStatus.ValidateUser(out pwChange, out pwReset, out allowLogin);

                        //_oxStatus.SapUserDetails = _syncManager.GetUserName(null, null, PingString, AppConfig.UserId, AppConfig.Pw, true);
                        if (_oxStatus.SapUserDetails.IndexOf(PingString) != -1)
                        {
                            _oxStatus.SapAvailable = true;
                        }
                        else
                        {
                            _oxStatus.SapAvailable = false;
                        }

                        if (!allowLogin)
                        {
                            lblMessage.Text = valid_User;
                            return;
                        }

                        if (!pwChange && !pwReset)
                        {
                            lblMessage.Text = "";
                            lblMessage.Text = "";
                            // Generate Device
                            //OxDbManager.GetInstance().CreateDeviceGuid(true, AppConfig.Mandt, AppConfig.UserId);
                            //OxDbManager.GetInstance().CreateDeltaTableContent(true, false);
                            OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
                            OxDbManager.GetInstance().CreateDeltaTableContent();

                            OnUserLogon(new EventArgs());
                            //StartApplication();
                            ucHome.GetInstance().RefreshData();
                            ucHome.GetInstance().ShowThisControl(0, 0, FormMain.Instance);
                            ucHome.GetInstance().SVMCheck();
                            GetInstance().Parent.Controls.Remove(GetInstance());
                            OxDbManager.GetInstance().CommitTransaction();
                        }
                        else if (pwChange)
                        {
                            tbPwOld.Text = tbPw.Text;
                            tbPw.Text = "";
                            tbPw2.Text = "";
                            ResetElements(LogonMode.Mode.CPwChange);
                        }
                    }
                    else
                    {
                        if (userExists)
                        {
                            AppConfig.UserId = tbUserid.Text.ToLower();
                            AppConfig.Mandt = tbMandt.Text;
                            AppConfig.Pw = tbPw.Text;
                            User.userName = tbUserid.Text; // Wird später mit dem Namen überschrieben

                            NameValueCollection parameter = new NameValueCollection();
                            parameter.Add("pa_ping", PingString);
                            parameter.Add("userid", AppConfig.UserId);
                            parameter.Add("pw", AppConfig.Pw);
                            _oxStatus.SapLocnt = "0";
                            //_oxStatus.SapUserDetails =
                            //    _syncManager.GetUserName(SyncManager.GetInstance().DefaultGatewayConfig, parameter,
                            //                             "user_details.htm", true, false);
                            _oxStatus.SapUserDetails = _syncManager.GetUserName(parameter, "user_details.htm", true);

                            Boolean pwChange, pwReset, allowLogin;
                            if (string.IsNullOrEmpty(_oxStatus.SapUserDetails))
                                allowLogin = false;
                            else
                            {
                                errormessage = _oxStatus.ValidateUser(out pwChange, out pwReset, out allowLogin);
                            }

                            if (allowLogin)
                            {
                                Logon(LogonMode.Mode.CPwReset);
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(errormessage))
                                    lblMessage.Text = "Lokales Kennwort oder Benutzername falsch.";
                                else
                                    lblMessage.Text = errormessage;
                            }
                        }
                        else
                        {
                            lblMessage.Text =
                                "Benutzer nicht lokal angelegt. Es wird versucht den Benutzer anzulegen. Bitte verbinden Sie Ihr Gerät mit dem Netzwerk.";
                            Logon(LogonMode.Mode.CNewUser);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            Cursor.Current = Cursors.Default;
        }

        private void tbMandt_GotFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = true;
        }

        private void tbMandt_LostFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = false;
        }

        private void tbUserid_GotFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = true;
        }

        private void tbPwOld_GotFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = true;
        }

        private void tbPw_GotFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = true;
        }

        private void tbPw2_GotFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = true;
        }

        private void tbUserid_LostFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = false;
        }

        private void tbPwOld_LostFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = false;
        }

        private void tbPw_LostFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = false;
        }

        private void tbPw2_LostFocus(object sender, EventArgs e)
        {
            if (Visible)
                inputPanel1.Enabled = false;
        }
        protected void OnUserLogon(EventArgs e)
        {
            tbUserid.Text = "";
            tbPw.Text = "";
            tbPw2.Text = "";
            tbPwOld.Text = "";
            if (UserLogon != null)
            {
                UserLogon(this, e);
            }
        }

        /*private void BtnResetClick(object sender, EventArgs e)
        {
            tbPwOld.Text = "";
            tbPw2.Text = "";
            //btScan.Enabled = true;
            btPwReset.Enabled = true;
            btChangePW.Enabled = true;
            tbPwOld.Visible = false;
            tbPw2.Visible = false;
            pnlPwdRepeat.Visible = false;
            pnlOldPwd.Visible = false;
            tbPw.Focus();
        }*/

        private void PbExitClick(object sender, EventArgs e)
        {

            OxDbManager.GetInstance().CommitTransaction(); 
            Application.Exit();
        }


        private void IconClick(object sender, EventArgs e)
        {
            try
            {
                if (sender is OxPictureBox)
                //if (sender.GetType().Name.Equals("Button"))
                {
                    //Button pb = (Button)sender;

                    var pb = (OxPictureBox)sender;
                    switch (pb.Name)
                    {
                        case "pbLogin":
                            Logon(LogonMode.Mode.CLogon);
                            break;
                        case "pbChangePW":
                            if (tbPwOld.Visible == false)
                            {
                                ResetElements(LogonMode.Mode.CPwChange);
                                tbPwOld.Text = tbPw.Text;
                                tbPw.Text = "";
                                break;
                            }
                            Logon(LogonMode.Mode.CPwChange);
                            break;
                        case "pbPwReset":
                            Logon(LogonMode.Mode.CPwReset);
                            break;
                        //case "pbNewAccount":
                        //    Logon(LogonMode.Mode.CNewUser);
                        //    break;
                        case "pbReset":
                            ResetElements(LogonMode.Mode.CLogon);
                            break;
                        case "btnReset":
                            ResetElements(LogonMode.Mode.CLogon);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        private void ResetElements(LogonMode.Mode mode)
        {
            try
            {
                if (mode.Equals(LogonMode.Mode.CInit))
                {
                    tbUserid.Text = AppConfig.UserId;
                    tbMandt.Text = AppConfig.Mandt;
                    tbPw.Text = AppConfig.Pw;
                    lblMessage.Text = "";
                    lblPw2.Visible = false;
                    tbPw2.Visible = false;
                    lblPwOld.Visible = false;
                    tbPwOld.Visible = false;
                    pbChangePW.Visible = true;
                    btnReset.Visible = false;
                    lblReset.Visible = false;
                    pbLogin.Visible = true;
                    lblLogin.Visible = true;
                }
                else if (mode.Equals(LogonMode.Mode.CLogon))
                {
                    //BackColor = _color3;
                    lblMessage.Text = "";
                    lblPw2.Visible = false;
                    tbPw2.Visible = false;
                    lblPwOld.Visible = false;
                    tbPwOld.Visible = false;
                    //lblTitle.Text = "";
                    //pbNewAccount.Visible = true;
                    pbChangePW.Visible = true;
                    btnReset.Visible = false;
                    lblReset.Visible = false;
                    pbLogin.Visible = true;
                    lblLogin.Visible = true;
                    pnlPwdRepeat.Visible = false;
                    pnlOldPwd.Visible = false;
                }
                else if (mode.Equals(LogonMode.Mode.CPwChange))
                {
                    //tbPwOld.Text = tbPw.Text;
                    //tbPw.Text = "";
                    tbPw2.Visible = true;
                    lblPw2.Visible = true;
                    tbPwOld.Visible = true;
                    lblPwOld.Visible = true;
                    //pbNewAccount.Visible = false;
                    pbChangePW.Visible = true;
                    btnReset.Visible = true;
                    lblReset.Visible = true;
                    pbLogin.Visible = false;
                    lblLogin.Visible = false;
                    lblMessage.Text =
                        "Bitte geben Sie Ihr aktuelles Kennwort sowie das neue Kennwort zweimal ein. Verbinden Sie das Gerät mit dem Netzwerk.";
                    pnlPwdRepeat.Visible = true;
                    pnlOldPwd.Visible = true;
                }
                else if (mode.Equals(LogonMode.Mode.CPwReset))
                {
                    lblMessage.Text =
                                "Vorsicht! Benutzer wird zurückgesetzt. Bitte verbinden Sie Ihr Gerät und melden sich mit dem SAP Kennwort an.";
                    btnReset.Visible = true;
                    lblReset.Visible = true;
                    pbLogin.Visible = true;
                    lblLogin.Visible = true;
                }
                else if (mode.Equals(LogonMode.Mode.CNewUser))
                {
                    btnReset.Visible = false;
                    tbPw2.Visible = false;
                    lblPw2.Visible = false;
                    //pbNewAccount.Visible = true;
                    pbLogin.Visible = true;
                    lblLogin.Visible = true;
                    pnlPwdRepeat.Visible = true;

                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void ucUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_isShiftDown && _isCtrlDown && _isT)
            {
                //var rfidTest = new ucRfidTest();
                //rfidTest.ShowThisControl(0, 0, Parent);
            }
        }

        private void ucUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 16)
                _isShiftDown = true;
            if (e.KeyValue == 17)
                _isCtrlDown = true;
            if (e.KeyValue == 84)
                _isT = true;
        }

        private void ucUser_KeyUp(object sender, KeyEventArgs e)
        {
            _isShiftDown = false;
            _isCtrlDown = false;
            _isT = false;
        }
    }



    public class LogonMode
    {
        public enum Mode { CLogon, CNewUser, CPwChange, CPwReset, CInit };

        private Mode _mode = Mode.CLogon;

        public LogonMode(Mode mode)
        {
            _mode = mode;
        }

        /*public Mode CurrentLogonMode
        {
            get { return _mode; }
            set { _mode = value; }
        }*/
    }
}
