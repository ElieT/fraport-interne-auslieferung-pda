﻿using System;
//using System.Linq;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using IA_ce;
using IA_ce.ucFraport;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.BusinessLogic.com.ox.signature;
using oxmc.Common;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucDeliveryPdf  : OxUserControlCe
    {
        //public SignatureControl _signature = new SignatureControl("sign_1.png");
        //public SignatureControl _signature = new SignatureControl("sign_3.png");
        public SignatureControl _signature = new SignatureControl("sign_3_big.png");
        public SignatureControl _signature_mc75 = new SignatureControl("sign_3_big_mc75.png");
        private List<Delivery> changedDelivs;
        //private ucBuildList ucBuildList;

        public ucDeliveryPdf()
        {
            InitializeComponent();
            try
            {
                // *** BEGIN CHANGE 2013-08-26 ***
                // signature area is too small 
                //if (AppConfig.SVMClientOs.ToUpper().Equals("MC75"))
                if (Screen.PrimaryScreen.Bounds.Height != 320)
                {
                    areaSignature.Width = 458;
                    //areaSignature.Width = 360;
                    areaSignature.Height = 120;
                    _signature_mc75.Location = areaSignature.Location;
                    _signature_mc75.Size = areaSignature.Size;
                } 
                //else
                //{
                //    areaSignature.Width = 229;
                //    areaSignature.Height = 60;
                //    _signature.Location = areaSignature.Location;
                //    _signature.Size = areaSignature.Size;
                //}
                // *** END CHANGE 2013-08-26 ***

                _signature.Location = areaSignature.Location;
               
                _signature.Size = areaSignature.Size;
                //if (!AppConfig.SVMClientOs.ToUpper().Equals("MC75"))
                if (Screen.PrimaryScreen.Bounds.Height == 320)
                    this.Controls.Add(_signature);
                else
                    this.Controls.Add(_signature_mc75);

                displayKostst();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        
        public void RefreshData (List<Delivery> changedDelivs)
        {
            this.changedDelivs = changedDelivs;
            try
            {
                // clear List of shown deliverys
                foreach (Control item in pnDevItemList.Controls)
                {
                    item.Visible = false;
                    item.Dispose();
                }
                pnDevItemList.Controls.Clear();

                // clear signature
                _signature.Clear();
                _signature_mc75.Clear();

                int top = 0;
                foreach(Delivery deliv in changedDelivs)
                {
                    ucDeliveryItemComp ucDeliveryItemComp = new ucDeliveryItemComp(deliv);

                    // Default the costcenter field
                    if (!String.IsNullOrEmpty(deliv.Kostl))
                    {
                        tbKostSt.Text = deliv.Kostl;
                    }
                    //ucDeliveryItemComp.ClickEvent += ClickEvent;
                    ucDeliveryItemComp.Top = top * ucDeliveryItemComp.Height + 2;
                    if (top % 2 == 0)
                        ucDeliveryItemComp.BackColor = Color.LightGray;
                    pnDevItemList.Controls.Add(ucDeliveryItemComp);
                    top++;
                }

                displayKostst();

                //resizeSignature();

            } catch (Exception ex)
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Es ist ein Fehler aufgetreten.",
                                                                  null, true, 5000);
                FileManager.LogException(ex);
            }
        }

        // public EventHandler ClickEvent;
        public void btSave_click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (changedDelivs.Count > 0)
            {
                try
                {
                    //Cursor.Current = Cursors.WaitCursor;
                    String signFile = AppConfig.GetAppPath() + "\\Unterschrift.gif";
                    String pdfFile = AppConfig.GetAppPath() + "\\Lieferreport_" +
                                     DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".pdf";

                    String persNr = "";
                    if (!String.IsNullOrEmpty(tbPerNr.Text))
                        persNr = tbPerNr.Text;
                    else
                    {
                        OxStatus.GetInstance().TriggerEventMessage(2, 'E', DateTime.Now,
                                                                   "Geben Sie eine Personalnummer an.", null, true,
                                                                   AppConfig.MessageTimeout);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                    String kostst = "";
                    if (!String.IsNullOrEmpty(tbKostSt.Text) && tbKostSt.Visible)
                    {
                        kostst = tbKostSt.Text;
                        FileManager.LogMessage("Prüfung: Kostenstelle sichtbar");
                    }
                    else
                    {
                        if (changedDelivs != null && changedDelivs.Count > 0 && !changedDelivs[0].IsPickUp.ToUpper().Equals("X"))
                        {
                            OxStatus.GetInstance().TriggerEventMessage(2, 'E', DateTime.Now,
                                                                       "Geben Sie eine Kostenstelle an.", null, true,
                                                                       AppConfig.MessageTimeout);
                            Cursor.Current = Cursors.Default;
                            return;
                        }
                        else
                        {
                            kostst = "";
                            FileManager.LogMessage("Prüfung:  Kostenstelle nicht sichtbar.");
                        }
                    }

                    // *** BEGIN CHANGE 2015-06-30 ***
                    // check if signature exists
                    bool exists = checkDocSigned();
                    if (!exists)
                    {
                        OxStatus.GetInstance().TriggerEventMessage(0, 'E', DateTime.Now,
                                                               "Bitte geben Sie eine Unterschrift an.", null, true, 10);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                    // *** END CHANGE 2015-06-30 ***

                    foreach (var deliv in changedDelivs)
                    {
                        deliv.Kostl = kostst;
                        DeliveryManager.SaveDelivery(deliv);
                    }
                    OxStatus.GetInstance().TriggerEventMessage(0, 'S', DateTime.Now,
                                                               "Daten wurden erfolgreich gespeichert.", null, true, 10);

                    // Save Signature as a image
                    // *** BEGIN CHANGE 2015-01-23 ***
                    //if (!AppConfig.SVMClientOs.ToUpper().Equals("MC75"))
                    //    _signature.GetBitmap().Save(signFile, System.Drawing.Imaging.ImageFormat.Gif);
                    //else
                    //    _signature_mc75.GetBitmap().Save(signFile, System.Drawing.Imaging.ImageFormat.Gif);
                    //if (!AppConfig.SVMClientOs.ToUpper().Equals("MC75"))
                    if (Screen.PrimaryScreen.Bounds.Height == 320)
                        _signature.GetBitmap().Save(signFile, System.Drawing.Imaging.ImageFormat.Gif);
                    else
                        _signature_mc75.GetBitmap().Save(signFile, System.Drawing.Imaging.ImageFormat.Gif);
                    // *** END CHANGE 2015-01-23 ***

                    OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now,
                                                               "Die Unterschrift wurde erfolgreich gespeichert.",
                                                               null, true, 5000);

                    DocumentHelper.CreateDeliveryPdf(changedDelivs, persNr, kostst, pdfFile, signFile);

                    // Create Upload document
                    IA_Doch header = new IA_Doch();
                    header.Description = "Lieferbericht " + DateTime.Now.ToString("yyyy-MM-dd");
                    header.DocType = "MEB";
                    header.Email = "";
                    header.Filename = pdfFile;
                    header.RefType = "OR";
                    header.RefObject = changedDelivs[0].Gebnr;
                    header.Spras = "D";

                    var hashString = changedDelivs[0].Gebnr + AppConfig.UserId + AppConfig.Mandt +
                                     DateTime.Now.ToString("yyyyMMddHHmmffff");
                    var hash = OxCrypto.HashPasswordForStoringInConfigFile(hashString);
                    header.MobileKey = hash;

                    DeliveryManager.SetMobileKey(changedDelivs, hash);

                    if (!FileManager.Processing)
                    {
                        FileManager.IA_ReadBinFile(header);
                    }

                    FileManager.DeleteFile(signFile);

                    OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
                                                               "Das Dokument wurde erfolgreich gespeichert.",
                                                               null, true, 5000);

                    // Prüfen, ob Abholungen 
                    if (!changedDelivs[0].IsPickUp.Equals("X"))
                    {
                        ucBuildList.GetInstance().RefreshData();
                        NavigationManager.NavigateForward(ucBuildList.GetInstance());
                    }
                    else
                    {
                        ucHome.GetInstance().RefreshData();
                        NavigationManager.NavigateHome();
                        //ucPickUpList.getInstance().refreshData();
                        //NavigationManager.NavigateForward(ucPickUpList.getInstance());
                    }
                }
                catch (Exception ex)
                {
                    Cursor.Current = Cursors.Default;
                    FileManager.LogException(ex);
                }
                Cursor.Current = Cursors.Default;
            }
            else
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now, "Es wurden keine Lieferungen getätigt.",
                                                      null, true, 5000);
                return;
            }
        }

        private void pbClear_Click(object sender, EventArgs e)
        {
            _signature.Clear();
            _signature_mc75.Clear();
            _signature.Written = false;
            _signature_mc75.Written = false;
        }

        private void pbBack_Click (object sender, EventArgs e)
        {
            Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            NavigationManager.NavigateBack();
            Cursor.Current = System.Windows.Forms.Cursors.Default;
        }

        private void displayKostst ()
        {
            try
            {
                if (changedDelivs != null && changedDelivs.Count > 0)
                {
                    Delivery firstItem = changedDelivs[0];
                    if (firstItem.IsPickUp.ToUpper().Equals("X"))
                    {
                        tbKostSt.Visible = false;
                        label2.Visible = false;
                    } else
                    {
                        tbKostSt.Visible = true;
                        label2.Visible = true;
                    }
                }
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void resizeSignature ()
        {
            try
            {
                if (Screen.PrimaryScreen.Bounds.Height == 320)
                {
                    // AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                    // AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                    // Size = new System.Drawing.Size(240, 268);
                    //areaSignature.Width = 360;
                    //_signature.Location = areaSignature.Location;
                    //_signature.Size = areaSignature.Size;

                    int widthFullScreen = Screen.PrimaryScreen.Bounds.Width;
                    areaSignature.Width = widthFullScreen;
                    var size = new System.Drawing.Size(widthFullScreen, 60);
                    //_signature_mc75.Location = areaSignature.Location;
                    _signature_mc75.Size = size;

                }
                else
                {
                    int widthFullScreen = Screen.PrimaryScreen.Bounds.Width;
                    areaSignature.Width = widthFullScreen;
                    var size = new System.Drawing.Size(widthFullScreen, 120);
                    //_signature_mc75.Location = areaSignature.Location;
                    _signature_mc75.Size = size;
                }

            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private bool checkDocSigned ()
        {
           try
           {
               if (_signature_mc75.Written || _signature.Written)
                   return true;
               return false;

           } catch (Exception ex)
           {
               FileManager.LogException(ex);
               return false;
           } 
        }

        private void pbHomeClick (object sender, EventArgs e)
        {
            try
            {
                //Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //this.Visible = false;
                //NavigationManager.NavigateHome();
                //Cursor.Current = System.Windows.Forms.Cursors.Default;

                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Legen Sie bitte zunächst den Bericht an.",
                                      null, true, 5000);
                return;
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}
