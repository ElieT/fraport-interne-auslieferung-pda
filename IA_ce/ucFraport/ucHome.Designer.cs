﻿namespace oxmc.IA_ce.ucFraport {

    partial class ucHome
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucHome));
            this.label5 = new System.Windows.Forms.Label();
            this.lblUserVal = new System.Windows.Forms.Label();
            this.lblMandtVal = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblMandt = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDelivery = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLastSyncVal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pbDelivery = new System.Windows.Forms.PictureBox();
            this.pbSync = new System.Windows.Forms.PictureBox();
            this.pbLogout = new System.Windows.Forms.PictureBox();
            this.pbExit = new System.Windows.Forms.PictureBox();
            this.pbDispo = new System.Windows.Forms.PictureBox();
            this.lblDispo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblVersion2 = new System.Windows.Forms.Label();
            this.lblUser2 = new System.Windows.Forms.Label();
            this.pbPickUp = new System.Windows.Forms.PictureBox();
            this.lblPickUp = new System.Windows.Forms.Label();
            this.pbHelp = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(160, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 19);
            this.label5.Text = "Beenden";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblUserVal
            // 
            this.lblUserVal.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.lblUserVal.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblUserVal.Location = new System.Drawing.Point(112, 470);
            this.lblUserVal.Name = "lblUserVal";
            this.lblUserVal.Size = new System.Drawing.Size(222, 20);
            this.lblUserVal.Text = "n/a";
            // 
            // lblMandtVal
            // 
            this.lblMandtVal.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.lblMandtVal.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblMandtVal.Location = new System.Drawing.Point(112, 450);
            this.lblMandtVal.Name = "lblMandtVal";
            this.lblMandtVal.Size = new System.Drawing.Size(38, 20);
            this.lblMandtVal.Text = "n/a";
            // 
            // lblUser
            // 
            this.lblUser.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.lblUser.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblUser.Location = new System.Drawing.Point(3, 470);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(79, 20);
            this.lblUser.Text = "Benutzer";
            // 
            // lblMandt
            // 
            this.lblMandt.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.lblMandt.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblMandt.Location = new System.Drawing.Point(3, 450);
            this.lblMandt.Name = "lblMandt";
            this.lblMandt.Size = new System.Drawing.Size(79, 20);
            this.lblMandt.Text = "Mandant";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(2, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 30);
            this.label3.Text = "Synchronisation";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDelivery
            // 
            this.lblDelivery.Location = new System.Drawing.Point(11, 45);
            this.lblDelivery.Name = "lblDelivery";
            this.lblDelivery.Size = new System.Drawing.Size(76, 30);
            this.lblDelivery.Text = "Auslieferung";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(163, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.Text = "Logout";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblLastSyncVal
            // 
            this.lblLastSyncVal.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.lblLastSyncVal.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblLastSyncVal.Location = new System.Drawing.Point(112, 490);
            this.lblLastSyncVal.Name = "lblLastSyncVal";
            this.lblLastSyncVal.Size = new System.Drawing.Size(222, 20);
            this.lblLastSyncVal.Text = "n/a";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label6.Location = new System.Drawing.Point(3, 490);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 20);
            this.label6.Text = "Letzter Sync";
            // 
            // lblVersion
            // 
            this.lblVersion.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular);
            this.lblVersion.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblVersion.Location = new System.Drawing.Point(4, 510);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(324, 20);
            this.lblVersion.Text = "Letzter Sync";
            // 
            // pbDelivery
            // 
            this.pbDelivery.Image = ((System.Drawing.Image)(resources.GetObject("pbDelivery.Image")));
            this.pbDelivery.Location = new System.Drawing.Point(31, 14);
            this.pbDelivery.Name = "pbDelivery";
            this.pbDelivery.Size = new System.Drawing.Size(25, 25);
            this.pbDelivery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDelivery.Click += new System.EventHandler(this.pbDelivery_Click);
            // 
            // pbSync
            // 
            this.pbSync.Image = ((System.Drawing.Image)(resources.GetObject("pbSync.Image")));
            this.pbSync.Location = new System.Drawing.Point(31, 153);
            this.pbSync.Name = "pbSync";
            this.pbSync.Size = new System.Drawing.Size(25, 25);
            this.pbSync.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSync.Click += new System.EventHandler(this.pbSync_Click);
            // 
            // pbLogout
            // 
            this.pbLogout.Image = ((System.Drawing.Image)(resources.GetObject("pbLogout.Image")));
            this.pbLogout.Location = new System.Drawing.Point(181, 153);
            this.pbLogout.Name = "pbLogout";
            this.pbLogout.Size = new System.Drawing.Size(25, 25);
            this.pbLogout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogout.Click += new System.EventHandler(this.PbLogoutClick);
            // 
            // pbExit
            // 
            this.pbExit.Image = ((System.Drawing.Image)(resources.GetObject("pbExit.Image")));
            this.pbExit.Location = new System.Drawing.Point(181, 203);
            this.pbExit.Name = "pbExit";
            this.pbExit.Size = new System.Drawing.Size(25, 25);
            this.pbExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbExit.Click += new System.EventHandler(this.pbExit_Click);
            // 
            // pbDispo
            // 
            this.pbDispo.Image = ((System.Drawing.Image)(resources.GetObject("pbDispo.Image")));
            this.pbDispo.Location = new System.Drawing.Point(117, 14);
            this.pbDispo.Name = "pbDispo";
            this.pbDispo.Size = new System.Drawing.Size(25, 25);
            this.pbDispo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDispo.Click += new System.EventHandler(this.pbDispo_Click);
            // 
            // lblDispo
            // 
            this.lblDispo.Location = new System.Drawing.Point(94, 45);
            this.lblDispo.Name = "lblDispo";
            this.lblDispo.Size = new System.Drawing.Size(76, 30);
            this.lblDispo.Text = "Transport- disposition";
            this.lblDispo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(2, 231);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.Text = "Version:";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(2, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 14);
            this.label4.Text = "Benutzer:";
            // 
            // lblVersion2
            // 
            this.lblVersion2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblVersion2.Location = new System.Drawing.Point(64, 231);
            this.lblVersion2.Name = "lblVersion2";
            this.lblVersion2.Size = new System.Drawing.Size(100, 14);
            this.lblVersion2.Text = "2013.07.03";
            // 
            // lblUser2
            // 
            this.lblUser2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblUser2.Location = new System.Drawing.Point(64, 217);
            this.lblUser2.Name = "lblUser2";
            this.lblUser2.Size = new System.Drawing.Size(100, 14);
            this.lblUser2.Text = "User";
            // 
            // pbPickUp
            // 
            this.pbPickUp.Image = ((System.Drawing.Image)(resources.GetObject("pbPickUp.Image")));
            this.pbPickUp.Location = new System.Drawing.Point(31, 79);
            this.pbPickUp.Name = "pbPickUp";
            this.pbPickUp.Size = new System.Drawing.Size(25, 25);
            this.pbPickUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPickUp.Click += new System.EventHandler(this.pbPickUpClicked);
            // 
            // lblPickUp
            // 
            this.lblPickUp.Location = new System.Drawing.Point(11, 107);
            this.lblPickUp.Name = "lblPickUp";
            this.lblPickUp.Size = new System.Drawing.Size(64, 20);
            this.lblPickUp.Text = "Abholung";
            // 
            // pbHelp
            // 
            this.pbHelp.Image = ((System.Drawing.Image)(resources.GetObject("pbHelp.Image")));
            this.pbHelp.Location = new System.Drawing.Point(117, 153);
            this.pbHelp.Name = "pbHelp";
            this.pbHelp.Size = new System.Drawing.Size(25, 25);
            this.pbHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHelp.Click += new System.EventHandler(this.sendLogFiles);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(103, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 32);
            this.label7.Text = "Log-Files senden";
            // 
            // ucHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pbHelp);
            this.Controls.Add(this.lblPickUp);
            this.Controls.Add(this.pbPickUp);
            this.Controls.Add(this.lblUser2);
            this.Controls.Add(this.lblVersion2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDispo);
            this.Controls.Add(this.pbDispo);
            this.Controls.Add(this.pbExit);
            this.Controls.Add(this.pbLogout);
            this.Controls.Add(this.pbSync);
            this.Controls.Add(this.pbDelivery);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblLastSyncVal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblDelivery);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblUserVal);
            this.Controls.Add(this.lblMandtVal);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblMandt);
            this.Controls.Add(this.label3);
            this.Name = "ucHome";
            this.Size = new System.Drawing.Size(237, 267);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblUserVal;
        private System.Windows.Forms.Label lblMandtVal;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblMandt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDelivery;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLastSyncVal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.PictureBox pbDelivery;
        private System.Windows.Forms.PictureBox pbSync;
        private System.Windows.Forms.PictureBox pbLogout;
        private System.Windows.Forms.PictureBox pbExit;
        private System.Windows.Forms.PictureBox pbDispo;
        private System.Windows.Forms.Label lblDispo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblVersion2;
        private System.Windows.Forms.Label lblUser2;
        private System.Windows.Forms.PictureBox pbPickUp;
        private System.Windows.Forms.Label lblPickUp;
        private System.Windows.Forms.PictureBox pbHelp;
        private System.Windows.Forms.Label label7;

    }
}
