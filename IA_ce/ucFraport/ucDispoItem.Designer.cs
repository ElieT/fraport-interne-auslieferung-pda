﻿namespace oxmc.IA_ce.ucFraport
{
    partial class ucDispoItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDispoItem));
            this.cbItem = new System.Windows.Forms.CheckBox();
            this.lblNumber = new System.Windows.Forms.Label();
            this.pbError = new System.Windows.Forms.PictureBox();
            this.pbSuccess = new System.Windows.Forms.PictureBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbItem
            // 
            this.cbItem.Location = new System.Drawing.Point(2, 1);
            this.cbItem.Name = "cbItem";
            this.cbItem.Size = new System.Drawing.Size(24, 20);
            this.cbItem.TabIndex = 0;
            this.cbItem.CheckStateChanged += new System.EventHandler(this.ChangeBoxValue);
            // 
            // lblNumber
            // 
            this.lblNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblNumber.Location = new System.Drawing.Point(23, 4);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(78, 15);
            this.lblNumber.Text = "1000123456";
            // 
            // pbError
            // 
            this.pbError.Image = ((System.Drawing.Image)(resources.GetObject("pbError.Image")));
            this.pbError.Location = new System.Drawing.Point(205, 0);
            this.pbError.Name = "pbError";
            this.pbError.Size = new System.Drawing.Size(22, 22);
            this.pbError.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pbSuccess
            // 
            this.pbSuccess.Image = ((System.Drawing.Image)(resources.GetObject("pbSuccess.Image")));
            this.pbSuccess.Location = new System.Drawing.Point(205, 0);
            this.pbSuccess.Name = "pbSuccess";
            this.pbSuccess.Size = new System.Drawing.Size(22, 22);
            this.pbSuccess.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblStatus.Location = new System.Drawing.Point(99, 4);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(100, 15);
            this.lblStatus.Text = "lblStatus";
            // 
            // ucDispoItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.pbSuccess);
            this.Controls.Add(this.pbError);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.cbItem);
            this.Name = "ucDispoItem";
            this.Size = new System.Drawing.Size(227, 22);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox cbItem;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.PictureBox pbError;
        private System.Windows.Forms.PictureBox pbSuccess;
        private System.Windows.Forms.Label lblStatus;
    }
}
