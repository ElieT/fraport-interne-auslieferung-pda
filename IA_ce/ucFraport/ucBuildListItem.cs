﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

//using oxmc.UI_ce.ucCommon;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucBuildListItem : OxUserControlCe
    {
        private String gebnr;
        private List<Delivery> _allDelivsToBuilding;
        private int _sumDelivered = 0;
        
        public ucBuildListItem(List<Delivery> allDelivsToBuilding)
        {
            _allDelivsToBuilding = allDelivsToBuilding;
            if(allDelivsToBuilding.Capacity > 0)
            {
                int sumDelivered = 0;
                gebnr = "";

                // get number of deliverys which have not been finished
                foreach (Delivery deliv in allDelivsToBuilding)
                {
                    // if ("Z".Equals(deliv.Status))
                    if (!String.IsNullOrEmpty(deliv.MobileKey))
                    {
                        sumDelivered = sumDelivered + 1;
                    }
                    gebnr = deliv.Gebnr;
                }
                _sumDelivered = sumDelivered;

                InitializeComponent();

                // Content of labels
                lblBuilding.Text = gebnr;
                lblStatus.Text = sumDelivered + "/" + allDelivsToBuilding.Count;
            }
        }

        public EventHandler ClickEvent;
        private void pbIcon_Click(object sender, EventArgs e)
        {
            if (ClickEvent != null){
                ClickEvent(sender, e);
            } 
        }

        public string GetGebnr()
        {
            return gebnr;
        }

        public bool allItemsDelivered ()
        {
            try
            {
                if (_sumDelivered == _allDelivsToBuilding.Count)
                {
                    return true;
                }
                return false;
            } catch (Exception ex)
            {
                FileManager.LogException(ex);
                return false;
            }
        }
    }
}
