﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using IA_ce;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using System.Drawing;
using oxmc.Common;

namespace oxmc.IA_ce.ucFraport
{
    public partial class ucDeliveryItemList : OxUserControlCe
    {
        private static ucDeliveryItemList _instance;
        private List<Delivery> delivsInBuilding;
        private List<Delivery> changedDelivs;
        private static oxmc.IA_ce.ucFraport.ucHome _home;

        // private ucDeliveryItemList()
        public ucDeliveryItemList()
        {
            InitializeComponent();
        }

        public static ucDeliveryItemList GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ucDeliveryItemList();
            }
            return _instance;
        }
        
        public void RefreshData(string gebnr)
        {
            try 
            {
                lblBuilding.Text = gebnr;

                delivsInBuilding = DeliveryManager.GetDeliverysInBuilding(gebnr, "Z");

                foreach (Control item in pnDelivItems.Controls)
                {
                    item.Visible = false;
                    item.Dispose();
                }
                pnDelivItems.Controls.Clear();

                int top = 0;
                if (delivsInBuilding != null && delivsInBuilding.Count > 0)
                {
                    foreach (Delivery deliv in delivsInBuilding)
                    {
                        //if (("N").Equals(deliv.Status) || String.IsNullOrEmpty(deliv.Status.TrimStart(' ')))
                        //if (!("Z").Equals(deliv.Status))
                        if (String.IsNullOrEmpty(deliv.MobileKey))
                        {
                            ucDeliveryItem ucDeliveryItem = new ucDeliveryItem(deliv);
                            ucDeliveryItem.ClickEvent += ClickEvent;
                            ucDeliveryItem.Top = top*ucDeliveryItem.Height + 2;
                            if (top%2 == 0)
                                ucDeliveryItem.BackColor = Color.LightGray;
                            pnDelivItems.Controls.Add(ucDeliveryItem);
                            top++;
                        }
                    }
                }
                if (top == 0)
                {
                    OxStatus.GetInstance().TriggerEventMessage(0, 'E', DateTime.Now, "Keine offenen Auslieferungsbelege verfügbar!", null, true, 1000);
                }
                if (delivsInBuilding.Count > 0)
                    pbSave.Visible = true;
                else
                    pbSave.Visible = false;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

        }

        public EventHandler ClickEvent;
        public override void btSave_Click(object sender, EventArgs e)
        {
            changedDelivs = new List<Delivery>();

            Cursor.Current = Cursors.WaitCursor;
            // für jedes angezeigte Element im Panel
            foreach (ucDeliveryItem item in pnDelivItems.Controls)
            {
                // current delivery
                Delivery curDeliv = item.Deliv;

                // Holen der neuen Werte
                // Status: zugestellt
                if (item.GetValueRbDelivered())
                {
                    // Überprüfung des vorherigen Werts.
                    //if (!item.Deliv.Status.Equals("Z"))
                    //{
                        curDeliv.Status = "Z";
                        curDeliv.Lidat = DateTime.Now.Date;
                        curDeliv.Gtext = "";
                        curDeliv.Updflag = "U";
                        curDeliv.Lfname = AppConfig.UserId;

                        // Hinzufügen zu Liste der übergebenen Deliverys
                        changedDelivs.Add(curDeliv);
                    //}
                    /*else
                    {
                        // Der Wert war auch vorher schon "J". Ist allerdings nicht möglich, da sich nur 
                        // Deliverys mit Status "N" in der Liste befinden.
                    }*/
                }
                    // Status: fehlgeschlagen
                else if (item.GetValueRbFailed())
                {
                    if (!(item.GetChangedText().Equals("")))
                    {
                        
                        //if (!(item.GetChangedText().Equals(curDeliv.Gtext)))
                        //{
                            curDeliv.Status = "F";

                            curDeliv.Grund = DeliveryManager.GetStatus(item.GetChangedText());
                            curDeliv.Gtext = item.GetChangedText();
                            // Updflag
                            curDeliv.Updflag = "U";

                            // Hinzufügen zu Liste der übergebenen Deliverys
                            changedDelivs.Add(curDeliv);
                        //}
                    }
                    else
                    {
                        OxStatus.GetInstance().TriggerEventMessage(0, 'E', DateTime.Now, "Geben Sie für alle fehlgeschlagenen Auslieferbelege einen Grund an.", null, true, 1000);
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                }

                //i = i + 1;
            }

            if (changedDelivs.Count > 0)
            {
                foreach (var del in changedDelivs)
                {
                    if ((!del.Status.Equals("Z")) && (!String.IsNullOrEmpty(del.Status)) && (String.IsNullOrEmpty(del.Gtext)))
                    {
                        OxStatus.GetInstance().TriggerEventMessage(0, 'E', DateTime.Now, "Geben Sie für alle fehlgeschlagenen Auslieferbelege einen Grund an.", null, true, 10);
                        return;
                    } 
                    //DeliveryManager.SaveDelivery(del);
                }
                // OxStatus.GetInstance().TriggerEventMessage(0, 'S', DateTime.Now, "Daten wurden erfolgreich gespeichert.", null, true, 10);
                // Aufruf der nächsten UI
                ucDeliveryPdf pdf = new ucDeliveryPdf();
                pdf.ChangeUserControl += ChangeUserControl;
                pdf.RefreshData(changedDelivs);
                Cursor.Current = Cursors.Default;
                NavigationManager.NavigateForward(pdf);
            }
            else
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void pbHome_Clicked (Object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            NavigationManager.NavigateHome();
            Cursor.Current = Cursors.Default;
        }

        private void pbBack_Clicked (Object sender, EventArgs e)
        {
            Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            NavigationManager.NavigateBack();
            Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }
}
