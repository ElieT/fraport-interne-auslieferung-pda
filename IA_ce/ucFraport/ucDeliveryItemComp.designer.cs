﻿namespace oxmc.IA_ce.ucFraport
{
    partial class ucDeliveryItemComp
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRoom = new System.Windows.Forms.Label();
            this.lblAddressee = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblUnit = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblCause = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRoom
            // 
            this.lblRoom.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblRoom.Location = new System.Drawing.Point(6, 2);
            this.lblRoom.Name = "lblRoom";
            this.lblRoom.Size = new System.Drawing.Size(47, 15);
            this.lblRoom.Text = "lblRoom";
            // 
            // lblAddressee
            // 
            this.lblAddressee.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblAddressee.Location = new System.Drawing.Point(50, 2);
            this.lblAddressee.Name = "lblAddressee";
            this.lblAddressee.Size = new System.Drawing.Size(90, 15);
            this.lblAddressee.Text = "lblAddressee";
            // 
            // lblQuantity
            // 
            this.lblQuantity.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblQuantity.Location = new System.Drawing.Point(143, 3);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(50, 15);
            this.lblQuantity.Text = "lblQuantity";
            this.lblQuantity.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblUnit
            // 
            this.lblUnit.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblUnit.Location = new System.Drawing.Point(198, 3);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(39, 15);
            this.lblUnit.Text = "lblUnit";
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblStatus.Location = new System.Drawing.Point(6, 17);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(42, 15);
            this.lblStatus.Text = "zugest.";
            // 
            // lblCause
            // 
            this.lblCause.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblCause.Location = new System.Drawing.Point(50, 17);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(185, 15);
            this.lblCause.Text = "lblCause";
            // 
            // ucDeliveryItemComp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lblCause);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblUnit);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.lblAddressee);
            this.Controls.Add(this.lblRoom);
            this.Name = "ucDeliveryItemComp";
            this.Size = new System.Drawing.Size(237, 35);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblRoom;
        private System.Windows.Forms.Label lblAddressee;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblCause;
    }
}
