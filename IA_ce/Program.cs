﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.WindowsCE.Forms;
using oxmc.Common;
using oxmc.IA_ce;

namespace oxmc.IA_ce
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main(string[] args)
        {
            String configMode = "app_prd";
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "dev":
                        configMode = "app_dev";
                        break;
                    case "ox":
                        configMode = "app_ox";
                        break;
                    case "kon":
                        configMode = "app_kon";
                        break;
                    default:
                        configMode = "app_prd";
                        break;
                }
            }
            AppConfig.LoadSettings(configMode);
            Application.Run(new FormMain());
        }
    }
}