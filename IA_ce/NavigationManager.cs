﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using oxmc.IA_ce;

namespace IA_ce
{
    public class NavigationManager
    {
        private static Stack<Object[]> _navStack;

        /// <summary>
        /// Call this method if moving to a new Page.
        /// If the container is the same as the last container, the last container will be replaced on the stack, otherwise a new object-array containing 
        /// the container, control and data will be pushed on the stack
        /// </summary>
        /// <param name="control">The control displaying the data</param>
        /// <param name="data">The data that is displaed in the control</param>
        public static void NavigateForward(OxUserControlCe control)
        {
            if (_navStack == null)
                _navStack = new Stack<object[]>();
            var page = new Object[2];
            page[0] = control;
            _navStack.Push(page);
            control.ShowThisControl(0,0,FormMain.Instance);
        }

        /// <summary>
        /// Pops the last element from the navigationstack an returns its predecessor
        /// If one or no Elements are on the stack, the stack will be cleared and the method return null to indicate a return to the homescreen
        /// </summary>
        /// <returns>Objectarray containing the last container, control and data</returns>
        public static Object[] NavigateBack()
        {
            if (_navStack == null)
                return null;
            if (_navStack.Count > 0)
            {
                ((OxUserControlCe)_navStack.Peek()[0]).Visible = false;
                _navStack.Pop();
            }
            if (_navStack.Count > 0)
                return _navStack.Peek();
            return null;
        }

        /// <summary>
        /// Removes the last element from the stack e.g. when data is saved and the current control is to be closed
        /// </summary>
        public static void RemoveLast()
        {
            if (_navStack.Count > 0)
                _navStack.Pop();
        }

        /// <summary>
        /// Returns the previous control Object on the Stack without removing it
        /// </summary>
        /// <returns></returns>
        public static Object[] GetLast()
        {
            var last = _navStack.Pop();
            var previous = _navStack.Peek();
            _navStack.Push(last);
            return previous;
        }

        public static void NavigateHome()
        {
            var retVal = new Object[3];
            do
            {
                retVal = NavigateBack();
                if (retVal != null)
                {
                    if (retVal.GetValue(0) != null)
                    {
                        var i = retVal.GetValue(0).GetType().Name.IndexOf("ucHome");
                        if(i > -1)
                        {
                            break;
                        }
                    }
                }
            } while (retVal != null);
        }
    }
}
