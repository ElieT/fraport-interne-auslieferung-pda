﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using IA_ce;
using IA_ce.ucFraport;
using Microsoft.WindowsCE.Forms;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;
//using oxmc.IA_ce.ucCommon;
using oxmc.IA_ce.ucFraport;
using oxmc.IA_ce;
using oxmc_ce.IA_ce.ucFraport;

namespace oxmc.IA_ce
{
    public partial class FormMain : Form
    {
        private IA_ce.OxUserControlCe _ucHome;
        private readonly OxStatus _oxStatusbar;
        private ucEventMessage _ucEventMessage;
        public static FormMain Instance;
        
        public FormMain()
        {
            InitializeComponent();
            _oxStatusbar = OxStatus.GetInstance();
            _oxStatusbar.EventMessageTriggered += ShowEventMessage;
            //_oxStatusbar.PersIdentChanged += ChangePersIdent;
            Instance = this;
            //ChangePersIdent(this, EventArgs.Empty);

            if (_ucHome == null)
            {
                if (AppConfig.FlagAutoLogon.Equals("X"))
                    _ucHome = ucFraport.ucHome.GetInstance();
                else
                    _ucHome = ucUser.GetInstance();
            }
            _ucHome.ShowThisControl(this);
        }


        public void ShowEventMessage(object sender, EventArgs e)
        {
            if (_ucEventMessage == null)
                _ucEventMessage = ucEventMessage.GetInstance();
            _ucEventMessage.BringToFront();
            _ucEventMessage.BtMaximizeClick();
        }

        private void FormMain_Closing(object sender, CancelEventArgs e)
        {
            var res = MessageBox.Show("Möchten Sie die Applikation wirklich beenden?", "Beenden", MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if(res == DialogResult.No)
                e.Cancel = true;

        }

        private void SayHello(object sender, EventArgs e)
        {
            MessageBox.Show("Hello World");
        }

        public void WebServerHelpMeStarted(object sender, EventArgs e)
        {
            try
            {
                if (InvokeRequired)
                    Invoke((ThreadStart)delegate { ucHelpMessage.GetInstance().ShowMessage(); Refresh(); });
                else
                {
                    ucHelpMessage.GetInstance().ShowMessage();
                    Refresh();
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void WebServerHelpMeProgress(object sender, EventArgs e)
        {
            try
            {
                if (sender is int)
                {
                    var progress = (int)sender;

                    if (InvokeRequired)
                        Invoke((ThreadStart)delegate
                        {
                            ucHelpMessage.GetInstance().SetProgress(progress);
                            Refresh();
                        });
                    else
                    {
                        ucHelpMessage.GetInstance().SetProgress(progress);
                        Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void WebServerHelpMeEnded(object sender, EventArgs e)
        {
            try
            {
                if (InvokeRequired)
                    Invoke((ThreadStart)delegate
                    {
                        ucHelpMessage.GetInstance().HideMessage();
                        Refresh();
                    });
                else
                {
                    ucHelpMessage.GetInstance().HideMessage();
                    Refresh();
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

    }
}