﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace Restarter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main(string[] args)
        {
            Thread.Sleep(2500);
            ProcessStartInfo s = new ProcessStartInfo();
            string AppPath =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

            s.FileName = AppPath + "\\oxmc.UI_ce.exe";
            s.UseShellExecute = false;
            foreach (var arg in args)
                s.Arguments += " " + arg;
            Process.Start(s);
            Application.Exit();
        }
    }
}