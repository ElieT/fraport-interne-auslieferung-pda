﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace SVMPackageBuilder
{
    public partial class Form1 : Form
    {
        private string path = "";
        public Form1()
        {
            InitializeComponent();
            textBox2.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\oxmc.zip";
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var d = Uri.UnescapeDataString(uri.Path);
            while (!d.EndsWith("SVMPackageBuilder"))
            {
                d = Directory.GetParent(d).FullName;
            }
            d = Directory.GetParent(d).FullName;
            textBox3.Text = d + "\\Updater\\bin\\Release\\Updater.exe";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var d = Uri.UnescapeDataString(uri.Path);
            while (!d.EndsWith("SVMPackageBuilder"))
            {
                d = Directory.GetParent(d).FullName;
            }
            folderBrowserDialog1.SelectedPath = d + "\\UI\\bin\\x86\\";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
            d = folderBrowserDialog1.SelectedPath;
            var subD = Directory.GetDirectories(d);
            var files = Directory.GetFiles(d).ToList();
            foreach (var direct in subD)
            {
                if (direct.Equals(d))
                continue;
                var f = GetFiles(direct);
                foreach (var file in f)
                {
                    var node = new TreeNode(file.Substring(textBox1.Text.Length));
                    node.Checked = true;
                    treeView1.Nodes.Add(node);
                }

            }
            foreach (var file in files)
            {
                if (file.EndsWith("pdb") || file.Contains("vshost"))
                    continue;
                var node = new TreeNode(file.Substring(textBox1.Text.Length));
                node.Checked = true;
                treeView1.Nodes.Add(node);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var fsOut = File.Create(textBox2.Text);
                var zipStream = new ZipOutputStream(fsOut);
                zipStream.SetLevel(3);

                var fi = new FileInfo(textBox3.Text.TrimStart('/'));
                var entryName = textBox3.Text.Substring(textBox3.Text.IndexOf("Updater.exe"));
                entryName = ZipEntry.CleanName(entryName);
                var newEntry = new ZipEntry(entryName);
                newEntry.DateTime = fi.LastWriteTime;
                newEntry.Size = fi.Length;
                zipStream.PutNextEntry(newEntry);
                var buffer = new byte[4096];
                using (var streamReader = File.OpenRead(textBox3.Text))
                {
                    StreamUtils.Copy(streamReader, zipStream, buffer);
                }
                zipStream.CloseEntry();

                foreach (TreeNode node in treeView1.Nodes)
                {
                    if (node.Checked)
                    {
                        fi = new FileInfo(textBox1.Text + node.Text);
                        entryName = node.Text;
                        entryName = ZipEntry.CleanName(entryName);
                        newEntry = new ZipEntry(entryName);
                        newEntry.DateTime = fi.LastWriteTime;
                        newEntry.Size = fi.Length;
                        zipStream.PutNextEntry(newEntry);
                        buffer = new byte[4096];
                        using (var streamReader = File.OpenRead(textBox1.Text + node.Text))
                        {
                            StreamUtils.Copy(streamReader, zipStream, buffer);
                        }
                        zipStream.CloseEntry();
                    }
                }


                zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
                zipStream.Close();

                Application.Exit();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private List<string> GetFiles(string path)
        {
            var retVal = new List<string>();
            foreach (var d in Directory.GetDirectories(path))
                retVal.AddRange(GetFiles(d).ToList());
            retVal.AddRange(Directory.GetFiles(path).ToList());
            return retVal;
        }

        private void button3_Click(object sender, EventArgs e)
        {

            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var d = Uri.UnescapeDataString(uri.Path);
            while (!d.EndsWith("oxmc"))
            {
                d = Directory.GetParent(d).FullName;
            }
            folderBrowserDialog1.SelectedPath = d + "\\Updater\\bin\\Release\\Updater.exe";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox3.Text = folderBrowserDialog1.SelectedPath;
            }
        }
    }
}
