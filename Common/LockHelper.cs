﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace oxmc.Common
{
    public class LockHelper
    {
        private static object _locker = new object();
        public static object Locker { get { return _locker; } }
    }
}
