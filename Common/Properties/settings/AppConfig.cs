﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Xml;

namespace oxmc.Common
{
    public class AppConfig
    {
        #region Fields

        private static string AppPath =
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static NameValueCollection Settings;
        private static XmlNodeList _nodeList;
        private static String _configMode;
        private static String _defaultConfigMode = "app_prd";
        private static String _configFile = "\\" + _defaultConfigMode + ".config";
        private static String _OSVersion = "";
        public const String OS_WINCE = "WinCE";
        public const String OS_WIN32 = "Win32NT";
        public static string ConfigFile
        {
            get { return _configMode; }
            set { _configMode = value; }
        }

        private static XmlDocument _xmlDocument;
        #endregion

        #region Constructors

        // Static Ctor
        static AppConfig()
        {
            _OSVersion = System.Environment.OSVersion.Platform.ToString();
        }

        public static void LoadSettings(String configMode)
        {
            if (String.IsNullOrEmpty(configMode))
                configMode = _defaultConfigMode;
            _configMode = configMode;
            //System.Console.WriteLine("load settings: " + _configMode);
            try
            {
                if (AppPath == null)
                    return;
                if (AppPath.Length > 7 && AppPath.IndexOf("file:") != -1)
                    AppPath = AppPath.Substring(6, AppPath.Length - 6);
                _configFile = AppPath + "\\" + configMode + ".config";

                if (!File.Exists(_configFile))
                {
                    throw new FileNotFoundException(string.Format("Application configuration file '{0}' not found.",
                                                                  _configFile));
                }
                _xmlDocument = new XmlDocument();
                _xmlDocument.Load(_configFile);
                _nodeList = _xmlDocument.GetElementsByTagName("appSettings");
                if (Settings == null)
                    Settings = new NameValueCollection();

                foreach (XmlNode node in _nodeList)
                {
                    foreach (XmlNode key in node.ChildNodes)
                    {
                        if ((key.Attributes["key"].Value.Equals("UserId")) && !String.IsNullOrEmpty(Settings["UserId"]))
                        {

                        }
                        else if ((key.Attributes["key"].Value.Equals("Mandt")) && !String.IsNullOrEmpty(Settings["Mandt"]))
                        {

                        }
                        else if ((key.Attributes["key"].Value.Equals("Password")) && !String.IsNullOrEmpty(Settings["Password"]))
                        {

                        }
                        else
                        {
                            Settings.Set(key.Attributes["key"].Value,
                                         key.Attributes["value"].Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public static void SaveSettings(String configMode)
        {
            try
            {
                if (String.IsNullOrEmpty(configMode))
                    configMode = _defaultConfigMode;
                //configMode = _defaultConfigMode;
                _configMode = configMode;
                System.Console.WriteLine("save settings: " + _configMode);
                if (AppPath == null)
                    return;
                if (AppPath.Length > 7 && AppPath.IndexOf("file:") != -1)
                    AppPath = AppPath.Substring(6, AppPath.Length - 6);
                _configFile = AppPath + "\\" + configMode + ".config";

                foreach (XmlNode node in _nodeList)
                {
                    foreach (XmlNode key in node.ChildNodes)
                    {
                        if (Settings[key.Attributes["key"].Value] != null)
                            key.Attributes["value"].Value = Settings[key.Attributes["key"].Value];
                        if (key.Attributes["key"].Value.Equals("Password"))
                            key.Attributes["value"].Value = "";
                    }
                }
                _xmlDocument.Save(_configFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion Constructors

        #region Public Properties

        /// <summary>
        /// Defines the location where userdata should be stored
        /// The default is the user directory. It will be used íf UseUserDirectory != 0 and  DataLocation is empty
        /// If UseUserDirectory == 0 and DataLocation == empty, the application directory will be used
        /// Folder "\oxando\oxmc\" will be appended if any directory except the application directory is used
        /// </summary>
        public static string DataLocation
        {
            get
            {
                var locvalue = Settings.Get("DataLocation");
                string path;
                if (string.IsNullOrEmpty(locvalue.Trim()) && UseUserDirectory)
                {
                    path = "";//Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                    path += "\\oxando\\oxmc\\";
                    //MessageBox.Show(path);
                }
                else if (string.IsNullOrEmpty(locvalue))
                {
                    string codeBase = Assembly.GetExecutingAssembly().GetName().CodeBase;
                    var d = Uri.UnescapeDataString(codeBase);

                    var slash = "";
                    if (d.StartsWith("file:///"))
                    {
                        d = d.Substring(8);
                    }
                    if (d.StartsWith("\\"))
                        slash = "\\";
                    else
                        slash = "/";
                    path = d.Substring(0, d.LastIndexOf(slash));
                }
                else
                {
                    path = locvalue;
                    path += "\\oxando\\oxmc\\";
                }
                return path;
            }
        }

        /// <summary>
        /// Defines if the systems user directory should be used to store all userdata (logfiles, database, documents)
        /// true if UseUserDirectory != 0
        /// </summary>
        public static bool UseUserDirectory
        {
            get
            {
                var locvalue = Settings.Get("UseUserDirectory");
                if (!locvalue.Equals("0"))
                    return true;
                return false;
            }
            set
            {
                if (value)
                    Settings.Set("UseUserDirectory", "1");
                else
                    Settings.Set("UseUserDirectory", "0");
            }
        }

        public static string UserDirectory
        {
            get
            {
                var path = DataLocation;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                return path;
            }
        }

        public static string DatabasePath
        {
            get
            {
                String retwert = "";
                retwert = @"Data Source = " + Path.Combine(AppPath, "AssetManagementDB_SQLite.sqlite");
                return retwert;
            }
        }

        public static string GetAppPath()
        {
            return AppPath;
        }

        public static String SVMApplicationId
        {
            get { return Settings.Get("SVMApplicationId"); }
            set { Settings.Set("SVMApplicationId", value); }
        }
        public static String SVMClientOs
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("SVMClientOSCE"); else return Settings.Get("SVMClientOSW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("SVMClientOSCE", value); else Settings.Set("SVMClientOSW32", value); }
        }

        public static String SVMClientExe
        {
            get { return Settings.Get("SVMClientExe"); }
            set { Settings.Set("SVMClientExe", value); }
        }

        public static String SVMTempDirectory
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("SVMTempDirectoryCE"); else return Settings.Get("SVMTempDirectoryW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("SVMTempDirectoryCE", value); else Settings.Set("SVMTempDirectoryW32", value); }
        }

        public static String SVMTempDirectory_CE
        {
            get { return Settings.Get("SVMTempDirectoryCE"); }
            set { Settings.Set("SVMTempDirectoryCE", value); }
        }

        public static String SVMTempDirectory_W32
        {
            get { return Settings.Get("SVMTempDirectoryW32"); }
            set { Settings.Set("SVMTempDirectoryW32", value); }
        }

        public static int RfidTechObjMaxTagLength
        {
            get { return int.Parse(Settings.Get("rfid.TechObjMaxTagLength")); }
            set { Settings.Set("rfid.TechObjMaxTagLength", value.ToString()); }
        }

        public static int RfidPersIdMaxTagLength
        {
            get { return int.Parse(Settings.Get("rfid.PersIdMaxTagLength")); }
            set { Settings.Set("rfid.PersIdMaxTagLength", value.ToString()); }
        }

        public static int RfidTechObjIdentLength
        {
            get { return int.Parse(Settings.Get("rfid.TechObjIdentLength")); }
            set { Settings.Set("rfid.TechObjIdentLength", value.ToString()); }
        }

        public static Boolean DebugMode
        {
            get { return Settings.Get("System.DebugMode").Equals("X"); }
            set { Settings.Set("System.DebugMode", (value) ? "X" : ""); }
        }

        public static String TraceFileName
        {
            get { return Settings.Get("trace.filename"); }
            set { Settings.Set("trace.filename", value); }
        }

        public static String RFIDTimeout
        {
            get { return Settings.Get("rfid.timeout"); }
            set { Settings.Set("rfid.timeout", value); }
        }

        public static String RFIDPortName
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("rfid.PortNameCE"); else return Settings.Get("rfid.PortNameW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("rfid.PortNameCE", value); else Settings.Set("rfid.PortNameW32", value); }
        }

        public static String RFIDPortName_CE
        {
            get { return Settings.Get("rfid.PortNameCE"); }
            set { Settings.Set("rfid.PortNameCE", value); }
        }

        public static String RFIDPortName_W32
        {
            get { return Settings.Get("rfid.PortNameW32"); }
            set { Settings.Set("rfid.PortNameW32", value); }
        }

        public static String RFIDPortType
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("rfid.PortTypeCE"); else return Settings.Get("rfid.PortTypeW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("rfid.PortTypeCE", value); else Settings.Set("rfid.PortTypeW32", value); }
        }

        public static String RFIDPortType_CE
        {
            get { return Settings.Get("rfid.PortTypeCE"); }
            set { Settings.Set("rfid.PortTypeCE", value); }
        }

        public static String RFIDPortType_W32
        {
            get { return Settings.Get("rfid.PortTypeW32"); }
            set { Settings.Set("rfid.PortTypeW32", value); }
        }

        public static Boolean RFIDWriteVerify
        {
            get { return Settings.Get("rfid.WriteVerify").Equals("X"); }
            set { Settings.Set("rfid.WriteVerify", (value) ? "X" : ""); }
        }

        public static String RFIDDriverTimeOut
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("rfid.DriverTimeOutCE"); else return Settings.Get("rfid.DriverTimeOutW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("rfid.DriverTimeOutCE", value); else Settings.Set("rfid.DriverTimeOutW32", value); }
        }

        public static String RFIDDriverTimeOut_CE
        {
            get { return Settings.Get("rfid.DriverTimeOutCE"); }
            set { Settings.Set("rfid.DriverTimeOutCE", value); }
        }

        public static String RFIDDriverTimeOut_W32
        {
            get { return Settings.Get("rfid.DriverTimeOutW32"); }
            set { Settings.Set("rfid.DriverTimeOutW32", value); }
        }

        public static String TraceFileSize
        {
            get { return Settings.Get("trace.filesize"); }
            set { Settings.Set("trace.filesize", value); }
        }

        public static String UserId
        {
            get { return Settings.Get("UserId"); }
            set { Settings.Set("UserId", value); }
        }

        public static String Mandt
        {
            get { return Settings.Get("Mandt"); }
            set { Settings.Set("Mandt", value); }
        }

        public static String Email1
        {
            get { return Settings.Get("user.email1"); }
            set { Settings.Set("user.email1", value); }
        }

        public static String Email2
        {
            get { return Settings.Get("user.email2"); }
            set { Settings.Set("user.email2", value); }
        }

        public static String Pw
        {
            get { return Settings.Get("Password"); }
            set { Settings.Set("Password", value); }
        }

        public static string Protocol
        {
            get { return Settings.Get("Protocol"); }
            set { Settings.Set("Protocol", value); }
        }

        public static string Gateway
        {
            get { return Settings.Get("Gateway"); }
            set { Settings.Set("Gateway", value); }
        }

        public static string GatewayIdent
        {
            get { return Settings.Get("GatewayIdent"); }
            set { Settings.Set("GatewayIdent", value); }
        }

        public static string GatewayPort
        {
            get { return Settings.Get("GatewayPort"); }
            set { Settings.Set("GatewayPort", value); }
        }

        public static string GatewayServiceSync
        {
            get { return Settings.Get("GatewayServiceSync"); }
            set { Settings.Set("GatewayServiceSync", value); }
        }

        public static string GatewayServiceInfo
        {
            get { return Settings.Get("GatewayServiceInfo"); }
            set { Settings.Set("GatewayServiceInfo", value); }
        }


        public static string GatewayServiceSVM
        {
            get { return Settings.Get("GatewayServiceSVM"); }
            set { Settings.Set("GatewayServiceSVM", value); }
        }

        public static string DataFileFolder
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("EquiFileFolderCE"); else return Settings.Get("EquiFileFolderW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("EquiFileFolderCE", value); else Settings.Set("EquiFileFolderW32", value); }
        }

        public static String DataFileFolder_CE
        {
            get { return Settings.Get("EquiFileFolderCE"); }
            set { Settings.Set("EquiFileFolderCE", value); }
        }

        public static String DataFileFolder_W32
        {
            get { return Settings.Get("EquiFileFolderW32"); }
            set { Settings.Set("EquiFileFolderW32", value); }
        }

        public static string FlagMassdata
        {
            get { return Settings.Get("sync.massData"); }
            set { Settings.Set("sync.massData", value); }
        }

        public static string FlagCustomizing
        {
            get { return Settings.Get("sync.reqCust"); }
            set { Settings.Set("sync.reqCust", value); }
        }

        public static string FlagDocuments
        {
            get { return Settings.Get("sync.reqDoc"); }
            set { Settings.Set("sync.reqDoc", value); }
        }

        public static string FlagMasterdata
        {
            get { return Settings.Get("sync.reqMD"); }
            set { Settings.Set("sync.reqMD", value); }
        }

        public static string FlagAutoLogon
        {
            get { return Settings.Get("user.AutoLogon"); }
            set { Settings.Set("user.AutoLogon", value); }
        }

        public static string FlagTestmode
        {
            get { return Settings.Get("sync.testMode"); }
            set { Settings.Set("sync.testMode", value); }
        }
        public static string FlagConfirmation
        {
            get { return Settings.Get("sync.confirmation"); }
            set { Settings.Set("sync.confirmation", value); }
        }
        public static string FlagBackgroundSync
        {
            get { return Settings.Get("sync.backgroundSync"); }
            set { Settings.Set("sync.backgroundSync", value); }
        }

        public static string TestMode
        {
            get { return Settings.Get("sync.testMode"); }
            set { Settings.Set("sync.testMode", value); }
        }
        public static string FlagSyncAtStartup
        {
            get { return Settings.Get("sync.SyncAtStartup"); }
            set { Settings.Set("sync.SyncAtStartup", value); }
        }

        public static string FlagShowOrderReportOnSave
        {
            get { return Settings.Get("ShowOrderReportOnSave"); }
            set { Settings.Set("ShowOrderReportOnSave", value); }
        }

        public static string PresetNetworkAddress
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("PresetNetworkAddressCE"); else return Settings.Get("PresetNetworkAddressW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("PresetNetworkAddressCE", value); else Settings.Set("PresetNetworkAddressW32", value); }
        }

        public static string PresetVPNAddress
        {
            get { if (_OSVersion.Equals(OS_WINCE)) return Settings.Get("PresetVPNAddressCE"); else return Settings.Get("PresetVPNAddressW32"); }
            set { if (_OSVersion.Equals(OS_WINCE)) Settings.Set("PresetVPNAddressCE", value); else Settings.Set("PresetVPNAddressW32", value); }
        }

        public static string TimePrecision
        {
            get { return Settings.Get("TimePrecision"); }
            set { Settings.Set("TimePrecision", value); }
        }

        public static Boolean RfidEnabled
        {
            get { return Settings.Get("RfidEnabled").Equals("X"); }
            set { Settings.Set("RfidEnabled", (value) ? "X" : ""); }
        }

        public static String DeviceId
        {
            get { return Settings.Get("System.DeviceId"); }
            set { Settings.Set("System.DeviceId", value); }
        }

        public static int MessageTimeout
        {
            get { return int.Parse(Settings.Get("Statusmessage.Timeout")); }
            set { Settings.Set("Statusmessage.Timeout", value.ToString()); }
        }

        public static string SVMAppPath
        {
            get { return Settings.Get("SVMAppPath"); }
            set { Settings.Set("SVMAppPath", value); }
        }

        public static string ResidenceTime
        {
            get { return Settings.Get("ResidenceTime"); }
            set { Settings.Set("ResidenceTime", value); }
        }

        public static string BackendNumberFormat
        {
            get
            {
                var nf = Settings.Get("BackendNumberFormat");
                if (string.IsNullOrEmpty(nf))
                    return "en-US";
                return Settings.Get("BackendNumberFormat");
            }
            set { Settings.Set("BackendLanguage", value); }
        }

        public static int TimeoutDataSync
        {
            get { return int.Parse(Settings.Get("TimeoutDataSync")); }
            set { Settings.Set("TimeoutDataSync", value.ToString()); }
        }

        public static int TimeoutUserSync
        {
            get { return int.Parse(Settings.Get("TimeoutUserSync")); }
            set { Settings.Set("TimeoutUserSync", value.ToString()); }
        }

        public static NumberFormatInfo NumberFormat
        {
            get { return CultureInfo.GetCultureInfo(BackendNumberFormat).NumberFormat; }
        }
        /// <summary>
        /// Defines the Level for displaying messages to the user
        /// Messagelevels are:
        ///  - 0: No messages are displayed
        ///  - 1: only informatory messages are displayed (type I)
        ///  - 2: All messages but Exceptions (type E) are displayed 
        ///  - 3: Debugging mode, all Mmssages are displayed
        /// </summary>
        public static string MessageDisplayLevel
        {
            get { return Settings.Get("MessageDisplayLevel"); }
            set { Settings.Set("MessageDisplayLevel", value); }
        }

        public static String DefaultApplication
        {
            get { return Settings.Get("application.default"); }
        }

        public static string FilePath
        {
            get { return Settings.Get("FilePathCE"); }
        }

        public static int[] Color1
        {
            get
            {
                if (Settings == null)
                    return null;

                String strColor = Settings.Get("Color1");
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        public static int[] Color2
        {
            get
            {
                String strColor = Settings.Get("Color2");
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        public static int[] Color3
        {
            get
            {
                String strColor = Settings.Get("Color3");
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        public static Boolean ResetData { get; set; }

        public static int FileResidenceDays
        {
            get { return int.Parse(Settings.Get("FileResidenceDays")); }
        }

        public static string SapLanguage
        {
            get { return Settings.Get("SAPLanguage"); }
            set { Settings.Set("SAPLanguage", value); }
        }

        public static int TimeoutDetail
        {
            get
            {
                var timeoutSt = Settings.Get("TimeoutDetail");
                var timeout = 30000;
                if (!string.IsNullOrEmpty(timeoutSt))
                {
                    try
                    {
                        timeout = int.Parse(timeoutSt);
                    }
                    catch (Exception ex) { }
                }
                return timeout;
            }
        }

        public static string GatewayControllerSync
        {
            get
            {
                return Settings.Get("GatewayControllerSync");
            }
            set
            {
                Settings.Set("GatewayControllerSync", value);
            }
        }

        public static string GatewayServiceSyncOld
        {
            get { return Settings.Get("GatewayServiceSyncOld"); }
            set { Settings.Set("GatewayServiceSyncOld", value); }
        }

        public static string ProxyHostname
        {
            get { return Settings.Get("Proxy.Hostname"); }
            set { Settings.Set("Proxy.Hostname", value); }
        }

        public static string ProxyPort
        {
            get
            {
                return Settings.Get("Proxy.Port");
            }
            set
            {
                Settings.Set("Proxy.Port", value);
            }
        }

        public static int TimeoutSync
        {
            get
            {
                var timeoutSt = Settings.Get("TimeoutSync");
                var timeout = 900000;
                if (!string.IsNullOrEmpty(timeoutSt))
                {
                    try
                    {
                        timeout = int.Parse(timeoutSt);
                    }
                    catch (Exception ex) { }
                }
                return timeout;
            }
        }

        #endregion
    }
}