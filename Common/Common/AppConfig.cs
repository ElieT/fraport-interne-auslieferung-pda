﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Xml;

namespace oxmc.Common
{
    public class AppConfig
    {
        #region Fields

        private static readonly string AppPath =
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static readonly NameValueCollection Settings;
        private static XmlNodeList _nodeList;
        private static String _configFile;
        private static XmlDocument _xmlDocument;
        #endregion

        #region Constructors

        // Static Ctor
        static AppConfig()
        {
            try
            {
                if (AppPath == null)
                    return;
                if (AppPath.Length > 7 && AppPath.IndexOf("file:") != -1)
                    AppPath = AppPath.Substring(6, AppPath.Length - 6);
                _configFile = AppPath + "\\app.config";
                // note: the app config itself must be copied to the output directory. (set the respective settings
                // for the file in the visual studio)
                if (!File.Exists(_configFile))
                {
                    throw new FileNotFoundException(string.Format("Application configuration file '{0}' not found.",
                                                                  _configFile));
                }
                _xmlDocument = new XmlDocument();
                _xmlDocument.Load(_configFile);
                _nodeList = _xmlDocument.GetElementsByTagName("appSettings");
                Settings = new NameValueCollection();

                foreach (XmlNode node in _nodeList)
                {
                    foreach (XmlNode key in node.ChildNodes)
                    {
                        Settings.Add(key.Attributes["key"].Value,
                                     key.Attributes["value"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void SaveSettings()
        {
            try
            {
                foreach (XmlNode node in _nodeList)
                {
                    foreach (XmlNode key in node.ChildNodes)
                    {
                        if (Settings[key.Attributes["key"].Value] != null)
                            key.Attributes["value"].Value = Settings[key.Attributes["key"].Value];
                    }
                }
                _xmlDocument.Save(_configFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion Constructors

        #region Public Properties

        public static string DatabasePath
        {
            get
            {
                String retwert = "";
                //retwert = @"Data Source = " + Path.Combine(AppPath, "AssetManagementDB.sdf");
                retwert = @"Data Source = " + Path.Combine(AppPath, "AssetManagementDB_SQLite.sqlite");
                return retwert;
            }
        }

        public static string GetAppPath()
        {
            return AppPath;
        }

        public static Boolean DebugMode
        {
            get { return Settings.Get("system.DebugMode").Equals("true"); }
            set { Settings.Set("system.DebugMode", value.ToString()); }
        }
        public static String TraceFileName
        {
            get { return Settings.Get("trace.filename"); }
            set { Settings.Set("trace.filename", value); }
        }
        public static String RFIDTimeout
        {
            get { return Settings.Get("rfid.timeout"); }
            set { Settings.Set("rfid.timeout", value); }
        }

        public static String RFIDPortName
        {
            get { return Settings.Get("rfid.PortName"); }
            set { Settings.Set("rfid.PortName", value); }
        }

        public static String RFIDPortType
        {
            get { return Settings.Get("rfid.PortType"); }
            set { Settings.Set("rfid.PortType", value); }
        }

        public static String TraceFileSize
        {
            get { return Settings.Get("trace.filesize"); }
            set { Settings.Set("trace.filesize", value); }
        }

        public static String UserId
        {
            get { return Settings.Get("UserId"); }
            set { Settings.Set("UserId", value); }
        }

        public static String Mandt
        {
            get { return Settings.Get("Mandt"); }
            set { Settings.Set("Mandt", value); }
        }

        public static String Email1
        {
            get { return Settings.Get("user.email1"); }
            set { Settings.Set("user.email", value); }
        }

        public static String Email2
        {
            get { return Settings.Get("user.email2"); }
            set { Settings.Set("user.email", value); }
        }

        public static String Pw
        {
            get { return Settings.Get("Password"); }
            set { Settings.Set("Password", value); }
        }

        public static string Protocol
        {
            get { return Settings.Get("Protocol"); }
            set { Settings.Set("Protocol", value); }
        }

        public static string Gateway
        {
            get { return Settings.Get("Gateway"); }
            set { Settings.Set("Gateway", value); }
        }

        public static string GatewayPort
        {
            get { return Settings.Get("GatewayPort"); }
            set { Settings.Set("GatewayPort", value); }
        }

        public static string GatewayServiceSync
        {
            get { return Settings.Get("GatewayServiceSync"); }
            set { Settings.Set("GatewayServiceSync", value); }
        }

        public static string GatewayServiceInfo
        {
            get { return Settings.Get("GatewayServiceInfo"); }
            set { Settings.Set("GatewayServiceInfo", value); }
        }

        public static string FlagMassdata
        {
            get { return Settings.Get("sync.massData"); }
            set { Settings.Set("sync.massData", value); }
        }

        public static string FlagCustomizing
        {
            get { return Settings.Get("sync.reqCust"); }
            set { Settings.Set("sync.reqCust", value); }
        }

        public static string FlagDocuments
        {
            get { return Settings.Get("sync.reqDoc"); }
            set { Settings.Set("sync.reqDoc", value); }
        }

        public static string FlagMasterdata
        {
            get { return Settings.Get("sync.reqMD"); }
            set { Settings.Set("sync.reqMD", value); }
        }

        public static string FlagAutoLogon
        {
            get { return Settings.Get("user.AutoLogon"); }
            set { Settings.Set("user.AutoLogon", value); }
        }

        public static string FlagTestmode
        {
            get { return Settings.Get("sync.testMode"); }
            set { Settings.Set("sync.testMode", value); }
        }
        public static string FlagConfirmation
        {
            get { return Settings.Get("sync.confirmation"); }
            set { Settings.Set("sync.confirmation", value); }
        }
        public static string FlagBackgroundSync
        {
            get { return Settings.Get("sync.backgroundSync"); }
            set { Settings.Set("sync.backgroundSync", value); }
        }

        public static string TestMode
        {
            get { return Settings.Get("sync.testMode"); }
            set { Settings.Set("sync.testMode", value); }
        }
        public static string FlagSyncAtStartup
        {
            get { return Settings.Get("sync.SyncAtStartup"); }
            set { Settings.Set("sync.SyncAtStartup", value); }
        }

        public static string FlagShowOrderReportOnSave
        {
            get { return Settings.Get("ShowOrderReportOnSave"); }
            set { Settings.Set("ShowOrderReportOnSave", value); }
        }

        public static string PresetNetworkAddress
        {
            get { return Settings.Get("PresetNetworkAddress"); }
            set { Settings.Set("PresetNetworkAddress", value); }
        }

        public static string PresetVPNAddress
        {
            get { return Settings.Get("PresetVPNAddress"); }
            set { Settings.Set("PresetVPNAddress", value); }
        }

        public static string TimePrecision
        {
            get { return Settings.Get("TimePrecision"); }
            set { Settings.Set("TimePrecision", value); }
        }

        public static Boolean RfidEnabled
        {
            get { return Settings.Get("RfidEnabled").Equals("true"); }
            set { Settings.Set("RfidEnabled", value.ToString()); }
        }

        public static String DeviceId
        {
            get { return Settings.Get("System.DeviceId"); }
            set { Settings.Set("System.DeviceId", value); }
        }



        /// <summary>
        /// Defines the Level for displaying messages to the user
        /// Messagelevels are:
        ///  - 0: No messages are displayed
        ///  - 1: only informatory messages are displayed (type I)
        ///  - 2: All messages but Exceptions (type E) are displayed 
        ///  - 3: Debugging mode, all Mmssages are displayed
        /// </summary>
        public static string MessageDisplayLevel
        {
            get { return Settings.Get("MessageDisplayLevel"); }
            set { Settings.Set("MessageDisplayLevel", value); }
        }

        public static String DefaultApplication
        {
            get { return Settings.Get("application.default"); }
        }

        public static int[] Color1
        {
            get
            {
                if (Settings == null)
                    return null;

                String strColor = Settings.Get("Color1");
                //strColor.Split(',');
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        public static int[] Color2
        {
            get
            {
                String strColor = Settings.Get("Color2");
                //strColor.Split(',');
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        public static int[] Color3
        {
            get
            {
                String strColor = Settings.Get("Color3");
                //strColor.Split(',');
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        #endregion
    }
}