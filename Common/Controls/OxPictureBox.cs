﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace oxmc.Common.Controls
{
    public partial class OxPictureBox : PictureBox, ISupportInitialize
    {
        public object Cursor;

        public OxPictureBox()
        {
            InitializeComponent();
        }

        #region ISupportInitialize Members
        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }
        #endregion

        public OxPictureBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnClick(EventArgs e)
        {
            Monitor.Enter(LockHelper.Locker);
            base.OnClick(e);
            Monitor.Exit(LockHelper.Locker);
        }
    }
}
