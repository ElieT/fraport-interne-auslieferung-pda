﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace oxmc.Common.Controls
{
    public partial class OxTextBox : TextBox
    {
        /// <summary>
        /// The default length of a textfield. Since most textfields in the backend are limited to 40 characters, the default length is also 40
        /// </summary>
        private const int Defaultlength = 40;

        private Color _defaultBackColor;
        private Color _defaultForeColor;


        public OxTextBox()
        {
            InitializeComponent();
            base.MaxLength = Defaultlength;
            _defaultBackColor = Color.White;
            _defaultBackColor = Color.Black;
        }

        public OxTextBox(IContainer container)
        {
            container.Add(this);
            base.MaxLength = Defaultlength;

            InitializeComponent();
        }

        /// <summary>
        /// Restricts the entry of characters so that no ; or | can be entered
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            if (e.KeyChar == ';' || e.KeyChar == '|')
            {
                //Swallow the invalid Char
                e.Handled = true;
            }
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            if (!Enabled)
            {
                BackColor = Color.FromArgb(240, 240, 240);
                ForeColor = Color.FromArgb(109, 109, 109);
            }
            else
            {
                BackColor = Color.White;
                ForeColor = Color.Black;
            }
        }
    }
}
