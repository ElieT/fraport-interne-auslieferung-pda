﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCharact
    {
        public string Clint { get; set; }

        public string Atinn { get; set; }

        public string Adzhl { get; set; }

        public string Atnam { get; set; }

        public string Atfor { get; set; }

        public string Anzst { get; set; }

        public string Anzdz { get; set; }

        public string Atkle { get; set; }

        public string Aterf { get; set; }

        public string Atein { get; set; }

        public string Atint { get; set; }

        public string Atson { get; set; }

        public string Atinp { get; set; }

        public string Atvie { get; set; }

        public string Atbez { get; set; }
    }
}