﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BasePushMessage
    {
        protected String MESSAGEID, MESSAGE;
        protected DateTime DATETIME;

        public string MessageId
        {
            get { return MESSAGEID; }
            set { MESSAGEID = value; }
        }

        public DateTime MessageDate
        {
            get { return DATETIME; }
            set { DATETIME = value; }
        }

        public string Message
        {
            get { return MESSAGE; }
            set { MESSAGE = value; }
        }

        public BasePushMessage(string message, DateTime datetime)
        {
            MESSAGE = message;
            DATETIME = datetime;
        }
        public BasePushMessage()
        {
        }
    }
}
