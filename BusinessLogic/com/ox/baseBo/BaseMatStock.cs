﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMatStock
    {
        protected string MATNR, MEINS, MAKTX, WERKS, LGORT, LABST;

        public string Labst
        {
            get { return LABST; }
            set { LABST = value; }
        }

        public string Lgort
        {
            get { return LGORT; }
            set { LGORT = value; }
        }

        public string Werks
        {
            get { return WERKS; }
            set { WERKS = value; }
        }

        public string Matnr
        {
            get { return MATNR; }
            set { MATNR = value; }
        }

        public string Meins
        {
            get { return MEINS; }
            set { MEINS = value; }
        }

        public string Maktx
        {
            get { return MAKTX; }
            set { MAKTX = value; }
        }
    }
}
