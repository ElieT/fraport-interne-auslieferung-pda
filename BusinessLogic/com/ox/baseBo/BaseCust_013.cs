﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCust_013
    {
        public String MobileMandt { get; set; }

        public String Werks { get; set; }

        public String Katalogart { get; set; }

        public String Auswahlmge { get; set; }

        public String Funktion { get; set; }

        public String Codegruppe { get; set; }

        public String Code { get; set; }
    }
}
