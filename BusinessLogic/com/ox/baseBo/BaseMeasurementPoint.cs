﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMeasurementPoint : OxBusinessObject
    {
        public string Point { get; set; }

        public string Mpobj { get; set; }

        public string Psort { get; set; }

        public string Pttxt { get; set; }

        public string Locas { get; set; }

        public string Atinn { get; set; }

        public string Mrmin { get; set; }

        public string Mrmax { get; set; }

        public string Mrngu { get; set; }

        public string Desir { get; set; }

        public string Indct { get; set; }

        public string Indrv { get; set; }

        public string Last_rec { get; set; }

        public string Last_unit { get; set; }

        public string GetValueByString(String field)
        {
            var retValue = "";

            switch (field.ToUpper())
            {
                case "POINT":
                    retValue = Point;
                    break;
                case "MPOBJ":
                    retValue = Mpobj;
                    break;
                case "PSORT":
                    retValue = Psort;
                    break;
                case "PTTXT":
                    retValue = Pttxt;
                    break;
                case "LOCAS":
                    retValue = Locas;
                    break;
                case "ATINN":
                    retValue = Atinn;
                    break;
                case "MRMIN":
                    retValue = Mrmin;
                    break;
                case "MRMAX":
                    retValue = Mrmax;
                    break;
                case "MRNGU":
                    retValue = Mrngu;
                    break;
                case "DESIR":
                    retValue = Desir;
                    break;
                case "INDCT":
                    retValue = Indct;
                    break;
                case "INDRV":
                    retValue = Indrv;
                    break;
                case "LAST_REC":
                    retValue = Last_rec;
                    break;
                case "LAST_UNIT":
                    retValue = Last_unit;
                    break;
            }
            return retValue;
        }


        public class BaseMeasurementPointSort : IComparer<MeasurementPoint>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseMeasurementPointSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(MeasurementPoint x, MeasurementPoint y)
            {
                switch (_compareField.ToLower())
                {
                    case "point":
                        if (_desc)
                            return x.Point.CompareTo(y.Point);
                        return y.Point.CompareTo(x.Point);
                    case "mpobj":
                        if (_desc)
                            return x.Mpobj.CompareTo(y.Mpobj);
                        return y.Mpobj.CompareTo(x.Mpobj);
                    case "psort":
                        if (_desc)
                            return x.Psort.CompareTo(y.Psort);
                        return y.Psort.CompareTo(x.Psort);
                    case "pttxt":
                        if (_desc)
                            return x.Pttxt.CompareTo(y.Pttxt);
                        return y.Pttxt.CompareTo(x.Pttxt);
                    case "locas":
                        if (_desc)
                            return x.Locas.CompareTo(y.Locas);
                        return y.Locas.CompareTo(x.Locas);
                    case "atinn":
                        if (_desc)
                            return x.Atinn.CompareTo(y.Atinn);
                        return y.Atinn.CompareTo(x.Atinn);
                    case "mrmin":
                        if (_desc)
                            return x.Mrmin.CompareTo(y.Mrmin);
                        return y.Mrmin.CompareTo(x.Mrmin);
                    case "mrmax":
                        if (_desc)
                            return x.Mrmax.CompareTo(y.Mrmax);
                        return y.Mrmax.CompareTo(x.Mrmax);
                    case "mrngu":
                        if (_desc)
                            return x.Mrngu.CompareTo(y.Mrngu);
                        return y.Mrngu.CompareTo(x.Mrngu);
                    case "desir":
                        if (_desc)
                            return x.Desir.CompareTo(y.Desir);
                        return y.Desir.CompareTo(x.Desir);
                    case "indct":
                        if (_desc)
                            return x.Indct.CompareTo(y.Indct);
                        return y.Indct.CompareTo(x.Indct);
                    case "indrv":
                        if (_desc)
                            return x.Indrv.CompareTo(y.Indrv);
                        return y.Indrv.CompareTo(x.Indrv);
                    case "lastRec":
                        if (_desc)
                            return x.Last_rec.CompareTo(y.Last_rec);
                        return y.Last_rec.CompareTo(x.Last_rec);
                    case "lastUnit":
                        if (_desc)
                            return x.Last_unit.CompareTo(y.Last_unit);
                        return y.Last_unit.CompareTo(x.Last_unit);
                    default:
                        return (x).Point.CompareTo(y.Point);
                }
            }
        }
    }
}