﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMatConfManager
    {
        public static List<MatConf> GetMatConf(Order order, Boolean inTransaction)
        {
            List<MatConf> _matconfs = new List<MatConf>();
            try
            {
                var sqlStmt = "select * from D_MATCONF WHERE aufnr = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.aufnr);
                List<Dictionary<string, string>> records; 
                if(inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var _matConf = new MatConf();
                    DateTime date = DateTime.Now;
                    _matConf.Aufnr = (String)rdr["AUFNR"];
                    DateTimeHelper.getDate(rdr["ISDD"], out date);
                    _matConf.MoveType = (String)rdr["MOVE_TYPE"];
                    _matConf.SpecStock = (String)rdr["SPEC_STOCK"];
                    _matConf.StckType = (String)rdr["STCK_TYPE"];
                    _matConf.Plant = (String)rdr["PLANT"];
                    _matConf.Storloc = (String)rdr["STORLOC"];
                    _matConf.Customer = (String)rdr["CUSTOMER"];
                    _matConf.Matnr = (String)rdr["MATNR"];
                    _matConf.Quantity = (String)rdr["QUANTITY"];
                    _matConf.Unit = (String)rdr["UNIT"];
                    _matConf.Rsnum = (String)rdr["RSNUM"];
                    _matConf.Rspos = (String)rdr["RSPOS"];
                    _matConf.Kzear = (String)rdr["KZEAR"];
                    _matConf.Batch = (String)rdr["BATCH"];
                    _matConf.Equnr = (String)rdr["EQUNR"];
                    _matConf.Mblnr = (String)rdr["MBLNR"];
                    _matConf.Mjahr = (String)rdr["MJAHR"];
                    _matConf.UpdFlag = (String)rdr["UPDFLAG"];
                    _matconfs.Add(_matConf);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _matconfs;
        }
        public static void SaveMatConf(MatConf matConf)
        {
            try
            {
                int intDbKey = -1;
                String sqlStmt = "SELECT MAX(rowid) + 1 FROM D_MATCONF";
                String result = "";
                result = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt))[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;

                matConf.Mblnr = intDbKey.ToString();

                // Insert Data
                int dbResult = -1;
                sqlStmt = "INSERT INTO [D_MATCONF] " +
                                 "(MANDT, USERID, AUFNR, ISDD, MOVE_TYPE, SPEC_STOCK, STCK_TYPE, PLANT, " +
                                 "STORLOC, CUSTOMER, MATNR, QUANTITY, UNIT, RSNUM, RSPOS, KZEAR, " +
                                 "BATCH, EQUNR, MBLNR, MJAHR, UPDFLAG, MOBILEKEY) " +
                                 "Values (@MANDT, @USERID, @AUFNR, @ISDD, @MOVE_TYPE, @SPEC_STOCK, @STCK_TYPE, @PLANT, " +
                                 "@STORLOC, @CUSTOMER, @MATNR, @QUANTITY, @UNIT, @RSNUM, @RSPOS, @KZEAR, " +
                                 "@BATCH, @EQUNR, @MBLNR, @MJAHR, @UPDFLAG, @MOBILEKEY)";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@AUFNR", matConf.Aufnr);
                cmd.Parameters.AddWithValue("@ISDD", DateTimeHelper.GetDateString(matConf.Isdd));
                cmd.Parameters.AddWithValue("@MOVE_TYPE", matConf.MoveType);
                cmd.Parameters.AddWithValue("@SPEC_STOCK", matConf.SpecStock);
                cmd.Parameters.AddWithValue("@STCK_TYPE", matConf.StckType);
                cmd.Parameters.AddWithValue("@PLANT", matConf.Plant);
                cmd.Parameters.AddWithValue("@STORLOC", matConf.Storloc);
                cmd.Parameters.AddWithValue("@CUSTOMER", matConf.Customer);
                cmd.Parameters.AddWithValue("@MATNR", matConf.Matnr);
                cmd.Parameters.AddWithValue("@QUANTITY", matConf.Quantity);
                cmd.Parameters.AddWithValue("@UNIT", matConf.Unit);
                cmd.Parameters.AddWithValue("@RSNUM", matConf.Rsnum);
                cmd.Parameters.AddWithValue("@RSPOS", matConf.Rspos);
                cmd.Parameters.AddWithValue("@KZEAR", matConf.Kzear);
                cmd.Parameters.AddWithValue("@BATCH", matConf.Batch);
                cmd.Parameters.AddWithValue("@EQUNR", matConf.Equnr);
                cmd.Parameters.AddWithValue("@MBLNR", matConf.Mblnr);
                cmd.Parameters.AddWithValue("@MJAHR", matConf.Mjahr);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@MOBILEKEY", matConf.MobileKey);

                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            //todo:   return dbIdInsplot;
        }

        public static void DeleteMatConf(MatConf matConf)
        {
            try
            {
                int dbResult = -1;
                String sqlStmt = "DELETE FROM [D_MATCONF] WHERE MANDT = @MANDT AND USERID = @USERID AND MBLNR = @MBLNR AND MJAHR = @MJAHR";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@MBLNR", matConf.Mblnr);
                cmd.Parameters.AddWithValue("@MJAHR", matConf.Mjahr);
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

    }
}
