﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseChecklistResults
    {
        //public String MANDT, USERID, PLTY, PLNNR, PLNKN, KZEINSTELL, MERKNR, ZAEHL, KURZTEXT, STATUS, VORNR, MATNR, WERK, TPLNR, CODE_GRP, CODE;
        //public String mandt, userid, matnr, werk, code_grp, code, tplnr, inspoper, inspchar, status;

        public DateTime END_DATE,
                        END_TIME,
                        START_DATE,
                        START_TIME;

        public Object Lbl { get; set; }

        public Object Obj { get; set; }

        public string Kurztext { get; set; }

        public string Closed { get; set; }

        public string CodeGrp1 { get; set; }

        public string CodeGrp2 { get; set; }

        public string Code1 { get; set; }

        public string Code2 { get; set; }

        public string Inschar { get; set; }

        public string Insoper { get; set; }

        public string Opktxt { get; set; }

        public string Inspector { get; set; }

        public string Insplot { get; set; }

        public string Matnr { get; set; }

        public string MeanValue { get; set; }

        public string Qmnum { get; set; }

        public string Remark { get; set; }

        public string Status { get; set; }

        public string Tplnr { get; set; }

        public string Equnr { get; set; }

        public string Werk { get; set; }

        public string MobileKey { get; set; }

        public string Updateflag { get; set; }

        public string Evaluate { get; set; }

        public DateTime EndDate
        {
            get { return END_DATE; }
            set { END_DATE = value; }
        }

        public DateTime EndTime
        {
            get { return END_TIME; }
            set { END_TIME = value; }
        }

        public DateTime StartDate
        {
            get { return START_DATE; }
            set { START_DATE = value; }
        }

        public DateTime StartTime
        {
            get { return START_TIME; }
            set { START_TIME = value; }
        }
    }
}