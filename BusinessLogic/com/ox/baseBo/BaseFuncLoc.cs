﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseFuncLoc : OxBusinessObject
    {
        protected String IWERK;
        protected String PLTXT;
        protected String SUBMT;

        protected String TPLNR,
                       RBNR,
                       POSNR,
                       EQART,
                       INVNR,
                       HERST,
                       HERLD,
                       BAUJJ,
                       BAUMM,
                       TYPBZ,
                       STORT,
                       BEBER,
                       KOSTL,
                       FLTYP,
                       TPLMA,
                       MSGRP,
                       EQFNR,
                       OBJNR;

        public string Gewrk { get; set; }
        public string Wergw { get; set; }
        public string TxtGewrk { get; set; }
        public DateTime GwldtI { get; set; }
        public DateTime GwlenI { get; set; }
        public string Adrnr { get; set; }

        public string Objnr
        {
            get { return OBJNR; }
            set { OBJNR = value; }
        }

        public string Msgrp
        {
            get { return MSGRP; }
            set { MSGRP = value; }
        }

        protected DateTime ANSDT, DATAB;

        public string Rbnr
        {
            get { return RBNR; }
            set { RBNR = value; }
        }

        public string Posnr
        {
            get { return POSNR; }
            set { POSNR = value; }
        }

        public string Eqart
        {
            get { return EQART; }
            set { EQART = value; }
        }

        public string Herst
        {
            get { return HERST; }
            set { HERST = value; }
        }

        public string Herld
        {
            get { return HERLD; }
            set { HERLD = value; }
        }

        public string Invnr
        {
            get { return INVNR; }
            set { INVNR = value; }
        }

        public string Baujj
        {
            get { return BAUJJ; }
            set { BAUJJ = value; }
        }

        public string Baumm
        {
            get { return BAUMM; }
            set { BAUMM = value; }
        }

        public string Typbz
        {
            get { return TYPBZ; }
            set { TYPBZ = value; }
        }

        public string Stort
        {
            get { return STORT; }
            set { STORT = value; }
        }

        public string Tplma
        {
            get { return TPLMA; }
            set { TPLMA = value; }
        }

        public string Kostl
        {
            get { return KOSTL; }
            set { KOSTL = value; }
        }


        public string Beber
        {
            get { return BEBER; }
            set { BEBER = value; }
        }

        public string Fltyp
        {
            get { return FLTYP; }
            set { FLTYP = value; }
        }

        public DateTime Datab
        {
            get { return DATAB; }
            set { DATAB = value; }
        }

        public DateTime Ansdt
        {
            get { return ANSDT; }
            set { ANSDT = value; }
        }

        public string Eqfnr
        {
            get { return EQFNR; }
            set { EQFNR = value; }
        }

        public string Submt
        {
            get { return SUBMT; }
            set { SUBMT = value; }
        }

        public string Pltxt
        {
            get { return PLTXT; }
            set { PLTXT = value; }
        }

        public string Tplnr
        {
            get { return TPLNR; }
            set { TPLNR = value; }
        }

        public string Iwerk
        {
            get { return IWERK; }
            set { IWERK = value; }
        }

        public string GetValueByString(String field)
        {
            var retValue = "";

            switch (field.ToUpper())
            {
                case "IWERK":
                    retValue = IWERK;
                    break;
                case "PLTXT":
                    retValue = PLTXT;
                    break;
                case "SUBMT":
                    retValue = SUBMT;
                    break;
                case "TPLNR":
                    retValue = TPLNR;
                    break;
                case "RBNR":
                    retValue = RBNR;
                    break;
                case "POSNR":
                    retValue = POSNR;
                    break;
                case "EQART":
                    retValue = EQART;
                    break;
                case "INVNR":
                    retValue = INVNR;
                    break;
                case "HERST":
                    retValue = HERST;
                    break;
                case "HERLD":
                    retValue = HERLD;
                    break;
                case "BAUJJ":
                    retValue = BAUJJ;
                    break;
                case "BAUMM":
                    retValue = BAUMM;
                    break;
                case "TYPBZ":
                    retValue = TYPBZ;
                    break;
                case "STORT":
                    retValue = STORT;
                    break;
                case "BEBER":
                    retValue = BEBER;
                    break;
                case "KOSTL":
                    retValue = KOSTL;
                    break;
                case "FLTYP":
                    retValue = FLTYP;
                    break;
                case "TPLMA":
                    retValue = TPLMA;
                    break;
                case "MSGRP":
                    retValue = MSGRP;
                    break;
                case "EQFNR":
                    retValue = EQFNR;
                    break;
                case "OBJNR":
                    retValue = OBJNR;
                    break;
            }
            return retValue;
        }
        public class BaseFuncLocSort : IComparer<FuncLoc>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseFuncLocSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(FuncLoc x, FuncLoc y)
            {
                switch (_compareField.ToUpper())
                {
                    case "IWERK":
                        if (_desc)
                            return x.IWERK.CompareTo(y.IWERK);
                        return y.IWERK.CompareTo(x.IWERK);
                    case "PLTXT":
                        if (_desc)
                            return x.PLTXT.CompareTo(y.PLTXT);
                        return y.PLTXT.CompareTo(x.PLTXT);
                    case "SUBMT":
                        if (_desc)
                            return x.SUBMT.CompareTo(y.SUBMT);
                        return y.SUBMT.CompareTo(x.SUBMT);
                    case "TPLNR":
                        if (_desc)
                            return x.TPLNR.CompareTo(y.TPLNR);
                        return y.TPLNR.CompareTo(x.TPLNR);
                    case "RBNR":
                        if (_desc)
                            return x.RBNR.CompareTo(y.RBNR);
                        return y.RBNR.CompareTo(x.RBNR);
                    case "POSNR":
                        if (_desc)
                            return x.POSNR.CompareTo(y.POSNR);
                        return y.POSNR.CompareTo(x.POSNR);
                    case "EQART":
                        if (_desc)
                            return x.EQART.CompareTo(y.EQART);
                        return y.EQART.CompareTo(x.EQART);
                    case "INVNR":
                        if (_desc)
                            return x.INVNR.CompareTo(y.INVNR);
                        return y.INVNR.CompareTo(x.INVNR);
                    case "HERST":
                        if (_desc)
                            return x.HERST.CompareTo(y.HERST);
                        return y.HERST.CompareTo(x.HERST);
                    case "HERLD":
                        if (_desc)
                            return x.HERLD.CompareTo(y.HERLD);
                        return y.HERLD.CompareTo(x.HERLD);
                    case "BAUJJ":
                        if (_desc)
                            return x.BAUJJ.CompareTo(y.BAUJJ);
                        return y.BAUJJ.CompareTo(x.BAUJJ);
                    case "BAUMM":
                        if (_desc)
                            return x.BAUMM.CompareTo(y.BAUMM);
                        return y.BAUMM.CompareTo(x.BAUMM);
                    case "TYPBZ":
                        if (_desc)
                            return x.TYPBZ.CompareTo(y.TYPBZ);
                        return y.TYPBZ.CompareTo(x.TYPBZ);
                    case "STORT":
                        if (_desc)
                            return x.STORT.CompareTo(y.STORT);
                        return y.STORT.CompareTo(x.STORT);
                    case "BEBER":
                        if (_desc)
                            return x.BEBER.CompareTo(y.BEBER);
                        return y.BEBER.CompareTo(x.BEBER);
                    case "KOSTL":
                        if (_desc)
                            return x.KOSTL.CompareTo(y.KOSTL);
                        return y.KOSTL.CompareTo(x.KOSTL);
                    case "FLTYP":
                        if (_desc)
                            return x.FLTYP.CompareTo(y.FLTYP);
                        return y.FLTYP.CompareTo(x.FLTYP);
                    case "TPLMA":
                        if (_desc)
                            return x.TPLMA.CompareTo(y.TPLMA);
                        return y.TPLMA.CompareTo(x.TPLMA);
                    case "MSGRP":
                        if (_desc)
                            return x.MSGRP.CompareTo(y.MSGRP);
                        return y.MSGRP.CompareTo(x.MSGRP);
                    case "EQFNR":
                        if (_desc)
                            return x.EQFNR.CompareTo(y.EQFNR);
                        return y.EQFNR.CompareTo(x.EQFNR);
                    case "OBJNR":
                        if (_desc)
                            return x.OBJNR.CompareTo(y.OBJNR);
                        return y.OBJNR.CompareTo(x.OBJNR);
                    default:
                        return (x).IWERK.CompareTo(y.IWERK);
                }
            }
        }
    }
}