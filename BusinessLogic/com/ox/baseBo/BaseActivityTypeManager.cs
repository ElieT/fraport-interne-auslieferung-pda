﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseActivityTypeManager
    {
        public static Dictionary<String, ActivityType> GetActivityTypes(String arbpl)
        {
            var retVal = new Dictionary<String, ActivityType>();
            try
            {
                var sqlStmt = "SELECT * FROM C_ACTIVITY_TYPE WHERE ARBPL = @ARBPL";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@ARBPL", arbpl);
                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach(var ret in result)
                {
                    var aT = new ActivityType();
                    aT.Arbpl = ret["ARBPL"];
                    aT.Kokrs = ret["KOKRS"];
                    aT.Lstar = ret["LSTAR"];
                    aT.Werks = ret["WERKS"];
                    aT.Ktext = ret["KTEXT"];
                    retVal.Add(ret["LSTAR"], aT);
                }
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
    }
}
