﻿using System;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOrdComponents : OxBusinessObject
    {
        protected String _aufnr;
        protected String _rsnum;
        protected String _rspos;
        protected String _xloek;
        protected String _kzear;
        protected String _matnr;
        protected String _werks;
        protected String _lgort;
        protected String _bdmng;
        protected String _meins;
        protected String _enmng;
        protected String _erfmg;
        protected String _erfme;
        protected String _bwart;
        protected String _sgtxt;
        protected String _vornr;
        protected String _locEnmng;
        protected String _mobileKey;
        protected String _postp;
        protected String _vmeng;
        protected String _updflag;

        public BaseOrdComponents()
        {

        }

        public BaseOrdComponents(Order order, OrdOperation oper)
        {
            _aufnr = order.Aufnr;
            _vornr = oper.Vornr;
        }

        public string Aufnr
        {
            get { return _aufnr; }
            set { _aufnr = value; }
        }

        public string Rsnum
        {
            get { return _rsnum; }
            set { _rsnum = value; }
        }

        public string Rspos
        {
            get { return _rspos; }
            set { _rspos = value; }
        }

        public string Xloek
        {
            get { return _xloek; }
            set { _xloek = value; }
        }

        public string Kzear
        {
            get { return _kzear; }
            set { _kzear = value; }
        }

        public string Matnr
        {
            get { return _matnr; }
            set { _matnr = value; }
        }

        public string Werks
        {
            get { return _werks; }
            set { _werks = value; }
        }

        public string Lgort
        {
            get { return _lgort; }
            set { _lgort = value; }
        }

        public string Bdmng
        {
            get { return _bdmng; }
            set
            {
                if (value.EndsWith(".000"))
                    _bdmng = value.Substring(0, value.IndexOf(".000"));
                else
                    _bdmng = value;
            }
        }

        public string Meins
        {
            get { return _meins; }
            set { _meins = value; }
        }

        public string Enmng
        {
            get { return _enmng; }
            set
            {
                if (value.EndsWith(".000"))
                    _enmng = value.Substring(0, value.IndexOf(".000"));
                else
                    _enmng = value;
            }
        }

        public string Erfmg
        {
            get { return _erfmg; }
            set
            {
                if (value.EndsWith(".000"))
                    _erfmg = value.Substring(0, value.IndexOf(".000"));
                else
                    _erfmg = value;
            }
        }

        public string Erfme
        {
            get { return _erfme; }
            set { _erfme = value; }
        }

        public string Bwart
        {
            get { return _bwart; }
            set { _bwart = value; }
        }

        public string Sgtxt
        {
            get { return _sgtxt; }
            set { _sgtxt = value; }
        }

        public string Vornr
        {
            get { return _vornr; }
            set { _vornr = value; }
        }
        
        public string Vmeng
        {
            get { return _vmeng; }
            set { _vmeng = value; }
        }

        public string Postp
        {
            get { return _postp; }
            set { _postp = value; }
        }

        public string Updflag
        {
            get { return _updflag; }
            set { _updflag = value; }
        }

        public string LocEnmng
        {
            get { return _locEnmng; }
            set
            {
                if (value.EndsWith(".000"))
                    _locEnmng = value.Substring(0, value.IndexOf(".000"));
                else
                    _locEnmng = value;
            }
        }

        public string MobileKey
        {
            get { return _mobileKey; }
            set { _mobileKey = value; }
        }
    }
}
