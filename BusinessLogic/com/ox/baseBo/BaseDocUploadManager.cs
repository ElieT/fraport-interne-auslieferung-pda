﻿using System;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDocUploadManager
    {
        public static void SaveDoc(DocUpload _doc)
        {
            try
            {

                int intDbKey = 1;
                var sqlStmt = "SELECT MAX(CAST(rowid AS Int))+1 FROM S_DOCUPLOAD";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;

                _doc.Id = intDbKey.ToString();
                //Check if file already exists in Database. If it does, delete all entries in the database before creating new ones
                sqlStmt = "SELECT ID FROM S_DOCUPLOAD WHERE FILENAME = @FILENAME";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@FILENAME", _doc.Filename);
                records = OxDbManager.GetInstance().FeedTransaction(cmd);
                if (records.Count > 0)
                {
                    _doc.Id = records[0]["ID"];
                }

                //Create entries for the file in the Database
                sqlStmt = "INSERT OR REPLACE INTO [S_DOCUPLOAD] " +
                         "(MANDT, USERID, ID, DOC_TYPE, DOC_SIZE, REF_TYPE, FILE_LOCATION, REF_OBJECT, DESCRIPTION, SPRAS, FILENAME, EMAIL, UPDFLAG, MOBILEKEY, RECORDNAME, TIMESTAMP, ORG_FILENAME, RECORDNAME_BIN) " +
                         "Values (@MANDT, @USERID, @ID, @DOC_TYPE, @DOC_SIZE, @REF_TYPE, @FILE_LOCATION, @REF_OBJECT, @DESCRIPTION, @SPRAS, @FILENAME, @EMAIL, @UPDFLAG, @MOBILEKEY, @RECORDNAME, @TIMESTAMP, @ORG_FILENAME, @RECORDNAME_BIN)";

                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", _doc.Id);
                cmd.Parameters.AddWithValue("@DOC_TYPE", _doc.DocType.ToUpper());
                cmd.Parameters.AddWithValue("@DOC_SIZE", _doc.DocSize);
                cmd.Parameters.AddWithValue("@FILE_LOCATION", _doc.FileLocation);
                cmd.Parameters.AddWithValue("@RECORDNAME", _doc.Recordname);
                cmd.Parameters.AddWithValue("@RECORDNAME_BIN", _doc.RecordnameBin);
                cmd.Parameters.AddWithValue("@REF_TYPE", _doc.RefType);
                cmd.Parameters.AddWithValue("@REF_OBJECT", _doc.RefObject);
                cmd.Parameters.AddWithValue("@DESCRIPTION", _doc.Description);
                cmd.Parameters.AddWithValue("@SPRAS", _doc.Spras);
                cmd.Parameters.AddWithValue("@FILENAME", _doc.Filename);
                cmd.Parameters.AddWithValue("@EMAIL", _doc.Email);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@MOBILEKEY", _doc.MobileKey);
                cmd.Parameters.AddWithValue("@ORG_FILENAME", _doc.OrgFilename);
                cmd.Parameters.AddWithValue("@TIMESTAMP", _doc.Timestamp.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
                //cmd.Parameters.AddWithValue("@SYNCTIMESTAMP", _doc.Synctimestamp.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
                OxDbManager.GetInstance().FeedTransaction(cmd);

                sqlStmt = "INSERT OR REPLACE INTO [D_DOCD] " +
                          "(MANDT, USERID, ID, LINENUMBER, LINE, UPDFLAG, MOBILEKEY) " +
                          "Values (@MANDT, @USERID, @ID, @LINENUMBER, @LINE, @UPDFLAG, @MOBILEKEY)";
                cmd = new SQLiteCommand(sqlStmt);
                foreach (var item in _doc.Docds)
                {
                    //Thread.Sleep(10000);
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@ID", _doc.Id);
                    cmd.Parameters.AddWithValue("@LINENUMBER", item.Linenumber);
                    cmd.Parameters.AddWithValue("@LINE", item.Line);
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                    cmd.Parameters.AddWithValue("@MOBILEKEY", _doc.MobileKey);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
        }


        public static void DeleteDoc(DocUpload docUpload, bool inTransaction)
        {
            DeleteDoc(docUpload.Id, docUpload.Filename, docUpload.FileLocation, docUpload.OrgFilename, inTransaction);
        }

        public static void DeleteDoc(String id, string path, string fileLocation, string orgFilename, bool inTransaction)
        {
            try
            {
                var sqlStmt = "DELETE FROM D_DOCD " +
                              "WHERE MANDT = @MANDT AND " +
                              "USERID = @USERID AND " +
                              "ID = @ID AND (UPDFLAG = 'I' OR UPDFLAG = 'A' OR UPDFLAG = 'X')";

                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", id);
                OxDbManager.GetInstance().FeedTransaction(cmd);


                sqlStmt = "DELETE FROM S_DOCUPLOAD " +
                              "WHERE MANDT = @MANDT AND " +
                              "USERID = @USERID AND " +
                              "ID = @ID AND (UPDFLAG = 'I' OR UPDFLAG = 'A' OR UPDFLAG = 'X')";

                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", id);
                OxDbManager.GetInstance().FeedTransaction(cmd);

                string filename = path;

                if (File.Exists(AppConfig.UserDirectory + "\\data\\" + filename))
                {
                    FileManager.DeleteFile(AppConfig.UserDirectory + "\\data\\" + filename);
                }
                if (File.Exists(AppConfig.UserDirectory + "\\data\\" + orgFilename))
                {
                    FileManager.DeleteFile(AppConfig.UserDirectory + "\\data\\" + orgFilename);
                }
                if (File.Exists(fileLocation))
                {
                    FileManager.DeleteFile(fileLocation);
                }
                if (File.Exists(AppConfig.UserDirectory + "\\temp\\" + filename))
                {
                    FileManager.DeleteFile(AppConfig.UserDirectory + "\\temp\\" + filename);
                }
                if (File.Exists(AppConfig.UserDirectory + "\\temp\\" + orgFilename))
                {
                    FileManager.DeleteFile(AppConfig.UserDirectory + "\\temp\\" + orgFilename);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                if (!inTransaction)
                    OxDbManager.GetInstance().CommitTransaction();
            }
        }

        public static void DeleteDocsForObject(string refType, string refobj, bool inTransaction)
        {
            try
            {
                var sqlStmt = "SELECT ID, FILE_LOCATION FROM S_DOCUPLOAD " +
                    "WHERE REF_TYPE = @REF_TYPE AND REF_OBJECT = @REF_OBJECT AND (UPDFLAG = 'I' OR UPDFLAG = 'A')";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@REF_TYPE", refType);
                cmd.Parameters.AddWithValue("@REF_OBJECT", refobj);

                var results = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (var ret in results)
                {
                    var id = ret["ID"];
                    var path = ret["FILE_LOCATION"];

                    sqlStmt = "DELETE FROM D_DOCD " +
                              "WHERE MANDT = @MANDT AND " +
                              "USERID = @USERID AND " +
                              "ID = @ID";
                    cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@ID", id);
                    OxDbManager.GetInstance().FeedTransaction(cmd);


                    sqlStmt = "DELETE FROM S_DOCUPLOAD " +
                              "WHERE MANDT = @MANDT AND " +
                              "USERID = @USERID AND " +
                              "ID = @ID";
                    cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@ID", id);
                    OxDbManager.GetInstance().FeedTransaction(cmd);


                    if (File.Exists(AppConfig.UserDirectory + "\\Documents\\" + path))
                    {
                        FileManager.DeleteFile(AppConfig.UserDirectory + "\\Documents\\" + path);
                    }
                    else if (File.Exists(AppConfig.UserDirectory + "\\" + path))
                    {
                        FileManager.DeleteFile(AppConfig.UserDirectory + "\\" + path);
                    }
                    else if (File.Exists(path))
                    {
                        FileManager.DeleteFile(path);
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
            finally
            {
                if (!inTransaction)
                    OxDbManager.GetInstance().CommitTransaction();
            }
        }

        public static List<DocUpload> GetDocUploadList(String refType, String refObject)
        {
            return GetDocUploadList(refType, refObject, "");
        }

        public static List<DocUpload> GetDocUploadList(String refType, String refObject, String filetype)
        {
            var docList = new List<DocUpload>();
            try
            {
                var sqlStmt = "SELECT * " +
                              "FROM " +
                              "S_DOCUPLOAD " +
                              "WHERE  REF_TYPE = @REF_TYPE AND REF_OBJECT = @REF_OBJECT";
                if (!String.IsNullOrEmpty(filetype))
                {
                    sqlStmt += " AND DOC_TYPE = '" + filetype + "'";
                }

                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@REF_TYPE", refType);
                cmd.Parameters.AddWithValue("@REF_OBJECT", refObject);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();

                foreach (var rdr in records)
                {
                    var docitem = new DocUpload();
                    docitem.Id = rdr["ID"];
                    docitem.Updflag = rdr["UPDFLAG"];
                    docitem.Filename = rdr["FILENAME"];
                    docitem.DocSize = rdr["DOC_SIZE"];
                    docitem.DocType = rdr["DOC_TYPE"];
                    docitem.FileLocation = rdr["FILE_LOCATION"];
                    docitem.Email = rdr["EMAIL"];
                    docitem.Description = rdr["DESCRIPTION"];
                    docitem.MobileKey = rdr["MOBILEKEY"];
                    docitem.RefObject = rdr["REF_OBJECT"];
                    docitem.RefType = rdr["REF_TYPE"];
                    docitem.Spras = rdr["SPRAS"];
                    docitem.Recordname = rdr["RECORDNAME"];
                    docitem.RecordnameBin = rdr["RECORDNAME_BIN"];
                    docitem.OrgFilename = rdr["ORG_FILENAME"];
                    DateTime temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["TIMESTAMP"], out temp);
                    docitem.Timestamp = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["SYNCTIMESTAMP"], out temp);
                    docitem.Synctimestamp = temp;


                    docList.Add(docitem);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return docList;

        }

        public static List<DocUpload> GetDocUploadList(String filetype)
        {
            var docList = new List<DocUpload>();
            try
            {
                var sqlStmt = "SELECT * " +
                              "FROM " +
                              "S_DOCUPLOAD";
                if (!String.IsNullOrEmpty(filetype))
                {
                    sqlStmt += " WHERE DOC_TYPE = '" + filetype + "'";
                }

                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();

                foreach (var rdr in records)
                {
                    var docitem = new DocUpload();
                    docitem.Id = rdr["ID"];
                    docitem.Updflag = rdr["UPDFLAG"];
                    docitem.Filename = rdr["FILENAME"];
                    docitem.DocSize = rdr["DOC_SIZE"];
                    docitem.DocType = rdr["DOC_TYPE"];
                    docitem.FileLocation = rdr["FILE_LOCATION"];
                    docitem.Email = rdr["EMAIL"];
                    docitem.Description = rdr["DESCRIPTION"];
                    docitem.MobileKey = rdr["MOBILEKEY"];
                    docitem.RefObject = rdr["REF_OBJECT"];
                    docitem.RefType = rdr["REF_TYPE"];
                    docitem.Spras = rdr["SPRAS"];
                    docitem.Recordname = rdr["RECORDNAME"];
                    docitem.RecordnameBin = rdr["RECORDNAME_BIN"];
                    docitem.OrgFilename = rdr["ORG_FILENAME"];
                    DateTime temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["TIMESTAMP"], out temp);
                    docitem.Timestamp = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["SYNCTIMESTAMP"], out temp);
                    docitem.Synctimestamp = temp;


                    docList.Add(docitem);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return docList;

        }

        //Löscht Dateien
        public static void DeleteDocOldFiles(String id)
        {
            try
            {
                var sqlStmt = "DELETE FROM S_DOCUPLOAD " +
                              "WHERE MANDT = @MANDT AND " +
                              "USERID = @USERID AND " +
                              "ID = @ID AND UPDFLAG = 'X'";

                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", id);
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
        }

        //Öffnet Dateien
        public static void OpenDocUpload(DocUpload docUpload)
        {
            if (docUpload != null)
            {
                String tempPath = AppConfig.UserDirectory + "\\temp";
                String source = docUpload.FileLocation;
                String destination = tempPath + "\\" + docUpload.OrgFilename;

                bool tempFolderExist = System.IO.Directory.Exists(AppConfig.UserDirectory + "\\temp");

                // Create the subfolder
                if (!tempFolderExist)
                {
                    System.IO.Directory.CreateDirectory(tempPath);
                    File.Copy(source, destination, true);

                    FileManager.ExecuteFile(destination);

                    return;
                }
                else if (File.Exists(destination))
                    FileManager.ExecuteFile(destination);
                else
                {
                    File.Copy(source, destination, true);

                    if (File.Exists(destination))
                        FileManager.ExecuteFile(destination);
                }
            }
        }
    }
}
