﻿using System;
using System.Linq;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCachingManager
    {
        public static void UpdateOrderList()
        {
            try
            {
                OrderManager.OrderList = OrderManager.GetOrdersFromDb();
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void UpdateNotifList()
        {
            try
            {
                NotifManager.NotifList = NotifManager.GetNotifsFromDb();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void UpdateTimeConfList()
        {
            try
            {
                TimeConfManager.TimeConfList = TimeConfManager.GetTimeConfsFromDb();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void AddNotif(Notif notif)
        {
            var notiflist = (from n in NotifManager.NotifList
                             where n.Qmnum == notif.Qmnum
                             select n).ToList<Notif>();

            if (notiflist.Count == 1)
                notiflist[0] = notif;
            else
                NotifManager.NotifList.Add(notif);
        }


        public static void AddOrder(Order order)
        {
            var orderlist = (from o in OrderManager.OrderList
                             where o.Aufnr == order.Aufnr
                             select o).ToList();

            if (orderlist.Count == 0)
                OrderManager.OrderList.Add(order);
            if (orderlist.Count == 1)
                orderlist[0] = order;
        }

        public static void AddTimeConf(TimeConf tc)
        {
            var tcList = (from o in TimeConfManager.TimeConfList
                             where o.Aufnr == tc.Aufnr && o.Vornr == tc.Vornr && o.Rmzhl == tc.Rmzhl
                             select o).ToList();

            if (tcList.Count == 0)
                TimeConfManager.TimeConfList.Add(tc);
        }
    }
}
