﻿using System;
using System.Data.SQLite;
using System.IO;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDocManager
    {
        public static void SaveDoc(Doch _doc)
        {
            try
            {
                //var intDbKey = -1;
                //// Get Key
                //var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_DOCH";
                //var cmd = new SQLiteCommand(sqlStmt);
                //var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                //var result = records[0]["RetVal"];
                //if (result.Length > 0)
                //    intDbKey = int.Parse(result);
                //else
                //    intDbKey = 1;

                //_doc.Id = intDbKey.ToString();

                //Check if file already exists in Database. If it does, delete all entries in the database before creating new ones
                //sqlStmt = "SELECT ID FROM D_DOCH WHERE FILENAME = @FILENAME";
                //cmd = new SQLiteCommand(sqlStmt);
                //cmd.Parameters.AddWithValue("@FILENAME", _doc.Filename);
                //records = OxDbManager.GetInstance().FeedTransaction(cmd);
                //if (records.Count > 0)
                //{
                //    _doc.Id = records[0]["ID"];
                //}

                //Create entries for the file in the Database
                var sqlStmt = "INSERT OR REPLACE INTO [D_DOCH] " +
                          "(MANDT, USERID, ID, DOC_TYPE, REF_TYPE, REF_OBJECT, DESCRIPTION, SPRAS, FILENAME, EMAIL, UPDFLAG, MOBILEKEY) " +
                          "Values (@MANDT, @USERID, @ID, @DOC_TYPE, @REF_TYPE, @REF_OBJECT, @DESCRIPTION, @SPRAS, @FILENAME, @EMAIL, @UPDFLAG, @MOBILEKEY)";

                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", _doc.Id);
                cmd.Parameters.AddWithValue("@DOC_TYPE", _doc.DocType.ToUpper());
                cmd.Parameters.AddWithValue("@REF_TYPE", _doc.RefType);
                cmd.Parameters.AddWithValue("@REF_OBJECT", _doc.RefObject);
                cmd.Parameters.AddWithValue("@DESCRIPTION", _doc.Description);
                cmd.Parameters.AddWithValue("@SPRAS", _doc.Spras);
                cmd.Parameters.AddWithValue("@FILENAME", _doc.Filename);
                cmd.Parameters.AddWithValue("@EMAIL", _doc.Email);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@MOBILEKEY", _doc.MobileKey);
                OxDbManager.GetInstance().FeedTransaction(cmd);

                sqlStmt = "INSERT OR REPLACE INTO [D_DOCD] " +
                          "(MANDT, USERID, ID, LINENUMBER, LINE, UPDFLAG, MOBILEKEY) " +
                          "Values (@MANDT, @USERID, @ID, @LINENUMBER, @LINE, @UPDFLAG, @MOBILEKEY)";
                cmd = new SQLiteCommand(sqlStmt);
                foreach (var item in _doc.Docds)
                {
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@ID", _doc.Id);
                    cmd.Parameters.AddWithValue("@LINENUMBER", item.Linenumber);
                    cmd.Parameters.AddWithValue("@LINE", item.Line);
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                    cmd.Parameters.AddWithValue("@MOBILEKEY", _doc.MobileKey);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
        }

        public static void DeleteDoc(string id, string path)
        {
            try
            {
                var sqlStmt = "DELETE FROM [D_DOCD] " +
                              "WHERE MANDT = @MANDT AND " +
                              "USERID = @USERID AND " +
                              "ID = @ID";

                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", id);
                OxDbManager.GetInstance().FeedTransaction(cmd);


                sqlStmt = "DELETE FROM [D_DOCH] " +
                              "WHERE MANDT = @MANDT AND " +
                              "USERID = @USERID AND " +
                              "ID = @ID";

                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", id);
                OxDbManager.GetInstance().FeedTransaction(cmd);


                if (File.Exists(AppConfig.GetAppPath() + "\\images\\" + path))
                {
                    FileManager.DeleteFile(AppConfig.GetAppPath() + "\\images\\" + path);
                }
                else if (File.Exists(AppConfig.GetAppPath() + "\\" + path))
                {
                    FileManager.DeleteFile(AppConfig.GetAppPath() + "\\" + path);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
        }

        public static void IA_SaveDoc(IA_Doch _doc)
        {
            try
            {
                //var intDbKey = -1;
                //// Get Key
                //var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_DOCH";
                //var cmd = new SQLiteCommand(sqlStmt);
                //var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                //var result = records[0]["RetVal"];
                //if (result.Length > 0)
                //    intDbKey = int.Parse(result);
                //else
                //    intDbKey = 1;

                //_doc.Id = intDbKey.ToString();

                //Check if file already exists in Database. If it does, delete all entries in the database before creating new ones
                //sqlStmt = "SELECT ID FROM D_DOCH WHERE FILENAME = @FILENAME";
                //cmd = new SQLiteCommand(sqlStmt);
                //cmd.Parameters.AddWithValue("@FILENAME", _doc.Filename);
                //records = OxDbManager.GetInstance().FeedTransaction(cmd);
                //if (records.Count > 0)
                //{
                //    _doc.Id = records[0]["ID"];
                //}

                //Create entries for the file in the Database
                var sqlStmt = "INSERT OR REPLACE INTO [D_ZFRAPORT_IA_DOCH] " +
                          "(MANDT, USERID, ID, DOC_TYPE, REF_TYPE, REF_OBJECT, DESCRIPTION, SPRAS, FILENAME, EMAIL, UPDFLAG, DIRTY, MOBILEKEY) " +
                          "Values (@MANDT, @USERID, @ID, @DOC_TYPE, @REF_TYPE, @REF_OBJECT, @DESCRIPTION, @SPRAS, @FILENAME, @EMAIL, @UPDFLAG, @DIRTY, @MOBILEKEY)";

                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", _doc.Id);
                cmd.Parameters.AddWithValue("@DOC_TYPE", _doc.DocType.ToUpper());
                cmd.Parameters.AddWithValue("@REF_TYPE", _doc.RefType);
                cmd.Parameters.AddWithValue("@REF_OBJECT", _doc.RefObject);
                cmd.Parameters.AddWithValue("@DESCRIPTION", _doc.Description);
                cmd.Parameters.AddWithValue("@SPRAS", _doc.Spras);
                cmd.Parameters.AddWithValue("@FILENAME", _doc.Filename);
                cmd.Parameters.AddWithValue("@EMAIL", _doc.Email);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@DIRTY", "0");
                cmd.Parameters.AddWithValue("@MOBILEKEY", _doc.MobileKey);
                OxDbManager.GetInstance().FeedTransaction(cmd);

                sqlStmt = "INSERT OR REPLACE INTO [D_ZFRAPORT_IA_DOCD] " +
                          "(MANDT, USERID, ID, LINENUMBER, LINE, UPDFLAG, MOBILEKEY, DIRTY) " +
                          "Values (@MANDT, @USERID, @ID, @LINENUMBER, @LINE, @UPDFLAG, @MOBILEKEY, @DIRTY)";
                cmd = new SQLiteCommand(sqlStmt);
                foreach (var item in _doc.Docds)
                {
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@ID", _doc.Id);
                    cmd.Parameters.AddWithValue("@LINENUMBER", item.Linenumber);
                    cmd.Parameters.AddWithValue("@LINE", item.Line);
                    cmd.Parameters.AddWithValue("@DIRTY", "0");
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                    cmd.Parameters.AddWithValue("@MOBILEKEY", _doc.MobileKey);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
        }
    }
}