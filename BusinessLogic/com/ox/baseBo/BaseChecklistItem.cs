﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseChecklistItem
    {
        public string Vornr { get; set; }

        public string Vorltx { get; set; }

        public string Plnty { get; set; }

        public string Plnnr { get; set; }

        public string Plnkn { get; set; }

        public string Plnal { get; set; }

        public string HeadZaehl { get; set; }

        public string OperZaehl { get; set; }

        public string Kzeinstell { get; set; }

        public string Merknr { get; set; }

        public string Zaehl { get; set; }

        public string Gueltigab { get; set; }

        public string Kurztext { get; set; }

        public string Katab1 { get; set; }

        public string Katalgart1 { get; set; }

        public string Auswmenge1 { get; set; }

        public string Auswmgwrk1 { get; set; }

        public string Katab2 { get; set; }

        public string Katalgart2 { get; set; }

        public string Auswmenge2 { get; set; }

        public string Auswmgwrk2 { get; set; }

        public string Sollwert { get; set; }

        public string Toleranzob { get; set; }

        public string Toleranzun { get; set; }

        public string Masseinhsw { get; set; }

        public string CharType { get; set; }

        public string Werks { get; set; }

        public string HeadKtext { get; set; }

        public string Stellen { get; set; }
    }
}