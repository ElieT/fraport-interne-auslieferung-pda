﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDocd
    {
        protected String ID,
                       LINENUMBER,
                       LINE;

        public string MobileKey { get; set;  }

        public string Id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string Linenumber
        {
            get { return LINENUMBER; }
            set { LINENUMBER = value; }
        }

        public string Line
        {
            get { return LINE; }
            set { LINE = value; }
        }
    }
}
