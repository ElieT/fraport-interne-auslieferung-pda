﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCustNotifType
    {
        protected BaseCustNotifType()
        {
            
        }

        public String Qmart { get; set; }
        public String Rbnr { get; set; }
        public String Auart { get; set; }
        public String Stsma { get; set; }
        public String Fekat { get; set; }
        public String Urkat { get; set; }
        public String Makat { get; set; }
        public String Mfkat { get; set; }
        public String Otkat { get; set; }
        public String Sakat { get; set; }
        public String CreateAllowed { get; set; }
        public String Qmartx { get; set; }
    }
}
