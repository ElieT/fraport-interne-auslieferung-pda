﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMaterial  : OxBusinessObject
    {
        protected string MATNR, MEINS, MAKTX, LABST;

        public string Matnr
        {
            get { return MATNR; }
            set { MATNR = value; }
        }

        public string Meins
        {
            get { return MEINS; }
            set { MEINS = value; }
        }

        public string Maktx
        {
            get { return MAKTX; }
            set { MAKTX = value; }
        }

        public string Labst
        {
            get { return LABST; }
            set { LABST = value; }
        }

        public string GetValueByString(String field)
        {
            var retValue = "";

            switch (field.ToUpper())
            {
                case "MATNR":
                    retValue = MATNR;
                    break;
                case "MEINS":
                    retValue = MEINS;
                    break;
                case "MAKTX":
                    retValue = MAKTX;
                    break;
            }
            return retValue;
        }

        public class BaseMaterialSort : IComparer<Material>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseMaterialSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Material x, Material y)
            {
                switch (_compareField.ToUpper())
                {
                    case "MATNR":
                        if (_desc)
                            return x.Matnr.CompareTo(y.Matnr);
                        return y.Matnr.CompareTo(x.Matnr);
                    case "MEINS":
                        if (_desc)
                            return x.Meins.CompareTo(y.Meins);
                        return y.Meins.CompareTo(x.Meins);
                    case "MAKTX":
                        if (_desc)
                            return x.Maktx.CompareTo(y.Maktx);
                        return y.Maktx.CompareTo(x.Maktx);
                    default:
                        return (x).Matnr.CompareTo(y.Matnr);
                }
            }
        }
    }
}
