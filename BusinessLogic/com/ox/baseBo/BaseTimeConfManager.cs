﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseTimeConfManager
    {
        protected static List<TimeConf> _timeConfList;

        public static List<TimeConf> TimeConfList
        {
            get
            {
                if (_timeConfList == null)
                    CachingManager.UpdateTimeConfList();
                return _timeConfList;
            }
            set { _timeConfList = value; }
        }

        public static List<TimeConf> GetTimeConfsFromDb()
        {
            var retVal = new List<TimeConf>();
            try
            {
                var sqlStmt = "SELECT * FROM D_TIMECONF " +
                                    "ORDER BY " +
                                        "AUFNR, VORNR";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var _timeConf = new TimeConf();
                    DateTime _time;
                    _timeConf.Aufnr = (String)rdr["AUFNR"];
                    _timeConf.Vornr = (String)rdr["VORNR"];
                    _timeConf.Rmzhl = (String)rdr["RMZHL"];
                    DateTimeHelper.getDate(rdr["ERSDA"], out _time);
                    _timeConf.Ersda = _time;
                    _timeConf.Ernam = (String)rdr["ERNAM"];
                    _timeConf.Split = (String)rdr["SPLIT"];
                    _timeConf.Arbpl = (String)rdr["ARBPL"];
                    _timeConf.Werks = (String)rdr["WERKS"];
                    _timeConf.Ltxa1 = (String)rdr["LTXA1"];
                    _timeConf.Txtsp = (String)rdr["TXTSP"];
                    _timeConf.Ismnw = (String)rdr["ISMNW"];
                    _timeConf.Ismne = (String)rdr["ISMNE"];
                    _timeConf.Learr = (String)rdr["LEARR"];
                    _timeConf.Idaur = (String)rdr["IDAUR"];
                    _timeConf.Idaue = (String)rdr["IDAUE"];
                    _timeConf.Pernr = (String)rdr["PERNR"];
                    DateTimeHelper.getDate(rdr["ISDD"], out _time);
                    _timeConf.Isdd = _time;
                    DateTimeHelper.getTime(rdr["ISDD"], rdr["ISDZ"], out _time);
                    _timeConf.Isdz = _time;
                    DateTimeHelper.getDate(rdr["IEDD"], out _time);
                    _timeConf.Iedd = _time;
                    DateTimeHelper.getTime(rdr["IEDD"], rdr["IEDZ"], out _time);
                    _timeConf.Iedz = _time;
                    _timeConf.Aueru = (String)rdr["AUERU"];
                    _timeConf.Ausor = (String)rdr["AUSOR"];
                    _timeConf.Ofmnw = (String)rdr["OFMNW"];
                    _timeConf.Ofmne = (String)rdr["OFMNE"];
                    _timeConf.Bemot = (String)rdr["BEMOT"];
                    _timeConf.UpdFlag = (String)rdr["UPDFLAG"];


                    retVal.Add(_timeConf);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static IEnumerable<TimeConf> GetTimeConfStatus(Order order)
        {
            var tcl = (from n in TimeConfList
                        where n.Aufnr == order.Aufnr
                        select n).ToList();
            return tcl;
        }

        public static IEnumerable<TimeConf> GetTimeConfStatus(OrdOperation operation)
        {
            var tcl = (from n in TimeConfList
                       where n.Aufnr == operation.Aufnr && n.Vornr == operation.Vornr && n.Split == operation.Split
                       select n).ToList();
            return tcl;
        }

        public static List<TimeConf> GetTimeConfs(Order order)
        {
            var retVal = (from n in TimeConfList
                          where n.Aufnr == order.Aufnr
                          orderby n.Vornr, n.Split
                          select n).ToList(); 
            return retVal;
        }

        public static List<TimeConf> GetTimeConfs(OrdOperation ordOperation)
        {
            var retVal = (from n in TimeConfList
                          where n.Aufnr == ordOperation.Aufnr && n.Vornr == ordOperation.Vornr && n.Split == ordOperation.Split
                          select n).ToList(); 
                
            return retVal;
        }

        protected static String GetActHours(String aufnr, String vornr, String split)
        {
            var list = (from n in TimeConfList
                          where n.Aufnr == aufnr && n.Vornr == vornr && n.Split == split
                          select n).ToList(); 
            var total = 0.0;
            try
            {
                foreach (var item in list)
                {
                    var ismnw = item.Ismnw;
                    if (ismnw != null)
                    {
                        ismnw = ismnw.Replace('.', ',');
                        var intIsmnw = Double.Parse(ismnw, NumberStyles.AllowDecimalPoint,
                                                        CultureInfo.CreateSpecificCulture("de-DE"));
                        total = total + intIsmnw;
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return total.ToString();
        }

        public static void SaveTimeConf(TimeConf timeConf)
        {
            try
            {
                int intDbKeyTimeConf;

                // get db key
                var sqlStmt = "SELECT MAX(CAST(RMZHL AS Int))+1 FROM D_TIMECONF";
                var cmd = new SQLiteCommand(sqlStmt);
                var record = OxDbManager.GetInstance().FeedTransaction(cmd);
                var result = record[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyTimeConf = int.Parse(result);
                else
                    intDbKeyTimeConf = 1;

                timeConf.Rmzhl = intDbKeyTimeConf.ToString();
                // Insert Data
                timeConf.Ismnw = timeConf.Ismnw.Replace(',', '.');
                timeConf.Idaur = timeConf.Idaur.Replace(',', '.');
                timeConf.Ofmnw = timeConf.Ofmnw.Replace(',', '.');

                sqlStmt = "INSERT INTO [D_TIMECONF] " +
                          "(MANDT, USERID, AUFNR, VORNR, RMZHL, ERSDA, ERNAM, SPLIT, " +
                          "ARBPL, WERKS, LTXA1, TXTSP, ISMNW, ISMNE, LEARR, IDAUR, " +
                          "IDAUE, PERNR, ISDD, ISDZ, IEDD, IEDZ, AUERU, AUSOR, OFMNW, " +
                          "OFMNE, BEMOT, STATUS, UPDFLAG, MOBILEKEY) " +
                          "Values (@MANDT, @USERID, @AUFNR, @VORNR, @RMZHL, @ERSDA, @ERNAM, @SPLIT, " +
                          "@ARBPL, @WERKS, @LTXA1, @TXTSP, @ISMNW, @ISMNE, @LEARR, @IDAUR, " +
                          "@IDAUE, @PERNR, @ISDD, @ISDZ, @IEDD, @IEDZ, @AUERU, @AUSOR, @OFMNW, " +
                          "@OFMNE, @BEMOT, @STATUS, @UPDFLAG, @MOBILEKEY)";
                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@AUFNR", timeConf.Aufnr);
                cmd.Parameters.AddWithValue("@VORNR", timeConf.Vornr);
                cmd.Parameters.AddWithValue("@RMZHL", timeConf.Rmzhl);
                cmd.Parameters.AddWithValue("@ERSDA", DateTimeHelper.GetDateString(timeConf.Ersda));
                cmd.Parameters.AddWithValue("@ERNAM", timeConf.Ernam);
                cmd.Parameters.AddWithValue("@SPLIT", timeConf.Split);
                cmd.Parameters.AddWithValue("@ARBPL", timeConf.Arbpl);
                cmd.Parameters.AddWithValue("@WERKS", timeConf.Werks);
                cmd.Parameters.AddWithValue("@LTXA1", timeConf.Ltxa1);
                cmd.Parameters.AddWithValue("@TXTSP", timeConf.Txtsp);
                cmd.Parameters.AddWithValue("@ISMNW", timeConf.Ismnw);
                cmd.Parameters.AddWithValue("@ISMNE", timeConf.Ismne);
                cmd.Parameters.AddWithValue("@LEARR", timeConf.Learr);
                cmd.Parameters.AddWithValue("@IDAUR", timeConf.Idaur);
                cmd.Parameters.AddWithValue("@IDAUE", timeConf.Idaue);
                cmd.Parameters.AddWithValue("@PERNR", timeConf.Pernr);
                if (timeConf.Isdd > timeConf.Iedd)
                    timeConf.Isdd = timeConf.Iedd;
                if (timeConf.Isdz > timeConf.Iedz)
                    timeConf.Isdz = timeConf.Iedz;

                cmd.Parameters.AddWithValue("@ISDD", DateTimeHelper.GetDateString(timeConf.Isdd));
                cmd.Parameters.AddWithValue("@ISDZ", DateTimeHelper.GetTimeString(timeConf.Isdz));
                cmd.Parameters.AddWithValue("@IEDD", DateTimeHelper.GetDateString(timeConf.Iedd));
                cmd.Parameters.AddWithValue("@IEDZ", DateTimeHelper.GetTimeString(timeConf.Iedz));
                cmd.Parameters.AddWithValue("@AUERU", timeConf.Aueru);
                cmd.Parameters.AddWithValue("@AUSOR", timeConf.Ausor);
                cmd.Parameters.AddWithValue("@OFMNW", timeConf.Ofmnw);
                cmd.Parameters.AddWithValue("@OFMNE", timeConf.Ofmne);
                cmd.Parameters.AddWithValue("@BEMOT", timeConf.Bemot);
                cmd.Parameters.AddWithValue("@MOBILEKEY", timeConf.MobileKey);
                cmd.Parameters.AddWithValue("@STATUS", "0");
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                timeConf.UpdFlag = "I";
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();
                CachingManager.AddTimeConf(timeConf);
            }

            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static List<TimeConf> GetTimeConfs()
        {
            return (from n in TimeConfList
                    orderby n.Aufnr, n.Vornr
                    select n).ToList();
        }

        public static List<TimeConf> GetTimeConfsByUser()
        {
            var retVal = (from n in TimeConfList
                        orderby n.Pernr, n.Ersda
                        select n).ToList();
            return retVal;
        }

        public static void DeleteTimeConf(TimeConf timeConf)
        {
            try
            {
                var sqlStmt =
                    "DELETE FROM [D_TIMECONF] WHERE MANDT = @MANDT AND USERID = @USERID AND AUFNR = @AUFNR AND VORNR = @VORNR AND SPLIT = @SPLIT AND RMZHL = @RMZHL";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@AUFNR", timeConf.Aufnr);
                cmd.Parameters.AddWithValue("@VORNR", timeConf.Vornr);
                cmd.Parameters.AddWithValue("@SPLIT", timeConf.Split);
                cmd.Parameters.AddWithValue("@RMZHL", timeConf.Rmzhl);
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

                TimeConfList.Remove(timeConf);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void GetTimeConfStatus(OrdOperation operation, out int count, out String total)
        {
            var tcl = (from n in TimeConfList
                       where n.Aufnr == operation.Aufnr && n.Vornr == operation.Vornr && n.Split == operation.Split
                       select n).ToList();
            count = tcl.Count;
            total = GetActHours(operation.Aufnr, operation.Vornr, operation.Split);
            var flTotal = float.Parse(total.Replace('.', ','));
            total = TimeValue(flTotal);
            if (operation.SArbeh.Equals("MIN") || operation.Arbeh.Equals("MIN"))
            {
                total = total.Substring(0, total.IndexOf(":")) + " MIN";
            }
            else
            {
                total = total + " H";
            }
        }

        public static String TimeValue(float input)
        {
            var fraction = Math.Round(input - Math.Floor(input), 2);
            var minutes = fraction * 60;
            return Math.Floor(input) + ":" + Math.Floor(minutes).ToString().PadLeft(2, '0');
        }
    }
}