﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMeasurementManager
    {
        public static List<MeasurementPoint> GetMeasurementPoints()
        {
            var _measurementPoints = new List<MeasurementPoint>();
            try
            {
                String sqlStmt = "SELECT * FROM C_MEASPOINT";
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var _measurementPoint = new MeasurementPoint();
                    _measurementPoint.Point = (String)rdr["POINT"];
                    _measurementPoint.Mpobj = (String)rdr["MPOBJ"];
                    _measurementPoint.Psort = (String)rdr["PSORT"];
                    _measurementPoint.Pttxt = (String)rdr["PTTXT"];
                    _measurementPoint.Locas = (String)rdr["LOCAS"];
                    _measurementPoint.Atinn = (String)rdr["ATINN"];
                    _measurementPoint.Mrmin = (String)rdr["MRMIN"];
                    _measurementPoint.Mrmax = (String)rdr["MRMAX"];
                    _measurementPoint.Mrngu = (String)rdr["MRNGU"];
                    _measurementPoint.Desir = (String)rdr["DESIR"];
                    _measurementPoint.Indct = (String)rdr["INDCT"];
                    _measurementPoint.Indrv = (String)rdr["INDRV"];
                    _measurementPoint.Last_rec = (String)rdr["LAST_REC"];
                    _measurementPoint.Last_unit = (String)rdr["LAST_UNIT"];
                    _measurementPoints.Add(_measurementPoint);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _measurementPoints;
        }

        public static List<MeasurementPoint> getMeasurementPoints(Object techObj)
        {
            String _mpobj = "";
            if (techObj.GetType().Name.Equals("FuncLoc"))
            {
                var _funcLoc = (FuncLoc)techObj;
                _mpobj = _funcLoc.Objnr;
            }
            else
            {
                var _equi = (Equi)techObj;
                _mpobj = "IE" + _equi.Equnr;
            }
            var _measurementPoints = new List<MeasurementPoint>();
            try
            {
                String sqlStmt = "SELECT * FROM C_MEASPOINT WHERE MPOBJ = @MPOBJ";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MPOBJ", _mpobj);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var _measurementPoint = new MeasurementPoint();
                    _measurementPoint.Point = (String) rdr["POINT"];
                    _measurementPoint.Mpobj = (String) rdr["MPOBJ"];
                    _measurementPoint.Psort = (String) rdr["PSORT"];
                    _measurementPoint.Pttxt = (String) rdr["PTTXT"];
                    _measurementPoint.Locas = (String) rdr["LOCAS"];
                    _measurementPoint.Atinn = (String) rdr["ATINN"];
                    _measurementPoint.Mrmin = (String) rdr["MRMIN"];
                    _measurementPoint.Mrmax = (String) rdr["MRMAX"];
                    _measurementPoint.Mrngu = (String) rdr["MRNGU"];
                    _measurementPoint.Desir = (String) rdr["DESIR"];
                    _measurementPoint.Indct = (String) rdr["INDCT"];
                    _measurementPoint.Indrv = (String) rdr["INDRV"];
                    _measurementPoint.Last_rec = (String)rdr["LAST_REC"];
                    _measurementPoint.Last_unit = (String)rdr["LAST_UNIT"];
                    _measurementPoints.Add(_measurementPoint);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _measurementPoints;
        }

        public static void SaveMeasurementDoc(MeasurementDoc measDoc)
        {
            try
            {
                int intDbKey = -1;
                String sqlStmt = "SELECT MAX(rowid) + 1 FROM D_MEASDOC WHERE UPDFLAG = 'I' OR UPDFLAG = 'X'";
                String result = "";
                var retval = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                result = retval[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;

                measDoc.Mdocm = intDbKey.ToString();

                int dbResult = -1;
                sqlStmt = "INSERT INTO [D_MEASDOC] " +
                          "(MANDT, USERID, POINT, MDOCM, IDATE, ITIME, MDTXT, READR, " +
                          "RECDC, UNITR, UPDFLAG) " +
                          "Values (@MANDT, @USERID, @POINT, @MDOCM, @IDATE, @ITIME, @MDTXT, @READR, " +
                          "@RECDC, @UNITR, @UPDFLAG)";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@POINT", measDoc.Point);
                cmd.Parameters.AddWithValue("@MDOCM", measDoc.Mdocm);
                cmd.Parameters.AddWithValue("@IDATE", DateTimeHelper.GetDateString(measDoc.Idate));
                cmd.Parameters.AddWithValue("@ITIME",DateTimeHelper.GetTimeString( measDoc.Itime));
                cmd.Parameters.AddWithValue("@MDTXT", measDoc.Mdtxt);
                cmd.Parameters.AddWithValue("@READR", measDoc.Readr);
                cmd.Parameters.AddWithValue("@RECDC", measDoc.Recdc);
                cmd.Parameters.AddWithValue("@UNITR", measDoc.Unitr);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");

                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static List<MeasurementDoc> getMeasurementDocs(MeasurementPoint point)
        {
            List<MeasurementDoc> measDocs = new List<MeasurementDoc>();
            try
            {
                String sqlStmt = "SELECT * FROM D_MEASDOC WHERE POINT = @POINT ORDER BY RECDC DESC";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@POINT", point.Point);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var measurementDoc = new MeasurementDoc();
                    measurementDoc.Point = (String) rdr["POINT"];
                    measurementDoc.Mdocm = (String) rdr["MDOCM"];
                    DateTime idate = DateTime.Now;
                    DateTime itime = DateTime.Now;
                    DateTimeHelper.getDate(rdr["IDATE"], out idate);
                    measurementDoc.Idate = idate;
                    DateTimeHelper.getTime(rdr["IDATE"], rdr["ITIME"], out itime);
                    measurementDoc.Itime = itime;
                    measurementDoc.Mdtxt = (String) rdr["MDTXT"];
                    measurementDoc.Readr = (String) rdr["READR"];
                    measurementDoc.Recdc = (String) rdr["RECDC"];
                    measurementDoc.Unitr = (String) rdr["UNITR"];
                    measurementDoc.UpdFlag = (String)rdr["UPDFLAG"];
                    measDocs.Add(measurementDoc);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return measDocs;
        }
        public static List<MeasurementDoc> getMeasurementDocs()
        {
            List<MeasurementDoc> measDocs = new List<MeasurementDoc>();
            try
            {
                var sqlStmt = "SELECT * FROM D_MEASDOC";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var measurementDoc = new MeasurementDoc();
                    measurementDoc.Point = (String)rdr["POINT"];
                    measurementDoc.Mdocm = (String)rdr["MDOCM"];
                    DateTime idate = DateTime.Now;
                    DateTime itime = DateTime.Now;
                    DateTimeHelper.getDate(rdr["IDATE"], out idate);
                    measurementDoc.Idate = idate;
                    DateTimeHelper.getTime(rdr["IDATE"], rdr["ITIME"], out itime);
                    measurementDoc.Itime = itime;
                    measurementDoc.Mdtxt = (String)rdr["MDTXT"];
                    measurementDoc.Readr = (String)rdr["READR"];
                    measurementDoc.Recdc = (String)rdr["RECDC"];
                    measurementDoc.Unitr = (String)rdr["UNITR"];
                    measurementDoc.UpdFlag = (String)rdr["UPDFLAG"];
                    measDocs.Add(measurementDoc);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return measDocs;
        }
        public static void DeleteMeasDoc(MeasurementDoc measDoc)
        {
            try
            {
                int dbResult = -1;
                String sqlStmt =
                    "DELETE FROM [D_MEASDOC] WHERE POINT = @POINT AND MDOCM = @MDOCM";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@POINT", measDoc.Point);
                cmd.Parameters.AddWithValue("@MDOCM", measDoc.Mdocm);
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static string GetMeasureStatus(OxBusinessObject techObj)
        {
            String ret = "";
            int countMeasPoint = 0;
            int countMeasDoc = 0;
            List<MeasurementPoint> mPoints = getMeasurementPoints(techObj);
            foreach (var item in mPoints)
            {
                countMeasPoint++;
                List<MeasurementDoc> mDocs = getMeasurementDocs(item);
                if (mDocs.Count > 0)
                    countMeasDoc++;
            }
            return countMeasDoc + "/" + countMeasPoint;
        }

        public static MeasurementPoint GetMeasurementPoint(String measPoint)
        {

            var _measurementPoint = new MeasurementPoint();
            try
            {
                String sqlStmt = "SELECT * FROM C_MEASPOINT WHERE POINT = @POINT";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@POINT", measPoint);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    _measurementPoint.Point = (String)rdr["POINT"];
                    _measurementPoint.Mpobj = (String)rdr["MPOBJ"];
                    _measurementPoint.Psort = (String)rdr["PSORT"];
                    _measurementPoint.Pttxt = (String)rdr["PTTXT"];
                    _measurementPoint.Locas = (String)rdr["LOCAS"];
                    _measurementPoint.Atinn = (String)rdr["ATINN"];
                    _measurementPoint.Mrmin = (String)rdr["MRMIN"];
                    _measurementPoint.Mrmax = (String)rdr["MRMAX"];
                    _measurementPoint.Mrngu = (String)rdr["MRNGU"];
                    _measurementPoint.Desir = (String)rdr["DESIR"];
                    _measurementPoint.Indct = (String)rdr["INDCT"];
                    _measurementPoint.Indrv = (String)rdr["INDRV"];
                    _measurementPoint.Last_rec = (String)rdr["LAST_REC"];
                    _measurementPoint.Last_unit = (String)rdr["LAST_UNIT"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _measurementPoint;
        }

        public static OxBusinessObject GetTechObj(MeasurementPoint point)
        {
            if (point.Mpobj.StartsWith("IE"))
                return EquipmentManager.GetEqui(point.Mpobj.Substring(2));
            if (point.Mpobj.StartsWith("IF"))
                return FuncLocManager.GetFuncLocByObjnr(point.Mpobj);
            return null;
        }
    }
}