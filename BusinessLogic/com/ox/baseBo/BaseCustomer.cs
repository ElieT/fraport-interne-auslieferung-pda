﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCustomer
    {
        public string KUNNR { get; set; }

        public string NAME1 { get; set; }

        public string NAME2 { get; set; }

        public string ORTO1 { get; set; }

        public string PSTLZ { get; set; }

        public string REGIO { get; set; }

        public string SORTL { get; set; }

        public string STRAS { get; set; }

        public string TELF1 { get; set; }

        public string TELFX { get; set; }

        public string ANRED { get; set; }

    }
}