﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCharactValue
    {
        public string Atinn { get; set; }

        public string Atzhl { get; set; }

        public string Adzhl { get; set; }

        public string Atwrt { get; set; }

        public string Atstd { get; set; }

        public string Atwtb { get; set; }

        public string Atflv { get; set; }
        
        public string Atflb { get; set; }
        
        public string Atcod { get; set; }
        
        public string Atawe { get; set; }
        
        public string Ataw1 { get; set; }
        
        public string Attlv { get; set; }

        public string Attlb { get; set; }

        //Value comes from C_CLASSCHARACT
        public string Atfor { get; set; }
    }
}