﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCustCodes : IComparable
    {
        public string Code { get; set; }

        public string CodegrKurztext { get; set; }

        public string Codegruppe { get; set; }

        public string Katalogart { get; set; }

        public string Kurztext { get; set; }

        public string Bewertung { get; set; }

        #region IComparable Members

        public int CompareTo(object obj1)
        {
                return (0);
        }


        public class BaseCustCodesSort : IComparer<CustCodes>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseCustCodesSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(CustCodes x, CustCodes y)
            {
                switch (_compareField.ToLower())
                {
                    case "codegrkurztext":
                        if (_desc)
                            return x.CodegrKurztext.CompareTo(y.CodegrKurztext);
                        return y.CodegrKurztext.CompareTo(x.CodegrKurztext);
                    case "kurztext":
                        if (_desc)
                            return (x).Kurztext.CompareTo(y.Kurztext);
                        return (y).Kurztext.CompareTo(x.Kurztext);
                    default:
                        return (x).Codegruppe.CompareTo(y.Codegruppe);
                }
            }
        }
        #endregion
    }
}