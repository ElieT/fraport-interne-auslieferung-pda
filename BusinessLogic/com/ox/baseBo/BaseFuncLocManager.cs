﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseFuncLocManager
    {
        #region Constructors (1) 

        #endregion Constructors 

        public ArrayList GetTPL()
        {
            var retVal = new ArrayList();
            try
            {
                String sqlStmt = "SELECT * FROM [D_FLOC]";
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var funcLoc = new FuncLoc();
                    DateTime tempDate;
                    funcLoc.Tplnr = (String)rdr["TPLNR"];
                    funcLoc.Pltxt = (String)rdr["PLTXT"];
                    funcLoc.Submt = (String)rdr["SUBMT"];
                    funcLoc.Iwerk = (String)rdr["IWERK"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out tempDate);
                    tempDate = funcLoc.Ansdt;
                    funcLoc.Baujj = (String)rdr["BAUJJ"];
                    funcLoc.Baumm = (String)rdr["BAUMM"];
                    funcLoc.Beber = (String)rdr["BEBER"];
                    funcLoc.Eqart = (String)rdr["EQART"];
                    funcLoc.Eqfnr = (String)rdr["EQFNR"];
                    funcLoc.Fltyp = (String)rdr["FLTYP"];
                    funcLoc.Herld = (String)rdr["HERLD"];
                    funcLoc.Herst = (String)rdr["HERST"];
                    DateTimeHelper.getDate(rdr["DATAB"], out tempDate);
                    funcLoc.Datab = tempDate;
                    funcLoc.Posnr = (String)rdr["POSNR"];
                    funcLoc.Invnr = (String)rdr["INVNR"];
                    funcLoc.Kostl = (String)rdr["KOSTL"];
                    funcLoc.Stort = (String)rdr["STORT"];
                    funcLoc.Msgrp = (String)rdr["MSGRP"];
                    //funcLoc.Raumnr = (String)rdr["RAUMNR"];
                    funcLoc.Rbnr = (String)rdr["RBNR"];
                    funcLoc.Tplma = (String)rdr["TPLMA"];
                    funcLoc.Typbz = (String)rdr["TYPBZ"];
                    funcLoc.Objnr = (String)rdr["OBJNR"];
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out tempDate);
                    funcLoc.GwldtI = tempDate;
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out tempDate);
                    funcLoc.GwlenI = tempDate;
                    funcLoc.Gewrk = rdr["GEWRK"];
                    funcLoc.Wergw = rdr["WERGW"];
                    funcLoc.TxtGewrk = rdr["TXT_GEWRK"];
                    funcLoc.Adrnr = rdr["ADRNR"];
                    retVal.Add(funcLoc);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static List<OxBusinessObject> getFuncLocObjs()
        {
            var _funcLocs = FuncLocManager.getFuncLocs();
            var _funcLocObjs = new List<OxBusinessObject>();
            try
            {
                foreach (FuncLoc _funcLoc in _funcLocs)
                {
                    _funcLocObjs.Add(_funcLoc);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _funcLocObjs;
        }

        public static List<FuncLoc> getFuncLocs()
        {
            var _funcLocs = new List<FuncLoc>();
            try
            {
                String sqlStmt = "SELECT * FROM D_FLOC";
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    //var _funcLoc = new FuncLoc();
                    var funcLoc = new FuncLoc();
                    DateTime tempDate;
                    funcLoc.Tplnr = (String)rdr["TPLNR"];
                    funcLoc.Pltxt = (String)rdr["PLTXT"];
                    funcLoc.Submt = (String)rdr["SUBMT"];
                    funcLoc.Iwerk = (String)rdr["IWERK"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out tempDate);
                    tempDate = funcLoc.Ansdt;
                    funcLoc.Baujj = (String)rdr["BAUJJ"];
                    funcLoc.Baumm = (String)rdr["BAUMM"];
                    funcLoc.Beber = (String)rdr["BEBER"];
                    funcLoc.Eqart = (String)rdr["EQART"];
                    funcLoc.Eqfnr = (String)rdr["EQFNR"];
                    funcLoc.Fltyp = (String)rdr["FLTYP"];
                    funcLoc.Herld = (String)rdr["HERLD"];
                    funcLoc.Herst = (String)rdr["HERST"];
                    DateTimeHelper.getDate(rdr["DATAB"], out tempDate);
                    funcLoc.Datab = tempDate;
                    funcLoc.Posnr = (String)rdr["POSNR"];
                    funcLoc.Invnr = (String)rdr["INVNR"];
                    funcLoc.Kostl = (String)rdr["KOSTL"];
                    funcLoc.Stort = (String)rdr["STORT"];
                    funcLoc.Msgrp = (String)rdr["MSGRP"];
                    funcLoc.Rbnr = (String)rdr["RBNR"];
                    funcLoc.Tplma = (String)rdr["TPLMA"];
                    funcLoc.Typbz = (String)rdr["TYPBZ"];
                    funcLoc.Objnr = (String)rdr["OBJNR"];
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out tempDate);
                    funcLoc.GwldtI = tempDate;
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out tempDate);
                    funcLoc.GwlenI = tempDate;
                    funcLoc.Gewrk = (String)rdr["GEWRK"];
                    funcLoc.Wergw = (String)rdr["WERGW"];
                    funcLoc.TxtGewrk = (String)rdr["TXT_GEWRK"];
                    funcLoc.Adrnr = (String)rdr["ADRNR"];
                    _funcLocs.Add(funcLoc);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _funcLocs;
        }


        public static new FuncLoc getFuncLoc(string tplnr)
        {
            var funcLoc = new FuncLoc();
            try
            {
                var sqlStmt = "SELECT * FROM D_FLOC WHERE TPLNR = @TPLNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TPLNR", tplnr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    DateTime tempDate;
                    funcLoc.Tplnr = rdr["TPLNR"];
                    funcLoc.Pltxt = rdr["PLTXT"];
                    funcLoc.Submt = rdr["SUBMT"];
                    funcLoc.Iwerk = rdr["IWERK"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out tempDate);
                    funcLoc.Ansdt = tempDate;
                    funcLoc.Baujj = rdr["BAUJJ"];
                    funcLoc.Baumm = rdr["BAUMM"];
                    funcLoc.Beber = rdr["BEBER"];
                    funcLoc.Eqart = rdr["EQART"];
                    funcLoc.Eqfnr = rdr["EQFNR"];
                    funcLoc.Fltyp = rdr["FLTYP"];
                    funcLoc.Herld = rdr["HERLD"];
                    funcLoc.Herst = rdr["HERST"];
                    DateTimeHelper.getDate(rdr["DATAB"], out tempDate);
                    funcLoc.Datab = tempDate;
                    funcLoc.Posnr = rdr["POSNR"];
                    funcLoc.Invnr = rdr["INVNR"];
                    funcLoc.Kostl = rdr["KOSTL"];
                    funcLoc.Stort = rdr["STORT"];
                    funcLoc.Msgrp = rdr["MSGRP"];
                    funcLoc.Rbnr = rdr["RBNR"];
                    funcLoc.Tplma = rdr["TPLMA"];
                    funcLoc.Typbz = rdr["TYPBZ"];
                    funcLoc.Objnr = rdr["OBJNR"];
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out tempDate);
                    funcLoc.GwldtI = tempDate;
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out tempDate);
                    funcLoc.GwlenI = tempDate;
                    funcLoc.Gewrk = rdr["GEWRK"];
                    funcLoc.Wergw = rdr["WERGW"];
                    funcLoc.TxtGewrk = rdr["TXT_GEWRK"];
                    funcLoc.Adrnr = rdr["ADRNR"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return funcLoc;
        }

        public static void SaveFloc(FuncLoc floc)
        {
            try
            {
                var intDbResult = -1;
                String sqlStmt = "UPDATE " +
                                        "D_FLOC " +
                                    "SET " +
                                        "SUBMT = @SUBMT, " +
                                        "IWERK = @IWERK, " +
                                        "PLTXT = @PLTXT, " +
                                        "RBNR = @RBNR, " +
                                        "POSNR = @POSNR, " +
                                        "EQART = @EQART, " +
                                        "INVNR = @INVNR, " +
                                        "ANSDT = @ANSDT, " +
                                        "HERST = @HERST, " +
                                        "BAUJJ = @BAUJJ, " +
                                        "BAUMM = @BAUMM, " +
                                        "TYPBZ = @TYPBZ, " +
                                        "STORT = @STORT, " +
                                        "MSGRP = @MSGRP, " +
                                        "BEBER = @BEBER, " +
                                        "KOSTL = @KOSTL, " +
                                        "FLTYP = @FLTYP, " +
                                        "TPLMA = @TPLMA, " +
                                        "DATAB = @DATAB, " +
                                        "EQFNR = @EQFNR, " +
                                        "HERLD = @HERLD, " +
                                        "OBJNR = @OBJNR, " +
                                        "GEWRK = @GEWRK, " +
                                        "WERGW = @WERGW, " +
                                        "TXT_GEWRK = @TXT_GEWRK " +
                                    "WHERE TPLNR = @TPLNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TPLNR", floc.Tplnr);
                cmd.Parameters.AddWithValue("@SUBMT", floc.Submt);
                cmd.Parameters.AddWithValue("@IWERK", floc.Iwerk);
                cmd.Parameters.AddWithValue("@PLTXT", floc.Pltxt);
                cmd.Parameters.AddWithValue("@RBNR", floc.Rbnr);
                cmd.Parameters.AddWithValue("@POSNR", floc.Posnr);
                cmd.Parameters.AddWithValue("@EQART", floc.Eqart);
                cmd.Parameters.AddWithValue("@INVNR", floc.Invnr);
                if (floc.Ansdt == (new DateTime()))
                    cmd.Parameters.AddWithValue("@ANSDT", "00000000");
                else
                    cmd.Parameters.AddWithValue("@ANSDT", "" + floc.Ansdt.Year.ToString().PadLeft(4, '0') + floc.Ansdt.Month.ToString().PadLeft(2, '0') + floc.Ansdt.Day.ToString().PadLeft(2, '0'));
                cmd.Parameters.AddWithValue("@HERST", floc.Herst);
                cmd.Parameters.AddWithValue("@BAUJJ", floc.Baujj);
                cmd.Parameters.AddWithValue("@BAUMM", floc.Baumm);
                cmd.Parameters.AddWithValue("@TYPBZ", floc.Typbz);
                cmd.Parameters.AddWithValue("@STORT", floc.Stort);
                cmd.Parameters.AddWithValue("@MSGRP", floc.Msgrp);
                cmd.Parameters.AddWithValue("@BEBER", floc.Beber);
                cmd.Parameters.AddWithValue("@KOSTL", floc.Kostl);
                cmd.Parameters.AddWithValue("@FLTYP", floc.Fltyp);
                cmd.Parameters.AddWithValue("@TPLMA", floc.Tplma);
                if (floc.Datab == (new DateTime()))
                    cmd.Parameters.AddWithValue("@DATAB", "00000000");
                else
                    cmd.Parameters.AddWithValue("@DATAB", "" + floc.Datab.Year.ToString().PadLeft(4, '0') + floc.Datab.Month.ToString().PadLeft(2, '0') + floc.Datab.Day.ToString().PadLeft(2, '0'));
                cmd.Parameters.AddWithValue("@EQFNR", floc.Eqfnr);
                cmd.Parameters.AddWithValue("@HERLD", floc.Herld);
                cmd.Parameters.AddWithValue("@OBJNR", floc.Objnr);
                cmd.Parameters.AddWithValue("@GWLDT_I", DateTimeHelper.GetDateString(floc.GwldtI));
                cmd.Parameters.AddWithValue("@GWLEN_I", DateTimeHelper.GetDateString(floc.GwlenI));
                cmd.Parameters.AddWithValue("@GEWRK", floc.Gewrk);
                cmd.Parameters.AddWithValue("@WERGW", floc.Wergw);
                cmd.Parameters.AddWithValue("@TXT_GEWRK", floc.TxtGewrk);
                cmd.Parameters.AddWithValue("@ADRNR", floc.Adrnr);
                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                intDbResult = int.Parse(result[0]["RetVal"]);
                if (intDbResult != 1)
                {
                    throw new Exception("Wrong SQLQuery result. Expecting 1, result was " + intDbResult);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }


        public static List<ObjClass> GetObjClass(FuncLoc funcLoc)
        {
            var objClasses = new List<ObjClass>();
            try
            {
                var sqlStmt = "SELECT DISTINCT " +
                                   "D_OBJCLASS.OBJNR, " +
                                   "D_OBJCLASS.ATINN, " +
                                   "D_OBJCLASS.CLINT, " +
                                   "D_OBJCLASS.ATZHL, " +
                                   "D_OBJCLASS.MAFID, " +
                                   "D_OBJCLASS.KLART, " +
                                   "D_OBJCLASS.ADZHL, " +
                                   "D_OBJCLASS.ATFLV, " +
                                   "D_OBJCLASS.ATFLB, " +
                                   "D_OBJCLASS.ATAWE, " +
                                   "D_OBJCLASS.ATAW1, " +
                                   "D_OBJCLASS.ATCOD, " +
                                   "D_OBJCLASS.ATTLV, " +
                                   "D_OBJCLASS.ATTLB, " +
                                   "D_OBJCLASS.ATWRT, " +
                                   "C_CLASSCHARACT.ATBEZ, " +
                                   "C_CLASSCHARACT.ATFOR, " +
                                   "C_CLASSCHARACT.ATEIN, " +
                                   "C_CLASSCHARACT.MSEHI, " +
                                   "C_CLASSCHARACT.MSEH6, " +
                                   "C_CLASSCHARACT.MSEHT, " +
                                   "C_CLASSCHARACT.ATINT " +
                               "FROM D_OBJCLASS " +
                               "LEFT OUTER JOIN C_CLASSCHARACT ON " +
                                   "C_CLASSCHARACT.ATINN = D_OBJCLASS.ATINN AND C_CLASSCHARACT.CLINT IN (SELECT C_OBJCLASSAS.CLINT FROM C_OBJCLASSAS WHERE C_OBJCLASSAS.OBJNR = D_OBJCLASS.OBJNR)" +
                               "LEFT OUTER JOIN " +
                                   "C_CHARACTVALUE ON C_CHARACTVALUE.ATINN = C_CLASSCHARACT.ATINN AND C_CHARACTVALUE.ADZHL = C_CLASSCHARACT.ADZHL AND C_CHARACTVALUE.ATZHL = D_OBJCLASS.ATZHL " +
                               "WHERE " +
                               "D_OBJCLASS.OBJNR = @OBJNR " +
                               "ORDER BY D_OBJCLASS.OBJNR, D_OBJCLASS.ATINN";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@OBJNR", funcLoc.Objnr);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (var rdr in records)
                {
                    var objClass = new ObjClass();

                    objClass.Objnr = rdr["OBJNR"];
                    objClass.Atinn = rdr["ATINN"];
                    objClass.Clint = rdr["CLINT"];
                    objClass.Atzhl = rdr["ATZHL"];
                    objClass.Mafid = rdr["MAFID"];
                    objClass.Klart = rdr["KLART"];
                    objClass.Adzhl = rdr["ADZHL"];
                    objClass.Atflv = rdr["ATFLV"];
                    objClass.Atflb = rdr["ATFLB"];
                    objClass.Atawe = rdr["ATAWE"];
                    objClass.Ataw1 = rdr["ATAW1"];
                    objClass.Atcod = rdr["ATCOD"];
                    objClass.Attlv = rdr["ATTLV"];
                    objClass.Attlb = rdr["ATTLB"];
                    objClass.Atwrt = rdr["ATWRT"];
                    objClass.Atbez = rdr["ATBEZ"];
                    objClass.Atfor = rdr["ATFOR"];
                    objClass.Atein = rdr["ATEIN"];
                    objClass.Atint = rdr["ATINT"];
                    objClass.Msehi = rdr["MSEHI"];
                    objClass.Mseh6 = rdr["MSEH6"];
                    objClass.Mseht = rdr["MSEHT"];
                    objClass.Objnr = funcLoc.Objnr; 
                    objClasses.Add(objClass);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return objClasses;
        }
        
        public static List<MeasurementPoint> GetMeasPoints(FuncLoc funcLoc)
        {
            var measPoints = new List<MeasurementPoint>();
            try
            {
                String sqlStmt = "SELECT * FROM C_MEASPOINT WHERE MPOBJ = @OBJNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@OBJNR", funcLoc.Objnr);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var measPoint = new MeasurementPoint();
                    measPoint.Point = (String)rdr["POINT"];
                    measPoint.Mpobj = (String)rdr["MPOBJ"];
                    measPoint.Psort = (String)rdr["PSORT"];
                    measPoint.Pttxt = (String)rdr["PTTXT"];
                    measPoint.Locas = (String)rdr["LOCAS"];
                    measPoint.Atinn = (String)rdr["ATINN"];
                    measPoint.Mrmin = (String)rdr["MRMIN"];
                    measPoint.Mrmax = (String)rdr["MRMAX"];
                    measPoint.Mrngu = (String)rdr["MRNGU"];
                    measPoint.Desir = (String)rdr["DESIR"];
                    measPoint.Indct = (String)rdr["INDCT"];
                    measPoint.Indrv = (String)rdr["INDRV"];
                    measPoint.Last_rec = (String)rdr["LAST_REC"];
                    measPoint.Last_unit = (String)rdr["LAST_UNIT"];
                    measPoints.Add(measPoint);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return measPoints;
        }

        public static FuncLoc GetFuncLocByObjnr(string mpobj)
        {
            var funcLoc = new FuncLoc();
            try
            {
                String sqlStmt = "SELECT * FROM D_FLOC WHERE OBJNR = '" + mpobj + "'";
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    DateTime tempDate;
                    funcLoc.Tplnr = rdr["TPLNR"];
                    funcLoc.Pltxt = rdr["PLTXT"];
                    funcLoc.Submt = rdr["SUBMT"];
                    funcLoc.Iwerk = rdr["IWERK"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out tempDate);
                    tempDate = funcLoc.Ansdt;
                    funcLoc.Baujj = rdr["BAUJJ"];
                    funcLoc.Baumm = rdr["BAUMM"];
                    funcLoc.Beber = rdr["BEBER"];
                    funcLoc.Eqart = rdr["EQART"];
                    funcLoc.Eqfnr = rdr["EQFNR"];
                    funcLoc.Fltyp = rdr["FLTYP"];
                    funcLoc.Herld = rdr["HERLD"];
                    funcLoc.Herst = rdr["HERST"];
                    DateTimeHelper.getDate(rdr["DATAB"], out tempDate);
                    funcLoc.Datab = tempDate;
                    funcLoc.Posnr = rdr["POSNR"];
                    funcLoc.Invnr = rdr["INVNR"];
                    funcLoc.Kostl = rdr["KOSTL"];
                    funcLoc.Stort = rdr["STORT"];
                    funcLoc.Msgrp = rdr["MSGRP"];
                    funcLoc.Rbnr = rdr["RBNR"];
                    funcLoc.Tplma = rdr["TPLMA"];
                    funcLoc.Typbz = rdr["TYPBZ"];
                    funcLoc.Objnr = rdr["OBJNR"];
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out tempDate);
                    funcLoc.GwldtI = tempDate;
                    tempDate = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out tempDate);
                    funcLoc.GwlenI = tempDate;
                    funcLoc.Gewrk = (String)rdr["GEWRK"];
                    funcLoc.Wergw = (String)rdr["WERGW"];
                    funcLoc.TxtGewrk = (String)rdr["TXT_GEWRK"];
                    funcLoc.Adrnr = (String)rdr["ADRNR"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return funcLoc;
        }

        public static List<String[]> GetMasterEditingList(FuncLoc floc)
        {
            var ret = new List<String[]>();
            try
            {
                ret.Add(new[] { "TPLNR", "Tech. Platz", floc.Tplnr, "X" });
                ret.Add(new[] { "STORT", "Inventarnummer", floc.Stort, "" });
                ret.Add(new[] { "ANSDT", "Anschaffungsdatum", floc.Ansdt.ToString("dd.MM.yyyy"), "" });
                ret.Add(new[] { "TYPBZ", "Hersteller der Anlage", floc.Typbz, "" });
                ret.Add(new[] { "BAUJJ", "Herstellerland", floc.Baujj, "" });
                ret.Add(new[] { "BAUMM", "Serialnummer gemäß Hersteller", floc.Baumm, "" });
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return ret;
        }


        public static void SetMasterEditingData(Dictionary<String, TextBox> tbList, FuncLoc floc)
        {
            try
            {
                floc.Tplnr = tbList["tb_TPLNR"].Text;
                floc.Stort = tbList["tb_STORT"].Text;
                if (!String.IsNullOrEmpty(tbList["tb_ANSDT"].Text))
                    floc.Ansdt = DateTimeHelper.getDate(tbList["tb_ANSDT"].Text);
                floc.Typbz = tbList["tb_TYPBZ"].Text;
                floc.Baujj = tbList["tb_BAUJJ"].Text;
                floc.Baumm = tbList["tb_BAUMM"].Text;
                SaveFloc(floc);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}