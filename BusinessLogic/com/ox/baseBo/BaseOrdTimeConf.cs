﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOrdTimeConf
    {
        protected String AUFNR, VORNR, RMZHL;

        public string Aufnr
        {
            get { return AUFNR; }
            set { AUFNR = value; }
        }

        public string Vornr
        {
            get { return VORNR; }
            set { VORNR = value; }
        }

        public string Rmzhl
        {
            get { return RMZHL; }
            set { RMZHL = value; }
        }
    }
}
