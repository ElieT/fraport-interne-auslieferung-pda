﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseEqui : OxBusinessObject
    {
        public DateTime ANSDT;

        public String BAUJJ,
                      BAUMM;

        public String EQART;
        public String EQKTX;
        public String EQTYP;
        private String _objnr;
        public String EQUNR;

        public String HEQNR;

        public String HEQUI;
        public String HERLD;
        public String HERST;
        public DateTime INBDT;

        public String INGRP;

        public String INVNR;
        public String IWERK;

        public String KUND1,
                      KUND2,
                      KUND3;

        public String LAGER;
        public String MATNR;

        public String RBNR;

        public String SERGE;
        public String SERNR;
        public String SPART;

        public String SPRAS;

        public String SUBMT;

        public String TPLNR;

        public String TYPBZ;

        public String VKORG,
                      VTWEG;

        public String WERK;
        public String EQFNR;
        public DateTime Gwldt { get; set; }
        public DateTime Gwlen { get; set; }
        public string Groes { get; set; }
        public string Mapar { get; set; }
        public DateTime GwldtI { get; set; }
        public DateTime GwlenI { get; set; }
        public string Gewrk { get; set; }
        public string Wergw { get; set; }
        public string TxtGewrk { get; set; }
        public string Adrnr { get; set; }

        private String STORT, TIDNR, MSGRP;

        public string Msgrp
        {
            get { return MSGRP; }
            set { MSGRP = value; }
        }

        public string Tidnr
        {
            get { return TIDNR; }
            set { TIDNR = value; }
        }

        public string Stort
        {
            get { return STORT; }
            set { STORT = value; }
        }

        public string Equnr
        {
            get { return EQUNR; }
            set { EQUNR = value; }
        }

        public string Eqtyp
        {
            get { return EQTYP; }
            set { EQTYP = value; }
        }

        public string Eqart
        {
            get { return EQART; }
            set { EQART = value; }
        }

        public string Invnr
        {
            get { return INVNR; }
            set { INVNR = value; }
        }

        public DateTime Ansdt
        {
            get { return ANSDT; }
            set { ANSDT = value; }
        }

        public string Herst
        {
            get { return HERST; }
            set { HERST = value; }
        }

        public string Herld
        {
            get { return HERLD; }
            set { HERLD = value; }
        }

        public string Serge
        {
            get { return SERGE; }
            set { SERGE = value; }
        }

        public string Typbz
        {
            get { return TYPBZ; }
            set { TYPBZ = value; }
        }

        public string Baujj
        {
            get { return BAUJJ; }
            set { BAUJJ = value; }
        }

        public string Baumm
        {
            get { return BAUMM; }
            set { BAUMM = value; }
        }

        public string Matnr
        {
            get { return MATNR; }
            set { MATNR = value; }
        }

        public string Sernr
        {
            get { return SERNR; }
            set { SERNR = value; }
        }

        public string Werk
        {
            get { return WERK; }
            set { WERK = value; }
        }

        public string Eqfnr
        {
            get { return EQFNR; }
            set { EQFNR = value; }
        }

        public string Lager
        {
            get { return LAGER; }
            set { LAGER = value; }
        }

        public string Iwerk
        {
            get { return IWERK; }
            set { IWERK = value; }
        }

        public string Submt
        {
            get { return SUBMT; }
            set { SUBMT = value; }
        }

        public string Hequi
        {
            get { return HEQUI; }
            set { HEQUI = value; }
        }

        public string Heqnr
        {
            get { return HEQNR; }
            set { HEQNR = value; }
        }

        public string Ingrp
        {
            get { return INGRP; }
            set { INGRP = value; }
        }

        public string Kund1
        {
            get { return KUND1; }
            set { KUND1 = value; }
        }

        public string Kund2
        {
            get { return KUND2; }
            set { KUND2 = value; }
        }

        public string Kund3
        {
            get { return KUND3; }
            set { KUND3 = value; }
        }

        public string Rbnr
        {
            get { return RBNR; }
            set { RBNR = value; }
        }

        public string Spras
        {
            get { return SPRAS; }
            set { SPRAS = value; }
        }

        public string Eqktx
        {
            get { return EQKTX; }
            set { EQKTX = value; }
        }

        public string Tplnr
        {
            get { return TPLNR; }
            set { TPLNR = value; }
        }

        public string Vkorg
        {
            get { return VKORG; }
            set { VKORG = value; }
        }

        public string Vtweg
        {
            get { return VTWEG; }
            set { VTWEG = value; }
        }

        public string Spart
        {
            get { return SPART; }
            set { SPART = value; }
        }

        public DateTime Inbdt
        {
            get { return INBDT; }
            set { INBDT = value; }
        }

        public string Objnr
        {
            get { return _objnr; }
            set { _objnr = value; }
        }

        public string GetValueByString(String field)
        {
            var retValue = "";

            switch (field.ToUpper())
            {
                case "EQUI":
                    retValue = EQUNR;
                    break;
                case "BAUJJ":
                    retValue = BAUJJ;
                    break;
                case "BAUMM":
                    retValue = BAUMM;
                    break;
                case "EQART":
                    retValue = EQART;
                    break;
                case "EQKTX":
                    retValue = EQKTX;
                    break;
                case "EQTYP":
                    retValue = EQTYP;
                    break;
                case "EQUNR":
                    retValue = EQUNR;
                    break;
                case "HEQNR":
                    retValue = HEQNR;
                    break;
                case "HEQUI":
                    retValue = HEQUI;
                    break;
                case "HERLD":
                    retValue = HERLD;
                    break;
                case "HERST":
                    retValue = HERST;
                    break;
                case "INGRP":
                    retValue = INGRP;
                    break;
                case "INVNR":
                    retValue = INVNR;
                    break;
                case "IWERK":
                    retValue = IWERK;
                    break;
                case "KUND1":
                    retValue = KUND1;
                    break;
                case "KUND2":
                    retValue = KUND2;
                    break;
                case "KUND3":
                    retValue = KUND3;
                    break;
                case "LAGER":
                    retValue = LAGER;
                    break;
                case "MATNR":
                    retValue = MATNR;
                    break;
                case "RBNR":
                    retValue = RBNR;
                    break;
                case "SERGE":
                    retValue = SERGE;
                    break;
                case "SERNR":
                    retValue = SERNR;
                    break;
                case "SPART":
                    retValue = SPART;
                    break;
                case "SPRAS":
                    retValue = SPRAS;
                    break;
                case "SUBMT":
                    retValue = SUBMT;
                    break;
                case "TIDNR":
                    retValue = TIDNR;
                    break;
                case "TPLNR":
                    retValue = TPLNR;
                    break;
                case "TYPBZ":
                    retValue = TYPBZ;
                    break;
                case "VKORG":
                    retValue = VKORG;
                    break;
                case "VTWEG":
                    retValue = VTWEG;
                    break;
                case "WERK":
                    retValue = WERK;
                    break;
                case "MSGRP":
                    retValue = MSGRP;
                    break;
                case "STORT":
                    retValue = STORT;
                    break;
            }
            return retValue;
        }

        public class BaseEquiSort : IComparer<Equi>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseEquiSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Equi x, Equi y)
            {
                switch (_compareField.ToUpper())
                {
                    case "EQUI":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "ANSDT":
                        if (_desc)
                            return x.ANSDT.CompareTo(y.ANSDT);
                        return y.ANSDT.CompareTo(x.ANSDT);
                    case "BAUJJ":
                        if (_desc)
                            return x.BAUJJ.CompareTo(y.BAUJJ);
                        return y.BAUJJ.CompareTo(x.BAUJJ);
                    case "BAUMM":
                        if (_desc)
                            return x.BAUMM.CompareTo(y.BAUMM);
                        return y.BAUMM.CompareTo(x.BAUMM);
                    case "EQART":
                        if (_desc)
                            return x.EQART.CompareTo(y.EQART);
                        return y.EQART.CompareTo(x.EQART);
                    case "EQKTX":
                        if (_desc)
                            return x.EQKTX.CompareTo(y.EQKTX);
                        return y.EQKTX.CompareTo(x.EQKTX);
                    case "EQTYP":
                        if (_desc)
                            return x.EQTYP.CompareTo(y.EQTYP);
                        return y.EQTYP.CompareTo(x.EQTYP);
                    case "EQUNR":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "HEQNR":
                        if (_desc)
                            return x.HEQNR.CompareTo(y.HEQNR);
                        return y.HEQNR.CompareTo(x.HEQNR);
                    case "HEQUI":
                        if (_desc)
                            return x.HEQUI.CompareTo(y.HEQUI);
                        return y.HEQUI.CompareTo(x.HEQUI);
                    case "HERLD":
                        if (_desc)
                            return x.HERLD.CompareTo(y.HERLD);
                        return y.HERLD.CompareTo(x.HERLD);
                    case "HERST":
                        if (_desc)
                            return x.HERST.CompareTo(y.HERST);
                        return y.HERST.CompareTo(x.HERST);
                    case "INGRP":
                        if (_desc)
                            return x.INGRP.CompareTo(y.INGRP);
                        return y.INGRP.CompareTo(x.INGRP);
                    case "INVNR":
                        if (_desc)
                            return x.INVNR.CompareTo(y.INVNR);
                        return y.INVNR.CompareTo(x.INVNR);
                    case "IWERK":
                        if (_desc)
                            return x.IWERK.CompareTo(y.IWERK);
                        return y.IWERK.CompareTo(x.IWERK);
                    case "KUND1":
                        if (_desc)
                            return x.KUND1.CompareTo(y.KUND1);
                        return y.KUND1.CompareTo(x.KUND1);
                    case "KUND2":
                        if (_desc)
                            return x.KUND2.CompareTo(y.KUND2);
                        return y.KUND2.CompareTo(x.KUND2);
                    case "KUND3":
                        if (_desc)
                            return x.KUND3.CompareTo(y.KUND3);
                        return y.KUND3.CompareTo(x.KUND3);
                    case "LAGER":
                        if (_desc)
                            return x.LAGER.CompareTo(y.LAGER);
                        return y.LAGER.CompareTo(x.LAGER);
                    case "MATNR":
                        if (_desc)
                            return x.MATNR.CompareTo(y.MATNR);
                        return y.MATNR.CompareTo(x.MATNR);
                    case "RBNR":
                        if (_desc)
                            return x.RBNR.CompareTo(y.RBNR);
                        return y.RBNR.CompareTo(x.RBNR);
                    case "SERGE":
                        if (_desc)
                            return x.SERGE.CompareTo(y.SERGE);
                        return y.SERGE.CompareTo(x.SERGE);
                    case "SERNR":
                        if (_desc)
                            return x.SERNR.CompareTo(y.SERNR);
                        return y.SERNR.CompareTo(x.SERNR);
                    case "SPART":
                        if (_desc)
                            return x.SPART.CompareTo(y.SPART);
                        return y.SPART.CompareTo(x.SPART);
                    case "SPRAS":
                        if (_desc)
                            return x.SPRAS.CompareTo(y.SPRAS);
                        return y.SPRAS.CompareTo(x.SPRAS);
                    case "SUBMT":
                        if (_desc)
                            return x.SUBMT.CompareTo(y.SUBMT);
                        return y.SUBMT.CompareTo(x.SUBMT);
                    case "TPLNR":
                        if (_desc)
                            return x.TPLNR.CompareTo(y.TPLNR);
                        return y.TPLNR.CompareTo(x.TPLNR);
                    case "TYPBZ":
                        if (_desc)
                            return x.TYPBZ.CompareTo(y.TYPBZ);
                        return y.TYPBZ.CompareTo(x.TYPBZ);
                    case "VKORG":
                        if (_desc)
                            return x.VKORG.CompareTo(y.VKORG);
                        return y.VKORG.CompareTo(x.VKORG);
                    case "VTWEG":
                        if (_desc)
                            return x.VTWEG.CompareTo(y.VTWEG);
                        return y.VTWEG.CompareTo(x.VTWEG);
                    case "WERK":
                        if (_desc)
                            return x.WERK.CompareTo(y.WERK);
                        return y.WERK.CompareTo(x.WERK);
                    default:
                        return (x).EQUNR.CompareTo(y.EQUNR);
                }
            }
        }
    }
}