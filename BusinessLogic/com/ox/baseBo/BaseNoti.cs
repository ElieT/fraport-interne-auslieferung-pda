﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseNotif : OxBusinessObject
    {
        public List<NotifActivity> _notifActivities = new List<NotifActivity>();
        public List<NotifItem> _notifItems = new List<NotifItem>();
        public List<NotifTask> _notifTasks = new List<NotifTask>();
        protected String ABNUM = "";
        protected String ARTPR = "";

        protected String AUFNR = "";

        protected DateTime AUSBS = DateTime.Now;
        protected DateTime AUSVN = DateTime.Now;
        protected DateTime AUZTB = DateTime.Now;
        protected DateTime AUZTV = DateTime.Now;

        protected String BAUTL = "";

        protected String EQKTX = "";

        protected String EQUNR = "";
        protected String INGRP = "";
        protected String IWERK = "";

        protected DateTime LTRMN = DateTime.Now,
                        LTRUR = DateTime.Now;

        protected DateTime MZEIT = DateTime.Now;

        protected String PLTXT = "";

        protected String PRIOK = "";
        protected String PRUEFLOS = "";
        protected String QMART = "";
        protected DateTime QMDAT = DateTime.Now;
        protected String QMNUM = "";
        protected String QMTXT = "";

        protected DateTime STRMN = DateTime.Now,
                        STRUR = DateTime.Now;

        protected String TPLNR = "";
        protected String WAPOS = "";
        protected String WARPL = "";
        protected String UPDFLAG = "";


        protected String STORT = "";
        protected String EQFNR = "";
        protected String TIDNR = "";
        protected String MSGRP = "";
        protected String mobileKey = "";
        protected String GEWRK = "";
        protected String SWERK = "";
        protected String _qmnam;
        protected String RBNR = "";
        protected String _flagPlos = "";

        public BaseNotif()
        {

        }

        public BaseNotif(FuncLoc funcLoc)
        {
            Tplnr = funcLoc.Tplnr;
        }
        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }
        public BaseNotif(Equi equi)
        {
            Equnr = equi.Equnr;
        }

        public BaseNotif(Order order)
        {
            Equnr = order.Equnr;
            Tplnr = order.Tplnr;
            Aufnr = order.Aufnr;
            Eqktx = order.Eqktx;
        }

        public string Eqktx
        {
            get { return EQKTX; }
            set { EQKTX = value; }
        }

        public string Eqfnr
        {
            get { return EQFNR; }
            set { EQFNR = value; }
        }

        public string Stort
        {
            get { return STORT; }
            set { STORT = value; }
        }

        public string Tidnr
        {
            get { return TIDNR; }
            set { TIDNR = value; }
        }

        public string Msgrp
        {
            get { return MSGRP; }
            set { MSGRP = value; }
        }

        public string Pltxt
        {
            get { return PLTXT; }
            set { PLTXT = value; }
        }

        public string Qmnum1
        {
            get { return QMNUM; }
            set { QMNUM = value; }
        }

        public string Qmart
        {
            get { return QMART; }
            set { QMART = value; }
        }

        public string Qmtxt
        {
            get { return QMTXT; }
            set { QMTXT = value; }
        }

        public string Artpr
        {
            get { return ARTPR; }
            set { ARTPR = value; }
        }

        public string Priok
        {
            get { return PRIOK; }
            set { PRIOK = value; }
        }

        public string Aufnr
        {
            get { return AUFNR; }
            set { AUFNR = value; }
        }

        public string Prueflos
        {
            get { return PRUEFLOS; }
            set { PRUEFLOS = value; }
        }

        public string Iwerk
        {
            get { return IWERK; }
            set { IWERK = value; }
        }

        public string Equnr
        {
            get { return EQUNR; }
            set { EQUNR = value; }
        }

        public string Bautl
        {
            get { return BAUTL; }
            set { BAUTL = value; }
        }

        public string Ingrp
        {
            get { return INGRP; }
            set { INGRP = value; }
        }

        public string Warpl
        {
            get { return WARPL; }
            set { WARPL = value; }
        }

        public string Abnum
        {
            get { return ABNUM; }
            set { ABNUM = value; }
        }

        public string Wapos
        {
            get { return WAPOS; }
            set { WAPOS = value; }
        }

        public string Tplnr
        {
            get { return TPLNR; }
            set { TPLNR = value; }
        }

        public DateTime Mzeit
        {
            get { return MZEIT; }
            set { MZEIT = value; }
        }

        public DateTime Qmdat
        {
            get { return QMDAT; }
            set { QMDAT = value; }
        }

        public DateTime Strmn
        {
            get { return STRMN; }
            set { STRMN = value; }
        }

        public DateTime Strur
        {
            get { return STRUR; }
            set { STRUR = value; }
        }

        public DateTime Ltrmn
        {
            get { return LTRMN; }
            set { LTRMN = value; }
        }

        public DateTime Ltrur
        {
            get { return LTRUR; }
            set { LTRUR = value; }
        }

        public DateTime Ausvn
        {
            get { return AUSVN; }
            set { AUSVN = value; }
        }

        public DateTime Ausbs
        {
            get { return AUSBS; }
            set { AUSBS = value; }
        }

        public DateTime Auztv
        {
            get { return AUZTV; }
            set { AUZTV = value; }
        }

        public DateTime Auztb
        {
            get { return AUZTB; }
            set { AUZTB = value; }
        }

        public List<NotifItem> NotifItems
        {
            get { return _notifItems; }
            set { _notifItems = value; }
        }

        public List<NotifTask> NotifTasks
        {
            get { return _notifTasks; }
            set { _notifTasks = value; }
        }

        public List<NotifActivity> NotifActivities
        {
            get { return _notifActivities; }
            set { _notifActivities = value; }
        }

        public string Qmnum
        {
            get { return QMNUM; }
            set { QMNUM = value; }
        }

        public string MobileKey
        {
            get { return mobileKey; }
            set { mobileKey = value; }
        }

        public string Gewrk
        {
            get { return GEWRK; }
            set { GEWRK = value; }
        }

        public string Swerk
        {
            get { return SWERK; }
            set { SWERK = value; }
        }

        public string Qmnam
        {
            get { return _qmnam; }
            set { _qmnam = value; }
        }

        public string Rbnr
        {
            get { return RBNR; }
            set { RBNR = value; }
        }

        public string FlagPlos
        {
            get { return _flagPlos; }
            set { _flagPlos = value; }
        }

        public string GetValueByString(String field)
        {
            var retValue = "";

            switch (field.ToUpper())
            {
                case "ABNUM":
                    retValue = ABNUM;
                    break;
                case "ARTPR":
                    retValue = ARTPR;
                    break;
                case "AUFNR":
                    retValue = AUFNR;
                    break;
                case "BAUTL":
                    retValue = BAUTL;
                    break;
                case "EQKTX":
                    retValue = EQKTX;
                    break;
                case "EQUNR":
                    retValue = EQUNR;
                    break;
                case "INGRP":
                    retValue = INGRP;
                    break;
                case "IWERK":
                    retValue = IWERK;
                    break;
                case "PLTXT":
                    retValue = PLTXT;
                    break;
                case "PRIOK":
                    retValue = PRIOK;
                    break;
                case "PRUEFLOS":
                    retValue = PRUEFLOS;
                    break;
                case "QMART":
                    retValue = QMART;
                    break;
                case "QMNUM":
                    retValue = QMNUM;
                    break;
                case "QMTXT":
                    retValue = QMTXT;
                    break;
                case "TPLNR":
                    retValue = TPLNR;
                    break;
                case "WAPOS":
                    retValue = WAPOS;
                    break;
                case "WARPL":
                    retValue = WARPL;
                    break;
                case "STRMN":
                    retValue = STRMN.ToString();
                    break;
                case "GEWRK":
                    retValue = GEWRK;
                    break;
                case "EQFNR":
                    retValue = EQFNR;
                    break;
            }
            return retValue;
        }

        public class BaseNotifSort : IComparer<Notif>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseNotifSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Notif x, Notif y)
            {
                switch (_compareField.ToUpper())
                {
                    case "ABNUM":
                        if (_desc)
                            return x.ABNUM.CompareTo(y.ABNUM);
                         return y.ABNUM.CompareTo(x.ABNUM);
                    case "ARTPR":
                        if (_desc)
                            return x.ARTPR.CompareTo(y.ARTPR);
                         return y.ARTPR.CompareTo(x.ARTPR);
                    case "AUFNR":
                        if (_desc)
                            return x.AUFNR.CompareTo(y.AUFNR);
                         return y.AUFNR.CompareTo(x.AUFNR);
                    case "BAUTL":
                        if (_desc)
                            return x.BAUTL.CompareTo(y.BAUTL);
                         return y.BAUTL.CompareTo(x.BAUTL);
                    case "EQKTX":
                        if (_desc)
                            return x.EQKTX.CompareTo(y.EQKTX);
                         return y.EQKTX.CompareTo(x.EQKTX);
                    case "EQUNR":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "INGRP":
                        if (_desc)
                            return x.INGRP.CompareTo(y.INGRP);
                         return y.INGRP.CompareTo(x.INGRP);
                    case "IWERK":
                        if (_desc)
                            return x.IWERK.CompareTo(y.IWERK);
                         return y.IWERK.CompareTo(x.IWERK);
                    case "PLTXT":
                        if (_desc)
                            return x.PLTXT.CompareTo(y.PLTXT);
                         return y.PLTXT.CompareTo(x.PLTXT);
                    case "PRIOK":
                        if (_desc)
                            return x.PRIOK.CompareTo(y.PRIOK);
                         return y.PRIOK.CompareTo(x.PRIOK);
                    case "PRUEFLOS":
                        if (_desc)
                            return x.PRIOK.CompareTo(y.PRIOK);
                         return y.PRIOK.CompareTo(x.PRIOK);
                    case "QMART":
                        if (_desc)
                            return x.QMART.CompareTo(y.QMART);
                         return y.QMART.CompareTo(x.QMART);
                    case "QMNUM":
                        if (_desc)
                            return x.QMNUM.CompareTo(y.QMNUM);
                         return y.QMNUM.CompareTo(x.QMNUM);
                    case "QMTXT":
                        if (_desc)
                            return x.QMTXT.CompareTo(y.QMTXT);
                         return y.QMTXT.CompareTo(x.QMTXT);
                    case "TPLNR":
                        if (_desc)
                            return x.TPLNR.CompareTo(y.TPLNR);
                         return y.TPLNR.CompareTo(x.TPLNR);
                    case "WAPOS":
                        if (_desc)
                            return x.WAPOS.CompareTo(y.WAPOS);
                         return y.WAPOS.CompareTo(x.WAPOS);
                    case "WARPL":
                        if (_desc)
                            return x.WARPL.CompareTo(y.WARPL);
                        return y.WARPL.CompareTo(x.WARPL);
                    case "GEWRK":
                        if (_desc)
                            return x.GEWRK.CompareTo(y.GEWRK);
                        return y.GEWRK.CompareTo(x.GEWRK);
                    case "EQFNR":
                        if (_desc)
                            return x.EQFNR.CompareTo(y.EQFNR);
                        return y.EQFNR.CompareTo(x.EQFNR);
                    case "MSGRP":
                        if (_desc)
                            return x.MSGRP.CompareTo(y.MSGRP);
                        return x.MSGRP.CompareTo(y.MSGRP);
                    case "STORT":
                        if (_desc)
                            return x.STORT.CompareTo(y.STORT);
                        return x.STORT.CompareTo(y.STORT);
                    default:
                        return (x).ABNUM.CompareTo(y.ABNUM);
                }
            }
        }


    }

    public class BaseNotifItem
    {
        public List<NotifActivity> _notifItemActivities = new List<NotifActivity>();
        public List<NotifCause> _notifItemCauses = new List<NotifCause>();
        public List<NotifTask> _notifItemTasks = new List<NotifTask>();
        protected String BAUTL = "";
        protected DateTime ERZEIT = DateTime.Now;
        protected String FECOD = "";
        protected String FEGRP = "";
        protected String FEKAT = "";

        protected String FENUM = "",
                      FETXT = "";

        protected String FEVER = "";

        protected String OTEIL = "";

        protected String OTGRP = "";
        protected String OTKAT = "";

        protected String OTVER = "";

        protected String POSNR = "";

        protected String QMNUM = "";
        protected String UPDFLAG = "";
        protected String mobileKey = "";

        public BaseNotifItem() { }
        public BaseNotifItem(Notif notif)
        {
            QMNUM = notif.Qmnum;
        }
        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }
        public string Bautl
        {
            get { return BAUTL; }
            set { BAUTL = value; }
        }

        public DateTime Erzeit
        {
            get { return ERZEIT; }
            set { ERZEIT = value; }
        }

        public string Fecod
        {
            get { return FECOD; }
            set { FECOD = value; }
        }

        public string Fegrp
        {
            get { return FEGRP; }
            set { FEGRP = value; }
        }

        public string Fekat
        {
            get { return FEKAT; }
            set { FEKAT = value; }
        }

        public string Fenum
        {
            get { return FENUM; }
            set { FENUM = value; }
        }

        public string Fetxt
        {
            get { return FETXT; }
            set { FETXT = value; }
        }

        public string Fever
        {
            get { return FEVER; }
            set { FEVER = value; }
        }

        public string Oteil
        {
            get { return OTEIL; }
            set { OTEIL = value; }
        }

        public string Otgrp
        {
            get { return OTGRP; }
            set { OTGRP = value; }
        }

        public string Otkat
        {
            get { return OTKAT; }
            set { OTKAT = value; }
        }

        public string Otver
        {
            get { return OTVER; }
            set { OTVER = value; }
        }

        public string Posnr
        {
            get { return POSNR; }
            set { POSNR = value; }
        }

        public string Qmnum
        {
            get { return QMNUM; }
            set { QMNUM = value; }
        }

        public string MobileKey
        {
            get { return mobileKey; }
            set { mobileKey = value; }
        }
    }

    public class BaseNotifActivity
    {
        protected String BAUTL = "";

        protected DateTime ERLDAT, ERLZEIT, ERZEIT;

        protected String FENUM = "";

        protected String MANUM = "";

        protected String MATXT = "";

        protected String MNCOD = "";

        protected String MNGRP = "";
        protected String MNKAT = "";

        protected String MNVER = "";

        protected String PARNR = "";

        protected String PARVW = "";

        protected DateTime PETER = DateTime.Now;

        protected DateTime PETUR = DateTime.Now;

        protected DateTime PSTER = DateTime.Now;
        protected DateTime PSTUR = DateTime.Now;
        protected String QMNUM = "";
        protected String URNUM = "";
        protected String UPDFLAG = "";
        protected String _mobileKey = "";
        protected String _erlnam = "";

        public BaseNotifActivity() { }
        public BaseNotifActivity(Notif notif)
        {
            QMNUM = notif.Qmnum;
        }

        public string Qmnum
        {
            get { return QMNUM; }
            set { QMNUM = value; }
        }
        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }
        public string Manum
        {
            get { return MANUM; }
            set { MANUM = value; }
        }

        public string Mnkat
        {
            get { return MNKAT; }
            set { MNKAT = value; }
        }

        public string Mngrp
        {
            get { return MNGRP; }
            set { MNGRP = value; }
        }

        public string Mncod
        {
            get { return MNCOD; }
            set { MNCOD = value; }
        }

        public string Mnver
        {
            get { return MNVER; }
            set { MNVER = value; }
        }

        public string Matxt
        {
            get { return MATXT; }
            set { MATXT = value; }
        }

        public string Fenum
        {
            get { return FENUM; }
            set { FENUM = value; }
        }

        public string Urnum
        {
            get { return URNUM; }
            set { URNUM = value; }
        }

        public string Parvw
        {
            get { return PARVW; }
            set { PARVW = value; }
        }

        public string Parnr
        {
            get { return PARNR; }
            set { PARNR = value; }
        }

        public string Bautl
        {
            get { return BAUTL; }
            set { BAUTL = value; }
        }

        public DateTime Pster
        {
            get { return PSTER; }
            set { PSTER = value; }
        }

        public DateTime Peter
        {
            get { return PETER; }
            set { PETER = value; }
        }

        public DateTime Pstur
        {
            get { return PSTUR; }
            set { PSTUR = value; }
        }

        public DateTime Petur
        {
            get { return PETUR; }
            set { PETUR = value; }
        }

        public DateTime Erldat
        {
            get { return ERLDAT; }
            set { ERLDAT = value; }
        }

        public DateTime Erlzeit
        {
            get { return ERLZEIT; }
            set { ERLZEIT = value; }
        }

        public DateTime Erzeit
        {
            get { return ERZEIT; }
            set { ERZEIT = value; }
        }

        public string MobileKey
        {
            get { return _mobileKey; }
            set { _mobileKey = value; }
        }

        public string Erlnam
        {
            get { return _erlnam; }
            set { _erlnam = value; }
        }
    }

    public class BaseNotifCause
    {
        protected String BAUTL = "";
        protected DateTime ERZEIT = DateTime.Now;
        protected String FENUM = "";

        protected String QMNUM = "";
        protected String QURNUM = "";
        protected String URCOD = "";
        protected String URGRP = "";
        protected String URKAT = "";

        protected String URNUM = "",
                      URTXT = "";

        protected String URVER = "";
        protected String UPDFLAG = "";
        protected String _mobileKey = "";
        public BaseNotifCause() { }
        public BaseNotifCause(Notif notif)
        {
            QMNUM = notif.Qmnum;
        }

        public string Qmnum
        {
            get { return QMNUM; }
            set { QMNUM = value; }
        }

        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }
        public string Fenum
        {
            get { return FENUM; }
            set { FENUM = value; }
        }

        public string Urnum
        {
            get { return URNUM; }
            set { URNUM = value; }
        }

        public string Urtxt
        {
            get { return URTXT; }
            set { URTXT = value; }
        }

        public string Urkat
        {
            get { return URKAT; }
            set { URKAT = value; }
        }

        public string Urgrp
        {
            get { return URGRP; }
            set { URGRP = value; }
        }

        public string Urcod
        {
            get { return URCOD; }
            set { URCOD = value; }
        }

        public string Urver
        {
            get { return URVER; }
            set { URVER = value; }
        }

        public string Bautl
        {
            get { return BAUTL; }
            set { BAUTL = value; }
        }

        public string Qurnum
        {
            get { return QURNUM; }
            set { QURNUM = value; }
        }

        public DateTime Erzeit
        {
            get { return ERZEIT; }
            set { ERZEIT = value; }
        }

        public string MobileKey
        {
            get { return _mobileKey; }
            set { _mobileKey = value; }
        }
    }

    public class BaseNotifTask
    {
        protected DateTime ERZEIT = DateTime.Now;
        protected String FENUM = "";

        protected String MANUM = "";

        protected String MATXT = "";

        protected String MNCOD = "";

        protected String MNGFA = "";

        protected String MNGRP = "";
        protected String MNKAT = "";
        protected String MNVER = "";
        protected DateTime PETER = DateTime.Now;

        protected DateTime PETUR = DateTime.Now;

        protected DateTime PSTER = DateTime.Now;

        protected DateTime PSTUR = DateTime.Now;
        protected String QMANUM = "";
        protected String QMNUM = "";
        protected String URNUM = "";
        protected String UPDFLAG = "";
        protected String _mobileKey = "";

        public BaseNotifTask() { }
        public BaseNotifTask(Notif notif)
        {
            QMNUM = notif.Qmnum;
        }

        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }
        public string Qmnum
        {
            get { return QMNUM; }
            set { QMNUM = value; }
        }

        public string Manum
        {
            get { return MANUM; }
            set { MANUM = value; }
        }

        public string Fenum
        {
            get { return FENUM; }
            set { FENUM = value; }
        }

        public string Urnum
        {
            get { return URNUM; }
            set { URNUM = value; }
        }

        public string Mnkat
        {
            get { return MNKAT; }
            set { MNKAT = value; }
        }

        public string Mngrp
        {
            get { return MNGRP; }
            set { MNGRP = value; }
        }

        public string Mncod
        {
            get { return MNCOD; }
            set { MNCOD = value; }
        }

        public string Mnver
        {
            get { return MNVER; }
            set { MNVER = value; }
        }

        public string Matxt
        {
            get { return MATXT; }
            set { MATXT = value; }
        }

        public string Mngfa
        {
            get { return MNGFA; }
            set { MNGFA = value; }
        }

        public string Qmanum
        {
            get { return QMANUM; }
            set { QMANUM = value; }
        }

        public DateTime Pstur
        {
            get { return PSTUR; }
            set { PSTUR = value; }
        }

        public DateTime Petur
        {
            get { return PETUR; }
            set { PETUR = value; }
        }

        public DateTime Erzeit
        {
            get { return ERZEIT; }
            set { ERZEIT = value; }
        }

        public DateTime Pster
        {
            get { return PSTER; }
            set { PSTER = value; }
        }

        public DateTime Peter
        {
            get { return PETER; }
            set { PETER = value; }
        }

        public string MobileKey
        {
            get { return _mobileKey; }
            set { _mobileKey = value; }
        }
    }
}