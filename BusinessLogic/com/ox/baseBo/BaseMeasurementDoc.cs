﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMeasurementDoc : OxBusinessObject
    {
        public String POINT,
                       MDOCM,
                       MDTXT,
                       READR,
                       RECDC,
                       UNITR,
                       UPDFLAG;

        public string UpdFlag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }

        protected DateTime IDATE, ITIME;

        public BaseMeasurementDoc()
        {
            POINT = "";
            MDOCM = "";
            MDTXT = "";
            READR = AppConfig.UserId;
            RECDC = "";
            UNITR = "";
            UPDFLAG = "";
            IDATE = DateTime.Now;
            ITIME = DateTime.Now;
        }

        protected BaseMeasurementDoc(MeasurementPoint point)
        {
            MDOCM = "";
            MDTXT = "";
            READR = AppConfig.UserId;
            RECDC = "";
            POINT = point.Point;
            UNITR = point.Mrngu;
            IDATE = DateTime.Now;
            ITIME = DateTime.Now;
            UPDFLAG = "";
        }

        public string Point
        {
            get { return POINT; }
            set { POINT = value; }
        }

        public string Mdocm
        {
            get { return MDOCM; }
            set { MDOCM = value; }
        }

        public string Mdtxt
        {
            get { return MDTXT; }
            set { MDTXT = value; }
        }

        public string Readr
        {
            get { return READR; }
            set { READR = value; }
        }

        public string Recdc
        {
            get { return RECDC; }
            set { RECDC = value; }
        }

        public string Unitr
        {
            get { return UNITR; }
            set { UNITR = value; }
        }

        public DateTime Idate
        {
            get { return IDATE; }
            set { IDATE = value; }
        }

        public DateTime Itime
        {
            get { return ITIME; }
            set { ITIME = value; }
        }
        public class BaseMeasurementDocSort : IComparer<MeasurementDoc>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseMeasurementDocSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(MeasurementDoc x, MeasurementDoc y)
            {
                switch (_compareField.ToLower())
                {
                    case "POINT":
                        if (_desc)
                            return x.Point.CompareTo(y.Point);
                        return y.Point.CompareTo(x.Point);
                    case "MDOCM":
                        if (_desc)
                            return x.MDOCM.CompareTo(y.MDOCM);
                        return y.MDOCM.CompareTo(x.MDOCM);
                    case "MDTXT":
                        if (_desc)
                            return x.MDTXT.CompareTo(y.MDTXT);
                        return y.MDTXT.CompareTo(x.MDTXT);
                    case "READR":
                        if (_desc)
                            return x.READR.CompareTo(y.READR);
                        return y.READR.CompareTo(x.READR);
                    case "RECDC":
                        if (_desc)
                            return x.RECDC.CompareTo(y.RECDC);
                        return y.RECDC.CompareTo(x.RECDC);
                    case "UNITR":
                        if (_desc)
                            return x.UNITR.CompareTo(y.UNITR);
                        return y.UNITR.CompareTo(x.UNITR);
                    case "UPDFLAG":
                        if (_desc)
                            return x.UPDFLAG.CompareTo(y.UPDFLAG);
                        return y.UPDFLAG.CompareTo(x.UPDFLAG);
                    case "IDATE":
                        if (_desc)
                            return x.IDATE.CompareTo(y.IDATE);
                        return y.IDATE.CompareTo(x.IDATE);
                    case "ITIME":
                        if (_desc)
                            return x.ITIME.CompareTo(y.ITIME);
                        return y.ITIME.CompareTo(x.ITIME);
                    default:
                        return (x).Point.CompareTo(y.Point);
                }
            }
        }
    }
}
