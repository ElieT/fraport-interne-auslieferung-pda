﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCustBersManager
    {
        public static List<CustBers> GetCustBers()
        {
            var retList = new List<CustBers>();
            try
            {
                String sqlStmt = "SELECT * FROM G_CUSTBERS";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var custBers = new CustBers();
                    custBers.Rbnr = rdr["RBNR"];
                    custBers.Qkatart = rdr["QKATART"];
                    custBers.Qcodegrp = rdr["QCODEGRP"];
                    retList.Add(custBers);
                }
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }
    }
}
