﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCustomizingManager
    {
        public static Cust_012 GetCust_AM_012()
        {
            var cust = Cust_012.GetInstance();
            try
            {
                var sqlStmt = "SELECT * FROM C_AM_CUST_012 ";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    if (rdr["SCENARIO"] is DBNull || rdr["SCENARIO"] == null)
                        cust.Scenario = "";
                    else
                        cust.Scenario = rdr["SCENARIO"];
                    if (rdr["ORD_ACTIVE"].Equals("X"))
                        cust.OrdActive = true;
                    else
                        cust.OrdActive = false;
                    if (rdr["ORD_CREAT_ACTIVE"].Equals("X"))
                        cust.OrdCreatActive = true;
                    else
                        cust.OrdCreatActive = false;
                    if (rdr["NOTI_ACTIVE"].Equals("X"))
                        cust.NotiActive = true;
                    else
                        cust.NotiActive = false;
                    if (rdr["NOTI_CREA_ACTIVE"].Equals("X"))
                        cust.NotiCreaActive = true;
                    else
                        cust.NotiCreaActive = false;
                    if (rdr["CONF_REP_ACTIVE"].Equals("X"))
                        cust.ConfRepActive = true;
                    else
                        cust.ConfRepActive = false;
                    if (rdr["CALENDER_ACTIVE"].Equals("X"))
                        cust.CalendarActive = true;
                    else
                        cust.CalendarActive = false;
                    if (rdr["RFID_ACTIVE"].Equals("X"))
                        cust.RfidActive = true;
                    else
                        cust.RfidActive = false;
                    if (rdr["FLOC_ACTIVE"].Equals("X"))
                        cust.FlocActive = true;
                    else
                        cust.FlocActive = false;
                    if (rdr["FLOC_CL_ACTIVE"].Equals("X"))
                        cust.FlocClActive = true;
                    else
                        cust.FlocClActive = false;
                    if (rdr["FLOC_MP_ACTIVE"].Equals("X"))
                        cust.FlocMpActive = true;
                    else
                        cust.FlocMpActive = false;
                    if (rdr["FLOC_CHKL_ACTIVE"].Equals("X"))
                        cust.FlocChklActive = true;
                    else
                        cust.FlocChklActive = false;
                    if (rdr["FLOC_CHANGE"].Equals("X"))
                        cust.FlocChange = true;
                    else
                        cust.FlocChange = false;
                    if (rdr["EQUI_ACTIVE"].Equals("X"))
                        cust.EquiActive = true;
                    else
                        cust.EquiActive = false;
                    if (rdr["EQUI_CL_ACTIVE"].Equals("X"))
                        cust.EquiClActive = true;
                    else
                        cust.EquiClActive = false;
                    if (rdr["EQUI_MP_ACTIVE"].Equals("X"))
                        cust.EquiMpActive = true;
                    else
                        cust.EquiMpActive = false;
                    if (rdr["EQUI_CHKL_ACTIVE"].Equals("X"))
                        cust.EquiChklActive = true;
                    else
                        cust.EquiChklActive = false;
                    if (rdr["EQUI_CHANGE"].Equals("X"))
                        cust.EquiChange = true;
                    else
                        cust.EquiChange = false;
                    if (rdr["TIMECONF_ACTIVE"].Equals("X"))
                        cust.TimeConfActive = true;
                    else
                        cust.TimeConfActive = false;
                    if (rdr["MATCONF_ACTIVE"].Equals("X"))
                        cust.MatConfActive = true;
                    else
                        cust.MatConfActive = false;
                    if (rdr["ATTACH_ACTIVE"].Equals("X"))
                        cust.AttachActive = true;
                    else
                        cust.AttachActive = false;
                    if (rdr["PDF_REP_ACTIVE"].Equals("X"))
                        cust.PdfRepActive = true;
                    else
                        cust.PdfRepActive = false;
                    if (rdr["ORD_STAT_ACTIVE"].Equals("X"))
                        cust.OrdStatActive = true;
                    else
                        cust.OrdStatActive = false;
                    if (rdr["ORD_TECHC_ACTIVE"].Equals("X"))
                        cust.OrdTechcActive = true;
                    else
                        cust.OrdTechcActive = false;
                    if (rdr["NOTI_POS_ACTIVE"].Equals("X"))
                        cust.NotiPosActive = true;
                    else
                        cust.NotiPosActive = false;
                    if (rdr["NOTI_URS_ACTIVE"].Equals("X"))
                        cust.NotiUrsActive = true;
                    else
                        cust.NotiUrsActive = false;
                    if (rdr["NOTI_MAS_ACTIVE"].Equals("X"))
                        cust.NotiMasActive = true;
                    else
                        cust.NotiMasActive = false;
                    if (rdr["NOTI_AKT_ACTIVE"].Equals("X"))
                        cust.NotiAktActive = true;
                    else
                        cust.NotiAktActive = false;
                    if (rdr["NOTI_OBJ_ACTIVE"].Equals("X"))
                        cust.NotiObjActive = true;
                    else
                        cust.NotiObjActive = false;
                    if (rdr["ORD_SPLIT_ACTIVE"].Equals("X"))
                        cust.OrdSplitActive = true;
                    else
                        cust.OrdSplitActive = false;
                    if (rdr["MAT_STL_ACTIVE"].Equals("X"))
                        cust.MatStlActive = true;
                    else
                        cust.MatStlActive = false;
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return cust;
        }

        public static List<Cust_011> GetCust_AM_011()
        {
            var custList = new List<Cust_011>();
            try
            {
                var sqlStmt = "SELECT * FROM C_AM_CUST_011";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var cust = new Cust_011();
                    if (rdr["SCENARIO"] is DBNull || rdr["SCENARIO"] == null)
                        cust.Scenario = "";
                    else
                        cust.Scenario = rdr["SCENARIO"];
                    cust.Auart = rdr["AUART"];
                    cust.Txt = rdr["TXT"];
                    cust.CreateAllowed = rdr["CREATE_ALLOWED"];
                    cust.Steus = rdr["STEUS"];
                    cust.FlagRelease = rdr["FLAG_RELEASE"];
                    cust.WithMeasDoc = rdr["WITH_MEAS_DOC"];
                    cust.PartnerTransfer = rdr["PARTNER_TRANSFER"];
                    cust.Qmart = rdr["QMART"];
                    cust.Ilart = rdr["ILART"];
                    cust.Service = rdr["SERVICE"];
                    cust.PdfRepActive = rdr["PDF_REP_ACTIVE"];
                    custList.Add(cust);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return custList;
        }

        public static Cust_011 GetCust_AM_011(String auart)
        {
            var cust = new Cust_011();
            try
            {

                var sqlStmt = "SELECT * FROM C_AM_CUST_011 WHERE AUART = @AUART";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUART", auart);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    if (rdr["SCENARIO"] is DBNull || rdr["SCENARIO"] == null)
                        cust.Scenario = "";
                    else
                        cust.Scenario = rdr["SCENARIO"];
                    cust.Auart = rdr["AUART"];
                    cust.Txt = rdr["TXT"];
                    cust.CreateAllowed = rdr["CREATE_ALLOWED"];
                    cust.Steus = rdr["STEUS"];
                    cust.FlagRelease = rdr["FLAG_RELEASE"];
                    cust.WithMeasDoc = rdr["WITH_MEAS_DOC"];
                    cust.PartnerTransfer = rdr["PARTNER_TRANSFER"];
                    cust.Qmart = rdr["QMART"];
                    cust.Ilart = rdr["ILART"];
                    cust.Service = rdr["SERVICE"];
                    cust.PdfRepActive = rdr["PDF_REP_ACTIVE"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return cust;
        }

        public static Cust_001 GetCust_AM_001()
        {
            var cust = Cust_001.GetInstance();
            try
            {
                var sqlStmt = "SELECT * FROM C_AM_CUST_001 ";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    cust.OrderScenario = rdr["ORDER_SCENARIO"];
                    cust.OrderWorkcenter = rdr["ORDER_WORKCENTER"];
                    cust.OrderPlantWrkc = rdr["ORDER_PLANT_WRKC"];
                    cust.OrderPernr = rdr["ORDER_PERNR"];
                    cust.OrderPlangroup = rdr["ORDER_PLANGROUP"];
                    cust.OrderPlanPlant = rdr["ORDER_PLAN_PLANT"];
                    cust.OrderParRole = rdr["ORDER_PAR_ROLE"];
                    cust.OrderPartnerNo = rdr["ORDER_PARTNER_NO"];
                    cust.NotifScenario = rdr["NOTIF_SCENARIO"];
                    cust.NotifWorkcenter = rdr["NOTIF_WORKCENTER"];
                    cust.NotifPlantWrkc = rdr["NOTIF_PLANT_WRKC"];
                    cust.NotifPlanGroup = rdr["NOTIF_PLANGROUP"];
                    cust.NotifPlanPlant = rdr["NOTIF_PLAN_PLANT"];
                    cust.NotifPernr = rdr["NOTIF_PERNR"];
                    cust.NotifParRole = rdr["NOTIF_PAR_ROLE"];
                    cust.NotifPartnerNo = rdr["NOTIF_PARTNER_NO"]; ;
                    cust.VariTpl = rdr["VARI_TPL"];
                    cust.ChangeTpl = rdr["CHANGE_TPL"];
                    cust.VariEqui = rdr["VARI_EQUI"];
                    cust.ChangeEqui = rdr["CHANGE_EQUI"];
                    cust.VariClass = rdr["VARI_CLASS"];
                    cust.Sprache = rdr["SPRACHE"];
                    cust.LartDefault = rdr["LART_DEFAULT"];
                    cust.Intern = rdr["INTERN"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return cust;
            }
            return cust;
        }

        public static new Dictionary<string, CustIlartOrtype> GetCustIlartOrtype(String auart)
        {
            var retVal = new Dictionary<string, CustIlartOrtype>();
            try
            {
                var sqlStmt = "SELECT * FROM C_AM_CUST_ILART_ORTYPE WHERE AUART = @AUART";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUART", auart);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var cust = new CustIlartOrtype();
                    cust.Auart = rdr["AUART"];
                    cust.Ilart = rdr["ILART"];
                    cust.Ilatx = rdr["ILATX"];
                    retVal.Add(rdr["ILART"], cust);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return retVal;
            }
            return retVal;
        }

        public static CustIlartOrtype GetOrderIlart(String auart, String ilart)
        {
            var retVal = new CustIlartOrtype();
            try
            {
                var sqlStmt = "SELECT * FROM C_AM_CUST_ILART_ORTYPE WHERE AUART = @AUART AND ILART = @ILART";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUART", auart);
                cmd.Parameters.AddWithValue("@Ilart", ilart);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if(records.Count == 1)
                {
                    retVal.Auart = records[0]["AUART"];
                    retVal.Ilart = records[0]["ILART"];
                    retVal.Ilatx = records[0]["ILATX"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return retVal;
            }
            return retVal;
        }

        public static List<CustNotifType> GetCustNotifTypes()
        {
            var retVal = new List<CustNotifType>();
            try
            {
                var sqlStmt = "SELECT * FROM C_CUSTNOTIFTYPE";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var cust = new CustNotifType();
                    cust.Qmart = rdr["QMART"];
                    cust.Rbnr = rdr["RBNR"];
                    cust.Auart = rdr["AUART"];
                    cust.Stsma = rdr["STSMA"];
                    cust.Fekat = rdr["FEKAT"];
                    cust.Urkat = rdr["URKAT"];
                    cust.Makat = rdr["MAKAT"];
                    cust.Mfkat = rdr["MFKAT"];
                    cust.Otkat = rdr["OTKAT"];
                    cust.Sakat = rdr["SAKAT"];
                    cust.CreateAllowed = rdr["CREATE_ALLOWED"];
                    cust.Qmartx = rdr["QMARTX"];
                    retVal.Add(cust);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return retVal;
            }
            return retVal;
        }

        public static List<CustNotifType> GetCustNotifType(String qmart)
        {
            var retVal = new List<CustNotifType>();
            try
            {
                var sqlStmt = "SELECT * FROM C_CUSTNOTIFTYPE WHERE QMART = @QMART";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMART", qmart);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var cust = new CustNotifType();
                    cust.Qmart = rdr["QMART"];
                    cust.Rbnr = rdr["RBNR"];
                    cust.Auart = rdr["AUART"];
                    cust.Stsma = rdr["STSMA"];
                    cust.Fekat = rdr["FEKAT"];
                    cust.Urkat = rdr["URKAT"];
                    cust.Makat = rdr["MAKAT"];
                    cust.Mfkat = rdr["MFKAT"];
                    cust.Otkat = rdr["OTKAT"];
                    cust.Sakat = rdr["SAKAT"];
                    cust.CreateAllowed = rdr["CREATE_ALLOWED"];
                    cust.Qmartx = rdr["QMARTX"];
                    retVal.Add(cust);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return retVal;
            }
            return retVal;
        }

        public static void GetCust_MC_010()
        {
            try
            {
                var sqlStmt = "SELECT * FROM C_MC_CUST_010 ";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    if (!(rdr["TRACE_FILESIZE"] is DBNull || rdr["TRACE_FILESIZE"] == null || String.IsNullOrEmpty(rdr["TRACE_FILESIZE"].Trim('0'))))
                        AppConfig.TraceFileSize = rdr["TRACE_FILESIZE"].TrimStart('0');

                    if (!(rdr["RFID_TIMEOUT"] is DBNull || rdr["RFID_TIMEOUT"] == null || String.IsNullOrEmpty(rdr["RFID_TIMEOUT"].Trim('0'))))
                        AppConfig.RFIDTimeout = rdr["RFID_TIMEOUT"].TrimStart('0');

                    if (!(rdr["RFID_PORTNAME_CE"] is DBNull || rdr["RFID_PORTNAME_CE"] == null || String.IsNullOrEmpty(rdr["RFID_PORTNAME_CE"])))
                    {
                        rdr["RFID_PORTNAME_CE"] = rdr["RFID_PORTNAME_CE"].Trim();
                        rdr["RFID_PORTNAME_CE"] = rdr["RFID_PORTNAME_CE"].TrimEnd(':');
                        rdr["RFID_PORTNAME_CE"] += ":";
                        AppConfig.RFIDPortName_CE = rdr["RFID_PORTNAME_CE"];
                    }
                    if (!(rdr["RFID_PORTNAME_W32"] is DBNull || rdr["RFID_PORTNAME_W32"] == null || String.IsNullOrEmpty(rdr["RFID_PORTNAME_W32"])))
                    {
                        rdr["RFID_PORTNAME_W32"] = rdr["RFID_PORTNAME_W32"].Trim();
                        rdr["RFID_PORTNAME_W32"] = rdr["RFID_PORTNAME_W32"].TrimEnd(':');
                        rdr["RFID_PORTNAME_W32"] += ":";
                        AppConfig.RFIDPortName_W32 = rdr["RFID_PORTNAME_W32"];
                    }
                    if (!(rdr["RFID_PORTNAME_W32"] is DBNull || rdr["RFID_PORTNAME_W32"] == null || String.IsNullOrEmpty(rdr["RFID_PORTNAME_W32"])))
                        AppConfig.RFIDPortType_CE = rdr["RFID_PORTTYPE_CE"];
                    if (!(rdr["RFID_PORTNAME_W32"] is DBNull || rdr["RFID_PORTNAME_W32"] == null || String.IsNullOrEmpty(rdr["RFID_PORTNAME_W32"])))
                        AppConfig.RFIDPortType_W32 = rdr["RFID_PORTTYPE_W32"];
                    if (!(rdr["RFID_TIMEOUT_DR_CE"] is DBNull || rdr["RFID_TIMEOUT_DR_CE"] == null || String.IsNullOrEmpty(rdr["RFID_TIMEOUT_DR_CE"].Trim('0'))))
                        AppConfig.RFIDDriverTimeOut_CE = rdr["RFID_TIMEOUT_DR_CE"].TrimStart('0');
                    if (!(rdr["RFID_TIMEOUT_DR_W32"] is DBNull || rdr["RFID_TIMEOUT_DR_W32"] == null || String.IsNullOrEmpty(rdr["RFID_TIMEOUT_DR_W32"].Trim('0'))))
                        AppConfig.RFIDDriverTimeOut_W32 = rdr["RFID_TIMEOUT_DR_W32"].TrimStart('0');


                    if (!(rdr["RFID_WRITE_VERIFY"] is DBNull) && rdr["RFID_WRITE_VERIFY"] != null && rdr["RFID_WRITE_VERIFY"].Equals("-"))
                        AppConfig.RFIDWriteVerify = false;
                    else if (!(rdr["RFID_WRITE_VERIFY"] is DBNull) && rdr["RFID_WRITE_VERIFY"] != null && rdr["RFID_WRITE_VERIFY"].Equals("X"))
                        AppConfig.RFIDWriteVerify = true;


                    if (!(rdr["STATUSMESSAGE_TIMEOUT"] is DBNull || rdr["STATUSMESSAGE_TIMEOUT"] == null || String.IsNullOrEmpty(rdr["STATUSMESSAGE_TIMEOUT"].Trim('0'))))
                        AppConfig.MessageTimeout = int.Parse(rdr["STATUSMESSAGE_TIMEOUT"]);
                    if (!(rdr["FILE_FOLDER_W32"] is DBNull || rdr["FILE_FOLDER_W32"] == null || String.IsNullOrEmpty(rdr["FILE_FOLDER_W32"])))
                        AppConfig.DataFileFolder_W32 = rdr["FILE_FOLDER_W32"];
                    if (!(rdr["FILE_FOLDER_CE"] is DBNull || rdr["FILE_FOLDER_CE"] == null || String.IsNullOrEmpty(rdr["FILE_FOLDER_CE"])))
                        AppConfig.DataFileFolder_CE = rdr["FILE_FOLDER_CE"];
                    if (!(rdr["MESSAGE_DISPLAY_MODE"] is DBNull || rdr["MESSAGE_DISPLAY_MODE"] == null || String.IsNullOrEmpty(rdr["MESSAGE_DISPLAY_MODE"].Trim('0'))))
                        AppConfig.MessageDisplayLevel = rdr["MESSAGE_DISPLAY_MODE"];
                    if (!(rdr["SVM_TEMP_DIR_CE"] is DBNull || rdr["SVM_TEMP_DIR_CE"] == null || String.IsNullOrEmpty(rdr["SVM_TEMP_DIR_CE"])))
                        AppConfig.SVMTempDirectory_CE = rdr["SVM_TEMP_DIR_CE"];
                    if (!(rdr["SVM_TEMP_DIR_W32"] is DBNull || rdr["SVM_TEMP_DIR_W32"] == null || String.IsNullOrEmpty(rdr["SVM_TEMP_DIR_W32"])))
                        AppConfig.SVMTempDirectory_W32 = rdr["SVM_TEMP_DIR_W32"];
                }
                if (records.Count > 0)
                    AppConfig.SaveSettings(AppConfig.ConfigFile);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}
