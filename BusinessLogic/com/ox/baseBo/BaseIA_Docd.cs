﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseIA_Docd
    {
        protected String ID,
                         LINENUMBER,
                         LINE;

        private String DIRTY;

        public string MobileKey { get; set;  }

        public string Id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string Linenumber
        {
            get { return LINENUMBER; }
            set { LINENUMBER = value; }
        }

        public string Line
        {
            get { return LINE; }
            set { LINE = value; }
        }

        public string Dirty
        {
            get { return DIRTY; }
            set { DIRTY = value; }
        }
    }
}
