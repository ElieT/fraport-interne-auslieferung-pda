﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseHRCharManager
    {
        #region Constructors (1)

        #endregion Constructors

        public ArrayList GetChars()
        {
            var retVal = new ArrayList();
            try
            {

                String sqlStmt = "SELECT * FROM [C_HRCHAR]";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var hrChar = new HRChar();
                    hrChar.id = (String) rdr["ID"];
                    hrChar.merkmal = (String) rdr["MERKMAL"];
                    retVal.Add(hrChar);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
    }
}