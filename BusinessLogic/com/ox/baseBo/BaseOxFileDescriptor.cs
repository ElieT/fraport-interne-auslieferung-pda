﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOxFileDescriptor
    {
        public enum FileStatus
        {
            Local, Downloading, Online
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        public string notifyChange
        {
            get { return ""; }
            set { OnPropertyChanged(new PropertyChangedEventArgs("")); }
        }

        private String _fileName;
        private String _fileSize;
        private String _fileDirectory;
        private FileStatus _currentFileStatus;
        private DateTime _fileCreationDate;
        private String _fileType;
        private String _fileDescription;
        private String _fileReferenceType;


        private String _fileReferenceObjectID;

        public String FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public String FileSize
        {
            get { return _fileSize; }
            set { _fileSize = value; }
        }

        public String FileDirectory
        {
            get { return _fileDirectory; }
            set { _fileDirectory = value; }
        }

        public FileStatus CurrentFileStatus
        {
            get { return _currentFileStatus; }
            set { _currentFileStatus = value; }
        }

        public DateTime FileCreationDate
        {
            get { return _fileCreationDate; }
            set { _fileCreationDate = value; }
        }

        public String FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        public String FileDescription
        {
            get { return _fileDescription; }
            set { _fileDescription = value; }
        }

        public String FileReferenceType
        {
            get { return _fileReferenceType; }
            set { _fileReferenceType = value; }
        }

        public String FileReferenceObjectID
        {
            get { return _fileReferenceObjectID; }
            set { _fileReferenceObjectID = value; }
        }

    }
}
