﻿using System;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCust_012
    {
        private static Cust_012 instance;

        protected BaseCust_012() 
        {
        
        }

        public String Scenario { get; set; }

        public bool OrdActive { get; set; }

        public bool OrdCreatActive { get; set; }

        public bool NotiActive { get; set; }

        public bool NotiCreaActive { get; set; }

        public bool ConfRepActive { get; set; }

        public bool CalendarActive { get; set; }

        public bool RfidActive { get; set; }

        public bool FlocActive { get; set; }

        public bool FlocClActive { get; set; }

        public bool FlocMpActive { get; set; }

        public bool FlocChklActive { get; set; }

        public bool FlocChange { get; set; }

        public bool EquiActive { get; set; }

        public bool EquiClActive { get; set; }

        public bool EquiMpActive { get; set; }

        public bool EquiChklActive { get; set; }

        public bool EquiChange { get; set; }

        public bool TimeConfActive { get; set; }

        public bool MatConfActive { get; set; }

        public bool AttachActive { get; set; }

        public bool PdfRepActive { get; set; }

        public bool OrdStatActive { get; set; }

        public bool OrdTechcActive { get; set; }

        public bool NotiPosActive { get; set; }

        public bool NotiUrsActive { get; set; }

        public bool NotiMasActive { get; set; }

        public bool NotiAktActive { get; set; }

        public bool NotiObjActive { get; set; }

        public bool OrdSplitActive { get; set; }

        public bool MatStlActive { get; set; }

    }
}
