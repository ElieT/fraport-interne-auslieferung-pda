﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDoch
    {
        protected string ID,
                       DOC_TYPE,
                       REF_TYPE,
                       REF_OBJECT,
                       DESCRIPTION,
                       SPRAS,
                       FILENAME,
                       EMAIL,
                       UPDFLAG;

        public String MobileKey { get; set;  }

        List<Docd> DOCDS = new List<Docd>();

        public List<Docd> Docds
        {
            get { return DOCDS; }
            set { DOCDS = value; }
        }

        public string Id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string DocType
        {
            get { return DOC_TYPE; }
            set { DOC_TYPE = value; }
        }

        public string RefType
        {
            get { return REF_TYPE; }
            set { REF_TYPE = value; }
        }

        public string RefObject
        {
            get { return REF_OBJECT; }
            set { REF_OBJECT = value; }
        }

        public string Description
        {
            get { return DESCRIPTION; }
            set { DESCRIPTION = value; }
        }

        public string Spras
        {
            get { return SPRAS; }
            set { SPRAS = value; }
        }

        public string Filename
        {
            get { return FILENAME; }
            set { FILENAME = value; }
        }

        public string Email
        {
            get { return EMAIL; }
            set { EMAIL = value; }
        }

        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }
    }
}
