﻿using System;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseNotifType : OxBusinessObject
    {
        protected String _qmart;
        protected String _rbnr;
        protected String _auart;
        protected String _stsma;
        protected String _fekat;
        protected String _urkat;
        protected String _makat;
        protected String _mfkat;
        protected String _otkat;
        protected String _sakat;
        protected String _createAllowed;
        protected String _qmartx;

        public BaseNotifType()
        {
            
        }

        public string Qmart
        {
            get { return _qmart; }
            set { _qmart = value; }
        }

        public string Rbnr
        {
            get { return _rbnr; }
            set { _rbnr = value; }
        }

        public string Auart
        {
            get { return _auart; }
            set { _auart = value; }
        }

        public string Stsma
        {
            get { return _stsma; }
            set { _stsma = value; }
        }

        public string Fekat
        {
            get { return _fekat; }
            set { _fekat = value; }
        }

        public string Urkat
        {
            get { return _urkat; }
            set { _urkat = value; }
        }

        public string Makat
        {
            get { return _makat; }
            set { _makat = value; }
        }

        public string Mfkat
        {
            get { return _mfkat; }
            set { _mfkat = value; }
        }

        public string Otkat
        {
            get { return _otkat; }
            set { _otkat = value; }
        }

        public string Sakat
        {
            get { return _sakat; }
            set { _sakat = value; }
        }

        public string CreateAllowed
        {
            get { return _createAllowed; }
            set { _createAllowed = value; }
        }

        public string Qmartx
        {
            get { return _qmartx; }
            set { _qmartx = value; }
        }
    }
}
