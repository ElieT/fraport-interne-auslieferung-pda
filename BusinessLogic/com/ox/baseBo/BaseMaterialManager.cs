﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMaterialManager
    {
        public static List<Material> GetMaterials()
        {
            var materials = new List<Material>();
            try
            {
                var sqlStmt = "SELECT C_MATERIAL.* FROM C_MATERIAL LEFT OUTER JOIN C_MATSTOCK ON C_MATERIAL.MATNR = C_MATSTOCK.MATNR";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var material = new Material();
                    material.Matnr = rdr["MATNR"];
                    material.Meins = rdr["MEINS"];
                    material.Maktx = rdr["MAKTX"];
                    materials.Add(material);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return materials;
        }

        public static Material GetMaterial(String matnr, Boolean inTransaction)
        {
            var material = new Material();
            try
            {
                var sqlStmt = "SELECT * FROM C_MATERIAL WHERE " + 
                                  "MATNR = @MATNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MATNR", matnr);
                List<Dictionary<string, string>> records;
                if(inTransaction) 
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    material.Matnr = rdr["MATNR"];
                    material.Meins = rdr["MEINS"];
                    material.Maktx = rdr["MAKTX"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return material;
        }

        public static MatStock GetMatStock(String matnr, String werks, String lgort)
        {
            var matStock = new MatStock();
            try
            {
                String sqlStmt = "SELECT C_MATSTOCK.MATNR, C_MATSTOCK.WERKS, C_MATSTOCK.LGORT, " +
                                  "C_MATSTOCK.LABST, C_MATSTOCK.MEINS, C_MATERIAL.MAKTX " +
                                  "FROM C_MATSTOCK LEFT OUTER JOIN C_MATERIAL ON C_MATSTOCK.MATNR = C_MATERIAL.MATNR " +
                                  "WHERE C_MATSTOCK.MATNR = @MATNR " +
                                  "AND C_MATSTOCK.WERKS = @WERKS " +
                                  "AND C_MATSTOCK.LGORT = @LGORT ";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MATNR", matnr);
                cmd.Parameters.AddWithValue("@WERKS", werks);
                cmd.Parameters.AddWithValue("@LGORT", lgort);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    matStock.Matnr = rdr["MATNR"];
                    matStock.Werks = rdr["WERKS"];
                    matStock.Lgort = rdr["LGORT"];
                    matStock.Labst = rdr["LABST"];
                    matStock.Meins = rdr["MEINS"];
                    matStock.Maktx = rdr["MAKTX"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return matStock;
        }

        public static List<MatStock> GetMatStocks(Material material)
        {
            var _matStocks = new List<MatStock>();
            try
            {
                var sqlStmt = "SELECT C_MATSTOCK.MATNR, C_MATSTOCK.WERKS, C_MATSTOCK.LGORT, " +
                                 "C_MATSTOCK.LABST, C_MATSTOCK.MEINS, C_MATERIAL.MAKTX " +
                                 " FROM C_MATSTOCK LEFT OUTER JOIN C_MATERIAL ON C_MATSTOCK.MATNR = C_MATERIAL.MATNR " +
                                 " WHERE C_MATSTOCK.MATNR = @MATNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MATNR", material.Matnr);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var matStock = new MatStock();
                    matStock.Matnr =  rdr["MATNR"];
                    matStock.Werks = rdr["WERKS"];
                    matStock.Lgort = rdr["LGORT"];
                    matStock.Labst = rdr["LABST"];
                    matStock.Meins = rdr["MEINS"];
                    matStock.Maktx = rdr["MAKTX"];
                    _matStocks.Add(matStock);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _matStocks;
        }

        public static List<MatStock> GetMatStocks()
        {
            var _matStocks = new List<MatStock>();
            try
            {
                String sqlStmt = "SELECT C_MATSTOCK.MATNR, C_MATSTOCK.WERKS, C_MATSTOCK.LGORT, " +
                                  "C_MATSTOCK.LABST, C_MATSTOCK.MEINS, C_MATERIAL.MAKTX " +
                                  " FROM C_MATSTOCK LEFT OUTER JOIN C_MATERIAL ON C_MATSTOCK.MATNR = C_MATERIAL.MATNR";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var matStock = new MatStock();
                    matStock.Matnr = (String)rdr["MATNR"];
                    matStock.Werks = (String)rdr["WERKS"];
                    matStock.Lgort = (String)rdr["LGORT"];
                    matStock.Labst = (String)rdr["LABST"];
                    matStock.Meins = (String)rdr["MEINS"];
                    matStock.Maktx = (String)rdr["MAKTX"];
                    _matStocks.Add(matStock);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _matStocks;
        }
        //public static List<BaseMatConf> GetMatConfs(BaseOrdOperation ordOperation)
        //{
        //    //var order = new BaseOrder();
        //    //order.Aufnr = ordOperation.Aufnr;
        //    //return GetMatConfs(order);
        //}

        public static List<MatConf> GetMatConfs(Order order)
        {
            var matConfs = new List<MatConf>();
            try
            {
                var sqlStmt = "SELECT * FROM D_MATCONF " +
                                  " WHERE D_MATCONF.AUFNR = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr); 
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var matConf = new MatConf();
                    matConf.Mblnr = (String)rdr["MBLNR"];
                    matConf.Mjahr = (String)rdr["MJAHR"];
                    matConf.Aufnr = (String)rdr["AUFNR"];
                    var isdd = DateTime.Now;
                    DateTimeHelper.getDate(rdr["ISDD"], out isdd);
                    matConf.Isdd = isdd;
                    matConf.MoveType = (String)rdr["MOVE_TYPE"];
                    matConf.SpecStock = (String)rdr["SPEC_STOCK"];
                    matConf.StckType = (String)rdr["STCK_TYPE"];
                    matConf.Plant = (String)rdr["PLANT"];
                    matConf.Storloc = (String)rdr["STORLOC"];
                    matConf.Customer = (String)rdr["CUSTOMER"];
                    matConf.Matnr = (String)rdr["MATNR"];
                    matConf.Quantity = (String)rdr["QUANTITY"];
                    matConf.Unit = (String)rdr["UNIT"];
                    matConf.Rsnum = (String)rdr["RSNUM"];
                    matConf.Rspos = (String)rdr["RSPOS"];
                    matConf.Kzear = (String)rdr["KZEAR"];
                    matConf.Batch = (String)rdr["BATCH"];
                    matConf.Equnr = (String)rdr["EQUNR"];
                    matConf.UpdFlag = (String)rdr["UPDFLAG"];
                    matConfs.Add(matConf);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return matConfs;
        }

        public static List<MatConf> GetMatConfs()
        {
            var matConfs = new List<MatConf>();
            try
            {
                var sqlStmt = "SELECT * FROM D_MATCONF";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var matConf = new MatConf();
                    matConf.Mblnr = rdr["MBLNR"];
                    matConf.Mjahr = rdr["MJAHR"];
                    matConf.Aufnr = rdr["AUFNR"];
                    var isdd = DateTime.Now;
                    DateTimeHelper.getDate(rdr["ISDD"], out isdd);
                    matConf.Isdd = isdd;
                    matConf.MoveType = rdr["MOVE_TYPE"];
                    matConf.SpecStock = rdr["SPEC_STOCK"];
                    matConf.StckType = rdr["STCK_TYPE"];
                    matConf.Plant = rdr["PLANT"];
                    matConf.Storloc = rdr["STORLOC"];
                    matConf.Customer = rdr["CUSTOMER"];
                    matConf.Matnr = rdr["MATNR"];
                    matConf.Quantity = rdr["QUANTITY"];
                    matConf.Unit = rdr["UNIT"];
                    matConf.Rsnum = rdr["RSNUM"];
                    matConf.Rspos = rdr["RSPOS"];
                    matConf.Kzear = rdr["KZEAR"];
                    matConf.Batch = rdr["BATCH"];
                    matConf.Equnr = rdr["EQUNR"];
                    matConf.UpdFlag = rdr["UPDFLAG"];

                    matConfs.Add(matConf);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return matConfs;
        }

        public static List<Mast> GetAllMast()
        {
            var mastList = new List<Mast>();
            try
            {
                var sqlStmt = "SELECT * FROM C_STL_MAST";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach(var rdr in records)
                {
                    var mast = new Mast();
                    mast.Matnr = rdr["MATNR"];
                    mast.Werks = rdr["WERKS"];
                    mast.Stlan = rdr["STLAN"];
                    mast.Stlnr = rdr["STLNR"];
                    mast.Stlal = rdr["STLAL"];
                    mastList.Add(mast);
                }
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return mastList;
        }

        public static Mast GetMast(String werks, String matnr)
        {
            var mast = new Mast();
            try
            {
                var sqlStmt = "SELECT * FROM C_STL_MAST WHERE MATNR = @MATNR AND WERKS = @WERKS";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MATNR", matnr);
                cmd.Parameters.AddWithValue("@WERKS", werks);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    mast.Matnr = rdr["MATNR"];
                    mast.Werks = rdr["WERKS"];
                    mast.Stlan = rdr["STLAN"];
                    mast.Stlnr = rdr["STLNR"];
                    mast.Stlal = rdr["STLAL"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return mast;
        }

        public static List<Stpo> GetBom(Mast mast)
        {
            var stpoList = new List<Stpo>();
            try
            {
                var sqlStmt = "SELECT " +
                                "C_STL_STPO.STLTY, C_STL_STPO.STLNR, C_STL_STPO.STLKN, C_STL_STPO.STPOZ, " +
                                "C_STL_STPO.IDNRK, C_STL_STPO.POSTP, C_STL_STPO.POSNR, C_STL_STPO.SORTF, " +
                                "C_STL_STPO.MEINS, C_STL_STPO.MENGE, C_STL_STPO.ERSKZ, C_STL_STPO.SANIN, C_STL_STPO.STKKZ " +
                                "FROM C_STL_STKO " +
                                "INNER JOIN C_STL_STAS ON " +
                                "C_STL_STKO.STLTY = @STLTY AND C_STL_STKO.STLNR = C_STL_STAS.STLNR AND C_STL_STKO.STLAL = C_STL_STAS.STLAL " +
                                "INNER JOIN C_STL_STPO ON " +
                                "C_STL_STAS.STLKN = C_STL_STPO.STLKN AND C_STL_STAS.STLTY = @STLTY AND C_STL_STAS.STLNR = C_STL_STPO.STLNR " +
                                "WHERE C_STL_STKO.STLTY = @STLTY AND C_STL_STKO.STLNR = @STLNR AND C_STL_STKO.STLAL = @STLAL";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@STLTY", "M");
                cmd.Parameters.AddWithValue("@STLNR", mast.Stlnr);
                cmd.Parameters.AddWithValue("@STLAL", mast.Stlal);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var stpo = new Stpo();
                    stpo.Stlty = rdr["STLTY"];
                    stpo.Stlnr = rdr["STLNR"];
                    stpo.Stlkn = rdr["STLKN"];
                    stpo.Stpoz = rdr["STPOZ"];
                    stpo.Idnrk = rdr["IDNRK"];
                    stpo.Postp = rdr["POSTP"];
                    stpo.Posnr = rdr["POSNR"];
                    stpo.Sortf = rdr["SORTF"];
                    stpo.Meins = rdr["MEINS"];
                    stpo.Menge = rdr["MENGE"];
                    stpo.Erskz = rdr["ERSKZ"];
                    stpo.Sanin = rdr["SANIN"];
                    stpo.Stkkz = rdr["STKKZ"];
                    stpoList.Add(stpo);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return stpoList;
        }

        public static String GetMatConfStatus(Order order)
        {
            var matConfs = GetMatConfs(order);
            if (matConfs.Count > 0)
                return "Erfasste Materialrückmeldungen: " + matConfs.Count;
            return "Materialrückmeldungen [nicht erfasst]";
        }
    }
}
