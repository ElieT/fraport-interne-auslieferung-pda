﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCustCodesManager
    {
        public static List<CustCodes> GetCustCodes(String rbnr, String katalogart, Boolean inTransaction)
        {
            var retList = new List<CustCodes>();
            try
            {

                var oldCodeGruppe = "";
                var sqlStmt = "SELECT CODEGRUPPE, KATALOGART, CODE, CODEGR_KURZTEXT, KURZTEXT FROM G_CUSTCODES " +
                                  "WHERE KATALOGART = @KATALOGART AND CODEGRUPPE IN " +
                                  "(SELECT QCODEGRP FROM G_CUSTBERS WHERE RBNR = @RBNR AND QKATART = @KATALOGART) ORDER BY CODEGRUPPE";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@RBNR", rbnr);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    if (!oldCodeGruppe.Equals( rdr["CODEGRUPPE"]))
                    {
                        var custcodes = new CustCodes();
                        custcodes.Katalogart = rdr["KATALOGART"];
                        custcodes.Codegruppe = rdr["CODEGRUPPE"];
                        custcodes.Code = rdr["CODE"];
                        custcodes.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                        custcodes.Kurztext = rdr["KURZTEXT"];
                        oldCodeGruppe = custcodes.Codegruppe;
                        retList.Add(custcodes);
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }

        public static List<CustCodes> GetCustCodes(String rbnr, String katalogart, String codegrp, Boolean inTransaction)
        {
            var retList = new List<CustCodes>();


            try
            {
                String sqlStmt = "SELECT  CODEGRUPPE, KATALOGART, CODE, CODEGR_KURZTEXT, KURZTEXT  FROM G_CUSTCODES " +
                                  "WHERE KATALOGART = @KATALOGART " +
                                  "AND CODEGRUPPE = @CODEGRP";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@CODEGRP", codegrp);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var custcodes = new CustCodes();
                    custcodes.Katalogart = (String) rdr["KATALOGART"];
                    custcodes.Codegruppe = (String) rdr["CODEGRUPPE"];
                    custcodes.Code = (String) rdr["CODE"];
                    custcodes.CodegrKurztext = (String) rdr["CODEGR_KURZTEXT"];
                    custcodes.Kurztext = (String) rdr["KURZTEXT"];
                    retList.Add(custcodes);
                }
                //retList.Sort();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }



        public static CustCodes GetCustCode(String rbnr, String katalogart, String codegrp, String code, Boolean inTransaction)
        {
            var custcode = new CustCodes();
            try
            {
                var sqlStmt = "SELECT  CODEGRUPPE, KATALOGART, CODE, CODEGR_KURZTEXT, KURZTEXT  FROM G_CUSTCODES " +
                                  "WHERE KATALOGART = @KATALOGART " +
                                  "AND CODEGRUPPE = @CODEGRP AND " +
                                  "AND CODE = @CODE";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@CODEGRP", codegrp);
                cmd.Parameters.AddWithValue("@CODE", code);

                List<Dictionary<String, String>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var rdr = records[0];
                custcode.Katalogart = rdr["KATALOGART"];
                custcode.Codegruppe = rdr["CODEGRUPPE"];
                custcode.Code = rdr["CODE"];
                custcode.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                custcode.Kurztext = rdr["KURZTEXT"];
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return custcode;
        }
    }
}