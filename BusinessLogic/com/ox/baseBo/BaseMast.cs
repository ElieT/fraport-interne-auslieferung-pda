﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.basebo
{
    public class BaseMast
    {
        public string Matnr { get; set; }
        public string Werks { get; set; }
        public string Stlan { get; set; }
        public string Stlnr { get; set; }
        public string Stlal { get; set; }
    }
}
