﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseNotifManager
    {

        protected static List<Notif> _notifList;
        protected static int _lastTempItemId;
        protected static int _lastTempCauseId;
        protected static int _lastTempTaskId;
        protected static int _lastTempActivityId;

        public static List<Notif> NotifList
        {
            get
            {
                if (_notifList == null)
                    CachingManager.UpdateNotifList();
                return _notifList;
            }
            set {_notifList = value; }
        }

        public static List<Notif> GetNotifsFromDb()
        {
            var notifs = new List<Notif>();
            try
            {

                var sqlStmt = "SELECT D_NOTIHEAD.MANDT, D_NOTIHEAD.USERID, D_NOTIHEAD.QMNUM, D_NOTIHEAD.QMART, D_NOTIHEAD.QMTXT, D_NOTIHEAD.ARTPR, " +
                                "D_NOTIHEAD.PRIOK, D_NOTIHEAD.MZEIT, D_NOTIHEAD.QMDAT, D_NOTIHEAD.RBNR, D_NOTIHEAD.QMNAM, " +
                                "D_NOTIHEAD.STRMN, D_NOTIHEAD.STRUR, D_NOTIHEAD.LTRMN, D_NOTIHEAD.LTRUR, D_NOTIHEAD.AUFNR, D_NOTIHEAD.PRUEFLOS, D_NOTIHEAD.IWERK, " +
                                "D_NOTIHEAD.EQUNR, D_NOTIHEAD.BAUTL, D_NOTIHEAD.AUSVN, D_NOTIHEAD.AUSBS, D_NOTIHEAD.AUZTV, D_NOTIHEAD.AUZTB, D_NOTIHEAD.INGRP, " +
                                "D_NOTIHEAD.WARPL, D_NOTIHEAD.WAPOS, D_NOTIHEAD.TPLNR, D_NOTIHEAD.GEWRK, D_NOTIHEAD.UPDFLAG, D_NOTIHEAD.MOBILEKEY, D_NOTIHEAD.FLAG_PLOS, " +
                                "D_FLOC.PLTXT, D_ORDHEAD.AUFNR, D_EQUI.EQKTX, D_EQUI.STORT AS ESTORT, D_FLOC.STORT AS FSTORT, " +
                                "D_EQUI.EQFNR, D_EQUI.TIDNR, D_EQUI.MSGRP AS EMSGRP, D_FLOC.MSGRP AS FMSGRP " +
                                "FROM D_NOTIHEAD " +
                                "LEFT OUTER JOIN D_FLOC ON " +
                                "D_NOTIHEAD.TPLNR = D_FLOC.TPLNR " +
                                "LEFT OUTER JOIN D_EQUI ON " +
                                "D_NOTIHEAD.EQUNR = D_EQUI.EQUI " +
                                "LEFT OUTER JOIN D_ORDHEAD ON " +
                                "D_NOTIHEAD.AUFNR = D_ORDHEAD.AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);

                sqlStmt = "SELECT * FROM D_NOTIITEM WHERE QMNUM = @QMNUM ORDER BY FENUM";
                cmd = new SQLiteCommand(sqlStmt);

                var sqlCause = "SELECT * FROM D_NOTICAUSE WHERE QMNUM = @QMNUM AND FENUM = @FENUM ORDER BY URNUM";
                var cmdCause = new SQLiteCommand(sqlStmt);

                var sqlTask = "SELECT * FROM D_NOTITASK WHERE QMNUM = @QMNUM AND FENUM = @FENUM ORDER BY MANUM";
                var cmdTask = new SQLiteCommand(sqlStmt);

                var sqlAct = "SELECT * FROM D_NOTIACTIVITY WHERE QMNUM = @QMNUM AND FENUM = @FENUM ORDER BY MANUM";
                var cmdAct = new SQLiteCommand(sqlStmt);

                var sqlTask2 = "SELECT * FROM D_NOTITASK WHERE QMNUM = @QMNUM AND (FENUM = '0000' OR FENUM = '0') ORDER BY MANUM";
                var cmdTask2 = new SQLiteCommand(sqlStmt);

                var sqlAct2 = "SELECT * FROM D_NOTIACTIVITY WHERE QMNUM = @QMNUM AND (FENUM = '0000' OR FENUM = '0') ORDER BY MANUM";
                var cmdAct2 = new SQLiteCommand(sqlStmt);
                foreach (var rdr in records)
                {
                    var _notif = new Notif();
                    _notif.Qmnum = rdr["QMNUM"];
                    _notif.Qmart = rdr["QMART"];
                    _notif.Qmtxt = rdr["QMTXT"];
                    _notif.Artpr = rdr["ARTPR"];
                    _notif.Priok = rdr["PRIOK"];
                    DateTime temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["QMDAT"], rdr["MZEIT"], out temp);
                    _notif.Mzeit = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["QMDAT"], out temp);
                    _notif.Qmdat = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["STRMN"], out temp);
                    _notif.Strmn = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["STRMN"], rdr["STRUR"], out temp);
                    _notif.Strur = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["LTRMN"], out temp);
                    _notif.Ltrmn = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["LTRMN"], rdr["LTRUR"], out temp);
                    _notif.Ltrur = temp;
                    _notif.Aufnr = rdr["AUFNR"];
                    _notif.Prueflos = rdr["PRUEFLOS"];
                    _notif.Iwerk = rdr["IWERK"];
                    _notif.Equnr = rdr["EQUNR"];
                    _notif.Bautl = rdr["BAUTL"];
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["AUSVN"], out temp);
                    _notif.Ausvn = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["AUSBS"], out temp);
                    _notif.Ausbs = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["AUSVN"], rdr["AUZTV"], out temp);
                    _notif.Auztv = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getTime(rdr["AUSBS"], rdr["AUZTB"], out temp);
                    _notif.Auztb = temp;
                    _notif.Ingrp = rdr["INGRP"];
                    _notif.Warpl = rdr["WARPL"];
                    _notif.Wapos = rdr["WAPOS"];
                    _notif.Tplnr = rdr["TPLNR"];
                    _notif.Updflag = rdr["UPDFLAG"];
                    _notif.Pltxt = rdr["PLTXT"];
                    _notif.Eqktx = rdr["EQKTX"];
                    _notif.Eqfnr = rdr["EQFNR"];
                    if (!String.IsNullOrEmpty(rdr["ESTORT"]))
                        _notif.Stort = rdr["ESTORT"];
                    else
                        _notif.Stort = rdr["FSTORT"];
                    _notif.Tidnr = rdr["TIDNR"];
                    if (!String.IsNullOrEmpty(rdr["EMSGRP"]))
                        _notif.Msgrp = rdr["EMSGRP"];
                    else
                        _notif.Msgrp = rdr["FMSGRP"];
                    _notif.Aufnr = rdr["AUFNR"];
                    _notif.Qmnam = rdr["QMNAM"];
                    _notif.Rbnr = rdr["RBNR"];
                    _notif.Gewrk = rdr["GEWRK"];
                    _notif.MobileKey = rdr["MOBILEKEY"];
                    _notif.FlagPlos = rdr["FLAG_PLOS"];


                    // Get items for notification
                    var notifItems = new List<NotifItem>();
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlStmt;
                    cmd.Parameters.AddWithValue("@AUFNR", rdr["AUFNR"]);
                    cmd.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                    var itemRecords = OxDbManager.GetInstance().FeedTransaction(cmd);

                    foreach (var rdrItems in itemRecords)
                    {
                        var notifItem = new NotifItem();
                        notifItem.Qmnum = (String) rdrItems["QMNUM"];
                        notifItem.Fetxt = (String) rdrItems["FETXT"];
                        notifItem.Fenum = (String) rdrItems["FENUM"];
                        notifItem.Fetxt = (String) rdrItems["FETXT"];
                        notifItem.Fekat = (String) rdrItems["FEKAT"];
                        notifItem.Fegrp = (String) rdrItems["FEGRP"];
                        notifItem.Fecod = (String) rdrItems["FECOD"];
                        notifItem.Fever = (String) rdrItems["FEVER"];
                        notifItem.Otkat = (String) rdrItems["OTKAT"];
                        notifItem.Otgrp = (String) rdrItems["OTGRP"];
                        notifItem.Oteil = (String) rdrItems["OTEIL"];
                        notifItem.Otver = (String) rdrItems["OTVER"];
                        notifItem.Bautl = (String) rdrItems["BAUTL"];
                        notifItem.Updflag = (String) rdrItems["UPDFLAG"];
                        temp = DateTime.Now;
                        DateTimeHelper.getTime(null, rdrItems["ERZEIT"], out temp);
                        notifItem.Erzeit = temp;
                        notifItem.Posnr = (String) rdrItems["POSNR"];

                        cmdCause.Parameters.Clear();
                        cmdCause.CommandText = sqlCause;
                        cmdCause.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                        cmdCause.Parameters.AddWithValue("@FENUM", notifItem.Fenum);
                        var _notifCauses = new List<NotifCause>();
                        var causeRecords = OxDbManager.GetInstance().FeedTransaction(cmdCause);
                        foreach (var rdrCauses in causeRecords)
                        {
                            var _notifCause = new NotifCause();
                            _notifCause.Qmnum = rdrCauses["QMNUM"];
                            _notifCause.Fenum = rdrCauses["FENUM"];
                            _notifCause.Urnum = rdrCauses["URNUM"];
                            _notifCause.Urtxt = rdrCauses["URTXT"];
                            _notifCause.Urkat = rdrCauses["URKAT"];
                            _notifCause.Urgrp = rdrCauses["URGRP"];
                            _notifCause.Urcod = rdrCauses["URCOD"];
                            _notifCause.Urver = rdrCauses["URVER"];
                            temp = DateTime.Now;
                            DateTimeHelper.getTime(null, rdrCauses["ERZEIT"], out temp);
                            _notifCause.Erzeit = temp;
                            _notifCause.Bautl = rdrCauses["BAUTL"];
                            _notifCause.Qurnum = rdrCauses["QURNUM"];
                            _notifCause.Updflag = rdrCauses["UPDFLAG"];
                            _notifCauses.Add(_notifCause);
                        }
                        notifItem._notifItemCauses = _notifCauses;

                        // Get tasks for notification item
                        cmdTask.Parameters.Clear();
                        cmdTask.CommandText = sqlTask;
                        cmdTask.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                        cmdTask.Parameters.AddWithValue("@FENUM", notifItem.Fenum);

                        var _notifItemTasks = new List<NotifTask>();
                        var taskItemRecords = OxDbManager.GetInstance().FeedTransaction(cmdTask);
                        foreach (var rdrItemTasks in taskItemRecords)
                        {
                            var _notifItemTask = new NotifTask();
                            _notifItemTask.Qmnum = rdrItemTasks["QMNUM"];
                            _notifItemTask.Manum = rdrItemTasks["MANUM"];
                            _notifItemTask.Fenum = rdrItemTasks["FENUM"];
                            _notifItemTask.Urnum = rdrItemTasks["URNUM"];
                            _notifItemTask.Mnkat = rdrItemTasks["MNKAT"];
                            _notifItemTask.Mngrp = rdrItemTasks["MNGRP"];
                            _notifItemTask.Mncod = rdrItemTasks["MNCOD"];
                            _notifItemTask.Mnver = rdrItemTasks["MNVER"];
                            _notifItemTask.Matxt = rdrItemTasks["MATXT"];
                            temp = DateTime.Now;
                            DateTimeHelper.getDate(rdrItemTasks["PSTER"], out temp);
                            _notifItemTask.Pster = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getDate(rdrItemTasks["PETER"], out temp);
                            _notifItemTask.Peter = temp;
                            _notifItemTask.Mngfa = rdrItemTasks["MNGFA"];
                            temp = DateTime.Now;
                            DateTimeHelper.getTime(rdrItemTasks["PSTER"], rdrItemTasks["PSTUR"],
                                                   out temp);
                            _notifItemTask.Pstur = temp;
                            _notifItemTask.Pster = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getTime(rdrItemTasks["PETER"], rdrItemTasks["PETUR"],
                                                   out temp);
                            _notifItemTask.Petur = temp;
                            _notifItemTask.Peter = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getTime(null, rdrItemTasks["ERZEIT"], out temp);
                            _notifItemTask.Erzeit = temp;
                            _notifItemTask.Qmanum = rdrItemTasks["QMANUM"];
                            _notifItemTask.Updflag = rdrItemTasks["UPDFLAG"];
                            _notifItemTasks.Add(_notifItemTask);
                        }
                        notifItem._notifItemTasks = _notifItemTasks;

                        // Get activities for notification item
                        cmdAct.Parameters.Clear();
                        cmdAct.CommandText = sqlAct;
                        cmdAct.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                        cmdAct.Parameters.AddWithValue("@FENUM", notifItem.Fenum);
                        var _notifItemActivities = new List<NotifActivity>();
                        var actItemRecords = OxDbManager.GetInstance().FeedTransaction(cmdAct);

                        foreach (var rdrItemActivities in actItemRecords)
                        {
                            var _notifItemActivity = new NotifActivity();
                            _notifItemActivity.Qmnum = rdrItemActivities["QMNUM"];
                            _notifItemActivity.Manum = rdrItemActivities["MANUM"];
                            _notifItemActivity.Mnkat = rdrItemActivities["MNKAT"];
                            _notifItemActivity.Mngrp = rdrItemActivities["MNGRP"];
                            _notifItemActivity.Mncod = rdrItemActivities["MNCOD"];
                            _notifItemActivity.Mnver = rdrItemActivities["MNVER"];
                            _notifItemActivity.Matxt = rdrItemActivities["MATXT"];
                            temp = DateTime.Now;
                            DateTimeHelper.getDate(rdrItemActivities["PSTER"], out temp);
                            _notifItemActivity.Pster = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getDate(rdrItemActivities["PETER"], out temp);
                            _notifItemActivity.Peter = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getTime(rdrItemActivities["PSTER"], rdrItemActivities["PSTUR"],
                                                   out temp);
                            _notifItemActivity.Pstur = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getTime(rdrItemActivities["PETER"], rdrItemActivities["PETUR"],
                                                   out temp);
                            _notifItemActivity.Petur = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getDate(rdrItemActivities["ERLDAT"], out temp);
                            _notifItemActivity.Erldat = temp;
                            temp = DateTime.Now;
                            DateTimeHelper.getTime(rdrItemActivities["ERLDAT"], rdrItemActivities["ERLZEIT"],
                                                   out temp);
                            _notifItemActivity.Erlzeit = temp;
                            _notifItemActivity.Fenum = rdrItemActivities["FENUM"];
                            _notifItemActivity.Urnum = rdrItemActivities["URNUM"];
                            _notifItemActivity.Parvw = rdrItemActivities["PARVW"];
                            _notifItemActivity.Parnr = rdrItemActivities["PARNR"];
                            _notifItemActivity.Bautl = rdrItemActivities["BAUTL"];
                            _notifItemActivity.Erlzeit = temp;
                            DateTimeHelper.getTime(null, rdrItemActivities["ERZEIT"], out temp);
                            _notifItemActivity.Erlzeit = temp;
                            _notifItemActivity.Updflag = rdrItemActivities["UPDFLAG"];
                            _notifItemActivity.Erlnam = rdrItemActivities["ERLNAM"];
                            _notifItemActivities.Add(_notifItemActivity);
                        }
                        notifItem._notifItemActivities = _notifItemActivities;

                        notifItems.Add(notifItem);
                    }
                    _notif._notifItems = notifItems;

                    cmdTask2.Parameters.Clear();
                    cmdTask2.CommandText = sqlTask2;
                    cmdTask2.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                    var _notifTasks = new List<NotifTask>();
                    var taskRecords = OxDbManager.GetInstance().FeedTransaction(cmdTask2);
                    foreach (var rdrTasks in taskRecords)
                    {
                        var _notifTask = new NotifTask();
                        _notifTask.Qmnum = (String) rdrTasks["QMNUM"];
                        _notifTask.Manum = (String) rdrTasks["MANUM"];
                        _notifTask.Fenum = (String) rdrTasks["FENUM"];
                        _notifTask.Urnum = (String) rdrTasks["URNUM"];
                        _notifTask.Mnkat = (String) rdrTasks["MNKAT"];
                        _notifTask.Mngrp = (String) rdrTasks["MNGRP"];
                        _notifTask.Mncod = (String) rdrTasks["MNCOD"];
                        _notifTask.Mnver = (String) rdrTasks["MNVER"];
                        _notifTask.Matxt = (String) rdrTasks["MATXT"];
                        temp = DateTime.Now;
                        DateTimeHelper.getDate(rdrTasks["PSTER"], out temp);
                        _notifTask.Pster = temp;
                        DateTimeHelper.getDate(rdrTasks["PETER"], out temp);
                        _notifTask.Peter = temp;
                        _notifTask.Mngfa = (String) rdrTasks["MNGFA"];
                        DateTimeHelper.getTime(rdrTasks["PSTER"], rdrTasks["PSTUR"], out temp);
                        _notifTask.Pstur = temp;
                        DateTimeHelper.getTime(rdrTasks["PETER"], rdrTasks["PETUR"], out temp);
                        _notifTask.Petur = temp;
                        DateTimeHelper.getTime(null, rdrTasks["ERZEIT"], out temp);
                        _notifTask.Erzeit = temp;
                        _notifTask.Qmanum = (String) rdrTasks["QMANUM"];
                        _notifTask.Updflag = (String) rdrTasks["UPDFLAG"];
                        _notifTasks.Add(_notifTask);
                    }
                    _notif._notifTasks = _notifTasks;

                    // Get activities for notification
                    
                    cmdAct2.Parameters.Clear();
                    cmdAct2.CommandText = sqlAct2;
                    cmdAct2.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                    var _notifActivities = new List<NotifActivity>();
                    var actRecords = OxDbManager.GetInstance().FeedTransaction(cmdAct2);
                    foreach (var rdrActivities in actRecords)
                    {
                        var _notifActivity = new NotifActivity();
                        _notifActivity.Qmnum = (String) rdrActivities["QMNUM"];
                        _notifActivity.Manum = (String) rdrActivities["MANUM"];
                        _notifActivity.Mnkat = (String) rdrActivities["MNKAT"];
                        _notifActivity.Mngrp = (String) rdrActivities["MNGRP"];
                        _notifActivity.Mncod = (String) rdrActivities["MNCOD"];
                        _notifActivity.Mnver = (String) rdrActivities["MNVER"];
                        _notifActivity.Matxt = (String) rdrActivities["MATXT"];
                        DateTimeHelper.getDate(rdrActivities["PSTER"], out temp);
                        _notifActivity.Pster = temp;
                        DateTimeHelper.getDate(rdrActivities["PETER"], out temp);
                        _notifActivity.Peter = temp;
                        DateTimeHelper.getTime(rdrActivities["PSTER"], rdrActivities["PSTUR"], out temp);
                        _notifActivity.Pstur = temp;
                        DateTimeHelper.getTime(rdrActivities["PETER"], rdrActivities["PETUR"], out temp);
                        _notifActivity.Petur = temp;
                        DateTimeHelper.getDate(rdrActivities["ERLDAT"], out temp);
                        _notifActivity.Erldat = temp;
                        DateTimeHelper.getTime(rdrActivities["ERLDAT"], rdrActivities["ERLZEIT"],
                                               out temp);
                        _notifActivity.Erlzeit = temp;
                        _notifActivity.Fenum = (String) rdrActivities["FENUM"];
                        _notifActivity.Urnum = (String) rdrActivities["URNUM"];
                        _notifActivity.Parvw = (String) rdrActivities["PARVW"];
                        _notifActivity.Parnr = (String) rdrActivities["PARNR"];
                        _notifActivity.Bautl = (String) rdrActivities["BAUTL"];
                        DateTimeHelper.getDate(rdrActivities["ERZEIT"], out temp);
                        _notifActivity.Erzeit = temp;
                        _notifActivity.Updflag = (String)rdrActivities["UPDFLAG"];
                        _notifActivity.Erlnam = (String)rdrActivities["ERLNAM"];
                        _notifActivities.Add(_notifActivity);
                    }
                    _notif._notifActivities = _notifActivities;

                    notifs.Add(_notif);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return notifs;
        }

        public static void CreateNewNotifID(ref String dbIdNotif, Boolean inTransaction)
        {
            var intDbKeyNotif = -1;
            try
            {
                var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_NOTIHEAD";
                var cmd = new SQLiteCommand(sqlStmt);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyNotif = int.Parse(result);
                else
                    intDbKeyNotif = 1;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            dbIdNotif = "MO"+intDbKeyNotif.ToString().PadLeft(10, '0');
        }

        public static void CreateNewNotifItemID(String aQmnum, ref String dbIdNotifItem, Boolean inTransaction)
        {
            var intDbKeyNotif = -1;
            try
            {
                var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_NOTIITEM WHERE QMNUM = @QMNUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", aQmnum);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyNotif = int.Parse(result);
                else
                    intDbKeyNotif = 1;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            dbIdNotifItem = intDbKeyNotif.ToString();
        }

        public static void CreateNewNotifTaskID(String aQmnum, ref String dbIdNotifTask, Boolean inTransaction)
        {
            var intDbKeyNotif = -1;
            try
            {
                var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_NOTITASK WHERE UPDFLAG = 'I' AND QMNUM = @QMNUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", aQmnum);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyNotif = int.Parse(result);
                else
                    intDbKeyNotif = 1;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            dbIdNotifTask = intDbKeyNotif.ToString();
        }

        public static void CreateNewNotifActivityID(String aQmnum, ref String dbIdNotifActivity, Boolean inTransaction)
        {
            var intDbKeyNotif = -1;
            try
            {
                var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_NOTIACTIVITY WHERE UPDFLAG = 'I' AND QMNUM = @QMNUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", aQmnum);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyNotif = int.Parse(result);
                else
                    intDbKeyNotif = 1;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            dbIdNotifActivity = intDbKeyNotif.ToString();
        }

        public static void CreateNewNotifCauseID(String aQmnum, String aFenum, ref String dbIdNotifCause, Boolean inTransaction)
        {
            var intDbKeyNotif = -1;
            try
            {
                var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_NOTICAUSE WHERE UPDFLAG = 'I' AND QMNUM = @QMNUM " +
                            " AND FENUM = @FENUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", aQmnum);
                cmd.Parameters.AddWithValue("@FENUM", aFenum);

                List<Dictionary<string, string>> records;
                if(inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyNotif = int.Parse(result);
                else
                    intDbKeyNotif = 1;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            dbIdNotifCause = intDbKeyNotif.ToString();
        }

        private static Notif CreateUpdate(Notif notif)
        {
            try
            {
                deleteMissingElements(notif);
                var sqlStmt = "UPDATE [D_NOTIHEAD] SET " +
                          "QMTXT = @QMTXT, MZEIT = @MZEIT, QMDAT = @QMDAT, EQUNR = @EQUNR, AUFNR = @AUFNR, " +
                          "TPLNR = @TPLNR, QMNAM = @QMNAM, AUSVN = @AUSVN, AUSBS = @AUSBS, AUZTB = @AUZTB, AUZTV = @AUZTV, UPDFLAG = @UPDFLAG " +
                          "WHERE QMNUM = @QMNUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMTXT", notif.Qmtxt);
                cmd.Parameters.AddWithValue("@MZEIT", DateTimeHelper.GetTimeString(notif.Mzeit));
                cmd.Parameters.AddWithValue("@QMDAT", DateTimeHelper.GetDateString(notif.Qmdat));
                cmd.Parameters.AddWithValue("@EQUNR", notif.Equnr);
                cmd.Parameters.AddWithValue("@EQFNR", notif.Eqfnr);
                cmd.Parameters.AddWithValue("@TPLNR", notif.Tplnr);
                cmd.Parameters.AddWithValue("@QMNAM", notif.Qmnam);
                cmd.Parameters.AddWithValue("@QMNUM", notif.Qmnum);
                cmd.Parameters.AddWithValue("@AUSVN", DateTimeHelper.GetDateString(notif.Ausvn));
                cmd.Parameters.AddWithValue("@AUSBS", DateTimeHelper.GetDateString(notif.Ausbs));
                cmd.Parameters.AddWithValue("@AUZTB", DateTimeHelper.GetTimeString(notif.Auztb));
                cmd.Parameters.AddWithValue("@AUZTV", DateTimeHelper.GetTimeString(notif.Auztv));
                cmd.Parameters.AddWithValue("@AUFNR", notif.Aufnr);
                if (notif.Updflag.Equals("I"))
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                else
                    cmd.Parameters.AddWithValue("@UPDFLAG", "U");
                OxDbManager.GetInstance().FeedTransaction(cmd);
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return notif;
        }

        private static Notif CreateInsert(Notif notif)
        {
            try
            {
                var qmnum = notif.Qmnum;
                var updflagNeu = notif.Updflag;
                if (String.IsNullOrEmpty(qmnum))
                {
                    CreateNewNotifID(ref qmnum, true);
                    updflagNeu = "I";
                }
                if ((notif.Aufnr.Equals("") && notif.MobileKey.Equals("")))
                {
                    var hash = OxCrypto.HashPasswordForStoringInConfigFile(
                        notif.Qmnum + AppConfig.UserId + AppConfig.Mandt +
                        DateTime.Now.ToString("yyyyMMddHHmmssffff"));
                    notif.MobileKey = hash;
                }
                else if (notif.MobileKey.Equals(""))
                {
                    var ord = OrderManager.GetOrder(notif.Aufnr);
                    notif.MobileKey = ord.MobileKey;
                }

                notif.Qmnum = qmnum;
                notif.Updflag = updflagNeu;
                var sqlStmt = "INSERT INTO [D_NOTIHEAD] " +
                            "(MANDT, USERID, QMNUM, QMART, QMTXT, ARTPR, PRIOK, MZEIT, QMDAT, " +
                            "STRMN, STRUR, LTRMN, LTRUR, AUFNR, PRUEFLOS, IWERK, " +
                            "EQUNR, BAUTL, AUSVN, AUSBS, AUZTV, AUZTB, INGRP, WARPL, WAPOS, TPLNR, QMNAM, RBNR, GEWRK, UPDFLAG, MOBILEKEY) " +
                            "Values (@MANDT, @USERID, @QMNUM, @QMART, @QMTXT, @ARTPR, @PRIOK, @MZEIT, @QMDAT, " +
                            "@STRMN, @STRUR, @LTRMN, @LTRUR, @AUFNR, @PRUEFLOS, @IWERK, " +
                            "@EQUNR, @BAUTL, @AUSVN, @AUSBS, @AUZTV, @AUZTB, @INGRP, @WARPL, @WAPOS, @TPLNR, @QMNAM, @RBNR, @GEWRK, @UPDFLAG, @MOBILEKEY)";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@UPDFLAG", updflagNeu);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@QMNUM", notif.Qmnum);
                cmd.Parameters.AddWithValue("@QMART", notif.Qmart);
                cmd.Parameters.AddWithValue("@QMTXT", notif.Qmtxt);
                cmd.Parameters.AddWithValue("@ARTPR", notif.Artpr);
                cmd.Parameters.AddWithValue("@PRIOK", notif.Priok);
                cmd.Parameters.AddWithValue("@MZEIT", DateTimeHelper.GetTimeString(notif.Mzeit));
                cmd.Parameters.AddWithValue("@QMDAT", DateTimeHelper.GetDateString(notif.Qmdat));
                cmd.Parameters.AddWithValue("@STRMN", DateTimeHelper.GetDateString(notif.Strmn));
                cmd.Parameters.AddWithValue("@STRUR", DateTimeHelper.GetTimeString(notif.Strur));
                cmd.Parameters.AddWithValue("@LTRMN", DateTimeHelper.GetDateString(notif.Ltrmn));
                cmd.Parameters.AddWithValue("@LTRUR", DateTimeHelper.GetTimeString(notif.Ltrur));
                cmd.Parameters.AddWithValue("@AUFNR", notif.Aufnr);
                cmd.Parameters.AddWithValue("@PRUEFLOS", notif.Prueflos);
                cmd.Parameters.AddWithValue("@IWERK", notif.Iwerk);
                cmd.Parameters.AddWithValue("@EQUNR", notif.Equnr);
                cmd.Parameters.AddWithValue("@BAUTL", notif.Bautl);
                cmd.Parameters.AddWithValue("@AUSVN", DateTimeHelper.GetDateString(notif.Ausvn));
                cmd.Parameters.AddWithValue("@AUSBS", DateTimeHelper.GetDateString(notif.Ausbs));
                cmd.Parameters.AddWithValue("@AUZTV", DateTimeHelper.GetTimeString(notif.Auztv));
                cmd.Parameters.AddWithValue("@AUZTB", DateTimeHelper.GetTimeString(notif.Auztb));
                cmd.Parameters.AddWithValue("@INGRP", notif.Ingrp);
                cmd.Parameters.AddWithValue("@WARPL", notif.Warpl);
                cmd.Parameters.AddWithValue("@ABNUM", notif.Abnum);
                cmd.Parameters.AddWithValue("@WAPOS", notif.Wapos);
                cmd.Parameters.AddWithValue("@TPLNR", notif.Tplnr);
                cmd.Parameters.AddWithValue("@QMNAM", notif.Qmnam);
                cmd.Parameters.AddWithValue("@RBNR", notif.Rbnr);
                cmd.Parameters.AddWithValue("@GEWRK", notif.Gewrk);
                cmd.Parameters.AddWithValue("@MOBILEKEY", notif.MobileKey);

                OxDbManager.GetInstance().FeedTransaction(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return notif;
        }

        public static String CreateNotif(Notif aNotif)
        {
            var retQmnum = "";
            try
            {
                deleteMissingElements(aNotif);
                var sqlStmt = "";

                if (aNotif.Qmnum.Equals(""))
                    aNotif = CreateInsert(aNotif);
                else
                    aNotif = CreateUpdate(aNotif);

                retQmnum = aNotif.Qmnum;
                SQLiteCommand cmd;
                
                if (aNotif._notifItems != null)
                {
                    sqlStmt = "INSERT OR REPLACE INTO [D_NOTIITEM] " +
                              "(MANDT, USERID, QMNUM, FENUM, FETXT, FEKAT, FEGRP, FECOD, FEVER, OTKAT, " +
                              "OTGRP, OTEIL, OTVER, BAUTL, ERZEIT, POSNR, UPDFLAG, MOBILEKEY) " +
                              "Values (@MANDT, @USERID, @QMNUM, @FENUM, @FETXT, @FEKAT, @FEGRP, @FECOD, @FEVER, @OTKAT, " +
                              "@OTGRP, @OTEIL, @OTVER, @BAUTL, @ERZEIT, @POSNR, @UPDFLAG, @MOBILEKEY)";
                    var cmdItem = new SQLiteCommand(sqlStmt);

                    sqlStmt = "UPDATE [D_NOTIITEM] SET FETXT = @FETXT, FEKAT = @FEKAT, FEGRP = @FEGRP, " +
                              "FECOD = @FECOD, FEVER = @FEVER, OTKAT = @OTKAT, OTGRP = @OTGRP, OTEIL = @OTEIL, OTVER = @OTVER, " +
                              "BAUTL = @BAUTL, ERZEIT = @ERZEIT, POSNR = @POSNR WHERE QMNUM = @QMNUM AND FENUM = @FENUM";
                    var cmdItemUpdate = new SQLiteCommand(sqlStmt);

                    sqlStmt = "INSERT OR REPLACE INTO [D_NOTICAUSE] " +
                              "(MANDT, USERID, QMNUM, FENUM, URNUM, URTXT, URKAT, URGRP, " +
                              "URCOD, URVER, ERZEIT, BAUTL, QURNUM, UPDFLAG, MOBILEKEY) " +
                              "Values (@MANDT, @USERID, @QMNUM, @FENUM, @URNUM, @URTXT, @URKAT, @URGRP, " +
                              "@URCOD, @URVER, @ERZEIT, @BAUTL, @QURNUM, @UPDFLAG, @MOBILEKEY)";
                    var cmdCause = new SQLiteCommand(sqlStmt);

                    sqlStmt = "UPDATE [D_NOTICAUSE] SET URTXT = @URTXT, URKAT = @URKAT, URGRP = @URGRP, " +
                              "URCOD = @URCOD, URVER = @URVER, ERZEIT = @ERZEIT, BAUTL = @BAUTL, QURNUM = @QURNUM " +
                              "WHERE QMNUM = @QMNUM AND FENUM = @FENUM AND URNUM = @URNUM";
                    var cmdCauseUpdate = new SQLiteCommand(sqlStmt);

                    sqlStmt = "INSERT OR REPLACE INTO [D_NOTITASK] " +
                              "(MANDT, USERID, QMNUM, MANUM, FENUM, URNUM, MNKAT, MNGRP, MNCOD, " +
                              "MNVER, MATXT, PSTER, PETER, MNGFA, PSTUR, PETUR, ERZEIT, QMANUM, UPDFLAG, MOBILEKEY) " +
                              "Values (@MANDT, @USERID, @QMNUM, @MANUM, @FENUM, @URNUM, @MNKAT, @MNGRP, @MNCOD, " +
                              "@MNVER, @MATXT, @PSTER, @PETER, @MNGFA, @PSTUR, @PETUR, @ERZEIT, @QMANUM, @UPDFLAG, @MOBILEKEY)";
                    var cmdTask = new SQLiteCommand(sqlStmt);

                    sqlStmt = "UPDATE [D_NOTITASK] SET MATXT = @MATXT, MNKAT = @MNKAT, MNGRP = @MNGRP, " +
                              "MNCOD = @MNCOD, MNVER = @MNVER, PSTER = @PSTER, PETER = @PETER, MNGFA = @MNGFA, PSTUR = @PSTUR, " +
                              "ERZEIT = @ERZEIT, QMANUM = @QMANUM " +
                              "WHERE QMNUM = @QMNUM AND MANUM = @MANUM";
                    var cmdTaskUpdate = new SQLiteCommand(sqlStmt);

                    sqlStmt = "INSERT OR REPLACE INTO [D_NOTIACTIVITY] " +
                              "(MANDT, USERID, QMNUM, MANUM, MNKAT, MNGRP, MNCOD, MNVER, MATXT, " +
                              "PSTER, PETER, PSTUR, PETUR, ERLDAT, ERLZEIT, FENUM, URNUM, PARVW, " +
                              "PARNR, BAUTL, ERZEIT, UPDFLAG, MOBILEKEY, ERLNAM) " +
                              "Values (@MANDT, @USERID, @QMNUM, @MANUM, @MNKAT, @MNGRP, @MNCOD, @MNVER, @MATXT, " +
                              "@PSTER, @PETER, @PSTUR, @PETUR, @ERLDAT, @ERLZEIT, @FENUM, @URNUM, @PARVW, " +
                              "@PARNR, @BAUTL, @ERZEIT, @UPDFLAG, @MOBILEKEY, @ERLNAM)";
                    var cmdAct = new SQLiteCommand(sqlStmt);

                    sqlStmt = "UPDATE [D_NOTIACTIVITY] SET MATXT = @MATXT, MNKAT = @MNKAT, MNGRP = @MNGRP, " +
                              "MNCOD = @MNCOD, MNVER = @MNVER, PSTER = @PSTER, PETER = @PETER, PSTUR = @PSTUR, " +
                              "PETUR = @PETUR, ERLDAT = @ERLDAT, ERLZEIT = @ERLZEIT, FENUM = @FENUM, URNUM = @URNUM, " +
                              "PARVW = @PARVW, PARNR = @PARNR, BAUTL = @BAUTL, ERZEIT = @ERZEIT, ERLNAM = @ERLNAM " +
                              "WHERE QMNUM = @QMNUM AND MANUM = @MANUM";
                    var cmdActUpdate = new SQLiteCommand(sqlStmt);

                    foreach (var item in aNotif._notifItems)
                    {
                        if (item.Updflag.Equals("I") || item.Updflag.Equals(""))
                            cmd = cmdItem;
                        else
                            cmd = cmdItemUpdate;
                        cmd.Parameters.Clear();
                        var fenum = item.Fenum;
                        if (String.IsNullOrEmpty(fenum) || fenum.StartsWith("z"))
                        {
                            CreateNewNotifItemID(aNotif.Qmnum, ref fenum, true);
                        }
                        item.Fenum = fenum;

                        if (item.Updflag == "")
                            item.Updflag = "I";
                        cmd.Parameters.AddWithValue("@UPDFLAG", item.Updflag);

                        if (item.Updflag.Equals("") || item.Updflag.Equals("I") || item.Updflag.Equals("U"))
                        {
                            cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                            cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                            cmd.Parameters.AddWithValue("@QMNUM", aNotif.Qmnum);
                            cmd.Parameters.AddWithValue("@FENUM", item.Fenum);
                            cmd.Parameters.AddWithValue("@FETXT", item.Fetxt);
                            cmd.Parameters.AddWithValue("@FEKAT", item.Fekat);
                            cmd.Parameters.AddWithValue("@FEGRP", item.Fegrp);
                            cmd.Parameters.AddWithValue("@FECOD", item.Fecod);
                            cmd.Parameters.AddWithValue("@FEVER", item.Fever);
                            cmd.Parameters.AddWithValue("@OTKAT", item.Otkat);
                            cmd.Parameters.AddWithValue("@OTGRP", item.Otgrp);
                            cmd.Parameters.AddWithValue("@OTEIL", item.Oteil);
                            cmd.Parameters.AddWithValue("@OTVER", item.Otver);
                            cmd.Parameters.AddWithValue("@BAUTL", item.Bautl);
                            cmd.Parameters.AddWithValue("@ERZEIT", DateTimeHelper.GetTimeString(item.Erzeit));
                            cmd.Parameters.AddWithValue("@POSNR", item.Fenum);
                            //cmd.Parameters.AddWithValue("@AXOENH", item.AxoEnh);
                            cmd.Parameters.AddWithValue("@MOBILEKEY", aNotif.MobileKey);
                            OxDbManager.GetInstance().FeedTransaction(cmd);
                        }

                        //Notification item causes
                        if (item._notifItemCauses != null)
                        {
                            foreach (var cause in item._notifItemCauses)
                            {
                                if (cause.Updflag.Equals("I") || cause.Updflag.Equals(""))
                                    cmd = cmdCause;
                                else
                                    cmd = cmdCauseUpdate;
                                cmd.Parameters.Clear();
                                var urnum = cause.Urnum;
                                if (String.IsNullOrEmpty(urnum) || urnum.StartsWith("z"))
                                {
                                    CreateNewNotifCauseID(aNotif.Qmnum, item.Fenum, ref urnum, true);
                                    cause.Urnum = urnum;
                                }
                                cause.Urnum = urnum;
                                if (cause.Updflag.Equals(""))
                                    cause.Updflag = "I";
                                cmd.Parameters.AddWithValue("@UPDFLAG", cause.Updflag);

                                if (cause.Updflag.Equals("") || cause.Updflag.Equals("I") || cause.Updflag.Equals("U"))
                                {
                                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                                    cmd.Parameters.AddWithValue("@QMNUM", aNotif.Qmnum);
                                    cmd.Parameters.AddWithValue("@FENUM", item.Fenum);
                                    cmd.Parameters.AddWithValue("@URNUM", cause.Urnum);
                                    cmd.Parameters.AddWithValue("@URTXT", cause.Urtxt);
                                    cmd.Parameters.AddWithValue("@URKAT", cause.Urkat);
                                    cmd.Parameters.AddWithValue("@URGRP", cause.Urgrp);
                                    cmd.Parameters.AddWithValue("@URCOD", cause.Urcod);
                                    cmd.Parameters.AddWithValue("@URVER", cause.Urver);
                                    cmd.Parameters.AddWithValue("@ERZEIT", DateTimeHelper.GetTimeString(cause.Erzeit));
                                    cmd.Parameters.AddWithValue("@BAUTL", cause.Bautl);
                                    cmd.Parameters.AddWithValue("@QURNUM", cause.Urnum);
                                    cmd.Parameters.AddWithValue("@MOBILEKEY", aNotif.MobileKey);
                                    OxDbManager.GetInstance().FeedTransaction(cmd);
                                }
                            }
                        }

                        //Notification item tasks
                        if (item._notifItemTasks != null)
                        {
                            foreach (var task in item._notifItemTasks)
                            {
                                if (task.Updflag.Equals("I") || task.Updflag.Equals(""))
                                    cmd = cmdTask;
                                else
                                    cmd = cmdTaskUpdate;
                                cmd.Parameters.Clear();
                                var manum = task.Manum;
                                if (String.IsNullOrEmpty(manum) || manum.StartsWith("z"))
                                {
                                    CreateNewNotifTaskID(aNotif.Qmnum, ref manum, true);
                                    task.Manum = manum;
                                }
                                if (task.Updflag.Equals(""))
                                    task.Updflag = "I";
                                cmd.Parameters.AddWithValue("@UPDFLAG", task.Updflag);

                                if (task.Updflag.Equals("") || task.Updflag.Equals("I") || task.Updflag.Equals("U"))
                                {
                                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                                    cmd.Parameters.AddWithValue("@QMNUM", aNotif.Qmnum);
                                    cmd.Parameters.AddWithValue("@MANUM", task.Manum);
                                    cmd.Parameters.AddWithValue("@FENUM", item.Fenum);
                                    cmd.Parameters.AddWithValue("@URNUM", task.Urnum);
                                    cmd.Parameters.AddWithValue("@MNKAT", task.Mnkat);
                                    cmd.Parameters.AddWithValue("@MNGRP", task.Mngrp);
                                    cmd.Parameters.AddWithValue("@MNCOD", task.Mncod);
                                    cmd.Parameters.AddWithValue("@MNVER", task.Mnver);
                                    cmd.Parameters.AddWithValue("@MATXT", task.Matxt);
                                    cmd.Parameters.AddWithValue("@PSTER",
                                                                    DateTimeHelper.GetDateString(task.Pster));
                                    cmd.Parameters.AddWithValue("@PETER",
                                                                    DateTimeHelper.GetDateString(task.Pster));
                                    cmd.Parameters.AddWithValue("@MNGFA", task.Mngfa);
                                    cmd.Parameters.AddWithValue("@PSTUR",
                                                                    DateTimeHelper.GetTimeString(task.Pstur));
                                    cmd.Parameters.AddWithValue("@PETUR",
                                                                    DateTimeHelper.GetTimeString(task.Peter));
                                    cmd.Parameters.AddWithValue("@ERZEIT",
                                                                    DateTimeHelper.GetTimeString(task.Erzeit));
                                    cmd.Parameters.AddWithValue("@QMANUM", task.Manum);
                                    cmd.Parameters.AddWithValue("@MOBILEKEY", aNotif.MobileKey);
                                    OxDbManager.GetInstance().FeedTransaction(cmd);
                                }
                            }
                        }

                        //Notification item activities
                        if (item._notifItemActivities != null)
                        {
                            foreach (var activity in item._notifItemActivities)
                            {
                                if (activity.Updflag.Equals("I") || activity.Updflag.Equals(""))
                                    cmd = cmdAct;
                                else
                                    cmd = cmdActUpdate;

                                cmd.Parameters.Clear();
                                var manum = activity.Manum;
                                if (String.IsNullOrEmpty(manum) || manum.StartsWith("z"))
                                {
                                    CreateNewNotifActivityID(aNotif.Qmnum, ref manum, true);
                                    activity.Manum = manum;
                                }
                                if (activity.Updflag.Equals(""))
                                    activity.Updflag = "I";
                                cmd.Parameters.AddWithValue("@UPDFLAG", activity.Updflag);

                                if (activity.Updflag.Equals("") || activity.Updflag.Equals("I") || activity.Updflag.Equals("U"))
                                {
                                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                                    cmd.Parameters.AddWithValue("@QMNUM", aNotif.Qmnum);
                                    cmd.Parameters.AddWithValue("@MANUM", activity.Manum);
                                    cmd.Parameters.AddWithValue("@MNKAT", activity.Mnkat);
                                    cmd.Parameters.AddWithValue("@MNGRP", activity.Mngrp);
                                    cmd.Parameters.AddWithValue("@MNCOD", activity.Mncod);
                                    cmd.Parameters.AddWithValue("@MNVER", activity.Mnver);
                                    cmd.Parameters.AddWithValue("@MATXT", activity.Matxt);
                                    cmd.Parameters.AddWithValue("@PSTER",
																   DateTimeHelper.GetDateString(activity.Pster));
                                    cmd.Parameters.AddWithValue("@PETER",
																   DateTimeHelper.GetDateString(activity.Pster));
                                    cmd.Parameters.AddWithValue("@PSTUR",
                                                                   DateTimeHelper.GetTimeString(activity.Pstur));
                                    cmd.Parameters.AddWithValue("@PETUR",
                                                                   DateTimeHelper.GetTimeString(activity.Petur));
                                    cmd.Parameters.AddWithValue("@ERLDAT",
                                                                   DateTimeHelper.GetDateString(activity.Erldat));
                                    cmd.Parameters.AddWithValue("@ERLZEIT",
                                                                   DateTimeHelper.GetTimeString(activity.Erlzeit));
                                    cmd.Parameters.AddWithValue("@FENUM", item.Fenum);
                                    cmd.Parameters.AddWithValue("@URNUM", activity.Urnum);
                                    cmd.Parameters.AddWithValue("@PARVW", activity.Parvw);
                                    cmd.Parameters.AddWithValue("@PARNR", activity.Parnr);
                                    cmd.Parameters.AddWithValue("@BAUTL", activity.Bautl);
                                    cmd.Parameters.AddWithValue("@ERZEIT",
                                                                   DateTimeHelper.GetTimeString(activity.Erlzeit));
                                    cmd.Parameters.AddWithValue("@MOBILEKEY", aNotif.MobileKey);
                                    cmd.Parameters.AddWithValue("@ERLNAM", activity.Erlnam);
                                    OxDbManager.GetInstance().FeedTransaction(cmd);
                                }
                            }
                        }
                    }
                }

                if (aNotif._notifTasks != null)
                {
                    sqlStmt = "INSERT OR REPLACE INTO [D_NOTITASK] " +
                              "(MANDT, USERID, QMNUM, MANUM, FENUM, URNUM, MNKAT, MNGRP, MNCOD, " +
                              "MNVER, MATXT, PSTER, PETER, MNGFA, PSTUR, PETUR, ERZEIT, QMANUM, UPDFLAG, MOBILEKEY) " +
                              "Values (@MANDT, @USERID, @QMNUM, @MANUM, @FENUM, @URNUM, @MNKAT, @MNGRP, @MNCOD, " +
                              "@MNVER, @MATXT, @PSTER, @PETER, @MNGFA, @PSTUR, @PETUR, @ERZEIT, @QMANUM, @UPDFLAG, @MOBILEKEY)";
                    var cmdTask = new SQLiteCommand(sqlStmt);

                    sqlStmt = "UPDATE [D_NOTITASK] SET MATXT = @MATXT, MNKAT = @MNKAT, MNGRP = @MNGRP, " +
                              "MNCOD = @MNCOD, MNVER = @MNVER, PSTER = @PSTER, PETER = @PETER, MNGFA = @MNGFA, PSTUR = @PSTUR, " +
                              "ERZEIT = @ERZEIT, QMANUM = @QMANUM " +
                              "WHERE QMNUM = @QMNUM AND MANUM = @MANUM";
                    var cmdTaskUpdate = new SQLiteCommand(sqlStmt);

                    foreach (var task in aNotif._notifTasks)
                    {
                        if (task.Updflag.Equals("I") || task.Updflag.Equals(""))
                            cmd = cmdTask;
                        else
                            cmd = cmdTaskUpdate;

                        cmd.Parameters.Clear();
                        var manum = task.Manum;
                        if (String.IsNullOrEmpty(manum) || manum.StartsWith("z"))
                        {
                            CreateNewNotifTaskID(aNotif.Qmnum, ref manum, true);
                            task.Manum = manum;
                        }

                        if (task.Updflag.Equals(""))
                            task.Updflag = "I";
                        cmd.Parameters.AddWithValue("@UPDFLAG", task.Updflag);

                        if (task.Updflag.Equals("") || task.Updflag.Equals("I") || task.Updflag.Equals("U"))
                        {
                            cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                            cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                            cmd.Parameters.AddWithValue("@QMNUM", aNotif.Qmnum);
                            cmd.Parameters.AddWithValue("@MANUM", task.Manum);
                            cmd.Parameters.AddWithValue("@FENUM", "0");
                            cmd.Parameters.AddWithValue("@URNUM", task.Urnum);
                            cmd.Parameters.AddWithValue("@MNKAT", task.Mnkat);
                            cmd.Parameters.AddWithValue("@MNGRP", task.Mngrp);
                            cmd.Parameters.AddWithValue("@MNCOD", task.Mncod);
                            cmd.Parameters.AddWithValue("@MNVER", task.Mnver);
                            cmd.Parameters.AddWithValue("@MATXT", task.Matxt);
                            cmd.Parameters.AddWithValue("@PSTER", DateTimeHelper.GetDateString(task.Pster));
                            cmd.Parameters.AddWithValue("@PETER", DateTimeHelper.GetDateString(task.Peter));
                            cmd.Parameters.AddWithValue("@MNGFA", task.Mngfa);
                            cmd.Parameters.AddWithValue("@PSTUR", DateTimeHelper.GetTimeString(task.Pstur));
                            cmd.Parameters.AddWithValue("@PETUR", DateTimeHelper.GetTimeString(task.Petur));
                            cmd.Parameters.AddWithValue("@ERZEIT", DateTimeHelper.GetTimeString(task.Erzeit));
                            cmd.Parameters.AddWithValue("@QMANUM", task.Qmanum);
                            cmd.Parameters.AddWithValue("@MOBILEKEY", aNotif.MobileKey);
                            OxDbManager.GetInstance().FeedTransaction(cmd);
                        }
                    }
                }
                if (aNotif._notifActivities != null)
                {
                    sqlStmt = "INSERT OR REPLACE INTO [D_NOTIACTIVITY] " +
                              "(MANDT, USERID, QMNUM, MANUM, MNKAT, MNGRP, MNCOD, MNVER, MATXT, " +
                              "PSTER, PETER, PSTUR, PETUR, ERLDAT, ERLZEIT, FENUM, URNUM, PARVW, " +
                              "PARNR, BAUTL, ERZEIT, UPDFLAG, MOBILEKEY, ERLNAM) " +
                              "Values (@MANDT, @USERID, @QMNUM, @MANUM, @MNKAT, @MNGRP, @MNCOD, @MNVER, @MATXT, " +
                              "@PSTER, @PETER, @PSTUR, @PETUR, @ERLDAT, @ERLZEIT, @FENUM, @URNUM, @PARVW, " +
                              "@PARNR, @BAUTL, @ERZEIT, @UPDFLAG, @MOBILEKEY, @ERLNAM)";
                    var cmdAct = new SQLiteCommand(sqlStmt);

                    sqlStmt = "UPDATE [D_NOTIACTIVITY] SET MATXT = @MATXT, MNKAT = @MNKAT, MNGRP = @MNGRP, " +
                              "MNCOD = @MNCOD, MNVER = @MNVER, PSTER = @PSTER, PETER = @PETER, PSTUR = @PSTUR, " +
                              "PETUR = @PETUR, ERLDAT = @ERLDAT, ERLZEIT = @ERLZEIT, FENUM = @FENUM, URNUM = @URNUM, " +
                              "PARVW = @PARVW, PARNR = @PARNR, BAUTL = @BAUTL, ERZEIT = @ERZEIT, ERLNAM = @ERLNAM " +
                              "WHERE QMNUM = @QMNUM AND MANUM = @MANUM";
                    var cmdActUpdate = new SQLiteCommand(sqlStmt);

                    foreach (var activity in aNotif._notifActivities)
                    {
                        if (activity.Updflag.Equals("I") || activity.Updflag.Equals(""))
                            cmd = cmdAct;
                        else
                            cmd = cmdActUpdate;

                        var manum = activity.Manum;
                        if (String.IsNullOrEmpty(manum) || manum.StartsWith("z"))
                        {
                            CreateNewNotifActivityID(aNotif.Qmnum, ref manum, true);
                            activity.Manum = manum;
                        }
                        if (activity.Updflag.Equals(""))
                            activity.Updflag = "I";
                        cmd.Parameters.AddWithValue("@UPDFLAG", activity.Updflag);

                        if (activity.Updflag.Equals("") || activity.Updflag.Equals("I") || activity.Updflag.Equals("U"))
                        {
                            cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                            cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                            cmd.Parameters.AddWithValue("@QMNUM", aNotif.Qmnum);
                            cmd.Parameters.AddWithValue("@MANUM", activity.Manum);
                            cmd.Parameters.AddWithValue("@MNKAT", activity.Mnkat);
                            cmd.Parameters.AddWithValue("@MNGRP", activity.Mngrp);
                            cmd.Parameters.AddWithValue("@MNCOD", activity.Mncod);
                            cmd.Parameters.AddWithValue("@MNVER", activity.Mnver);
                            cmd.Parameters.AddWithValue("@MATXT", activity.Matxt);
                            cmd.Parameters.AddWithValue("@PSTER", DateTimeHelper.GetDateString(activity.Pster));
                            cmd.Parameters.AddWithValue("@PETER", DateTimeHelper.GetDateString(activity.Peter));
                            cmd.Parameters.AddWithValue("@PSTUR", DateTimeHelper.GetTimeString(activity.Pstur));
                            cmd.Parameters.AddWithValue("@PETUR", DateTimeHelper.GetTimeString(activity.Petur));
                            cmd.Parameters.AddWithValue("@ERLDAT", DateTimeHelper.GetDateString(activity.Erldat));
                            cmd.Parameters.AddWithValue("@ERLZEIT", DateTimeHelper.GetTimeString(activity.Erlzeit));
                            cmd.Parameters.AddWithValue("@FENUM", "0");
                            cmd.Parameters.AddWithValue("@URNUM", activity.Urnum);
                            cmd.Parameters.AddWithValue("@PARVW", activity.Parvw);
                            cmd.Parameters.AddWithValue("@PARNR", activity.Parnr);
                            cmd.Parameters.AddWithValue("@BAUTL", activity.Bautl);
                            cmd.Parameters.AddWithValue("@ERZEIT", DateTimeHelper.GetTimeString(activity.Erlzeit));
                            cmd.Parameters.AddWithValue("@MOBILEKEY", aNotif.MobileKey);
                            cmd.Parameters.AddWithValue("@ERLNAM", activity.Erlnam);
                            OxDbManager.GetInstance().FeedTransaction(cmd);
                        }
                    }
                }
                OxDbManager.GetInstance().CommitTransaction();
                CachingManager.AddNotif(aNotif);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retQmnum;
        }

        public static void ReplaceNotif(Notif _notif)
        {
            try
            {
                var temp = DateTime.Now;

                var sqlStmt = "SELECT D_NOTIHEAD.MANDT, D_NOTIHEAD.USERID, D_NOTIHEAD.QMNUM, D_NOTIHEAD.QMART, D_NOTIHEAD.QMTXT, D_NOTIHEAD.ARTPR, " +
                                "D_NOTIHEAD.PRIOK, D_NOTIHEAD.MZEIT, D_NOTIHEAD.QMDAT, " +
                                "D_NOTIHEAD.STRMN, D_NOTIHEAD.STRUR, D_NOTIHEAD.LTRMN, D_NOTIHEAD.LTRUR, D_NOTIHEAD.AUFNR, D_NOTIHEAD.PRUEFLOS, D_NOTIHEAD.IWERK, " +
                                "D_NOTIHEAD.EQUNR, D_NOTIHEAD.BAUTL, D_NOTIHEAD.AUSVN, D_NOTIHEAD.AUSBS, D_NOTIHEAD.AUZTV, D_NOTIHEAD.AUZTB, D_NOTIHEAD.INGRP, " +
                                "D_NOTIHEAD.WARPL, D_NOTIHEAD.WAPOS, D_NOTIHEAD.TPLNR, D_NOTIHEAD.QMNAM, D_NOTIHEAD.RBNR, D_NOTIHEAD.GEWRK, D_NOTIHEAD.UPDFLAG, " +
                                "D_NOTIHEAD.MOBILEKEY, D_NOTIHEAD.FLAG_PLOS, D_FLOC.PLTXT, D_ORDHEAD.AUFNR, D_EQUI.EQKTX, D_EQUI.STORT, " +
                                "D_EQUI.EQFNR, D_EQUI.TIDNR, D_EQUI.MSGRP " +
                                "FROM D_NOTIHEAD " +
                                "LEFT OUTER JOIN D_FLOC ON " +
                                "D_NOTIHEAD.TPLNR = D_FLOC.TPLNR " +
                                "LEFT OUTER JOIN D_EQUI ON " +
                                "D_NOTIHEAD.EQUNR = D_EQUI.EQUI " +
                                "LEFT OUTER JOIN D_ORDHEAD ON " +
                                "D_NOTIHEAD.AUFNR = D_ORDHEAD.AUFNR WHERE D_NOTIHEAD.QMNUM = @QMNUM ORDER BY D_NOTIHEAD.QMNUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);

                var sqlItem = "SELECT * FROM D_NOTIITEM WHERE QMNUM = @QMNUM ORDER BY FENUM ";
                var cmdItem = new SQLiteCommand(sqlItem);
                cmdItem.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);

                var sqlCause = "SELECT * FROM D_NOTICAUSE WHERE QMNUM = @QMNUM AND FENUM = @FENUM ORDER BY FENUM, URNUM";
                var cmdCause = new SQLiteCommand(sqlStmt);
                cmdCause.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);

                var sqlTask = "SELECT * FROM D_NOTITASK WHERE QMNUM = @QMNUM AND FENUM = @FENUM ORDER BY FENUM, URNUM";
                var cmdTask = new SQLiteCommand(sqlStmt);
                cmdTask.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);

                var sqlAct = "SELECT * FROM D_NOTIACTIVITY WHERE QMNUM = @QMNUM AND FENUM = @FENUM ORDER BY FENUM, MANUM";
                var cmdAct = new SQLiteCommand(sqlStmt);
                cmdAct.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);

                var sqlTask2 = "SELECT * FROM D_NOTITASK WHERE QMNUM = @QMNUM AND FENUM = '0' ORDER BY MANUM";
                var cmdTask2 = new SQLiteCommand(sqlStmt);
                cmdTask2.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);

                var sqlAct2 = "SELECT * FROM D_NOTIACTIVITY WHERE QMNUM = @QMNUM AND FENUM = '0' ORDER BY MANUM";
                var cmdAct2 = new SQLiteCommand(sqlStmt);
                cmdAct2.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);

                foreach (var rdr in records)
                {
                    _notif.Qmnum = (String)rdr["QMNUM"];
                    _notif.Qmart = (String)rdr["QMART"];
                    _notif.Qmtxt = (String)rdr["QMTXT"];
                    _notif.Artpr = (String)rdr["ARTPR"];
                    _notif.Priok = (String)rdr["PRIOK"];
                    DateTimeHelper.getTime(rdr["QMDAT"], rdr["MZEIT"], out temp);
                    _notif.Mzeit = temp;
                    DateTimeHelper.getDate(rdr["QMDAT"], out temp);
                    _notif.Qmdat = temp;
                    DateTimeHelper.getDate(rdr["STRMN"], out temp);
                    _notif.Strmn = temp;
                    DateTimeHelper.getTime(rdr["STRMN"], rdr["STRUR"], out temp);
                    _notif.Strur = temp;
                    DateTimeHelper.getDate(rdr["LTRMN"], out temp);
                    _notif.Ltrmn = temp;
                    DateTimeHelper.getTime(rdr["LTRMN"], rdr["LTRUR"], out temp);
                    _notif.Ltrur = temp;
                    _notif.Aufnr = (String)rdr["AUFNR"];
                    _notif.Prueflos = (String)rdr["PRUEFLOS"];
                    _notif.Iwerk = (String)rdr["IWERK"];
                    _notif.Equnr = (String)rdr["EQUNR"];
                    _notif.Bautl = (String)rdr["BAUTL"];
                    DateTimeHelper.getDate(rdr["AUSVN"], out temp);
                    _notif.Ausvn = temp;
                    DateTimeHelper.getDate(rdr["AUSBS"], out temp);
                    _notif.Ausbs = temp;
                    DateTimeHelper.getTime(rdr["AUSVN"], rdr["AUZTV"], out temp);
                    _notif.Auztv = temp;
                    DateTimeHelper.getTime(rdr["AUSBS"], rdr["AUZTB"], out temp);
                    _notif.Auztb = temp;
                    _notif.Ingrp = rdr["INGRP"];
                    _notif.Warpl = rdr["WARPL"];
                    _notif.Wapos = rdr["WAPOS"];
                    _notif.Tplnr = rdr["TPLNR"];
                    _notif.Qmnam = rdr["QMNAM"];
                    _notif.Rbnr = rdr["RBNR"];
                    _notif.Gewrk = rdr["GEWRK"];
                    _notif.Updflag = rdr["UPDFLAG"];
                    _notif.Eqfnr = rdr["EQFNR"];
                    _notif.FlagPlos = rdr["FLAG_PLOS"];

                    cmdItem.Parameters.Clear();
                    cmdItem.CommandText = sqlItem;
                    cmdItem.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                    // Get items for notification
                    var itemRecords = OxDbManager.GetInstance().FeedTransaction(cmdItem);

                    var _notifItems = new List<NotifItem>();
                    foreach (var rdrItems in itemRecords)
                    {
                        var _notifItem = new NotifItem();
                        _notifItem.Qmnum = rdrItems["QMNUM"];
                        _notifItem.Fenum = rdrItems["FENUM"];
                        _notifItem.Fetxt = rdrItems["FETXT"];
                        _notifItem.Fekat = rdrItems["FEKAT"];
                        _notifItem.Fegrp = rdrItems["FEGRP"];
                        _notifItem.Fecod = rdrItems["FECOD"];
                        _notifItem.Fever = rdrItems["FEVER"];
                        _notifItem.Otkat = rdrItems["OTKAT"];
                        _notifItem.Otgrp = rdrItems["OTGRP"];
                        _notifItem.Oteil = rdrItems["OTEIL"];
                        _notifItem.Otver = rdrItems["OTVER"];
                        _notifItem.Bautl = rdrItems["BAUTL"];
                        DateTimeHelper.getTime(null, rdrItems["ERZEIT"], out temp);
                        _notifItem.Erzeit = temp;
                        _notifItem.Posnr = rdrItems["POSNR"];
                        _notifItem.Updflag = rdrItems["UPDFLAG"];

                        // Get causes for notification item
                        var _notifCauses = new List<NotifCause>();
                        cmdCause.Parameters.Clear();
                        cmdCause.CommandText = sqlCause;
                        cmdCause.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                        cmdCause.Parameters.AddWithValue("@FENUM", rdrItems["FENUM"]);

                        var itemCauseRecords = OxDbManager.GetInstance().FeedTransaction(cmdCause);

                        foreach (var rdrCauses in itemCauseRecords)
                        {
                            var _notifCause = new NotifCause();
                            _notifCause.Qmnum = (String)rdrCauses["QMNUM"];
                            _notifCause.Fenum = (String)rdrCauses["FENUM"];
                            _notifCause.Urnum = (String)rdrCauses["URNUM"];
                            _notifCause.Urtxt = (String)rdrCauses["URTXT"];
                            _notifCause.Urkat = (String)rdrCauses["URKAT"];
                            _notifCause.Urgrp = (String)rdrCauses["URGRP"];
                            _notifCause.Urcod = (String)rdrCauses["URCOD"];
                            _notifCause.Urver = (String)rdrCauses["URVER"];
                            DateTimeHelper.getTime(null, rdrCauses["ERZEIT"], out temp);
                            _notifCause.Erzeit = temp;
                            _notifCause.Bautl = (String)rdrCauses["BAUTL"];
                            _notifCause.Qurnum = (String)rdrCauses["QURNUM"];
                            _notifCause.Updflag = (String)rdrCauses["UPDFLAG"];
                            _notifCauses.Add(_notifCause);
                        }
                        _notifItem._notifItemCauses = _notifCauses;

                        // Get tasks for notification item
                        var _notifItemTasks = new List<NotifTask>();

                        cmdTask.Parameters.Clear();
                        cmdTask.CommandText = sqlTask;
                        cmdTask.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                        cmdTask.Parameters.AddWithValue("@FENUM", rdrItems["FENUM"]);

                        var itemTaskRecords = OxDbManager.GetInstance().FeedTransaction(cmdTask);

                        foreach (var rdrItemTasks in itemTaskRecords)
                        {
                            var _notifItemTask = new NotifTask();
                            _notifItemTask.Qmnum = (String)rdrItemTasks["QMNUM"];
                            _notifItemTask.Manum = (String)rdrItemTasks["MANUM"];
                            _notifItemTask.Fenum = (String)rdrItemTasks["FENUM"];
                            _notifItemTask.Urnum = (String)rdrItemTasks["URNUM"];
                            _notifItemTask.Mnkat = (String)rdrItemTasks["MNKAT"];
                            _notifItemTask.Mngrp = (String)rdrItemTasks["MNGRP"];
                            _notifItemTask.Mncod = (String)rdrItemTasks["MNCOD"];
                            _notifItemTask.Mnver = (String)rdrItemTasks["MNVER"];
                            _notifItemTask.Matxt = (String)rdrItemTasks["MATXT"];
                            DateTimeHelper.getDate(rdrItemTasks["PSTER"], out temp);
                            _notifItemTask.Pster = temp;
                            DateTimeHelper.getDate(rdrItemTasks["PETER"], out temp);
                            _notifItemTask.Peter = temp;
                            _notifItemTask.Mngfa = (String)rdrItemTasks["MNGFA"];
                            DateTimeHelper.getTime(rdrItemTasks["PSTER"], rdrItemTasks["PSTUR"],
                                                   out temp);
                            _notifItemTask.Pstur = temp;
                            DateTimeHelper.getTime(rdrItemTasks["PETER"], rdrItemTasks["PETUR"],
                                                   out temp);
                            _notifItemTask.Petur = temp;
                            DateTimeHelper.getTime(null, rdrItemTasks["ERZEIT"], out temp);
                            _notifItemTask.Erzeit = temp;
                            _notifItemTask.Qmanum = (String)rdrItemTasks["QMANUM"];
                            _notifItemTask.Updflag = (String)rdrItemTasks["UPDFLAG"];
                            _notifItemTasks.Add(_notifItemTask);
                        }
                        _notifItem._notifItemTasks = _notifItemTasks;

                        // Get activities for notification item
                        var _notifItemActivities = new List<NotifActivity>();

                        cmdAct.Parameters.Clear();
                        cmdAct.CommandText = sqlAct;
                        cmdAct.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                        cmdAct.Parameters.AddWithValue("@FENUM", rdrItems["FENUM"]);
                        var itemActRecords = OxDbManager.GetInstance().FeedTransaction(cmdAct);

                        foreach (var rdrItemActivities in itemActRecords)
                        {
                            var _notifItemActivity = new NotifActivity();
                            _notifItemActivity.Qmnum = (String)rdrItemActivities["QMNUM"];
                            _notifItemActivity.Manum = (String)rdrItemActivities["MANUM"];
                            _notifItemActivity.Mnkat = (String)rdrItemActivities["MNKAT"];
                            _notifItemActivity.Mngrp = (String)rdrItemActivities["MNGRP"];
                            _notifItemActivity.Mncod = (String)rdrItemActivities["MNCOD"];
                            _notifItemActivity.Mnver = (String)rdrItemActivities["MNVER"];
                            _notifItemActivity.Matxt = (String)rdrItemActivities["MATXT"];
                            DateTimeHelper.getDate(rdrItemActivities["PSTER"], out temp);
                            _notifItemActivity.Pster = temp;
                            DateTimeHelper.getDate(rdrItemActivities["PETER"], out temp);
                            _notifItemActivity.Peter = temp;
                            DateTimeHelper.getTime(rdrItemActivities["PSTER"], rdrItemActivities["PSTUR"],
                                                   out temp);
                            _notifItemActivity.Pstur = temp;
                            DateTimeHelper.getTime(rdrItemActivities["PETER"], rdrItemActivities["PETUR"],
                                                   out temp);
                            _notifItemActivity.Petur = temp;
                            DateTimeHelper.getDate(rdrItemActivities["ERLDAT"], out temp);
                            _notifItemActivity.Erldat = temp;
                            DateTimeHelper.getTime(rdrItemActivities["ERLDAT"], rdrItemActivities["ERLZEIT"],
                                                   out temp);
                            _notifItemActivity.Erlzeit = temp;
                            _notifItemActivity.Fenum = (String)rdrItemActivities["FENUM"];
                            _notifItemActivity.Urnum = (String)rdrItemActivities["URNUM"];
                            _notifItemActivity.Parvw = (String)rdrItemActivities["PARVW"];
                            _notifItemActivity.Parnr = (String)rdrItemActivities["PARNR"];
                            _notifItemActivity.Bautl = (String)rdrItemActivities["BAUTL"];
                            DateTimeHelper.getTime(null, rdrItemActivities["ERZEIT"], out temp);
                            _notifItemActivity.Erlzeit = temp;
                            _notifItemActivity.Updflag = (String)rdrItemActivities["UPDFLAG"];
                            _notifItemActivity.Erlnam = rdrItemActivities["ERLNAM"];
                            _notifItemActivities.Add(_notifItemActivity);
                        }
                        _notifItem._notifItemActivities = _notifItemActivities;

                        _notifItems.Add(_notifItem);
                    }
                    _notif._notifItems = _notifItems;

                    // Get tasks for notification
                    var _notifTasks = new List<NotifTask>();
                    cmdTask2.Parameters.Clear();
                    cmdTask2.CommandText = sqlTask2;
                    cmdTask2.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                    var taskRecords = OxDbManager.GetInstance().FeedTransaction(cmdTask2);

                    foreach (var rdrTasks in taskRecords)
                    {
                        var _notifTask = new NotifTask();
                        _notifTask.Qmnum = (String)rdrTasks["QMNUM"];
                        _notifTask.Manum = (String)rdrTasks["MANUM"];
                        _notifTask.Fenum = (String)rdrTasks["FENUM"];
                        _notifTask.Urnum = (String)rdrTasks["URNUM"];
                        _notifTask.Mnkat = (String)rdrTasks["MNKAT"];
                        _notifTask.Mngrp = (String)rdrTasks["MNGRP"];
                        _notifTask.Mncod = (String)rdrTasks["MNCOD"];
                        _notifTask.Mnver = (String)rdrTasks["MNVER"];
                        _notifTask.Matxt = (String)rdrTasks["MATXT"];
                        DateTimeHelper.getDate(rdrTasks["PSTER"], out temp);
                        _notifTask.Pster = temp;
                        DateTimeHelper.getDate(rdrTasks["PETER"], out temp);
                        _notifTask.Peter = temp;
                        _notifTask.Mngfa = (String)rdrTasks["MNGFA"];
                        DateTimeHelper.getTime(rdrTasks["PSTER"], rdrTasks["PSTUR"],
                                               out temp);
                        _notifTask.Pstur = temp;
                        DateTimeHelper.getTime(rdrTasks["PETER"], rdrTasks["PETUR"],
                                               out temp);
                        _notifTask.Petur = temp;
                        DateTimeHelper.getTime(null, rdrTasks["ERZEIT"], out temp);
                        _notifTask.Erzeit = temp;
                        _notifTask.Qmanum = (String)rdrTasks["QMANUM"];
                        _notifTask.Updflag = (String)rdrTasks["UPDFLAG"];
                        _notifTasks.Add(_notifTask);
                    }
                    _notif._notifTasks = _notifTasks;

                    // Get activities for notification
                    var _notifActivities = new List<NotifActivity>();

                    cmdAct2.Parameters.Clear();
                    cmdAct2.CommandText = sqlAct2;
                    cmdAct2.Parameters.AddWithValue("@QMNUM", _notif.Qmnum);
                    var actRecords = OxDbManager.GetInstance().FeedTransaction(cmdAct2);

                    foreach (var rdrActivities in actRecords)
                    {
                        var _notifActivity = new NotifActivity();
                        _notifActivity.Qmnum = rdrActivities["QMNUM"];
                        _notifActivity.Manum = rdrActivities["MANUM"];
                        _notifActivity.Mnkat = rdrActivities["MNKAT"];
                        _notifActivity.Mngrp = rdrActivities["MNGRP"];
                        _notifActivity.Mncod = rdrActivities["MNCOD"];
                        _notifActivity.Mnver = rdrActivities["MNVER"];
                        _notifActivity.Matxt = rdrActivities["MATXT"];
                        DateTimeHelper.getDate(rdrActivities["PSTER"], out temp);
                        _notifActivity.Pster = temp;
                        DateTimeHelper.getDate(rdrActivities["PETER"], out temp);
                        _notifActivity.Peter = temp;
                        DateTimeHelper.getTime(rdrActivities["PSTER"], rdrActivities["PSTUR"],
                                               out temp);
                        _notifActivity.Pstur = temp;
                        DateTimeHelper.getTime(rdrActivities["PETER"], rdrActivities["PETUR"],
                                               out temp);
                        _notifActivity.Petur = temp;
                        DateTimeHelper.getDate(rdrActivities["ERLDAT"], out temp);
                        _notifActivity.Erldat = temp;
                        DateTimeHelper.getTime(rdrActivities["ERLDAT"], rdrActivities["ERLZEIT"],
                                               out temp);
                        _notifActivity.Erlzeit = temp;
                        _notifActivity.Fenum = rdrActivities["FENUM"];
                        _notifActivity.Urnum = rdrActivities["URNUM"];
                        _notifActivity.Parvw = rdrActivities["PARVW"];
                        _notifActivity.Parnr = rdrActivities["PARNR"];
                        _notifActivity.Bautl = rdrActivities["BAUTL"];
                        DateTimeHelper.getTime(null, rdrActivities["ERZEIT"], out temp);
                        _notifActivity.Erlzeit = temp;
                        _notifActivity.Updflag = rdrActivities["UPDFLAG"];
                        _notifActivity.Erlnam = rdrActivities["ERLNAM"];
                        _notifActivities.Add(_notifActivity);
                    }
                    _notif._notifActivities = _notifActivities;
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static Notif getNotif(string qmnum)
        {
            var notif = new Notif();
            if (!String.IsNullOrEmpty(qmnum))
            {
                var notiflist = (from n in NotifList
                             where n.Qmnum == qmnum
                             select n).ToList();
                if (notiflist.Count == 1)
                    notif = notiflist[0];
            }

            return notif;
        }

        public static void InsertPosition(Notif notif, NotifItem item)
        {
            try
            {

                String fenum = item.Fenum;
                String sqlStmt = "";
                SQLiteCommand cmd = null;
                if (String.IsNullOrEmpty(fenum))
                {
                    CreateNewNotifItemID(notif.Qmnum, ref fenum, true);
                    item.Fenum = fenum;
                    item.Updflag = "I";
                    item.Posnr = fenum;
                }

                if ((notif.Aufnr.Equals("") && notif.MobileKey.Equals("")))
                {
                    var hash = OxCrypto.HashPasswordForStoringInConfigFile(
                        notif.Qmnum + AppConfig.UserId + AppConfig.Mandt +
                        DateTime.Now.ToString("yyyyMMddHHmmssffff"));
                    notif.MobileKey = hash;

                    sqlStmt = "UPDATE [D_NOTIHEAD] SET " +
                              "UPDFLAG = @UPDFLAG, " +
                              "MOBILEKEY = @MOBILEKEY, " +
                              "WHERE MANDT = @MANDT AND USERID = @USERID AND QMNUM = @QMNUM";
                     cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@UPDFLAG", notif.Updflag);
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@QMNUM", notif.Qmnum);
                    cmd.Parameters.AddWithValue("@MOBILEKEY", notif.MobileKey);

                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                else if(notif.MobileKey.Equals(""))
                {
                    var ord = OrderManager.GetOrder(notif.Aufnr);
                    notif.MobileKey = ord.MobileKey;
                }

                sqlStmt = "INSERT OR REPLACE INTO [D_NOTIITEM] " +
                              "(MANDT, USERID, QMNUM, FENUM, FETXT, FEKAT, FEGRP, FECOD, FEVER, OTKAT, " +
                              "OTGRP, OTEIL, OTVER, BAUTL, ERZEIT, POSNR, UPDFLAG, MOBILEKEY) " +
                              "Values (@MANDT, @USERID, @QMNUM, @FENUM, @FETXT, @FEKAT, @FEGRP, @FECOD, @FEVER, @OTKAT, " +
                              "@OTGRP, @OTEIL, @OTVER, @BAUTL, @ERZEIT, @POSNR, @UPDFLAG, @MOBILEKEY)";

                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@QMNUM", notif.Qmnum);
                cmd.Parameters.AddWithValue("@FENUM", item.Fenum);
                cmd.Parameters.AddWithValue("@FETXT", item.Fetxt);
                cmd.Parameters.AddWithValue("@FEKAT", item.Fekat);
                cmd.Parameters.AddWithValue("@FEGRP", item.Fegrp);
                cmd.Parameters.AddWithValue("@FECOD", item.Fecod);
                cmd.Parameters.AddWithValue("@FEVER", item.Fever);
                cmd.Parameters.AddWithValue("@OTKAT", item.Otkat);
                cmd.Parameters.AddWithValue("@OTGRP", item.Otgrp);
                cmd.Parameters.AddWithValue("@OTEIL", item.Oteil);
                cmd.Parameters.AddWithValue("@OTVER", item.Otver);
                cmd.Parameters.AddWithValue("@BAUTL", item.Bautl);
                cmd.Parameters.AddWithValue("@ERZEIT", item.Erzeit.ToString("HHmmss"));
                cmd.Parameters.AddWithValue("@POSNR", item.Posnr);
                cmd.Parameters.AddWithValue("@MOBILEKEY", notif.MobileKey);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");

                OxDbManager.GetInstance().FeedTransaction(cmd);
                if (!notif._notifItems.Contains(item))
                    notif._notifItems.Add(item);
                else if (notif._notifItems[notif._notifItems.IndexOf(item)].Fenum != item.Fenum)
                {
                    notif._notifItems.Remove(item);
                    notif._notifItems.Add(item);
                }

                //Notification item causes
                if (item._notifItemCauses != null)
                {

                    sqlStmt = "INSERT INTO [D_NOTICAUSE] " +
                              "(MANDT, USERID, QMNUM, FENUM, URNUM, URTXT, URKAT, URGRP, " +
                              "URCOD, URVER, ERZEIT, BAUTL, QURNUM, UPDFLAG, MOBILEKEY) " +
                              "Values (@MANDT, @USERID, @QMNUM, @FENUM, @URNUM, @URTXT, @URKAT, @URGRP, " +
                              "@URCOD, @URVER, @ERZEIT, @BAUTL, @QURNUM, @UPDFLAG, @MOBILEKEY)";
                    cmd = new SQLiteCommand(sqlStmt);

                    foreach (NotifCause cause in item._notifItemCauses)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlStmt;
                        String urnum = cause.Urnum;
                        if (String.IsNullOrEmpty(urnum))
                        {
                            CreateNewNotifCauseID(notif.Qmnum, item.Fenum, ref urnum, true);
                            cause.Urnum = urnum;
                        }

                        if (cause.Updflag.Equals(""))
                            cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                        else
                            cmd.Parameters.AddWithValue("@UPDFLAG", cause.Updflag);

                        cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                        cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                        cmd.Parameters.AddWithValue("@QMNUM", notif.Qmnum);
                        cmd.Parameters.AddWithValue("@FENUM", item.Fenum);
                        cmd.Parameters.AddWithValue("@URNUM", cause.Urnum);
                        cmd.Parameters.AddWithValue("@URTXT", cause.Urtxt);
                        cmd.Parameters.AddWithValue("@URKAT", cause.Urkat);
                        cmd.Parameters.AddWithValue("@URGRP", cause.Urgrp);
                        cmd.Parameters.AddWithValue("@URCOD", cause.Urcod);
                        cmd.Parameters.AddWithValue("@URVER", cause.Urver);
                        cmd.Parameters.AddWithValue("@ERZEIT",
                                                    cause.Erzeit.ToString("HHmmss"));
                        cmd.Parameters.AddWithValue("@BAUTL", cause.Bautl);
                        cmd.Parameters.AddWithValue("@QURNUM", cause.Urnum);
                        cmd.Parameters.AddWithValue("@MOBILEKEY", notif.MobileKey);

                        
                        OxDbManager.GetInstance().FeedTransaction(cmd);
                    }
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void DeleteNotifItem(Notif notif, NotifItem item)
        {
            try
            {

                String sqlStmt = "DELETE FROM [D_NOTIITEM] WHERE [MANDT] = @MANDT " +
                                 "AND [USERID] = @USERID " +
                                 "AND [QMNUM] = @QMNUM AND " +
                                 "[FENUM] = @FENUM AND " +
                                 "[UPDFLAG] = @UPDFLAG";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@QMNUM", item.Qmnum);
                cmd.Parameters.AddWithValue("@FENUM", item.Fenum);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");

                OxDbManager.GetInstance().FeedTransaction(cmd);

                notif._notifItems.Remove(item);

                sqlStmt = "DELETE FROM [D_NOTICAUSE] WHERE [MANDT] = @MANDT " +
                          "AND [USERID] = @USERID " +
                          "AND [QMNUM] = @QMNUM AND " +
                          "[FENUM] = @FENUM AND " +
                          "[URNUM] = @URNUM AND " +
                          "[UPDFLAG] = @UPDFLAG";
                cmd = new SQLiteCommand(sqlStmt);
                foreach (NotifCause notifCause in item._notifItemCauses)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlStmt;
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@QMNUM", notifCause.Qmnum);
                    cmd.Parameters.AddWithValue("@FENUM", notifCause.Fenum);
                    cmd.Parameters.AddWithValue("@URNUM", notifCause.Urnum);
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void DeleteNotifActivity(NotifActivity activity)
        {
            try
            {
                int dbResult = -1;
                String sqlStmt = "DELETE FROM [D_NOTIACTIVITY] WHERE [MANDT] = @MANDT " +
                                 "AND [USERID] = @USERID " +
                                 "AND [QMNUM] = @QMNUM AND " +
                                 "[MANUM] = @MANUM AND " +
                                 "[UPDFLAG] = @UPDFLAG";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@QMNUM", activity.Qmnum);
                cmd.Parameters.AddWithValue("@MANUM", activity.Manum);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void DeleteNotifCause(NotifCause activity)
        {
            try
            {
                int dbResult = -1;
                String sqlStmt = "DELETE FROM [D_NOTICAUSE] WHERE [MANDT] = @MANDT " +
                                 "AND [USERID] = @USERID " +
                                 "AND [QMNUM] = @QMNUM AND " +
                                 "[FENUM] = @FENUM AND " +
                                 "[URNUM] = @URNUM AND " +
                                 "[UPDFLAG] = @UPDFLAG";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@QMNUM", activity.Qmnum);
                cmd.Parameters.AddWithValue("@FENUM", activity.Fenum);
                cmd.Parameters.AddWithValue("@URNUM", activity.Urnum);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void DeleteNotifTask(NotifTask task)
        {
            try
            {
                int dbResult = -1;
                String sqlStmt = "DELETE FROM [D_NOTITASK] WHERE [MANDT] = @MANDT " +
                                 "AND [USERID] = @USERID " +
                                 "AND [QMNUM] = @QMNUM AND " +
                                 "[MANUM] = @MANUM AND " +
                                 "[UPDFLAG] = @UPDFLAG";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@QMNUM", task.Qmnum);
                cmd.Parameters.AddWithValue("@MANUM", task.Manum);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static String GetNotifStatus(String qmnum)
        {
            Notif notif = getNotif(qmnum);
            if (notif != null
                && ((notif._notifItems != null
                && notif._notifItems.Count > 0) || 
                notif._notifActivities != null && notif._notifActivities.Count > 0))
            {
                return "[Erfasst]";
            }
            return "[nicht erfasst]";
        }

        /*public static StatusLocal GetNotifStatus(Notif notif)
        {
            
            var notifStatus = new StatusLocal();
            try
            {
                var SqlStmt = "SELECT " +
                                    " * " +
                              " FROM " +
                                    "C_CUSTNOTIFTYPE";
                var notifTypeRecords = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(SqlStmt));

                foreach (var notifTypeRec in notifTypeRecords)
                {
                    var notifType = new NotifType();
                    notifType.Qmart = (String)notifTypeRec["QMART"];
                    notifType.Rbnr = (String)notifTypeRec["RBNR"];
                    notifType.Auart = (String)notifTypeRec["AUART"];
                    notifType.Stsma = (String)notifTypeRec["STSMA"];
                    notifType.Fekat = (String)notifTypeRec["FEKAT"];
                    notifType.Urkat = (String)notifTypeRec["URKAT"];
                    notifType.Makat = (String)notifTypeRec["MAKAT"];
                    notifType.Mfkat = (String)notifTypeRec["MFKAT"];
                    notifType.Otkat = (String)notifTypeRec["OTKAT"];
                    notifType.Sakat = (String)notifTypeRec["SAKAT"];
                    notifType.CreateAllowed = (String)notifTypeRec["CREATE_ALLOWED"];
                    notifType.Qmartx = (String)notifTypeRec["QMARTX"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            
            return new StatusLocal(StatusLocal.Color.CRed, StatusLocal.Shape.CRect);
        }*/

        public static List<NotifType> GetNotifTypes()
        {
            var notifTypes = new List<NotifType>();
            try
            {
                var SqlStmt = "SELECT " +
                                    " * " +
                              " FROM " +
                                    "C_CUSTNOTIFTYPE";
                var notifTypeRecords = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(SqlStmt));

                foreach (var notifTypeRec in notifTypeRecords)
                {
                    var notifType = new NotifType();
                    notifType.Qmart = notifTypeRec["QMART"];
                    notifType.Rbnr = notifTypeRec["RBNR"];
                    notifType.Auart = notifTypeRec["AUART"];
                    notifType.Stsma = notifTypeRec["STSMA"];
                    notifType.Fekat = notifTypeRec["FEKAT"];
                    notifType.Urkat = notifTypeRec["URKAT"];
                    notifType.Makat = notifTypeRec["MAKAT"];
                    notifType.Mfkat = notifTypeRec["MFKAT"];
                    notifType.Otkat = notifTypeRec["OTKAT"];
                    notifType.Sakat = notifTypeRec["SAKAT"];
                    notifType.CreateAllowed = notifTypeRec["CREATE_ALLOWED"];
                    notifType.Qmartx = notifTypeRec["QMARTX"];
                    notifTypes.Add(notifType);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return notifTypes;
        }

        public static List<NotifType> GetNotifTypes(String auart)
        {
            var notifTypes = new List<NotifType>();
            try
            {
                var sqlStmt = "SELECT " +
                                    " * " +
                              "FROM " +
                                    "C_CUSTNOTIFTYPE " +
                              "WHERE AUART = @AUART";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUART", auart);
                var notifTypeRecords = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

                foreach (var notifTypeRec in notifTypeRecords)
                {
                    var notifType = new NotifType();
                    notifType.Qmart = notifTypeRec["QMART"];
                    notifType.Rbnr = notifTypeRec["RBNR"];
                    notifType.Auart = notifTypeRec["AUART"];
                    notifType.Stsma = notifTypeRec["STSMA"];
                    notifType.Fekat = notifTypeRec["FEKAT"];
                    notifType.Urkat = notifTypeRec["URKAT"];
                    notifType.Makat = notifTypeRec["MAKAT"];
                    notifType.Mfkat = notifTypeRec["MFKAT"];
                    notifType.Otkat = notifTypeRec["OTKAT"];
                    notifType.Sakat = notifTypeRec["SAKAT"];
                    notifType.CreateAllowed = notifTypeRec["CREATE_ALLOWED"];
                    notifType.Qmartx = notifTypeRec["QMARTX"];
                    notifTypes.Add(notifType);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return notifTypes;
        }

        public static NotifType GetNotifType()
        {
            var notifType = new NotifType();
            try
            {
                var SqlStmt = "SELECT " +
                                    " * " +
                              " FROM " +
                                    "C_CUSTNOTIFTYPE";
                var notifTypeRecords = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(SqlStmt));

                foreach (var notifTypeRec in notifTypeRecords)
                {
                    notifType.Qmart = (String)notifTypeRec["QMART"];
                    notifType.Rbnr = (String)notifTypeRec["RBNR"];
                    notifType.Auart = (String)notifTypeRec["AUART"];
                    notifType.Stsma = (String)notifTypeRec["STSMA"];
                    notifType.Fekat = (String)notifTypeRec["FEKAT"];
                    notifType.Urkat = (String)notifTypeRec["URKAT"];
                    notifType.Makat = (String)notifTypeRec["MAKAT"];
                    notifType.Mfkat = (String)notifTypeRec["MFKAT"];
                    notifType.Otkat = (String)notifTypeRec["OTKAT"];
                    notifType.Sakat = (String)notifTypeRec["SAKAT"];
                    notifType.CreateAllowed = (String)notifTypeRec["CREATE_ALLOWED"];
                    notifType.Qmartx = (String)notifTypeRec["QMARTX"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return notifType;
        }

        private static void deleteMissingElements(Notif newNotif)
        {
            var dbNotif = bo.NotifManager.NotifList.Find(delegate(Notif notif)
            {
                return notif.Qmnum == newNotif.Qmnum;
            });
            if (dbNotif == null)
                return;
            if (newNotif._notifItems.Count == 0)
            {

                foreach (NotifItem notifItem in dbNotif._notifItems)
                {
                    foreach (NotifActivity notifActivity in notifItem._notifItemActivities)
                    {
                        if (!notifItem._notifItemActivities[notifItem._notifItemActivities.IndexOf(notifActivity)].Updflag.Equals("X"))
                        {
                            DeleteNotifActivity(notifActivity);
                        }
                    }
                    foreach (NotifCause notifCause in notifItem._notifItemCauses)
                    {
                        if (!notifItem._notifItemCauses[notifItem._notifItemCauses.IndexOf(notifCause)].Updflag.Equals("X"))
                        {
                            DeleteNotifCause(notifCause);
                        }
                    }
                    foreach (NotifTask notifTask in notifItem._notifItemTasks)
                    {
                        if (!notifItem._notifItemTasks[notifItem._notifItemTasks.IndexOf(notifTask)].Updflag.Equals("X"))
                        {
                            DeleteNotifTask(notifTask);
                        }
                    }
                    DeleteNotifItem(dbNotif, notifItem);
                }
            }
            foreach (NotifItem notifItem in dbNotif._notifItems)
            {

                var found = false;
                foreach (NotifItem newItem in newNotif._notifItems)
                {
                    if (newItem.Qmnum == notifItem.Qmnum && newItem.Fenum == notifItem.Fenum)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    foreach (NotifActivity notifActivity in notifItem._notifItemActivities)
                    {
                        if (notifActivity.Updflag.Equals("X"))
                            break;
                        DeleteNotifActivity(notifActivity);
                    }
                    foreach (NotifCause notifCause in notifItem._notifItemCauses)
                    {
                        if (notifCause.Updflag.Equals("X"))
                            break;
                        DeleteNotifCause(notifCause);
                    }
                    foreach (NotifTask notifTask in notifItem._notifItemTasks)
                    {
                        if (notifTask.Updflag.Equals("X"))
                            break;
                        DeleteNotifTask(notifTask);
                    }
                    if (notifItem._notifItemActivities.Count == 0 &&
                        notifItem._notifItemCauses.Count == 0 &&
                        notifItem._notifItemTasks.Count == 0)
                        DeleteNotifItem(dbNotif, notifItem);
                }
            }
            foreach (var notifActivity in dbNotif._notifActivities)
            {
                if (dbNotif._notifActivities.IndexOf(notifActivity) > 0)
                {
                    if (!dbNotif._notifActivities.Contains(notifActivity) &&
                            !dbNotif._notifActivities[dbNotif._notifActivities.IndexOf(notifActivity)].Updflag.Equals("X"))
                    {
                        DeleteNotifActivity(notifActivity);
                    }
                }
            }
            foreach (var notifTask in dbNotif._notifTasks)
            {
                if (dbNotif._notifTasks.IndexOf(notifTask) > 0)
                {
                    if (!dbNotif._notifTasks.Contains(notifTask) &&
                            !dbNotif._notifTasks[dbNotif._notifTasks.IndexOf(notifTask)].Updflag.Equals("X"))
                    {
                        DeleteNotifTask(notifTask);
                    }
                }
            }
        }

        public static void DeleteMobileKeyData(String mobileKey)
        {
            var sqlStmt = "DELETE FROM D_NOTIHEAD WHERE MOBILEKEY = @MOBILEKEY";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MOBILEKEY", mobileKey);
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_NOTIITEM WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_NOTITASK WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_NOTICAUSE WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_NOTIACTIVITY WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
        }

        public static string GetNewTempItemId()
        {
            return "z" + _lastTempItemId++;
        }

        public static string GetNewTempCauseId()
        {
            return "z" + _lastTempCauseId++;
        }

        public static string GetNewTempTaskId()
        {
            return "z" + _lastTempTaskId++;
        }

        public static string GetNewTempActivityId()
        {
            return "z" + _lastTempActivityId++;
        }

        public static bool SaveNotifHead(Notif notif)
        {
            var retVal = false;
            if (String.IsNullOrEmpty(notif.Qmart))
            {

                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                           "Bitte wählen Sie eine Meldungsart.", null,
                                                           true, AppConfig.MessageTimeout);
                return false;
            }
            /*
            if (Cust_012.GetInstance().FlocActive && String.IsNullOrEmpty(notif.Tplnr))
            {

                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                           "Bitte wählen Sie einen technischen Platz aus.", null,
                                                           true, AppConfig.MessageTimeout);
                return false;
            }
             */
            if (!Cust_012.GetInstance().FlocActive && Cust_012.GetInstance().EquiActive && String.IsNullOrEmpty(notif.Equnr))
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                           "Bitte wählen Sie ein Equipment aus.", null,
                                                           true, AppConfig.MessageTimeout);
                return false;
            }
            var qmnum = CreateNotif(notif);
            if (!String.IsNullOrEmpty(qmnum))
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
                                                            "Meldung " + qmnum + " wurde gespeichert.", null,
                                                            true, AppConfig.MessageTimeout); if (!String.IsNullOrEmpty(notif.Aufnr))
                if(!String.IsNullOrEmpty(notif.Aufnr))
                {
                    var order = OrderManager.GetOrder(notif.Aufnr);
                    order.Qmnum = qmnum;
                    OrderManager.UpdateQmnum(order);
                }
                retVal = true;
            }
            else
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                            "Die Meldung konnte nicht gespeichert werden. Vorgang abgebrochen.", null,
                                                            true, AppConfig.MessageTimeout);
            }
            return retVal;
        }

        public static bool SaveNotif(Notif notif)
        {
            var retVal = false;
            if (CheckNotifBeforeSave(notif))
            {
                var qmnum = CreateNotif(notif);
                if (!String.IsNullOrEmpty(qmnum))
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
                                                               "Meldung " + qmnum + " wurde gespeichert.", null,
                                                               true, AppConfig.MessageTimeout);
                    
                    retVal = true;
                }
                else
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                               "Die Meldung konnte nicht gespeichert werden. Vorgang abgebrochen.", null,
                                                               true, AppConfig.MessageTimeout);
                }
            }
            return retVal;
        }

        public static void SetAufnr(string aufnr, string qmnum)
        {
            try
            {
                if(qmnum.StartsWith("MO"))
                {
                    var sqlStmt = "UPDATE D_NOTIHEAD SET AUFNR = @AUFNR WHERE QMNUM = @QMNUM";
                    var cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@AUFNR", aufnr);
                    cmd.Parameters.AddWithValue("@QMNUM", qmnum);
                    OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                }
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private static Boolean CheckNotifBeforeSave(Notif notif)
        {
            foreach (var item in notif._notifItems)
            {
                if (String.IsNullOrEmpty(item.Fegrp) || String.IsNullOrEmpty(item.Fecod))
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                               "Bitte geben Sie bei allen Positionen ein Schadensbild an.", null,
                                                               true, AppConfig.MessageTimeout);
                    return false;
                }

                if (!String.IsNullOrEmpty(item.Otgrp) && String.IsNullOrEmpty(item.Oteil))
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                               "Bitte geben Sie bei allen Positionen das vollständige Objekt an.", null,
                                                               true, AppConfig.MessageTimeout);
                    return false;
                }

                foreach (var cause in item._notifItemCauses)
                {
                    if (String.IsNullOrEmpty(cause.Urgrp) || String.IsNullOrEmpty(cause.Urcod))
                    {
                        OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                                   "Bitte geben Sie bei allen Ursachen Codegruppe und Code an.", null,
                                                                   true, AppConfig.MessageTimeout);
                        return false;
                    }
                }
                foreach (var task in item._notifItemTasks)
                {
                    if (String.IsNullOrEmpty(task.Mngrp) || String.IsNullOrEmpty(task.Mncod))
                    {
                        OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                                   "Bitte geben Sie bei allen Positionsaktionen Codegruppe und Code an.", null,
                                                                   true, AppConfig.MessageTimeout);
                        return false;
                    }
                }
                foreach (var activity in item._notifItemActivities)
                {
                    if (String.IsNullOrEmpty(activity.Mngrp) || String.IsNullOrEmpty(activity.Mncod))
                    {
                        OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                                   "Bitte geben Sie bei allen Positionsmaßnahmen Codegruppe und Code an.", null,
                                                                   true, AppConfig.MessageTimeout);
                        return false;
                    }
                }
            }
            foreach (var task in notif._notifTasks)
            {
                if (String.IsNullOrEmpty(task.Mngrp) || String.IsNullOrEmpty(task.Mncod))
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                               "Bitte geben Sie bei allen Aktionen (Kopf) ein Schadensbild an.",
                                                               null,
                                                               true, AppConfig.MessageTimeout);
                    return false;
                }
            }
            foreach (var activity in notif._notifActivities)
            {
                if (String.IsNullOrEmpty(activity.Mngrp) || String.IsNullOrEmpty(activity.Mncod))
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                               "Bitte geben Sie bei allen Maßnahmen (Kopf) ein Schadensbild an.",
                                                               null,
                                                               true, AppConfig.MessageTimeout);
                    return false;
                }
            }
            return true;
        }
    }
}