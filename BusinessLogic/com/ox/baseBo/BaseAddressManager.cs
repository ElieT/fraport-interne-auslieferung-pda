﻿using System;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.basebo
{
    public class BaseAddressManager
    {
        /// <summary>
        /// Returns an AddressObject determined by the provided addressnumber
        /// </summary>
        /// <param name="addressNumber"></param>
        /// <returns></returns>
        public static Address GetAddress(string addressNumber)
        {
            Address address = null;
            try
            {
                var sqlStmt = "SELECT * FROM C_ADDRESS WHERE ADDRNUMBER = @ADDRNUMBER";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@ADDRNUMBER", addressNumber);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count == 1)
                {
                    address = new Address();
                    address.Updflag = records[0]["UPDFLAG"];
                    address.Addrnumber = records[0]["ADDRNUMBER"];
                    address.Title = records[0]["TITLE"];
                    address.Name1 = records[0]["NAME1"];
                    address.Name2 = records[0]["NAME2"];
                    address.Name3 = records[0]["NAME3"];
                    address.Name4 = records[0]["NAME4"];
                    address.City1 = records[0]["CITY1"];
                    address.City2 = records[0]["CITY2"];
                    address.PostCode1 = records[0]["POST_CODE1"];
                    address.Street = records[0]["STREET"];
                    address.HouseNum1 = records[0]["HOUSE_NUM1"];
                    address.Country = records[0]["COUNTRY"];
                    address.Region = records[0]["REGION"];
                    address.TelNumber = records[0]["TEL_NUMBER"];
                    address.TelExtens = records[0]["TEL_EXTENS"];
                    address.FaxNumber = records[0]["FAX_NUMBER"];
                    address.FaxExtens = records[0]["FAX_EXTENS"];
                }
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return address;
        }

        public static string GetShortAddressString(Address address)
        {
            var retVal = "";
            if(!String.IsNullOrEmpty(address.Name1))
                retVal += address.Name1 + " ";
            if (!String.IsNullOrEmpty(address.Name2))
                retVal += address.Name2 + " ";
            if (!String.IsNullOrEmpty(address.Country))
                retVal += address.Country + " ";
            if (!String.IsNullOrEmpty(address.PostCode1))
                retVal += address.PostCode1 + " ";
            if (!String.IsNullOrEmpty(address.City1))
                retVal += address.City1 + " ";
            if (!String.IsNullOrEmpty(address.City2))
                retVal += address.City2 + " ";
            if (!String.IsNullOrEmpty(address.Street))
                retVal += address.Street + " ";
            if (!String.IsNullOrEmpty(address.HouseNum1))
                retVal += address.HouseNum1 + " ";

            return retVal;
        }
    }
}
