﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseObjectStatusManager
    {
        public static void SetStatus(ObjectStatus objStatus)
        {
            try
            {
                var result = "";
                int intDbKey;

                // Sitzt der Status schon?
                var sqlStmt = "SELECT OBJNR FROM D_OBJECT_STATUS WHERE OBJNR = @OBJNR " +
                                 "AND STATUS_INT = @STATUS_INT " +
                                 "AND INACT = @INACT ";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@OBJNR", objStatus.Objnr);
                cmd.Parameters.AddWithValue("@STATUS_INT", objStatus.StatusInt);
                cmd.Parameters.AddWithValue("@INACT", objStatus.Inact);
                var statusRecords = OxDbManager.GetInstance().FeedTransaction(cmd);
                if (statusRecords.Count > 0)
                    return;

                // Status neu setzen: Neuer DB Schlüssel
                sqlStmt = "SELECT MAX(CHGNR) + 1 FROM D_OBJECT_STATUS WHERE OBJNR = @OBJNR ";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@OBJNR", objStatus.Objnr);

                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;

                objStatus.Chgnr = intDbKey.ToString();
                // Status speichern
                sqlStmt = "INSERT INTO [D_OBJECT_STATUS] " +
                          "(MANDT, USERID, OBJNR, CHGNR, STATUS_INT, INACT, CHANGED, USER_STATUS_CODE, USER_STATUS_DESC, " +
                          "UPDFLAG, MOBILEKEY) " +
                          "Values (@MANDT, @USERID, @OBJNR, @CHGNR, @STATUS_INT, @INACT, @CHANGED, @USER_STATUS_CODE, " +
                          "@USER_STATUS_DESC, @UPDFLAG, @MOBILEKEY)";
                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@OBJNR", objStatus.Objnr);
                cmd.Parameters.AddWithValue("@CHGNR", objStatus.Chgnr);
                cmd.Parameters.AddWithValue("@STATUS_INT", objStatus.StatusInt);
                cmd.Parameters.AddWithValue("@INACT", objStatus.Inact);
                cmd.Parameters.AddWithValue("@CHANGED", objStatus.Changed);
                cmd.Parameters.AddWithValue("@USER_STATUS_CODE", objStatus.UserStatusCode);
                cmd.Parameters.AddWithValue("@USER_STATUS_DESC", objStatus.UserStatusDesc);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@MOBILEKEY", objStatus.MobileKey);
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void DeleteStatus(Order order)
        {
            /*
             * 
             * *** DEACTIVATED FOR CUSTOMER INSTALLATION ***
             * 
            String result = "";
            int intDbKey = -1;
            int dbResult = -1;
            var parameter = new Dictionary<string, string>();

            // Status löschen
            String sqlStmt = "DELETE FROM [D_OBJECT_STATUS] WHERE OBJNR =@OBJNR";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@OBJNR", "OR" + order.Aufnr);
            OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
             * 
             */
        }
    }
}
