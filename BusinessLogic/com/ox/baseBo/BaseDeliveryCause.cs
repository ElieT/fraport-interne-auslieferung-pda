﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDeliveryCause
    {
        private String mandt;
        private String spras;
        private String grund;
        private String text;

        // GETTER/SETTER 
        public string Mandt
        {
            get { return mandt; }
            set { mandt = value; }
        }

        public string Spras
        {
            get { return spras; }
            set { spras = value; }
        }

        public string Grund
        {
            get { return grund; }
            set { grund = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
    }
}
