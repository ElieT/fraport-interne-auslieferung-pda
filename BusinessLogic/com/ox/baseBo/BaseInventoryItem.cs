﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseInventoryItem
    {
        public String Updflag { get; set; }
        public String Physinventory { get; set; }
        public String Fiscalyear { get; set; }
        public String Item { get; set; }
        public String Material { get; set; }
        public String Plant { get; set; }
        public String StgeLoc { get; set; }
        public String Batch { get; set; }
        public String SpecStock { get; set; }
        public String StockType { get; set; }
        public String ChangeUser { get; set; }
        public DateTime ChangeDate { get; set; }
        public String CountUser { get; set; }
        public DateTime CountDate { get; set; }
        public String PhInvRef { get; set; }
        public String Counted { get; set; }
        public String DiffPosted { get; set; }
        public String Recount { get; set; }
        public String AltUnit { get; set; }
        public String BookQty { get; set; }
        public String ZeroCount { get; set; }
        public String Quantity { get; set; }
        public String BaseUom { get; set; }
        public String EntryQty { get; set; }
        public String EntryUom { get; set; }
    }
}
