﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseEquipmentManager
    {
        public static List<Equi> GetEquis()
        {
            var equis = new List<Equi>();
            try
            {
                var sqlStmt = "SELECT * FROM D_EQUI";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var equi = new Equi();
                    equi.Equnr = rdr["EQUI"];
                    equi.EQTYP = rdr["EQTYP"];
                    equi.Objnr = rdr["OBJNR"];
                    equi.EQART = rdr["EQART"];
                    equi.INVNR = rdr["INVNR"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out equi.ANSDT);
                    equi.HERST = rdr["HERST"];
                    equi.HERLD = rdr["HERLD"];
                    equi.SERGE = rdr["SERGE"];
                    equi.TYPBZ = rdr["TYPBZ"];
                    equi.BAUJJ = rdr["BAUJJ"];
                    equi.BAUMM = rdr["BAUMM"];
                    DateTimeHelper.getDate(rdr["INBDT"], out equi.INBDT);
                    equi.MATNR = rdr["MATNR"];
                    equi.SERNR = rdr["SERNR"];
                    equi.WERK = rdr["WERK"];
                    equi.LAGER = rdr["LAGER"];
                    equi.IWERK = rdr["IWERK"];
                    equi.SUBMT = rdr["SUBMT"];
                    equi.HEQUI = rdr["HEQUI"];
                    equi.HEQNR = rdr["HEQNR"];
                    equi.INGRP = rdr["INGRP"];
                    equi.KUND1 = rdr["KUND1"];
                    equi.KUND2 = rdr["KUND2"];
                    equi.KUND3 = rdr["KUND3"];
                    equi.RBNR = rdr["RBNR"];
                    equi.SPRAS = rdr["SPRAS"];
                    equi.EQKTX = rdr["EQKTX"];
                    equi.TPLNR = rdr["TPLNR"];
                    equi.VKORG = rdr["VKORG"];
                    equi.VTWEG = rdr["VTWEG"];
                    equi.SPART = rdr["SPART"];
                    equi.Stort = rdr["STORT"];
                    equi.Tidnr = rdr["TIDNR"];
                    equi.Msgrp = rdr["MSGRP"];
                    equi.Eqfnr = rdr["EQFNR"];
                    var temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT"], out temp);
                    equi.Gwldt = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN"], out temp);
                    equi.Gwlen = temp;
                    equi.Groes = rdr["GROES"];
                    equi.Mapar = rdr["MAPAR"];
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out temp);
                    equi.GwldtI = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out temp);
                    equi.GwlenI = temp;
                    equi.Gewrk = rdr["GEWRK"];
                    equi.Wergw = rdr["WERGW"];
                    equi.TxtGewrk = rdr["TXT_GEWRK"];
                    equi.Adrnr = rdr["ADRNR"];
                    equis.Add(equi);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return equis;
        }


        public static List<Equi> GetEquis(string tplnr)
        {
            var equis = new List<Equi>();
            try
            {
                var sqlStmt = "SELECT * FROM D_EQUI WHERE TPLNR = @TPLNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TPLNR", tplnr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var equi = new Equi();
                    equi.Equnr = rdr["EQUI"];
                    equi.EQTYP = rdr["EQTYP"];
                    equi.Objnr = rdr["OBJNR"];
                    equi.EQART = rdr["EQART"];
                    equi.INVNR = rdr["INVNR"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out equi.ANSDT);
                    equi.HERST = rdr["HERST"];
                    equi.HERLD = rdr["HERLD"];
                    equi.SERGE = rdr["SERGE"];
                    equi.TYPBZ = rdr["TYPBZ"];
                    equi.BAUJJ = rdr["BAUJJ"];
                    equi.BAUMM = rdr["BAUMM"];
                    DateTimeHelper.getDate(rdr["INBDT"], out equi.INBDT);
                    equi.MATNR = rdr["MATNR"];
                    equi.SERNR = rdr["SERNR"];
                    equi.WERK = rdr["WERK"];
                    equi.LAGER = rdr["LAGER"];
                    equi.IWERK = rdr["IWERK"];
                    equi.SUBMT = rdr["SUBMT"];
                    equi.HEQUI = rdr["HEQUI"];
                    equi.HEQNR = rdr["HEQNR"];
                    equi.INGRP = rdr["INGRP"];
                    equi.KUND1 = rdr["KUND1"];
                    equi.KUND2 = rdr["KUND2"];
                    equi.KUND3 = rdr["KUND3"];
                    equi.RBNR = rdr["RBNR"];
                    equi.SPRAS = rdr["SPRAS"];
                    equi.EQKTX = rdr["EQKTX"];
                    equi.TPLNR = rdr["TPLNR"];
                    equi.VKORG = rdr["VKORG"];
                    equi.VTWEG = rdr["VTWEG"];
                    equi.SPART = rdr["SPART"];
                    equi.Stort = rdr["STORT"];
                    equi.Tidnr = rdr["TIDNR"];
                    equi.Msgrp = rdr["MSGRP"];
                    equi.Eqfnr = rdr["EQFNR"];
                    var temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT"], out temp);
                    equi.Gwldt = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN"], out temp);
                    equi.Gwlen = temp;
                    equi.Groes = rdr["GROES"];
                    equi.Mapar = rdr["MAPAR"];
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out temp);
                    equi.GwldtI = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out temp);
                    equi.GwlenI = temp;
                    equi.Gewrk = rdr["GEWRK"];
                    equi.Wergw = rdr["WERGW"];
                    equi.TxtGewrk = rdr["TXT_GEWRK"];
                    equi.Adrnr = rdr["ADRNR"];
                    equis.Add(equi);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return equis;
        }

        public static Equi GetEqui(string equnr)
        {
            var equi = new Equi();
            try
            {
                var sqlStmt = "SELECT * FROM D_EQUI WHERE EQUI = @EQUI";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@EQUI", equnr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    equi.Equnr = rdr["EQUI"];
                    equi.EQTYP = rdr["EQTYP"];
                    equi.Objnr = rdr["OBJNR"];
                    equi.EQART = rdr["EQART"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out equi.ANSDT);
                    equi.HERST = rdr["HERST"];
                    equi.HERLD = rdr["HERLD"];
                    equi.SERGE = rdr["SERGE"];
                    equi.TYPBZ = rdr["TYPBZ"];
                    equi.BAUJJ = rdr["BAUJJ"];
                    equi.BAUMM = rdr["BAUMM"];
                    DateTimeHelper.getDate(rdr["INBDT"], out equi.INBDT);
                    equi.MATNR = rdr["MATNR"];
                    equi.SERNR = rdr["SERNR"];
                    equi.WERK = rdr["WERK"];
                    equi.LAGER = rdr["LAGER"];
                    equi.IWERK = rdr["IWERK"];
                    equi.SUBMT = rdr["SUBMT"];
                    equi.HEQUI = rdr["HEQUI"];
                    equi.HEQNR = rdr["HEQNR"];
                    equi.INGRP = rdr["INGRP"];
                    equi.KUND1 = rdr["KUND1"];
                    equi.KUND2 = rdr["KUND2"];
                    equi.KUND3 = rdr["KUND3"];
                    equi.RBNR = rdr["RBNR"];
                    equi.SPRAS = rdr["SPRAS"];
                    equi.EQKTX = rdr["EQKTX"];
                    equi.TPLNR = rdr["TPLNR"];
                    equi.VKORG = rdr["VKORG"];
                    equi.VTWEG = rdr["VTWEG"];
                    equi.SPART = rdr["SPART"];
                    equi.Stort = rdr["STORT"];
                    equi.Tidnr = rdr["TIDNR"];
                    equi.Msgrp = rdr["MSGRP"];
                    equi.Eqfnr = rdr["EQFNR"];
                    var temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT"], out temp);
                    equi.Gwldt = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN"], out temp);
                    equi.Gwlen = temp;
                    equi.Groes = rdr["GROES"];
                    equi.Mapar = rdr["MAPAR"];
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out temp);
                    equi.GwldtI = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out temp);
                    equi.GwlenI = temp;
                    equi.Gewrk = rdr["GEWRK"];
                    equi.Wergw = rdr["WERGW"];
                    equi.TxtGewrk = rdr["TXT_GEWRK"];
                    equi.Adrnr = rdr["ADRNR"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return equi;
        }

        public static Equi GetEquiByEqfnr(string eqfnr)
        {
            var equi = new Equi();
            try
            {
                var sqlStmt = "SELECT * FROM D_EQUI WHERE EQFNR = @EQFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@EQFNR", eqfnr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    equi.Equnr = rdr["EQUI"];
                    equi.EQTYP = rdr["EQTYP"];
                    equi.Objnr = rdr["OBJNR"];
                    equi.EQART = rdr["EQART"];
                    DateTimeHelper.getDate(rdr["ANSDT"], out equi.ANSDT);
                    equi.HERST = rdr["HERST"];
                    equi.HERLD = rdr["HERLD"];
                    equi.SERGE = rdr["SERGE"];
                    equi.TYPBZ = rdr["TYPBZ"];
                    equi.BAUJJ = rdr["BAUJJ"];
                    equi.BAUMM = rdr["BAUMM"];
                    DateTimeHelper.getDate(rdr["INBDT"], out equi.INBDT);
                    equi.MATNR = rdr["MATNR"];
                    equi.SERNR = rdr["SERNR"];
                    equi.WERK = rdr["WERK"];
                    equi.LAGER = rdr["LAGER"];
                    equi.IWERK = rdr["IWERK"];
                    equi.SUBMT = rdr["SUBMT"];
                    equi.HEQUI = rdr["HEQUI"];
                    equi.HEQNR = rdr["HEQNR"];
                    equi.INGRP = rdr["INGRP"];
                    equi.KUND1 = rdr["KUND1"];
                    equi.KUND2 = rdr["KUND2"];
                    equi.KUND3 = rdr["KUND3"];
                    equi.RBNR = rdr["RBNR"];
                    equi.SPRAS = rdr["SPRAS"];
                    equi.EQKTX = rdr["EQKTX"];
                    equi.TPLNR = rdr["TPLNR"];
                    equi.VKORG = rdr["VKORG"];
                    equi.VTWEG = rdr["VTWEG"];
                    equi.SPART = rdr["SPART"];
                    equi.Stort = rdr["STORT"];
                    equi.Tidnr = rdr["TIDNR"];
                    equi.Msgrp = rdr["MSGRP"];
                    equi.Eqfnr = rdr["EQFNR"];
                    var temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT"], out temp);
                    equi.Gwldt = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN"], out temp);
                    equi.Gwlen = temp;
                    equi.Groes = rdr["GROES"];
                    equi.Mapar = rdr["MAPAR"];
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLDT_I"], out temp);
                    equi.GwldtI = temp;
                    temp = DateTime.Now;
                    DateTimeHelper.getDate(rdr["GWLEN_I"], out temp);
                    equi.GwlenI = temp;
                    equi.Gewrk = rdr["GEWRK"];
                    equi.Wergw = rdr["WERGW"];
                    equi.TxtGewrk = rdr["TXT_GEWRK"];
                    equi.Adrnr = rdr["ADRNR"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return equi;
        }

        public static void SaveEqui(Equi equi)
        {
            try
            {
                var intDbResult = -1;
                var sqlStmt = "UPDATE " +
                                        "D_EQUI " +
                                    "SET " +
                                        "EQTYP = @EQTYP, " +
                                        "OBJNR = @OBJNR, " +
                                        "EQART = @EQART, " +
                                        "INVNR = @INVNR, " +
                                        "ANSDT = @ANSDT, " +
                                        "HERST = @HERST, " +
                                        "HERLD = @HERLD, " +
                                        "SERGE = @SERGE, " +
                                        "TYPBZ = @TYPBZ, " +
                                        "BAUJJ = @BAUJJ, " +
                                        "BAUMM = @BAUMM, " +
                                        "INBDT = @INBDT, " +
                                        "MATNR = @MATNR, " +
                                        "SERNR = @SERNR, " +
                                        "WERK = @WERK, " +
                                        "LAGER = @LAGER, " +
                                        "IWERK = @IWERK, " +
                                        "SUBMT = @SUBMT, " +
                                        "HEQUI = @HEQUI, " +
                                        "INGRP = @INGRP, " +
                                        "KUND1 = @KUND1, " +
                                        "KUND2 = @KUND2, " +
                                        "KUND3 = @KUND3, " +
                                        "RBNR = @RBNR, " +
                                        "SPRAS = @SPRAS, " +
                                        "EQKTX = @EQKTX, " +
                                        "TPLNR = @TPLNR, " +
                                        "VKORG = @VKORG, " +
                                        "VTWEG = @VTWEG, " +
                                        "SPART = @SPART, " +
                                        "STORT = @STORT, " +
                                        "TIDNR = @TIDNR, " +
                                        "MSGRP = @MSGRP, " +
                                        "GWLDT = @GWLDT, " +
                                        "GWLEN = @GWLEN, " +
                                        "GROES = @GROES, " +
                                        "MAPAR = @MAPAR, " +
                                        "GWLDT_I = @GWLDT_I, " +
                                        "GWLEN_I = @GWLEN_I, " +
                                        "GEWRK = @GEWRK, " +
                                        "WERGW = @WERGW, " +
                                        "TXT_GEWRK = @TXT_GEWRK " +
                                    "WHERE EQUI = @EQUI";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@EQUI", equi.EQUNR);
                cmd.Parameters.AddWithValue("@EQTYP", equi.EQTYP);
                cmd.Parameters.AddWithValue("@OBJNR", equi.Objnr);
                cmd.Parameters.AddWithValue("@EQART", equi.EQART);
                cmd.Parameters.AddWithValue("@INVNR", equi.INVNR);
                cmd.Parameters.AddWithValue("@ANSDT", DateTimeHelper.GetDateString(equi.ANSDT));
                cmd.Parameters.AddWithValue("@HERST", equi.HERST);
                cmd.Parameters.AddWithValue("@HERLD", equi.HERLD);
                cmd.Parameters.AddWithValue("@SERGE", equi.SERGE);
                cmd.Parameters.AddWithValue("@TYPBZ", equi.TYPBZ);
                cmd.Parameters.AddWithValue("@BAUJJ", equi.BAUJJ);
                cmd.Parameters.AddWithValue("@BAUMM", equi.BAUMM);
                cmd.Parameters.AddWithValue("@INBDT", DateTimeHelper.GetDateString(equi.INBDT));
                cmd.Parameters.AddWithValue("@MATNR", equi.MATNR);
                cmd.Parameters.AddWithValue("@SERNR", equi.SERNR);
                cmd.Parameters.AddWithValue("@WERK", equi.WERK);
                cmd.Parameters.AddWithValue("@LAGER", equi.LAGER);
                cmd.Parameters.AddWithValue("@IWERK", equi.IWERK);
                cmd.Parameters.AddWithValue("@SUBMT", equi.SUBMT);
                cmd.Parameters.AddWithValue("@HEQUI", equi.HEQUI);
                cmd.Parameters.AddWithValue("@INGRP", equi.INGRP);
                cmd.Parameters.AddWithValue("@KUND1", equi.KUND1);
                cmd.Parameters.AddWithValue("@KUND2", equi.KUND2);
                cmd.Parameters.AddWithValue("@KUND3", equi.KUND3);
                cmd.Parameters.AddWithValue("@RBNR", equi.RBNR);
                cmd.Parameters.AddWithValue("@SPRAS", equi.SPRAS);
                cmd.Parameters.AddWithValue("@EQKTX", equi.EQKTX);
                cmd.Parameters.AddWithValue("@TPLNR", equi.TPLNR);
                cmd.Parameters.AddWithValue("@VKORG", equi.VKORG);
                cmd.Parameters.AddWithValue("@VTWEG", equi.VTWEG);
                cmd.Parameters.AddWithValue("@SPART", equi.SPART);
                cmd.Parameters.AddWithValue("@STORT", equi.Stort);
                cmd.Parameters.AddWithValue("@TIDNR", equi.Tidnr);
                cmd.Parameters.AddWithValue("@MSGRP", equi.Msgrp);
                cmd.Parameters.AddWithValue("@EQFNR", equi.Eqfnr);
                cmd.Parameters.AddWithValue("@GWLDT", DateTimeHelper.GetDateString(equi.Gwldt));
                cmd.Parameters.AddWithValue("@GWLEN", DateTimeHelper.GetDateString(equi.Gwlen));
                cmd.Parameters.AddWithValue("@GROES", equi.Groes);
                cmd.Parameters.AddWithValue("@MAPAR", equi.Mapar);
                cmd.Parameters.AddWithValue("@GWLDT_I", DateTimeHelper.GetDateString(equi.GwldtI));
                cmd.Parameters.AddWithValue("@GWLEN_I", DateTimeHelper.GetDateString(equi.GwlenI));
                cmd.Parameters.AddWithValue("@GEWRK", equi.Gewrk);
                cmd.Parameters.AddWithValue("@WERGW", equi.Wergw);
                cmd.Parameters.AddWithValue("@TXT_GEWRK", equi.TxtGewrk);
                cmd.Parameters.AddWithValue("@ADRNR", equi.Adrnr);
                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                intDbResult = int.Parse(result[0]["RetVal"]);
                if (intDbResult != 1)
                {
                    throw new Exception("Wrong SQLQuery result. Expecting 1, result was " + intDbResult);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static List<ObjClass> GetObjClass(Equi equipment)
        {
            var objClasses = new List<ObjClass>();
            try
            {
                var sqlStmt = "SELECT DISTINCT " +
                                   "D_OBJCLASS.OBJNR, " +
                                   "D_OBJCLASS.ATINN, " +
                                   "D_OBJCLASS.CLINT, " +
                                   "D_OBJCLASS.ATZHL, " +
                                   "D_OBJCLASS.MAFID, " +
                                   "D_OBJCLASS.KLART, " +
                                   "D_OBJCLASS.ADZHL, " +
                                   "D_OBJCLASS.ATFLV, " +
                                   "D_OBJCLASS.ATFLB, " +
                                   "D_OBJCLASS.ATAWE, " +
                                   "D_OBJCLASS.ATAW1, " +
                                   "D_OBJCLASS.ATCOD, " +
                                   "D_OBJCLASS.ATTLV, " +
                                   "D_OBJCLASS.ATTLB, " +
                                   "D_OBJCLASS.ATWRT, " +
                                   "C_CLASSCHARACT.ATBEZ, " +
                                   "C_CLASSCHARACT.ATFOR, " +
                                   "C_CLASSCHARACT.ATEIN, " +
                                   "C_CLASSCHARACT.MSEHI, " +
                                   "C_CLASSCHARACT.MSEH6, " +
                                   "C_CLASSCHARACT.MSEHT, " +
                                   "C_CLASSCHARACT.ATINT " +
                               "FROM D_OBJCLASS " +
                               "LEFT OUTER JOIN C_CLASSCHARACT ON " +
                                   "C_CLASSCHARACT.ATINN = D_OBJCLASS.ATINN AND C_CLASSCHARACT.CLINT IN (SELECT C_OBJCLASSAS.CLINT FROM C_OBJCLASSAS WHERE C_OBJCLASSAS.OBJNR = D_OBJCLASS.OBJNR)" +
                               "LEFT OUTER JOIN " +
                                   "C_CHARACTVALUE ON C_CHARACTVALUE.ATINN = C_CLASSCHARACT.ATINN AND C_CHARACTVALUE.ADZHL = C_CLASSCHARACT.ADZHL AND C_CHARACTVALUE.ATZHL = D_OBJCLASS.ATZHL " +
                               "WHERE " +
                               "D_OBJCLASS.OBJNR = @OBJNR " +
                               "ORDER BY D_OBJCLASS.OBJNR, D_OBJCLASS.ATINN";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@OBJNR", equipment.Objnr);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);

                foreach (var rdr in records)
                {
                    var objClass = new ObjClass();

                    objClass.Objnr = rdr["OBJNR"];
                    objClass.Atinn = rdr["ATINN"];
                    objClass.Clint = rdr["CLINT"];
                    objClass.Atzhl = rdr["ATZHL"];
                    objClass.Mafid = rdr["MAFID"];
                    objClass.Klart = rdr["KLART"];
                    objClass.Adzhl = rdr["ADZHL"];
                    objClass.Atflv = rdr["ATFLV"];
                    objClass.Atflb = rdr["ATFLB"];
                    objClass.Atawe = rdr["ATAWE"];
                    objClass.Ataw1 = rdr["ATAW1"];
                    objClass.Atcod = rdr["ATCOD"];
                    objClass.Attlv = rdr["ATTLV"];
                    objClass.Attlb = rdr["ATTLB"];
                    objClass.Atwrt = rdr["ATWRT"];
                    objClass.Atbez = rdr["ATBEZ"];
                    objClass.Atfor = rdr["ATFOR"];
                    objClass.Atein = rdr["ATEIN"];
                    objClass.Atint = rdr["ATINT"];
                    objClass.Msehi = rdr["MSEHI"];
                    objClass.Mseh6 = rdr["MSEH6"];
                    objClass.Mseht = rdr["MSEHT"];
                    objClass.Objnr = equipment.Objnr;
                    objClasses.Add(objClass);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return objClasses;
        }

        public static List<MeasurementPoint> GetMeasPoints(Equi equi)
        {
            var measPoints = new List<MeasurementPoint>();
            try
            {
                var sqlStmt = "SELECT * FROM C_MEASPOINT WHERE MPOBJ = @MPOBJ";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MPOBJ", "IE" + equi.Equnr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var measPoint = new MeasurementPoint();
                    measPoint.Point = rdr["POINT"];
                    measPoint.Mpobj = rdr["MPOBJ"];
                    measPoint.Psort = rdr["PSORT"];
                    measPoint.Pttxt = rdr["PTTXT"];
                    measPoint.Locas = rdr["LOCAS"];
                    measPoint.Atinn = rdr["ATINN"];
                    measPoint.Mrmin = rdr["MRMIN"];
                    measPoint.Mrmax = rdr["MRMAX"];
                    measPoint.Mrngu = rdr["MRNGU"];
                    measPoint.Desir = rdr["DESIR"];
                    measPoint.Indct = rdr["INDCT"];
                    measPoint.Indrv = rdr["INDRV"];
                    measPoints.Add(measPoint);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return measPoints;
        }

        public static List<String[]> GetMasterEditingList(Equi equi)
        {
            var ret = new List<String[]>();
            try
            {
                ret.Add(new[] { "EQUI", "Equipment", equi.EQUNR, "X" });
                ret.Add(new[] { "OBJNR", "Objektnummer", equi.Objnr, "X" });
                ret.Add(new[] { "INVNR", "Inventarnummer", equi.INVNR, "" });
                ret.Add(new[] { "ANSDT", "Anschaff. Datum", equi.ANSDT.ToString("dd.MM.yyyy"), "" });
                ret.Add(new[] { "HERST", "Hersteller", equi.HERST, "" });
                ret.Add(new[] { "STORT", "Standort", equi.Stort, "" });
                ret.Add(new[] { "MSGRP", "Raum", equi.Msgrp, "" });
                ret.Add(new[] { "HERLD", "Herstellerland", equi.HERLD, "" });
                ret.Add(new[] { "SERGE", "Serialnummer", equi.SERGE, "" });
                ret.Add(new[] { "TYPBZ", "Typenbez. Herst.", equi.TYPBZ, "" });
                ret.Add(new[] { "BAUJJ", "Baujahr", equi.BAUJJ, "" });
                ret.Add(new[] { "BAUMM", "Baumonat", equi.BAUMM, "" });
                //ret.Add(new[] { "GWLDT_I", "Gewährl. Begin", equi.GwldtI.ToString("dd.MM.yyyy"), "" });
                //ret.Add(new[] { "GWLEN_I", "Gewährl. Ende", equi.GwlenI.ToString("dd.MM.yyyy"), "" });
                ret.Add(new[] { "TIDNR", "Tech. Identnr.", equi.Tidnr, "" });
                ret.Add(new[] { "EQFNR", "Sortierfeld", equi.Eqfnr, "" });
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return ret;
        }


        public static void SetMasterEditingData(Dictionary<String, TextBox> tbList, Equi equi)
        {
            try
            {
                //equi.Objnr = tbList["tb_OBJNR"].Text;
                equi.Invnr = tbList["tb_INVNR"].Text;
                if (!String.IsNullOrEmpty(tbList["tb_ANSDT"].Text))
                    equi.ANSDT = DateTimeHelper.getDate(tbList["tb_ANSDT"].Text);
                equi.HERST = tbList["tb_HERST"].Text;
                equi.Stort = tbList["tb_STORT"].Text;
                equi.Msgrp = tbList["tb_MSGRP"].Text;
                equi.HERLD = tbList["tb_HERLD"].Text;
                equi.SERGE = tbList["tb_SERGE"].Text;
                equi.TYPBZ = tbList["tb_TYPBZ"].Text;
                equi.BAUJJ = tbList["tb_BAUJJ"].Text;
                equi.BAUMM = tbList["tb_BAUMM"].Text;
                /*
                if (!String.IsNullOrEmpty(tbList["tb_GWLDT_I"].Text))
                    equi.GwldtI = DateTimeHelper.getDate(tbList["tb_GWLDT_I"].Text);
                if (!String.IsNullOrEmpty(tbList["tb_GWLEN_I"].Text))
                    equi.GwlenI = DateTimeHelper.getDate(tbList["tb_GWLEN_I"].Text);
                 * */
                equi.Tidnr = tbList["tb_TIDNR"].Text;
                equi.Eqfnr = tbList["tb_EQFNR"].Text;
                SaveEqui(equi);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}