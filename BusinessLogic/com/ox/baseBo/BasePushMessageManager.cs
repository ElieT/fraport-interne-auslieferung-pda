﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BasePushMessageManager
    {
        public static List<PushMessage> GetPushMessages()
        {
            var messages = new List<PushMessage>();
            try
            {
                String sqlStmt = "SELECT * FROM D_PUSH_MESSAGE";
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var message = new PushMessage();
                    message.MessageId = (String)rdr["MESSAGEID"];
                    message.Message = (String)rdr["MESSAGE"];
                    DateTime t_date = DateTime.Now;
                    DateTimeHelper.getTime(rdr["MESSAGEDATE"], rdr["MESSAGETIME"], out t_date);
                    message.MessageDate = t_date;
                    messages.Add(message);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return messages;
        }
        public static void SavePushMessage(PushMessage message)
        {
            try
            {
                int intDbKey = -1;
                int dbResult = -1;


                // Get Key
                String sqlStmt = "SELECT MAX(rowid) + 1 FROM D_PUSH_MESSAGE";
                var cmd = new SQLiteCommand(sqlStmt);
                var record = OxDbManager.GetInstance().FeedTransaction(cmd);
                var result = record[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;

                message.MessageId = intDbKey.ToString();

                // Insert Data
                sqlStmt = "INSERT INTO [D_PUSH_MESSAGE] " +
                          "(MANDT, USERID, UPDFLAG, MESSAGEID, MESSAGE, MESSAGEDATE, MESSAGETIME) " +
                          "Values (@MANDT, @USERID, @UPDFLAG, @MESSAGEID, @MESSAGE, @MESSAGEDATE, @MESSAGETIME)";
                cmd = new SQLiteCommand(sqlStmt);


                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@MESSAGEID", message.MessageId);
                cmd.Parameters.AddWithValue("@MESSAGE", message.Message);
                cmd.Parameters.AddWithValue("@MESSAGEDATE", DateTimeHelper.GetDateString(message.MessageDate));
                cmd.Parameters.AddWithValue("@MESSAGETIME", DateTimeHelper.GetTimeString(message.MessageDate));
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}
