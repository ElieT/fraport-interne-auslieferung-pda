﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseEMSEqui
    {
        public String bnv, einbauort;
        public String equnr;
        public String leerungsdienst;
        public String nummer;
        public String richtung;
        public String standort;
        public String typ;
    }
}