﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOrdOperation : OxBusinessObject, IComparable
    {
        protected String aufnr;
        protected String ktext;
        protected string ltxa1;
        protected String pltxt;
        protected String priok;
        protected String qmtxt;
        protected String vornr;
        protected String _aufpl;
        protected String _aplzl;
        protected String larnt;
        protected DateTime fsavd;
        protected DateTime fsavz;
        protected String tplnr;
        protected String pernr;
        protected String dauno;
        protected String daune;
        protected String arbei;
        protected String arbeh;
        protected DateTime fsedd;
        protected DateTime fsedz;
        protected String arbpl;
        protected String werks;
        protected String updflag;
        protected String _anzma;
        protected DateTime _ntanf;
        protected DateTime _ntanz;
        protected DateTime _ntend;
        protected DateTime _ntenz;
        protected String _isdd;
        protected String _isdz;
        protected String _iedd;
        protected String _iedz;
        protected String _mobileKey;

        public String Steus { get; set; }
        public string Bedid { get; set; }
        public string Bedzl { get; set; }
        public string Canum { get; set; }
        public string Arbid { get; set; }
        public DateTime Fstad { get; set; }
        public DateTime Fstau { get; set; }
        public DateTime Fendd { get; set; }
        public DateTime Fendu { get; set; }
        public string Split { get; set; }
        public string Istru { get; set; }

        protected BaseOrdOperation()
        {
            aufnr = "";
            ktext = "";
            ltxa1 = "";
            pltxt = "";
            priok = "";
            qmtxt = "";
            vornr = "0010";
            _aufpl = "";
            _aplzl = "";
            larnt = "";
            fsavd = DateTime.Now;
            fsavz = DateTime.Now;
            tplnr = "";
            pernr = "";
            dauno = "0";
            daune = "H";
            arbei = "0";
            arbeh = "H";
            fsedd = DateTime.Now;
            fsedz = DateTime.Now;
            arbpl = Cust_001.GetInstance().OrderWorkcenter;
            werks = Cust_001.GetInstance().OrderPlantWrkc;
            updflag = "";
            MobileKey = "";
            Bedid = "";
            Bedzl = "";
            Canum = "";
            Arbid = "";
            Fstad = DateTime.MinValue;
            Fstau = DateTime.MinValue;
            Fendd = DateTime.MinValue;
            Fendu = DateTime.MinValue;
            Split = "";
        }


        protected BaseOrdOperation(OrdOperation ordOper)
        {
            Anzma = "";
            Arbeh = ordOper.Arbeh;
            Arbei = ordOper.Arbei;
            Arbpl = ordOper.Arbpl;
            Aufnr = ordOper.Aufnr;
            Daune = ordOper.Daune;
            Dauno = ordOper.Dauno;
            Fsavd = ordOper.Fsavd;
            Fsavz = ordOper.Fsavz;
            Fsedd = ordOper.Fsedd;
            Fsedz = ordOper.Fsedz;
            Larnt = ordOper.Larnt;
            Ltxa1 = ordOper.Ltxa1;
            Pernr = ordOper.Pernr;
            vornr = ordOper.Vornr.PadLeft(4, '0');
            Aufpl = ordOper.Aufpl;
            Aplzl = ordOper.Aplzl;
            Werks = ordOper.Werks;
            MobileKey = ordOper.MobileKey;
        }

        protected BaseOrdOperation(Order order)
        {
            aufnr = order.Aufnr;
            ktext = order.Ktext;
            ltxa1 = "";
            pltxt = order.Tplnr;
            priok = "";
            Notif notif = NotifManager.getNotif(order.Qmnum);
            if (notif != null && notif.Qmtxt != null)
                qmtxt = notif.Qmtxt;
            vornr = "0010";
            _aufpl = "";
            _aplzl = "";
            larnt = "";
            fsavd = DateTime.Now;
            fsavz = DateTime.Now;
            tplnr = "";
            pernr = "";
            dauno = "0";
            daune = "H";
            arbei = "0";
            arbeh = "H";
            fsedd = DateTime.Now;
            fsedz = DateTime.Now;
            arbpl = Cust_001.GetInstance().OrderWorkcenter;
            werks = Cust_001.GetInstance().OrderPlantWrkc;
            updflag = "";
            MobileKey = order.MobileKey;
            Bedid = "";
            Bedzl = "";
            Canum = "";
            Arbid = "";
            Fstad = DateTime.MinValue;
            Fstau = DateTime.MinValue;
            Fendd = DateTime.MinValue;
            Fendu = DateTime.MinValue;
            Split = "";
        }

        protected BaseOrdOperation(Order order, int newVornr)
        {
            aufnr = order.Aufnr;
            ktext = order.Ktext;
            ltxa1 = "";
            pltxt = order.Tplnr;
            priok = "";
            Notif notif = NotifManager.getNotif(order.Qmnum);
            if (notif != null && notif.Qmtxt != null)
                qmtxt = notif.Qmtxt;
            vornr = newVornr.ToString().PadLeft(4, '0');
            _aufpl = "";
            _aplzl = "";
            larnt = "";
            fsavd = DateTime.Now;
            fsavz = DateTime.Now;
            tplnr = "";
            pernr = "";
            dauno = "0";
            daune = "H";
            arbei = "0";
            arbeh = "H";
            fsedd = DateTime.Now;
            fsedz = DateTime.Now;

            if (String.IsNullOrEmpty(order.Vaplz))
                arbpl = Cust_001.GetInstance().OrderWorkcenter;
            else
                arbpl = order.Vaplz;
            if (String.IsNullOrEmpty(order.Wawrk))
                werks = Cust_001.GetInstance().OrderPlantWrkc;
            else
                werks = order.Wawrk;

            //arbpl = Cust_001.GetInstance().OrderWorkcenter;
            //werks = Cust_001.GetInstance().OrderPlantWrkc;
            updflag = "";
            MobileKey = order.MobileKey;
            Bedid = "";
            Bedzl = "";
            Canum = "";
            Arbid = "";
            Fstad = DateTime.MinValue;
            Fstau = DateTime.MinValue;
            Fendd = DateTime.MinValue;
            Fendu = DateTime.MinValue;
            Split = "";
        }

        public virtual string Aufnr
        {
            get { return aufnr; }
            set { aufnr = value; }
        }

        public virtual string Updflag
        {
            get { return updflag; }
            set { updflag = value; }
        }

        public virtual string Ktext
        {
            get { return ktext; }
            set { ktext = value; }
        }

        public virtual string Ltxa1
        {
            get { return ltxa1; }
            set { ltxa1 = value; }
        }

        public virtual string Pltxt
        {
            get { return pltxt; }
            set { pltxt = value; }
        }

        public virtual string Priok
        {
            get { return priok; }
            set { priok = value; }
        }

        public virtual string Qmtxt
        {
            get { return qmtxt; }
            set { qmtxt = value; }
        }

        public virtual string Vornr
        {
            get { return vornr; }
            set { vornr = value; }
        }

        public virtual string Aufpl
        {
            get { return _aufpl; }
            set { _aufpl = value; }
        }

        public virtual string Aplzl
        {
            get { return _aplzl; }
            set { _aplzl = value; }
        }

        public virtual string Larnt
        {
            get { return larnt; }
            set { larnt = value; }
        }

        public virtual DateTime Fsavd
        {
            get { return fsavd; }
            set { fsavd = value; }
        }

        public virtual DateTime Fsavz
        {
            get { return fsavz; }
            set { fsavz = value; }
        }

        public virtual string Tplnr
        {
            get { return tplnr; }
            set { tplnr = value; }
        }

        public virtual string Pernr
        {
            get { return pernr; }
            set { pernr = value; }
        }

        public virtual string Dauno
        {
            get { return dauno; }
            set { dauno = value; }
        }

        public virtual string Daune
        {
            get { return daune; }
            set { daune = value; }
        }

        public virtual string Arbei
        {
            get { return arbei; }
            set { arbei = value; }
        }

        public virtual string Arbeh
        {
            get { return arbeh; }
            set { arbeh = value; }
        }

        public virtual DateTime Fsedd
        {
            get { return fsedd; }
            set { fsedd = value; }
        }

        public virtual DateTime Fsedz
        {
            get { return fsedz; }
            set { fsedz = value; }
        }

        public virtual string Arbpl
        {
            get { return arbpl; }
            set { arbpl = value; }
        }

        public virtual string Werks
        {
            get { return werks; }
            set { werks = value; }
        }

        public virtual DateTime Ntanf
        {
            get { return _ntanf; }
            set { _ntanf = value; }
        }

        public virtual DateTime Ntanz
        {
            get { return _ntanz; }
            set { _ntanz = value; }
        }

        public virtual DateTime Ntend
        {
            get { return _ntend; }
            set { _ntend = value; }
        }

        public virtual DateTime Ntenz
        {
            get { return _ntenz; }
            set { _ntenz = value; }
        }

        public virtual string Isdd
        {
            get { return _isdd; }
            set { _isdd = value; }
        }

        public virtual string Isdz
        {
            get { return _isdz; }
            set { _isdz = value; }
        }

        public virtual string Iedd
        {
            get { return _iedd; }
            set { _iedd = value; }
        }

        public virtual string Iedz
        {
            get { return _iedz; }
            set { _iedz = value; }
        }

        public virtual string Anzma
        {
            get { return _anzma; }
            set { _anzma = value; }
        }

        public virtual string MobileKey
        {
            get { return _mobileKey; }
            set { _mobileKey = value; }

        }

        #region IComparable Members

        public virtual int CompareTo(object obj1)
        {
            var obj2 = (OrdOperation)obj1;
            if (Fstau != null && obj2.Fstau != null)
            {
                if (Fstau > obj2.Fstau)
                    return (-1);
                if (Fstau < obj2.Fstau)
                    return (1);
                return (0);
            }
            if (Fstau != null && obj2.Fstau == null)
            {
                if (Fstau > obj2.Fsavz)
                    return (-1);
                if (Fstau < obj2.Fsavz)
                    return (1);
                return (0);
            }
            if (Fstau == null && obj2.Fstau != null)
            {
                if (Fsavz > obj2.Fstau)
                    return (-1);
                if (Fsavz < obj2.Fstau)
                    return (1);
                return (0);
            }
            if (Fstau > obj2.Fstau)
                return (-1);
            if (Fstau < obj2.Fstau)
                return (1);
            else
                return (0);
        }

        protected static String _sortBy = "";
        protected static Boolean _desc;

        public static void SetSortMode(String sortByVar, Boolean descVar)
        {
            _sortBy = sortByVar;
            _desc = descVar;
        }

        //Sorts the List according to the Values set in setSortMode()
        public class BaseSortOrdOperation : IComparer<OrdOperation>
        {
            public int Compare(OrdOperation obj1, OrdOperation obj2)
            {
                var emp1 = obj1;
                var emp2 = obj2;
                int returnVal = 0;

                //definition of Variables to compare by direction
                if (_desc)
                {
                    emp1 = obj1;
                    emp2 = obj2;
                }
                else
                {
                    emp1 = obj2;
                    emp2 = obj1;
                }

                //Sets which Fields in the List will be compared
                switch (_sortBy.ToLower())
                {
                    case "fsavz":
                        goto case "fstau";
                    case "fstau":
                        if (emp1.Fstau != null && emp2.Fstau != null)
                        {
                            if (emp1.Fstau > emp2.Fstau)
                                return (-1);
                            if (emp1.Fstau < emp2.Fstau)
                                return (1);
                            return (0);
                        }
                        if (emp1.Fstau != null && emp2.Fstau == null)
                        {
                            if (emp1.Fstau > emp2.Fsavz)
                                return (-1);
                            if (emp1.Fstau < emp2.Fsavz)
                                return (1);
                            return (0);
                        }
                        if (emp1.Fstau == null && emp2.Fstau != null)
                        {
                            if (emp1.Fsavz > emp2.Fstau)
                                return (-1);
                            if (emp1.Fsavz < emp2.Fstau)
                                return (1);
                            return (0);
                        }
                        if (emp1.Fstau > emp2.Fstau)
                            return (-1);
                        if (emp1.Fstau < emp2.Fstau)
                            return (1);
                        return (0);
                    case "aufnr":
                        return (String.Compare(emp1.aufnr, emp2.aufnr));
                    case "priok":
                        return (String.Compare(emp1.priok, emp2.priok));
                    case "vornr":
                        return (String.Compare(emp1.Vornr, emp2.Vornr));
                    case "aufpl":
                        return (String.Compare(emp1.Aufpl, emp2.Aufpl));
                    case "aplzl":
                        return (String.Compare(emp1.Aplzl, emp2.Aplzl));
                    case "larnt":
                        return (String.Compare(emp1.Larnt, emp2.Larnt));
                    case "tplnr":
                        return (String.Compare(emp1.Tplnr, emp2.Tplnr));
                    case "pernr":
                        return (String.Compare(emp1.Pernr, emp2.Pernr));
                    case "dauno":
                        return (String.Compare(emp1.dauno, emp2.dauno));
                    case "arbei":
                        return (String.Compare(emp1.arbei, emp2.arbei));
                    case "arbeh":
                        return (String.Compare(emp1.arbeh, emp2.arbeh));
                    case "arbpl":
                        return (String.Compare(emp1.arbpl, emp2.arbpl));
                    case "werks":
                        return (String.Compare(emp1.werks, emp2.werks));
                    case "anzma":
                        return (String.Compare(emp1.Anzma, emp2.Anzma));
                    case "bedid":
                        return (String.Compare(emp1.Bedid, emp2.Bedid));
                    case "bedzl":
                        return (String.Compare(emp1.Bedzl, emp2.Bedzl));
                    case "canum":
                        return (String.Compare(emp1.Canum, emp2.Canum));
                    case "arbid":
                        return (String.Compare(emp1.Arbid, emp2.Arbid));
                    case "split":
                        if (String.Compare(emp1.Vornr, emp2.Vornr) == 0)
                            return (String.Compare(emp1.Split, emp2.Split));
                        return (String.Compare(emp1.Vornr, emp2.Vornr));
                }
                return returnVal;
            }
        }
        #endregion

        public virtual string GetValueByString(String field)
        {
            var retValue = "";
            if (field == null)
                return retValue;
            switch (field.ToLower())
            {
                case "fsavz":
                    return Fsavz.ToString("HHmmss");
                case "fstau":
                    return Fstau.ToString("HHmmss");
                case "aufnr":
                    return Aufnr;
                case "priok":
                    return Priok;
                case "vornr":
                    return Vornr;
                case "aufpl":
                    return Aufpl;
                case "aplzl":
                    return Aplzl;
                case "larnt":
                    return Larnt;
                case "tplnr":
                    return Tplnr;
                case "pernr":
                    return Pernr;
                case "dauno":
                    return Dauno;
                case "arbei":
                    return Arbei;
                case "arbeh":
                    return Arbeh;
                case "arbpl":
                    return Arbpl;
                case "werks":
                    return Werks;
                case "anzma":
                    return Anzma;
                case "bedid":
                    return Bedid;
                case "bedzl":
                    return Bedzl;
                case "canum":
                    return Canum;
                case "arbid":
                    return Arbid;
                case "split":
                    return Split;
            }
            return retValue;
        }

        public class BaseOrdOperationSort : IComparer<OrdOperation>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseOrdOperationSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(OrdOperation x, OrdOperation y)
            {
                switch (_compareField.ToLower())
                {
                    case "fsavz":
                        if (_desc)
                            return x.Fsavz.CompareTo(y.Fsavz);
                        return y.Fsavz.CompareTo(x.Fsavz);
                    case "fstau":
                        if (_desc)
                            return (x).Fstau.CompareTo(y.Fstau);
                        return (y).Fstau.CompareTo(x.Fstau);
                    case "aufnr":
                        if (_desc)
                            return (x).Aufnr.CompareTo(y.Aufnr);
                        return (y).Aufnr.CompareTo(x.Aufnr);
                    case "priok":
                        if (_desc)
                            return (x).Priok.CompareTo(y.Priok);
                        return (y).Priok.CompareTo(x.Priok);
                    case "vornr":
                        if (_desc)
                            return (x).Vornr.CompareTo(y.Vornr);
                        return (y).Vornr.CompareTo(x.Vornr);
                    case "aufpl":
                        if (_desc)
                            return (x).Aufpl.CompareTo(y.Aufpl);
                        return (y).Aufpl.CompareTo(x.Aufpl);
                    case "aplzl":
                        if (_desc)
                            return (x).Aplzl.CompareTo(y.Aplzl);
                        return (y).Aplzl.CompareTo(x.Aplzl);
                    case "larnt":
                        if (_desc)
                            return (x).Larnt.CompareTo(y.Larnt);
                        return (y).Larnt.CompareTo(x.Larnt);
                    case "tplnr":
                        if (_desc)
                            return (x).Tplnr.CompareTo(y.Tplnr);
                        return (y).Tplnr.CompareTo(x.Tplnr);
                    case "pernr":
                        if (_desc)
                            return (x).Pernr.CompareTo(y.Pernr);
                        return (y).Pernr.CompareTo(x.Pernr);
                    case "dauno":
                        if (_desc)
                            return (x).Dauno.CompareTo(y.Dauno);
                        return (y).Dauno.CompareTo(x.Dauno);
                    case "arbei":
                        if (_desc)
                            return (x).Arbei.CompareTo(y.Arbei);
                        return (y).Arbei.CompareTo(x.Arbei);
                    case "arbeh":
                        if (_desc)
                            return (x).Arbeh.CompareTo(y.Arbeh);
                        return (y).Arbeh.CompareTo(x.Arbeh);
                    case "arbpl":
                        if (_desc)
                            return (x).Arbpl.CompareTo(y.Arbpl);
                        return (y).Arbpl.CompareTo(x.Arbpl);
                    case "werks":
                        if (_desc)
                            return (x).Werks.CompareTo(y.Werks);
                        return (y).Werks.CompareTo(x.Werks);
                    case "anzma":
                        if (_desc)
                            return (x).Anzma.CompareTo(y.Anzma);
                        return (y).Anzma.CompareTo(x.Anzma);
                    case "bedid":
                        if (_desc)
                            return (x).Bedid.CompareTo(y.Bedid);
                        return (y).Bedid.CompareTo(x.Bedid);
                    case "bedzl":
                        if (_desc)
                            return (x).Bedzl.CompareTo(y.Bedzl);
                        return (y).Bedzl.CompareTo(x.Bedzl);
                    case "canum":
                        if (_desc)
                            return (x).Canum.CompareTo(y.Canum);
                        return (y).Canum.CompareTo(x.Canum);
                    case "arbid":
                        if (_desc)
                            return (x).Arbid.CompareTo(y.Arbid);
                        return (y).Arbid.CompareTo(x.Arbid);
                    case "split":
                        if (_desc)
                            return (x).Split.CompareTo(y.Split);
                        return (y).Split.CompareTo(x.Split);
                    default:
                        return (x).Fsavz.CompareTo(y.Fsavz);
                }
            }
        }
    }
}