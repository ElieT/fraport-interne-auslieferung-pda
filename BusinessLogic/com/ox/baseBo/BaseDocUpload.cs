﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDocUpload : BaseOxBusinessObject
    {
        protected BaseDocUpload()
        {
            _descriptor = new OxFileDescriptor();
        }

        protected string ID,
                         DOC_TYPE,
                         DOC_SIZE,
                         REF_TYPE,
                         FILE_LOCATION,
                         REF_OBJECT,
                         DESCRIPTION,
                         SPRAS,
                         FILENAME,
                         EMAIL,
                         RECORDNAME,
                         RECORDNAME_BIN,
                         ORG_FILENAME,
                         UPDFLAG;
        private DateTime timestamp;
        private DateTime synctimestamp;

        private OxFileDescriptor _descriptor;

        public OxFileDescriptor Descriptor
        {
            get { return _descriptor; }
            set { _descriptor = value; }
        }

        public String MobileKey { get; set; }

        List<Docd> DOCDS = new List<Docd>();

        public List<Docd> Docds
        {
            get { return DOCDS; }
            set { DOCDS = value; }
        }

        public string Id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string FileLocation
        {
            get { return FILE_LOCATION; }
            set
            {
                FILE_LOCATION = value;
                Descriptor.FileDirectory = value;
            }
        }
        public string DocType
        {
            get { return DOC_TYPE; }
            set
            {
                DOC_TYPE = value;
                Descriptor.FileType = value;
            }
        }

        public string DocSize
        {
            get { return DOC_SIZE; }
            set
            {
                DOC_SIZE = value;
                Descriptor.FileSize = value;
            }
        }

        public string RefType
        {
            get { return REF_TYPE; }
            set
            {
                REF_TYPE = value;
                Descriptor.FileReferenceType = value;
            }
        }

        public string RefObject
        {
            get { return REF_OBJECT; }
            set
            {
                REF_OBJECT = value;
                Descriptor.FileReferenceObjectID = value;
            }
        }

        public string Description
        {
            get { return DESCRIPTION; }
            set
            {
                DESCRIPTION = value;
                Descriptor.FileDescription = value;
            }
        }

        public string Spras
        {
            get { return SPRAS; }
            set { SPRAS = value; }
        }

        public string Filename
        {
            get { return FILENAME; }
            set
            {
                FILENAME = value;
                Descriptor.FileName = value;
            }
        }

        public string Email
        {
            get { return EMAIL; }
            set { EMAIL = value; }
        }

        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }

        public string Recordname
        {
            get { return RECORDNAME; }
            set { RECORDNAME = value; }
        }

        public DateTime Timestamp
        {
            get { return timestamp; }
            set
            {
                timestamp = value;
                Descriptor.FileCreationDate = value;
            }
        }

        public string OrgFilename
        {
            get { return ORG_FILENAME; }
            set { ORG_FILENAME = value; }
        }

        public DateTime Synctimestamp
        {
            get { return synctimestamp; }
            set { synctimestamp = value; }
        }

        public string RecordnameBin
        {
            get { return RECORDNAME_BIN; }
            set { RECORDNAME_BIN = value; }
        }

    }
}
