﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDelivery
    {
        protected String belnr,
                       segid,
                       erdat,
                       ertim,
                       mblnr,
                       mjahr,
                       mblpo,
                       ebeln,
                       ebelp,
                       usnam,
                       txzo1,
                       menge,
                       meins,
                       empf,
                       gebnr,
                       raum,
                       shortobj,
                       kostl,
                       aufnr,
                       pspnr,
                       wlief,
                       wuzeitv,
                       wuzeitb,
                       sgtxt,
                       bereich,
                       status,
                       grund,
                       gtext,
                       dienst,
                       nummer,
                       lfname,
                       telefon,
                       guid,
                       email,
                       prio,
                       trans,
                       xblock,
                       vlgort,
                       equnr,
                       sequ,
                       matnr,
                       updflag;

        protected String mobilekey;

        public DateTime Lidat { get; set; }

        // Constructor
        public BaseDelivery()
        {
            updflag = "";
        }

        // Getter and Setter
        public string Belnr
        {
            get { return belnr; }
            set { belnr = value; }
        }

        public string Segid
        {
            get { return segid; }
            set { segid = value; }
        }

        public string Mblnr
        {
            get { return mblnr; }
            set { mblnr = value; }
        }

        public string Mjahr
        {
            get { return mjahr; }
            set { mjahr = value; }
        }

        public string Mblpo
        {
            get { return mblpo; }
            set { mblpo = value; }
        }

        public string Ebeln
        {
            get { return ebeln; }
            set { ebeln = value; }
        }

        public string Ebelp
        {
            get { return ebelp; }
            set { ebelp = value; }
        }

        public string Usnam
        {
            get { return usnam; }
            set { usnam = value; }
        }

        public string Txzo1
        {
            get { return txzo1; }
            set { txzo1 = value; }
        }

        public string Menge
        {
            get { return menge; }
            set { menge = value; }
        }

        public string Meins
        {
            get { return meins; }
            set { meins = value; }
        }

        public string Empf
        {
            get { return empf; }
            set { empf = value; }
        }

        public string Gebnr
        {
            get { return gebnr; }
            set { gebnr = value; }
        }

        public string Raum
        {
            get { return raum; }
            set { raum = value; }
        }

        public string Shortobj
        {
            get { return shortobj; }
            set { shortobj = value; }
        }

        public string Erdat
        {
            get { return erdat; }
            set { erdat = value; }
        }

        public string Ertim
        {
            get { return ertim; }
            set { ertim = value; }
        }

        public string Kostl
        {
            get { return kostl; }
            set { kostl = value; }
        }

        public string Aufnr
        {
            get { return aufnr; }
            set { aufnr = value; }
        }

        public string Pspnr
        {
            get { return pspnr; }
            set { pspnr = value; }
        }

        public string Wlief
        {
            get { return wlief; }
            set { wlief = value; }
        }

        public string Wuzeitv
        {
            get { return wuzeitv; }
            set { wuzeitv = value; }
        }

        public string Wuzeitb
        {
            get { return wuzeitb; }
            set { wuzeitb = value; }
        }

        public string Sgtxt
        {
            get { return sgtxt; }
            set { sgtxt = value; }
        }

        public string Bereich
        {
            get { return bereich; }
            set { bereich = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public string Gtext
        {
            get { return gtext; }
            set { gtext = value; }
        }

        public string Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }

        public string Lfname
        {
            get { return lfname; }
            set { lfname = value; }
        }

        public string Nummer
        {
            get { return nummer; }
            set { nummer = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Trans
        {
            get { return trans; }
            set { trans = value; }
        }

        public string Vlgort
        {
            get { return vlgort; }
            set { vlgort = value; }
        }


        public string Matnr
        {
            get { return matnr; }
            set { matnr = value; }
        }


        public string Grund
        {
            get { return grund; }
            set { grund = value; }
        }

        public string Dienst
        {
            get { return dienst; }
            set { dienst = value; }
        }

        public string Guid
        {
            get { return guid; }
            set { guid = value; }
        }


        public string Prio
        {
            get { return prio; }
            set { prio = value; }
        }

        public string Xblock
        {
            get { return xblock; }
            set { xblock = value; }
        }

        public string Equnr
        {
            get { return equnr; }
            set { equnr = value; }
        }

        public string Sequ
        {
            get { return sequ; }
            set { sequ = value; }
        }

        public string Updflag
        {
            get { return updflag; }
            set { updflag = value; }
        }

        public string MobileKey
        {
            get { return mobilekey; }
            set { mobilekey = value; }
        }
    }
}
