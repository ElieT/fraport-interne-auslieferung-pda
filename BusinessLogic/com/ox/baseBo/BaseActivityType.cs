﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseActivityType
    {
        public string Kokrs { get; set; }
        public string Lstar { get; set; }
        public string Ktext { get; set; }
        public string Arbpl { get; set; }
        public string Werks { get; set; }
    }
}
