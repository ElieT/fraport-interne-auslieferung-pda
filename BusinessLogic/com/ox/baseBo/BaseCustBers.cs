﻿using System;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCustBers : OxBusinessObject
    {
        protected String _rbnr;
        protected String _qkatart;
        protected String _qcodegrp;

        public BaseCustBers()
        {
            
        }

        public string Rbnr
        {
            get { return _rbnr; }
            set { _rbnr = value; }
        }

        public string Qkatart
        {
            get { return _qkatart; }
            set { _qkatart = value; }
        }

        public string Qcodegrp
        {
            get { return _qcodegrp; }
            set { _qcodegrp = value; }
        }
    }
}
