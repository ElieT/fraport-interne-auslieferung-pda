﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCust_011
    {
        public String Scenario { get; set; }

        public String Auart { get; set; }

        public String Txt { get; set; }

        public String CreateAllowed { get; set; }

        public String Steus { get; set; }

        public String FlagRelease { get; set; }

        public String WithMeasDoc { get; set; }

        public String PartnerTransfer { get; set; }

        public String Qmart { get; set; }

        public String Ilart { get; set; }

        public String Service { get; set; }
     }
}
