﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseLongtextManager
    {
        public const string Plko = "PLKO";
        public const string Plpo = "PLPO";
        public const string Qm = "QM";
        public const string ObjRouting = "ROUTING";
        public const string ObjQss = "QSS";

        public static Longtext GetLongtext(string id, ChecklistItem clItem)
        {
            var retVal = new Longtext();
            try
            {
                var tdname = "";
                var tdObject = "";
                switch (id)
                {
                    case Plko:
                        tdname = AppConfig.Mandt + clItem.Plnty + clItem.Plnnr + clItem.Plnal + clItem.HeadZaehl;
                        tdObject = ObjRouting;
                        break;
                    case Plpo:
                        tdname = AppConfig.Mandt + clItem.Plnty + clItem.Plnnr + clItem.Plnkn + clItem.OperZaehl;
                        tdObject = ObjRouting;
                        break;
                    case Qm:
                        tdname = AppConfig.Mandt + clItem.Plnty + clItem.Plnnr + clItem.Plnkn;
                        if (string.IsNullOrEmpty(clItem.Kzeinstell))
                            tdname += " ";
                        else
                            tdname += clItem.Kzeinstell;
                        tdname += clItem.Merknr + clItem.Zaehl;
                        tdObject = ObjQss;
                        break;
                }

                retVal.TdId = id;
                retVal.TdName = tdname;
                retVal.TdObject = tdObject;

                var sqlStmt = "SELECT TDFORMAT, TDLINE FROM C_LTXT_LINE WHERE " +
                                    "TDOBJECT = @TDOBJECT AND TDNAME = @TDNAME AND TDID = @TDID AND TDSPRAS = @TDSPRAS " + 
                                    "ORDER BY COUNTER";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TDOBJECT", tdObject);
                cmd.Parameters.AddWithValue("@TDNAME", tdname);
                cmd.Parameters.AddWithValue("@TDID", id);
                cmd.Parameters.AddWithValue("@TDSPRAS", "D");

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var first = true;
                foreach (var rdr in records)
                {
                    if(first && rdr["TDFORMAT"].Equals("*"))
                    {
                        retVal.Text = rdr["TDLINE"];
                    }
                    else
                    {
                        switch(rdr["TDFORMAT"])
                        {
                            case "*":
                                retVal.Text += "\r\n" + rdr["TDLINE"];
                                break;
                            case "=":
                                retVal.Text += rdr["TDLINE"];
                                break;
                            case "":
                                retVal.Text += " " + rdr["TDLINE"];
                                break;
                            case "/":
                                retVal.Text += "\r\n\t" + rdr["TDLINE"];
                                break;
                            case "/=":
                                retVal.Text += "\t" + rdr["TDLINE"];
                                break;
                            default:
                                retVal.Text += " " + rdr["TDLINE"];
                                break;
                        }
                    }
                    first = false;
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static bool HasLongtext(string id,ChecklistItem clItem)
        {
            var retVal = false;
            try
            {
                var tdname = "";
                var tdObject = "";
                switch(id)
                {
                    case Plko:
                        tdname = AppConfig.Mandt + clItem.Plnty + clItem.Plnnr + clItem.Plnal + clItem.HeadZaehl;
                        tdObject = ObjRouting;
                        break;
                    case Plpo:
                        tdname = AppConfig.Mandt + clItem.Plnty + clItem.Plnnr + clItem.Plnkn + clItem.OperZaehl;
                        tdObject = ObjRouting;
                        break;
                    case Qm:
                        tdname = AppConfig.Mandt + clItem.Plnty + clItem.Plnnr + clItem.Plnkn;
                        if (string.IsNullOrEmpty(clItem.Kzeinstell))
                            tdname += " ";
                        else
                            tdname += clItem.Kzeinstell;
                        tdname += clItem.Merknr + clItem.Zaehl;
                        tdObject = ObjQss;
                        break;
                }

                var sqlStmt = "SELECT COUNT(*) FROM C_LTXT_HEADER WHERE " + 
                                    "TDOBJECT = @TDOBJECT AND TDNAME = @TDNAME AND TDID = @TDID AND TDSPRAS = @TDSPRAS";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TDOBJECT", tdObject);
                cmd.Parameters.AddWithValue("@TDNAME", tdname);
                cmd.Parameters.AddWithValue("@TDID", id);
                cmd.Parameters.AddWithValue("@TDSPRAS", "D");

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count == 1 && !records[0]["RetVal"].Equals("0"))
                    retVal = true;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
    }
}
