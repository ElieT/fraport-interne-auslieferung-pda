﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.Common;
using oxmc.Common.Controls;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseDeliveryManager
    {
        protected static List<DeliveryCause> _delivCauses;

        public static List<DeliveryCause> GetDeliveryCauses()
        {
            // var sqlState = "SELECT * FROM D_DELIVERYCUST ";
            var sqlState = "SELECT * FROM C_ZFRAPORT_IA_GRND "+
                    "ORDER BY GRUND ";
            _delivCauses = new List<DeliveryCause>();

            var cmd = new SQLiteCommand(sqlState);


            try
            {
                // var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (var rdr in records)
                {
                    var delivCause = new DeliveryCause();
                    delivCause.Spras = rdr["SPRAS"];
                    delivCause.Grund = rdr["GRUND"];
                    delivCause.Text = rdr["TEXT"];

                    _delivCauses.Add(delivCause);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _delivCauses;
        }

        // Returns a list of all existing buildings
        public static List<string> GetAllBuildings()
        {

            var retval = new List<string>();
            try
            {
                //var sqlState = "SELECT DISTINCT GEBNR FROM D_ZFRAPORT_IA_IAUS ";
                var sqlState = "SELECT DISTINCT GEBNR FROM D_ZFRAPORT_IA_IAUS WHERE IA_STATUS <> 'O' AND IA_STATUS <> 'A'";
                var cmd = new SQLiteCommand(sqlState);

                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (var rdr in records)
                {
                    var gebnr = (String)rdr["GEBNR"];
                    retval.Add(gebnr);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retval;
        }

        public static List<Delivery> GetDeliverysInBuilding(String gebnr, String status)
        {
            var sqlState = "SELECT * FROM D_ZFRAPORT_IA_IAUS WHERE "
                                        + "GEBNR=@GEBNR AND MOBILEKEY=@MOBILEKEY";

            var cmd = new SQLiteCommand(sqlState);

            cmd.Parameters.AddWithValue("@GEBNR", gebnr);
            cmd.Parameters.AddWithValue("@MOBILEKEY", "");

            var retval = new List<Delivery>();
            try
            {
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                DateTime date;
                foreach (var rdr in records)
                {
                    var deliv = new Delivery();
                    deliv.Belnr = rdr["BELNR"];
                    deliv.Segid = rdr["SEGID"];

                    deliv.Txzo1 = rdr["TXZ01"];
                    deliv.Menge = rdr["MENGE"];
                    deliv.Meins = rdr["MEINS"];
                    deliv.Empf = rdr["EMPF"];
                    deliv.Gebnr = rdr["GEBNR"];
                    deliv.Raum = rdr["RAUM"];
                    deliv.Shortobj = rdr["SHORT"];
                    deliv.Kostl = rdr["KOSTL"];

                    deliv.Status = rdr["IA_STATUS"];
                    deliv.Grund = rdr["GRUND"];
                    deliv.Gtext = rdr["GTEXT"];

                    deliv.IsPickUp = rdr["ISPICKUP"];

                    DateTimeHelper.getDate(rdr["LIDAT"], out date);
                    deliv.Lidat = date;

                    if (!deliv.IsPickUp.Equals("X") && !deliv.Status.Equals("O") && !deliv.Status.Equals("A"))
                    retval.Add(deliv);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retval;
        }

        public static List<Delivery> GetAllDeliverys()
        {
            var retval = new List<Delivery>();
            try
            {
                var sqlState = "SELECT * FROM D_ZFRAPORT_IA_IAUS WHERE IA_STATUS <> 'O' AND IA_STATUS <> 'A'";

                var cmd = new SQLiteCommand(sqlState);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

                DateTime date;
                foreach (var rdr in records)
                {
                    var deliv = new Delivery();
                    deliv.Belnr = rdr["BELNR"];
                    deliv.Segid = rdr["SEGID"];

                    deliv.Txzo1 = rdr["TXZ01"];
                    deliv.Menge = rdr["MENGE"];
                    deliv.Meins = rdr["MEINS"];
                    deliv.Empf = rdr["EMPF"];
                    deliv.Gebnr = rdr["GEBNR"];
                    deliv.Raum = rdr["RAUM"];
                    deliv.Shortobj = rdr["SHORT"];
                    deliv.Kostl = rdr["KOSTL"];

                    deliv.Status = rdr["IA_STATUS"];
                    deliv.Grund = rdr["GRUND"];
                    deliv.Gtext = rdr["GTEXT"];
                    deliv.MobileKey = rdr["MOBILEKEY"];

                    deliv.IsPickUp = rdr["ISPICKUP"];

                    DateTimeHelper.getDate(rdr["LIDAT"], out date);
                    deliv.Lidat = date;

                    retval.Add(deliv);
                }
                //OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retval;
        }

        public static String GetStatus(string gtext)
        {
            foreach (var cause in _delivCauses)
            {
                if (gtext.Equals(cause.Text))
                {
                    return cause.Grund;
                }
            }
            return "";
        }


        public static void SaveDelivery(Delivery deliv)
        {
            try
            {
                var sqlState = "UPDATE D_ZFRAPORT_IA_IAUS SET " +
                                  "IA_STATUS=@STATUS, " +
                                  "GRUND=@GRUND, " +
                                  "GTEXT=@GTEXT, " +
                                  "LIDAT=@LIDAT, " +
                                  "KOSTL=@KOSTL, " +
                                  "WHERE BELNR=@BELNR " +
                                  "AND SEGID=@SEGID " +
                                  "AND MANDT=@MANDT " +
                                  "AND USERID=@USERID " +
                                  "AND DIRTY <> 'X' ";
                var cmd = new SQLiteCommand(sqlState);

                cmd.Parameters.AddWithValue("@BELNR", deliv.Belnr);
                cmd.Parameters.AddWithValue("@SEGID", deliv.Segid);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@STATUS", deliv.Status);
                cmd.Parameters.AddWithValue("@GRUND", deliv.Grund);
                cmd.Parameters.AddWithValue("@GTEXT", deliv.Gtext);
                cmd.Parameters.AddWithValue("@LIDAT", DateTimeHelper.GetDateString(deliv.Lidat));
                cmd.Parameters.AddWithValue("@KOSTL", deliv.Kostl);

                //OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void SetMobileKey(List<Delivery> pdfDelivs, String hash)
        {
            foreach (var deliv in pdfDelivs)
            {
                deliv.MobileKey = hash;

                var sqlState = "UPDATE D_ZFRAPORT_IA_IAUS SET " +
                                  "MOBILEKEY=@MOBILEKEY " +
                               "WHERE BELNR=@BELNR " +
                                  "AND SEGID=@SEGID ";
                var cmd = new SQLiteCommand(sqlState);

                cmd.Parameters.AddWithValue("@BELNR", deliv.Belnr);
                cmd.Parameters.AddWithValue("@SEGID", deliv.Segid);
                cmd.Parameters.AddWithValue("@MOBILEKEY", deliv.MobileKey);

                //OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();
            }
        }

        public static List<Doch> GetDocForFile(string filename)
        {
            filename = filename.Trim('\\');
            var retVal = new List<Doch>();
            try
            {
                var sqlStmt = "SELECT * FROM D_ZFRAPORT_IA_DOCH WHERE FILENAME = @FILENAME";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@FILENAME", filename);
                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach(var rec in result)
                {
                    var doc = new Doch();

                    doc.Id = rec["ID"];
                    doc.DocType = rec["DOC_TYPE"];
                    doc.RefType = rec["REF_TYPE"];
                    doc.RefObject = rec["REF_OBJECT"];
                    doc.Description = rec["DESCRIPTION"];
                    doc.Spras = rec["SPRAS"];
                    doc.Filename = rec["FILENAME"];
                    doc.Email = rec["EMAIL"];
                    doc.Updflag = rec["UPDFLAG"];
                    doc.MobileKey = rec["MOBILEKEY"];
                }
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static void DeleteMobileKeyData(String mobileKey)
        {
            var sqlStmt = "DELETE FROM D_ZFRAPORT_IA_IAUS WHERE MOBILEKEY = @MOBILEKEY";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MOBILEKEY", mobileKey);
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_ZFRAPORT_IA_DOCD WHERE MOBILEKEY = @MOBILEKEY";
            cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MOBILEKEY", mobileKey);
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_ZFRAPORT_IA_DOCH WHERE MOBILEKEY = @MOBILEKEY";
            cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MOBILEKEY", mobileKey);
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);

        }

        public static void setPickUpFlag(Delivery deliv)
        {
            try
            {
                var sqlState = "UPDATE D_ZFRAPORT_IA_IAUS SET " +
                                  "ISPICKUP=@ISPICKUP " +
                                  "WHERE BELNR=@BELNR " +
                                  "AND SEGID=@SEGID ";
                var cmd = new SQLiteCommand(sqlState);

                cmd.Parameters.AddWithValue("@BELNR", deliv.Belnr);
                cmd.Parameters.AddWithValue("@SEGID", deliv.Segid);
                cmd.Parameters.AddWithValue("@ISPICKUP", "X");

                //OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                //OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().ExecuteDbSpecialUpdateCommand(cmd);
                //OxDbManager.GetInstance().CommitTransaction();

                
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}
