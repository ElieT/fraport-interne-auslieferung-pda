﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseTimeConf :  OxBusinessObject
    {
        protected String arbpl;
        protected String aueru;
        protected String aufnr;
        protected String ausor;
        protected String bemot;

        protected String ernam;

        protected DateTime ersda;

        protected String idaue;

        protected String idaur;

        protected DateTime iedd,
                      iedz;

        protected DateTime isdd,
                      isdz;

        protected String ismne;
        protected String ismnw;
        protected String learr;
        protected String ltxa1;

        protected String ofmne;

        protected String ofmnw;
        protected String pernr;
        protected String rmzhl;
        protected String split;
        protected String txtsp;
        protected String vornr;
        protected String werks;
        protected String UPDFLAG;
        public String MobileKey { get; set; }


        public BaseTimeConf()
        {
            arbpl = "";
            aueru = "";
            aufnr = "";
            ausor = "";
            bemot = "";
            ernam = AppConfig.UserId;
            ersda = DateTime.Now;
            idaue = "H";
            idaur = "0";
            iedd = DateTime.Now;
            iedz = DateTime.Now;
            isdd = DateTime.Now;
            isdz = DateTime.Now;
            ismne = "H";
            ismnw = "0";
            learr = "";
            ltxa1 = "";
            ofmne = "";
            ofmnw = "";
            pernr = "";
            rmzhl = "";
            split = "";
            txtsp = "";
            vornr = "";
            werks = "";
            UpdFlag = "";
        }

        public BaseTimeConf(OrdOperation operation)
        {
            Aufnr = operation.Aufnr;
            Vornr = operation.Vornr;
            Arbpl = operation.Arbpl;
            Ismne = operation.Daune;
            Idaue = operation.Daune;
            aueru = "";
            ausor = "";
            bemot = "";
            ernam = AppConfig.UserId;
            ersda = DateTime.Now;
            idaur = "0";
            iedd = DateTime.Now;
            iedz = DateTime.Now;
            isdd = DateTime.Now;
            isdz = DateTime.Now;
            ismnw = "0";
            learr = "";
            ltxa1 = "";
            ofmne = "";
            ofmnw = "";
            pernr = operation.Pernr;
            rmzhl = "";
            split = operation.Split;
            txtsp = "";
            werks = operation.Werks;
            UpdFlag = "";
            MobileKey = operation.MobileKey;
        }

        public string UpdFlag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }

        public string Arbpl
        {
            get { return arbpl; }
            set { arbpl = value; }
        }

        public string Aueru
        {
            get { return aueru; }
            set { aueru = value; }
        }

        public string Aufnr
        {
            get { return aufnr; }
            set { aufnr = value; }
        }

        public string Ausor
        {
            get { return ausor; }
            set { ausor = value; }
        }

        public string Bemot
        {
            get { return bemot; }
            set { bemot = value; }
        }

        public string Ernam
        {
            get { return ernam; }
            set { ernam = value; }
        }

        public DateTime Ersda
        {
            get { return ersda; }
            set { ersda = value; }
        }

        public string Idaue
        {
            get { return idaue; }
            set { idaue = value; }
        }

        public string Idaur
        {
            get { return idaur; }
            set { idaur = value; }
        }

        public DateTime Iedd
        {
            get { return iedd; }
            set { iedd = value; }
        }

        public DateTime Iedz
        {
            get { return iedz; }
            set { iedz = value; }
        }

        public DateTime Isdd
        {
            get { return isdd; }
            set { isdd = value; }
        }

        public DateTime Isdz
        {
            get { return isdz; }
            set { isdz = value; }
        }

        public string Ismne
        {
            get { return ismne; }
            set { ismne = value; }
        }

        public string Ismnw
        {
            get { return ismnw; }
            set { ismnw = value; }
        }

        public string Learr
        {
            get { return learr; }
            set { learr = value; }
        }

        public string Ltxa1
        {
            get { return ltxa1; }
            set { ltxa1 = value; }
        }

        public string Ofmne
        {
            get { return ofmne; }
            set { ofmne = value; }
        }

        public string Ofmnw
        {
            get { return ofmnw; }
            set { ofmnw = value; }
        }

        public string Pernr
        {
            get { return pernr; }
            set { pernr = value; }
        }

        public string Rmzhl
        {
            get { return rmzhl; }
            set { rmzhl = value; }
        }

        public string Split
        {
            get { return split; }
            set { split = value; }
        }

        public string Txtsp
        {
            get { return txtsp; }
            set { txtsp = value; }
        }

        public string Vornr
        {
            get { return vornr; }
            set { vornr = value; }
        }

        public string Werks
        {
            get { return werks; }
            set { werks = value; }
        }

        public class BaseTimeConfSort : IComparer<TimeConf>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseTimeConfSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(TimeConf x, TimeConf y)
            {
                switch (_compareField.ToLower())
                {
                    case "Aufnr":
                        if (_desc)
                            return x.Aufnr.CompareTo(y.Aufnr);
                        return y.Aufnr.CompareTo(x.Aufnr);
                    case "Vornr":
                        if (_desc)
                            return x.Vornr.CompareTo(y.Vornr);
                        return y.Vornr.CompareTo(x.Vornr);
                    case "Arbpl":
                        if (_desc)
                            return x.Arbpl.CompareTo(y.Arbpl);
                        return y.Arbpl.CompareTo(x.Arbpl);
                    case "Ismne":
                        if (_desc)
                            return x.Ismne.CompareTo(y.Ismne);
                        return y.Ismne.CompareTo(x.Ismne);
                    case "Idaue":
                        if (_desc)
                            return x.Idaue.CompareTo(y.Idaue);
                        return y.Idaue.CompareTo(x.Idaue);
                    case "aueru":
                        if (_desc)
                            return x.aueru.CompareTo(y.aueru);
                        return y.aueru.CompareTo(x.aueru);
                    case "ausor":
                        if (_desc)
                            return x.ausor.CompareTo(y.ausor);
                        return y.ausor.CompareTo(x.ausor);
                    case "bemot":
                        if (_desc)
                            return x.bemot.CompareTo(y.bemot);
                        return y.bemot.CompareTo(x.bemot);
                    case "ernam":
                        if (_desc)
                            return x.ernam.CompareTo(y.ernam);
                        return y.ernam.CompareTo(x.ernam);
                    case "ersda":
                        if (_desc)
                            return x.ersda.CompareTo(y.ersda);
                        return y.ersda.CompareTo(x.ersda);
                    case "idaur":
                        if (_desc)
                            return x.idaur.CompareTo(y.idaur);
                        return y.idaur.CompareTo(x.idaur);
                    case "iedd":
                        if (_desc)
                            return x.iedd.CompareTo(y.iedd);
                        return y.iedd.CompareTo(x.iedd);
                    case "iedz":
                        if (_desc)
                            return x.iedz.CompareTo(y.iedz);
                        return y.iedz.CompareTo(x.iedz);
                    case "isdd":
                        if (_desc)
                            return x.isdd.CompareTo(y.isdd);
                        return y.isdd.CompareTo(x.isdd);
                    case "isdz":
                        if (_desc)
                            return x.isdz.CompareTo(y.isdz);
                        return y.isdz.CompareTo(x.isdz);
                    case "ismnw":
                        if (_desc)
                            return x.ismnw.CompareTo(y.ismnw);
                        return y.ismnw.CompareTo(x.ismnw);
                    case "learr":
                        if (_desc)
                            return x.learr.CompareTo(y.learr);
                        return y.learr.CompareTo(x.learr);
                    case "ltxa1":
                        if (_desc)
                            return x.ltxa1.CompareTo(y.ltxa1);
                        return y.ltxa1.CompareTo(x.ltxa1);
                    case "ofmne":
                        if (_desc)
                            return x.ofmne.CompareTo(y.ofmne);
                        return y.ofmne.CompareTo(x.ofmne);
                    case "ofmnw":
                        if (_desc)
                            return x.ofmnw.CompareTo(y.ofmnw);
                        return y.ofmnw.CompareTo(x.ofmnw);
                    case "pernr":
                        if (_desc)
                            return x.pernr.CompareTo(y.pernr);
                        return y.pernr.CompareTo(x.pernr);
                    case "rmzhl":
                        if (_desc)
                            return x.rmzhl.CompareTo(y.rmzhl);
                        return y.rmzhl.CompareTo(x.rmzhl);
                    case "split":
                        if (_desc)
                            return x.split.CompareTo(y.split);
                        return y.split.CompareTo(x.split);
                    case "txtsp":
                        if (_desc)
                            return x.txtsp.CompareTo(y.txtsp);
                        return y.txtsp.CompareTo(x.txtsp);
                    case "werks":
                        if (_desc)
                            return x.werks.CompareTo(y.werks);
                        return y.werks.CompareTo(x.werks);
                    case "UpdFlag":
                        if (_desc)
                            return x.UpdFlag.CompareTo(y.UpdFlag);
                        return y.UpdFlag.CompareTo(x.UpdFlag);
                    default:
                        return (x).Aufnr.CompareTo(y.Aufnr);
                }
            }
        }
    }
}