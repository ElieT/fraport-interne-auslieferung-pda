﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseClassManager
    {
        public List<Class> GetClass()
        {
            var classes = new List<Class>();
            try
            {
                var sqlStmt = "SELECT * FROM C_CLASS";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var cClass = new Class();
                    cClass.Clint = rdr["CLINT"];
                    cClass.Klart = rdr["KLART"];
                    cClass.ClassNo = rdr["CLASS"];
                    cClass.Txtbz = rdr["TXTBZ"];
                    classes.Add(cClass);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return classes;
        }

        public static List<String> GetClassName(ObjClass objClass)
        {
            var classNames = new List<string>();
            try
            {
                if (objClass != null)
                {
                    var allClintsToObjNr = GetClintByObjnr(objClass);
                    if (allClintsToObjNr.Count == 0)
                    {
                        // no clints was found. this case is not possible.
                    }
                    if (allClintsToObjNr.Count == 1)
                    {
                        classNames.Add(GetClassNameByClint(allClintsToObjNr[0]));
                    }
                    if (allClintsToObjNr.Count > 1)
                    {
                        foreach (var sClint in allClintsToObjNr)
                        {
                            classNames.Add(GetClassNameByClint(sClint));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return classNames;
        }

        /*
         * Get all clints to an objnr
         */
        protected static List<String> GetClintByObjnr(ObjClass objClass)
        {

            var listClint = new List<string>();

            try
            {
                var sqlStmt = "SELECT C_OBJCLASSAS.CLINT FROM C_OBJCLASSAS WHERE C_OBJCLASSAS.OBJNR = @OBJNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@OBJNR", objClass.Objnr);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

                foreach (var record in records)
                {
                    var ergClint = record["CLINT"];
                    listClint.Add(ergClint);
                }

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return listClint;
        }

        /*
         * Method returns description text of a class 
         */
        protected static String GetClassNameByClint(String sClint)
        {
            var className = "";
            try
            {
                var sqlStmt = "SELECT C_CLASS.TXTBZ FROM C_CLASS WHERE C_CLASS.CLINT = @CLINT";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@CLINT", sClint);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

                foreach (var record in records)
                {
                    className = record["TXTBZ"];
                }

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return className;
        }

        public static Charact GetCharact(ObjClass objClass)
        {
            var charact = new Charact();
            try
            {
                var sqlStmt = "SELECT * FROM C_CLASSCHARACT WHERE " +
                                  "CLINT = @CLINT AND " +
                                  "ATINN = @ATINN AND " +
                                  "ADZHL = @ADZHL";
                var cmd = new SQLiteCommand(sqlStmt);
                var clintList = GetClintByObjnr(objClass);
                String clint;
                var records = new List<Dictionary<string, string>>();
                if (clintList.Count != 0)
                {
                    foreach (var cl in clintList)
                    {
                        clint = cl;
                        cmd.Parameters.AddWithValue("@CLINT", clint);
                        cmd.Parameters.AddWithValue("@ATINN", objClass.Atinn);
                        cmd.Parameters.AddWithValue("@ADZHL", objClass.Adzhl);
                        records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                        if (records.Count > 0)
                            break;
                    }
                }
                else
                {
                    clint = objClass.Clint;
                    cmd.Parameters.AddWithValue("@CLINT", clint);
                    cmd.Parameters.AddWithValue("@ATINN", objClass.Atinn);
                    cmd.Parameters.AddWithValue("@ADZHL", objClass.Adzhl);
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                }
                if (records.Count == 1) 
                {
                    var rdr = records[0];
                    charact.Clint = rdr["CLINT"];
                    charact.Atinn = rdr["ATINN"];
                    charact.Adzhl = rdr["ADZHL"];
                    charact.Atnam = rdr["ATNAM"];
                    charact.Atfor = rdr["ATFOR"];
                    charact.Anzst = rdr["ANZST"];
                    charact.Anzdz = rdr["ANZDZ"];
                    charact.Atkle = rdr["ATKLE"];
                    charact.Aterf = rdr["ATERF"];
                    charact.Atein = rdr["ATEIN"];
                    charact.Atint = rdr["ATINT"];
                    charact.Atson = rdr["ATSON"];
                    charact.Atinp = rdr["ATINP"];
                    charact.Atvie = rdr["ATVIE"];
                    charact.Atbez = rdr["ATBEZ"];
                }
                else
                {
                    
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return charact;
        }

        public static CharactValue GetCharactValue(ObjClass objClass, Boolean inTransaction)
        {
            var charValue = new CharactValue();
            try
            {
                var sqlStmt = "SELECT C_CHARACTVALUE.*, C_CLASSCHARACT.ATFOR FROM C_CHARACTVALUE " +
                                 "LEFT JOIN C_CLASSCHARACT ON " +
                                 "C_CLASSCHARACT.CLINT = @CLINT AND " +
                                 "C_CLASSCHARACT.ATINN = @ATINN AND " +
                                 "C_CLASSCHARACT.ADZHL = C_CHARACTVALUE.ADZHL " +
                                 "WHERE " +
                                 "C_CHARACTVALUE.ATINN = @ATINN AND " +
                                 "C_CHARACTVALUE.ATFLV = @ATFLV AND " +
                                 "C_CHARACTVALUE.ATFLB = @ATFLB AND " +
                                 "C_CHARACTVALUE.ATWRT = @ATWRT";
                var cmd = new SQLiteCommand(sqlStmt);

                var clList = GetClintByObjnr(objClass);
                if (clList != null && clList.Count > 0)
                {
                    var clint = clList[0];
                    cmd.Parameters.AddWithValue("@ATINN", objClass.Atinn);
                    cmd.Parameters.AddWithValue("@ATFLV", objClass.Atflv);
                    cmd.Parameters.AddWithValue("@ATFLB", objClass.Atflb);
                    cmd.Parameters.AddWithValue("@ATWRT", objClass.Atwrt);
                    cmd.Parameters.AddWithValue("@CLINT", clint);
                    List<Dictionary<string, string>> records;
                    if (inTransaction)
                        records = OxDbManager.GetInstance().FeedTransaction(cmd);
                    else
                        records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

                    if (records.Count == 1)
                    {
                        charValue.Atinn = records[0]["ATINN"];
                        charValue.Atzhl = records[0]["ATZHL"];
                        charValue.Adzhl = records[0]["ADZHL"];
                        charValue.Atwrt = records[0]["ATWRT"];
                        charValue.Atstd = records[0]["ATSTD"];
                        charValue.Atwtb = records[0]["ATWTB"];
                        charValue.Atflv = records[0]["ATFLV"];
                        charValue.Atflb = records[0]["ATFLB"];
                        charValue.Atcod = records[0]["ATCOD"];
                        charValue.Atawe = records[0]["ATAWE"];
                        charValue.Ataw1 = records[0]["ATAW1"];
                        charValue.Attlv = records[0]["ATTLV"];
                        charValue.Attlb = records[0]["ATTLB"];
                        charValue.Atfor = records[0]["ATFOR"];
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return charValue;
        }



        public static List<CharactValue> GetCharactValues(ObjClass objClass)
        {
            var charValues = new List<CharactValue>();
            try
            {
                var sqlStmt = "SELECT * FROM C_CHARACTVALUE " +
                                 "WHERE " +
                                 "ATINN = @ATINN";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@ATINN", objClass.Atinn);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

                foreach (var rdr in records)
                {
                    var charValue = new CharactValue();
                    charValue.Atinn = rdr["ATINN"];
                    charValue.Atzhl = rdr["ATZHL"];
                    charValue.Adzhl = rdr["ADZHL"];
                    charValue.Atwrt = rdr["ATWRT"];
                    charValue.Atstd = rdr["ATSTD"];
                    charValue.Atwtb = rdr["ATWTB"];
                    charValue.Atflv = rdr["ATFLV"];
                    charValue.Atflb = rdr["ATFLB"];
                    charValue.Atcod = rdr["ATCOD"];
                    charValue.Atawe = rdr["ATAWE"];
                    charValue.Ataw1 = rdr["ATAW1"];
                    charValue.Attlv = rdr["ATTLV"];
                    charValue.Attlb = rdr["ATTLB"];
                    charValues.Add(charValue);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return charValues;
        }


        public static void SaveCharact(ObjClass objClass)
        {
            try
            {
                String sClint;

                sClint = GetClintByObjnr(objClass)[0];

                var sqlStmt = "UPDATE D_OBJCLASS SET " +
                                 "ATWRT = @ATWRT, " +
                                 "CLINT = @CLINT, " +
                                 "ATFLV = @ATFLV " +
                                 "WHERE OBJNR = @OBJNR " +
                                 "AND ATINN = @ATINN " +
                                 "AND ATZHL = @ATZHL " +
                                 "AND MAFID = @MAFID " +
                                 "AND ADZHL = @ADZHL";

                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@ATWRT", objClass.Atwrt);
                cmd.Parameters.AddWithValue("@OBJNR", objClass.Objnr);
                cmd.Parameters.AddWithValue("@ATFLV", objClass.Atflv);
                cmd.Parameters.AddWithValue("@ATINN", objClass.Atinn);
                cmd.Parameters.AddWithValue("@CLINT", sClint);
                cmd.Parameters.AddWithValue("@ATZHL", objClass.Atzhl);
                cmd.Parameters.AddWithValue("@MAFID", objClass.Mafid);
                cmd.Parameters.AddWithValue("@ADZHL", objClass.Adzhl);

                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}