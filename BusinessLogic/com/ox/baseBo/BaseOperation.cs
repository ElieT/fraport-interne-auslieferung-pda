﻿using System;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOperation
    {
        protected String AUFNR,
                       VORNR,
                       AUFPL,
                       APLZL,
                       STEUS,
                       LTXA1,
                       LARNT,
                       PERNR,
                       DAUNO,
                       DAUNE,
                       ANZMA,
                       ARBEI,
                       ARBEH,
                       ARBPL,
                       WERKS,
                       MOBILEKEY;

        protected BaseOperation()
        {
        }
        protected BaseOperation(OrdOperation ordOper)
        {
            Anzma = "";
            Arbeh = ordOper.Arbeh;
            Arbei = ordOper.Arbei;
            Arbpl = ordOper.Arbpl;
            Aufnr = ordOper.Aufnr;
            Daune = ordOper.Daune;
            Dauno = ordOper.Dauno;
            Fsavd = ordOper.Fsavd;
            Fsavz = ordOper.Fsavz;
            Fsedd = ordOper.Fsedd;
            Fsedz = ordOper.Fsedz;
            Larnt = ordOper.Larnt;
            Ltxa1 = ordOper.Ltxa1;
            Pernr = ordOper.Pernr;
            Steus = ordOper.Steus;
            Vornr = ordOper.Vornr;
            Aufpl = ordOper.Aufpl;
            Aplzl = ordOper.Aplzl;
            Werks = ordOper.Werks;
            MobileKey = ordOper.MobileKey;
        }

        protected DateTime FSAVD,
                         FSAVZ,
                         FSEDD,
                         FSEDZ;

        public DateTime Fsedz
        {
            get { return FSEDZ; }
            set { FSEDZ = value; }
        }

        public DateTime Fsedd
        {
            get { return FSEDD; }
            set { FSEDD = value; }
        }

        public DateTime Fsavz
        {
            get { return FSAVZ; }
            set { FSAVZ = value; }
        }

        public DateTime Fsavd
        {
            get { return FSAVD; }
            set { FSAVD = value; }
        }

        public string Werks
        {
            get { return WERKS; }
            set { WERKS = value; }
        }

        public string Arbpl
        {
            get { return ARBPL; }
            set { ARBPL = value; }
        }

        public string Arbeh
        {
            get { return ARBEH; }
            set { ARBEH = value; }
        }

        public string Arbei
        {
            get { return ARBEI; }
            set { ARBEI = value; }
        }

        public string Anzma
        {
            get { return ANZMA; }
            set { ANZMA = value; }
        }

        public string Daune
        {
            get { return DAUNE; }
            set { DAUNE = value; }
        }

        public string Dauno
        {
            get { return DAUNO; }
            set { DAUNO = value; }
        }

        public string Pernr
        {
            get { return PERNR; }
            set { PERNR = value; }
        }

        public string Larnt
        {
            get { return LARNT; }
            set { LARNT = value; }
        }

        public string Ltxa1
        {
            get { return LTXA1; }
            set { LTXA1 = value; }
        }

        public string Steus
        {
            get { return STEUS; }
            set { STEUS = value; }
        }

        public string Vornr
        {
            get { return VORNR; }
            set { VORNR = value; }
        }

        public string Aufpl
        {
            get { return AUFPL; }
            set { AUFPL = value; }
        }

        public string Aplzl
        {
            get { return APLZL; }
            set { APLZL = value; }
        }

        public string Aufnr
        {
            get { return AUFNR; }
            set { AUFNR = value; }
        }

        public string MobileKey
        {
            get { return MOBILEKEY; }
            set { MOBILEKEY = value; }
        }
    }
}