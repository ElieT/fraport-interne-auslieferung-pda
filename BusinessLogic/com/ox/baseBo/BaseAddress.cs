﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.basebo
{
    public class BaseAddress
    {
        public string Updflag { get; set; }

        public string Addrnumber { get; set; }

        public string Title { get; set; }

        public string Name1 { get; set; }

        public string Name2 { get; set; }

        public string Name3 { get; set; }

        public string Name4 { get; set; }

        public string City1 { get; set; }

        public string City2 { get; set; }

        public string PostCode1 { get; set; }

        public string Street { get; set; }

        public string HouseNum1 { get; set; }

        public string Country { get; set; }

        public string Region { get; set; }

        public string TelNumber { get; set; }

        public string TelExtens { get; set; }

        public string FaxNumber { get; set; }

        public string FaxExtens { get; set; }
    }
}
