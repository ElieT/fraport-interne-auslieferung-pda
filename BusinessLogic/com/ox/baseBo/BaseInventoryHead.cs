﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseInventoryHead
    {
        public String Updflag { get; set; }
        public String Physinventory { get; set; }
        public String Fiscalyear { get; set; }
        public String Plant { get; set; }
        public String StgeLoc { get; set; }
        public String DocDate { get; set; }
        public String PlanDate { get; set; }
        public String CountStatus { get; set; }
        public String AdjustStatus { get; set; }
        public String PhysInvRef { get; set; }
        public List<InventoryItem> Items { get; set; }

    }
}
