﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BasePartner
    {
        public string Objnr { get; set; }

        public string Parvw { get; set; }

        public string Counter { get; set; }

        public string Parnr { get; set; }

        public string PartnerType { get; set; }

        public string Adrnr { get; set; }

        public string Name1 { get; set; }

        public string Name2 { get; set; }

        public string PostCode1 { get; set; }

        public string City1 { get; set; }

        public string Street { get; set; }


    }
}