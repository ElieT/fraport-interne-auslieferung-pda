﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;
using oxmc.UI.uiHelper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOrderManager
    {

        public static List<Order> GetOrdersFromDb()
        {
            return GetOrdersFromDb("default", true);
        }

        protected static int _sortDirection = 1;
        protected static List<OrdOperation> _ordOperations = new List<OrdOperation>();

        protected static List<Order> _orderList;

        public static List<Order> OrderList
        {
            get
            {
                if (_orderList == null)
                    CachingManager.UpdateOrderList();
                return _orderList;
            }
            set { _orderList = value; }
        }

        public static String GetNextOrderId(Boolean inTransaction)
        {
            var intDbKey = -1;
            var aufnr = "";
            var sqlStmt = "SELECT MAX(rowid)+1 FROM D_ORDHEAD " +
                    "WHERE UPDFLAG = 'I' OR UPDFLAG = 'X' OR UPDFLAG = '' OR UPDFLAG = '0' OR UPDFLAG = 'U'";
            var cmd = new SQLiteCommand(sqlStmt);
            List<Dictionary<string, string>> records;
            if (inTransaction)
                records = OxDbManager.GetInstance().FeedTransaction(cmd);
            else
                records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            var result = records[0]["RetVal"];
            if (result.Length > 0)
            {
                intDbKey = int.Parse(result);
            }
            else
                intDbKey = 1;
            aufnr = "MO" + intDbKey.ToString().PadLeft(10, '0');
            return aufnr;
        }

        public static String GetMobileKey(String aufnr)
        {
            var hash = OxCrypto.HashPasswordForStoringInConfigFile(
                    aufnr + AppConfig.UserId + AppConfig.Mandt +
                    DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            return hash;
        }

        public static String CreateOrder(Order order)
        {
            string aufnr = "";
            try
            {
                var intDbKey = -1;
                var dbResult = -1;

                // get db key Aufnr
                String sqlStmt;
                SQLiteCommand cmd;
                List<Dictionary<string, string>> records;
                string result;

                if (String.IsNullOrEmpty(order.Aufnr))
                {
                    order.Aufnr = GetNextOrderId(true);
                }
                aufnr = order.Aufnr;

                if (String.IsNullOrEmpty(order.MobileKey))
                {

                    var hash = GetMobileKey(order.Aufnr);
                    order.MobileKey = hash;
                }
                else
                {
                    DeleteMobileKeyData(order.MobileKey);
                }

                // Insert Data
                sqlStmt = "INSERT INTO D_ORDHEAD " +
                          "(MANDT, USERID, AUFNR, EQUNR, BAUTL, IWERK, INGPR, PM_OBJTY, " +
                          "GEWRK, KUNUM, QMNUM, SERIALNR, SERMAT, AUART, KTEXT, VAPLZ, " +
                          "WAWRK, GLTRP, GSTRP, TPLNR, PRIOK, ILART, OBJNR, UPDFLAG, MOBILEKEY, RSNUM) " +
                          "Values (@MANDT, @USERID, @AUFNR, @EQUNR, @BAUTL, @IWERK, @INGPR, @PM_OBJTY, " +
                          "@GEWRK, @KUNUM, @QMNUM, @SERIALNR, @SERMAT, @AUART, @KTEXT, @VAPLZ, " +
                          "@WAWRK, @GLTRP, @GSTRP, @TPLNR, @PRIOK, @ILART, @OBJNR, @UPDFLAG, @MOBILEKEY, @RSNUM)";
                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                cmd.Parameters.AddWithValue("@EQUNR", order.Equnr);
                cmd.Parameters.AddWithValue("@BAUTL", order.Bautl);
                cmd.Parameters.AddWithValue("@IWERK", order.Iwerk);
                cmd.Parameters.AddWithValue("@INGPR", order.Ingrp);
                cmd.Parameters.AddWithValue("@PM_OBJTY", order.PmObjty);
                cmd.Parameters.AddWithValue("@GEWRK", order.Gewrk);
                cmd.Parameters.AddWithValue("@KUNUM", order.Kunum);
                cmd.Parameters.AddWithValue("@QMNUM", order.Qmnum);
                cmd.Parameters.AddWithValue("@SERIALNR", order.Serialnr);
                cmd.Parameters.AddWithValue("@SERMAT", order.Sermat);
                cmd.Parameters.AddWithValue("@AUART", order.Auart);
                cmd.Parameters.AddWithValue("@KTEXT", order.Ktext);
                cmd.Parameters.AddWithValue("@VAPLZ", order.Vaplz);
                cmd.Parameters.AddWithValue("@WAWRK", order.Wawrk);
                cmd.Parameters.AddWithValue("@GLTRP", DateTimeHelper.GetDateString(order.Gltrp));
                cmd.Parameters.AddWithValue("@GSTRP", DateTimeHelper.GetDateString(order.Gstrp));
                cmd.Parameters.AddWithValue("@TPLNR", order.Tplnr);
                cmd.Parameters.AddWithValue("@PRIOK", order.Priok);
                cmd.Parameters.AddWithValue("@ILART", order.Ilart);
                cmd.Parameters.AddWithValue("@OBJNR", order.Objnr);
                cmd.Parameters.AddWithValue("@RSNUM", "");
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@MOBILEKEY", order.MobileKey);
                OxDbManager.GetInstance().FeedTransaction(cmd);

                sqlStmt = "SELECT MAX(VORNR) + 10 FROM D_ORDOPER WHERE (UPDFLAG = 'I' OR UPDFLAG = 'X') " +
                    "AND AUFNR = @AUFNR";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.aufnr);
                records = OxDbManager.GetInstance().FeedTransaction(cmd);
                result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 10;

                sqlStmt = "INSERT INTO D_ORDOPER " +
                          "(MANDT, USERID, AUFNR, VORNR, STEUS, LTXA1, PERNR, DAUNO, DAUNE, " +
                          "ARBEI, ARBEH, FSAVD, FSAVZ, FSEDD, FSEDZ, LARNT, ARBPL, " +
                          "WERKS, ANZMA, UPDFLAG, MOBILEKEY) " +
                          "Values (@MANDT, @USERID, @AUFNR, @VORNR, @STEUS, @LTXA1, @PERNR, @DAUNO, @DAUNE, " +
                          "@ARBEI, @ARBEH, @FSAVD, @FSAVZ, @FSEDD, @FSEDZ, @LARNT, @ARBPL, " +
                          "@WERKS, @ANZMA, @UPDFLAG, @MOBILEKEY)";

                var cmdOrdOper = new SQLiteCommand(sqlStmt);
                foreach (var operation in order.OrderOperations)
                {
                    // get db key Aufnr
                    try
                    {
                        operation.Aufnr = order.aufnr;
                        operation.MobileKey = order.MobileKey;
                        operation.Vornr = intDbKey.ToString().PadLeft(4, '0');
                        intDbKey += 10;

                        // Insert Data
                        cmdOrdOper.Parameters.Clear();
                        cmdOrdOper.CommandText = sqlStmt;
                        cmdOrdOper.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                        cmdOrdOper.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                        cmdOrdOper.Parameters.AddWithValue("@AUFNR", operation.Aufnr);
                        cmdOrdOper.Parameters.AddWithValue("@VORNR", operation.Vornr);
                        cmdOrdOper.Parameters.AddWithValue("@AUFPL", operation.Aufpl);
                        cmdOrdOper.Parameters.AddWithValue("@APLZL", operation.Aplzl);
                        cmdOrdOper.Parameters.AddWithValue("@STEUS", operation.Steus);
                        cmdOrdOper.Parameters.AddWithValue("@DAUNO", operation.Dauno);
                        cmdOrdOper.Parameters.AddWithValue("@LTXA1", operation.Ltxa1);
                        cmdOrdOper.Parameters.AddWithValue("@PERNR", operation.Pernr);
                        cmdOrdOper.Parameters.AddWithValue("@DAUNO", operation.Dauno);
                        cmdOrdOper.Parameters.AddWithValue("@DAUNE", operation.Daune);
                        cmdOrdOper.Parameters.AddWithValue("@ARBEI", operation.Arbei);
                        cmdOrdOper.Parameters.AddWithValue("@ARBEH", operation.Arbeh);
                        cmdOrdOper.Parameters.AddWithValue("@FSAVD", DateTimeHelper.GetDateString(operation.Fsavd));
                        cmdOrdOper.Parameters.AddWithValue("@FSAVZ", DateTimeHelper.GetTimeString(operation.Fsavz));
                        cmdOrdOper.Parameters.AddWithValue("@FSEDD", DateTimeHelper.GetDateString(operation.Fsedd));
                        cmdOrdOper.Parameters.AddWithValue("@FSEDZ", DateTimeHelper.GetTimeString(operation.Fsedz));
                        cmdOrdOper.Parameters.AddWithValue("@LARNT", operation.Larnt);
                        cmdOrdOper.Parameters.AddWithValue("@ARBPL", operation.Arbpl);
                        cmdOrdOper.Parameters.AddWithValue("@WERKS", operation.Werks);
                        cmdOrdOper.Parameters.AddWithValue("@ANZMA", operation.Anzma);
                        cmdOrdOper.Parameters.AddWithValue("@UPDFLAG", "I");
                        cmdOrdOper.Parameters.AddWithValue("@MOBILEKEY", operation.MobileKey);
                        OxDbManager.GetInstance().FeedTransaction(cmdOrdOper);
                    }
                    catch (Exception ex)
                    {
                        FileManager.LogException(ex);
                    }
                }
                OxDbManager.GetInstance().CommitTransaction();
                if (!String.IsNullOrEmpty(order.Qmnum))
                    NotifManager.SetAufnr(order.Aufnr, order.Qmnum);
                CachingManager.AddOrder(order);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return aufnr;
        }

        public static int DeleteOrder(Order order)
        {
            var retVal = 0;
            try
            {
                DeleteMobileKeyData(order.MobileKey);
                NotifManager.DeleteMobileKeyData(order.MobileKey);
                CachingManager.UpdateOrderList();
                CachingManager.UpdateNotifList();
                CachingManager.UpdateTimeConfList();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static void UpdateOrder(Order order)
        {
            try
            {
                // Insert Data
                var sqlStmt = "UPDATE D_ORDHEAD SET " +
                                  "EQUNR = @EQUNR, " +
                                  "BAUTL = @BAUTL, " +
                                  "IWERK = @IWERK, " +
                                  "INGPR = @INGPR, " +
                                  "PM_OBJTY = @PM_OBJTY, " +
                                  "GEWRK = @GEWRK, " +
                                  "KUNUM = @KUNUM, " +
                                  "QMNUM = @QMNUM, " +
                                  "SERIALNR = @SERIALNR, " +
                                  "SERMAT = @SERMAT, " +
                                  "KTEXT = @KTEXT, " +
                                  "VAPLZ = @VAPLZ, " +
                                  "WAWRK = @WAWRK, " +
                                  "GLTRP = @GLTRP, " +
                                  "GSTRP = @GSTRP, " +
                                  "TPLNR = @TPLNR, " +
                                  "PRIOK = @PRIOK " +
                              "WHERE AUFNR = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                cmd.Parameters.AddWithValue("@EQUNR", order.Equnr);
                cmd.Parameters.AddWithValue("@BAUTL", order.Bautl);
                cmd.Parameters.AddWithValue("@IWERK", order.Iwerk);
                cmd.Parameters.AddWithValue("@INGPR", order.Ingrp);
                cmd.Parameters.AddWithValue("@PM_OBJTY", order.PmObjty);
                cmd.Parameters.AddWithValue("@GEWRK", order.Gewrk);
                cmd.Parameters.AddWithValue("@KUNUM", order.Kunum);
                cmd.Parameters.AddWithValue("@QMNUM", order.Qmnum);
                cmd.Parameters.AddWithValue("@SERIALNR", order.Serialnr);
                cmd.Parameters.AddWithValue("@SERMAT", order.Sermat);
                cmd.Parameters.AddWithValue("@AUART", order.Auart);
                cmd.Parameters.AddWithValue("@KTEXT", order.Ktext);
                cmd.Parameters.AddWithValue("@VAPLZ", order.Vaplz);
                cmd.Parameters.AddWithValue("@WAWRK", order.Wawrk);
                cmd.Parameters.AddWithValue("@GLTRP", DateTimeHelper.GetDateString(order.Gltrp));
                cmd.Parameters.AddWithValue("@GSTRP", DateTimeHelper.GetDateString(order.Gstrp));
                cmd.Parameters.AddWithValue("@TPLNR", order.Tplnr);
                cmd.Parameters.AddWithValue("@PRIOK", order.Priok);
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                CachingManager.AddOrder(order);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        /// <summary>
        /// Returns the updateflag to the provided order object
        /// </summary>
        /// <param name="order">Order object for which the updateflag should be returned</param>
        /// <returns>String representation of the updateflag. 
        /// \nIf the returnvalue is empty, no data associated with the provided order object could be found in the database 
        /// (e.g. because the order has not been saved yet.</returns>
        public static String GetOrderUpdflag(Order order)
        {
            try
            {
                String sqlStmt = "SELECT " +
                                    "UPDFLAG " +
                                 "FROM D_ORDHEAD " +
                                    "WHERE " +
                                 "AUFNR = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().FeedTransaction(cmd);
                if (records.Count != 1)
                {
                    return "";
                }
                return records[0]["UPDFLAG"];
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return "";
        }

        protected static List<Order> GetOrdersFromDb(String sortBy, Boolean desc)
        {
            var orders = new List<Order>();
            try
            {
                var sqlStmt = "SELECT " +
                                 "D_ORDHEAD.AUFNR, D_ORDHEAD.AUART, D_ORDHEAD.KTEXT, D_ORDHEAD.EQUNR, D_ORDHEAD.MOBILEKEY, " +
                                 "D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMNUM, D_NOTIHEAD.QMTXT, D_ORDHEAD.IWERK, D_ORDHEAD.SERMAT, " +
                                 "D_ORDHEAD.TPLNR, D_ORDHEAD.GLTRP, D_ORDHEAD.GSTRP, D_ORDHEAD.TPLNR, D_ORDHEAD.ILART, D_ORDHEAD.OBJNR, D_ORDHEAD.RSNUM, " +
                                 "D_EQUI.EQKTX, D_EQUI.EQFNR AS EQUI_EQFNR, D_FLOC.EQFNR AS FLOC_EQFNR, D_EQUI.STORT AS ESTORT, " +
                                 "D_FLOC.STORT AS FSTORT, D_EQUI.MSGRP AS EMSGRP, D_FLOC.MSGRP AS FMSGRP, D_EQUI.TIDNR " +
                                 "FROM " +
                                 "D_ORDHEAD " +
                                 "LEFT OUTER JOIN " +
                                 "D_FLOC ON D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                                 "LEFT OUTER JOIN " +
                                 "D_EQUI ON D_ORDHEAD.EQUNR = D_EQUI.EQUI " +
                                 "LEFT OUTER JOIN " +
                                 "D_NOTIHEAD ON D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                                 "ORDER BY " +
                                 "D_ORDHEAD.GSTRP";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);

                sqlStmt = "SELECT * FROM D_OBJECT_STATUS WHERE OBJNR = @OBJNR";
                cmd = new SQLiteCommand(sqlStmt);
                var operations = OrderManager.GetOrderOperationsFromDb(true);

                foreach (var rdr in records)
                {
                    var _order = new Order();
                    _order.aufnr = rdr["AUFNR"];
                    _order.auart = rdr["AUART"];
                    _order.tplnr = rdr["TPLNR"];
                    _order.ktext = rdr["KTEXT"];
                    _order.equnr = rdr["EQUNR"];
                    _order.MobileKey = rdr["MOBILEKEY"];
                    DateTimeHelper.getDate(rdr["GLTRP"], out _order.gltrp);
                    DateTimeHelper.getDate(rdr["GSTRP"], out _order.gstrp);

                    _order.pltxt = rdr["PLTXT"];
                    _order.eqktx = rdr["EQKTX"];
                    _order.FlocEqfnr = rdr["FLOC_EQFNR"];
                    _order.EquiEqfnr = rdr["EQUI_EQFNR"];
                    if (!String.IsNullOrEmpty(rdr["ESTORT"]))
                        _order.stort = rdr["ESTORT"];
                    else
                        _order.stort = rdr["FSTORT"];
                    _order.raumnr = "";
                    if (!String.IsNullOrEmpty(rdr["EMSGRP"]))
                        _order.raumnr = rdr["EMSGRP"];
                    else
                        _order.raumnr = rdr["FMSGRP"];
                    _order.Qmnum = rdr["QMNUM"];
                    _order.qmtxt = rdr["QMTXT"];
                    _order.Tidnr = rdr["TIDNR"];
                    _order.Ilart = rdr["ILART"];
                    _order.Objnr = rdr["OBJNR"];
                    _order.Iwerk = rdr["IWERK"];
                    _order.Sermat = rdr["SERMAT"];
                    _order.Rsnum = rdr["RSNUM"];

                    if (operations.ContainsKey(_order.Aufnr))
                    {
                        operations[_order.Aufnr].Sort(new OrdOperation.OrdOperationSort("fsavz", true));
                        _order.OrderOperations = operations[_order.Aufnr];
                        if (operations[_order.Aufnr][0].Fstau != null && operations[_order.Aufnr][0].Fstau != DateTime.MinValue)
                            _order.DateSort = operations[_order.Aufnr][0].Fstau;
                        else
                            _order.DateSort = operations[_order.Aufnr][0].Fsavz;
                    }
                    else
                    {
                        _order.DateSort = _order.Gstrp;
                    }
                    _order.ObjectStatus = new List<ObjectStatus>();


                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@OBJNR", _order.Objnr);
                    cmd.CommandText = sqlStmt;

                    var status_records = OxDbManager.GetInstance().FeedTransaction(cmd);
                    foreach (var status_rdr in status_records)
                    {
                        var _objectStatus = new ObjectStatus(_order);
                        _objectStatus.Chgnr = (String)status_rdr["CHGNR"];
                        _objectStatus.StatusInt = (String)status_rdr["STATUS_INT"];
                        _objectStatus.Inact = (String)status_rdr["INACT"];
                        _objectStatus.Changed = (String)status_rdr["CHANGED"];
                        _objectStatus.UserStatusCode = (String)status_rdr["USER_STATUS_CODE"];
                        _objectStatus.UserStatusDesc = (String)status_rdr["USER_STATUS_DESC"];
                        _objectStatus.Updflag = (String)status_rdr["UPDFLAG"];
                        //_objectStatus.Qmnum = (String)status_rdr["QMNUM"];
                        _objectStatus.MobileKey = (String)status_rdr["MOBILEKEY"];
                        _order.ObjectStatus.Add(_objectStatus);
                        //_order.Status = _objectStatus.UserStatusCode;
                    }
                    if (status_records.Count == 0)
                    {
                        var _objectStatus = new ObjectStatus(_order);
                        _objectStatus.Chgnr = "000";
                        _objectStatus.StatusInt = "0";
                        _objectStatus.Inact = "";
                        _objectStatus.Changed = "";
                        _objectStatus.UserStatusCode = "FREI";
                        _objectStatus.UserStatusDesc = "Frei";
                        _objectStatus.Updflag = "I";
                        _objectStatus.Aufnr = _order.Aufnr;
                        //_objectStatus.Qmnum = "";
                        _objectStatus.MobileKey = _order.MobileKey;
                        _order.ObjectStatus.Add(_objectStatus);
                    }


                    orders.Add(_order);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return orders;
        }

        public static void ReplaceOrder(Order order)
        {
            try
            {
                var sqlStmt = "SELECT " +
                                 "D_ORDHEAD.AUFNR, D_ORDHEAD.AUART, D_ORDHEAD.KTEXT, D_ORDHEAD.EQUNR, D_ORDHEAD.MOBILEKEY, " +
                                 "D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMNUM, D_NOTIHEAD.QMTXT, D_ORDHEAD.IWERK, D_ORDHEAD.SERMAT, " +
                                 "D_ORDHEAD.TPLNR, D_ORDHEAD.GLTRP, D_ORDHEAD.GSTRP, D_ORDHEAD.TPLNR, D_ORDHEAD.ILART, D_ORDHEAD.OBJNR, D_ORDHEAD.RSNUM, " +
                                 "D_EQUI.EQKTX, D_EQUI.EQFNR AS EQUI_EQFNR, D_FLOC.EQFNR AS FLOC_EQFNR, D_EQUI.STORT AS ESTORT, " +
                                 "D_FLOC.STORT AS FSTORT, D_EQUI.MSGRP AS EMSGRP, D_FLOC.MSGRP AS FMSGRP, D_EQUI.TIDNR " +
                                 "FROM " +
                                 "D_ORDHEAD " +
                                 "LEFT OUTER JOIN " +
                                 "D_FLOC ON D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                                 "LEFT OUTER JOIN " +
                                 "D_EQUI ON D_ORDHEAD.EQUNR = D_EQUI.EQUI " +
                                 "LEFT OUTER JOIN " +
                                 "D_NOTIHEAD ON D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                                 "WHERE D_ORDHEAD.AUFNR = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                if (records.Count == 1)
                {
                    sqlStmt = "SELECT * FROM D_OBJECT_STATUS WHERE OBJNR = @OBJNR";
                    cmd = new SQLiteCommand(sqlStmt);
                    var operations = GetOrderOperationsFromDb(true);

                    foreach (var rdr in records)
                    {
                        order.aufnr = rdr["AUFNR"];
                        order.auart = rdr["AUART"];
                        order.tplnr = rdr["TPLNR"];
                        order.ktext = rdr["KTEXT"];
                        order.equnr = rdr["EQUNR"];
                        order.MobileKey = rdr["MOBILEKEY"];
                        DateTimeHelper.getDate(rdr["GLTRP"], out order.gltrp);
                        DateTimeHelper.getDate(rdr["GSTRP"], out order.gstrp);

                        order.pltxt = rdr["PLTXT"];
                        order.eqktx = rdr["EQKTX"];
                        order.FlocEqfnr = rdr["FLOC_EQFNR"];
                        order.EquiEqfnr = rdr["EQUI_EQFNR"];
                        if (!String.IsNullOrEmpty(rdr["ESTORT"]))
                            order.stort = rdr["ESTORT"];
                        else
                            order.stort = rdr["FSTORT"];
                        order.raumnr = "";
                        if (!String.IsNullOrEmpty(rdr["EMSGRP"]))
                            order.raumnr = rdr["EMSGRP"];
                        else
                            order.raumnr = rdr["FMSGRP"];
                        order.Qmnum = rdr["QMNUM"];
                        order.qmtxt = rdr["QMTXT"];
                        order.Tidnr = rdr["TIDNR"];
                        order.Ilart = rdr["ILART"];
                        order.Objnr = rdr["OBJNR"];
                        order.Iwerk = rdr["IWERK"];
                        order.Sermat = rdr["SERMAT"];
                        order.Rsnum = rdr["RSNUM"];

                        if (operations.ContainsKey(order.Aufnr))
                        {
                            operations[order.Aufnr].Sort(new OrdOperation.OrdOperationSort("fsavz", true));
                            order.OrderOperations = operations[order.Aufnr];
                            if (operations[order.Aufnr][0].Fstau != null &&
                                operations[order.Aufnr][0].Fstau != DateTime.MinValue)
                                order.DateSort = operations[order.Aufnr][0].Fstau;
                            else
                                order.DateSort = operations[order.Aufnr][0].Fsavz;
                        }
                        else
                        {
                            order.DateSort = order.Gstrp;
                        }
                        order.ObjectStatus = new List<ObjectStatus>();


                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@OBJNR", order.Objnr);
                        cmd.CommandText = sqlStmt;

                        var status_records = OxDbManager.GetInstance().FeedTransaction(cmd);
                        foreach (var status_rdr in status_records)
                        {
                            var objectStatus = new ObjectStatus(order);
                            objectStatus.Chgnr = status_rdr["CHGNR"];
                            objectStatus.StatusInt = status_rdr["STATUS_INT"];
                            objectStatus.Inact = status_rdr["INACT"];
                            objectStatus.Changed = status_rdr["CHANGED"];
                            objectStatus.UserStatusCode = status_rdr["USER_STATUS_CODE"];
                            objectStatus.UserStatusDesc = status_rdr["USER_STATUS_DESC"];
                            objectStatus.Updflag = status_rdr["UPDFLAG"];
                            //_objectStatus.Qmnum = status_rdr["QMNUM"];
                            objectStatus.MobileKey = status_rdr["MOBILEKEY"];
                            order.ObjectStatus.Add(objectStatus);
                            //_order.Status = _objectStatus.UserStatusCode;
                        }
                        if (status_records.Count == 0)
                        {
                            var objectStatus = new ObjectStatus(order);
                            objectStatus.Chgnr = "000";
                            objectStatus.StatusInt = "0";
                            objectStatus.Inact = "";
                            objectStatus.Changed = "";
                            objectStatus.UserStatusCode = "FREI";
                            objectStatus.UserStatusDesc = "Frei";
                            objectStatus.Updflag = "I";
                            objectStatus.Aufnr = order.Aufnr;
                            //_objectStatus.Qmnum = "";
                            objectStatus.MobileKey = order.MobileKey;
                            order.ObjectStatus.Add(objectStatus);
                        }
                    }
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static Dictionary<String, List<OrdOperation>> GetOrderOperationsFromDb(Boolean useSplits)
        {
            var retVal = new Dictionary<String, List<OrdOperation>>();
            var cmd = new SQLiteCommand();
            try
            {
                var records = new List<Dictionary<string, string>>();
                var sqlStmt = "";
                //If scenario for customizing of usage of splits and/or operations is implemented, replace with flag-checking
                if (useSplits)
                {
                    sqlStmt =
                        "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                        "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                        "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, D_NOTIHEAD.UPDFLAG, " +
                        "D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                        "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, " +
                        "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                        "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY, " +
                        "D_ORDSPLIT.BEDID, D_ORDSPLIT.BEDZL, D_ORDSPLIT.CANUM, D_ORDSPLIT.AUFPL, D_ORDSPLIT.APLZL, " +
                        "D_ORDSPLIT.FSTAD, D_ORDSPLIT.FSTAU, D_ORDSPLIT.FENDD, D_ORDSPLIT.FENDU, D_ORDSPLIT.PERNR, " +
                        "D_ORDSPLIT.SPLIT, D_ORDSPLIT.ARBID, " +
                        "D_ORDSPLIT.DAUNO AS SDAUNO, D_ORDSPLIT.DAUNE AS SDAUNE, D_ORDSPLIT.ARBEI AS SARBEI, " +
                        "D_ORDSPLIT.ARBEH AS SARBEH " +
                        "FROM D_ORDSPLIT " +
                        "LEFT OUTER JOIN D_ORDOPER ON " +
                        "D_ORDSPLIT.AUFPL = D_ORDOPER.AUFPL AND D_ORDSPLIT.APLZL = D_ORDOPER.APLZL " +
                        "LEFT OUTER JOIN D_ORDHEAD ON " +
                        "D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                        "LEFT OUTER JOIN D_FLOC ON " +
                        "D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                        "LEFT OUTER JOIN D_NOTIHEAD ON " +
                        "D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                        "ORDER BY D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDSPLIT.SPLIT";

                    cmd = new SQLiteCommand(sqlStmt);
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                sqlStmt =
                    "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                    "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                    "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, " +
                    "D_ORDOPER.PERNR, D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                    "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, D_ORDOPER.UPDFLAG, " +
                    "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                    "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY " +
                    "FROM D_ORDOPER " +
                    "LEFT OUTER JOIN " +
                    "D_ORDHEAD ON D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                    "LEFT OUTER JOIN " +
                    "D_FLOC ON D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                    "LEFT OUTER JOIN " +
                    "D_NOTIHEAD ON D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                    "WHERE " +
                    "(D_ORDOPER.AUFPL = '' OR D_ORDOPER.AUFPL IS NULL OR D_ORDOPER.AUFPL NOT IN " +
                    "(SELECT D_ORDSPLIT.AUFPL FROM D_ORDSPLIT WHERE D_ORDSPLIT.APLZL = D_ORDOPER.APLZL AND D_ORDOPER.AUFPL = D_ORDSPLIT.AUFPL)) " +
                    "ORDER BY D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ";
                cmd = new SQLiteCommand(sqlStmt);
                records.AddRange(OxDbManager.GetInstance().FeedTransaction(cmd));
                foreach (var rdr in records)
                {
                    var ordOperation = new OrdOperation();
                    ordOperation.Aufnr = rdr["AUFNR"];
                    ordOperation.Vornr = rdr["VORNR"];
                    ordOperation.Aufpl = rdr["AUFPL"];
                    ordOperation.Aplzl = rdr["APLZL"];
                    ordOperation.Ktext = rdr["KTEXT"];
                    ordOperation.Ltxa1 = rdr["LTXA1"];
                    ordOperation.Priok = rdr["PRIOK"];
                    ordOperation.MobileKey = rdr["MOBILEKEY"];
                    ordOperation.Larnt = rdr["LARNT"];
                    ordOperation.Pltxt = rdr["PLTXT"];
                    ordOperation.Qmtxt = rdr["QMTXT"];
                    ordOperation.Tplnr = rdr["TPLNR"];
                    DateTime test;
                    DateTimeHelper.getDate(rdr["FSAVD"], out test);
                    ordOperation.Fsavd = test;
                    DateTimeHelper.getTime(rdr["FSAVD"], rdr["FSAVZ"], out test);
                    ordOperation.Fsavz = test;
                    ordOperation.Pernr = rdr["PERNR"];
                    ordOperation.Dauno = rdr["DAUNO"];
                    ordOperation.Daune = rdr["DAUNE"];
                    ordOperation.Arbei = rdr["ARBEI"];
                    ordOperation.Arbeh = rdr["ARBEH"];
                    DateTimeHelper.getDate(rdr["FSEDD"], out test);
                    ordOperation.Fsedd = test;
                    DateTimeHelper.getTime(rdr["FSEDD"], rdr["FSEDZ"], out test);
                    ordOperation.Fsedz = test;
                    ordOperation.Arbpl = rdr["ARBPL"];
                    ordOperation.Werks = rdr["WERKS"];
                    ordOperation.Updflag = rdr["UPDFLAG"];
                    DateTimeHelper.getDate(rdr["NTANF"], out test);
                    ordOperation.Ntanf = test;
                    DateTimeHelper.getTime(rdr["NTANF"], rdr["NTANZ"], out test);
                    ordOperation.Ntanz = test;
                    DateTimeHelper.getDate(rdr["NTEND"], out test);
                    ordOperation.Ntend = test;
                    DateTimeHelper.getTime(rdr["NTEND"], rdr["NTENZ"], out test);
                    ordOperation.Ntenz = test;
                    ordOperation.Isdd = rdr["ISDD"];
                    ordOperation.Isdz = rdr["ISDZ"];
                    ordOperation.Iedd = rdr["IEDD"];
                    ordOperation.Iedz = rdr["IEDZ"];

                    if (rdr.ContainsKey("BEDID"))
                    {
                        ordOperation.Bedid = rdr["BEDID"];
                        ordOperation.Bedzl = rdr["BEDZL"];
                        ordOperation.Canum = rdr["CANUM"];
                        ordOperation.Aufpl = rdr["AUFPL"];
                        ordOperation.Aplzl = rdr["APLZL"];
                        ordOperation.Arbid = rdr["ARBID"];
                        DateTimeHelper.getDate(rdr["FSTAD"], out test);
                        ordOperation.Fstad = test;
                        DateTimeHelper.getTime(rdr["FSTAD"], rdr["FSTAU"], out test);
                        ordOperation.Fstau = test;
                        DateTimeHelper.getDate(rdr["FENDD"], out test);
                        ordOperation.Fendd = test;
                        DateTimeHelper.getTime(rdr["FENDD"], rdr["FENDU"], out test);
                        ordOperation.Fendu = test;
                        ordOperation.Split = rdr["SPLIT"];
                        ordOperation.SDaune = rdr["SDAUNE"];
                        ordOperation.SDauno = rdr["SDAUNO"];
                        ordOperation.SArbei = rdr["SARBEI"];
                        ordOperation.SArbeh = rdr["SARBEH"];
                    }

                    if (!retVal.ContainsKey(ordOperation.Aufnr))
                        retVal.Add(ordOperation.Aufnr, new List<OrdOperation>());

                    retVal[ordOperation.Aufnr].Add(ordOperation);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        /// <summary>
        /// Gets a List of OrderOperations to the provided aufnr from the DB. Does not read splits (used for OrderComponents)
        /// </summary>
        /// <param name="aufnr"></param>
        /// <returns></returns>
        public static List<OrdOperation> GetOrderOperationsFromDb(string aufnr)
        {
            var retVal = new List<OrdOperation>();
            var cmd = new SQLiteCommand();
            try
            {
                var sqlStmt =
                    "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                    "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                    "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, " +
                    "D_ORDOPER.PERNR, D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                    "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, D_ORDOPER.UPDFLAG, " +
                    "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                    "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY " +
                    "FROM D_ORDOPER " +
                    "LEFT OUTER JOIN " +
                    "D_ORDHEAD ON D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                    "LEFT OUTER JOIN " +
                    "D_FLOC ON D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                    "LEFT OUTER JOIN " +
                    "D_NOTIHEAD ON D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                    "WHERE " +
                    "D_ORDOPER.AUFNR = @AUFNR " +
                    "ORDER BY D_ORDOPER.VORNR";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", aufnr);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (var rdr in records)
                {
                    var ordOperation = new OrdOperation();
                    ordOperation.Aufnr = rdr["AUFNR"];
                    ordOperation.Vornr = rdr["VORNR"];
                    ordOperation.Aufpl = rdr["AUFPL"];
                    ordOperation.Aplzl = rdr["APLZL"];
                    ordOperation.Ktext = rdr["KTEXT"];
                    ordOperation.Ltxa1 = rdr["LTXA1"];
                    ordOperation.Priok = rdr["PRIOK"];
                    ordOperation.MobileKey = rdr["MOBILEKEY"];
                    ordOperation.Larnt = rdr["LARNT"];
                    ordOperation.Pltxt = rdr["PLTXT"];
                    ordOperation.Qmtxt = rdr["QMTXT"];
                    ordOperation.Tplnr = rdr["TPLNR"];
                    DateTime test;
                    DateTimeHelper.getDate(rdr["FSAVD"], out test);
                    ordOperation.Fsavd = test;
                    DateTimeHelper.getTime(rdr["FSAVD"], rdr["FSAVZ"], out test);
                    ordOperation.Fsavz = test;
                    ordOperation.Pernr = rdr["PERNR"];
                    ordOperation.Dauno = rdr["DAUNO"];
                    ordOperation.Daune = rdr["DAUNE"];
                    ordOperation.Arbei = rdr["ARBEI"];
                    ordOperation.Arbeh = rdr["ARBEH"];
                    DateTimeHelper.getDate(rdr["FSEDD"], out test);
                    ordOperation.Fsedd = test;
                    DateTimeHelper.getTime(rdr["FSEDD"], rdr["FSEDZ"], out test);
                    ordOperation.Fsedz = test;
                    ordOperation.Arbpl = rdr["ARBPL"];
                    ordOperation.Werks = rdr["WERKS"];
                    ordOperation.Updflag = rdr["UPDFLAG"];
                    DateTimeHelper.getDate(rdr["NTANF"], out test);
                    ordOperation.Ntanf = test;
                    DateTimeHelper.getTime(rdr["NTANF"], rdr["NTANZ"], out test);
                    ordOperation.Ntanz = test;
                    DateTimeHelper.getDate(rdr["NTEND"], out test);
                    ordOperation.Ntend = test;
                    DateTimeHelper.getTime(rdr["NTEND"], rdr["NTENZ"], out test);
                    ordOperation.Ntenz = test;
                    ordOperation.Isdd = rdr["ISDD"];
                    ordOperation.Isdz = rdr["ISDZ"];
                    ordOperation.Iedd = rdr["IEDD"];
                    ordOperation.Iedz = rdr["IEDZ"];
                    retVal.Add(ordOperation);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }


        /// <summary>
        /// Gets a single OrderOperation to the provided aufnr and vornr from the DB. Does not read splits (used for OrderComponents)
        /// </summary>
        /// <param name="aufnr"></param>
        /// <returns></returns>
        public static OrdOperation GetOrderOperationFromDb(string aufnr, string vornr)
        {
            var ordOperation = new OrdOperation();
            var cmd = new SQLiteCommand();
            try
            {
                var sqlStmt =
                    "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                    "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                    "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, " +
                    "D_ORDOPER.PERNR, D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                    "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, D_ORDOPER.UPDFLAG, " +
                    "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                    "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY " +
                    "FROM D_ORDOPER " +
                    "LEFT OUTER JOIN " +
                    "D_ORDHEAD ON D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                    "LEFT OUTER JOIN " +
                    "D_FLOC ON D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                    "LEFT OUTER JOIN " +
                    "D_NOTIHEAD ON D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                    "WHERE " +
                    "D_ORDOPER.AUFNR = @AUFNR AND D_ORDOPER.VORNR = @VORNR";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", aufnr);
                cmd.Parameters.AddWithValue("@VORNR", vornr);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (var rdr in records)
                {
                    ordOperation.Aufnr = rdr["AUFNR"];
                    ordOperation.Vornr = rdr["VORNR"];
                    ordOperation.Aufpl = rdr["AUFPL"];
                    ordOperation.Aplzl = rdr["APLZL"];
                    ordOperation.Ktext = rdr["KTEXT"];
                    ordOperation.Ltxa1 = rdr["LTXA1"];
                    ordOperation.Priok = rdr["PRIOK"];
                    ordOperation.MobileKey = rdr["MOBILEKEY"];
                    ordOperation.Larnt = rdr["LARNT"];
                    ordOperation.Pltxt = rdr["PLTXT"];
                    ordOperation.Qmtxt = rdr["QMTXT"];
                    ordOperation.Tplnr = rdr["TPLNR"];
                    DateTime test;
                    DateTimeHelper.getDate(rdr["FSAVD"], out test);
                    ordOperation.Fsavd = test;
                    DateTimeHelper.getTime(rdr["FSAVD"], rdr["FSAVZ"], out test);
                    ordOperation.Fsavz = test;
                    ordOperation.Pernr = rdr["PERNR"];
                    ordOperation.Dauno = rdr["DAUNO"];
                    ordOperation.Daune = rdr["DAUNE"];
                    ordOperation.Arbei = rdr["ARBEI"];
                    ordOperation.Arbeh = rdr["ARBEH"];
                    DateTimeHelper.getDate(rdr["FSEDD"], out test);
                    ordOperation.Fsedd = test;
                    DateTimeHelper.getTime(rdr["FSEDD"], rdr["FSEDZ"], out test);
                    ordOperation.Fsedz = test;
                    ordOperation.Arbpl = rdr["ARBPL"];
                    ordOperation.Werks = rdr["WERKS"];
                    ordOperation.Updflag = rdr["UPDFLAG"];
                    DateTimeHelper.getDate(rdr["NTANF"], out test);
                    ordOperation.Ntanf = test;
                    DateTimeHelper.getTime(rdr["NTANF"], rdr["NTANZ"], out test);
                    ordOperation.Ntanz = test;
                    DateTimeHelper.getDate(rdr["NTEND"], out test);
                    ordOperation.Ntend = test;
                    DateTimeHelper.getTime(rdr["NTEND"], rdr["NTENZ"], out test);
                    ordOperation.Ntenz = test;
                    ordOperation.Isdd = rdr["ISDD"];
                    ordOperation.Isdz = rdr["ISDZ"];
                    ordOperation.Iedd = rdr["IEDD"];
                    ordOperation.Iedz = rdr["IEDZ"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return ordOperation;
        }

        public static OrdOperation GetOrdOperation(String aufnr, String vornr, String split)
        {
            OrdOperation ordOperation = null;
            try
            {
                ordOperation = OrderList.Find(
                    delegate(Order order)
                    {
                        return order.Aufnr == aufnr;
                    }).OrderOperations.Find(
                            delegate(OrdOperation ordOper)
                            {
                                return (ordOper.Vornr == vornr && ordOper.Split == split);
                            });
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return ordOperation;
        }

        public static OrdOperation GetOrdOperation(String aufnr, String vornr)
        {
            OrdOperation ordOperation = null;
            try
            {
                ordOperation = OrderList.Find(
                    delegate(Order order)
                    {
                        return order.Aufnr == aufnr;
                    }).OrderOperations.Find(
                            delegate(OrdOperation ordOper)
                            {
                                return (ordOper.Vornr == vornr);
                            });
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return ordOperation;
        }

        public static Order GetOrder(string aufnr)
        {
            Order ord = null;
            try
            {
                var ordList = (from n in OrderList
                               where n.Aufnr == aufnr
                               select n).ToList();
                if (ordList.Count > 0)
                    ord = ordList[0];
                else ord = null;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return ord;
        }
        
        public static Order GetOrder(OrdOperation ordOperation)
        {
            var order = new Order();
            order.Aufnr = ordOperation.Aufnr;
            return order;
        }

        public static void UpdateOrdOperation(OrdOperation ordOperation)
        {
            try
            {
                // Insert Data
                var sqlStmt = "UPDATE D_ORDOPER SET " +
                                  "LTXA1 = @LTAXL, " +
                                  "LARNT = @LARNT, " +
                                  "FSAVD = @FSAVD, " +
                                  "FSAVZ = @FSAVZ, " +
                                  "FSEDD = @FSEDD, " +
                                  "FSEDZ = @FSEDZ, " +
                                  "PERNR = @PERNR, " +
                                  "DAUNO = @DAUNO, " +
                                  "DAUNE = @DAUNE, " +
                                  "ARBEI = @ARBEI, " +
                                  "ARBEH = @ARBEH, " +
                                  "ARBPL = @ARBPL, " +
                                  "WERKS = @WERKS, " +
                                  "UPDFLAG = @UPDFLAG, " +
                                  "ANZMA = @ANZMA, " +
                                  "NTANF = @NTANF, " +
                                  "NTANZ = @NTANZ, " +
                                  "NTEND = @NTEND, " +
                                  "NTENZ = @NTENZ, " +
                                  "ISDD = @ISDD, " +
                                  "ISDZ = @ISDZ, " +
                                  "IEDD = @IEDD, " +
                                  "IEDZ = @IEDZ, " +
                                  "MOBILEKEY = @MOBILEKEY " +
                              "WHERE " +
                                "AUFNR = @AUFNR AND VORNR = @VORNR ";

                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", ordOperation.Aufnr);
                cmd.Parameters.AddWithValue("@VORNR", ordOperation.Vornr);
                cmd.Parameters.AddWithValue("@LTAXL", ordOperation.Ltxa1);
                cmd.Parameters.AddWithValue("@LARNT", ordOperation.Larnt);

                cmd.Parameters.AddWithValue("@FSAVD", "" + DateTimeHelper.GetDateString(ordOperation.Fsavd));
                cmd.Parameters.AddWithValue("@FSAVZ", "" + DateTimeHelper.GetTimeString(ordOperation.Fsavz));
                cmd.Parameters.AddWithValue("@FSEDD", "" + DateTimeHelper.GetDateString(ordOperation.Fsedd));
                cmd.Parameters.AddWithValue("@FSEDZ", "" + DateTimeHelper.GetTimeString(ordOperation.Fsedz));
                cmd.Parameters.AddWithValue("@PERNR", ordOperation.Pernr);
                cmd.Parameters.AddWithValue("@DAUNO", ordOperation.Dauno);
                cmd.Parameters.AddWithValue("@DAUNE", ordOperation.Daune);
                cmd.Parameters.AddWithValue("@ARBEI", ordOperation.Arbei);
                cmd.Parameters.AddWithValue("@ARBEH", ordOperation.Arbeh);
                cmd.Parameters.AddWithValue("@ARBPL", ordOperation.Arbpl);
                cmd.Parameters.AddWithValue("@WERKS", ordOperation.Werks);
                cmd.Parameters.AddWithValue("@MOBILEKEY", ordOperation.MobileKey);
                if (ordOperation.Updflag.Equals("I"))
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                else
                    cmd.Parameters.AddWithValue("@UPDFLAG", "U");
                if (ordOperation.Anzma == null)
                    cmd.Parameters.AddWithValue("@ANZMA", "");
                else
                    cmd.Parameters.AddWithValue("@ANZMA", ordOperation.Anzma);
                cmd.Parameters.AddWithValue("@NTANF", DateTimeHelper.GetDateString(ordOperation.Ntanf));
                cmd.Parameters.AddWithValue("@NTANZ", DateTimeHelper.GetTimeString(ordOperation.Ntanz));
                cmd.Parameters.AddWithValue("@NTEND", DateTimeHelper.GetDateString(ordOperation.Ntend));
                cmd.Parameters.AddWithValue("@NTENZ", DateTimeHelper.GetTimeString(ordOperation.Ntenz));
                cmd.Parameters.AddWithValue("@ISDD", ordOperation.Isdd);
                cmd.Parameters.AddWithValue("@ISDZ", ordOperation.Isdz);
                cmd.Parameters.AddWithValue("@IEDD", ordOperation.Iedd);
                cmd.Parameters.AddWithValue("@IEDZ", ordOperation.Iedz);
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

        }

        /// <summary>
        /// Returns a List of all operations
        /// </summary>
        /// <param name="date">Earliest date for the OrdOperations. If no date is provided, all Operations will be selected</param>
        /// <returns></returns>
        public static List<OrdOperation> GetOrdOperations(String date)
        {
            var operationList = new List<OrdOperation>();
            try
            {
                var opList = (from order in OrderList
                              select order.OrderOperations);
                if (String.IsNullOrEmpty(date))
                {
                    foreach (var orderOps in opList)
                        operationList.AddRange(orderOps);
                }
                else
                {
                    foreach (var orderOps in opList)
                    {
                        var ops = (from op in orderOps
                                   where DateTimeHelper.getDate(op.Isdd) >= DateTimeHelper.getDate(date)
                                   select op);
                        operationList.AddRange(ops);
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return operationList;
        }

        public static void DeleteComponent(OrdComponents comp)
        {
            try
            {
                String sqlStmt;
                if (!String.IsNullOrEmpty(comp.Updflag) && !comp.Updflag.Equals("I"))
                    sqlStmt = "UPDATE D_ORDCOMP SET UPDFLAG = 'D' WHERE AUFNR = @AUFNR AND RSNUM = @RSNUM AND RSPOS = @RSPOS";
                else
                    sqlStmt = "DELETE FROM D_ORDCOMP WHERE AUFNR = @AUFNR AND RSNUM = @RSNUM AND RSPOS = @RSPOS";

                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", comp.Aufnr);
                cmd.Parameters.AddWithValue("@RSNUM", comp.Rsnum);
                cmd.Parameters.AddWithValue("@RSPOS", comp.Rspos);
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void SaveOrderComponent(OrdComponents comp)
        {
            try
            {
                if (!String.IsNullOrEmpty(comp.Updflag) && comp.Updflag.Equals("D"))
                    return;
                var sqlStmt = "INSERT OR REPLACE INTO D_ORDCOMP " +
                              "(MANDT, USERID, AUFNR, RSNUM, RSPOS, XLOEK, KZEAR, MATNR, WERKS, LGORT, BDMNG, " +
                              "MEINS, ENMNG, ERFMG, ERFME, BWART, SGTXT, VORNR, POSTP, VMENG, MOBILEKEY, UPDFLAG) VALUES " +
                              "(@MANDT, @USERID, @AUFNR, @RSNUM, @RSPOS, @XLOEK, @KZEAR, @MATNR, @WERKS, @LGORT, @BDMNG, " +
                              "@MEINS, @ENMNG, @ERFMG, @ERFME, @BWART, @SGTXT, @VORNR, @POSTP, @VMENG, @MOBILEKEY, @UPDFLAG)";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@AUFNR", comp.Aufnr);
                cmd.Parameters.AddWithValue("@RSNUM", comp.Rsnum);
                if (String.IsNullOrEmpty(comp.Rspos))
                {
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                    cmd.Parameters.AddWithValue("@RSPOS", CreateNewComponentId(comp.Aufnr, comp.Rsnum));
                }
                else
                {
                    if (!comp.Updflag.Equals("I"))
                    {
                        cmd.Parameters.AddWithValue("@UPDFLAG", "U");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@UPDFLAG", comp.Updflag);
                    }
                    cmd.Parameters.AddWithValue("@RSPOS", comp.Rspos);
                }
                cmd.Parameters.AddWithValue("@XLOEK", comp.Xloek);
                cmd.Parameters.AddWithValue("@KZEAR", comp.Kzear);
                cmd.Parameters.AddWithValue("@MATNR", comp.Matnr);
                cmd.Parameters.AddWithValue("@WERKS", comp.Werks);
                cmd.Parameters.AddWithValue("@LGORT", comp.Lgort);
                cmd.Parameters.AddWithValue("@BDMNG", comp.Bdmng);
                cmd.Parameters.AddWithValue("@MEINS", comp.Meins);
                cmd.Parameters.AddWithValue("@ENMNG", comp.Enmng);
                cmd.Parameters.AddWithValue("@ERFMG", comp.Erfmg);
                cmd.Parameters.AddWithValue("@ERFME", comp.Erfme);
                cmd.Parameters.AddWithValue("@BWART", comp.Bwart);
                cmd.Parameters.AddWithValue("@SGTXT", comp.Sgtxt);
                cmd.Parameters.AddWithValue("@VORNR", comp.Vornr);
                cmd.Parameters.AddWithValue("@POSTP", comp.Postp);
                cmd.Parameters.AddWithValue("@VMENG", comp.Vmeng);
                cmd.Parameters.AddWithValue("@MOBILEKEY", comp.MobileKey);
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private static string CreateNewComponentId(String aufnr, String rsnum)
        {
            var intDbKeyNotif = -1;
            try
            {
                var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_ORDCOMP WHERE AUFNR = @AUFNR AND RSNUM = @RSNUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", aufnr);
                cmd.Parameters.AddWithValue("@RSNUM", rsnum);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyNotif = int.Parse(result);
                else
                    intDbKeyNotif = 1;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return "" + intDbKeyNotif;
        }

        /// <summary>
        /// returns all components associated with the defined order
        /// </summary>
        /// <param name="order">The order to which the Components should be retrieved</param>
        /// <returns></returns>
        public static List<OrdComponents> GetOrdComponents(Order order)
        {
            var retList = new List<OrdComponents>();
            try
            {
                String sqlStmt = "SELECT * FROM D_ORDCOMP WHERE AUFNR = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var ordComp = new OrdComponents();
                    ordComp.Aufnr = (String)rdr["AUFNR"];
                    ordComp.Rsnum = (String)rdr["RSNUM"];
                    ordComp.Rspos = (String)rdr["RSPOS"];
                    ordComp.Xloek = (String)rdr["XLOEK"];
                    ordComp.Kzear = (String)rdr["KZEAR"];
                    ordComp.Matnr = (String)rdr["MATNR"];
                    ordComp.Werks = (String)rdr["WERKS"];
                    ordComp.Lgort = (String)rdr["LGORT"];
                    ordComp.Bdmng = (String)rdr["BDMNG"];
                    ordComp.Meins = (String)rdr["MEINS"];
                    ordComp.Enmng = (String)rdr["ENMNG"];
                    ordComp.Erfmg = (String)rdr["ERFMG"];
                    ordComp.Erfme = (String)rdr["ERFME"];
                    ordComp.Bwart = (String)rdr["BWART"];
                    ordComp.Sgtxt = (String)rdr["SGTXT"];
                    ordComp.Vornr = (String)rdr["VORNR"];
                    ordComp.MobileKey = (String)rdr["MOBILEKEY"];
                    retList.Add(ordComp);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }

        /// <summary>
        /// Returns a String showing how many components are associated with the provided order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static String GetOrdComponentsStatus(Order order)
        {
            var compStatus = "";
            try
            {
                var sqlStmt = "SELECT COUNT(*) FROM D_ORDCOMP WHERE AUFNR = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if(records.Count == 1)
                {
                    compStatus = records[0]["RetVal"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return compStatus;
        }

		/// <summary>
        /// Retrievs a specific component to an order
        /// </summary>
        /// <param name="order">Order to which the component is to be retrieved</param>
        /// <param name="rsnum">reservation the component is associated with</param>
        /// <param name="rspos">position in the reservation the component is associated with</param>
        /// <returns></returns>
        public static OrdComponents GetOrderComponent(Order order, String rsnum, String rspos)
        {
            var retList = new List<OrdComponents>();
            try
            {
                String sqlStmt = "SELECT * FROM D_ORDCOMP WHERE AUFNR = @AUFNR AND RSNUM = @RSNUM AND RSPOS = @RSPOS";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                cmd.Parameters.AddWithValue("@RSNUM", rsnum);
                cmd.Parameters.AddWithValue("@RSPOS", rspos);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count != 1)
                {
                    throw new Exception("Wrong number of Results");
                }
                foreach (var rdr in records)
                {
                    var ordComp = new OrdComponents();
                    ordComp.Aufnr = (String)rdr["AUFNR"];
                    ordComp.Rsnum = (String)rdr["RSNUM"];
                    ordComp.Rspos = (String)rdr["RSPOS"];
                    ordComp.Xloek = (String)rdr["XLOEK"];
                    ordComp.Kzear = (String)rdr["KZEAR"];
                    ordComp.Matnr = (String)rdr["MATNR"];
                    ordComp.Werks = (String)rdr["WERKS"];
                    ordComp.Lgort = (String)rdr["LGORT"];
                    ordComp.Bdmng = (String)rdr["BDMNG"];
                    ordComp.Meins = (String)rdr["MEINS"];
                    ordComp.Enmng = (String)rdr["ENMNG"];
                    ordComp.Erfmg = (String)rdr["ERFMG"];
                    ordComp.Erfme = (String)rdr["ERFME"];
                    ordComp.Bwart = (String)rdr["BWART"];
                    ordComp.Sgtxt = (String)rdr["SGTXT"];
                    ordComp.Vornr = (String)rdr["VORNR"];
                    ordComp.MobileKey = (String)rdr["MOBILEKEY"];
                    retList.Add(ordComp);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList[0];
        }

        public static void DeleteMobileKeyData(String mobileKey)
        {
            var sqlStmt = "DELETE FROM D_ORDHEAD WHERE MOBILEKEY = @MOBILEKEY";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MOBILEKEY", mobileKey);
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_ORDOPER WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_ORDCOMP WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_ORDPARTNER WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_DOCH WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_DOCD WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            sqlStmt = "DELETE FROM D_MATCONF WHERE MOBILEKEY = @MOBILEKEY";
            cmd.CommandText = sqlStmt;
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            //sqlStmt = "DELETE FROM D_TIMECONF WHERE MOBILEKEY = @MOBILEKEY";
            //cmd.CommandText = sqlStmt;
            //OxDbManager.GetInstance().FeedSyncTransaction(cmd);
        }

        public static Partner GetPartnerToOrder(Order order)
        {
            Partner partner = null;
            try
            {
                var sqlStmt = "SELECT * FROM D_PARTNER WHERE OBJNR = @OBJNR AND PARVW = @PARVW";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@OBJNR", "OR" + order.Aufnr);
                cmd.Parameters.AddWithValue("@PARVW", "AG");
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if(records.Count == 1)
                {
                    partner = new Partner();
                    partner.Objnr = records[0]["OBJNR"];
                    partner.Parvw = records[0]["PARVW"];
                    partner.Counter = records[0]["COUNTER"];
                    partner.Parnr = records[0]["PARNR"];
                    partner.PartnerType = records[0]["PARTNER_TYPE"];
                    partner.Adrnr = records[0]["ADRNR"];
                    partner.Name1 = records[0]["NAME1"];
                    partner.Name2 = records[0]["NAME2"];
                    partner.PostCode1 = records[0]["POST_CODE1"];
                    partner.City1 = records[0]["CITY1"];
                    partner.Street = records[0]["STREET"];
                }
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return partner;
        }

        public static bool SaveOrderHead(Order order)
        {
            var retVal = false;
            try
            {
                if (String.IsNullOrEmpty(order.Auart) || String.IsNullOrEmpty(order.Ilart))
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Sie müssen zunächst die Auftragsart/IH-Leistungsart auswählen.", null, true, 5000);
                else
                {
                    order.Iwerk = Cust_001.GetInstance().OrderPlanPlant;
                    order.Gewrk = "";
                    if (String.IsNullOrEmpty(order.Vaplz))
                        order.Vaplz = Cust_001.GetInstance().OrderWorkcenter;
                    if (String.IsNullOrEmpty(order.Wawrk))
                        order.Wawrk = Cust_001.GetInstance().OrderPlantWrkc;
                    order.Ingrp = Cust_001.GetInstance().OrderPlangroup;
                    var aufnr = CreateOrder(order);
                    if (String.IsNullOrEmpty(aufnr))
                        OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Der Auftrag konnte nicht gespeichert werden.", null, true, 5000);
                    else
                    {
                        OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
                                                                   "Der Auftrag " + aufnr + " wurde gespeichert.", null,
                                                                   true, 5000);
                        retVal = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static string[] GetOrderTimeConfStatus(Order order)
        {
            var retVal = new string[3];
            try
            {
                var timeConfs = TimeConfManager.GetTimeConfs(order);
                retVal[0] = "Vorgänge: " + order.OrderOperations.Count;

                var numEndRueck = 0;
                var foundTimeConf = new TimeConf();

                foreach (var t in timeConfs)
                {
                    if (foundTimeConf.Vornr.Equals(t.Vornr) && foundTimeConf.Split.Equals(t.Split))
                        break;
                    if (!t.Aueru.Equals(""))
                    {
                        foundTimeConf = t;
                        numEndRueck++;
                    }
                }
                retVal[1] = "Endrückgemeldet: " + numEndRueck;
                retVal[2] = "Zeitrückmeldungen: " + timeConfs.Count;
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static StatusLocal GetOrderStatus(Order order, Boolean inTransaction)
        {
            try
            {
                var allExist = true;
                var someExist = false;
                foreach (var oper in order.OrderOperations)
                {
                    var tConfs = TimeConfManager.GetTimeConfStatus(oper);
                    if (tConfs == null || tConfs.Count() == 0)
                    {
                        allExist = false;
                        continue;
                    }
                    someExist = true;
                    var tConf = (from t in tConfs
                                 where !string.IsNullOrEmpty(t.Rmzhl) && t.Aueru.Equals("X")
                                 select t).ToList();
                    if (tConf.Count == 0)
                        allExist = false;
                }
                if (allExist)
                    return new StatusLocal(StatusLocal.Color.CGreen, StatusLocal.Shape.CCircle);
                if (!someExist)
                    return new StatusLocal(StatusLocal.Color.CWhite, StatusLocal.Shape.CCircle);
                return new StatusLocal(StatusLocal.Color.CYellow, StatusLocal.Shape.CTriangle);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return new StatusLocal(StatusLocal.Color.CWhite, StatusLocal.Shape.CCircle);
        }

        public static void UpdateQmnum(Order order)
        {
            try
            {
                var sqlStmt = "UPDATE D_ORDHEAD SET QMNUM = @QMNUM, UPDFLAG = @UPDFLAG WHERE AUFNR = @AUFNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", order.Qmnum);
                cmd.Parameters.AddWithValue("@AUFNR", order.Aufnr);
                if (OrderManager.GetOrderUpdflag(order).Equals("I"))
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                else
                    cmd.Parameters.AddWithValue("@UPDFLAG", "U");
                OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

        }
    }
}