﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseMatConf : OxBusinessObject
    {
        protected String AUFNR,
                       MOVE_TYPE,
                       SPEC_STOCK,
                       STCK_TYPE,
                       PLANT,
                       STORLOC,
                       CUSTOMER,
                       MATNR,
                       QUANTITY,
                       UNIT,
                       RSNUM,
                       RSPOS,
                       KZEAR,
                       BATCH,
                       EQUNR,
                       MBLNR,
                       MJAHR,
                       UPFLAG;

        public String MobileKey { get; set; }

        public string UpdFlag
        {
            get { return UPFLAG; }
            set { UPFLAG = value; }
        }

        public BaseMatConf()
        {
            AUFNR = "";
            MOVE_TYPE = "261";
            RSNUM = "";
            RSPOS = "";
            SPEC_STOCK = "";
            STCK_TYPE = "";
            EQUNR = "";
            CUSTOMER = "";
            PLANT = "";
            QUANTITY = "";
            STORLOC = "";
            MATNR = "";
            BATCH = "";
            Unit = "";
            ISDD = DateTime.Now;
            MBLNR = "";
            MJAHR = "";
            UPFLAG = "";
        }

        public BaseMatConf(Order order)
        {
            AUFNR = order.Aufnr;
            MOVE_TYPE = "261";
            RSNUM = "";
            RSPOS = "";
            SPEC_STOCK = "";
            STCK_TYPE = "";
            EQUNR = "";
            CUSTOMER = "";
            PLANT = "";
            QUANTITY = "0";
            STORLOC = "";
            MATNR = "";
            BATCH = "";
            Unit = "ST";
            ISDD = DateTime.Now;
            MBLNR = "";
            MJAHR = "";
            UPFLAG = "";
            MobileKey = order.MobileKey;
        }

        public string Aufnr
        {
            get { return AUFNR; }
            set { AUFNR = value; }
        }

        public string MoveType
        {
            get { return MOVE_TYPE; }
            set { MOVE_TYPE = value; }
        }

        public string SpecStock
        {
            get { return SPEC_STOCK; }
            set { SPEC_STOCK = value; }
        }

        public string StckType
        {
            get { return STCK_TYPE; }
            set { STCK_TYPE = value; }
        }

        public string Plant
        {
            get { return PLANT; }
            set { PLANT = value; }
        }

        public string Storloc
        {
            get { return STORLOC; }
            set { STORLOC = value; }
        }

        public string Customer
        {
            get { return CUSTOMER; }
            set { CUSTOMER = value; }
        }

        public string Matnr
        {
            get { return MATNR; }
            set { MATNR = value; }
        }

        public string Quantity
        {
            get { return QUANTITY; }
            set { QUANTITY = value; }
        }

        public string Unit
        {
            get { return UNIT; }
            set { UNIT = value; }
        }

        public string Rsnum
        {
            get { return RSNUM; }
            set { RSNUM = value; }
        }

        public string Rspos
        {
            get { return RSPOS; }
            set { RSPOS = value; }
        }

        public string Kzear
        {
            get { return KZEAR; }
            set { KZEAR = value; }
        }

        public string Batch
        {
            get { return BATCH; }
            set { BATCH = value; }
        }

        public string Equnr
        {
            get { return EQUNR; }
            set { EQUNR = value; }
        }

        public string Mblnr
        {
            get { return MBLNR; }
            set { MBLNR = value; }
        }

        public string Mjahr
        {
            get { return MJAHR; }
            set { MJAHR = value; }
        }

        public DateTime Isdd
        {
            get { return ISDD; }
            set { ISDD = value; }
        }

        protected DateTime ISDD;

        public class BaseMatConfSort : IComparer<MatConf>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseMatConfSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(MatConf x, MatConf y)
            {
                switch (_compareField.ToLower())
                {
                    case "AUFNR":
                        if (_desc)
                            return x.AUFNR.CompareTo(y.AUFNR);
                        return y.AUFNR.CompareTo(x.AUFNR);
                    case "MOVE_TYPE":
                        if (_desc)
                            return x.MOVE_TYPE.CompareTo(y.MOVE_TYPE);
                        return y.MOVE_TYPE.CompareTo(x.MOVE_TYPE);
                    case "RSNUM":
                        if (_desc)
                            return x.RSNUM.CompareTo(y.RSNUM);
                        return y.RSNUM.CompareTo(x.RSNUM);
                    case "RSPOS":
                        if (_desc)
                            return x.RSPOS.CompareTo(y.RSPOS);
                        return y.RSPOS.CompareTo(x.RSPOS);
                    case "SPEC_STOCK":
                        if (_desc)
                            return x.SPEC_STOCK.CompareTo(y.SPEC_STOCK);
                        return y.SPEC_STOCK.CompareTo(x.SPEC_STOCK);
                    case "STCK_TYPE":
                        if (_desc)
                            return x.STCK_TYPE.CompareTo(y.STCK_TYPE);
                        return y.STCK_TYPE.CompareTo(x.STCK_TYPE);
                    case "EQUNR":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "CUSTOMER":
                        if (_desc)
                            return x.CUSTOMER.CompareTo(y.CUSTOMER);
                        return y.CUSTOMER.CompareTo(x.CUSTOMER);
                    case "PLANT":
                        if (_desc)
                            return x.PLANT.CompareTo(y.PLANT);
                        return y.PLANT.CompareTo(x.PLANT);
                    case "QUANTITY":
                        if (_desc)
                            return x.QUANTITY.CompareTo(y.QUANTITY);
                        return y.QUANTITY.CompareTo(x.QUANTITY);
                    case "STORLOC":
                        if (_desc)
                            return x.STORLOC.CompareTo(y.STORLOC);
                        return y.STORLOC.CompareTo(x.STORLOC);
                    case "MATNR":
                        if (_desc)
                            return x.MATNR.CompareTo(y.MATNR);
                        return y.MATNR.CompareTo(x.MATNR);
                    case "BATCH":
                        if (_desc)
                            return x.BATCH.CompareTo(y.BATCH);
                        return y.BATCH.CompareTo(x.BATCH);
                    case "Unit":
                        if (_desc)
                            return x.Unit.CompareTo(y.Unit);
                        return y.Unit.CompareTo(x.Unit);
                    case "ISDD":
                        if (_desc)
                            return x.ISDD.CompareTo(y.ISDD);
                        return y.ISDD.CompareTo(x.ISDD);
                    case "MBLNR":
                        if (_desc)
                            return x.MBLNR.CompareTo(y.MBLNR);
                        return y.MBLNR.CompareTo(x.MBLNR);
                    case "MJAHR":
                        if (_desc)
                            return x.MJAHR.CompareTo(y.MJAHR);
                        return y.MJAHR.CompareTo(x.MJAHR);
                    case "UPFLAG":
                        if (_desc)
                            return x.UPFLAG.CompareTo(y.UPFLAG);
                        return y.UPFLAG.CompareTo(x.UPFLAG);
                    default:
                        return (x).AUFNR.CompareTo(y.AUFNR);
                }
            }
        }
    }
}
