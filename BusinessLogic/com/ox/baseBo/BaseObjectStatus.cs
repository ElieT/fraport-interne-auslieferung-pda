﻿using System;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseObjectStatus
    {
        protected string OBJNR, CHGNR, STATUS_INT, INACT, CHANGED, USER_STATUS_CODE, USER_STATUS_DESC, UPDFLAG, AUFNR, QMNUM;
        protected string MOBILEKEY;

        public BaseObjectStatus(Order order)
        {
            Objnr = "OR" + order.Aufnr;
            Chgnr = "";
            StatusInt = "";
            Inact = "";
            Changed = "X";
            UserStatusCode = "";
            UserStatusDesc = "";
            Aufnr = order.Aufnr;
            Qmnum = "";
            Updflag = "I";
            MobileKey = order.MobileKey;
        }
        public BaseObjectStatus(Order order, String status_int, String user_status_code, String user_status_desc)
        {
            Objnr = "OR" + order.Aufnr;
            Chgnr = "";
            StatusInt = status_int;
            Inact = "";
            Changed = "X";
            UserStatusCode = user_status_code;
            UserStatusDesc = user_status_desc;
            Aufnr = order.Aufnr;
            Qmnum = "";
            Updflag = "I";
            MobileKey = order.MobileKey;
        }

        public BaseObjectStatus(Notif notif, String status_int, String user_status_code, String user_status_desc)
        {
            Objnr = "QM" + notif.Qmnum;
            Chgnr = "";
            StatusInt = status_int;
            Inact = "";
            Changed = "X";
            UserStatusCode = user_status_code;
            UserStatusDesc = user_status_desc;
            Aufnr = "";
            Qmnum = notif.Qmnum;
            Updflag = "I";
            MobileKey = notif.MobileKey;
        }


        public string Qmnum
        {
            get { return QMNUM; }
            set { QMNUM = value; }
        }

        public string Aufnr
        {
            get { return AUFNR; }
            set { AUFNR = value; }
        }

        public string Updflag
        {
            get { return UPDFLAG; }
            set { UPDFLAG = value; }
        }

        public string Objnr
        {
            get { return OBJNR; }
            set { OBJNR = value; }
        }

        public string Chgnr
        {
            get { return CHGNR; }
            set { CHGNR = value; }
        }

        public string StatusInt
        {
            get { return STATUS_INT; }
            set { STATUS_INT = value; }
        }

        public string Inact
        {
            get { return INACT; }
            set { INACT = value; }
        }

        public string Changed
        {
            get { return CHANGED; }
            set { CHANGED = value; }
        }

        public string UserStatusCode
        {
            get { return USER_STATUS_CODE; }
            set { USER_STATUS_CODE = value; }
        }

        public string UserStatusDesc
        {
            get { return USER_STATUS_DESC; }
            set { USER_STATUS_DESC = value; }
        }

        public string MobileKey
        {
            get { return MOBILEKEY; }
            set { MOBILEKEY = value; }
        }
    }
}
