﻿using System;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCust_001
    {
        private static Cust_001 instance;

        protected BaseCust_001() 
        {
        }

        public String OrderScenario { get; set; }

        public String OrderWorkcenter { get; set; }

        public String OrderPlantWrkc { get; set; }

        public String OrderPernr { get; set; }

        public String OrderPlangroup { get; set; }

        public String OrderPlanPlant { get; set; }

        public String OrderParRole { get; set; }

        public String OrderPartnerNo { get; set; }

        public String NotifScenario { get; set; }

        public String NotifWorkcenter { get; set; }

        public String NotifPlantWrkc { get; set; }

        public String NotifPlanGroup { get; set; }

        public String NotifPlanPlant { get; set; }

        public String NotifPernr { get; set; }

        public String NotifParRole { get; set; }

        public String NotifPartnerNo { get; set; }

        public String VariTpl { get; set; }

        public String ChangeTpl { get; set; }

        public String VariEqui { get; set; }

        public String ChangeEqui { get; set; }

        public String VariClass { get; set; }

        public String Sprache { get; set; }

        public String LartDefault { get; set; }

        public String Intern { get; set; }
    }
}
