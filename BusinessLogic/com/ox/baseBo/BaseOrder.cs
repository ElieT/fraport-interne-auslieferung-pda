﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.bo;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOrder : IComparable
    {
        public String auart;
        public String aufnr;

        public String bautl;
        public String eqktx;
        public String equnr;
        public String rsnum;

        public String gewrk;

        public DateTime gltrp,
                        gstrp;

        protected string TIDNR;

        public DateTime _dateSort;

        public virtual string Tidnr
        {
            get { return TIDNR; }
            set { TIDNR = value; }
        }

        public virtual string Rsnum
        {
            get { return rsnum; }
            set { rsnum = value; }
        }

        public String ingrp;
        public String iwerk;
        public String ktext;

        public String kunum;
        public String pltxt;

        public static String _sortBy;
        public static Boolean _desc;

        protected String objnr;
        protected String mobileKey;

        protected String _ilart;

        protected List<ObjectStatus> objectStatus;

        public virtual List<ObjectStatus> ObjectStatus
        {
            get { return objectStatus; }
            set { objectStatus = value; }
        }

        public virtual string FlocEqfnr
        {
            get { return floc_eqfnr; }
            set { floc_eqfnr = value; }
        }

        public virtual string EquiEqfnr
        {
            get { return equi_eqfnr; }
            set { equi_eqfnr = value; }
        }

        public virtual string Stort
        {
            get { return stort; }
            set { stort = value; }
        }

        public virtual string Raumnr
        {
            get { return raumnr; }
            set { raumnr = value; }
        }

        public String floc_eqfnr;
        public String equi_eqfnr;
        public String stort;
        public String raumnr;
        public String pm_objty;
        public String priok;

        public String qmnum;
        public String qmtxt;

        public String serialnr,
                      sermat;

        public String tplnr;

        public String vaplz,
                      wawrk;
        protected List<OrdOperation> _orderOperations;

        protected BaseOrder()
        {
            _orderOperations = new List<OrdOperation>();
        }
        protected BaseOrder(FuncLoc _funcLoc)
        {
            Tplnr = _funcLoc.Tplnr;
            _orderOperations = new List<OrdOperation>();
        }
        protected BaseOrder(Equi _equi)
        {
            Equnr = _equi.Equnr;
            _orderOperations = new List<OrdOperation>();
        }
        protected BaseOrder(Notif notif)
        {
            Equnr = notif.Equnr;
            Tplnr = notif.Tplnr;
            Iwerk = notif.Iwerk;
            Qmnum = notif.Qmnum;
            var custNotifType = CustomizingManager.GetCustNotifType(notif.Qmart);
            if (custNotifType.Count == 1)
                Auart = custNotifType[0].Auart;
            _orderOperations = new List<OrdOperation>();
        }
        public virtual void AddOperation(OrdOperation op)
        {
            this._orderOperations.Add(op);
        }
        public virtual List<OrdOperation> GetOperations()
        {
            return _orderOperations;
        }
        public virtual void DelOperation(OrdOperation op)
        {
            this._orderOperations.Remove(op);
        }
        public virtual void DelOperations()
        {
            this._orderOperations.Clear();
        }

        public virtual string Auart
        {
            get { return auart; }
            set { auart = value; }
        }

        public virtual string Aufnr
        {
            get { return aufnr; }
            set { aufnr = value; }
        }

        public virtual string Bautl
        {
            get { return bautl; }
            set { bautl = value; }
        }

        public virtual string Equnr
        {
            get { return equnr; }
            set { equnr = value; }
        }

        public virtual string Gewrk
        {
            get { return gewrk; }
            set { gewrk = value; }
        }

        public virtual DateTime Gltrp
        {
            get { return gltrp; }
            set { gltrp = value; }
        }

        public virtual DateTime Gstrp
        {
            get { return gstrp; }
            set { gstrp = value; }
        }

        public virtual string Ingrp
        {
            get { return ingrp; }
            set { ingrp = value; }
        }

        public virtual string Iwerk
        {
            get { return iwerk; }
            set { iwerk = value; }
        }

        public virtual string Ktext
        {
            get { return ktext; }
            set { ktext = value; }
        }

        public virtual string Kunum
        {
            get { return kunum; }
            set { kunum = value; }
        }

        public virtual string PmObjty
        {
            get { return pm_objty; }
            set { pm_objty = value; }
        }

        public virtual string Priok
        {
            get { return priok; }
            set { priok = value; }
        }

        public virtual string Qmnum
        {
            get { return qmnum; }
            set { qmnum = value; }
        }

        public virtual string Serialnr
        {
            get { return serialnr; }
            set { serialnr = value; }
        }

        public virtual string Sermat
        {
            get { return sermat; }
            set { sermat = value; }
        }

        public virtual string Tplnr
        {
            get { return tplnr; }
            set { tplnr = value; }
        }

        public virtual string Vaplz
        {
            get { return vaplz; }
            set { vaplz = value; }
        }

        public virtual string Wawrk
        {
            get { return wawrk; }
            set { wawrk = value; }
        }

        public virtual string Qmtxt
        {
            get { return qmtxt; }
            set { qmtxt = value; }
        }

        public virtual string Pltxt
        {
            get { return pltxt; }
            set { pltxt = value; }
        }

        public virtual string Eqktx
        {
            get { return eqktx; }
            set { eqktx = value; }
        }

        public virtual string MobileKey
        {
            get { return mobileKey; }
            set { mobileKey = value; }
        }

        public virtual string Objnr
        {
            get { return objnr; }
            set { objnr = value; }
        }

        public virtual string Ilart
        {
            get { return _ilart; }
            set { _ilart = value; }
        }

        public virtual DateTime DateSort
        {
            get { return _dateSort; }
            set { _dateSort = value; }
        }

        public virtual List<OrdOperation> OrderOperations
        {
            get { return _orderOperations; }
            set { _orderOperations = value; }
        }
        #region IComparable Members

        public int CompareTo(object obj1)
        {
            var obj2 = (Order)obj1;
            if (Gstrp > obj2.Gstrp)
                return (-1);
            if (Gstrp < obj2.Gstrp)
                return (1);
            else
                return (0);
        }

        public static void SetSortMode(String sortByVar, Boolean descVar)
        {
            _sortBy = sortByVar;
            _desc = descVar;
        }

        //Sorts the List according to the Values set in setSortMode()
        public class BaseSortOrder : IComparer<Order>
        {
            public int Compare(Order obj1, Order obj2)
            {
                var emp1 = obj1;
                var emp2 = obj2;
                int returnVal = 0;

                //definition of Variables to compare by direction
                if (_desc)
                {
                    emp1 = obj1;
                    emp2 = obj2;
                }
                else
                {
                    emp1 = obj2;
                    emp2 = obj1;
                }

                //Sets which Fields in the List will be compared
                switch (_sortBy)
                {
                    case "Gstrp":
                        if (emp1.Gstrp > emp2.Gstrp)
                            returnVal = (-1);
                        else if (emp1.Gstrp < emp2.Gstrp)
                            returnVal = (1);
                        else
                            returnVal = (0);
                        break;
                    case "auart":
                        returnVal = (String.Compare(emp1.auart, emp2.auart));
                        break;
                    case "aufnr":
                        returnVal = (String.Compare(emp1.aufnr, emp2.aufnr));
                        break;
                    case "Ktext":
                        returnVal = (String.Compare(emp1.Ktext, emp2.Ktext));
                        break;
                    case "EquiEqfnr":
                        returnVal = (String.Compare(emp1.EquiEqfnr, emp2.EquiEqfnr));
                        break;
                    case "FlocEqfnr":
                        returnVal = (String.Compare(emp1.FlocEqfnr, emp2.FlocEqfnr));
                        break;
                    case "Raumnr":
                        returnVal = (String.Compare(emp1.Raumnr, emp2.Raumnr));
                        break;
                    case "Stort":
                        returnVal = (String.Compare(emp1.Stort, emp2.Stort));
                        break;
                    case "Tplnr":
                        returnVal = (String.Compare(emp1.Tplnr, emp2.Tplnr));
                        break;
                    case "Pltxt":
                        returnVal = (String.Compare(emp1.Pltxt, emp2.Pltxt));
                        break;
                    case "DateSort":
                        if(emp1.DateSort > emp2.DateSort)
                            returnVal = -1;
                        else if (emp1.DateSort < emp2.DateSort)
                            returnVal = 1;
                        else
                            returnVal = 0;
                        break;
                }
                return returnVal;
            }
        }
        #endregion

        public virtual string GetValueByString(String field)
        {
            var retValue = "";
            if (field == null)
                return retValue;
            switch (field.ToLower())
            {
                case "auart":
                    retValue = auart;
                    break;
                case "aufnr":
                    retValue = aufnr;
                    break;
                case "bautl":
                    retValue = bautl;
                    break;
                case "eqktx":
                    retValue = eqktx;
                    break;
                case "gewrk":
                    retValue = gewrk;
                    break;
                case "equnr":
                    retValue = equnr;
                    break;
                case "ingrp":
                    retValue = ingrp;
                    break;
                case "iwerk":
                    retValue = iwerk;
                    break;
                case "ktext":
                    retValue = ktext;
                    break;
                case "kunum":
                    retValue = kunum;
                    break;
                case "pltxt":
                    retValue = pltxt;
                    break;
                case "tplnr":
                    retValue = tplnr;
                    break;
                case "tidnr":
                    retValue = TIDNR;
                    break;
                case "stort":
                    retValue = stort;
                    break;
                case "raumnr":
                    retValue = raumnr;
                    break;
                case "mobilekey":
                    retValue = mobileKey;
                    break;
                case "datesort":
                    retValue = DateSort.ToString("yyyyMMdd HHmmss");
                    break;
                case "status":
                    if (ObjectStatus.Count > 0)
                        retValue = ObjectStatus[0].UserStatusCode;
                    else
                        retValue = "";
                    break;
            }
            return retValue;
        }

        public class BaseOrderSort : IComparer<Order>
        {
            private Boolean _desc = true;
            private String _compareField;

            public BaseOrderSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Order x, Order y)
            {
                switch (_compareField.ToLower())
                {
                    case "auart":
                        if (_desc)
                            return x.Auart.CompareTo(y.Auart);
                        return y.Auart.CompareTo(x.Auart);
                    case "aufnr":
                        if (_desc)
                            return (x).Aufnr.CompareTo(y.Aufnr);
                        return (y).Aufnr.CompareTo(x.Aufnr);
                    case "bautl":
                        if (_desc)
                            return (x).Bautl.CompareTo(y.Bautl);
                        return (y).Bautl.CompareTo(x.Bautl);
                    case "eqktx":
                        if (_desc)
                            return (x).Eqktx.CompareTo(y.Eqktx);
                        return (y).Eqktx.CompareTo(x.Eqktx);
                    case "gewrk":
                        if (_desc)
                            return (x).Gewrk.CompareTo(y.Gewrk);
                        return (y).Gewrk.CompareTo(x.Gewrk);
                    case "equnr":
                        if (_desc)
                            return (x).Equnr.CompareTo(y.Equnr);
                        return (y).Equnr.CompareTo(x.Equnr);
                    case "ingrp":
                        if (_desc)
                            return (x).Ingrp.CompareTo(y.Ingrp);
                        return (y).Ingrp.CompareTo(x.Ingrp);
                    case "iwerk":
                        if (_desc)
                            return (x).Iwerk.CompareTo(y.Iwerk);
                        return (y).Iwerk.CompareTo(x.Iwerk);
                    case "ktext":
                        if (_desc)
                            return (x).Ktext.CompareTo(y.Ktext);
                        return (y).Ktext.CompareTo(x.Ktext);
                    case "kunum":
                        if (_desc)
                            return (x).Kunum.CompareTo(y.Kunum);
                        return (y).Kunum.CompareTo(x.Kunum);
                    case "pltxt":
                        if (_desc)
                            return (x).Pltxt.CompareTo(y.Pltxt);
                        return (y).Pltxt.CompareTo(x.Pltxt);
                    case "tplnr":
                        if (_desc)
                            return (x).Tplnr.CompareTo(y.Tplnr);
                        return (y).Tplnr.CompareTo(x.Tplnr);
                    case "stort":
                        if (_desc)
                            return (x).Stort.CompareTo(y.Stort);
                        return (y).Stort.CompareTo(x.Stort);
                    case "raumnr":
                        if (_desc)
                            return (x).Raumnr.CompareTo(y.Raumnr);
                        return (y).Raumnr.CompareTo(x.Raumnr);
                    case "status":
                        if (_desc)
                            return (x).ObjectStatus[0].UserStatusCode.CompareTo(y.ObjectStatus[0].UserStatusCode);
                        return (y).ObjectStatus[0].UserStatusCode.CompareTo(x.ObjectStatus[0].UserStatusCode);
                    case "datesort":
                        if (_desc)
                            return (x).DateSort.CompareTo(y.DateSort);
                        return (y).DateSort.CompareTo(x.DateSort);
                    default:
                        return (x).Aufnr.CompareTo(y.Aufnr);
                }
            }
        }
    }
}