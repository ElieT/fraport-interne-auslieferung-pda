﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseObjClass
    {
        public string Objnr { get; set; }

        public string Atinn { get; set; }

        public string Clint { get; set; }

        public string Atzhl { get; set; }

        public string Mafid { get; set; }

        public string Klart { get; set; }

        public string Adzhl { get; set; }

        public string Atflv { get; set; }

        public string Atflb { get; set; }

        public string Atawe { get; set; }

        public string Ataw1 { get; set; }

        public string Atcod { get; set; }

        public string Attlv { get; set; }

        public string Attlb { get; set; }

        public string Atwrt { get; set; }

        public string Atbez { get; set; }

        public string Txtbz { get; set; }

        public string Atwtb { get; set; }


        //Value comes from C_CHARACTVALUE
        public string Atwme { get; set; }

        //Value comes from C_CLASSCHARACT
        public string Atfor { get; set; }
        //Value comes from C_CLASSCHARACT
        public string Atein { get; set; }
        //Value comes from C_CLASSCHARACT
        public string Atint { get; set; }
        //Value comes from C_CLASSCHARACT
        public string Msehi { get; set; }
        //Value comes from C_CLASSCHARACT
        public string Mseh6 { get; set; }
        //Value comes from C_CLASSCHARACT
        public string Mseht { get; set; }
    }
}