﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseLongtext
    {
        public String TdObject { get; set; }
        public String TdName { get; set; }
        public String TdId { get; set; }
        public String TdSpras { get; set; }
        public String Text { get; set; }
    }
}
