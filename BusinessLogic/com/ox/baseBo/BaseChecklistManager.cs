﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.control;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseChecklistManager
    {
        protected static ChecklistManager _instance;

        protected BaseChecklistManager()
        {

        }

        public static void CheckListResultExist(FuncLoc _funcloc, String _checkCode1, String _checkCode2,
                                                out Boolean _resultCode1, out Boolean _resultCode2)
        {
            _resultCode1 = false;
            _resultCode2 = false;
            try
            {
                String sqlStmt = "SELECT * FROM D_QPRESULT WHERE MANDT = @MANDT " +
                            "AND USERID = @USERID AND TPLNR = @TPLNR " +
                            "AND (UPDFLAG = @UPDFLAG1 OR UPDFLAG = @UPDFLAG2)";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@TPLNR", _funcloc.Tplnr);
                cmd.Parameters.AddWithValue("@UPDFLAG1", "I");
                cmd.Parameters.AddWithValue("@UPDFLAG2", "X");
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checklistResults = new ChecklistResults();
                    checklistResults.CodeGrp1 = (String)rdr["CODE_GRP1"];
                    checklistResults.Code1 = (String)rdr["CODE1"];
                    // Check if any positive result exist
                    if (checklistResults.Code1.Equals(_checkCode1))
                        _resultCode1 = true;
                    // Check if any negative result exist
                    if (checklistResults.Code1.Equals(_checkCode2))
                        _resultCode2 = true;
                }
                // if no negative result exist and at least on positive result exist -> Set positive = true
                if (!_resultCode2 && _resultCode1)
                    _resultCode1 = true;
                else
                    _resultCode1 = false;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void CheckListResultExist(Equi _equi, String _checkCode1, String _checkCode2,
                                                out Boolean _resultCode1, out Boolean _resultCode2)
        {
            _resultCode1 = false;
            _resultCode2 = false;
        }

        public ArrayList GetChecklistResults(String argMandt, String argUserid, String argTplnr)
        {
            var retVal = new ArrayList();
            try
            {
                String sqlStmt = "SELECT * FROM D_QPRESULT WHERE MANDT = @MANDT " +
                                 "AND USERID = @USERID AND TPLNR = @TPLNR " +
                                 "AND (UPDFLAG = @UPDFLAG1 OR UPDFLAG = @UPDFLAG2) ORDER BY D_QPRESULT.INSOPER";

                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@TPLNR", argTplnr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var status = (String)rdr["STATUS"];

                    if (status.Equals("0000") || status.Equals("0001"))
                    {
                        var checklistResults = new ChecklistResults();
                        checklistResults.Insplot = rdr["INSPLOT"];
                        checklistResults.Insoper = rdr["INSOPER"];
                        checklistResults.Inschar = rdr["INSCHAR"];
                        checklistResults.Matnr = rdr["MATNR"];
                        checklistResults.Werk = rdr["WERK"];
                        checklistResults.Qmnum = rdr["QMNUM"];
                        checklistResults.Evaluate = rdr["EVALUATION"];
                        if (rdr["INSPECTOR"].ToString().Equals(" "))
                        {
                            checklistResults.Inspector = rdr["INSPECTOR"];
                        }
                        if (rdr["START_DATE"].ToString().Equals(" "))
                        {
                            DateTimeHelper.getDate(rdr["START_DATE"], out checklistResults.START_DATE);
                        }
                        if (rdr["START_TIME"].ToString().Equals(" "))
                        {
                            DateTimeHelper.getTime(rdr["START_DATE"], rdr["START_TIME"],
                                                   out checklistResults.START_TIME);
                        }
                        if (rdr["END_DATE"].ToString().Equals(" "))
                        {
                            DateTimeHelper.getDate(rdr["END_DATE"], out checklistResults.END_DATE);
                        }
                        if (rdr["END_TIME"].ToString().Equals(" "))
                        {
                            DateTimeHelper.getTime(rdr["END_DATE"], rdr["END_TIME"], out checklistResults.END_TIME);
                        }
                        checklistResults.MeanValue = rdr["MEAN_VALUE"];
                        checklistResults.Remark = rdr["REMARK"];
                        if (rdr["CODE_GRP1"].ToString().Equals(" "))
                        {
                            checklistResults.CodeGrp1 = rdr["CODE_GRP1"];
                        }
                        checklistResults.Code1 = rdr["CODE1"];
                        checklistResults.CodeGrp2 = rdr["CODE_GRP2"];
                        checklistResults.Code2 = rdr["CODE2"];
                        if (rdr["TPLNR"].ToString().Equals(" "))
                        {
                            checklistResults.Tplnr = rdr["TPLNR"];
                        }
                        if (rdr["STATUS"].ToString().Equals(" "))
                        {
                            checklistResults.Status = rdr["STATUS"];
                        }

                        retVal.Add(checklistResults);
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public ArrayList GetCheckType(String matnr, String werk)
        {
            var retVal = new ArrayList();
            try
            {
                String sqlStmt =
                    "SELECT C_QPLANCHAR.PLNTY, C_QPLANCHAR.PLNNR, C_QPLANCHAR.PLNKN, C_QPLANCHAR.KZEINSTELL, C_QPLANCHAR.MERKNR, " +
                    "C_QPLANCHAR.ZAEHL, C_QPLANCHAR.KURZTEXT, C_QPLANOPER.VORNR " +
                    "FROM " +
                        "C_QPLANMAT INNER JOIN " +
                    "C_QPLANHEAD ON " +
                        "C_QPLANMAT.PLNTY = C_QPLANHEAD.PLNTY AND " +
                        "C_QPLANMAT.PLNNR = C_QPLANHEAD.PLNNR AND " +
                        "C_QPLANMAT.PLNAL = C_QPLANHEAD.PLNAL AND " +
                        "C_QPLANMAT.ZAEHL = C_QPLANHEAD.ZAEHL " +
                        "INNER JOIN " +
                    "C_QPLANOPER ON " +
                        "C_QPLANHEAD.PLNTY = C_QPLANOPER.PLNTY AND " +
                        "C_QPLANHEAD.PLNNR = C_QPLANOPER.PLNNR AND " +
                        "C_QPLANHEAD.ZAEHL = C_QPLANOPER.ZAEHL " +
                        "INNER JOIN " +
                    "C_QPLANCHAR ON  " +
                        "C_QPLANOPER.PLNTY = C_QPLANCHAR.PLNTY AND  " +
                        "C_QPLANOPER.PLNNR = C_QPLANCHAR.PLNNR AND  " +
                        "C_QPLANOPER.PLNKN = C_QPLANCHAR.PLNKN " +
                    "WHERE " +
                        "C_QPLANMAT.MATNR = @MATNR AND C_QPLANMAT.WERKS = @WERK";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MATNR", matnr);
                cmd.Parameters.AddWithValue("@WERK", werk);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checkType = new CheckType();
                    checkType.PLNTY = rdr["PLNTY"];
                    checkType.PLNNR = rdr["PLNNR"];
                    checkType.PLNKN = rdr["PLNKN"];
                    checkType.MERKNR = rdr["MERKNR"];
                    checkType.KZEINSTELL = rdr["KZEINSTELL"];
                    checkType.ZAEHL = rdr["ZAEHL"];
                    checkType.VORNR = rdr["VORNR"];
                    checkType.KURZTEXT = rdr["KURZTEXT"];
                    checkType.MATNR = matnr;
                    checkType.WERK = werk;
                    retVal.Add(checkType);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static void UpdateChecklistResult(List<ChecklistResults> _checkListResults)
        {
            try
            {
                String sqlStmt = "UPDATE [D_QPRESULT] SET " +
                                     "[QMNUM] = @QMNUM, [INSPECTOR] = @INSPECTOR, " +
                                     "[START_DATE] = @START_DATE, [START_TIME] = @START_TIME, " +
                                     "[END_DATE] = @END_DATE, [END_TIME] = @END_TIME, " +
                                     "[MEAN_VALUE] = @MEAN_VALUE, [REMARK] = @REMARK, " +
                                     "[CODE1] = @CODE1, [CODE_GRP1] = @CODE_GRP1, " +
                                     "[CODE2] = @CODE2, [CODE_GRP2] = @CODE_GRP2, " +
                                     "[CLOSED] = @CLOSED, [TPLNR] = @TPLNR, " +
                                     "[WERK] = @WERK, [UPDFLAG] = @UPDFLAG, " +
                                     "[KURZTEXT] = @KURZTEXT, " +
                                     "[EQUNR] = @EQUNR " +
                                     "[EVALUATION] = @EVALUATION " +
                                     "WHERE [MANDT] = @MANDT AND  [USERID] = @USERID AND " +
                                     "[INSPLOT] = @INSPLOT AND  [INSOPER] = @INSOPER AND [INSCHAR] = @INSCHAR AND DIRTY <> 'X'";
                var cmd = new SQLiteCommand(sqlStmt);
                foreach (var _checkListResult in _checkListResults)
                {
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@INSPLOT", _checkListResult.Insplot);
                    cmd.Parameters.AddWithValue("@INSOPER", _checkListResult.Insoper);
                    cmd.Parameters.AddWithValue("@INSCHAR", _checkListResult.Inschar);
                    cmd.Parameters.AddWithValue("@QMNUM", _checkListResult.Qmnum);
                    cmd.Parameters.AddWithValue("@INSPECTOR", _checkListResult.Inspector);
                    cmd.Parameters.AddWithValue("@START_DATE", DateTimeHelper.GetDateString(_checkListResult.StartDate));
                    cmd.Parameters.AddWithValue("@START_TIME", DateTimeHelper.GetTimeString(_checkListResult.StartTime));
                    cmd.Parameters.AddWithValue("@END_DATE", DateTimeHelper.GetDateString(_checkListResult.EndDate));
                    cmd.Parameters.AddWithValue("@END_TIME", DateTimeHelper.GetTimeString(_checkListResult.EndTime));
                    cmd.Parameters.AddWithValue("@MEAN_VALUE", _checkListResult.MeanValue);
                    cmd.Parameters.AddWithValue("@REMARK", _checkListResult.Remark);
                    cmd.Parameters.AddWithValue("@CODE1", _checkListResult.Code1);
                    cmd.Parameters.AddWithValue("@CODE_GRP1", _checkListResult.CodeGrp1);
                    cmd.Parameters.AddWithValue("@CODE2", _checkListResult.Code2);
                    cmd.Parameters.AddWithValue("@CODE_GRP2", _checkListResult.CodeGrp2);
                    cmd.Parameters.AddWithValue("@CLOSED", _checkListResult.Closed);
                    cmd.Parameters.AddWithValue("@TPLNR", _checkListResult.Tplnr);
                    cmd.Parameters.AddWithValue("@STATUS", _checkListResult.Status);
                    cmd.Parameters.AddWithValue("@MATNR", _checkListResult.Matnr);
                    cmd.Parameters.AddWithValue("@WERK", _checkListResult.Werk);
                    cmd.Parameters.AddWithValue("@EQUNR", _checkListResult.Equnr);
                    cmd.Parameters.AddWithValue("@KURZTEXT", _checkListResult.Kurztext);
                    cmd.Parameters.AddWithValue("@EVALUATION", _checkListResult.Evaluate);
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");

                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            OxDbManager.GetInstance().CommitTransaction();
        }


        public void createNewInsplot(ref String dbIdInsplot)
        {
            try
            {
                int intDbKeyInsplot = -1, intDbKeyNotif = -1;


                // get db key Insplot
                String sqlStmt = "SELECT MAX(rowid) + 1 FROM D_QPRESULT";
                String result = "";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyInsplot = int.Parse(result);
                else
                    intDbKeyInsplot = 1;

                dbIdInsplot = intDbKeyInsplot.ToString();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void SaveChecklistResult(List<ChecklistResults> _checkListResults)
        {
            try
            {
                int intDbKeyInsplot = -1;

                var sqlStmt = "SELECT MAX(rowid) + 1 FROM D_QPRESULT WHERE UPDFLAG = 'I' OR UPDFLAG = 'X'";
                var result = "";
                var records = OxDbManager.GetInstance().FeedTransaction(new SQLiteCommand(sqlStmt));
                result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKeyInsplot = int.Parse(result);
                else
                    intDbKeyInsplot = 1;

                // Insert Data
                try
                {

                    sqlStmt = "INSERT OR REPLACE INTO [D_QPRESULT] " +
                              "(MANDT, USERID, INSPLOT, INSOPER, INSCHAR, QMNUM, INSPECTOR, " +
                              "START_DATE, START_TIME, END_DATE, END_TIME, " +
                              "MEAN_VALUE, REMARK, CODE1, CODE_GRP1, CODE2, CODE_GRP2, CLOSED, TPLNR, STATUS, MATNR, WERK, EQUNR, KURZTEXT, " +
                              "UPDFLAG, MOBILEKEY, EVALUATION) " +
                              "Values ( " +
                              "@MANDT, @USERID, @INSPLOT, @INSOPER, @INSCHAR, @QMNUM, @INSPECTOR, " +
                              "@START_DATE, @START_TIME, @END_DATE, @END_TIME, " +
                              "@MEAN_VALUE, @REMARK, @CODE1, @CODE_GRP1, @CODE2, @CODE_GRP2, @CLOSED, " +
                              "@TPLNR, @STATUS, @MATNR, @WERK, @EQUNR, @KURZTEXT, @UPDFLAG, @MOBILEKEY, @EVALUATION) ";
                    var cmd = new SQLiteCommand(sqlStmt);

                    foreach (var _checkListResult in _checkListResults)
                    {
                        if (String.IsNullOrEmpty(_checkListResult.Insplot))
                            _checkListResult.Insplot = intDbKeyInsplot.ToString();

                        cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                        cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                        cmd.Parameters.AddWithValue("@INSPLOT", _checkListResult.Insplot);
                        cmd.Parameters.AddWithValue("@INSOPER", _checkListResult.Insoper);
                        cmd.Parameters.AddWithValue("@INSCHAR", _checkListResult.Inschar);
                        cmd.Parameters.AddWithValue("@QMNUM", _checkListResult.Qmnum);
                        cmd.Parameters.AddWithValue("@INSPECTOR", _checkListResult.Inspector);
                        cmd.Parameters.AddWithValue("@START_DATE",
                                                    DateTimeHelper.GetDateString(_checkListResult.StartDate));
                        cmd.Parameters.AddWithValue("@START_TIME", DateTimeHelper.GetTimeString(_checkListResult.StartTime));
                        cmd.Parameters.AddWithValue("@END_DATE", DateTimeHelper.GetDateString(DateTime.Now));
                        cmd.Parameters.AddWithValue("@END_TIME", DateTimeHelper.GetTimeString(DateTime.Now));
                        cmd.Parameters.AddWithValue("@MEAN_VALUE", _checkListResult.MeanValue);
                        cmd.Parameters.AddWithValue("@REMARK", _checkListResult.Remark);
                        cmd.Parameters.AddWithValue("@CODE1", _checkListResult.Code1);
                        cmd.Parameters.AddWithValue("@CODE_GRP1", _checkListResult.CodeGrp1);
                        cmd.Parameters.AddWithValue("@CODE2", _checkListResult.Code2);
                        cmd.Parameters.AddWithValue("@CODE_GRP2", _checkListResult.CodeGrp2);
                        cmd.Parameters.AddWithValue("@CLOSED", _checkListResult.Closed);
                        cmd.Parameters.AddWithValue("@TPLNR", _checkListResult.Tplnr);
                        cmd.Parameters.AddWithValue("@STATUS", _checkListResult.Status);
                        cmd.Parameters.AddWithValue("@MATNR", _checkListResult.Matnr);
                        cmd.Parameters.AddWithValue("@WERK", _checkListResult.Werk);
                        cmd.Parameters.AddWithValue("@EQUNR", _checkListResult.Equnr);
                        cmd.Parameters.AddWithValue("@KURZTEXT", _checkListResult.Kurztext);
                        cmd.Parameters.AddWithValue("@MOBILEKEY", _checkListResult.MobileKey);
                        cmd.Parameters.AddWithValue("@EVALUATION", _checkListResult.Evaluate);
                        cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                        OxDbManager.GetInstance().FeedTransaction(cmd);
                    }
                }
                catch (Exception ex)
                {
                    FileManager.LogException(ex);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static List<ChecklistResults> GetCheckListResults(FuncLoc funcloc, String qmnum)
        {
            var _checkListResults = new List<ChecklistResults>();
            try
            {
                var sqlStmt = "SELECT * FROM D_QPRESULT WHERE TPLNR = @TPLNR AND QMNUM = @QMNUM AND (UPDFLAG = @UPDFLAG OR UPDFLAG = @UPDFLAG2) " +
                                    "ORDER BY D_QPRESULT.INSOPER";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TPLNR", funcloc.Tplnr);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@UPDFLAG2", "X");
                cmd.Parameters.AddWithValue("@QMNUM", qmnum);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checklistResults = new ChecklistResults();
                    checklistResults.Insplot = rdr["INSPLOT"];
                    checklistResults.Insoper = rdr["INSOPER"];
                    checklistResults.Inschar = rdr["INSCHAR"];
                    checklistResults.Matnr = rdr["MATNR"];
                    checklistResults.Werk = rdr["WERK"];
                    checklistResults.Qmnum = rdr["QMNUM"];
                    checklistResults.Inspector = rdr["INSPECTOR"];
                    DateTimeHelper.getDate(rdr["START_DATE"], out checklistResults.START_DATE);
                    DateTimeHelper.getTime(rdr["START_DATE"], rdr["START_TIME"], out checklistResults.START_TIME);
                    DateTimeHelper.getDate(rdr["END_DATE"], out checklistResults.END_DATE);
                    DateTimeHelper.getTime(rdr["END_DATE"], rdr["END_TIME"], out checklistResults.END_TIME);
                    checklistResults.MeanValue = rdr["MEAN_VALUE"];
                    checklistResults.Remark = rdr["REMARK"];
                    checklistResults.CodeGrp1 = rdr["CODE_GRP1"];
                    checklistResults.Code1 = rdr["CODE1"];
                    checklistResults.CodeGrp2 = rdr["CODE_GRP2"];
                    checklistResults.Code2 = rdr["CODE2"];
                    checklistResults.Closed = rdr["CLOSED"];
                    checklistResults.Tplnr = rdr["TPLNR"];
                    checklistResults.Equnr = rdr["EQUNR"];
                    checklistResults.Status = rdr["STATUS"];
                    checklistResults.Kurztext = rdr["KURZTEXT"];
                    checklistResults.Updateflag = rdr["UPDFLAG"];
                    checklistResults.Evaluate = rdr["EVALUATION"];
                    _checkListResults.Add(checklistResults);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _checkListResults;
        }

        public static List<ChecklistResults> GetCheckListResults(Equi equi, String qmnum)
        {
            var _checkListResults = new List<ChecklistResults>();
            try
            {
                var sqlStmt = "SELECT * FROM D_QPRESULT WHERE EQUNR = @EQUNR AND QMNUM = @QMNUM  AND (UPDFLAG = @UPDFLAG OR UPDFLAG = @UPDFLAG2) " +
                                    "ORDER BY D_QPRESULT.INSOPER";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                if (equi.Equnr == null)
                    cmd.Parameters.AddWithValue("@EQUNR", DBNull.Value.ToString());
                else
                    cmd.Parameters.AddWithValue("@EQUNR", equi.Equnr);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@UPDFLAG2", "X");
                cmd.Parameters.AddWithValue("@QMNUM", qmnum);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checklistResults = new ChecklistResults();
                    checklistResults.Insplot = rdr["INSPLOT"];
                    checklistResults.Insoper = rdr["INSOPER"];
                    checklistResults.Inschar = rdr["INSCHAR"];
                    checklistResults.Matnr = rdr["MATNR"];
                    checklistResults.Werk = rdr["WERK"];
                    checklistResults.Qmnum = rdr["QMNUM"];
                    checklistResults.Inspector = rdr["INSPECTOR"];
                    DateTimeHelper.getDate(rdr["START_DATE"], out checklistResults.START_DATE);
                    DateTimeHelper.getTime(rdr["START_DATE"], rdr["START_TIME"], out checklistResults.START_TIME);
                    DateTimeHelper.getDate(rdr["END_DATE"], out checklistResults.END_DATE);
                    DateTimeHelper.getTime(rdr["END_DATE"], rdr["END_TIME"], out checklistResults.END_TIME);
                    checklistResults.MeanValue = rdr["MEAN_VALUE"];
                    checklistResults.Remark = rdr["REMARK"];
                    checklistResults.CodeGrp1 = rdr["CODE_GRP1"];
                    checklistResults.Code1 = rdr["CODE1"];
                    checklistResults.CodeGrp2 = rdr["CODE_GRP2"];
                    checklistResults.Code2 = rdr["CODE2"];
                    checklistResults.Closed = rdr["CLOSED"];
                    checklistResults.Equnr = rdr["EQUNR"];
                    checklistResults.Tplnr = rdr["TPLNR"];
                    checklistResults.Status = rdr["STATUS"];
                    checklistResults.Kurztext = rdr["KURZTEXT"];
                    checklistResults.Updateflag = rdr["UPDFLAG"];
                    checklistResults.Evaluate = rdr["EVALUATION"];
                    _checkListResults.Add(checklistResults);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _checkListResults;
        }

        public static void DeleteEmptyResults(Equi equi, FuncLoc floc, String qmnum)
        {
            try
            {
                var sqlStmt = "DELETE FROM D_QPRESULT WHERE EQUNR = @EQUNR AND TPLNR = @TPLNR AND QMNUM = @QMNUM  AND UPDFLAG = @UPDFLAG AND CLOSED <> @CLOSED";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                if (equi == null || equi.Equnr == null)
                    cmd.Parameters.AddWithValue("@EQUNR", DBNull.Value.ToString());
                else
                    cmd.Parameters.AddWithValue("@EQUNR", equi.Equnr);
                if (floc == null || floc.Tplnr == null)
                    cmd.Parameters.AddWithValue("@TPLNR", DBNull.Value.ToString());
                else
                    cmd.Parameters.AddWithValue("@TPLNR", floc.Tplnr);
                cmd.Parameters.AddWithValue("@QMNUM", qmnum);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@CLOSED", "X");

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static List<ChecklistItem> GetCheckList(FuncLoc funcLoc)
        {
            var checkListItems = new List<ChecklistItem>();
            try
            {
                String sqlStmt = "SELECT C_QPLANCHAR.PLNTY, C_QPLANCHAR.PLNNR, C_QPLANCHAR.PLNKN, C_QPLANCHAR.KZEINSTELL, C_QPLANCHAR.MERKNR, " +
                            "C_QPLANCHAR.ZAEHL, C_QPLANCHAR.GUELTIGAB, C_QPLANCHAR.KURZTEXT, C_QPLANCHAR.KATAB1, C_QPLANCHAR.KATALGART1, " +
                            "C_QPLANCHAR.AUSWMENGE1, C_QPLANCHAR.AUSWMGWRK1, C_QPLANCHAR.KATAB2, C_QPLANCHAR.KATALGART2, C_QPLANCHAR.AUSWMENGE2, " +
                            "C_QPLANCHAR.AUSWMGWRK2, C_QPLANCHAR.SOLLWERT, C_QPLANCHAR.TOLERANZOB, C_QPLANCHAR.TOLERANZUN, C_QPLANCHAR.MASSEINHSW, " +
                            "C_QPLANCHAR.CHAR_TYPE, C_QPLANOPER.VORNR, C_QPLANCHAR.STELLEN, C_QPLANOPER.LTXA1, C_QPLANMAT.WERKS, C_QPLANHEAD.KTEXT AS HEADKTEXT, " +
                            "C_QPLANHEAD.PLNAL, C_QPLANHEAD.ZAEHL AS HEAD_ZAEHL " +
                            "FROM " +
                                "C_QPLANMAT INNER JOIN " +
                            "C_QPLANHEAD ON " +
                                "C_QPLANMAT.PLNTY = C_QPLANHEAD.PLNTY AND " +
                                "C_QPLANMAT.PLNNR = C_QPLANHEAD.PLNNR AND " +
                                "C_QPLANMAT.PLNAL = C_QPLANHEAD.PLNAL " +
                                "INNER JOIN " +
                            "C_QPLANOPER ON " +
                                "C_QPLANHEAD.PLNTY = C_QPLANOPER.PLNTY AND " +
                                "C_QPLANHEAD.PLNNR = C_QPLANOPER.PLNNR " +
                                "INNER JOIN " +
                            "C_QPLANCHAR ON " +
                                "C_QPLANOPER.PLNTY = C_QPLANCHAR.PLNTY AND " +
                                "C_QPLANOPER.PLNNR = C_QPLANCHAR.PLNNR AND " +
                                "C_QPLANOPER.PLNKN = C_QPLANCHAR.PLNKN " +
                                "INNER JOIN " +
                            "D_FLOC ON " +
                                "C_QPLANMAT.MATNR = D_FLOC.SUBMT AND " +
                                "C_QPLANMAT.WERKS = D_FLOC.IWERK AND " +
                                "D_FLOC.TPLNR = @TPLNR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TPLNR", funcLoc.Tplnr);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checklistItem = new ChecklistItem();
                    checklistItem.Plnty = rdr["PLNTY"];
                    checklistItem.Plnnr = rdr["PLNNR"];
                    checklistItem.Plnkn = rdr["PLNKN"];
                    checklistItem.Plnal = rdr["PLNAL"];
                    checklistItem.HeadZaehl = rdr["HEAD_ZAEHL"];
                    checklistItem.Kzeinstell = rdr["KZEINSTELL"];
                    checklistItem.Merknr = rdr["MERKNR"];
                    checklistItem.Zaehl = rdr["ZAEHL"];
                    checklistItem.Gueltigab = rdr["GUELTIGAB"];
                    checklistItem.Kurztext = rdr["KURZTEXT"];
                    checklistItem.Katab1 = rdr["KATAB1"];
                    checklistItem.Katalgart1 = rdr["KATALGART1"];
                    checklistItem.Auswmenge1 = rdr["AUSWMENGE1"];
                    checklistItem.Auswmgwrk1 = rdr["AUSWMGWRK1"];
                    checklistItem.Katab2 = rdr["KATAB2"];
                    checklistItem.Katalgart2 = rdr["KATALGART2"];
                    checklistItem.Auswmenge2 = rdr["AUSWMENGE2"];
                    checklistItem.Auswmgwrk2 = rdr["AUSWMGWRK2"];
                    checklistItem.Sollwert = rdr["SOLLWERT"];
                    checklistItem.Toleranzob = rdr["TOLERANZOB"];
                    checklistItem.Toleranzun = rdr["TOLERANZUN"];
                    checklistItem.Masseinhsw = rdr["MASSEINHSW"];
                    checklistItem.CharType = rdr["CHAR_TYPE"];
                    checklistItem.Vornr = rdr["VORNR"];
                    checklistItem.Werks = rdr["WERKS"];
                    checklistItem.Vorltx = rdr["LTXA1"];
                    checklistItem.HeadKtext = rdr["HEADKTEXT"];
                    checklistItem.Stellen = rdr["STELLEN"];
                    checkListItems.Add(checklistItem);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return checkListItems;
        }

        public static List<ChecklistItem> GetCheckList(Equi equi)
        {
            var checkListItems = new List<ChecklistItem>();
            try
            {
                String sqlStmt = "SELECT C_QPLANCHAR.PLNTY, C_QPLANCHAR.PLNNR, C_QPLANCHAR.PLNKN, C_QPLANCHAR.KZEINSTELL, C_QPLANCHAR.MERKNR, " +
                            "C_QPLANCHAR.ZAEHL, C_QPLANCHAR.GUELTIGAB, C_QPLANCHAR.KURZTEXT, C_QPLANCHAR.KATAB1, C_QPLANCHAR.KATALGART1, " +
                            "C_QPLANCHAR.AUSWMENGE1, C_QPLANCHAR.AUSWMGWRK1, C_QPLANCHAR.KATAB2, C_QPLANCHAR.KATALGART2, C_QPLANCHAR.AUSWMENGE2, " +
                            "C_QPLANCHAR.AUSWMGWRK2, C_QPLANCHAR.SOLLWERT, C_QPLANCHAR.TOLERANZOB, C_QPLANCHAR.TOLERANZUN, C_QPLANCHAR.MASSEINHSW, " +
                            "C_QPLANCHAR.CHAR_TYPE, C_QPLANOPER.VORNR, C_QPLANCHAR.STELLEN, C_QPLANOPER.LTXA1, C_QPLANMAT.WERKS, C_QPLANHEAD.KTEXT AS HEADKTEXT, " +
                            "C_QPLANHEAD.PLNAL, C_QPLANHEAD.ZAEHL AS HEAD_ZAEHL, C_QPLANOPER.ZAEHL AS OPER_ZAEHL " +
                            "FROM " +
                                "C_QPLANMAT INNER JOIN " +
                            "C_QPLANHEAD ON " +
                                "C_QPLANMAT.PLNTY = C_QPLANHEAD.PLNTY AND " +
                                "C_QPLANMAT.PLNNR = C_QPLANHEAD.PLNNR AND " +
                                "C_QPLANMAT.PLNAL = C_QPLANHEAD.PLNAL " +
                                "INNER JOIN " +
                            "C_QPLANOPER ON " +
                                "C_QPLANHEAD.PLNTY = C_QPLANOPER.PLNTY AND " +
                                "C_QPLANHEAD.PLNNR = C_QPLANOPER.PLNNR " +
                                "INNER JOIN " +
                            "C_QPLANCHAR ON " +
                                "C_QPLANOPER.PLNTY = C_QPLANCHAR.PLNTY AND " +
                                "C_QPLANOPER.PLNNR = C_QPLANCHAR.PLNNR AND " +
                                "C_QPLANOPER.PLNKN = C_QPLANCHAR.PLNKN " +
                                "INNER JOIN " +
                            "D_EQUI ON " +
                                "C_QPLANMAT.MATNR = D_EQUI.SUBMT AND " +
                                "C_QPLANMAT.WERKS = D_EQUI.IWERK AND " +
                                "D_EQUI.EQUI = @EQUI";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@EQUI", equi.Equnr);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checklistItem = new ChecklistItem();
                    checklistItem.Plnty = rdr["PLNTY"];
                    checklistItem.Plnnr = rdr["PLNNR"];
                    checklistItem.Plnkn = rdr["PLNKN"];
                    checklistItem.Plnal = rdr["PLNAL"];
                    checklistItem.HeadZaehl = rdr["HEAD_ZAEHL"];
                    checklistItem.OperZaehl = rdr["OPER_ZAEHL"];
                    checklistItem.Kzeinstell = rdr["KZEINSTELL"];
                    checklistItem.Merknr = rdr["MERKNR"];
                    checklistItem.Zaehl = rdr["ZAEHL"];
                    checklistItem.Gueltigab = rdr["GUELTIGAB"];
                    checklistItem.Kurztext = rdr["KURZTEXT"];
                    checklistItem.Katab1 = rdr["KATAB1"];
                    checklistItem.Katalgart1 = rdr["KATALGART1"];
                    checklistItem.Auswmenge1 = rdr["AUSWMENGE1"];
                    checklistItem.Auswmgwrk1 = rdr["AUSWMGWRK1"];
                    checklistItem.Katab2 = rdr["KATAB2"];
                    checklistItem.Katalgart2 = rdr["KATALGART2"];
                    checklistItem.Auswmenge2 = rdr["AUSWMENGE2"];
                    checklistItem.Auswmgwrk2 = rdr["AUSWMGWRK2"];
                    checklistItem.Sollwert = rdr["SOLLWERT"];
                    checklistItem.Toleranzob = rdr["TOLERANZOB"];
                    checklistItem.Toleranzun = rdr["TOLERANZUN"];
                    checklistItem.Masseinhsw = rdr["MASSEINHSW"];
                    checklistItem.CharType = rdr["CHAR_TYPE"];
                    checklistItem.Vornr = rdr["VORNR"];
                    checklistItem.Vorltx = rdr["LTXA1"];
                    checklistItem.Werks = rdr["WERKS"];
                    checklistItem.HeadKtext = rdr["HEADKTEXT"];
                    checklistItem.Stellen = rdr["STELLEN"];
                    checkListItems.Add(checklistItem);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return checkListItems;
        }

        public static string GetCheckListStatus(OxBusinessObject techObj, String qmnum)
        {
            int countOK = 0, countError = 0, countNoValue = 0;
            var checkListResults = new List<ChecklistResults>();
            var checkListItems = new List<ChecklistItem>();
            if (techObj.GetType().Equals(typeof(FuncLoc)))
            {
                checkListItems = GetCheckList((FuncLoc)techObj);
                checkListResults = GetCheckListResults((FuncLoc)techObj, qmnum);
            }
            else if (techObj.GetType().Equals(typeof(Equi)))
            {
                checkListItems = GetCheckList((Equi)techObj);
                checkListResults = GetCheckListResults((Equi)techObj, qmnum);
            }
            if (checkListResults.Count == 0)
                countNoValue = checkListItems.Count;
            foreach (var list in checkListResults)
            {
                if (list.Code1.Equals("0001"))
                    countOK++;
                else if (list.Code1.Equals("0002"))
                    countError++;
                else
                    countNoValue++;
            }
            return countOK + "/" + countError + "/" + countNoValue;
        }

        public static List<CustCodes> GetCustCodes(String katalogart, String auswahlmge, String werks, Boolean inTransaction)
        {
            var retList = new List<CustCodes>();

            try
            {
                var sqlStmt = "SELECT G_CUSTCODES.CODEGRUPPE, G_CUSTCODES.KATALOGART, G_CUSTCODES.CODE, " +
                                    "G_CUSTCODES.CODEGR_KURZTEXT, G_CUSTCODES.KURZTEXT, C_CUSTAUSWM.BEWERTUNG " +
                                "FROM C_CUSTAUSWM " +
                                "INNER JOIN G_CUSTCODES ON " +
                                    "G_CUSTCODES.CODEGRUPPE = C_CUSTAUSWM.CODEGRUPPE AND " +
                                    "G_CUSTCODES.CODE = C_CUSTAUSWM.CODE AND " +
                                    "G_CUSTCODES.KATALOGART = C_CUSTAUSWM.KATALOGART " +
                                "WHERE C_CUSTAUSWM.KATALOGART = @KATALOGART AND " +
                                    "C_CUSTAUSWM.AUSWAHLMGE = @AUSWAHLMGE AND " +
                                    "C_CUSTAUSWM.WERKS = @WERKS";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@AUSWAHLMGE", auswahlmge);
                cmd.Parameters.AddWithValue("@WERKS", werks);

                List<Dictionary<String, String>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var custcodes = new CustCodes();
                    custcodes.Katalogart = rdr["KATALOGART"];
                    custcodes.Codegruppe = rdr["CODEGRUPPE"];
                    custcodes.Code = rdr["CODE"];
                    custcodes.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                    custcodes.Kurztext = rdr["KURZTEXT"];
                    custcodes.Bewertung = rdr["BEWERTUNG"];
                    retList.Add(custcodes);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }

        public static String GetCodegruppe(String katalogart, String auswahlmge, String werks, Boolean inTransaction)
        {
            try
            {
                var sqlStmt = "SELECT G_CUSTCODES.CODEGRUPPE, G_CUSTCODES.KATALOGART, G_CUSTCODES.CODE, " +
                                    "G_CUSTCODES.CODEGR_KURZTEXT, G_CUSTCODES.KURZTEXT, C_CUSTAUSWM.BEWERTUNG " +
                                "FROM C_CUSTAUSWM " +
                                "INNER JOIN G_CUSTCODES ON " +
                                    "G_CUSTCODES.CODEGRUPPE = C_CUSTAUSWM.CODEGRUPPE AND " +
                                    "G_CUSTCODES.CODE = C_CUSTAUSWM.CODE AND " +
                                    "G_CUSTCODES.KATALOGART = C_CUSTAUSWM.KATALOGART " +
                                "WHERE C_CUSTAUSWM.KATALOGART = @KATALOGART AND " +
                                    "C_CUSTAUSWM.AUSWAHLMGE = @AUSWAHLMGE AND " +
                                    "C_CUSTAUSWM.WERKS = @WERKS";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@AUSWAHLMGE", auswahlmge);
                cmd.Parameters.AddWithValue("@WERKS", werks);

                List<Dictionary<String, String>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    return rdr["CODEGRUPPE"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return "";
        }

        public static List<Cust_013> GetCatalogForButtons(Boolean inTransaction)
        {
            var retVal = new List<Cust_013>();
            try
            {
                var sqlStmt = "SELECT C_AM_CUST_013.WERKS, C_AM_CUST_013.KATALOGART, " +
                              "C_AM_CUST_013.AUSWAHLMGE, C_AM_CUST_013.FUNKTION, C_AM_CUST_013.CODEGRUPPE, C_AM_CUST_013.CODE " +
                              "FROM C_AM_CUST_013 ";
                var cmd = new SQLiteCommand(sqlStmt);

                List<Dictionary<String, String>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var cust013 = new Cust_013();
                    cust013.Werks = rdr["WERKS"];
                    cust013.Katalogart = rdr["KATALOGART"];
                    cust013.Auswahlmge = rdr["AUSWAHLMGE"];
                    cust013.Funktion = rdr["FUNKTION"];
                    cust013.Codegruppe = rdr["CODEGRUPPE"];
                    cust013.Code = rdr["CODE"];
                    retVal.Add(cust013);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
    }
}