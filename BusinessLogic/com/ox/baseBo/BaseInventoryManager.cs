﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseInventoryManager
    {
        public static List<InventoryHead> GetInventoryList()
        {
            var retList = new List<InventoryHead>();
            try
            {
                var sqlStmt = "SELECT * FROM D_PHYSINVHEAD";
                var cmd = new SQLiteCommand(sqlStmt);
                var result = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach(var row in result)
                {
                    var iHead = new InventoryHead();
                    iHead.Updflag = row["UPDFLAG"];
                    iHead.Physinventory = row["PHYSINVENTORY"];
                    iHead.Fiscalyear = row["FISCALYEAR"];
                    iHead.Plant = row["PLANT"];
                    iHead.StgeLoc = row["STGE_LOC"];
                    iHead.DocDate = row["DOC_DATE"];
                    iHead.PlanDate = row["PLAN_DATE"];
                    iHead.CountStatus = row["COUNT_STATUS"];
                    iHead.AdjustStatus = row["ADJUST_STATUS"];
                    iHead.PhysInvRef = row["PHYS_INV_REF"];
                    iHead.Items = GetInventoryItems(iHead, true);
                    retList.Add(iHead);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch(Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
            return retList;
        }

        public static List<InventoryItem> GetInventoryItems(InventoryHead iHead, Boolean inTransaction)
        {
            var retList = new List<InventoryItem>();
            try
            {
                var sqlStmt = "SELECT * FROM D_PHYSINVITEM WHERE PHYSINVENTORY = @PHYSINVENTORY";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@PHYSINVENTORY", iHead.Physinventory);
                List<Dictionary<string, string>> result;
                if(inTransaction)
                     result = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var row in result)
                {
                    var item = new InventoryItem();
                    DateTime time;
                    item.Updflag = row["UPDFLAG"];
                    item.Physinventory = row["PHYSINVENTORY"];
                    item.Fiscalyear = row["FISCALYEAR"];
                    item.Item = row["ITEM"];
                    item.Material = row["MATERIAL"];
                    item.Plant = row["PLANT"];
                    item.StgeLoc = row["STGE_LOC"];
                    item.Batch = row["BATCH"];
                    item.SpecStock = row["SPEC_STOCK"];
                    item.StockType = row["STOCK_TYPE"];
                    item.ChangeUser = row["CHANGE_USER"];
                    DateTimeHelper.getDate(row["CHANGE_DATE"], out time);
                    item.ChangeDate = time;
                    item.CountUser = row["COUNT_USER"];
                    DateTimeHelper.getDate(row["COUNT_DATE"], out time);
                    item.CountDate = time;
                    item.PhInvRef = row["PH_INV_REF"];
                    item.Counted = row["COUNTED"];
                    item.DiffPosted = row["DIFF_POSTED"];
                    item.Recount = row["RECOUNT"];
                    item.AltUnit = row["ALT_UNIT"];
                    item.BookQty = row["BOOK_QTY"];
                    item.ZeroCount = row["ZERO_COUNT"];
                    item.Quantity = row["QUANTITY"];
                    item.BaseUom = row["BASE_UOM"];
                    item.EntryQty = row["ENTRY_QNT"];
                    item.EntryUom = row["ENTRY_UOM"];
                    retList.Add(item);
                }

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }

        public static InventoryHead GetInventoryHead(String physInventory)
        {
            InventoryHead iHead = null;
            try
            {
                var sqlStmt = "SELECT * FROM D_PHYSINVHEAD WHERE PHYSINVENTORY = @PHYSINVENTORY";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@PHYSINVENTORY", physInventory);
                var result = OxDbManager.GetInstance().FeedTransaction(cmd);
                if(result.Count == 1)
                {
                    var row = result[0];
                    iHead = new InventoryHead();
                    iHead.Updflag = row["UPDFLAG"];
                    iHead.Physinventory = row["PHYSINVENTORY"];
                    iHead.Fiscalyear = row["FISCALYEAR"];
                    iHead.Plant = row["PLANT"];
                    iHead.StgeLoc = row["STGE_LOC"];
                    iHead.DocDate = row["DOC_DATE"];
                    iHead.PlanDate = row["PLAN_DATE"];
                    iHead.CountStatus = row["COUNT_STATUS"];
                    iHead.AdjustStatus = row["ADJUST_STATUS"];
                    iHead.PhysInvRef = row["PHYS_INV_REF"];
                    iHead.Items = GetInventoryItems(iHead, true);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
            return iHead;
        }

        public static void SaveInventoryHead(InventoryHead iHead)
        {
            try
            {
                var sqlStmt = "UPDATE D_PHYSINVITEM SET COUNTED = @COUNTED, ENTRY_QNT = @ENTRY_QNT " +
                    "WHERE PHYSINVENTORY = @PHYSINVENTORY AND "+
                    "ITEM = @ITEM AND " +
                    "FISCALYEAR = @FISCALYEAR ";
                var cmd = new SQLiteCommand(sqlStmt);
                foreach(var item in iHead.Items)
                {
                    cmd.Parameters.AddWithValue("@ENTRY_QNT", item.EntryQty);
                    cmd.Parameters.AddWithValue("@COUNTED", item.Counted);
                    cmd.Parameters.AddWithValue("@PHYSINVENTORY", item.Physinventory);
                    cmd.Parameters.AddWithValue("@FISCALYEAR", item.Fiscalyear);
                    cmd.Parameters.AddWithValue("@ITEM", item.Item);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                OxDbManager.GetInstance().CommitTransaction();
            }
        }
    }
}
