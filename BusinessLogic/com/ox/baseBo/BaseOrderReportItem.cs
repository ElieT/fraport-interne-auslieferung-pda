﻿namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseOrderReportItem
    {
        public BaseOrderReportItem(string s1, string s2, string s3, string s4, string s5, string s6)
        {
            Item1 = s1;
            Item2 = s2;
            Item3 = s3;
            Item4 = s4;
            Item5 = s5;
            Item6 = s6;
        }

        public string Item1 { get; set; }

        public string Item2 { get; set; }

        public string Item3 { get; set; }

        public string Item4 { get; set; }

        public string Item5 { get; set; }

        public string Item6 { get; set; }
    }
}
