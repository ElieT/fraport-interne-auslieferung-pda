﻿using System;

namespace oxmc.BusinessLogic.com.ox.baseBo
{
    public class BaseCheckType
    {
        public String KURZTEXT;
        public String KZEINSTELL;
        public String MATNR;
        public String MERKNR;
        public String PLNKN;
        public String PLNNR;
        public String PLNTY;
        public String TPLNR;
        public String VORNR;
        public String WERK;
        public String ZAEHL;
    }
}