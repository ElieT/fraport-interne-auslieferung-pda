﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.basebo
{
    public class BaseStpo
    {
        public string Stlty { get; set; }
        public string Stlnr { get; set; }
        public string Stlkn { get; set; }
        public string Stpoz { get; set; }
        public string Idnrk { get; set; }
        public string Postp { get; set; }
        public string Posnr { get; set; }
        public string Sortf { get; set; }
        public string Meins { get; set; }
        public string Menge { get; set; }
        public string Erskz { get; set; }
        public string Sanin { get; set; }
        public string Stkkz { get; set; }

    }
}
