﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class OrdOperation : BaseOrdOperation
    {

        public string SArbei { get; set; }
        public string SArbeh { get; set; }
        public string SDauno { get; set; }
        public string SDaune { get; set; }

        public String CalendarStatus { get; set; }

        public static String _sortBy;
        public static Boolean _desc;

        public OrdOperation() : base()
        {
            SArbei = "";
            SArbeh = "";
            SDauno = "";
            SDaune = "";
        }

        public OrdOperation(Order order) : base(order)
        {
            SArbei = "";
            SArbeh = "";
            SDauno = "";
            SDaune = "";
        }

        public OrdOperation(OrdOperation ordOper) : base(ordOper)
        {
            SArbei = "";
            SArbeh = "";
            SDauno = "";
            SDaune = "";
        }

        public OrdOperation(Order order, int newVornr) : base(order, newVornr)
        {
            SArbei = "";
            SArbeh = "";
            SDauno = "";
            SDaune = "";
        }

        //Sorts the List according to the Values set in setSortMode()
        public class SortOrdOperation : IComparer<OrdOperation>
        {
            public int Compare(OrdOperation obj1, OrdOperation obj2)
            {
                OrdOperation emp1 = obj1;
                OrdOperation emp2 = obj2;
                int returnVal = 0;

                //definition of Variables to compare by direction
                if (_desc)
                {
                    emp1 = obj1;
                    emp2 = obj2;
                }
                else
                {
                    emp1 = obj2;
                    emp2 = obj1;
                }

                //Sets which Fields in the List will be compared
                switch (_sortBy.ToLower())
                {
                    case "fsavz":
                        goto case "fstau";
                    case "fstau":
                        if (emp1.Fstau != null && emp2.Fstau != null)
                        {
                            if (emp1.Fstau > emp2.Fstau)
                                return (-1);
                            if (emp1.Fstau < emp2.Fstau)
                                return (1);
                            return (0);
                        }
                        if (emp1.Fstau != null && emp2.Fstau == null)
                        {
                            if (emp1.Fstau > emp2.Fsavz)
                                return (-1);
                            if (emp1.Fstau < emp2.Fsavz)
                                return (1);
                            return (0);
                        }
                        if (emp1.Fstau == null && emp2.Fstau != null)
                        {
                            if (emp1.Fsavz > emp2.Fstau)
                                return (-1);
                            if (emp1.Fsavz < emp2.Fstau)
                                return (1);
                            return (0);
                        }
                        if (emp1.Fstau > emp2.Fstau)
                            return (-1);
                        if (emp1.Fstau < emp2.Fstau)
                            return (1);
                        return (0);
                    case "aufnr":
                        return (String.Compare(emp1.aufnr, emp2.aufnr));
                    case "priok":
                        return (String.Compare(emp1.priok, emp2.priok));
                    case "vornr":
                        return (String.Compare(emp1.Vornr, emp2.Vornr));
                    case "aufpl":
                        return (String.Compare(emp1.Aufpl, emp2.Aufpl));
                    case "aplzl":
                        return (String.Compare(emp1.Aplzl, emp2.Aplzl));
                    case "larnt":
                        return (String.Compare(emp1.Larnt, emp2.Larnt));
                    case "tplnr":
                        return (String.Compare(emp1.Tplnr, emp2.Tplnr));
                    case "pernr":
                        return (String.Compare(emp1.Pernr, emp2.Pernr));
                    case "dauno":
                        return (String.Compare(emp1.dauno, emp2.dauno));
                    case "arbei":
                        return (String.Compare(emp1.arbei, emp2.arbei));
                    case "arbeh":
                        return (String.Compare(emp1.arbeh, emp2.arbeh));
                    case "arbpl":
                        return (String.Compare(emp1.arbpl, emp2.arbpl));
                    case "werks":
                        return (String.Compare(emp1.werks, emp2.werks));
                    case "anzma":
                        return (String.Compare(emp1.Anzma, emp2.Anzma));
                    case "bedid":
                        return (String.Compare(emp1.Bedid, emp2.Bedid));
                    case "bedzl":
                        return (String.Compare(emp1.Bedzl, emp2.Bedzl));
                    case "canum":
                        return (String.Compare(emp1.Canum, emp2.Canum));
                    case "arbid":
                        return (String.Compare(emp1.Arbid, emp2.Arbid));
                    case "split":
                        if (String.Compare(emp1.Vornr, emp2.Vornr) != 0)
                            return (String.Compare(emp1.Vornr, emp2.Vornr));
                        return (String.Compare(emp1.Split, emp2.Split));
                }
                return returnVal;
            }
        }

        public string GetValueByString(String field)
        {
            var retValue = "";
            if (field == null)
                return retValue;
            switch (field.ToLower())
            {
                case "fsavd":
                    return Fsavz.ToString("dd.MM.yyyy");
                case "fsavz":
                    return Fsavz.ToString("HH:mm");
                case "fstau":
                    return Fstau.ToString("dd.MM.yyyy HH:mm");
                case "aufnr":
                    return Aufnr;
                case "priok":
                    return Priok;
                case "vornr":
                    return Vornr;
                case "aufpl":
                    return Aufpl;
                case "aplzl":
                    return Aplzl;
                case "larnt":
                    return Larnt;
                case "tplnr":
                    return Tplnr;
                case "pernr":
                    return Pernr;
                case "dauno":
                    return Dauno;
                case "arbei":
                    return Arbei;
                case "arbeh":
                    return Arbeh;
                case "arbpl":
                    return Arbpl;
                case "werks":
                    return Werks;
                case "anzma":
                    return Anzma;
                case "bedid":
                    return Bedid;
                case "bedzl":
                    return Bedzl;
                case "canum":
                    return Canum;
                case "arbid":
                    return Arbid;
                case "split":
                    return Split;
            }
            return retValue;
        }

        public class OrdOperationSort : IComparer<OrdOperation>
        {
            private Boolean _desc = true;
            private String _compareField;

            public OrdOperationSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(OrdOperation x, OrdOperation y)
            {
                switch (_compareField.ToLower())
                {
                    case "fsavz":
                        if (_desc)
                            return x.Fsavz.CompareTo(y.Fsavz);
                        return y.Fsavz.CompareTo(x.Fsavz);
                    case "fstau":
                        if (_desc)
                            return (x).Fstau.CompareTo(y.Fstau);
                        return (y).Fstau.CompareTo(x.Fstau);
                    case "aufnr":
                        if (_desc)
                            return (x).Aufnr.CompareTo(y.Aufnr);
                        return (y).Aufnr.CompareTo(x.Aufnr);
                    case "priok":
                        if (_desc)
                            return (x).Priok.CompareTo(y.Priok);
                        return (y).Priok.CompareTo(x.Priok);
                    case "vornr":
                        if (_desc)
                            return (x).Vornr.CompareTo(y.Vornr);
                        return (y).Vornr.CompareTo(x.Vornr);
                    case "aufpl":
                        if (_desc)
                            return (x).Aufpl.CompareTo(y.Aufpl);
                        return (y).Aufpl.CompareTo(x.Aufpl);
                    case "aplzl":
                        if (_desc)
                            return (x).Aplzl.CompareTo(y.Aplzl);
                        return (y).Aplzl.CompareTo(x.Aplzl);
                    case "larnt":
                        if (_desc)
                            return (x).Larnt.CompareTo(y.Larnt);
                        return (y).Larnt.CompareTo(x.Larnt);
                    case "tplnr":
                        if (_desc)
                            return (x).Tplnr.CompareTo(y.Tplnr);
                        return (y).Tplnr.CompareTo(x.Tplnr);
                    case "pernr":
                        if (_desc)
                            return (x).Pernr.CompareTo(y.Pernr);
                        return (y).Pernr.CompareTo(x.Pernr);
                    case "dauno":
                        if (_desc)
                            return (x).Dauno.CompareTo(y.Dauno);
                        return (y).Dauno.CompareTo(x.Dauno);
                    case "arbei":
                        if (_desc)
                            return (x).Arbei.CompareTo(y.Arbei);
                        return (y).Arbei.CompareTo(x.Arbei);
                    case "arbeh":
                        if (_desc)
                            return (x).Arbeh.CompareTo(y.Arbeh);
                        return (y).Arbeh.CompareTo(x.Arbeh);
                    case "arbpl":
                        if (_desc)
                            return (x).Arbpl.CompareTo(y.Arbpl);
                        return (y).Arbpl.CompareTo(x.Arbpl);
                    case "werks":
                        if (_desc)
                            return (x).Werks.CompareTo(y.Werks);
                        return (y).Werks.CompareTo(x.Werks);
                    case "anzma":
                        if (_desc)
                            return (x).Anzma.CompareTo(y.Anzma);
                        return (y).Anzma.CompareTo(x.Anzma);
                    case "bedid":
                        if (_desc)
                            return (x).Bedid.CompareTo(y.Bedid);
                        return (y).Bedid.CompareTo(x.Bedid);
                    case "bedzl":
                        if (_desc)
                            return (x).Bedzl.CompareTo(y.Bedzl);
                        return (y).Bedzl.CompareTo(x.Bedzl);
                    case "canum":
                        if (_desc)
                            return (x).Canum.CompareTo(y.Canum);
                        return (y).Canum.CompareTo(x.Canum);
                    case "arbid":
                        if (_desc)
                            return (x).Arbid.CompareTo(y.Arbid);
                        return (y).Arbid.CompareTo(x.Arbid);
                    case "split":
                        if (_desc)
                            return (x).Split.CompareTo(y.Split);
                        return (y).Split.CompareTo(x.Split);
                    default:
                        return (x).Fsavz.CompareTo(y.Fsavz);
                }
            }
        }
    }
}