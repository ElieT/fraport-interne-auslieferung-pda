﻿using System;
using System.Collections;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class EMSDataManager
    {
        //public void getDrivers()
        //{
        //    var retList = new ArrayList();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT * FROM CUST_EMS_DRIVER";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            retList.Add(";" + (String) rdr["LASTNAME"] + ";" + (String) rdr["USER_ID"]);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while getDrivers");
        //        FileManager.LogException(ex);
        //    }
        //    retList.Sort();
        //    return retList;
        //}

        //public List<EMSEqui> searchEquipment(String aNummer, String aStandort, String aRichtung, String aEinbauort,
        //                                     String aBNV, String aLeerungsdienst)
        //{
        //    var retVal = new List<EMSEqui>();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT * FROM EQUI " +
        //                          "WHERE " +
        //                          "EQUI LIKE '%" + aNummer + "%' AND " +
        //                          "ATWRT_SORT LIKE '%" + aStandort + "%' AND " +
        //                          "ATWRT_RICH LIKE '%" + aRichtung + "%' AND " +
        //                          "ATWRT_EORT LIKE '%" + aEinbauort + "%' AND " +
        //                          "ATWRT_BNVT LIKE '%" + aBNV + "%' AND " +
        //                          "ATWRT_LERD LIKE '%" + aLeerungsdienst + "%'";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            var emsEqui = new EMSEqui();
        //            emsEqui.equnr = (String) rdr["EQUI"];
        //            emsEqui.standort = (String) rdr["ATWRT_SORT"];
        //            emsEqui.einbauort = (String) rdr["ATWRT_EORT"];
        //            emsEqui.richtung = (String) rdr["ATWRT_RICH"];
        //            emsEqui.bnv = (String) rdr["ATWRT_BNVT"];
        //            emsEqui.leerungsdienst = (String) rdr["ATWRT_LERD"];

        //            retVal.Add(emsEqui);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while searchEquipment");
        //        FileManager.LogException(ex);
        //    }
        //    //retVal.Sort();
        //    return retVal;
        //}

        //public ArrayList getSearchValues(String aColumn)
        //{
        //    var retList = new ArrayList();
        //    try
        //    {
        //        var conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT distinct " + aColumn + " FROM EQUI";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            retList.Add(rdr[aColumn]);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while getSearchValues");
        //        FileManager.LogException(ex);
        //    }
        //    retList.Sort();
        //    return retList;
        //}

        //public ArrayList getCodes(String aTyp, String aCodeGrpText, String aCodeGrp)
        //{
        //    var retList = new ArrayList();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT * FROM CODECATALOG " +
        //                          "WHERE CATALOG = '" + aTyp + "'";
        //        if (aCodeGrpText != "")
        //            cmd.CommandText = cmd.CommandText + " AND CODEGRP_TEXT = '" + aCodeGrpText + "'";
        //        if (aCodeGrp != "")
        //            cmd.CommandText = cmd.CommandText + " AND CODEGRP = '" + aCodeGrp + "'";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            if (aCodeGrp.Length == 0)
        //            {
        //                if ((!((String) rdr["CODEGRP_TEXT"]).Equals("Sonderarbeiten")))
        //                    retList.Add((String) rdr["CODEGRP_TEXT"] + ";" + (String) rdr["CODE_TEXT"]);
        //            }
        //            else
        //            {
        //                retList.Add((String) rdr["CODEGRP_TEXT"] + ";" + (String) rdr["CODE_TEXT"]);
        //            }
        //        }
        //        conn.Close();
        //        retList.Sort();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while getCodes");
        //        FileManager.LogException(ex);
        //    }
        //    retList.Sort();
        //    return retList;
        //}

        //public List<OrdOperation> GetOrderOperations(String date)
        //{
        //    var retVal = new List<OrdOperation>();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();

        //        String strDate = null;
        //        if (date != null)
        //            strDate = date.Substring(9, 4) + date.Substring(6, 2) + date.Substring(3, 2);

        //        // Get all db entries
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT ORDHEAD.AUFNR, ORDOPER.VORNR, ORDHEAD.KTEXT, " +
        //                          "ORDOPER.LTXA1, ORDOPER.LARNT, ORDHEAD.PRIOK, FLOC.PLTXT, NOTIHEAD.QMTXT, " +
        //                          "ORDHEAD.TPLNR, ORDOPER.FSAVD, ORDOPER.FSAVZ, " +
        //                          "ORDOPER.PERNR, ORDOPER.DAUNO, ORDOPER.DAUNE, ORDOPER.ARBEI, " +
        //                          "ORDOPER.ARBEH, ORDOPER.FSEDD, ORDOPER.FSEDZ, ORDOPER.ARBPL, ORDOPER.WERKS " +
        //                          "FROM ORDHEAD RIGHT OUTER JOIN " +
        //                          "ORDOPER ON ORDHEAD.AUFNR = ORDOPER.AUFNR LEFT OUTER JOIN " +
        //                          "FLOC ON ORDHEAD.TPLNR = FLOC.TPLNR LEFT OUTER JOIN " +
        //                          "NOTIHEAD ON ORDHEAD.QMNUM = NOTIHEAD.QMNUM ORDER BY ORDOPER.FSAVD, ORDOPER.FSAVZ";
        //        if (strDate != null)
        //            cmd.CommandText = cmd.CommandText + "WHERE ORDOPER.FSAVD = @STARTDATE";

        //        cmd.Parameters.AddWithValue("@STARTDATE", strDate);
        //        SqlCeDataReader rdr = cmd.ExecuteReader();

        //        while (rdr.Read())
        //        {
        //            var ordOperation = new OrdOperation();
        //            ordOperation.Aufnr = (String) rdr["AUFNR"];
        //            ordOperation.Vornr = (String) rdr["VORNR"];
        //            ordOperation.Ktext = (String) rdr["KTEXT"];
        //            ordOperation.Ltxa1 = (String) rdr["LTXA1"];
        //            ordOperation.Priok = (String) rdr["PRIOK"];
        //            if (rdr["LARNT"] is DBNull)
        //                ordOperation.Larnt = "";
        //            else
        //                ordOperation.Larnt = (String) rdr["LARNT"];

        //            if (rdr["PLTXT"] is DBNull)
        //                ordOperation.Pltxt = "";
        //            else
        //                ordOperation.Pltxt = (String) rdr["PLTXT"];
        //            if (rdr["QMTXT"] is DBNull)
        //                ordOperation.Qmtxt = "";
        //            else
        //                ordOperation.Qmtxt = (String) rdr["QMTXT"];
        //            ordOperation.Tplnr = (String) rdr["TPLNR"];
        //            DateTime test;
        //            DateTimeHelper.getDate(rdr["FSAVD"], out test);
        //            ordOperation.Fsavd = test;
        //            DateTimeHelper.getTime(rdr["FSAVD"], rdr["FSAVZ"], out test);
        //            ordOperation.Fsavz = test;
        //            /*
        //            ordOperation.fsavz = (String) rdr["FSAVZ"];
        //            ordOperation.fsavd = (String) rdr["FSAVD"];
        //             */

        //            ordOperation.Pernr = (String) rdr["PERNR"];
        //            ordOperation.Dauno = (String) rdr["DAUNO"];
        //            ordOperation.Daune = (String) rdr["DAUNE"];
        //            ordOperation.Arbei = (String) rdr["ARBEI"];
        //            ordOperation.Arbeh = (String) rdr["ARBEH"];
        //            DateTimeHelper.getDate(rdr["FSEDD"], out test);
        //            ordOperation.Fsedd = test;
        //            //ordOperation.fsedd = (String)rdr["FSEDD"];
        //            DateTimeHelper.getTime(rdr["FSEDD"], rdr["FSEDZ"], out test);
        //            ordOperation.Fsedz = test;
        //            /*
        //            ordOperation.fsedd = (String) rdr["FSEDD"];
        //            ordOperation.fsedz = (String) rdr["FSEDZ"];
        //             */
        //            ordOperation.Arbpl = (String) rdr["ARBPL"];
        //            ordOperation.Werks = (String) rdr["WERKS"];

        //            retVal.Add(ordOperation);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while GetOrderOperations");
        //        FileManager.LogException(ex);
        //    }
        //    return retVal;
        //}

        //public ArrayList getZustand()
        //{
        //    var retList = new ArrayList();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT * FROM CUST_EMS_ZUSTAND";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            retList.Add((String) rdr["ZUSTAND"] + ";" + (String) rdr["KTEXT"]);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while getZustand");
        //        FileManager.LogException(ex);
        //    }
        //    retList.Sort();
        //    return retList;
        //}

        //public ArrayList getActType()
        //{
        //    var retList = new ArrayList();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT * FROM CUST_EMS_ACTYPE";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            retList.Add((String) rdr["ACTTYP"] + ";" + (String) rdr["KTEXT"]);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while getActType");
        //        FileManager.LogException(ex);
        //    }
        //    retList.Sort();
        //    return retList;
        //}

        //public ArrayList getAutomat()
        //{
        //    var retList = new ArrayList();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT * FROM EQUI";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        String ListItem = "";
        //        while (rdr.Read())
        //        {
        //            if (!(rdr["EQUI"] is DBNull))
        //                ListItem = (String) rdr["EQUI"];
        //            ListItem += ";";
        //            if (!(rdr["EQART"] is DBNull))
        //                ListItem += (String) rdr["EQART"];
        //            retList.Add(ListItem);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while getAutomat");
        //        FileManager.LogException(ex);
        //    }
        //    retList.Sort();
        //    return retList;
        //}

        //public ArrayList getMeldender()
        //{
        //    var retList = new ArrayList();
        //    try
        //    {
        //        var conn = new SqlCeConnection();
        //        conn = new SqlCeConnection(AppConfig.DatabasePath);
        //        conn.Open();
        //        var cmd = new SqlCeCommand();
        //        cmd.Connection = conn;
        //        cmd.CommandText = "SELECT * FROM CUST_EMS_DRIVER";
        //        SqlCeDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            retList.Add(";" + (String) rdr["LASTNAME"]);
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        FileManager.LogMessage("Error while getMeldender");
        //        FileManager.LogException(ex);
        //    }
        //    retList.Sort();
        //    return retList;
        //}
    }
}