﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Material : BaseMaterial
    {
        public class MaterialSort : IComparer<Material>
        {
            private Boolean _desc = true;
            private String _compareField;

            public MaterialSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Material x, Material y)
            {
                switch (_compareField.ToUpper())
                {
                    case "MATNR":
                        if (_desc)
                            return x.Matnr.CompareTo(y.Matnr);
                        return y.Matnr.CompareTo(x.Matnr);
                    case "MEINS":
                        if (_desc)
                            return x.Meins.CompareTo(y.Meins);
                        return y.Meins.CompareTo(x.Meins);
                    case "MAKTX":
                        if (_desc)
                            return x.Maktx.CompareTo(y.Maktx);
                        return y.Maktx.CompareTo(x.Maktx);
                    default:
                        return (x).Matnr.CompareTo(y.Matnr);
                }
            }
        }
    }
}
