﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Equi : BaseEqui
    {
        public class EquiSort : IComparer<Equi>
        {
            private Boolean _desc = true;
            private String _compareField;

            public EquiSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Equi x, Equi y)
            {
                switch (_compareField.ToUpper())
                {
                    case "EQUI":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "ANSDT":
                        if (_desc)
                            return x.ANSDT.CompareTo(y.ANSDT);
                        return y.ANSDT.CompareTo(x.ANSDT);
                    case "BAUJJ":
                        if (_desc)
                            return x.BAUJJ.CompareTo(y.BAUJJ);
                        return y.BAUJJ.CompareTo(x.BAUJJ);
                    case "BAUMM":
                        if (_desc)
                            return x.BAUMM.CompareTo(y.BAUMM);
                        return y.BAUMM.CompareTo(x.BAUMM);
                    case "EQART":
                        if (_desc)
                            return x.EQART.CompareTo(y.EQART);
                        return y.EQART.CompareTo(x.EQART);
                    case "EQKTX":
                        if (_desc)
                            return x.EQKTX.CompareTo(y.EQKTX);
                        return y.EQKTX.CompareTo(x.EQKTX);
                    case "EQTYP":
                        if (_desc)
                            return x.EQTYP.CompareTo(y.EQTYP);
                        return y.EQTYP.CompareTo(x.EQTYP);
                    case "EQUNR":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "HEQNR":
                        if (_desc)
                            return x.HEQNR.CompareTo(y.HEQNR);
                        return y.HEQNR.CompareTo(x.HEQNR);
                    case "HEQUI":
                        if (_desc)
                            return x.HEQUI.CompareTo(y.HEQUI);
                        return y.HEQUI.CompareTo(x.HEQUI);
                    case "HERLD":
                        if (_desc)
                            return x.HERLD.CompareTo(y.HERLD);
                        return y.HERLD.CompareTo(x.HERLD);
                    case "HERST":
                        if (_desc)
                            return x.HERST.CompareTo(y.HERST);
                        return y.HERST.CompareTo(x.HERST);
                    case "INGRP":
                        if (_desc)
                            return x.INGRP.CompareTo(y.INGRP);
                        return y.INGRP.CompareTo(x.INGRP);
                    case "INVNR":
                        if (_desc)
                            return x.INVNR.CompareTo(y.INVNR);
                        return y.INVNR.CompareTo(x.INVNR);
                    case "IWERK":
                        if (_desc)
                            return x.IWERK.CompareTo(y.IWERK);
                        return y.IWERK.CompareTo(x.IWERK);
                    case "KUND1":
                        if (_desc)
                            return x.KUND1.CompareTo(y.KUND1);
                        return y.KUND1.CompareTo(x.KUND1);
                    case "KUND2":
                        if (_desc)
                            return x.KUND2.CompareTo(y.KUND2);
                        return y.KUND2.CompareTo(x.KUND2);
                    case "KUND3":
                        if (_desc)
                            return x.KUND3.CompareTo(y.KUND3);
                        return y.KUND3.CompareTo(x.KUND3);
                    case "LAGER":
                        if (_desc)
                            return x.LAGER.CompareTo(y.LAGER);
                        return y.LAGER.CompareTo(x.LAGER);
                    case "MATNR":
                        if (_desc)
                            return x.MATNR.CompareTo(y.MATNR);
                        return y.MATNR.CompareTo(x.MATNR);
                    case "RBNR":
                        if (_desc)
                            return x.RBNR.CompareTo(y.RBNR);
                        return y.RBNR.CompareTo(x.RBNR);
                    case "SERGE":
                        if (_desc)
                            return x.SERGE.CompareTo(y.SERGE);
                        return y.SERGE.CompareTo(x.SERGE);
                    case "SERNR":
                        if (_desc)
                            return x.SERNR.CompareTo(y.SERNR);
                        return y.SERNR.CompareTo(x.SERNR);
                    case "SPART":
                        if (_desc)
                            return x.SPART.CompareTo(y.SPART);
                        return y.SPART.CompareTo(x.SPART);
                    case "SPRAS":
                        if (_desc)
                            return x.SPRAS.CompareTo(y.SPRAS);
                        return y.SPRAS.CompareTo(x.SPRAS);
                    case "SUBMT":
                        if (_desc)
                            return x.SUBMT.CompareTo(y.SUBMT);
                        return y.SUBMT.CompareTo(x.SUBMT);
                    case "TPLNR":
                        if (_desc)
                            return x.TPLNR.CompareTo(y.TPLNR);
                        return y.TPLNR.CompareTo(x.TPLNR);
                    case "TYPBZ":
                        if (_desc)
                            return x.TYPBZ.CompareTo(y.TYPBZ);
                        return y.TYPBZ.CompareTo(x.TYPBZ);
                    case "VKORG":
                        if (_desc)
                            return x.VKORG.CompareTo(y.VKORG);
                        return y.VKORG.CompareTo(x.VKORG);
                    case "VTWEG":
                        if (_desc)
                            return x.VTWEG.CompareTo(y.VTWEG);
                        return y.VTWEG.CompareTo(x.VTWEG);
                    case "WERK":
                        if (_desc)
                            return x.WERK.CompareTo(y.WERK);
                        return y.WERK.CompareTo(x.WERK);
                    case "TIDNR":
                        if (_desc)
                            return x.Tidnr.CompareTo(y.Tidnr);
                        return y.Tidnr.CompareTo(x.Tidnr);
                    case "MSGRP":
                        if (_desc)
                            return x.Msgrp.CompareTo(y.Msgrp);
                        return y.Msgrp.CompareTo(x.Msgrp);
                    default:
                        return (x).EQUNR.CompareTo(y.EQUNR);
                }
            }
        }
    }
}