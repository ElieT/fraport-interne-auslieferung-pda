﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Notif : BaseNotif
    {
        public Notif() : base()
        {

        }



        public Notif(FuncLoc _funcLoc) : base(_funcLoc)
        {
            Tplnr = _funcLoc.Tplnr;
            if (!string.IsNullOrEmpty(_funcLoc.Gewrk) && !string.IsNullOrEmpty(_funcLoc.Wergw))
            {
                Gewrk = _funcLoc.Gewrk;
                Swerk = _funcLoc.Wergw;
            }
        }

        public Notif(Equi _equi) : base(_equi)
        {
            Equnr = _equi.Equnr;
            if (!string.IsNullOrEmpty(_equi.Gewrk) && !string.IsNullOrEmpty(_equi.Wergw))
            {
                Gewrk = _equi.Gewrk;
                Swerk = _equi.Wergw;
            }
        }

        public string GetValueByString(String field)
        {
            var retValue = "";

            switch (field.ToUpper())
            {
                case "ABNUM":
                    retValue = ABNUM;
                    break;
                case "ARTPR":
                    retValue = ARTPR;
                    break;
                case "AUFNR":
                    retValue = AUFNR;
                    break;
                case "BAUTL":
                    retValue = BAUTL;
                    break;
                case "EQKTX":
                    retValue = EQKTX;
                    break;
                case "EQUNR":
                    retValue = EQUNR;
                    break;
                case "INGRP":
                    retValue = INGRP;
                    break;
                case "IWERK":
                    retValue = IWERK;
                    break;
                case "PLTXT":
                    retValue = PLTXT;
                    break;
                case "PRIOK":
                    retValue = PRIOK;
                    break;
                case "PRUEFLOS":
                    retValue = PRUEFLOS;
                    break;
                case "QMART":
                    retValue = QMART;
                    break;
                case "QMNUM":
                    retValue = QMNUM;
                    break;
                case "QMTXT":
                    retValue = QMTXT;
                    break;
                case "TPLNR":
                    retValue = TPLNR;
                    break;
                case "EQFNR":
                    retValue = EQFNR;
                    break;
                case "MSGRP":
                    retValue = MSGRP;
                    break;
                case "WAPOS":
                    retValue = WAPOS;
                    break;
                case "WARPL":
                    retValue = WARPL;
                    break;
                case "STRMN":
                    retValue = STRMN.ToString();
                    break;
                case "STORT":
                    retValue = Stort.ToString();
                    break;
                case "TIDNR":
                    retValue = Tidnr;
                    break;
            }
            return retValue;
        }

        public class NotifSort : IComparer<Notif>
        {
            private Boolean _desc = true;
            private String _compareField;

            public NotifSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Notif x, Notif y)
            {
                switch (_compareField.ToUpper())
                {
                    case "ABNUM":
                        if (_desc)
                            return x.ABNUM.CompareTo(y.ABNUM);
                         return y.ABNUM.CompareTo(x.ABNUM);
                    case "ARTPR":
                        if (_desc)
                            return x.ARTPR.CompareTo(y.ARTPR);
                         return y.ARTPR.CompareTo(x.ARTPR);
                    case "AUFNR":
                        if (_desc)
                            return x.AUFNR.CompareTo(y.AUFNR);
                         return y.AUFNR.CompareTo(x.AUFNR);
                    case "BAUTL":
                        if (_desc)
                            return x.BAUTL.CompareTo(y.BAUTL);
                         return y.BAUTL.CompareTo(x.BAUTL);
                    case "EQKTX":
                        if (_desc)
                            return x.EQKTX.CompareTo(y.EQKTX);
                         return y.EQKTX.CompareTo(x.EQKTX);
                    case "EQUNR":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "INGRP":
                        if (_desc)
                            return x.INGRP.CompareTo(y.INGRP);
                         return y.INGRP.CompareTo(x.INGRP);
                    case "IWERK":
                        if (_desc)
                            return x.IWERK.CompareTo(y.IWERK);
                         return y.IWERK.CompareTo(x.IWERK);
                    case "PLTXT":
                        if (_desc)
                            return x.PLTXT.CompareTo(y.PLTXT);
                         return y.PLTXT.CompareTo(x.PLTXT);
                    case "PRIOK":
                        if (_desc)
                            return x.PRIOK.CompareTo(y.PRIOK);
                         return y.PRIOK.CompareTo(x.PRIOK);
                    case "PRUEFLOS":
                        if (_desc)
                            return x.PRIOK.CompareTo(y.PRIOK);
                         return y.PRIOK.CompareTo(x.PRIOK);
                    case "QMART":
                        if (_desc)
                            return x.QMART.CompareTo(y.QMART);
                         return y.QMART.CompareTo(x.QMART);
                    case "QMNUM":
                        if (_desc)
                            return x.QMNUM.CompareTo(y.QMNUM);
                         return y.QMNUM.CompareTo(x.QMNUM);
                    case "QMTXT":
                        if (_desc)
                            return x.QMTXT.CompareTo(y.QMTXT);
                         return y.QMTXT.CompareTo(x.QMTXT);
                    case "TPLNR":
                        if (_desc)
                            return x.TPLNR.CompareTo(y.TPLNR);
                        return y.TPLNR.CompareTo(x.TPLNR);
                    case "EQFNR":
                        if (_desc)
                            return x.EQFNR.CompareTo(y.EQFNR);
                        return y.EQFNR.CompareTo(x.EQFNR);
                    case "MSGRP":
                        if (_desc)
                            return x.MSGRP.CompareTo(y.MSGRP);
                        return y.MSGRP.CompareTo(x.MSGRP);
                    case "WAPOS":
                        if (_desc)
                            return x.WAPOS.CompareTo(y.WAPOS);
                        return y.WAPOS.CompareTo(x.WAPOS);
                    case "WARPL":
                        if (_desc)
                            return x.WARPL.CompareTo(y.WARPL);
                        return y.WARPL.CompareTo(x.WARPL);
                    case "STORT":
                        if (_desc)
                            return x.STORT.CompareTo(y.STORT);
                        return y.STORT.CompareTo(x.STORT);
                    case "TIDNR":
                        if (_desc)
                            return x.TIDNR.CompareTo(y.TIDNR);
                        return y.TIDNR.CompareTo(x.TIDNR);
                    default:
                         return (x).QMNUM.CompareTo(y.QMNUM);
                }
            }
        }
    }

    public class NotifItem : BaseNotifItem
    {

        public NotifItem() : base() { }
        protected String AXOENH = "";
        public NotifItem(Notif notif) : base(notif)
        {
            QMNUM = notif.Qmnum;
        }
        public string AxoEnh
        {
            get { return AXOENH; }
            set { AXOENH = value; }
        }
    }

    public class NotifActivity : BaseNotifActivity
    {
        public NotifActivity() : base() { }
        public NotifActivity(Notif notif) : base(notif)
        {
        }
    }

    public class NotifCause : BaseNotifCause
    {
        public NotifCause() : base() { }
        public NotifCause(Notif notif) : base(notif)
        {
        }
    }

    public class NotifTask : BaseNotifTask
    {
        public NotifTask() : base() { }
        public NotifTask(Notif notif) : base(notif)
        {
        }
    }
}