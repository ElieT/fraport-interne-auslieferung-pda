﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class TimeConf : BaseTimeConf
    {
        public TimeConf()
            : base()
        {
            TConfWork = "";
            TConfTravel = "";
        }

        public TimeConf(OrdOperation operation)
            : base(operation)
        {
            TConfWork = "";
            TConfTravel = "";
        }


        public class TimeConfSort : IComparer<TimeConf>
        {
            private Boolean _desc = true;
            private String _compareField;

            public TimeConfSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(TimeConf x, TimeConf y)
            {
                switch (_compareField.ToLower())
                {
                    case "Aufnr":
                        if (_desc)
                            return x.Aufnr.CompareTo(y.Aufnr);
                        return y.Aufnr.CompareTo(x.Aufnr);
                    case "Vornr":
                        if (_desc)
                            return x.Vornr.CompareTo(y.Vornr);
                        return y.Vornr.CompareTo(x.Vornr);
                    case "Arbpl":
                        if (_desc)
                            return x.Arbpl.CompareTo(y.Arbpl);
                        return y.Arbpl.CompareTo(x.Arbpl);
                    case "Ismne":
                        if (_desc)
                            return x.Ismne.CompareTo(y.Ismne);
                        return y.Ismne.CompareTo(x.Ismne);
                    case "Idaue":
                        if (_desc)
                            return x.Idaue.CompareTo(y.Idaue);
                        return y.Idaue.CompareTo(x.Idaue);
                    case "aueru":
                        if (_desc)
                            return x.aueru.CompareTo(y.aueru);
                        return y.aueru.CompareTo(x.aueru);
                    case "ausor":
                        if (_desc)
                            return x.ausor.CompareTo(y.ausor);
                        return y.ausor.CompareTo(x.ausor);
                    case "bemot":
                        if (_desc)
                            return x.bemot.CompareTo(y.bemot);
                        return y.bemot.CompareTo(x.bemot);
                    case "ernam":
                        if (_desc)
                            return x.ernam.CompareTo(y.ernam);
                        return y.ernam.CompareTo(x.ernam);
                    case "ersda":
                        if (_desc)
                            return x.ersda.CompareTo(y.ersda);
                        return y.ersda.CompareTo(x.ersda);
                    case "idaur":
                        if (_desc)
                            return x.idaur.CompareTo(y.idaur);
                        return y.idaur.CompareTo(x.idaur);
                    case "iedd":
                        if (_desc)
                            return x.iedd.CompareTo(y.iedd);
                        return y.iedd.CompareTo(x.iedd);
                    case "iedz":
                        if (_desc)
                            return x.iedz.CompareTo(y.iedz);
                        return y.iedz.CompareTo(x.iedz);
                    case "isdd":
                        if (_desc)
                            return x.isdd.CompareTo(y.isdd);
                        return y.isdd.CompareTo(x.isdd);
                    case "isdz":
                        if (_desc)
                            return x.isdz.CompareTo(y.isdz);
                        return y.isdz.CompareTo(x.isdz);
                    case "ismnw":
                        if (_desc)
                            return x.ismnw.CompareTo(y.ismnw);
                        return y.ismnw.CompareTo(x.ismnw);
                    case "learr":
                        if (_desc)
                            return x.learr.CompareTo(y.learr);
                        return y.learr.CompareTo(x.learr);
                    case "ltxa1":
                        if (_desc)
                            return x.ltxa1.CompareTo(y.ltxa1);
                        return y.ltxa1.CompareTo(x.ltxa1);
                    case "ofmne":
                        if (_desc)
                            return x.ofmne.CompareTo(y.ofmne);
                        return y.ofmne.CompareTo(x.ofmne);
                    case "ofmnw":
                        if (_desc)
                            return x.ofmnw.CompareTo(y.ofmnw);
                        return y.ofmnw.CompareTo(x.ofmnw);
                    case "pernr":
                        if (_desc)
                            return x.pernr.CompareTo(y.pernr);
                        return y.pernr.CompareTo(x.pernr);
                    case "rmzhl":
                        if (_desc)
                            return x.rmzhl.CompareTo(y.rmzhl);
                        return y.rmzhl.CompareTo(x.rmzhl);
                    case "split":
                        if (_desc)
                            return x.split.CompareTo(y.split);
                        return y.split.CompareTo(x.split);
                    case "txtsp":
                        if (_desc)
                            return x.txtsp.CompareTo(y.txtsp);
                        return y.txtsp.CompareTo(x.txtsp);
                    case "werks":
                        if (_desc)
                            return x.werks.CompareTo(y.werks);
                        return y.werks.CompareTo(x.werks);
                    case "UpdFlag":
                        if (_desc)
                            return x.UpdFlag.CompareTo(y.UpdFlag);
                        return y.UpdFlag.CompareTo(x.UpdFlag);
                    default:
                        return (x).Aufnr.CompareTo(y.Aufnr);
                }
            }
        }

        public string TConfWork { get; set; }
        public string TConfTravel { get; set; }
    }
}