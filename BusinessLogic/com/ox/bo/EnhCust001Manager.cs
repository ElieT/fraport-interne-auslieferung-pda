﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public static class EnhCust001Manager
    {
        public static List<EnhCust001> GetEnhCust001Data()
        {
            var list = new List<EnhCust001>();
            var sqlStmt = "SELECT * FROM C_ZFRAPORT_ENH_CUST001";
            var cmd = new SQLiteCommand(sqlStmt);
            var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            foreach(var item in records)
            {
                var enh = new EnhCust001();
                enh.Code = item["CODE"];
                enh.Codegruppe = item["CODEGRUPPE"];
                enh.JStatus = item["J_STATUS"];
                enh.Katalogart = item["KATALOGART"];
                enh.ProcessFlag = item["PROCESS_FLAG"];
                enh.Stsma = item["STSMA"];
                list.Add(enh);
            }
            return list;
        }

        public static int GetCountCodegrp(String codegrp)
        {
            var count = -1;
            try
            {
                var sqlStmt = "SELECT * FROM C_ZFRAPORT_ENH_CUST001 WHERE CODEGRUPPE=@CODEGRUPPE";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@CODEGRUPPE", codegrp);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count > 0)
                    return records.Count;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return count;
        }
    }
}
