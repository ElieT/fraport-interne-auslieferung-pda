﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.baseBo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class CustCodesManager : BaseCustCodesManager
    {

        public static new List<CustCodes> GetCustCodes(String rbnr, String katalogart, Boolean inTransaction)
        {
            var retList = new List<CustCodes>();
            var retListSorted = new List<CustCodes>();
            try
            {

                var oldCodeGruppe = "";
                var sqlStmt = "SELECT CODEGRUPPE, KATALOGART, CODE, CODEGR_KURZTEXT, KURZTEXT FROM G_CUSTCODES " +
                                  "WHERE KATALOGART = @KATALOGART AND CODEGRUPPE IN " +
                                  "(SELECT QCODEGRP FROM G_CUSTBERS WHERE RBNR = @RBNR AND QKATART = @KATALOGART) ORDER BY CODEGRUPPE";

                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@RBNR", rbnr);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    if (!oldCodeGruppe.Equals(rdr["CODEGRUPPE"]))
                    {
                        var custcodes = new CustCodes();
                        custcodes.Katalogart = rdr["KATALOGART"];
                        custcodes.Codegruppe = rdr["CODEGRUPPE"];
                        custcodes.Code = rdr["CODE"];
                        custcodes.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                        custcodes.Kurztext = rdr["KURZTEXT"];
                        oldCodeGruppe = custcodes.Codegruppe;
                        retList.Add(custcodes);
                    }
                }
                //retList.Sort();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }

        /// <summary>
        /// Returns CustCodes from the Database, filtered by the provided catalog type
        /// </summary>
        /// <param name="katalogart">The catalog type by which the CustCodes should be filtered</param>
        /// <returns></returns>
        public static new List<CustCodes> GetFilteredCustCodes(String rbnr, String katalogart, Boolean inTransaction)
        {
            var retList = new List<CustCodes>();
            try
            {

                String oldCodeGruppe = "";
                String sqlStmt =
                    "SELECT G_CUSTCODES.CODEGRUPPE, G_CUSTCODES.KATALOGART, G_CUSTCODES.CODE, G_CUSTCODES.CODEGR_KURZTEXT, " +
                    "G_CUSTCODES.KURZTEXT, C_ZFRAPORT_ENH_CUST001.PROCESS_FLAG " +
                    "FROM G_CUSTCODES " +
                    "INNER JOIN C_ZFRAPORT_ENH_CUST001 ON " +
                    "G_CUSTCODES.CODEGRUPPE = C_ZFRAPORT_ENH_CUST001.CODEGRUPPE AND " +
                    "G_CUSTCODES.CODE = C_ZFRAPORT_ENH_CUST001.CODE AND " +
                    "G_CUSTCODES.KATALOGART = @KATALOGART " +
                    "WHERE G_CUSTCODES.CODEGRUPPE IN " +
                       "(SELECT QCODEGRP FROM G_CUSTBERS WHERE RBNR = @RBNR AND QKATART = @KATALOGART) ORDER BY G_CUSTCODES.CODEGRUPPE";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@RBNR", rbnr);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    if (!oldCodeGruppe.Equals(rdr["CODEGRUPPE"]))
                    {
                        var custcodes = new CustCodes();
                        custcodes.Katalogart = rdr["KATALOGART"];
                        custcodes.Codegruppe = rdr["CODEGRUPPE"];
                        custcodes.Code = rdr["CODE"];
                        custcodes.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                        custcodes.Kurztext = rdr["KURZTEXT"];
                        oldCodeGruppe = custcodes.Codegruppe;
                        retList.Add(custcodes);
                    }
                }
            }
            catch (Exception ex)
            {
                //FileManager.LogMessage("Error while getCodes");
                FileManager.LogException(ex);
            }
            return retList;
        }

        /// <summary>
        /// Checks if the provided item has a codegroup/code-combination from the ENH_CUST001-table
        /// Returns the processflag if that is the case or an empty string
        /// </summary>
        /// <param name="rbnr"></param>
        /// <param name="item"></param>
        /// <param name="inTransaction"></param>
        /// <returns>processflag</returns>
        public static String GetItemProcessFlag(NotifItem item, Boolean inTransaction)
        {
            var sqlStmt = "SELECT PROCESS_FLAG FROM C_ZFRAPORT_ENH_CUST001 WHERE " +
                          "C_ZFRAPORT_ENH_CUST001.CODEGRUPPE = @CODEGROUP AND " +
                          "C_ZFRAPORT_ENH_CUST001.CODE = @CODE AND " +
                          "C_ZFRAPORT_ENH_CUST001.KATALOGART = @KATALOGART ";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@CODEGROUP", item.Fegrp);
            cmd.Parameters.AddWithValue("@CODE", item.Fecod);
            cmd.Parameters.AddWithValue("@KATALOGART", item.Fekat);

            List<Dictionary<string, string>> records;
            if (inTransaction)
                records = OxDbManager.GetInstance().FeedTransaction(cmd);
            else
                records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            if (records.Count == 1)
                return records[0]["PROCESS_FLAG"];
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="inTransaction"></param>
        /// <returns></returns>
        public static List<CustCodes> GetCustCodesForNoProcessing(String katalogArt, String process_flag, Boolean inTransaction)
        {

            var retList = new List<CustCodes>();
            try
            {
                var oldCodeGruppe = "";
                var sqlStmt =
                    "SELECT G_CUSTCODES.CODEGRUPPE, G_CUSTCODES.KATALOGART, G_CUSTCODES.CODE, G_CUSTCODES.CODEGR_KURZTEXT, " +
                    "G_CUSTCODES.KURZTEXT " +
                    "FROM G_CUSTCODES " +
                    "INNER JOIN C_ZFRAPORT_ENH_CUST001 ON " +
                    "G_CUSTCODES.CODEGRUPPE = C_ZFRAPORT_ENH_CUST001.CODEGRUPPE AND " +
                    "G_CUSTCODES.CODE = C_ZFRAPORT_ENH_CUST001.CODE AND " +
                    "G_CUSTCODES.KATALOGART = @KATALOGART  AND " +
                    "C_ZFRAPORT_ENH_CUST001.PROCESS_FLAG = @PROCESS_FLAG ";
                //+
                //    "WHERE G_CUSTCODES.CODEGRUPPE IN " +
                //    "(SELECT QCODEGRP FROM G_CUSTBERS WHERE RBNR = @RBNR AND QKATART = @KATALOGART) ORDER BY G_CUSTCODES.CODEGRUPPE";

                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogArt);
                cmd.Parameters.AddWithValue("@PROCESS_FLAG", process_flag);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    if (!oldCodeGruppe.Equals(rdr["CODEGRUPPE"]))
                    {
                        var custcodes = new CustCodes();
                        custcodes.Katalogart = rdr["KATALOGART"];
                        custcodes.Codegruppe = rdr["CODEGRUPPE"];
                        custcodes.Code = rdr["CODE"];
                        custcodes.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                        custcodes.Kurztext = rdr["KURZTEXT"];
                        oldCodeGruppe = custcodes.Codegruppe;
                        retList.Add(custcodes);
                    }
                }
            }
            catch (Exception ex)
            {
                //FileManager.LogMessage("Error while getCodes");
                FileManager.LogException(ex);
            }
            return retList;
        }

        public static new List<CustCodes> GetUnfilteredCustCodes(String rbnr, String katalogart, Boolean inTransaction)
        {
            var retList = new List<CustCodes>();
            try
            {
                String oldCodeGruppe = "";
                String sqlStmt =
                    "SELECT G_CUSTCODES.CODEGRUPPE, G_CUSTCODES.KATALOGART, G_CUSTCODES.CODE, G_CUSTCODES.CODEGR_KURZTEXT, " +
                    "G_CUSTCODES.KURZTEXT " +
                    "FROM G_CUSTCODES " +
                    "WHERE CODEGRUPPE NOT IN (SELECT CODEGRUPPE FROM C_ZFRAPORT_ENH_CUST001) AND KATALOGART = @KATALOGART " +
                    "AND G_CUSTCODES.CODEGRUPPE IN " +
                       "(SELECT QCODEGRP FROM G_CUSTBERS WHERE RBNR = @RBNR AND QKATART = @KATALOGART) ORDER BY G_CUSTCODES.CODEGRUPPE";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@RBNR", rbnr);

                List<Dictionary<String, String>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    if (!oldCodeGruppe.Equals(rdr["CODEGRUPPE"]))
                    {
                        var custcodes = new CustCodes();
                        custcodes.Katalogart = rdr["KATALOGART"];
                        custcodes.Codegruppe = rdr["CODEGRUPPE"];
                        custcodes.Code = rdr["CODE"];
                        custcodes.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                        custcodes.Kurztext = rdr["KURZTEXT"];
                        oldCodeGruppe = custcodes.Codegruppe;
                        retList.Add(custcodes);
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }

        public static new List<CustCodes> GetCustCodes(String rbnr, String katalogart, String codegrp, Boolean inTransaction)
        {
            var retList = new List<CustCodes>();


            try
            {
                String sqlStmt = "SELECT  CODEGRUPPE, KATALOGART, CODE, CODEGR_KURZTEXT, KURZTEXT  FROM G_CUSTCODES " +
                                  "WHERE KATALOGART = @KATALOGART " +
                                  "AND CODEGRUPPE = @CODEGRP";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@KATALOGART", katalogart);
                cmd.Parameters.AddWithValue("@CODEGRP", codegrp);

                List<Dictionary<String, String>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var custcodes = new CustCodes();
                    custcodes.Katalogart = rdr["KATALOGART"];
                    custcodes.Codegruppe = rdr["CODEGRUPPE"];
                    custcodes.Code = rdr["CODE"];
                    custcodes.CodegrKurztext = rdr["CODEGR_KURZTEXT"];
                    custcodes.Kurztext = rdr["KURZTEXT"];
                    retList.Add(custcodes);
                }
                //retList.Sort();
            }
            catch (Exception ex)
            {
                //FileManager.LogMessage("Error while getCodes");
                FileManager.LogException(ex);
            }
            return retList;
        }
    }
}