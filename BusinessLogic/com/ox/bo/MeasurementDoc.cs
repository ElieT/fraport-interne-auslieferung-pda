﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class MeasurementDoc : BaseMeasurementDoc
    {
       
        public MeasurementDoc(): base()
        {
        }

        public MeasurementDoc(MeasurementPoint point) : base(point)
        {
        }

        public class MeasurementDocSort : IComparer<MeasurementDoc>
        {
            private Boolean _desc = true;
            private String _compareField;

            public MeasurementDocSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(MeasurementDoc x, MeasurementDoc y)
            {
                switch (_compareField.ToLower())
                {
                    case "POINT":
                        if (_desc)
                            return x.Point.CompareTo(y.Point);
                        return y.Point.CompareTo(x.Point);
                    case "MDOCM":
                        if (_desc)
                            return x.MDOCM.CompareTo(y.MDOCM);
                        return y.MDOCM.CompareTo(x.MDOCM);
                    case "MDTXT":
                        if (_desc)
                            return x.MDTXT.CompareTo(y.MDTXT);
                        return y.MDTXT.CompareTo(x.MDTXT);
                    case "READR":
                        if (_desc)
                            return x.READR.CompareTo(y.READR);
                        return y.READR.CompareTo(x.READR);
                    case "RECDC":
                        if (_desc)
                            return x.RECDC.CompareTo(y.RECDC);
                        return y.RECDC.CompareTo(x.RECDC);
                    case "UNITR":
                        if (_desc)
                            return x.UNITR.CompareTo(y.UNITR);
                        return y.UNITR.CompareTo(x.UNITR);
                    case "UPDFLAG":
                        if (_desc)
                            return x.UPDFLAG.CompareTo(y.UPDFLAG);
                        return y.UPDFLAG.CompareTo(x.UPDFLAG);
                    case "IDATE":
                        if (_desc)
                            return x.IDATE.CompareTo(y.IDATE);
                        return y.IDATE.CompareTo(x.IDATE);
                    case "ITIME":
                        if (_desc)
                            return x.ITIME.CompareTo(y.ITIME);
                        return y.ITIME.CompareTo(x.ITIME);
                    default:
                        return (x).Point.CompareTo(y.Point);
                }
            }
        }
    }
}
