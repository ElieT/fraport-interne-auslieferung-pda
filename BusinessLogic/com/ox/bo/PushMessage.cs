﻿using System;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class PushMessage : BasePushMessage
    {

        public PushMessage(string message, DateTime datetime) : base(message, datetime)
        {
        }

        public PushMessage() : base()
        {
        }
    }
}
