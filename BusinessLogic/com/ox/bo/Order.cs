﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Order : BaseOrder
    {
        public Order() : base()
        {

        }

        public Order(FuncLoc funcLoc) : base(funcLoc)
        {
            Tplnr = funcLoc.Tplnr;
            if (!string.IsNullOrEmpty(funcLoc.Gewrk) && !string.IsNullOrEmpty(funcLoc.Wergw))
            {
                Vaplz = funcLoc.Gewrk;
                Wawrk = funcLoc.Wergw;
            }
        }

        public Order(Equi equi) : base(equi)
        {
            Equnr = equi.Equnr;
            if (!string.IsNullOrEmpty(equi.Gewrk) && !string.IsNullOrEmpty(equi.Wergw))
            {
                Vaplz = equi.Gewrk;
                Wawrk = equi.Wergw;
            }
        }

        #region IComparable Members

        //Sorts the List according to the Values set in setSortMode()
        public class SortOrder : IComparer<Order>
        {
            public int Compare(Order obj1, Order obj2)
            {
                Order emp1 = obj1;
                Order emp2 = obj2;
                int returnVal = 0;

                //definition of Variables to compare by direction
                if (_desc)
                {
                    emp1 = obj1;
                    emp2 = obj2;
                }
                else
                {
                    emp1 = obj2;
                    emp2 = obj1;
                }

                //Sets which Fields in the List will be compared
                switch (_sortBy)
                {
                    case "Gstrp":
                        if (emp1.Gstrp > emp2.Gstrp)
                            returnVal = (-1);
                        else if (emp1.Gstrp < emp2.Gstrp)
                            returnVal = (1);
                        else
                            returnVal = (0);
                        break;
                    case "auart":
                        returnVal = (String.Compare(emp1.auart, emp2.auart));
                        break;
                    case "aufnr":
                        returnVal = (String.Compare(emp1.aufnr, emp2.aufnr));
                        break;
                    case "Ktext":
                        returnVal = (String.Compare(emp1.Ktext, emp2.Ktext));
                        break;
                    case "EquiEqfnr":
                        returnVal = (String.Compare(emp1.EquiEqfnr, emp2.EquiEqfnr));
                        break;
                    case "FlocEqfnr":
                        returnVal = (String.Compare(emp1.FlocEqfnr, emp2.FlocEqfnr));
                        break;
                    case "Raumnr":
                        returnVal = (String.Compare(emp1.Raumnr, emp2.Raumnr));
                        break;
                    case "Stort":
                        returnVal = (String.Compare(emp1.Stort, emp2.Stort));
                        break;
                    case "Tplnr":
                        returnVal = (String.Compare(emp1.Tplnr, emp2.Tplnr));
                        break;
                    case "Pltxt":
                        returnVal = (String.Compare(emp1.Pltxt, emp2.Pltxt));
                        break;
                    case "DateSort":
                        if (emp1.DateSort > emp2.DateSort)
                            returnVal = -1;
                        else if (emp1.DateSort < emp2.DateSort)
                            returnVal = 1;
                        else
                            returnVal = 0;
                        break;
                }
                return returnVal;
            }
        }
        #endregion

        public new string GetValueByString(String field)
        {
            var retValue = "";
            if (field == null)
                return retValue;
            switch (field.ToLower())
            {
                case "auart":
                    retValue = auart;
                    break;
                case "aufnr":
                    retValue = aufnr;
                    break;
                case "bautl":
                    retValue = bautl;
                    break;
                case "eqktx":
                    retValue = eqktx;
                    break;
                case "gewrk":
                    retValue = gewrk;
                    break;
                case "equnr":
                    retValue = equnr;
                    break;
                case "ingrp":
                    retValue = ingrp;
                    break;
                case "iwerk":
                    retValue = iwerk;
                    break;
                case "ktext":
                    retValue = ktext;
                    break;
                case "kunum":
                    retValue = kunum;
                    break;
                case "pltxt":
                    retValue = pltxt;
                    break;
                case "tplnr":
                    retValue = tplnr;
                    break;
                case "tidnr":
                    retValue = TIDNR;
                    break;
                case "stort":
                    retValue = stort;
                    break;
                case "raumnr":
                    retValue = raumnr;
                    break;
                case "mobilekey":
                    retValue = mobileKey;
                    break;
                case "datesort":
                    retValue = DateSort.ToString("yyyyMMdd HHmmss");
                    break;
                case "gstrp":
                    if (OrderOperations[0].Fstau != null && OrderOperations[0].Fstau != DateTime.MinValue)
                    {
                        retValue = OrderOperations[0].Fstau.ToString("dd.MM.yyyy") + " " +
                                   OrderOperations[0].Fstau.ToString("HH:mm");
                    }
                    else
                    {
                        retValue = OrderOperations[0].Fsavz.ToString("dd.MM.yyyy") + " " +
                                   OrderOperations[0].Fsavz.ToString("HH:mm");
                    }
                    break;
                case "equieqfnr":
                    retValue = equi_eqfnr;
                    break;
                case "status":
                    if (ObjectStatus.Count > 0)
                        retValue = ObjectStatus[0].UserStatusCode;
                    else
                        retValue = "";
                    break;
            }
            return retValue;
        }

        public class OrderSort : IComparer<Order>
        {
            private Boolean _desc = true;
            private String _compareField;

            public OrderSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(Order x, Order y)
            {
                switch (_compareField.ToLower())
                {
                    case "auart":
                        if (_desc)
                            return x.Auart.CompareTo(y.Auart);
                        return y.Auart.CompareTo(x.Auart);
                    case "aufnr":
                        if (_desc)
                            return (x).Aufnr.CompareTo(y.Aufnr);
                        return (y).Aufnr.CompareTo(x.Aufnr);
                    case "bautl":
                        if (_desc)
                            return (x).Bautl.CompareTo(y.Bautl);
                        return (y).Bautl.CompareTo(x.Bautl);
                    case "eqktx":
                        if (_desc)
                            return (x).Eqktx.CompareTo(y.Eqktx);
                        return (y).Eqktx.CompareTo(x.Eqktx);
                    case "gewrk":
                        if (_desc)
                            return (x).Gewrk.CompareTo(y.Gewrk);
                        return (y).Gewrk.CompareTo(x.Gewrk);
                    case "equnr":
                        if (_desc)
                            return (x).Equnr.CompareTo(y.Equnr);
                        return (y).Equnr.CompareTo(x.Equnr);
                    case "equieqfnr":
                        if (_desc)
                            return (x).EquiEqfnr.CompareTo(y.EquiEqfnr);
                        return (y).EquiEqfnr.CompareTo(x.EquiEqfnr);
                    case "ingrp":
                        if (_desc)
                            return (x).Ingrp.CompareTo(y.Ingrp);
                        return (y).Ingrp.CompareTo(x.Ingrp);
                    case "iwerk":
                        if (_desc)
                            return (x).Iwerk.CompareTo(y.Iwerk);
                        return (y).Iwerk.CompareTo(x.Iwerk);
                    case "ktext":
                        if (_desc)
                            return (x).Ktext.CompareTo(y.Ktext);
                        return (y).Ktext.CompareTo(x.Ktext);
                    case "kunum":
                        if (_desc)
                            return (x).Kunum.CompareTo(y.Kunum);
                        return (y).Kunum.CompareTo(x.Kunum);
                    case "pltxt":
                        if (_desc)
                            return (x).Pltxt.CompareTo(y.Pltxt);
                        return (y).Pltxt.CompareTo(x.Pltxt);
                    case "tplnr":
                        if (_desc)
                            return (x).Tplnr.CompareTo(y.Tplnr);
                        return (y).Tplnr.CompareTo(x.Tplnr);
                    case "stort":
                        if (_desc)
                            return (x).Stort.CompareTo(y.Stort);
                        return (y).Stort.CompareTo(x.Stort);
                    case "raumnr":
                        if (_desc)
                            return (x).Raumnr.CompareTo(y.Raumnr);
                        return (y).Raumnr.CompareTo(x.Raumnr);
                    case "status":
                        if (_desc)
                            return (x).ObjectStatus[0].UserStatusCode.CompareTo(y.ObjectStatus[0].UserStatusCode);
                        return (y).ObjectStatus[0].UserStatusCode.CompareTo(x.ObjectStatus[0].UserStatusCode);
                    case "datesort":
                        if (_desc)
                            return (x).DateSort.CompareTo(y.DateSort);
                        return (y).DateSort.CompareTo(x.DateSort);
                    default:
                        return (x).Aufnr.CompareTo(y.Aufnr);
                }
            }
        }
    }
}