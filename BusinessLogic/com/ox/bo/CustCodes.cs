﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class CustCodes : BaseCustCodes
    {
        private String PROCESS_FLAG = "";

        public string ProcessFlag
        {
            get { return PROCESS_FLAG; }
            set { PROCESS_FLAG = value; }
        }

        public class CustCodesSort : IComparer<CustCodes>
        {
            private Boolean _desc = true;
            private String _compareField;

            public CustCodesSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(CustCodes x, CustCodes y)
            {
                switch (_compareField.ToLower())
                {
                    case "codegrkurztext":
                        if (_desc)
                            return x.CodegrKurztext.CompareTo(y.CodegrKurztext);
                        return y.CodegrKurztext.CompareTo(x.CodegrKurztext);
                    case "kurztext":
                        if (_desc)
                            return (x).Kurztext.CompareTo(y.Kurztext);
                        return (y).Kurztext.CompareTo(x.Kurztext);
                    default:
                        return (x).Codegruppe.CompareTo(y.Codegruppe);
                }
            }
        }
    }
}