﻿using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Operation : BaseOperation
    {

        public Operation() : base()
        {
        }
        public Operation(OrdOperation ordOper) : base(ordOper)
        {
        }

    }
}