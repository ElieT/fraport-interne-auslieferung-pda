﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Cust_001 : BaseCust_001
    {
        private static Cust_001 _instance;

        private Cust_001() : base()
        {
        
        }

        public static Cust_001 GetInstance()
        {
            if(_instance == null)
                _instance = new Cust_001();
            return _instance;
        }
        
    }
}
