﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class EnhCust001
    {
        public String Katalogart { get; set; }
        public String Codegruppe { get; set; }
        public String Code { get; set; }
        public String ProcessFlag { get; set; }
        public String Stsma { get; set; }
        public String JStatus { get; set; }
    }
}
