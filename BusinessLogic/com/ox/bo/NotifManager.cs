﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.baseBo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class NotifManager : BaseNotifManager
    {
        public static new Boolean IsRfidConfirmed(Notif notif, String statusCode, Boolean inTransaction)
        {
            // Check if already one notif item has been saved which indicates the reason of missing rfid ident
            var retList = new List<CustCodes>();
            try
            {
                var sqlStmt =
                    "SELECT C_ZFRAPORT_ENH_CUST001.CODEGRUPPE, C_ZFRAPORT_ENH_CUST001.KATALOGART, C_ZFRAPORT_ENH_CUST001.CODE, C_ZFRAPORT_ENH_CUST001.PROCESS_FLAG " +
                    "FROM C_ZFRAPORT_ENH_CUST001 WHERE PROCESS_FLAG = @PROCESS_FLAG";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@PROCESS_FLAG", statusCode);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var custcodes = new CustCodes();
                    custcodes.Katalogart = rdr["KATALOGART"];
                    custcodes.Codegruppe = rdr["CODEGRUPPE"];
                    custcodes.Code = rdr["CODE"];
                    foreach (var item in notif._notifItems)
                    {
                        if (item.Fecod.Equals(custcodes.Code) && item.Fegrp.Equals(custcodes.Codegruppe) &&
                            item.Fekat.Equals(custcodes.Katalogart))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //FileManager.LogMessage("Error while getCodes");
                FileManager.LogException(ex);
            }

            return false;
        }

        private static void SetAxoEnh(NotifItem notifItem)
        {
            try
            {
                var sqlStmt = "";
                sqlStmt = "UPDATE [D_NOTIITEM] SET " +
                          "AXOENH = @AXOENH " +
                          "WHERE QMNUM = @QMNUM AND FENUM = @FENUM";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", notifItem.Qmnum);
                cmd.Parameters.AddWithValue("@FENUM", notifItem.Fenum);
                cmd.Parameters.AddWithValue("@AXOENH", notifItem.AxoEnh);
                cmd.Parameters.AddWithValue("@UPDFLAG", notifItem.Updflag);
                OxDbManager.GetInstance().FeedTransaction(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public new static Boolean IsRfidItem(NotifItem notifItem, Boolean inTransaction)
        {
            // Check if already one notif item has been saved which indicates the reason of missing rfid ident
            var retList = new List<CustCodes>();
            try
            {
                String sqlStmt =
                    "SELECT C_ZFRAPORT_ENH_CUST001.CODEGRUPPE, C_ZFRAPORT_ENH_CUST001.KATALOGART, C_ZFRAPORT_ENH_CUST001.CODE, C_ZFRAPORT_ENH_CUST001.PROCESS_FLAG " +
                    "FROM C_ZFRAPORT_ENH_CUST001";
                var cmd = new SQLiteCommand(sqlStmt);

                List<Dictionary<string, string>> records;
                if (inTransaction)
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var custcodes = new CustCodes();
                    custcodes.Katalogart = rdr["KATALOGART"];
                    custcodes.Codegruppe = rdr["CODEGRUPPE"];
                    custcodes.Code = rdr["CODE"];
                    retList.Add(custcodes);
                    if (notifItem.Fecod.Equals(custcodes.Code) && notifItem.Fegrp.Equals(custcodes.Codegruppe) &&
                        notifItem.Fekat.Equals(custcodes.Katalogart))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return false;
        }

        public new static StatusLocal GetNotifStatus(Notif notif)
        {
            // Check if already one notif item has been saved which indicates the reason of missing rfid ident
            var retList = new List<CustCodes>();
            try
            {
                var sqlStmt =
                    "SELECT * FROM D_NOTIITEM  WHERE QMNUM = @QMNUM AND D_NOTIITEM.FECOD in " +
                    "(SELECT CODE FROM C_ZFRAPORT_ENH_CUST001 WHERE KATALOGART = 'C' AND (PROCESS_FLAG = 'N' OR PROCESS_FLAG = 'R')) AND " +
                    "D_NOTIITEM.FEGRP in " +
                    "(SELECT CODEGRUPPE FROM C_ZFRAPORT_ENH_CUST001 WHERE KATALOGART = 'C' AND (PROCESS_FLAG = 'N' OR PROCESS_FLAG = 'R'))";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", notif.Qmnum);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count > 0)
                    return new StatusLocal(StatusLocal.Color.CRed, StatusLocal.Shape.CRect);
                sqlStmt =
                    "SELECT * FROM D_NOTIACTIVITY WHERE QMNUM = @QMNUM";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", notif.Qmnum);
                records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count > 0)
                    return new StatusLocal(StatusLocal.Color.CGreen, StatusLocal.Shape.CCircle);

            }
            catch (Exception ex)
            {
                //FileManager.LogMessage("Error while getCodes");
                FileManager.LogException(ex);
            }
            return new StatusLocal(StatusLocal.Color.CWhite, StatusLocal.Shape.CCircle);
        }
    }
}