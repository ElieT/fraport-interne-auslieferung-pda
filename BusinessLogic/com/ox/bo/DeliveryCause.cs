﻿using System;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class DeliveryCause: BaseDeliveryCause
    {
        private String spras;
        protected String text;
        private String grund;

        // GETTER/SETTER 
        public string Text
        {
            get { return text; }
            set { text = value; }
        }     

        public string Spras
        {
            get { return spras; }
            set { spras = value; }
        }

        public string Grund
        {
            get { return grund; }
            set { grund = value; }
        }


    }
}
