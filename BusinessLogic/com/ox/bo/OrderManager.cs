﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using oxmc.BusinessLogic.com.ox.baseBo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class OrderManager : BaseOrderManager
    {
        protected new static List<OrdOperation> _ordOperations = new List<OrdOperation>();

        protected new static List<Order> _orderList;

        public static List<OrdOperation> GetOrderOperations(String date)
        {
            _ordOperations = new List<OrdOperation>();
            var noSplits = false;
            try
            {
                var records = new List<Dictionary<string, string>>();
                var sqlStmt = "";
                //If scenario for customizing of usage of splits and/or operations is implemented, replace with flag-checking
                if (true)
                {
                    sqlStmt =
                        "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                        "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                        "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, D_NOTIHEAD.UPDFLAG, " +
                        "D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                        "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, " +
                        "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                        "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY, D_ORDOPER.ISTRU, " +
                        "D_ORDSPLIT.BEDID, D_ORDSPLIT.BEDZL, D_ORDSPLIT.CANUM, D_ORDSPLIT.AUFPL, D_ORDSPLIT.APLZL, " +
                        "D_ORDSPLIT.FSTAD, D_ORDSPLIT.FSTAU, D_ORDSPLIT.FENDD, D_ORDSPLIT.FENDU, D_ORDSPLIT.PERNR, " +
                        "D_ORDSPLIT.SPLIT, D_ORDSPLIT.ARBID,  " +
                        "D_ORDSPLIT.DAUNO AS SDAUNO, D_ORDSPLIT.DAUNE AS SDAUNE, D_ORDSPLIT.ARBEI AS SARBEI, " +
                        "D_ORDSPLIT.ARBEH AS SARBEH "+
                        "FROM D_ORDSPLIT " +
                        "LEFT OUTER JOIN D_ORDOPER ON " +
                        "D_ORDSPLIT.AUFPL = D_ORDOPER.AUFPL AND D_ORDSPLIT.APLZL = D_ORDOPER.APLZL " +
                        "LEFT OUTER JOIN D_ORDHEAD ON " +
                        "D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                        "LEFT OUTER JOIN D_FLOC ON " +
                        "D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                        "LEFT OUTER JOIN D_NOTIHEAD ON " +
                        "D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                        "ORDER BY D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDSPLIT.SPLIT";

                    var cmd = new SQLiteCommand(sqlStmt);
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                }
                if (records.Count == 0)
                {
                    noSplits = true;
                    sqlStmt =
                        "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                        "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                        "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, " +
                        "D_ORDOPER.PERNR, D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                        "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, D_ORDOPER.UPDFLAG, " +
                        "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                        "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY, D_ORDOPER.ISTRU " +
                        "FROM D_ORDOPER " +
                        "LEFT OUTER JOIN " +
                        "D_ORDHEAD ON " +
                        "D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                        "LEFT OUTER JOIN " +
                        "D_FLOC ON " +
                        "D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                        "LEFT OUTER JOIN " +
                        "D_NOTIHEAD ON " +
                        "D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                        "ORDER BY D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ";
                    records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                }
                foreach (var rdr in records)
                {
                    var ordOperation = new OrdOperation();
                    ordOperation.Aufnr = rdr["AUFNR"];
                    ordOperation.Vornr = rdr["VORNR"];
                    ordOperation.Aufpl = rdr["AUFPL"];
                    ordOperation.Aplzl = rdr["APLZL"];
                    ordOperation.Ktext = rdr["KTEXT"];
                    ordOperation.Ltxa1 = rdr["LTXA1"];
                    ordOperation.Priok = rdr["PRIOK"];
                    ordOperation.MobileKey = rdr["MOBILEKEY"];
                    ordOperation.Larnt = rdr["LARNT"];
                    ordOperation.Pltxt = rdr["PLTXT"];
                    ordOperation.Qmtxt = rdr["QMTXT"];
                    ordOperation.Tplnr = rdr["TPLNR"];
                    DateTime test;
                    DateTimeHelper.getDate(rdr["FSAVD"], out test);
                    ordOperation.Fsavd = test;
                    DateTimeHelper.getTime(rdr["FSAVD"], rdr["FSAVZ"], out test);
                    ordOperation.Fsavz = test;
                    ordOperation.Pernr = rdr["PERNR"];
                    ordOperation.Dauno = rdr["DAUNO"];
                    ordOperation.Daune = rdr["DAUNE"];
                    ordOperation.Arbei = rdr["ARBEI"];
                    ordOperation.Arbeh = rdr["ARBEH"];
                    DateTimeHelper.getDate(rdr["FSEDD"], out test);
                    ordOperation.Fsedd = test;
                    DateTimeHelper.getTime(rdr["FSEDD"], rdr["FSEDZ"], out test);
                    ordOperation.Fsedz = test;
                    ordOperation.Arbpl = rdr["ARBPL"];
                    ordOperation.Werks = rdr["WERKS"];
                    ordOperation.Updflag = rdr["UPDFLAG"];

                    DateTimeHelper.getDate(rdr["NTANF"], out test);
                    ordOperation.Ntanf = test;
                    DateTimeHelper.getTime(rdr["NTANF"], rdr["NTANZ"], out test);
                    ordOperation.Ntanz = test;
                    DateTimeHelper.getDate(rdr["NTEND"], out test);
                    ordOperation.Ntend = test;
                    DateTimeHelper.getTime(rdr["NTEND"], rdr["NTENZ"], out test);
                    ordOperation.Ntenz = test;

                    ordOperation.Isdd = rdr["ISDD"];
                    ordOperation.Isdz = rdr["ISDZ"];
                    ordOperation.Iedd = rdr["IEDD"];
                    ordOperation.Iedz = rdr["IEDZ"];
                    ordOperation.Istru = rdr["ISTRU"];

                    if (!noSplits)
                    {
                        ordOperation.Bedid = rdr["BEDID"];
                        ordOperation.Bedzl = rdr["BEDZL"];
                        ordOperation.Canum = rdr["CANUM"];
                        ordOperation.Aufpl = rdr["AUFPL"];
                        ordOperation.Aplzl = rdr["APLZL"];
                        ordOperation.Arbid = rdr["ARBID"];
                        DateTimeHelper.getDate(rdr["FSTAD"], out test);
                        ordOperation.Fstad = test;
                        DateTimeHelper.getDate(rdr["FSTAU"], out test);
                        ordOperation.Fstau = test;
                        DateTimeHelper.getDate(rdr["FENDD"], out test);
                        ordOperation.Fendd = test;
                        DateTimeHelper.getDate(rdr["FENDU"], out test);
                        ordOperation.Fendu = test;
                        ordOperation.Split = rdr["SPLIT"];
                        ordOperation.SDaune = rdr["SDAUNE"];
                        ordOperation.SDauno = rdr["SDAUNO"];
                        ordOperation.SArbei = rdr["SARBEI"];
                        ordOperation.SArbeh = rdr["SARBEH"];
                    }

                    _ordOperations.Add(ordOperation);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _ordOperations;
        }

        public static new Dictionary<String, List<OrdOperation>> GetOrderOperationsFromDb()
        {
            var retVal = new Dictionary<String, List<OrdOperation>>();
            var cmd = new SQLiteCommand();
            try
            {
                var records = new List<Dictionary<string, string>>();
                var sqlStmt = "";
                //If scenario for customizing of usage of splits and/or operations is implemented, replace with flag-checking
                if (true)
                {
                    sqlStmt =
                        "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                        "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                        "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, D_NOTIHEAD.UPDFLAG, " +
                        "D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                        "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, " +
                        "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                        "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY, D_ORDOPER.ISTRU " +
                        "D_ORDSPLIT.BEDID, D_ORDSPLIT.BEDZL, D_ORDSPLIT.CANUM, D_ORDSPLIT.AUFPL, D_ORDSPLIT.APLZL, " +
                        "D_ORDSPLIT.FSTAD, D_ORDSPLIT.FSTAU, D_ORDSPLIT.FENDD, D_ORDSPLIT.FENDU, D_ORDSPLIT.PERNR, " +
                        "D_ORDSPLIT.SPLIT, D_ORDSPLIT.ARBID, " +
                        "D_ORDSPLIT.DAUNO AS SDAUNO, D_ORDSPLIT.DAUNE AS SDAUNE, D_ORDSPLIT.ARBEI AS SARBEI, " +
                        "D_ORDSPLIT.ARBEH AS SARBEH " +
                        "FROM D_ORDSPLIT " +
                        "LEFT OUTER JOIN D_ORDOPER ON " +
                        "D_ORDSPLIT.AUFPL = D_ORDOPER.AUFPL AND D_ORDSPLIT.APLZL = D_ORDOPER.APLZL " +
                        "LEFT OUTER JOIN D_ORDHEAD ON " +
                        "D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                        "LEFT OUTER JOIN D_FLOC ON " +
                        "D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                        "LEFT OUTER JOIN D_NOTIHEAD ON " +
                        "D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                        "ORDER BY D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDSPLIT.SPLIT";

                    cmd = new SQLiteCommand(sqlStmt);
                    records = OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                sqlStmt =
                    "SELECT D_ORDHEAD.AUFNR, D_ORDOPER.VORNR, D_ORDOPER.AUFPL, D_ORDOPER.APLZL, D_ORDHEAD.KTEXT, " +
                    "D_ORDOPER.LTXA1, D_ORDOPER.LARNT, D_ORDHEAD.PRIOK, D_FLOC.PLTXT, D_NOTIHEAD.QMTXT, " +
                    "D_ORDHEAD.TPLNR, D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ, " +
                    "D_ORDOPER.PERNR, D_ORDOPER.DAUNO, D_ORDOPER.DAUNE, D_ORDOPER.ARBEI, " +
                    "D_ORDOPER.ARBEH, D_ORDOPER.FSEDD, D_ORDOPER.FSEDZ, D_ORDOPER.ARBPL, D_ORDOPER.WERKS, D_ORDOPER.UPDFLAG, " +
                    "D_ORDOPER.ANZMA, D_ORDOPER.NTANF, D_ORDOPER.NTANZ, D_ORDOPER.NTEND, D_ORDOPER.NTENZ, " +
                    "D_ORDOPER.ISDD, D_ORDOPER.ISDZ, D_ORDOPER.IEDD, D_ORDOPER.IEDZ, D_ORDOPER.MOBILEKEY, D_ORDOPER.ISTRU " +
                    "FROM D_ORDOPER " +
                    "LEFT OUTER JOIN " +
                    "D_ORDHEAD ON D_ORDHEAD.AUFNR = D_ORDOPER.AUFNR " +
                    "LEFT OUTER JOIN " +
                    "D_FLOC ON D_ORDHEAD.TPLNR = D_FLOC.TPLNR " +
                    "LEFT OUTER JOIN " +
                    "D_NOTIHEAD ON D_ORDHEAD.QMNUM = D_NOTIHEAD.QMNUM " +
                    "WHERE " +
                    "(D_ORDOPER.AUFPL = '' OR D_ORDOPER.AUFPL IS NULL OR D_ORDOPER.AUFPL NOT IN " +
                    "(SELECT D_ORDSPLIT.AUFPL FROM D_ORDSPLIT WHERE D_ORDSPLIT.APLZL = D_ORDOPER.APLZL AND D_ORDOPER.AUFPL = D_ORDSPLIT.AUFPL)) " +
                    "ORDER BY D_ORDOPER.FSAVD, D_ORDOPER.FSAVZ";
                cmd = new SQLiteCommand(sqlStmt);
                records.AddRange(OxDbManager.GetInstance().FeedTransaction(cmd));
                foreach (var rdr in records)
                {
                    var ordOperation = new OrdOperation();
                    ordOperation.Aufnr = rdr["AUFNR"];
                    ordOperation.Vornr = rdr["VORNR"];
                    ordOperation.Aufpl = rdr["AUFPL"];
                    ordOperation.Aplzl = rdr["APLZL"];
                    ordOperation.Ktext = rdr["KTEXT"];
                    ordOperation.Ltxa1 = rdr["LTXA1"];
                    ordOperation.Priok = rdr["PRIOK"];
                    ordOperation.MobileKey = rdr["MOBILEKEY"];
                    ordOperation.Larnt = rdr["LARNT"];
                    ordOperation.Pltxt = rdr["PLTXT"];
                    ordOperation.Qmtxt = rdr["QMTXT"];
                    ordOperation.Tplnr = rdr["TPLNR"];
                    DateTime test;
                    DateTimeHelper.getDate(rdr["FSAVD"], out test);
                    ordOperation.Fsavd = test;
                    DateTimeHelper.getTime(rdr["FSAVD"], rdr["FSAVZ"], out test);
                    ordOperation.Fsavz = test;
                    ordOperation.Pernr = rdr["PERNR"];
                    ordOperation.Dauno = rdr["DAUNO"];
                    ordOperation.Daune = rdr["DAUNE"];
                    ordOperation.Arbei = rdr["ARBEI"];
                    ordOperation.Arbeh = rdr["ARBEH"];
                    DateTimeHelper.getDate(rdr["FSEDD"], out test);
                    ordOperation.Fsedd = test;
                    DateTimeHelper.getTime(rdr["FSEDD"], rdr["FSEDZ"], out test);
                    ordOperation.Fsedz = test;
                    ordOperation.Arbpl = rdr["ARBPL"];
                    ordOperation.Werks = rdr["WERKS"];
                    ordOperation.Updflag = rdr["UPDFLAG"];
                    DateTimeHelper.getDate(rdr["NTANF"], out test);
                    ordOperation.Ntanf = test;
                    DateTimeHelper.getTime(rdr["NTANF"], rdr["NTANZ"], out test);
                    ordOperation.Ntanz = test;
                    DateTimeHelper.getDate(rdr["NTEND"], out test);
                    ordOperation.Ntend = test;
                    DateTimeHelper.getTime(rdr["NTEND"], rdr["NTENZ"], out test);
                    ordOperation.Ntenz = test;
                    ordOperation.Isdd = rdr["ISDD"];
                    ordOperation.Isdz = rdr["ISDZ"];
                    ordOperation.Iedd = rdr["IEDD"];
                    ordOperation.Iedz = rdr["IEDZ"];
                    ordOperation.Istru = rdr["ISTRU"];

                    if (rdr.ContainsKey("BEDID"))
                    {
                        ordOperation.Bedid = rdr["BEDID"];
                        ordOperation.Bedzl = rdr["BEDZL"];
                        ordOperation.Canum = rdr["CANUM"];
                        ordOperation.Aufpl = rdr["AUFPL"];
                        ordOperation.Aplzl = rdr["APLZL"];
                        ordOperation.Arbid = rdr["ARBID"];
                        DateTimeHelper.getDate(rdr["FSTAD"], out test);
                        ordOperation.Fstad = test;
                        DateTimeHelper.getTime(rdr["FSTAD"], rdr["FSTAU"], out test);
                        ordOperation.Fstau = test;
                        DateTimeHelper.getDate(rdr["FENDD"], out test);
                        ordOperation.Fendd = test;
                        DateTimeHelper.getTime(rdr["FENDD"], rdr["FENDU"], out test);
                        ordOperation.Fendu = test;
                        ordOperation.Split = rdr["SPLIT"];
                        ordOperation.SDaune = rdr["SDAUNE"];
                        ordOperation.SDauno = rdr["SDAUNO"];
                        ordOperation.SArbei = rdr["SARBEI"];
                        ordOperation.SArbeh = rdr["SARBEH"];
                    }

                    if (!retVal.ContainsKey(ordOperation.Aufnr))
                        retVal.Add(ordOperation.Aufnr, new List<OrdOperation>());

                    retVal[ordOperation.Aufnr].Add(ordOperation);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        /// <summary>
        /// Check if every split/operation has at least one timeconf
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public new static StatusLocal GetOrderStatus(Order order, Boolean inTransaction)
        {
            try
            {
                Boolean timeConfExist = false;
                var sqlStmt =
                    "SELECT * FROM D_NOTIITEM  WHERE QMNUM = @QMNUM AND D_NOTIITEM.FECOD in " +
                             "(SELECT CODE FROM C_ZFRAPORT_ENH_CUST001) AND D_NOTIITEM.FEGRP in " +
                             "(SELECT CODEGRUPPE FROM C_ZFRAPORT_ENH_CUST001) AND FEKAT in " +
                             "(SELECT KATALOGART FROM C_ZFRAPORT_ENH_CUST001)";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@QMNUM", order.Qmnum);

                List<Dictionary<String, String>> notifItemsRecords;

                if (inTransaction)
                    notifItemsRecords = OxDbManager.GetInstance().FeedTransaction(cmd);
                else
                    notifItemsRecords = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                OxDbManager.GetInstance().CommitTransaction();
                if (notifItemsRecords.Count > 0)
                    return new StatusLocal(StatusLocal.Color.CRed, StatusLocal.Shape.CRect);

                var notifFound = false;
                var notifRecords = (from n in NotifManager.NotifList
                                    where n.Qmnum == order.Qmnum && n._notifActivities.Count > 0
                                    select n).ToList();
                if (notifRecords.Count == 0)
                {
                    foreach (var notif in NotifManager.NotifList)
                    {
                        foreach (var item in notif._notifItems)
                        {
                            if (item._notifItemActivities.Count > 0)
                            {
                                notifFound = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    notifFound = true;
                }

                
                var allExist = true;
                var someExist = false; 
                foreach (var oper in order.OrderOperations)
                {
                    var tConfs = TimeConfManager.GetTimeConfStatus(oper);
                    if(tConfs == null || tConfs.Count() == 0)
                    {
                        allExist = false;
                        continue;
                    }
                    someExist = true;
                    var tConf = (from t in tConfs
                                 where !string.IsNullOrEmpty(t.Rmzhl) && t.Aueru.Equals("X")
                                 select t).ToList();
                    if (tConf.Count == 0) 
                        allExist = false;
                }
                if ((notifRecords.Count != 0 || notifItemsRecords.Count > 0 ) && allExist && notifFound)
                    return new StatusLocal(StatusLocal.Color.CGreen, StatusLocal.Shape.CCircle);
                if ((notifRecords.Count == 0 || notifItemsRecords.Count == 0) && !someExist)
                    return new StatusLocal(StatusLocal.Color.CWhite, StatusLocal.Shape.CCircle);
                return new StatusLocal(StatusLocal.Color.CYellow, StatusLocal.Shape.CTriangle);
            }
            catch (Exception ex)
            {
                //FileManager.LogMessage("Error while getCodes");
                FileManager.LogException(ex);
            }
            return new StatusLocal(StatusLocal.Color.CWhite, StatusLocal.Shape.CCircle);
        }

        public static void UpdateTimesFromOperationTimes(Order order)
        {
            var operations = (from op in order.OrderOperations
                              orderby op.Fsedz
                              select op).ToList();
            if (operations.Count > 0)
            {
                order.Gstrp = operations.First().Fsavd;
                order.Gltrp = operations.Last().Fendd;
            }

            RecalculateOperationsTimes(operations);
        }

        public static void RecalculateOperationsTimes(List<OrdOperation> operations)
        {
            var lastFsedz = new DateTime(1793, 1, 1);
            foreach (var operation in operations)
            {
                if (lastFsedz != new DateTime(1793, 1, 1))
                {
                    operation.Fsavd = lastFsedz;
                    operation.Fsavz = lastFsedz;
                    var hours = DateTimeHelper.TimeValueHours(float.Parse(operation.Dauno.Replace('.', ',')));
                    var minutes = DateTimeHelper.TimeValueMinutes(float.Parse(operation.Dauno.Replace('.', ',')));
                    operation.Fsedz = lastFsedz.AddHours(hours).AddMinutes(minutes);
                    operation.Fsedd = operation.Fsedz;
                }
                lastFsedz = operation.Fsedz;
            }
        }
    }
}