﻿namespace oxmc.BusinessLogic.com.ox.bo
{
    public class StatusLocal
    {
        public enum Color { CRed, CYellow, CGreen, CWhite };
        public enum Shape { CCircle, CRect, CTriangle };

        private Color _color = Color.CWhite;
        private Shape _shape = Shape.CCircle;

        public StatusLocal(Color color, Shape shape)
        {
            _color = color;
            _shape = shape;
        }

        public Color StatusColor
        {
            get { return _color; }
            set { _color = value; }
        }

        public Shape StatusShape
        {
            get { return _shape; }
            set { _shape = value; }
        }
    }
}
