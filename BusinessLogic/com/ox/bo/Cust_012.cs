﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Cust_012 : BaseCust_012
    {
        private static Cust_012 _instance;

        private Cust_012() : base()
        {
        
        }

        public static Cust_012 GetInstance()
        {
            if(_instance == null) 
                _instance = new Cust_012();
            return _instance;
        }

        public static void ClearInstance()
        {
            _instance = null;
        }
    }
}
