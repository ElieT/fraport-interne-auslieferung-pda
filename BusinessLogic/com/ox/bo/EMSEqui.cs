﻿using System;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class EMSEqui
    {
        public String bnv, einbauort;
        public String equnr;
        public String leerungsdienst;
        public String nummer;
        public String richtung;
        public String standort;
        public String typ;
    }
}