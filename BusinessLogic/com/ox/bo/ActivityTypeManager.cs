﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class ActivityTypeManager : baseBo.BaseActivityTypeManager
    {
        /// <summary>
        /// ===CUSTOMER IMPLEMENTATION===
        /// 
        /// The Fraport scenario uses the C_ZFRAPORT_ENH_MD0001-table instead of C_ACTIVITY_TYPE 
        /// </summary>
        /// <param name="arbpl">Workplace</param>
        /// <returns></returns>
        public static new Dictionary<String, ActivityType> GetActivityTypes(String arbpl)
        {
            var retVal = new Dictionary<String, ActivityType>();
            try
            {
                var sqlStmt = "SELECT * FROM C_ZFRAPORT_ENH_MD0001 WHERE ARBPL = @ARBPL";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@ARBPL", arbpl);
                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var ret in result)
                {
                    var aT = new ActivityType();
                    aT.Arbpl = ret["ARBPL"];
                    aT.Lstar = ret["LSTAR_WORK"];
                    aT.Werks = ret["WERKS"];
                    aT.Ktext = ret["LSTAR_WORK_TXT"];
                    aT.LstarTravel = ret["LSTAR_TRAVEL"];
                    aT.LstarTravelKtext = ret["LSTAR_TRAV_TXT"];
                    retVal.Add(ret["LSTAR_WORK"], aT);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        /// <summary>
        /// ===CUSTOMER IMPLEMENTATION===
        /// 
        /// The Fraport scenario uses the C_ZFRAPORT_ENH_MD0001-table instead of C_ACTIVITY_TYPE 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, ActivityType> GetActivityTypes()
        {
            var retVal = new Dictionary<String, ActivityType>();
            try
            {
                var sqlStmt = "SELECT * FROM C_ZFRAPORT_ENH_MD0001";
                var cmd = new SQLiteCommand(sqlStmt);
                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var ret in result)
                {
                    var aT = new ActivityType();
                    aT.Arbpl = ret["ARBPL"];
                    aT.Lstar = ret["LSTAR_WORK"];
                    aT.Werks = ret["WERKS"];
                    aT.Ktext = ret["LSTAR_WORK_TXT"];
                    aT.LstarTravel = ret["LSTAR_TRAVEL"];
                    aT.LstarTravelKtext = ret["LSTAR_TRAV_TXT"];
                    retVal.Add(ret["LSTAR_WORK"], aT);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
        public static Dictionary<String, ActivityType> GetLstarTravel(String arbpl, String werks)
        {
            var retVal = new Dictionary<String, ActivityType>();
            try
            {
                var cmd = new SQLiteCommand("SELECT * FROM C_ZFRAPORT_ENH_MD0001 WHERE ARBPL = '" + arbpl + "' AND WERKS='" + werks + "'");
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var ret in records)
                {
                    var aT = new ActivityType();
                    aT.Arbpl = ret["ARBPL"];
                    //aT.Kokrs = ret["KOKRS"];
                    aT.Lstar = ret["LSTAR_WORK"];
                    aT.Werks = ret["WERKS"];
                    aT.Ktext = ret["LSTAR_WORK_TXT"];
                    aT.LstarTravel = ret["LSTAR_TRAVEL"];
                    retVal.Add(ret["LSTAR_WORK"], aT);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
    }


}
