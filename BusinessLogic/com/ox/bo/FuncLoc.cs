﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class FuncLoc : BaseFuncLoc
    {
        public class FuncLocSort : IComparer<FuncLoc>
        {
            private Boolean _desc = true;
            private String _compareField;

            public FuncLocSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(FuncLoc x, FuncLoc y)
            {
                switch (_compareField.ToUpper())
                {
                    case "IWERK":
                        if (_desc)
                            return x.IWERK.CompareTo(y.IWERK);
                        return y.IWERK.CompareTo(x.IWERK);
                    case "PLTXT":
                        if (_desc)
                            return x.PLTXT.CompareTo(y.PLTXT);
                        return y.PLTXT.CompareTo(x.PLTXT);
                    case "SUBMT":
                        if (_desc)
                            return x.SUBMT.CompareTo(y.SUBMT);
                        return y.SUBMT.CompareTo(x.SUBMT);
                    case "TPLNR":
                        if (_desc)
                            return x.TPLNR.CompareTo(y.TPLNR);
                        return y.TPLNR.CompareTo(x.TPLNR);
                    case "RBNR":
                        if (_desc)
                            return x.RBNR.CompareTo(y.RBNR);
                        return y.RBNR.CompareTo(x.RBNR);
                    case "POSNR":
                        if (_desc)
                            return x.POSNR.CompareTo(y.POSNR);
                        return y.POSNR.CompareTo(x.POSNR);
                    case "EQART":
                        if (_desc)
                            return x.EQART.CompareTo(y.EQART);
                        return y.EQART.CompareTo(x.EQART);
                    case "INVNR":
                        if (_desc)
                            return x.INVNR.CompareTo(y.INVNR);
                        return y.INVNR.CompareTo(x.INVNR);
                    case "HERST":
                        if (_desc)
                            return x.HERST.CompareTo(y.HERST);
                        return y.HERST.CompareTo(x.HERST);
                    case "HERLD":
                        if (_desc)
                            return x.HERLD.CompareTo(y.HERLD);
                        return y.HERLD.CompareTo(x.HERLD);
                    case "BAUJJ":
                        if (_desc)
                            return x.BAUJJ.CompareTo(y.BAUJJ);
                        return y.BAUJJ.CompareTo(x.BAUJJ);
                    case "BAUMM":
                        if (_desc)
                            return x.BAUMM.CompareTo(y.BAUMM);
                        return y.BAUMM.CompareTo(x.BAUMM);
                    case "TYPBZ":
                        if (_desc)
                            return x.TYPBZ.CompareTo(y.TYPBZ);
                        return y.TYPBZ.CompareTo(x.TYPBZ);
                    case "STORT":
                        if (_desc)
                            return x.STORT.CompareTo(y.STORT);
                        return y.STORT.CompareTo(x.STORT);
                    case "BEBER":
                        if (_desc)
                            return x.BEBER.CompareTo(y.BEBER);
                        return y.BEBER.CompareTo(x.BEBER);
                    case "KOSTL":
                        if (_desc)
                            return x.KOSTL.CompareTo(y.KOSTL);
                        return y.KOSTL.CompareTo(x.KOSTL);
                    case "FLTYP":
                        if (_desc)
                            return x.FLTYP.CompareTo(y.FLTYP);
                        return y.FLTYP.CompareTo(x.FLTYP);
                    case "TPLMA":
                        if (_desc)
                            return x.TPLMA.CompareTo(y.TPLMA);
                        return y.TPLMA.CompareTo(x.TPLMA);
                    case "MSGRP":
                        if (_desc)
                            return x.MSGRP.CompareTo(y.MSGRP);
                        return y.MSGRP.CompareTo(x.MSGRP);
                    case "EQFNR":
                        if (_desc)
                            return x.EQFNR.CompareTo(y.EQFNR);
                        return y.EQFNR.CompareTo(x.EQFNR);
                    case "OBJNR":
                        if (_desc)
                            return x.OBJNR.CompareTo(y.OBJNR);
                        return y.OBJNR.CompareTo(x.OBJNR);
                    default:
                        return (x).IWERK.CompareTo(y.IWERK);
                }
            }
        }
    }
}