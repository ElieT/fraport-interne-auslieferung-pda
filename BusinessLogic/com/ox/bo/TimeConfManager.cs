﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using oxmc.BusinessLogic.com.ox.baseBo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class TimeConfManager : BaseTimeConfManager
    {
        public new static List<TimeConf> GetTimeConfsFromDb()
        {
            var retVal = new List<TimeConf>();
            try
            {
                var sqlStmt = "SELECT * FROM D_TIMECONF " +
                                    "ORDER BY " +
                                        "AUFNR, VORNR";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var _timeConf = new TimeConf();
                    DateTime _time;
                    _timeConf.Aufnr = rdr["AUFNR"];
                    _timeConf.Vornr = rdr["VORNR"];
                    _timeConf.Rmzhl = rdr["RMZHL"];
                    DateTimeHelper.getDate(rdr["ERSDA"], out _time);
                    _timeConf.Ersda = _time;
                    _timeConf.Ernam = rdr["ERNAM"];
                    _timeConf.Split = rdr["SPLIT"];
                    _timeConf.Arbpl = rdr["ARBPL"];
                    _timeConf.Werks = rdr["WERKS"];
                    _timeConf.Ltxa1 = rdr["LTXA1"];
                    _timeConf.Txtsp = rdr["TXTSP"];
                    _timeConf.Ismnw = rdr["ISMNW"];
                    _timeConf.Ismne = rdr["ISMNE"];
                    _timeConf.Learr = rdr["LEARR"];
                    _timeConf.Idaur = rdr["IDAUR"];
                    _timeConf.Idaue = rdr["IDAUE"];
                    _timeConf.Pernr = rdr["PERNR"];
                    DateTimeHelper.getDate(rdr["ISDD"], out _time);
                    _timeConf.Isdd = _time;
                    DateTimeHelper.getTime(rdr["ISDD"], rdr["ISDZ"], out _time);
                    _timeConf.Isdz = _time;
                    DateTimeHelper.getDate(rdr["IEDD"], out _time);
                    _timeConf.Iedd = _time;
                    DateTimeHelper.getTime(rdr["IEDD"], rdr["IEDZ"], out _time);
                    _timeConf.Iedz = _time;
                    _timeConf.Aueru = rdr["AUERU"];
                    _timeConf.Ausor = rdr["AUSOR"];
                    _timeConf.Ofmnw = rdr["OFMNW"];
                    _timeConf.Ofmne = rdr["OFMNE"];
                    _timeConf.Bemot = rdr["BEMOT"];
                    _timeConf.UpdFlag = rdr["UPDFLAG"];
                    _timeConf.TConfWork = rdr["TCONFWORK"];
                    _timeConf.TConfTravel = rdr["TCONFTRAVEL"];

                    retVal.Add(_timeConf);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }



        public new static void SaveTimeConf(TimeConf timeConf)
        {
            try
            {
                if (string.IsNullOrEmpty(timeConf.Rmzhl))
                {
                    timeConf.Rmzhl = GetNextTimeConfKey();
                }

                // Insert Data
                timeConf.Ismnw = timeConf.Ismnw.Replace(',', '.');
                timeConf.Idaur = timeConf.Idaur.Replace(',', '.');
                timeConf.Ofmnw = timeConf.Ofmnw.Replace(',', '.');

                var sqlStmt = "INSERT INTO [D_TIMECONF] " +
                          "(MANDT, USERID, AUFNR, VORNR, RMZHL, ERSDA, ERNAM, SPLIT, " +
                          "ARBPL, WERKS, LTXA1, TXTSP, ISMNW, ISMNE, LEARR, IDAUR, " +
                          "IDAUE, PERNR, ISDD, ISDZ, IEDD, IEDZ, AUERU, AUSOR, OFMNW, " +
                          "OFMNE, BEMOT, STATUS, UPDFLAG, MOBILEKEY, TCONFWORK, TCONFTRAVEL) " +
                          "Values (@MANDT, @USERID, @AUFNR, @VORNR, @RMZHL, @ERSDA, @ERNAM, @SPLIT, " +
                          "@ARBPL, @WERKS, @LTXA1, @TXTSP, @ISMNW, @ISMNE, @LEARR, @IDAUR, " +
                          "@IDAUE, @PERNR, @ISDD, @ISDZ, @IEDD, @IEDZ, @AUERU, @AUSOR, @OFMNW, " +
                          "@OFMNE, @BEMOT, @STATUS, @UPDFLAG, @MOBILEKEY, @TCONFWORK, @TCONFTRAVEL)";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@AUFNR", timeConf.Aufnr);
                cmd.Parameters.AddWithValue("@VORNR", timeConf.Vornr);
                cmd.Parameters.AddWithValue("@RMZHL", timeConf.Rmzhl);
                cmd.Parameters.AddWithValue("@ERSDA", DateTimeHelper.GetDateString(timeConf.Ersda));
                cmd.Parameters.AddWithValue("@ERNAM", timeConf.Ernam);
                cmd.Parameters.AddWithValue("@SPLIT", timeConf.Split);
                cmd.Parameters.AddWithValue("@ARBPL", timeConf.Arbpl);
                cmd.Parameters.AddWithValue("@WERKS", timeConf.Werks);
                cmd.Parameters.AddWithValue("@LTXA1", timeConf.Ltxa1);
                cmd.Parameters.AddWithValue("@TXTSP", timeConf.Txtsp);
                cmd.Parameters.AddWithValue("@ISMNW", timeConf.Ismnw);
                cmd.Parameters.AddWithValue("@ISMNE", timeConf.Ismne);
                cmd.Parameters.AddWithValue("@LEARR", timeConf.Learr);
                cmd.Parameters.AddWithValue("@IDAUR", timeConf.Idaur);
                cmd.Parameters.AddWithValue("@IDAUE", timeConf.Idaue);
                cmd.Parameters.AddWithValue("@PERNR", timeConf.Pernr);
                cmd.Parameters.AddWithValue("@ISDD", DateTimeHelper.GetDateString(timeConf.Isdd));
                cmd.Parameters.AddWithValue("@ISDZ", DateTimeHelper.GetTimeString(timeConf.Isdz));
                cmd.Parameters.AddWithValue("@IEDD", DateTimeHelper.GetDateString(timeConf.Iedd));
                cmd.Parameters.AddWithValue("@IEDZ", DateTimeHelper.GetTimeString(timeConf.Iedz));
                cmd.Parameters.AddWithValue("@AUERU", timeConf.Aueru);
                cmd.Parameters.AddWithValue("@AUSOR", timeConf.Ausor);
                cmd.Parameters.AddWithValue("@OFMNW", timeConf.Ofmnw);
                cmd.Parameters.AddWithValue("@OFMNE", timeConf.Ofmne);
                cmd.Parameters.AddWithValue("@BEMOT", timeConf.Bemot);
                cmd.Parameters.AddWithValue("@MOBILEKEY", timeConf.MobileKey);
                cmd.Parameters.AddWithValue("@STATUS", "0");
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@TCONFWORK", timeConf.TConfWork);
                cmd.Parameters.AddWithValue("@TCONFTRAVEL", timeConf.TConfTravel);
                timeConf.UpdFlag = "I";
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();
                CachingManager.AddTimeConf(timeConf);
            }

            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static string GetNextTimeConfKey()
        {
            int intDbKeyTimeConf;
            var sqlStmt = "SELECT MAX(CAST(RMZHL AS Int))+1 FROM D_TIMECONF";
            var cmd = new SQLiteCommand(sqlStmt);
            var record = OxDbManager.GetInstance().FeedTransaction(cmd);
            var result = record[0]["RetVal"];
            if (result.Length > 0)
                intDbKeyTimeConf = int.Parse(result);
            else
                intDbKeyTimeConf = 1;
            return intDbKeyTimeConf.ToString();
        }

        public new static void DeleteTimeConf(TimeConf timeConf)
        {
            try
            {
                var sqlStmt =
                    "DELETE FROM [D_TIMECONF] WHERE MANDT = @MANDT AND USERID = @USERID AND AUFNR = @AUFNR AND VORNR = @VORNR AND SPLIT = @SPLIT AND RMZHL = @RMZHL";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@AUFNR", timeConf.Aufnr);
                cmd.Parameters.AddWithValue("@VORNR", timeConf.Vornr);
                cmd.Parameters.AddWithValue("@SPLIT", timeConf.Split);
                cmd.Parameters.AddWithValue("@RMZHL", timeConf.Rmzhl);
                OxDbManager.GetInstance().FeedTransaction(cmd);
                TimeConfList.Remove(timeConf);

                if (!String.IsNullOrEmpty(timeConf.TConfTravel))
                {
                    var travelTimeConf = (from t in TimeConfList
                                          where t.Aufnr == timeConf.Aufnr &&
                                                t.Vornr == timeConf.Vornr &&
                                                t.Split == timeConf.Split &&
                                                t.TConfWork == timeConf.Rmzhl
                                          select t).ToList();
                    if (travelTimeConf.Count > 0)
                    {
                        sqlStmt =
                            "DELETE FROM [D_TIMECONF] WHERE MANDT = @MANDT AND USERID = @USERID AND AUFNR = @AUFNR AND VORNR = @VORNR AND SPLIT = @SPLIT AND RMZHL = @RMZHL";
                        cmd = new SQLiteCommand(sqlStmt);
                        cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                        cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                        cmd.Parameters.AddWithValue("@AUFNR", timeConf.Aufnr);
                        cmd.Parameters.AddWithValue("@VORNR", timeConf.Vornr);
                        cmd.Parameters.AddWithValue("@SPLIT", timeConf.Split);
                        cmd.Parameters.AddWithValue("@RMZHL", travelTimeConf[0].Rmzhl);
                        OxDbManager.GetInstance().FeedTransaction(cmd);
                        TimeConfList.Remove(travelTimeConf[0]);
                    }
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                OxDbManager.GetInstance().CommitTransaction();
                FileManager.LogException(ex);
            }
        }

        public static TimeConf GetTravelTimeConf(TimeConf tConf)
        {
            var travelTimeConfList = (from t in TimeConfList
                                      where
                                          t.Vornr == tConf.Vornr && t.Split ==
                                          tConf.Split && t.TConfWork == tConf.Rmzhl
                                      select t).ToList();
            if (travelTimeConfList.Count == 1)
                return travelTimeConfList[0];
            return null;
        }



        public static String GetTravelTimeConfLstar(string learr)
        {
            var retVal = "";
            try
            {
                var sqlStmt = "SELECT LSTAR_TRAVEL FROM C_ZFRAPORT_ENH_MD0001 WHERE LSTAR_WORK = @LEARR";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@LEARR", learr);
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count == 1)
                {
                    retVal = records[0]["LSTAR_TRAVEL"];
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static new void GetTimeConfStatus(OrdOperation operation, out int count, out String total)
        {
            var tcl = (from n in TimeConfList
                       where n.Aufnr == operation.Aufnr && n.Vornr == operation.Vornr && n.Split == operation.Split && n.TConfWork.Equals("") && !n.TConfTravel.Equals(n.Rmzhl)
                       select n).ToList();
            count = tcl.Count;
            total = GetActHours(operation.Aufnr, operation.Vornr, operation.Split);
            var flTotal = float.Parse(total.Replace('.', ','));
            total = DateTimeHelper.TimeValue(flTotal);
            if (operation.SArbeh.Equals("MIN") || operation.Arbeh.Equals("MIN"))
            {
                total = total.Substring(0, total.IndexOf(":")) + " MIN";
            }
            else
            {
                total = total + " H";
            }
        }

        private static new String GetActHours(String aufnr, String vornr, String split)
        {
            var list = (from n in TimeConfList
                        where n.Aufnr == aufnr && n.Vornr == vornr && n.Split == split && n.TConfWork.Equals("") && !n.TConfTravel.Equals(n.Rmzhl)
                        select n).ToList();
            var total = 0.0;
            try
            {
                foreach (var item in list)
                {
                    var ismnw = item.Ismnw;
                    if (ismnw != null)
                    {
                        ismnw = ismnw.Replace('.', ',');
                        var intIsmnw = Double.Parse(ismnw, NumberStyles.AllowDecimalPoint,
                                                        CultureInfo.CreateSpecificCulture("de-DE"));
                        total = total + intIsmnw;
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return total.ToString();
        }

        public static new void GetTimeConfStatusTravel(OrdOperation operation, out int count, out String total)
        {
            var tcl = (from n in TimeConfList
                       where n.Aufnr == operation.Aufnr && n.Vornr == operation.Vornr && n.Split == operation.Split && (!n.TConfWork.Equals("") || n.TConfTravel.Equals(n.Rmzhl))
                       select n).ToList();
            count = tcl.Count;
            total = GetActHoursTravel(operation.Aufnr, operation.Vornr, operation.Split);
            var flTotal = float.Parse(total.Replace('.', ','));
            total = DateTimeHelper.TimeValue(flTotal);
            if (operation.SArbeh.Equals("MIN") || operation.Arbeh.Equals("MIN"))
            {
                total = total.Substring(0, total.IndexOf(":")) + " MIN";
            }
            else
            {
                total = total + " H";
            }
        }

        private static new String GetActHoursTravel(String aufnr, String vornr, String split)
        {
            var list = (from n in TimeConfList
                        where n.Aufnr == aufnr && n.Vornr == vornr && n.Split == split && (!n.TConfWork.Equals("") || n.TConfTravel.Equals(n.Rmzhl))
                        select n).ToList();
            var total = 0.0;
            try
            {
                foreach (var item in list)
                {
                    var ismnw = item.Ismnw;
                    if (ismnw != null)
                    {
                        ismnw = ismnw.Replace('.', ',');
                        var intIsmnw = Double.Parse(ismnw, NumberStyles.AllowDecimalPoint,
                                                        CultureInfo.CreateSpecificCulture("de-DE"));
                        total = total + intIsmnw;
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return total.ToString();
        }
    }
}