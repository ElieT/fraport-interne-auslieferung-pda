﻿using System;
using System.Collections.Generic;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class MatConf : BaseMatConf
    {

        public MatConf(): base()
        {
        }

        public MatConf(Order order): base(order)
        {
        }

        public class MatConfSort : IComparer<MatConf>
        {
            private Boolean _desc = true;
            private String _compareField;

            public MatConfSort(String compareField, Boolean desc)
            {
                _desc = desc;
                _compareField = compareField;
            }

            public int Compare(MatConf x, MatConf y)
            {
                switch (_compareField.ToLower())
                {
                    case "AUFNR":
                        if (_desc)
                            return x.AUFNR.CompareTo(y.AUFNR);
                        return y.AUFNR.CompareTo(x.AUFNR);
                    case "MOVE_TYPE":
                        if (_desc)
                            return x.MOVE_TYPE.CompareTo(y.MOVE_TYPE);
                        return y.MOVE_TYPE.CompareTo(x.MOVE_TYPE);
                    case "RSNUM":
                        if (_desc)
                            return x.RSNUM.CompareTo(y.RSNUM);
                        return y.RSNUM.CompareTo(x.RSNUM);
                    case "RSPOS":
                        if (_desc)
                            return x.RSPOS.CompareTo(y.RSPOS);
                        return y.RSPOS.CompareTo(x.RSPOS);
                    case "SPEC_STOCK":
                        if (_desc)
                            return x.SPEC_STOCK.CompareTo(y.SPEC_STOCK);
                        return y.SPEC_STOCK.CompareTo(x.SPEC_STOCK);
                    case "STCK_TYPE":
                        if (_desc)
                            return x.STCK_TYPE.CompareTo(y.STCK_TYPE);
                        return y.STCK_TYPE.CompareTo(x.STCK_TYPE);
                    case "EQUNR":
                        if (_desc)
                            return x.EQUNR.CompareTo(y.EQUNR);
                        return y.EQUNR.CompareTo(x.EQUNR);
                    case "CUSTOMER":
                        if (_desc)
                            return x.CUSTOMER.CompareTo(y.CUSTOMER);
                        return y.CUSTOMER.CompareTo(x.CUSTOMER);
                    case "PLANT":
                        if (_desc)
                            return x.PLANT.CompareTo(y.PLANT);
                        return y.PLANT.CompareTo(x.PLANT);
                    case "QUANTITY":
                        if (_desc)
                            return x.QUANTITY.CompareTo(y.QUANTITY);
                        return y.QUANTITY.CompareTo(x.QUANTITY);
                    case "STORLOC":
                        if (_desc)
                            return x.STORLOC.CompareTo(y.STORLOC);
                        return y.STORLOC.CompareTo(x.STORLOC);
                    case "MATNR":
                        if (_desc)
                            return x.MATNR.CompareTo(y.MATNR);
                        return y.MATNR.CompareTo(x.MATNR);
                    case "BATCH":
                        if (_desc)
                            return x.BATCH.CompareTo(y.BATCH);
                        return y.BATCH.CompareTo(x.BATCH);
                    case "Unit":
                        if (_desc)
                            return x.Unit.CompareTo(y.Unit);
                        return y.Unit.CompareTo(x.Unit);
                    case "ISDD":
                        if (_desc)
                            return x.ISDD.CompareTo(y.ISDD);
                        return y.ISDD.CompareTo(x.ISDD);
                    case "MBLNR":
                        if (_desc)
                            return x.MBLNR.CompareTo(y.MBLNR);
                        return y.MBLNR.CompareTo(x.MBLNR);
                    case "MJAHR":
                        if (_desc)
                            return x.MJAHR.CompareTo(y.MJAHR);
                        return y.MJAHR.CompareTo(x.MJAHR);
                    case "UPFLAG":
                        if (_desc)
                            return x.UPFLAG.CompareTo(y.UPFLAG);
                        return y.UPFLAG.CompareTo(x.UPFLAG);
                    default:
                        return (x).AUFNR.CompareTo(y.AUFNR);
                }
            }
        }
    }
}
