﻿using System;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class Docd : BaseDocd
    {
        private String ID,
                       LINENUMBER,
                       LINE;

        public string Id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string Linenumber
        {
            get { return LINENUMBER; }
            set { LINENUMBER = value; }
        }

        public string Line
        {
            get { return LINE; }
            set { LINE = value; }
        }
    }
}
