﻿using System;
using oxmc.BusinessLogic.com.ox.baseBo;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class ObjectStatus : BaseObjectStatus
    {

        public ObjectStatus(Order order) 
            : base(order)
        {
        }
        public ObjectStatus(Order order, String status_int, String user_status_code, String user_status_desc) 
            : base(order, status_int, user_status_code, user_status_desc)
        {
        }
        public ObjectStatus(Notif notif, String status_int, String user_status_code, String user_status_desc)
            : base(notif, status_int, user_status_code, user_status_desc)
        {
        }
    }
}
