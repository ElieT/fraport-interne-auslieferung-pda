﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.baseBo;
using oxmc.BusinessLogic.com.ox.control;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class ChecklistManager : BaseChecklistManager
    {

        public static void DeleteEmptyResults(String qmnum)
        {
            try
            {
                var sqlStmt = "DELETE FROM D_QPRESULT WHERE QMNUM = @QMNUM  AND UPDFLAG = @UPDFLAG AND CLOSED <> @CLOSED";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@QMNUM", qmnum);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@CLOSED", "X");

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static List<ChecklistItem> GetCheckList(String material)
        {
            var checkListItems = new List<ChecklistItem>();
            try
            {
                var sqlStmt = "SELECT DISTINCT C_QPLANMAT.DATUV FROM C_QPLANMAT INNER JOIN " +
                            "C_QPLANHEAD ON " +
                                "C_QPLANMAT.PLNTY = C_QPLANHEAD.PLNTY AND " +
                                "C_QPLANMAT.PLNNR = C_QPLANHEAD.PLNNR AND " +
                                "C_QPLANMAT.PLNAL = C_QPLANHEAD.PLNAL " +
                                "INNER JOIN " +
                            "C_QPLANOPER ON " +
                                "C_QPLANHEAD.PLNTY = C_QPLANOPER.PLNTY AND " +
                                "C_QPLANHEAD.PLNNR = C_QPLANOPER.PLNNR " +
                                "INNER JOIN " +
                            "C_QPLANCHAR ON " +
                                "C_QPLANOPER.PLNTY = C_QPLANCHAR.PLNTY AND " +
                                "C_QPLANOPER.PLNNR = C_QPLANCHAR.PLNNR AND " +
                                "C_QPLANOPER.PLNKN = C_QPLANCHAR.PLNKN " +
                            "WHERE " +
                                "C_QPLANMAT.MATNR = @SUBMT AND " +
                                "C_QPLANMAT.WERKS = @IWERK";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@SUBMT", material);
                cmd.Parameters.AddWithValue("@IWERK", Cust_001.GetInstance().OrderPlantWrkc);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count < 1)
                    return checkListItems;
                var startdate = records[0];

                sqlStmt = "SELECT C_QPLANCHAR.PLNTY, C_QPLANCHAR.PLNNR, C_QPLANCHAR.PLNKN, C_QPLANCHAR.KZEINSTELL, C_QPLANCHAR.MERKNR, " +
                            "C_QPLANCHAR.ZAEHL, C_QPLANCHAR.GUELTIGAB, C_QPLANCHAR.KURZTEXT, C_QPLANCHAR.KATAB1, C_QPLANCHAR.KATALGART1, " +
                            "C_QPLANCHAR.AUSWMENGE1, C_QPLANCHAR.AUSWMGWRK1, C_QPLANCHAR.KATAB2, C_QPLANCHAR.KATALGART2, C_QPLANCHAR.AUSWMENGE2, " +
                            "C_QPLANCHAR.AUSWMGWRK2, C_QPLANCHAR.SOLLWERT, C_QPLANCHAR.TOLERANZOB, C_QPLANCHAR.TOLERANZUN, C_QPLANCHAR.MASSEINHSW, " +
                            "C_QPLANCHAR.CHAR_TYPE, C_QPLANOPER.VORNR, C_QPLANCHAR.STELLEN, C_QPLANOPER.LTXA1, C_QPLANMAT.WERKS, C_QPLANHEAD.KTEXT AS HEADKTEXT, " +
                            "C_QPLANHEAD.PLNAL, C_QPLANHEAD.ZAEHL AS HEAD_ZAEHL, C_QPLANOPER.ZAEHL AS OPER_ZAEHL " +
                            "FROM " +
                                "C_QPLANMAT INNER JOIN " +
                            "C_QPLANHEAD ON " +
                                "C_QPLANMAT.PLNTY = C_QPLANHEAD.PLNTY AND " +
                                "C_QPLANMAT.PLNNR = C_QPLANHEAD.PLNNR AND " +
                                "C_QPLANMAT.PLNAL = C_QPLANHEAD.PLNAL " +
                                "INNER JOIN " +
                            "C_QPLANOPER ON " +
                                "C_QPLANHEAD.PLNTY = C_QPLANOPER.PLNTY AND " +
                                "C_QPLANHEAD.PLNNR = C_QPLANOPER.PLNNR " +
                                "INNER JOIN " +
                            "C_QPLANCHAR ON " +
                                "C_QPLANOPER.PLNTY = C_QPLANCHAR.PLNTY AND " +
                                "C_QPLANOPER.PLNNR = C_QPLANCHAR.PLNNR AND " +
                                "C_QPLANOPER.PLNKN = C_QPLANCHAR.PLNKN " +
                            "WHERE " +
                                "C_QPLANMAT.MATNR = @SUBMT AND " +
                                "C_QPLANMAT.WERKS = @IWERK";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@SUBMT", material);
                cmd.Parameters.AddWithValue("@IWERK", Cust_001.GetInstance().OrderPlantWrkc);
                cmd.Parameters.AddWithValue("@STARTDATE", startdate);

                records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checklistItem = new ChecklistItem();
                    checklistItem.Plnty = rdr["PLNTY"];
                    checklistItem.Plnnr = rdr["PLNNR"];
                    checklistItem.Plnkn = rdr["PLNKN"];
                    checklistItem.Plnal = rdr["PLNAL"];
                    checklistItem.HeadZaehl = rdr["HEAD_ZAEHL"];
                    checklistItem.OperZaehl = rdr["OPER_ZAEHL"];
                    checklistItem.Kzeinstell = rdr["KZEINSTELL"];
                    checklistItem.Merknr = rdr["MERKNR"];
                    checklistItem.Zaehl = rdr["ZAEHL"];
                    checklistItem.Gueltigab = rdr["GUELTIGAB"];
                    checklistItem.Kurztext = rdr["KURZTEXT"];
                    checklistItem.Katab1 = rdr["KATAB1"];
                    checklistItem.Katalgart1 = rdr["KATALGART1"];
                    checklistItem.Auswmenge1 = rdr["AUSWMENGE1"];
                    checklistItem.Auswmgwrk1 = rdr["AUSWMGWRK1"];
                    checklistItem.Katab2 = rdr["KATAB2"];
                    checklistItem.Katalgart2 = rdr["KATALGART2"];
                    checklistItem.Auswmenge2 = rdr["AUSWMENGE2"];
                    checklistItem.Auswmgwrk2 = rdr["AUSWMGWRK2"];
                    checklistItem.Sollwert = rdr["SOLLWERT"];
                    checklistItem.Toleranzob = rdr["TOLERANZOB"];
                    checklistItem.Toleranzun = rdr["TOLERANZUN"];
                    checklistItem.Masseinhsw = rdr["MASSEINHSW"];
                    checklistItem.CharType = rdr["CHAR_TYPE"];
                    checklistItem.Vornr = rdr["VORNR"];
                    checklistItem.Vorltx = rdr["LTXA1"];
                    checklistItem.Werks = rdr["WERKS"];
                    checklistItem.HeadKtext = rdr["HEADKTEXT"];
                    checklistItem.Stellen = rdr["STELLEN"];
                    checkListItems.Add(checklistItem);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return checkListItems;
        }
        
        public static List<ChecklistResults> GetCheckListResults(String qmnum)
        {
            var checkListResults = new List<ChecklistResults>();
            try
            {
                var sqlStmt = "SELECT * FROM D_QPRESULT WHERE QMNUM = @QMNUM  AND (UPDFLAG = @UPDFLAG OR UPDFLAG = @UPDFLAG2) " +
                                    "ORDER BY D_QPRESULT.INSOPER";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                cmd.Parameters.AddWithValue("@UPDFLAG2", "X");
                cmd.Parameters.AddWithValue("@QMNUM", qmnum);

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var rdr in records)
                {
                    var checklistResults = new ChecklistResults();
                    checklistResults.Insplot = rdr["INSPLOT"];
                    checklistResults.Insoper = rdr["INSOPER"];
                    checklistResults.Inschar = rdr["INSCHAR"];
                    checklistResults.Matnr = rdr["MATNR"];
                    checklistResults.Werk = rdr["WERK"];
                    checklistResults.Qmnum = rdr["QMNUM"];
                    checklistResults.Inspector = rdr["INSPECTOR"];
                    DateTimeHelper.getDate(rdr["START_DATE"], out checklistResults.START_DATE);
                    DateTimeHelper.getTime(rdr["START_DATE"], rdr["START_TIME"], out checklistResults.START_TIME);
                    DateTimeHelper.getDate(rdr["END_DATE"], out checklistResults.END_DATE);
                    DateTimeHelper.getTime(rdr["END_DATE"], rdr["END_TIME"], out checklistResults.END_TIME);
                    checklistResults.MeanValue = rdr["MEAN_VALUE"];
                    checklistResults.Remark = rdr["REMARK"];
                    checklistResults.CodeGrp1 = rdr["CODE_GRP1"];
                    checklistResults.Code1 = rdr["CODE1"];
                    checklistResults.CodeGrp2 = rdr["CODE_GRP2"];
                    checklistResults.Code2 = rdr["CODE2"];
                    checklistResults.Closed = rdr["CLOSED"];
                    checklistResults.Equnr = rdr["EQUNR"];
                    checklistResults.Tplnr = rdr["TPLNR"];
                    checklistResults.Status = rdr["STATUS"];
                    checklistResults.Kurztext = rdr["KURZTEXT"];
                    checklistResults.Updateflag = rdr["UPDFLAG"];
                    checklistResults.Evaluate = rdr["EVALUATION"];
                    checkListResults.Add(checklistResults);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return checkListResults;
        }
    }
}