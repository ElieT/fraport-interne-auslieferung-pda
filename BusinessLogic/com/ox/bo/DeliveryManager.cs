﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using oxmc.BusinessLogic.com.ox.baseBo;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.bo
{
    public class DeliveryManager: BaseDeliveryManager
    {

        public static List<Delivery> getAllPickUps()
        {
            try
            {
                List<Delivery> retval = new List<Delivery>();
                var sqlState = "SELECT * FROM D_ZFRAPORT_IA_IAUS WHERE IA_STATUS = @STATUS ORDER BY SHORT";

                var cmd = new SQLiteCommand(sqlState);
                cmd.Parameters.AddWithValue("@STATUS", "O");

                //var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction();

                DateTime date;
                foreach (var rdr in records)
                {
                    var deliv = new Delivery();
                    deliv.Belnr = rdr["BELNR"];
                    deliv.Segid = rdr["SEGID"];
                    deliv.Txzo1 = rdr["TXZ01"];
                    deliv.Menge = rdr["MENGE"];
                    deliv.Meins = rdr["MEINS"];
                    deliv.Empf = rdr["EMPF"];
                    deliv.Gebnr = rdr["GEBNR"];
                    deliv.Raum = rdr["RAUM"];
                    deliv.Shortobj = rdr["SHORT"];
                    deliv.Kostl = rdr["KOSTL"];
                    deliv.Status = rdr["IA_STATUS"];
                    deliv.Grund = rdr["GRUND"];
                    deliv.Gtext = rdr["GTEXT"];
                    deliv.MobileKey = rdr["MOBILEKEY"];
                    deliv.IsPickUp = rdr["ISPICKUP"];
                    DateTimeHelper.getDate(rdr["LIDAT"], out date);
                    deliv.Lidat = date;

                    retval.Add(deliv);
                }
                return retval;
            } catch (Exception ex )
            {
                FileManager.LogException(ex);
                return null;
            }
        }
    }
}
