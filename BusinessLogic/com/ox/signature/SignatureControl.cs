/*
	SignatureControl class
	--
	Collects and displays a signature. The signature is made up of 
	a list of line segments. Each segment contains an array of points 
	(x and y coordinates). 
	
	Draws to a memory bitmap to prevent flickering.
	
	Raises the SignatureUpdate event when a new segment is added to 
	the signature.
*/

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using Common;

namespace oxmc.BusinessLogic.com.ox.signature
{
    /// <summary>
    /// Custom control that collects and displays a signature.
    /// </summary>
    public class SignatureControl : Control
    {
        private String _resource_name;
        // gdi objects
        Bitmap _bmp;
        Graphics _graphics;
        Pen _pen = new Pen(Color.Black);

        // list of line segments
        ArrayList _lines = new ArrayList();

        // the current line segment
        ArrayList _points = new ArrayList();
        Point _lastPoint = new Point(0, 0);

        // if drawing signature or not
        bool _collectPoints = false;
        private bool _written = false;

        public bool Written
        {
            get { return _written; }
            set { _written = value; }
        }

        // notify parent that line segment was updated
        public event EventHandler SignatureUpdate;


        /// <summary>
        /// List of signature line segments.
        /// </summary>
        public ArrayList Lines
        {
            get { return _lines; }
        }

        /// <summary>
        /// Return the signature flattened to a stream of bytes.
        /// </summary>
        public byte[] SignatureBits
        {
            get { return SignatureData.GetBytes(this.Width, this.Height, _lines); }
        }
        public SignatureControl(String resource_name)
        {
            _resource_name = resource_name;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            // blit the memory bitmap to the screen
            // we draw on the memory bitmap on mousemove so there
            // is nothing else to draw at this time (the memory 
            // bitmap already contains all of the lines)
            CreateGdiObjects();
            e.Graphics.DrawImage(_bmp, 0, 0);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // don't pass to base since we paint everything, avoid flashing
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            Written = true;

            // process if currently drawing signature
            if (!_collectPoints)
            {
                // start collecting points
                _collectPoints = true;

                // use current mouse click as the first point
                _lastPoint.X = e.X;
                _lastPoint.Y = e.Y;

                // this is the first point in the list
                _points.Clear();
                _points.Add(_lastPoint);
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            // process if drawing signature
            if (_collectPoints)
            {
                // stop collecting points
                _collectPoints = false;

                // add current line to list of segments
                Point[] points = new Point[_points.Count];
                for (int i = 0; i < _points.Count; i++)
                {
                    Point pt = (Point)_points[i];
                    points[i].X = pt.X;
                    points[i].Y = pt.Y;
                }

                _lines.Add(points);

                // start over with a new line
                _points.Clear();

                // notify container a new segment was added
                RaiseSignatureUpdateEvent();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            System.Console.WriteLine("x");
            // process if drawing signature
            if (_collectPoints)
            {
                // add point to current line segment
                _points.Add(new Point(e.X, e.Y));
                System.Console.WriteLine("x: " + e.X + " " + e.Y);
                // draw the new segment on the memory bitmap
                _graphics.DrawLine(_pen, _lastPoint.X, _lastPoint.Y, e.X, e.Y);
                System.Console.WriteLine("draw x: " + e.X + " " + e.Y);
                // update the current position
                _lastPoint.X = e.X;
                _lastPoint.Y = e.Y;

                // display the updated bitmap
                Invalidate();
            }
        }

        /// <summary>
        /// Clear the signature.
        /// </summary>
        public void Clear()
        {
            _lines.Clear();
            InitMemoryBitmap(_resource_name);
            Invalidate();
        }

        /// <summary>
        /// Create any GDI objects required to draw signature.
        /// </summary>
        private void CreateGdiObjects()
        {
            // only create if don't have one or the size changed
            if (_bmp == null || _bmp.Width != this.Width || _bmp.Height != this.Height)
            {
                // memory bitmap to draw on
                InitMemoryBitmap(_resource_name);
            }
        }

        /// <summary>
        /// Create a memory bitmap that is used to draw the signature.
        /// </summary>
        private void InitMemoryBitmap(String resource_name)
        {
            // load the background image
            //_bmp = Global.LoadImage("sign here.png");
            //_bmp = Global.LoadImage("sign here.png");
            //_bmp = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().
            //    GetManifestResourceStream("UI.images.sign here.png"));
            _bmp = Global.LoadImage(resource_name);


            //_bmp = new Bitmap("C:\\images\\sign here.png");
            // get graphics object now to make drawing during mousemove faster
            _graphics = Graphics.FromImage(_bmp);

        }

        public Graphics getImage()
        {
            return _graphics;
        }
        public Bitmap GetBitmap()
        {
            return _bmp;
        }

        public int[] GetSize()
        {
            int[] retVal = { this.Width, this.Height };
            return retVal;
        }
        /// <summary>
        /// Notify container that a line segment has been added.
        /// </summary>
        private void RaiseSignatureUpdateEvent()
        {
            if (this.SignatureUpdate != null)
                SignatureUpdate(this, EventArgs.Empty);
        }
    }
}