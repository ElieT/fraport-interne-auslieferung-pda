/*
	Global properties and methods.
*/

using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.signature
{
    /// <summary>
    /// Global values and methods.
    /// </summary>
    public class Global
    {
        // public consts
        /*
        public class Const
        {
            public const string EmptyAddress = "0.0.0.0";
        }
	*/
        // settings 
        //static Settings _settings = new Settings(SettingDefaults.Values);
		
        /*
        static public Settings Settings
        {
            get { return _settings; }
        }
        */
        // static methods		
        /*
        private Global()
        {
        }
		*/

        /// <summary>
        /// Return embedded bitmap resource.
        /// </summary>
        static public Bitmap LoadImage(string imageName)
        {
            //return new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(imageName));
            Assembly _assembly;
            Stream _imageStream = null;
            Bitmap _bitMap = null;
            try
            {
                _assembly = Assembly.GetExecutingAssembly();
                _imageStream = _assembly.GetManifestResourceStream("oxmc.BusinessLogic.com.ox.signature." + imageName);
                _bitMap = new Bitmap(_imageStream);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return _bitMap;
        }		
    }
}