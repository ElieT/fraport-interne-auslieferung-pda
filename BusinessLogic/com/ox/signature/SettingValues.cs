/*
	The Settings class does all of the work. This is the 'settings 
	definition' file. It defined two things; first it specifies the 
	keynames used in the settings file. This allows you to use 
	IntelliSense when reading / writing settings.

	The second item is the default values. These are used if a config
	file is not found or the config file does not contain an entry 
	for the particular setting.
*/

using System;

namespace PocketSignature
{
	/// <summary>
	/// Contains setting key names.
	/// </summary>
	public class SettingKeys
	{
		public const string PortNumber = "PortNumber";
		public const string IpAddress = "IpAddress";
		public const string CryptPassphrase = "Passphrase";
	}


	/// <summary>
	/// Default settings
	/// </summary>
	public class SettingDefaults
	{
		static public string[,] Values = 
		{
			{SettingKeys.PortNumber, "10200"},
			{SettingKeys.IpAddress, "0.0.0.0"},
			{SettingKeys.CryptPassphrase, "212a658c-2edf-46e4-b550-ebe53b91e6b3"}
		};
	}
}
