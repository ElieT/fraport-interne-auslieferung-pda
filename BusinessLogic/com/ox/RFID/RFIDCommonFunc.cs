﻿#define WIN32_PLATFORM
using System.Runtime.InteropServices;

namespace oxmc.BusinessLogic.com.ox.RFID
{
    public class RFIDCommonFunc
    {
        public enum MessageBeepType : uint
        {
            //Default = -1,
            Ok = 0x00000000,
            Error = 0x00000010,
            Question = 0x00000020,
            Warning = 0x00000030,
            Information = 0x00000040,
        }

#if	WIN32_PLATFORM
        // eigentlicher Import: user32.dll
        [DllImport("CoreDll.dll")]
        public static extern bool MessageBeep(MessageBeepType type);
#else
        [DllImport("Coredll", SetLastError = true)]
        public static extern bool MessageBeep(MessageBeepType type);
#endif


        //----------- FailBeep ----------------------------------------------
        public void FailBeep()
        {
#if WIN32_PLATFORM
            MessageBeep(MessageBeepType.Error);
#else
            MessageBeep(MessageBeepType.Error);
#endif
        }

        //----------- SuccessBeep -------------------------------------------
        public void SuccessBeep()
        {
#if WIN32_PLATFORM
            MessageBeep(MessageBeepType.Ok);
#else
            MessageBeep(MessageBeepType.Ok);
#endif
        }
    }
}