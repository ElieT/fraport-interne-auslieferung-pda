﻿using System;
using System.Drawing;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.RFID
{
    public class RFIDManager
    {
        private static readonly object padlock = new object();
        private static RFIDManager instance = null;

        private static String _strg;
        public bool _cancelread;
        private RFIDCommonFunc CommonF;
        private int _ergeb;
        private int i;

        private string _objectIdentifier = "";
        private RFIDClass _reader;
        //private int _rfidMaxLength = 256;
        private string _stringToRead = "";
        private string _stringToWrite = "";
        private string _tagIdentifier = "";

        private Timer _timerCheckReader = new Timer();
        private Timer _timerReadText = new Timer();
        private Timer _timerScan = new Timer();
        private Timer _timerWriteText = new Timer();
        private Boolean _verifyMode = AppConfig.RFIDWriteVerify;
        private Boolean _writeMode = false;
        private DateTime _readerStartTimeStamp = DateTime.Now;
        private DateTime _readerStopTimeStamp = DateTime.Now;
        private OxStatus _oxStatus = OxStatus.GetInstance();

        private String _objectMessageString = "";
        TimeSpan ReaderMaxTimeOut = new TimeSpan(0, 0, 0, int.Parse(AppConfig.RFIDTimeout));

        public bool VerifyMode
        {
            get { return _verifyMode; }
            set { _verifyMode = value; }
        }

        public bool PortInitialized
        {
            get { return _reader.Port_Initialized.Equals(1); }
        }

        public string TagIdentifier
        {
            get { return _tagIdentifier; }
            set { _tagIdentifier = value; }
        }

        public static RFIDManager GetInstance()
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new RFIDManager();
                }

                return instance;
            }
        }

        public void WriteText(string StringtoWrite)
        {
            try
            {
                _readerStartTimeStamp = DateTime.Now;
                _writeMode = true;
                OnObjectColor(Color.Red, new EventArgs());
                _timerWriteText.Enabled = false;
                _timerReadText.Enabled = false;
                _cancelread = false;
                _stringToWrite = StringtoWrite;
                
                FileManager.LogMessage("WriteText: _stringToWrite: " + _stringToWrite);
                for (i = 0; i < _reader.TagMaxLength; i++) _reader.Write_Array[i] = 0;

                for (i = 0; i < _stringToWrite.Length; i++)
                {
                    _reader.Write_Array[i] = Convert.ToByte(_stringToWrite[i]);
                }
                _timerWriteText.Enabled = true;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void ReadText()
        {
            try
            {
                if (PortInitialized == false)
                    InitSettings_AndReader();
                System.Console.WriteLine(_reader.Port_Initialized);
                System.Console.WriteLine(_reader.Reader_Connected);
                if (DateTime.Compare(DateTime.Now, _readerStartTimeStamp.Add(ReaderMaxTimeOut)) > 0)
                {
                    _cancelread = true;
                    if (ReadTimeout != null)
                    {
                        OnReadTimeout(this, null);
                    }
                }
                _readerStartTimeStamp = DateTime.Now;
                _writeMode = false;
                OnObjectColor(Color.Red, new EventArgs());
                _timerWriteText.Enabled = false;
                _timerScan.Enabled = false;
                _stringToRead = "";
                _cancelread = false;
                _timerReadText.Enabled = true;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void Identify()
        {
            try
            {
                _readerStartTimeStamp = DateTime.Now;
                _writeMode = false;
                OnObjectColor(Color.Red, new EventArgs());
                _timerReadText.Enabled = false;
                _timerWriteText.Enabled = false;
                _cancelread = false;
                _timerScan.Enabled = true;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void Stop()
        {
            try
            {
                _timerReadText.Enabled = false;
                _timerWriteText.Enabled = false;
                _timerScan.Enabled = false;
                _cancelread = true;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        public void Cancel()
        {
            try
            {
                _timerReadText.Enabled = false;
                _timerWriteText.Enabled = false;
                _timerScan.Enabled = false;
                _cancelread = true;
                _oxStatus.SetStatusMessage("RFID Abbruch, keine Daten gelesen", AppConfig.MessageTimeout);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        public Boolean Busy()
        {
            if (_timerReadText.Enabled || _timerWriteText.Enabled || _timerScan.Enabled)
                return true;
            return false;
        }

        #region Constructor

        private RFIDManager()
        {
            _writeMode = false;

            _reader = new RFIDClass();
            CommonF = new RFIDCommonFunc();

            _timerScan.Interval = 1000;
            _timerScan.Tick += new EventHandler(Timer_Scan_Tick);
            _timerScan.Enabled = false;

            _timerCheckReader.Interval = 5000;
            _timerCheckReader.Tick += new EventHandler(Timer_CheckReader_Tick);
            _timerCheckReader.Enabled = false;

            _timerReadText.Interval = 1000;
            _timerReadText.Tick += new EventHandler(Timer_ReadText_Tick);
            _timerReadText.Enabled = false;

            _timerWriteText.Interval = 1000;
            _timerWriteText.Tick += new EventHandler(Timer_WriteText_Tick);
            _timerWriteText.Enabled = false;

            _reader.From = 0;
            InitSettings_AndReader();
        }

        #endregion

        #region Events from RFIDManager

        public EventHandler ObjectColor;
        public EventHandler ObjectIdentified;
        public EventHandler ObjectMessage;
        public EventHandler ObjectReadFinished;
        public EventHandler ObjectWriteFinished;
        public EventHandler ReadTimeout;

        public int From
        {
            get { return _reader.From; }
            set { FileManager.LogMessage("Set RFID From = " + value); _reader.From = value; }
        }

        public int Length
        {
            get { return _reader.Length; }
            set { FileManager.LogMessage("Set RFID Length = " + value); _reader.Length = value; }
        }

        protected void OnObjectIdentified(Object obj, EventArgs e)
        {
            if (ObjectIdentified != null)
                ObjectIdentified(obj, e);
        }

        protected void OnObjectReadFinished(Object obj, EventArgs e)
        {
            _oxStatus.SetStatusMessage("RFID Daten gelesen: " + obj.ToString(), AppConfig.MessageTimeout);
            if (ObjectReadFinished != null)
                ObjectReadFinished(obj, e);
        }

        protected void OnObjectWriteFinished(Object obj, EventArgs e)
        {
            _oxStatus.SetStatusMessage("RFID Daten geschrieben: " + obj.ToString(), AppConfig.MessageTimeout);
            if (ObjectWriteFinished != null)
                ObjectWriteFinished(obj, e);
        }

        protected void OnObjectMessage(Object obj, EventArgs e)
        {
            _oxStatus.SetStatusMessage("RFID aktiv: " + obj.ToString(), AppConfig.MessageTimeout);
            if (ObjectMessage != null)
                ObjectMessage(obj, e);
        }

        protected void OnObjectColor(Object obj, EventArgs e)
        {
            //_oxStatus.SetStatusMessage("RFID aktiv", AppConfig.MessageTimeout);
            //if (ObjectColor != null)
            //    ObjectColor(obj, e);
        }

        protected void OnReadTimeout(Object obj, EventArgs e)
        {
            //_oxStatus.TriggerEventMessage(1, 'I', DateTime.Now, "RFID Timeout, keine Daten gelesen", null, true, AppConfig.MessageTimeout);
            _oxStatus.SetStatusMessage("RFID Timeout, keine Daten gelesen", AppConfig.MessageTimeout);
            if (ObjectReadFinished != null)
                ObjectReadFinished("timeout", e);
        }

        protected void OnReadCancel(Object obj, EventArgs e)
        {
            //_oxStatus.TriggerEventMessage(1, 'I', DateTime.Now, "RFID Timeout, keine Daten gelesen", null, true, AppConfig.MessageTimeout);
            _oxStatus.SetStatusMessage("RFID Abbruch, keine Daten gelesen", AppConfig.MessageTimeout);
            if (ObjectReadFinished != null)
                ObjectReadFinished("cancel", e);
        }
        

        #endregion Events

        #region Initialization

        public void InitSettings_AndReader()
        {
            try
            {
                OnObjectMessage("Loading", new EventArgs());

                //standard port name COM6=USB,  COM5=Bluetooth
                _reader.PortName = AppConfig.RFIDPortName;
                //standard port type 0==serial port/CF port, 1==CF-port via elserial (only for PPC 2000 systems), 2==BT serial port, 4==USB port (only WIN32 systems)
                _reader.PortType = byte.Parse(AppConfig.RFIDPortType);

                _reader.InterfaceType = 1356; //13,56MHz = standard
                //_reader.InterfaceType = 0x10; //13,56MHz = standard
                //_reader.InterfaceType = 125; //125 //BT
                //125==125kHz
                //1356==1356MHz
                //0x10==PEN-BT with memory support
                _reader.DriverTimeOut = 3500; //250msec timeout, if no tag found
                if (!String.IsNullOrEmpty(AppConfig.RFIDDriverTimeOut))
                    _reader.DriverTimeOut = int.Parse(AppConfig.RFIDDriverTimeOut);
                _reader.DriverSystemMask = 0xFF; //handle all transponders as standard

                _reader.Driver_SetParameters();

                _reader.Reader_OpenInterface();

                if (_reader.Reader_Connected == 1)
                {
                    OnObjectMessage(String.Format("READER ID {0}, driver ver {1}.{2} initialized.",
                                                  _reader.Reader_Id, _reader.Version_Main_Driver_Engine,
                                                  _reader.Version_Sub_Driver_Engine), new EventArgs());
                    CommonF.SuccessBeep();
                }
                else
                {
                    OnObjectMessage(String.Format("No READER connected, driver ver {0}.{1} initialized.", _reader.Version_Main_Driver_Engine, _reader.Version_Sub_Driver_Engine), new EventArgs());
                }
                if (_timerCheckReader == null)
                {
                    _timerCheckReader = new Timer();
                    _timerCheckReader.Interval = 5000;
                    _timerCheckReader.Tick += new EventHandler(Timer_CheckReader_Tick);
                    _timerCheckReader.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void CloseInterface()
        {
            if (_reader.Port_Initialized == 1)
                _reader.Reader_CloseInterface();
        }

        public void OpenInterface()
        {
            if (_reader.Port_Initialized == 0 ||_reader.Reader_Connected == 0)
            {
                FileManager.LogMessage("Open RFID Interface. PortType=" + AppConfig.RFIDPortType + " Portname=" + AppConfig.RFIDPortName);
                _reader.PortType = byte.Parse(AppConfig.RFIDPortType);
                _reader.PortName = AppConfig.RFIDPortName;
                _reader.DriverTimeOut = int.Parse(AppConfig.RFIDDriverTimeOut);
                _reader.Reader_OpenInterface();
            }
        }

        public void GetReaderState()
        {
            _reader.Reader_GetState();
        }


        public String ReaderInfo()
        {
            String output = "";
            output += "Reader Address:          " + _reader.Address + "\r\n";
            output += "Reader ByteParam:        " + _reader.ByteParam + "\r\n";
            output += "Reader DriverName:       " + _reader.DriverName + "\r\n";
            output += "Reader DriverSystemMask: " + _reader.DriverSystemMask + "\r\n";
            output += "Reader DriverTimeOut:    " + _reader.DriverTimeOut + "\r\n";
            output += "Reader From:             " + _reader.From + "\r\n";
            output += "Reader Identifier:       " + _reader.Identifier + "\r\n";
            output += "Reader InterfaceType:    " + _reader.InterfaceType + "\r\n";
            output += "Reader Length:           " + _reader.Length + "\r\n";
            output += "Reader Lock:             " + _reader.Lock + "\r\n";
            output += "Reader Port_Initialized: " + _reader.Port_Initialized + "\r\n";
            output += "Reader PortName:         " + _reader.PortName + "\r\n";
            output += "Reader PortType:         " + _reader.PortType + "\r\n";
            output += "Reader Read_Array:       " + _reader.Read_Array + "\r\n";
            output += "Reader Reader_Connected: " + _reader.Reader_Connected + "\r\n";
            output += "Reader Reader_Id:        " + _reader.Reader_Id + "\r\n";
            output += "Reader Result:           " + _reader.Result + "\r\n";
            output += "Reader TagMaxLength:     " + _reader.TagMaxLength + "\r\n";
            output += "Reader TagSystem:        " + _reader.TagSystem + "\r\n";
            output += "Reader TagType:          " + _reader.TagType + "\r\n";
            output += "Reader TagxTraParam:     " + _reader.TagxTraParam + "\r\n";
            return output;
        }
        #endregion

        #region CheckDevice Timer

        private void Timer_CheckReader_Tick(object sender, EventArgs e)
        {
            try
            {
                _reader.Reader_GetState();
                if ((_reader.Port_Initialized == 1) && (_reader.Reader_Connected == 1))
                {
                    //Okay, _reader connected
                    OnObjectMessage(String.Format("_reader ID {0}, driver ver {1}.{2} present.", _reader.Reader_Id,
                                                  _reader.Version_Main_Driver_Engine,
                                                  _reader.Version_Sub_Driver_Engine), new EventArgs());
                    return;
                }
                if (_reader.Port_Initialized == 0)
                {
                    String newMessage = "Port not valid!";
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                    _reader.Reader_CloseInterface();
                    _reader.Reader_OpenInterface();
                    if (_reader.Port_Initialized == 1)
                    {
                        newMessage = "Port re-initialized!";
                        if (!_oxStatus.LastSync.Equals(newMessage))
                        {
                            _objectMessageString = newMessage;
                            OnObjectMessage(_objectMessageString, new EventArgs());
                        }
                    }
                }
                else
                {
                    OnObjectMessage(String.Format("No _reader connected, driver ver {0}.{1} ready.",
                                                  _reader.Version_Main_Driver_Engine,
                                                  _reader.Version_Sub_Driver_Engine), new EventArgs());
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        #endregion

        #region Read Timer

        private void Timer_ReadText_Tick(object sender, EventArgs e)
        {
            try
            {
                if (DateTime.Compare(DateTime.Now, _readerStartTimeStamp.Add(ReaderMaxTimeOut)) > 0)
                {
                    _cancelread = true;
                    //if (ReadTimeout != null)
                        OnReadTimeout(this, e);
                }

                if (_cancelread)
                {
                    _timerReadText.Enabled = false;
                    OnReadCancel(this, e);
                    return;
                }

                //Read UID at first
                _ergeb = _reader.Reader_Identify();
                if (_ergeb == 0)
                {
                    //TAG found
                    String newMessage = "RFID Tag gefunden.";
                    FileManager.LogMessage("Timer_ReadText_Tick: RFID Tag gefunden. From=" + From + " Length=" + Length);
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                    if (Length > _reader.TagMaxLength || Length == -1) Length = _reader.TagMaxLength;
                    //Length -= From;
                    newMessage = String.Format("Lese " + _reader.Length + " bytes [" + From + " - " + (From + Length) + "]");
                    FileManager.LogMessage("Timer_ReadText_Tick: Reader_ReadBytes " + Length + " bytes [" + From + " - " + (From + Length) + "]");

                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                    _ergeb = _reader.Reader_ReadBytes();
                    if (_ergeb == 0)
                    {
                        CommonF.SuccessBeep();
                        newMessage = "Daten gelesen.";
                        FileManager.LogMessage("Timer_ReadText_Tick: Daten gelesen.");
                        if (!_oxStatus.LastSync.Equals(newMessage))
                        {
                            _objectMessageString = newMessage;
                            OnObjectMessage(_objectMessageString, new EventArgs());
                        }
                        _cancelread = true;
                        FileManager.LogMessage("Timer_ReadText_Tick: cancelread = true");
                        i = 0;
                        _strg = "";
                        while (i < Length)
                        {
                            _strg = _strg + Convert.ToChar(_reader.Read_Array[i + 1]);
                            i++;
                        }
                        Length = -1;
                        _strg = _strg.Trim('\0');

                        if (_strg.IndexOf("91") != -1 && _strg.IndexOf("+") < _strg.LastIndexOf("+"))
                        {
                            newMessage = _strg.Substring(_strg.IndexOf("+") + 1,
                                                                              _strg.LastIndexOf("+") -
                                                                              _strg.IndexOf("+") - 1);
                        }
                        else
                        {
                            newMessage = _strg;
                        }
                        newMessage = "Eingelesene Daten: " + newMessage;
                        FileManager.LogMessage("Timer_ReadText_Tick: Eingelesene Daten: " + _strg);
                        if (!_oxStatus.LastSync.Equals(newMessage))
                        {
                            _objectMessageString = (newMessage.Length > 37) ? newMessage.Substring(0, 37) : newMessage;
                            OnObjectMessage(_objectMessageString, new EventArgs());
                        }
                        _stringToRead = _strg;
                        FileManager.LogMessage("Timer_ReadText_Tick: _stringToRead: " + _strg);
                        FileManager.LogMessage("Timer_ReadText_Tick: WriteMode: " + _writeMode.ToString() + " StringToWrite: " + _stringToWrite + " StringToRead: " + _stringToRead);
                        if (_writeMode && !_stringToWrite.Equals(_stringToRead))
                        {
                            newMessage = "Datenüberprüfung fehlerhaft: " + _strg;
                            FileManager.LogMessage("Timer_ReadText_Tick: Datenüberprüfung fehlerhaft");
                            if (!_oxStatus.LastSync.Equals(newMessage))
                            {
                                _objectMessageString = newMessage;
                                OnObjectMessage(_objectMessageString, new EventArgs());
                            }
                            OnObjectWriteFinished(false, new EventArgs());
                        }
                        else if (_writeMode && _stringToWrite.Equals(_stringToRead))
                        {
                            
                            newMessage = "Datenüberprüfung erfolgreich: " + _strg.Substring(0, 10) + "...";
                            FileManager.LogMessage("Timer_ReadText_Tick: Datenüberprüfung erfolgreich: " + _strg);
                            if (!_oxStatus.LastSync.Equals(newMessage))
                            {
                                _objectMessageString = newMessage;
                                OnObjectMessage(_objectMessageString, new EventArgs());
                            }
                             
                            OnObjectWriteFinished(_stringToWrite, new EventArgs());
                        } else
                        {
                            OnObjectReadFinished(_stringToRead, new EventArgs());
                        }
                        return;
                    }
                    else
                    {
                        newMessage = "Suche RFID Tag";
                        if (!_oxStatus.LastSync.Equals(newMessage))
                        {
                            _objectMessageString = newMessage;
                            OnObjectMessage(_objectMessageString, new EventArgs());
                        }
                    }
                }
                else
                {
                    String newMessage = "Suche RFID Tag";
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    //if (!_objectMessageString.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        #endregion

        #region Write Timer

        private void Timer_WriteText_Tick(object sender, EventArgs e)
        {
            try
            {
                if (DateTime.Compare(DateTime.Now, _readerStartTimeStamp.Add(ReaderMaxTimeOut)) > 0)
                {
                    _cancelread = true;
                    //if (ReadTimeout != null)
                    //{
                        OnReadTimeout(this, e);
                    //}
                }

                if (_cancelread)
                {
                    String newMessage = "RFID Leser stoppen.";
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                    _timerWriteText.Enabled = false;
                    OnReadCancel(this, e);
                    return;
                }

                //Read UID at first
                _ergeb = _reader.Reader_Identify();
                if (_ergeb == 0)
                {
                    String newMessage = "RFID Tag gefunden.";
                    FileManager.LogMessage("Timer_WriteText_Tick: RFID Tag gefunden. From=" + From + " Length=" + Length);
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }


                    int toWriteFrom = From;
                    From = 0;

                    newMessage = String.Format("Lese " + Length + " bytes [" + From + " - " + (From + Length) + "]");
                    FileManager.LogMessage("Timer_ReadText_Tick: Reader_ReadBytes " + Length + " bytes [" + From + " - " + (From + Length) + "]");

                    _ergeb = _reader.Reader_ReadBytes();
                    
                    From = toWriteFrom;

                    //_cancelread = true;
                    i = 0;
                    _strg = "";
                    while (i < Length)
                    {
                        _strg = _strg + Convert.ToChar(_reader.Read_Array[i + 1]);
                        i++;
                    }
                    _strg = _strg.Trim('\0');
                    newMessage = "Daten gelesen: " + _strg;
                    FileManager.LogMessage("Timer_WriteText_Tick: Daten gelesen: " + _strg);
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }

                    if (!_strg.Substring(0, 14).Equals(_tagIdentifier.Substring(0, 14)))
                    {
                        {
                            newMessage = "Objekt gehört nicht zum eingelesenen Objekt.";
                            FileManager.LogMessage("Timer_WriteText_Tick: Objekt gehört nicht zum eingelesenen Objekt (" + _strg + ")");
                            if (!_oxStatus.LastSync.Equals(newMessage))
                            {
                                _objectMessageString = newMessage;
                                OnObjectMessage(_objectMessageString, new EventArgs());
                            }
                            _timerWriteText.Enabled = false;
                            _timerScan.Enabled = false;
                            _stringToRead = "";
                            _cancelread = true;
                            OnObjectWriteFinished(false, new EventArgs());
                            FileManager.LogMessage("Timer_WriteText_Tick: _timerWriteText.Enabled = false");
                            FileManager.LogMessage("Timer_WriteText_Tick: _timerScan.Enabled = false");
                            FileManager.LogMessage("Timer_WriteText_Tick: _cancelread = true");
                            FileManager.LogMessage("Timer_WriteText_Tick: _timerReadText.Enabled = true");
                        }
                        return;
                    }
                    else
                    {
                        newMessage = "Objekt gehört zum eingelesenen Objekt.";
                        FileManager.LogMessage("Timer_WriteText_Tick: Objekt gehört zum eingelesenen Objekt.");
                    }

                    newMessage = "Daten schreiben.";
                    FileManager.LogMessage("Timer_WriteText_Tick: Daten schreiben: " + _stringToWrite);
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }

                    int toReadLength = Length;
                    Length = AppConfig.RfidTechObjMaxTagLength - AppConfig.RfidTechObjIdentLength;
                    if ((Length + From) > _reader.TagMaxLength || Length == -1) Length = _reader.TagMaxLength - From;
                    //Length -= From;
                    newMessage = "Schreibe " + Length + " bytes [" + From + " - " + (From + Length) + "]";
                    FileManager.LogMessage("Timer_WriteText_Tick: Reader_WriteBytes " + Length + " bytes [" + From + " - " + (From + Length) + "]");
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }

                    _ergeb = _reader.Reader_WriteBytes();
                    if (_ergeb == 0)
                    {
                        newMessage = "Daten geschrieben!";
                        FileManager.LogMessage("Timer_WriteText_Tick: Daten geschrieben!");
                        if (!_oxStatus.LastSync.Equals(newMessage))
                        {
                            _objectMessageString = newMessage;
                            OnObjectMessage(_objectMessageString, new EventArgs());
                        }

                        if (!_verifyMode)
                        {
                            Length = -1;
                            newMessage = "Daten geschrieben: " + _strg + " (ohne Kontrolle)";
                            FileManager.LogMessage("Timer_WriteText_Tick: Daten erfolgreich geschrieben: " + _strg);
                            if (!_oxStatus.LastSync.Equals(newMessage))
                            {
                                _objectMessageString = (newMessage.Length > 37) ? newMessage.Substring(0, 37) : newMessage;
                                OnObjectMessage(_objectMessageString, new EventArgs());
                            }
                            OnObjectWriteFinished(_stringToWrite, new EventArgs());
                            CommonF.SuccessBeep();
                            _cancelread = true;
                            FileManager.LogMessage("Timer_WriteText_Tick: cancelread = true");
                        }
                        else
                        {
                            //Length = toReadLength;
                            newMessage = "Kontrollprüfung";
                            FileManager.LogMessage("Timer_WriteText_Tick: Kontrollprüfung");
                            if (!_oxStatus.LastSync.Equals(newMessage))
                            {
                                _objectMessageString = newMessage;
                                OnObjectMessage(_objectMessageString, new EventArgs());
                            }
                            _timerWriteText.Enabled = false;
                            _timerScan.Enabled = false;
                            _stringToRead = "";
                            _cancelread = false;
                            _timerReadText.Enabled = true;
                            FileManager.LogMessage("Timer_WriteText_Tick: _timerWriteText.Enabled = false");
                            FileManager.LogMessage("Timer_WriteText_Tick: _timerScan.Enabled = false");
                            FileManager.LogMessage("Timer_WriteText_Tick: _stringToRead = " + _stringToRead);
                            FileManager.LogMessage("Timer_WriteText_Tick: _cancelread = false");
                            FileManager.LogMessage("Timer_WriteText_Tick: _timerReadText.Enabled = true");
                        }
                        return;
                    }
                    else
                    {
                        newMessage = "RFID Tag suchen.";
                        if (!_oxStatus.LastSync.Equals(newMessage))
                        {
                            _objectMessageString = newMessage;
                            OnObjectMessage(_objectMessageString, new EventArgs());
                        }
                    }
                }
                else
                {
                    String newMessage = "RFID Tag suchen.";
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        #endregion

        #region TagIdentifyTimer

        private void Timer_Scan_Tick(object sender, EventArgs e)
        {
            try
            {

                if (DateTime.Compare(DateTime.Now, _readerStartTimeStamp.Add(ReaderMaxTimeOut)) > 0)
                {

                    _cancelread = true;
                    if (ReadTimeout != null)
                    {
                        OnReadTimeout(this, e);
                    }
                }

                if (_cancelread)
                {
                    //Abort reading the TAG
                    String newMessage = "RFID Leser stoppen.";
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                    //OnObjectMessage("RFID Leser stoppen.", new EventArgs());
                    //Console.WriteLine("RFID Leser stoppen.");
                    _timerScan.Enabled = false;
                    OnReadCancel(this, e);
                    return;
                }

                //Read Identifiers of all known transponders
                _ergeb = _reader.Reader_Identify();
                if (_ergeb == 0)
                {
                    //Identifier found, bring into listbox
                    /*
                    if (checkBox_FilterUID.Checked)
                    {
                        if (OLD_UIDText == _reader.UIDText) return;
                    }
                    listBox_UID.Items.Add(_reader.UIDText);
                    OLD_UIDText = _reader.UIDText;
                    listBox_UID.SelectedIndex = listBox_UID.Items.Count - 1;
                     */
                    String newMessage = "RFID Tag gefunden: " + _objectIdentifier;
                    if (!_oxStatus.LastSync.Equals(newMessage))
                    {
                        _objectMessageString = newMessage;
                        OnObjectMessage(_objectMessageString, new EventArgs());
                    }
                    //OnObjectMessage("RFID Tag gefunden: " + ObjectIdentifier, new EventArgs());
                    //Console.WriteLine("RFID Tag gefunden: " + ObjectIdentifier);
                    _objectIdentifier = _reader.UIDText;
                    CommonF.SuccessBeep();
                    _cancelread = true;
                    //OnObjectColor(Color.Gray, new EventArgs());
                    OnObjectIdentified(_objectIdentifier, new EventArgs());
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        #endregion
    }
}