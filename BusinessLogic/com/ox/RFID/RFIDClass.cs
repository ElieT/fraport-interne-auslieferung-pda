﻿using System;
using System.Runtime.InteropServices;

namespace oxmc.BusinessLogic.com.ox.RFID
{
    internal class RFIDClass
    {
        public int DriverSystemMask;
        public int DriverTimeOut;
        public int Port_Initialized;	//Port initialized?!
        public int Reader_Connected;	//Reader connected?!
        public int Reader_Id;			//ID of connected Reader
        public byte Result;				//Result of previous operation
        public byte[] Identifier;			//Identifier
        private byte[] Dummy_Array;		//Data of last Read operation
        public byte[] Read_Array;			//Data of last Read operation
        public byte[] Write_Array;		//Data for next Write operation
        public byte ByteParam;			//Param for last/next parameter operation
        public int Address;			//Address parameter for Read/Write operations
        public byte[] Password;			//Actual Password
        public byte[] NewPassword;		//New Password
        public byte PasswordEnable;		//Send password before read/write operations
        public String DriverName;			//Name of platform dependend driver
        public int From, Length;		//ReadBytes() and WriteBytes() need these parameters
        public bool Lock = false;			//Lock Blocks after WriteBytes() ?
        public int Version_Main_Driver_Engine, Version_Sub_Driver_Engine;
        public int TagType, TagSystem, TagMaxLength, TagxTraParam;

        public byte PortType;
        //0==serial port/CF port
        //1==CF-port via elserial (only for PPC 2000 systems)
        //2==BT serial port
        //4=USB port (only WIN32 systems)
        public int InterfaceType;
        //125==125kHz
        //1356==1356MHz
        //0x10==PEN-BT with memory support

        public enum DriverRFIDSystems
        {
            GROUP_ISO15693 = 0x1,
            GROUP_IIDL = 0x2,
            GROUP_IIDD = 0x4,
            GROUP_IIDG = 0x8,
            GROUP_64BITRO = 0x10,
            GROUP_ICODEUID = 0x20,
            GROUP_ICODE1 = 0x40,
            GROUP_LEGICUID = 0x80,
            GROUP_MIFAREUL = 0x100,
            DEFAULT = 0xFF
        }

        //private float Temp;
        private int i;
        private int Port_Handle;
        private static string complete, zwischen;

        public string PortName = "COM2:";
        private const string DLL_NAME = "iiddrv30_pro.dll";

        #region "DLLIMPORTs iIDDrv30_Pro.Dll"
        [DllImport(DLL_NAME)]
        private static extern byte c_initialize();

        [DllImport(DLL_NAME)]
        private static extern byte c_terminate();

        [DllImport(DLL_NAME)]
        private static extern byte c_identify(byte[] Data);

        [DllImport(DLL_NAME)]
        private static extern byte c_readbytes(byte[] Identifier, int from, int length, byte[] Data);

        [DllImport(DLL_NAME)]
        private static extern byte c_writebytes(byte[] Identifier, int from, int length, byte[] Data, bool locked);

        [DllImport(DLL_NAME)]
        private static extern int c_get_port_state(int ActualPort);

        [DllImport(DLL_NAME)]
        private static extern byte c_get_driver_version(ref	int ver_main, ref int ver_sub);

        [DllImport(DLL_NAME)]
        private static extern byte c_read_reader_id(ref	int reader_id, byte[] Data);


        [DllImport(DLL_NAME)]
        private static extern byte c_set_port_type(byte PortType, string PortName);

        [DllImport(DLL_NAME)]
        private static extern byte c_set_system_mask(int newmask);

        [DllImport(DLL_NAME)]
        private static extern int c_get_system_mask();

        [DllImport(DLL_NAME)]
        private static extern byte c_set_timeout(int newtimeout);

        [DllImport(DLL_NAME)]
        private static extern int c_get_timeout();

        [DllImport(DLL_NAME)]
        private static extern byte c_set_interface_type(int frequency);


        [DllImport(DLL_NAME)]
        private static extern byte c_get_temperature(ref float TELID_Temperature, byte[] Data);

        [DllImport(DLL_NAME)]
        private static extern byte c_get_transponder_parameters(ref int tagtype, ref int pmaxlength, ref int tagsystem, ref int pxtraparam, byte[] Data);

        [DllImport(DLL_NAME)]
        private static extern int c_get_handle();
        #endregion

        public string UIDText
        {
            get
            {
                complete = "";
                for (int i = 0; i <= 7; i++)
                {
                    zwischen = Identifier[i].ToString("X");
                    if (zwischen.Length < 2) { zwischen = "0" + zwischen; }
                    complete = complete + zwischen;
                    if (i < 7) complete = complete + " ";

                }
                return (complete);
            }
        }


        //----------- Get Connection information of reader --------------------
        public void Reader_GetState()
        {
            Port_Handle = c_get_port_state(Port_Handle);
            if (Port_Handle == -1)
            {
                Port_Initialized = 0;
            }
            else
            {
                Port_Initialized = 1;
                Reader_Connected = 0;
                Result = c_read_reader_id(ref Reader_Id, Read_Array);
                if (Result == 0) Reader_Connected = 1;
            }
        }

        //----------- Open the Interface - Port--------------------------------
        public void Reader_OpenInterface()
        {
            Result = c_get_driver_version(ref Version_Main_Driver_Engine, ref Version_Sub_Driver_Engine);
            Result = c_set_interface_type(InterfaceType);
            Result = c_set_port_type(PortType, PortName);
            Result = c_initialize();
            if (Result != 0)
            {
                Port_Initialized = 0;
            }
            else
            {
                Port_Initialized = 1;
                Port_Handle = c_get_handle();
                Reader_Connected = 0;
                Result = c_read_reader_id(ref Reader_Id, Read_Array);
                if (Result == 0) Reader_Connected = 1;
            }
        }

        //----------- Close the Interface -Port --------------------------------
        public void Reader_CloseInterface()
        {
            Result = c_terminate();
            Port_Initialized = 0;
        }


        //----------- Set iID Driver parameters --------------------------------
        public int Driver_SetParameters()
        {
            Result = c_set_system_mask(DriverSystemMask);
            if (Result != 0) return (Result);
            Result = c_set_timeout(DriverTimeOut);
            return (Result);
        }


        //----------- Read Reader-ID --------------------------------------------
        public int Reader_ReadId()
        {
            if (Port_Initialized == 0) return (0xFF);
            Result = c_read_reader_id(ref Reader_Id, Read_Array);
            return (Result);
        }


        //----------- Write Bytes -------------------------------------------
        public int Reader_WriteBytes()
        {
            if (Port_Initialized == 0) return (0xFF);
            Result = c_writebytes(Identifier, From, Length, Write_Array, Lock);
            return (Result);
        }

        //----------- Read Bytes -------------------------------------------
        public int Reader_ReadBytes()
        {
            if (Port_Initialized == 0) return (0xFF);
            Result = c_readbytes(Identifier, From, Length, Read_Array);
            return (Result);
        }

        //----------- Read Identifier -------------------------------------------
        public int Reader_Identify()
        {
            if (Port_Initialized == 0) return (0xFF);
            Result = c_identify(Read_Array);
            if (Result == 0) for (i = 0; i <= 7; i++) Identifier[i] = 0;
            if (Result == 0) for (i = 0; i < Read_Array[0]; i++) Identifier[i] = Read_Array[i + 1];
            if (Result == 0) c_get_transponder_parameters(ref TagType, ref TagMaxLength, ref TagSystem, ref TagxTraParam, Write_Array);
            return (Result);
        }



        //------------------ Konstruktor for RFID_Class ---------------------------
        public RFIDClass()
        {
            //
            // TODO: Konstruktorlogik hier hinzufügen
            //
            Port_Initialized = 0;	//Port initialized?!
            Reader_Connected = 0;	//Reader connected?!
            Reader_Id = 0;	//ID of connected Reader
            Result = 0xFF;	//Result of previous operation
            ByteParam = 0;	//Param for last/next parameter operation
            Address = 0;	//Address parameter for Read/Write operations

            i = 0;	//counter
            Port_Handle = 0;	//Handle of serial Port
            PortType = 0;	//standard port = serial port
            InterfaceType = 1356;	//standard frequency = 1356MHz

            PasswordEnable = 0;
            DriverName = DLL_NAME;
            DriverSystemMask = c_get_system_mask();
            DriverTimeOut = c_get_timeout();
            Identifier = new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            Dummy_Array = new byte[0x20];
            Read_Array = new byte[0x8FF];
            Write_Array = new byte[0x8FF];
            Version_Main_Driver_Engine = 0;
            Version_Sub_Driver_Engine = 0;


        }
    }
}