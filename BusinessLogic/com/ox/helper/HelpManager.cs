﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class HelpManager
    {
        private const string PingString = "client_ping";
        private static readonly object Lock = new object();

        /// <summary>
        /// Saves the database and the logfiles and tries to upload the files to the Backend as a zipfile
        /// </summary>
        public static void TriggerDataSave(EventHandler startHandler, EventHandler endHandler, EventHandler progress)
        {
            ZipOutputStream zipStream = null;
            try
            {
                lock (Lock)
                {
                    Monitor.Enter(LockHelper.Locker);
                    //ensure that no filewrite-operations are occuring
                    var dateTime = DateTime.Now.AddSeconds(30);
                    //ensure that no Database-Operation is occuring
                    while ((!OxDbManager.GetInstance().TransactionActive || !FileManager.AccessAllowed))
                    {
                        if (DateTime.Now > dateTime)
                        {
                            throw new Exception("Exklusiver Zugriff auf Daten nicht möglich. Bitte Vorgang wiederholen.");
                        }
                    }
                    OxDbManager.GetInstance().CommitTransaction();
                    startHandler(null, EventArgs.Empty);
                    Monitor.Enter(FileManager.Locker);
                    Monitor.Enter(OxDbManager.Locker);
                    FileManager.AccessAllowed = false;
                    OxDbManager.GetInstance().AccessAllowed = false;

                    progress(10, EventArgs.Empty);


                    //stop the webserver to avoid multiple calls
                    //WebServer.GetInstance().Stop();

                    string codeBase = Assembly.GetExecutingAssembly().GetName().CodeBase;


                    //var uri = new UriBuilder(codeBase);
                    var d = Uri.UnescapeDataString(codeBase);

                    var slash = "";
                    if (d.StartsWith("file:///"))
                    {
                        d = d.Substring(8);
                    }
                    if (d.StartsWith("\\"))
                        slash = "\\";
                    else
                        slash = "/";

                    //Substring: Eine Möglichkeit um den String auseinander zu nehmen; LastIndexOf: Eine Möglichkeit anzugeben wo er bei dem String aufhören soll
                    var basePath = AppConfig.GetAppPath();
                    var pathDB = basePath + slash + "AssetManagementDB_SQLite.sqlite";
                    var pathLog = basePath + slash + "log";
                    var files = Directory.GetFiles(pathLog).ToList();
                    var path = basePath;


                    //=== BEGIN 2012-10-24 ===
                    //backup logfiles before creating zip
                    if (!Directory.Exists(path + "\\Backup\\"))
                        Directory.CreateDirectory(path + "\\Backup\\");
                    foreach (var file in files)
                    {
                        File.Copy(file, (path + "\\Backup\\" + file.Substring(file.LastIndexOf('\\'))), true);
                    }
                    //=== END 2012-10-24 ===


                    //=== BEGIN 2012-10-30 ===
                    //changed adding of files to zipfile because of errors caused by previous implementation
                    var backupFiles = Directory.GetFiles(path + "\\Backup\\").ToList();

                    pathLog = path + "\\Backup\\";

                    if (!Directory.Exists(path + "\\data\\"))
                        Directory.CreateDirectory(path + "\\data\\");
                    var zipFile = File.Create(path + "\\data\\!LogFiles.zip");
                    using (zipStream = new ZipOutputStream(zipFile))
                    {
                        zipStream.SetLevel(3);

                        var fi = new FileInfo(pathDB);
                        var entryName = "AssetManagementDB_SQLite.sqlite";
                        entryName = ZipEntry.CleanName(entryName);
                        var newEntry = new ZipEntry(entryName);
                        newEntry.DateTime = fi.LastWriteTime;
                        newEntry.Size = fi.Length;
                        zipStream.PutNextEntry(newEntry);
                        var buffer = new byte[4096];
                        var readSuccessful = false;
                        for (int i = 0; i < 5; i++)
                        {
                            try
                            {
                                using (var streamReader = File.OpenRead(pathDB))
                                {
                                    StreamUtils.Copy(streamReader, zipStream, buffer);
                                }
                                readSuccessful = true;
                                break;
                            }
                            catch (IOException ex)
                            {
                                //do nothing
                                Thread.Sleep(2000);
                            }
                        }
                        if (!readSuccessful)
                            throw new Exception("Exklusiver Zugriff auf Daten nicht möglich. Bitte Vorgang wiederholen.");

                        zipStream.CloseEntry();
                        foreach (var log in backupFiles)
                        {
                            fi = new FileInfo(log);
                            entryName = log.Substring(pathLog.Length);
                            entryName = ZipEntry.CleanName(entryName);
                            newEntry = new ZipEntry(entryName);
                            newEntry.DateTime = fi.LastWriteTime;
                            newEntry.Size = fi.Length;
                            zipStream.PutNextEntry(newEntry);
                            buffer = new byte[4096];
                            using (var streamReader = File.OpenRead(log))
                            {
                                StreamUtils.Copy(streamReader, zipStream, buffer);
                            }
                            zipStream.CloseEntry();
                        }

                        zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
                        zipStream.Close();
                    }
                    //=== END 2012-10-30 ===

                    //=== BEGIN 2012-10-24 ===
                    //delete backupfolder
                    if (Directory.Exists(path + "\\Backup\\"))
                    {
                        foreach (var file in backupFiles)
                        {
                            File.Delete(file);
                        }
                        Directory.Delete(path + "\\Backup\\");
                    }
                    //=== END 2012-10-24 ===

                    progress(50, EventArgs.Empty);

                    Monitor.Exit(FileManager.Locker);
                    FileManager.AccessAllowed = true;
                    Monitor.Exit(OxDbManager.Locker);
                    OxDbManager.GetInstance().AccessAllowed = true;

                    if (!String.IsNullOrEmpty(zipFile.Name))
                    {
                        // Create database entry
                        OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now,
                                                                   "Daten erfolgreich geschrieben", null, false, 5000);
                        var header = new DocUpload();
                        header.Filename = "!LogFiles.zip";
                        header.OrgFilename = "LogFiles.zip";
                        header.Description = "Zip_LogFiles";
                        header.Email = "";
                        header.Timestamp = DateTime.Now;
                        //header.Synctimestamp = DateTime.Now;
                        header.RefType = "OR";
                        header.DocType = "ZIP";
                        header.Recordname = "MC_DOCU_H_BIN";
                        header.RecordnameBin = "MC_DOCU_BIN";
                        header.FileLocation = path + "\\data\\!LogFiles.zip";
                        header.RefObject = "";
                        header.RefType = "";
                        header.DocSize = FileManager.GetFileSize(header);
                        header.Spras = "D";
                        header.Email = AppConfig.Email1;
                        header.MobileKey = "";
                        FileManager.AccessAllowed = true;
                        OxDbManager.GetInstance().AccessAllowed = true;

                        progress(60, EventArgs.Empty);
                        DocUploadManager.SaveDoc(header);

                        // Try to upload file
                        NameValueCollection parameter = new NameValueCollection();
                        parameter.Add("pa_ping", PingString);
                        parameter.Add("userid", AppConfig.UserId);
                        parameter.Add("pw", AppConfig.Pw);
                        OxStatus.GetInstance().SapLocnt = "0";
                        OxStatus.GetInstance().SapUserDetails = SyncManager.GetInstance().GetUserName(parameter,
                                                                                                      "user_details.htm",
                                                                                                      true);

                        if (OxStatus.GetInstance().SapUserDetails.IndexOf(PingString) != -1)
                        {
                            OxStatus.GetInstance().SapAvailable = true;
                        }
                        else
                        {
                            OxStatus.GetInstance().SapAvailable = false;
                            OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now,
                                                                       "30012: " +
                                                                       "Es konnte keine Verbindung zum SAP hergestellt werden. Bitte versuchen Sie es später erneut.", null, true, 8000);
                        }
                        if (OxStatus.GetInstance().SapAvailable)
                        {
                            progress(75, EventArgs.Empty);
                            SyncManager.GetInstance().UploadFiles("ZIP");
                            endHandler(null, EventArgs.Empty);
                            OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
                                                                       "Datei erfolgreich gespeichert!", null, true, 5000);

                            progress(100, EventArgs.Empty);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                try { Monitor.Exit(OxDbManager.Locker); }
                catch { }
                try { Monitor.Exit(FileManager.Locker); }
                catch { }
                OxDbManager.GetInstance().CommitTransaction();
                FileManager.AccessAllowed = true;
                OxDbManager.GetInstance().AccessAllowed = true;
                progress(100, EventArgs.Empty);
                endHandler(null, EventArgs.Empty);

                try { Monitor.Exit(LockHelper.Locker); }
                catch { }
            }
        }
    }
}
