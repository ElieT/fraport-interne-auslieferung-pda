﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class UserManager
    {
        public static Boolean CheckUser(String mandt, String userid, byte[] passHash)
        {
            Boolean retVal = false;
            String sqlStmt = "SELECT * FROM S_USER WHERE MANDT = @MANDT AND USERID = @USERID AND PASSHASH = @PASSHASH";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MANDT", mandt);
            cmd.Parameters.AddWithValue("@USERID", userid);
            cmd.Parameters.AddWithValue("@PASSHASH", passHash);

            var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

            foreach (Dictionary<string, string> rdr in records)
            {
                retVal = true;
            }
            return retVal;
        }

        public static Boolean UserExists(String mandt, String userid)
        {
            Boolean retVal = false;
            String sqlStmt = "SELECT * FROM S_USER WHERE MANDT = @MANDT AND USERID = @USERID";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MANDT", mandt);
            cmd.Parameters.AddWithValue("@USERID", userid);

            var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

            foreach (Dictionary<string, string> rdr in records)
            {
                retVal = true;
            }
            return retVal;
        }

        public static int GetNumberOfRefusedUpdates(String mandt, String userid)
        {
            int retVal = 0;
            var sqlStmt = "SELECT REFUSED_UPDATE FROM S_USER WHERE MANDT = @MANDT AND USERID = @USERID";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MANDT", mandt);
            cmd.Parameters.AddWithValue("@USERID", userid);

            var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

            if (records.Count == 1)
            {
                var num = records[0]["REFUSED_UPDATE"];
                if (string.IsNullOrEmpty(num))
                    num = "0";
                else
                    num = records[0]["REFUSED_UPDATE"];
                retVal = int.Parse(num);
            }
            return retVal;
        }

        public static void AddRefusedUpdate(String mandt, String userid)
        {
            var sqlStmt = "UPDATE S_USER SET REFUSED_UPDATE = @REFUSED_UPDATE WHERE MANDT = @MANDT AND USERID = @USERID";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MANDT", mandt);
            cmd.Parameters.AddWithValue("@USERID", userid);
            cmd.Parameters.AddWithValue("@REFUSED_UPDATE", GetNumberOfRefusedUpdates(mandt, userid) + 1);

            OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
        }

        public static Boolean CreateUser(String mandt, String userid, byte[] passHash)
        {
            OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
            Boolean retVal = false;
            try
            {
                // User anlegen
                String sqlStmt = "INSERT OR REPLACE INTO [S_USER] " +
                          "(MANDT, USERID, PASSHASH, NAME_FIRST, NAME_LAST, LAST_LOGIN_DATE, LAST_LOGIN_TIME, CREATE_DATE, CREATE_TIME, USER_LOCKED) " +
                          "Values (@MANDT, @USERID, @PASSHASH, @NAME_FIRST, @NAME_LAST, @LAST_LOGIN_DATE, @LAST_LOGIN_TIME, @CREATE_DATE, @CREATE_TIME, @USER_LOCKED)";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", mandt);
                cmd.Parameters.AddWithValue("@USERID", userid);
                cmd.Parameters.AddWithValue("@PASSHASH", passHash);
                cmd.Parameters.AddWithValue("@NAME_FIRST", "Test User");
                cmd.Parameters.AddWithValue("@NAME_LAST", "Test User");
                cmd.Parameters.AddWithValue("@LAST_LOGIN_DATE", DateTime.Today.ToString("yyyMMdd"));
                cmd.Parameters.AddWithValue("@LAST_LOGIN_TIME", DateTime.Today.ToString("HHmmss"));
                cmd.Parameters.AddWithValue("@CREATE_DATE", DateTime.Today.ToString("yyyMMdd"));
                cmd.Parameters.AddWithValue("@CREATE_TIME", DateTime.Today.ToString("HHmmss"));
                cmd.Parameters.AddWithValue("@USER_LOCKED", "");

                var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                if (records.Count == 1)
                {
                    if (records[0]["RetVal"].Equals("1"))
                        retVal = true;
                }
                else
                    retVal = false;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static Boolean UpdateUser(String mandt, String userid)
        {
            OxDbManager.GetInstance().CreateDeviceGuid(AppConfig.Mandt, AppConfig.UserId);
            Boolean retVal = false;
            String sqlStmt = "UPDATE S_USER SET LAST_LOGIN_DATE = @LAST_LOGIN_DATE, LAST_LOGIN_TIME = @LAST_LOGIN_TIME WHERE MANDT = @MANDT AND USERID = @USERID";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MANDT", mandt);
            cmd.Parameters.AddWithValue("@USERID", userid);
            cmd.Parameters.AddWithValue("@LAST_LOGIN_DATE", DateTime.Today.ToString("yyyMMdd"));
            cmd.Parameters.AddWithValue("@LAST_LOGIN_TIME", DateTime.Today.ToString("HHmmss"));

            var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
            var result = records[0]["RetVal"];
            if (result.Equals("1"))
                retVal = true;

            return retVal;
        }
        public static Boolean ChangePW(String mandt, String userid, byte[] passHash)
        {
            Boolean retVal = false;
            String sqlStmt = "UPDATE S_USER SET PASSHASH = @PASSHASH WHERE MANDT = @MANDT AND USERID = @USERID";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@PASSHASH", passHash);
            cmd.Parameters.AddWithValue("@MANDT", mandt);
            cmd.Parameters.AddWithValue("@USERID", userid);

            var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

            var result = records[0]["RetVal"];
            if (result.Equals("1"))
                retVal = true;

            return retVal;
        }

        public static Boolean ResetUser(String mandt, String userid)
        {
            Boolean retVal = false;
            String sqlStmt = "DELETE FROM S_USER WHERE MANDT = @MANDT AND USERID = @USERID";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MANDT", mandt);
            cmd.Parameters.AddWithValue("@USERID", userid);

            var records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);

            var result = records[0]["RetVal"];
            if (result.Equals("1"))
                retVal = true;

            return retVal;
        }
    }
}
