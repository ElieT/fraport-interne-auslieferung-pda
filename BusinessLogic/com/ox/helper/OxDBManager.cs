using System;
using System.Collections;
using System.Data;
using System.Data;
using System.Data.SQLite;

using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class OxDbManager
    {

        private static List<ThDataType> _tableStructure;
        private static Hashtable _tableList;
        private static SQLiteCommand _sqlCeCommand;
        private static Stack _scStack = new Stack();

        private readonly SQLiteConnection _dbConnection;

        private static readonly object Padlock = new object();
        private static OxDbManager _instance;
        private readonly SQLiteConnection _transConnection;
        private SQLiteCommand _sqlTransCommand;
        private SQLiteTransaction _transaction;
        private bool _rolledBack;
        private static Dictionary<String, String> _coolSqlSelectList;
        private static Dictionary<String, String> _coolSqlInsertList;
        private static Dictionary<String, String> _coolSqlUpdateList;
        private static Dictionary<String, String> _coolSqlDeleteList;
        private static String _lastInitialisedTable;
        private static Boolean _connOpen;
        public enum AccessType { CReader, CScalar, CUpdate, CInsert, CDelete };

        #region Threading
        private static object _locker = new object();
        private static object _locker2 = new object();
        public static object Locker { get { return _locker; } set { _locker = value; } }
        private static int _monEnter;

        public bool AccessAllowed { get; set; }
        public bool TransactionActive { get; private set; }
        private List<SQLiteCommand> _cmdList  = new List<SQLiteCommand>();

        /// <summary>
        /// Creates a new Instance of OxDbManager
        /// Using Singleton to allow exclusive connections
        /// </summary>
        private OxDbManager()
        {
            try
            {
                //_dbConnection = new SqlCeConnection(AppConfig.DatabasePath);
                _dbConnection = new SQLiteConnection(AppConfig.DatabasePath);
                _transConnection = new SQLiteConnection(AppConfig.DatabasePath);

                AccessAllowed = true;
                TransactionActive = false;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        /// <summary>
        /// Singletonmethod to initialise and/or return the Instance of OxDbManager
        /// </summary>
        /// <returns>Returns the instance of OxDbManager</returns>
        public static OxDbManager GetInstance()
        {
            lock (Padlock)
            {
                if (_instance == null)
                {
                    _instance = new OxDbManager();
                }
                return _instance;
            }
        }

        public static void ClearInstance()
        {
            _instance = null;
        }

        /// <summary>
        /// Returns the currently active DbConnection
        /// </summary>
        public SQLiteConnection DbConnection
        {
            get { return _dbConnection; }
        }

        public static Hashtable TableList
        {
            get { return _tableList; }
        }

        public static List<ThDataType> TableStructure
        {
            get
            {
                if (_tableStructure == null) _tableStructure = new List<ThDataType>();
                return _tableStructure;
            }
            set { _tableStructure = value; }
        }

        /// <summary>
        /// Returns the sqlite command variable for the current transactional connection
        /// </summary>
        public SQLiteCommand SqlTransCommand
        {
            get { return _sqlTransCommand; }
        }

        #endregion
        private List<Dictionary<string, string>> PrepareDbCommand(SQLiteCommand cmd)
        {

            if (cmd.CommandText.IndexOf("SELECT * FROM sqlite_master") == -1)
                cmd.CommandText = cmd.CommandText.ToUpper();

            if (cmd.CommandText.IndexOf("INSERT INTO") != -1)
                return DbCheckAndExecute(cmd, AccessType.CInsert);
            if (cmd.CommandText.IndexOf("INSERT OR REPLACE INTO") != -1)
                return DbCheckAndExecute(cmd, AccessType.CInsert);
            if (cmd.CommandText.StartsWith("DELETE"))
                return DbCheckAndExecute(cmd, AccessType.CDelete);
            if (cmd.CommandText.StartsWith("SELECT"))
                return DbCheckAndExecute(cmd, AccessType.CReader);
            return DbCheckAndExecute(cmd, AccessType.CUpdate);
        }

        private DataTable GetSchema(String table)
        {
            string sql = @"SELECT  * FROM [" + table + "]";
            var cmd = new SQLiteCommand(sql);
            if (_transConnection.State != ConnectionState.Open)
            {
                _transConnection.Open();
                _transaction = _transConnection.BeginTransaction();
            }
            cmd.Connection = _transConnection;
            cmd.Transaction = _transaction;
            SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            DataTable schemaTable = reader.GetSchemaTable();
            return schemaTable;
        }

        private List<Dictionary<string, string>> DbCheckAndExecute(SQLiteCommand cmd, AccessType at)
        {
            String table = GetTableNames(cmd.CommandText)[0];

            switch (at)
            {
                case AccessType.CReader: // Data Reader for SELECT statements
                    // TODO: Be sure condition includes updflag <> 0 so no old original dblines will be selected
                    if (table.StartsWith("D_") ||
                        table.StartsWith("C_") ||
                        table.StartsWith("G_") ||
                        table.StartsWith("[C_") ||
                        table.StartsWith("[D_") ||
                        table.StartsWith("[G_"))
                        cmd = SQLChange_Select(cmd);

                    return DbExecute(cmd, at);
                case AccessType.CScalar: // Execute SELECT SINGLE / SELECT MAX statements
                    // TODO: Be sure condition includes updflag <> 0 so no old original dblines will be selected
                        cmd = SQLChange_Select(cmd);
                    return DbExecute(cmd, at);
                case AccessType.CInsert: // Execute INSERT Statements
                    // TODO: Be sure the updflag is set to I
                    if (table.StartsWith("D_"))
                    {
                        cmd = SQLChange_Insert(cmd);
                        UpdateSyncDelta(table);
                    }
                    return DbExecute(cmd, at);
                case AccessType.CUpdate: // Execute UPDATE Statements
                    // TODO: Insert those dblines marked with updflag 0
                    if (table.StartsWith("D_"))
                    {
                        cmd = SQLChange_Update(cmd);
                        UpdateSyncDelta(table);
                    }
                    return DbExecute(cmd, at);
                case AccessType.CDelete: // Execute DELETE Statements
                    if (table.StartsWith("D_"))
                    {
                        cmd = SQLChange_Delete(cmd);
                        UpdateSyncDelta(table);
                    }
                    return DbExecute(cmd, at);
            }
            return null;
        }

        /// <summary>
        /// Alters an updatestatement to include whereclauses for dirtyflag and updateflag. Also, if necessary, a cloned record is 
        /// inserted into the database to preserve the original state of the record as provided by the Backend
        /// </summary>
        /// <param name="oxDbCommand">OxDbCommand containing the sql-statement and the parameters</param>
        /// <returns></returns>
        private SQLiteCommand SQLChange_Update(SQLiteCommand cmd)
        {
            try
            {
                if (cmd.Parameters.IndexOf("@UPDFLAG") >= 0 && cmd.Parameters["@UPDFLAG"].Value.Equals("S"))
                {
                    cmd.Parameters["@UPDFLAG"].Value = "";
                    return cmd;
                }

                var cmd2 = new SQLiteCommand(cmd.CommandText, cmd.Connection, cmd.Transaction);
                var tablename = GetTableNames(cmd.CommandText)[0];
                ThDataType thDataType = GetThDataType(tablename);
                if (thDataType.ColumnNames.Contains("DIRTY"))
                {
                    cmd2.CommandText = "SELECT * FROM " + tablename;

                    //if (cmd2.CommandText.IndexOf(" WHERE ") != -1)
                    if (cmd.CommandText.IndexOf(" WHERE ") != -1)
                        cmd2.CommandText += " WHERE " + Regex.Split(cmd.CommandText, "WHERE")[1] + " AND DIRTY <> 'X'";
                    else
                        cmd2.CommandText += " WHERE DIRTY <> 'X'";
                    for (var i = 0; i < cmd.Parameters.Count; i++)
                    {
                        if (cmd2.CommandText.IndexOf(cmd.Parameters[i].ParameterName) != -1)
                            cmd2.Parameters.AddWithValue(cmd.Parameters[i].ParameterName, cmd.Parameters[i].Value);
                    }
                    var records = DbExecute(cmd2, AccessType.CReader);
                    var cmd3 = new SQLiteCommand(GetCoolSqlSelect(tablename, GetThDataType(tablename)), cmd.Connection,
                                                 cmd.Transaction);
                    foreach (var result in records)
                    {
                        foreach (var field in result)
                            if (field.Key.ToUpper().Equals("UPDFLAG"))
                                cmd3.Parameters.AddWithValue(field.Key, "0");
                            else
                                cmd3.Parameters.AddWithValue(field.Key, field.Value);
                        if (GetThDataType(tablename).ColumnNames.Contains("DIRTY"))
                            cmd3.Parameters.AddWithValue("@DIRTY", "X");
                        var record2 = DbExecute(cmd3, AccessType.CReader);
                        if (record2.Count == 0)
                        {
                            var cmdInsert = new SQLiteCommand(GetCoolSqlInsert(tablename, GetThDataType(tablename)),
                                                              cmd.Connection, cmd.Transaction);
                            foreach (var para in cmd3.Parameters)
                                cmdInsert.Parameters.Add(para);
                            DbExecute(cmdInsert, AccessType.CInsert);
                        }
                    }
                    if (cmd.CommandText.IndexOf(" WHERE ") != -1)
                    {
                        cmd.CommandText = Regex.Split(cmd.CommandText, "WHERE")[0] + " WHERE " +
                                          Regex.Split(cmd.CommandText, "WHERE")[1] + " AND DIRTY <> 'X'" + " AND " + tablename + ".MANDT = @" + tablename +
                                           "_MANDT" + " AND " + tablename + ".USERID = @" + tablename + "_USERID";
                        cmd.Parameters.AddWithValue("@" + tablename + "_MANDT", AppConfig.Mandt);
                        cmd.Parameters.AddWithValue("@" + tablename + "_USERID", AppConfig.UserId);
                    }
                    else
                        cmd.CommandText += " WHERE DIRTY <> 'X'";
                }
                if (cmd.CommandText.IndexOf("UPDFLAG") == -1 && thDataType.ColumnNames.Contains("UPDFLAG"))
                {
                    var sqlStmtParts = Regex.Split(cmd.CommandText, " WHERE ");
                    cmd.CommandText = sqlStmtParts[0] + ", UPDFLAG = @UPDFLAG WHERE " + sqlStmtParts[1];
                    cmd.Parameters.AddWithValue("@UPDFLAG", "U");
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return cmd;
        }

        /// <summary>
        /// Changes a delete-statement to include parameters for mandt and userid
        /// If the update comes from the backend, the parameters are just getting appended
        /// If the update comes from the applicaton, it is ensured, that only data that has not been synchronised can be deleted.
        /// </summary>
        /// <param name="oxDbCommand">OxDbCommand containing the sql-statement and the parameters</param>
        /// <returns></returns>
        private SQLiteCommand SQLChange_Delete(SQLiteCommand cmd)
        {
            var tablename = GetTableNames(cmd.CommandText)[0];
            if (tablename.StartsWith("S_") || cmd.CommandText.IndexOf(" WHERE ") == -1)
                return cmd;
            if (!(cmd.Parameters.IndexOf("@UPDFLAG") < 0))
            {
                if (cmd.Parameters["@UPDFLAG"].Value.Equals("S"))
                {
                    cmd.CommandText += " AND " + tablename + ".MANDT = @" + tablename + "_MANDT";
                    cmd.CommandText += " AND " + tablename + ".USERID = @" + tablename + "_USERID";
                    cmd.Parameters.AddWithValue("@" + tablename + "_MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@" + tablename + "_USERID", AppConfig.UserId);
                    return cmd;
                }
            }

            var sqlSelect = "SELECT * FROM " + Regex.Split(cmd.CommandText, "DELETE FROM")[1];
            var cmd2 = new SQLiteCommand(sqlSelect, cmd.Connection, cmd.Transaction);
            foreach (var par in cmd.Parameters)
                cmd2.Parameters.Add(par);

            var record = DbExecute(cmd2, AccessType.CReader);
            foreach (var result in record)
            {
                String updFlag = result["UPDFLAG"];
                if (updFlag.Equals("I"))
                {
                    cmd.CommandText += " AND " + tablename + ".MANDT = @" + tablename + "_MANDT";
                    cmd.CommandText += " AND " + tablename + ".USERID = @" + tablename + "_USERID";
                    cmd.Parameters.AddWithValue("@" + tablename + "_MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@" + tablename + "_USERID", AppConfig.UserId);
                }
                else
                {
                    Exception ex = new Exception("Deletion of ERP data not allowed: " + cmd.CommandText);
                    cmd.CommandText = "";
                    FileManager.LogException(ex);
                }
            }
            return cmd;
        }

        /// <summary>
        /// Changes an insertstatement to have an updateflag set to I (Insert), thus ensuring the dataset will by synchronised to the backend
        /// </summary>
        /// <param name="oxDbCommand">OxDbCommand containing the sql-statement and the parameters</param>
        /// <returns></returns>
        private SQLiteCommand SQLChange_Insert(SQLiteCommand cmd)
        {
            if (cmd.Parameters.IndexOf("@UPDFLAG") > 0 && cmd.Parameters["@UPDFLAG"].Value.Equals("S"))
            {
                cmd.Parameters["@UPDFLAG"].Value = "";
                return cmd;
            }
            var tablename = GetTableNames(cmd.CommandText)[0];
            if (cmd.CommandText.IndexOf("UPDFLAG") == -1 && tablename.IndexOf("INFORMATION_SCHEMA.TABLES") == -1 && tablename.IndexOf("HLP_COLUMN") == -1 && GetThDataType(tablename).ColumnNames.Contains("UPDFLAG"))
            {
                String[] sqlStmtParts = Regex.Split(cmd.CommandText, "VALUES");
                String sqlStmt2 = sqlStmtParts[0].Substring(0, cmd.CommandText.IndexOf(")"));
                String sqlStmt3 = sqlStmtParts[1].Substring(0, cmd.CommandText.IndexOf(")"));
                cmd.CommandText = sqlStmt2 + " , UPDFLAG) VALUES " + sqlStmt3 + ", @UPDFLAG)";
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
            }
            return cmd;
        }

        /// <summary>
        /// Changes a selectstatement to have have whereclauses limiting the resultset to the users client (mandt), userid and Updateflag = 0.
        /// This ensures, that the users has only accecc to his/her own data and that the data dataset is never the cloned version used for the sync matching.
        /// </summary>
        /// <param name="oxDbCommand">OxDbCommand containing the sql-statement and the parameters</param>
        /// <returns></returns>
        private SQLiteCommand SQLChange_Select(SQLiteCommand cmd)
        {
            List<String> tablenames = GetTableNames(cmd.CommandText);
            if (cmd.CommandText.IndexOf("DIRTY = 'X'") != -1)
                return cmd;
            String SqlOrderBySuffix = "";
            if (cmd.CommandText.IndexOf("ORDER BY") != -1)
            {
                SqlOrderBySuffix = Regex.Split(cmd.CommandText, "ORDER BY")[1];
                cmd.CommandText = Regex.Split(cmd.CommandText, "ORDER BY")[0];
            }
            foreach (var tablename in tablenames)
                if (tablename.IndexOf("sqlite_master") == -1 && tablename.IndexOf("HLP_COLUMN") == -1 && (tablename.StartsWith("C_") || tablename.StartsWith("D_")  || tablename.StartsWith("G_") || tablename.StartsWith("[C_") || tablename.StartsWith("[D_") || tablename.StartsWith("[G_")))
                {
                    //append MANDT and USERID
                    var index = cmd.CommandText.IndexOf(" JOIN " + tablename + " ON ");
                    if (index != -1)
                    {
                        var length = (" JOIN " + tablename + " ON ").Length;

                        if (!tablename.StartsWith("[G_") && !tablename.StartsWith("G_"))
                            cmd.CommandText = cmd.CommandText.Insert(index + length,
                                                                     " " + tablename + ".MANDT = @" +
                                                                     tablename + "_MANDT  AND " +
                                                                     tablename + ".USERID = @" +
                                                                     tablename + "_USERID AND " +
                                                                     tablename + ".UPDFLAG <> @" +
                                                                     tablename + "_UPDFLAG AND ");
                        else
                            cmd.CommandText = cmd.CommandText.Insert(index + length,
                                                                     " " + tablename + ".MANDT = @" +
                                                                     tablename + "_MANDT  AND " +
                                                                     tablename + ".UPDFLAG <> @" +
                                                                     tablename + "_UPDFLAG AND ");
                        cmd.Parameters.AddWithValue( "@" + tablename + "_UPDFLAG", "0");
                        cmd.Parameters.AddWithValue( "@" + tablename + "_MANDT", AppConfig.Mandt);
                        if (!tablename.StartsWith("[G_") && !tablename.StartsWith("G_"))
                            cmd.Parameters.AddWithValue( "@" + tablename + "_USERID", AppConfig.UserId);
                    }
                    else
                    {
                        if (cmd.CommandText.IndexOf(" WHERE ") == -1)
                        {
                            cmd.CommandText += " WHERE " + tablename + ".UPDFLAG <> @" + tablename + "_UPDFLAG";
                            cmd.Parameters.AddWithValue("@" + tablename + "_UPDFLAG", "0");
                        }
                        else
                        {
                            cmd.CommandText += " AND " + tablename + ".UPDFLAG <> @" + tablename + "_UPDFLAG";
                            if (cmd.Parameters.IndexOf("@" + tablename + "_UPDFLAG") < 0)
                                cmd.Parameters.AddWithValue("@" + tablename + "_UPDFLAG", "0");
                        }
                        cmd.CommandText += " AND " + tablename + ".MANDT = @" + tablename + "_MANDT";
                        if (!tablename.StartsWith("[G_") && !tablename.StartsWith("G_"))
                            cmd.CommandText += " AND " + tablename + ".USERID = @" + tablename + "_USERID";
                        if (cmd.Parameters.IndexOf("@" + tablename + "_MANDT") < 0)
                            cmd.Parameters.AddWithValue("@" + tablename + "_MANDT", AppConfig.Mandt);
                        if (!tablename.StartsWith("[G_") && !tablename.StartsWith("G_") && cmd.Parameters.IndexOf("@" + tablename + "_USERID") < 0)
                            cmd.Parameters.AddWithValue("@" + tablename + "_USERID", AppConfig.UserId);
                    }
                }
            if (!String.IsNullOrEmpty(SqlOrderBySuffix))
                cmd.CommandText += " ORDER BY " + SqlOrderBySuffix;
            return cmd;
        }

        /// <summary>
        /// Executes an OxDBCommand according to its Accesstype.
        /// This Method also adds the parameters associated to this object to the SQLitecommand
        /// </summary>
        /// <param name="cmd">SQLiteCommand to be executed</param>
        /// <param name="at">Accesstype defining what type of execution is to be performed</param>
        /// <returns></returns>
        public List<Dictionary<string, string>> DbExecute(SQLiteCommand cmd, AccessType at)
        {
            // Return list
            var ret = new List<Dictionary<string, string>>();
            SQLiteDataReader rdr = null;

            // Database connection);
            try
            {
                var dbLine = new Dictionary<string, string>();
                switch (at)
                {
                    case AccessType.CReader: // Data Reader for SELECT statements
                        rdr = cmd.ExecuteReader();
                        var fc = rdr.FieldCount;
                        var keys = new string[fc];
                        var isSingleRetVal = false;
                        var isMasterTable = false;
                        if (cmd.CommandText.StartsWith("SELECT MAX(") || cmd.CommandText.StartsWith("SELECT MIN(") ||
                                        cmd.CommandText.StartsWith("SELECT COUNT(") || cmd.CommandText.StartsWith("SELECT AVG("))
                            isSingleRetVal = true;

                        if (cmd.CommandText.StartsWith("SELECT * FROM sqlite_master"))
                            isMasterTable = true;

                        var value = "";
                        for (var i = 0; i < fc; i++)
                        {
                            keys[i] = rdr.GetName(i);
                        }
                        while (rdr.Read())
                        {
                            var dbLine1 = new Dictionary<string, string>();
                            for (var i = 0; i < fc; i++)
                            {
                                if (!dbLine1.ContainsKey(keys[i]))
                                {
                                    if (isSingleRetVal)
                                    {
                                        keys[i] = "RetVal";
                                        if (rdr.IsDBNull(i))
                                            value = "";
                                        else
                                            value = "" + rdr.GetValue(i);
                                    }
                                    else if (isMasterTable)
                                    {
                                        if (rdr.IsDBNull(i))
                                            value = "";
                                        else {
                                        if (i < 3)
                                            value = "" + rdr.GetString(i);
                                        else
                                            value = "";
                                        }
                                    }
                                    else
                                        if (rdr.IsDBNull(i))
                                            value = "";
                                        else
                                            value = "" + rdr.GetString(i);
                                    dbLine1.Add(keys[i], value);
                                }
                            }
                            ret.Add(dbLine1);
                        }
                        rdr.Close();
                        break;
                    case AccessType.CScalar: // Execute SELECT SINGLE / SELECT MAX statements
                        dbLine.Add("RetVal", cmd.ExecuteScalar().ToString());
                        ret.Add(dbLine);
                        break;
                    case AccessType.CInsert: // Execute INSERT Statements
                        dbLine.Add("RetVal", cmd.ExecuteNonQuery().ToString());
                        ret.Add(dbLine);
                        break;
                    case AccessType.CUpdate: // Execute UPDATE Statements
                        dbLine.Add("RetVal", cmd.ExecuteNonQuery().ToString());
                        ret.Add(dbLine);
                        break;
                    case AccessType.CDelete: // Execute UPDATE Statements
                        dbLine.Add("RetVal", cmd.ExecuteNonQuery().ToString());
                        ret.Add(dbLine);
                        break;
                }
            }
            catch (Exception ex)
            {
                FileManager.LogMessage(cmd.CommandText);
                FileManager.LogException(ex);
            }
            finally
            {
                if (rdr != null && !rdr.IsClosed)
                    rdr.Close();
            }
            return ret;
        }

        //public List<Dictionary<string, string>> FeedTransaction(SQLiteCommand cmd)
        //{
        //    if (_transConnection.State != ConnectionState.Open)
        //    {
        //        _transConnection.Open();
        //        _transaction = _transConnection.BeginTransaction();
        //    }
        //    cmd.Connection = _transConnection;
        //    cmd.Transaction = _transaction;
        //    return PrepareDbCommand(cmd);
        //}

        public List<Dictionary<string, string>> FeedTransaction(SQLiteCommand cmd)
        {
            Monitor.Enter(Locker);
            _monEnter++;
            //AccessAllowed = true;
            if (AccessAllowed)
            {
                TransactionActive = false;
                cmd = OxDbManager.GetInstance().GetConnection(cmd, false);

                //FileManager.LogMessage("Feed: Calling Thread: " + Thread.CurrentThread.Name + " " + Thread.CurrentThread.ManagedThreadId);
                _cmdList.Add(cmd);
                var res = PrepareDbCommand(cmd);
                return res;
            }
            AccessAllowed = true;
            return new List<Dictionary<string, string>>();
        }

        public static DataTable GetTableSchema(SQLiteCommand cmd)
        {
            DataTable ret = null;
            try
            {
                cmd.Connection = GetInstance().DbConnection;
                cmd.Connection.Open();
                var dr = cmd.ExecuteReader(CommandBehavior.SchemaOnly);
                ret = dr.GetSchemaTable();
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                FileManager.LogMessage(cmd.CommandText);
                FileManager.LogException(ex);
            }
            return ret;
        }

        public List<Dictionary<string, string>> FeedUploadTransaction(SQLiteCommand cmd)
        {
            if (_transConnection.State != ConnectionState.Open)
            {
                _transConnection.Open();
                _transaction = _transConnection.BeginTransaction();
            }
            cmd.Connection = _transConnection;
            cmd.Transaction = _transaction;
            return DbExecute(cmd, AccessType.CReader);
        }

        public int FeedSyncTransaction(SQLiteCommand cmd)
        {
            if (_transConnection.State != ConnectionState.Open)
            {
                _transConnection.Open();
                _transaction = _transConnection.BeginTransaction();
            }
            cmd.Connection = _transConnection;
            cmd.Transaction = _transaction;
            return ExecuteDbSyncCommand(cmd);
        }

        public void Rollback()
        {
            if (_transConnection.State == ConnectionState.Open)
            {
                //_transaction.Rollback();
                //_rolledBack = true;
            }
        }

        /// <summary>
        /// Executes a single command on the database rather than feeding a transaction. 
        /// As such a command can involve the execution of other commands called by the OxDbManager, this method opens a transaction of it's own.
        /// Each call to the method adds an element to _scStack. If the db-connection is closed a new connection will be opened, 
        /// otherwise, if there is no element on the stack (the connection wasn't opened by ExecuteSingleCommand), the _connOpen Flag will be set.
        /// If the stack contains more than one element, or the _connOpen-Flag is set, (for example because the method was called with a connection already 
        /// opened by a FeedTransaction-call) a pop is executed on the stack. Otherwise the transaction will be commited and the connection closed.
        /// </summary>
        /// <param name="cmd">The SQLiteCommand to be executed</param>
        /// <returns>The result return by the execution of the command</returns>
        public List<Dictionary<string, string>> ExecuteSingleCommand(SQLiteCommand cmd)
        {
            List<Dictionary<string, string>> retVal = null;
            try
            {
                _connOpen = false;
                //if (AppConfig.DebugMode)
                //    LogDebugInfo(cmd, null);

                if (_transConnection.State != ConnectionState.Open)
                {
                    _transConnection.Open();
                    _transaction = _transConnection.BeginTransaction();
                }
                else if (_scStack.Count == 0)
                {
                    _connOpen = true;
                }
                _scStack.Push(1);

                cmd.Connection = _transConnection;
                cmd.Transaction = _transaction;
                retVal = PrepareDbCommand(cmd);
                if (!_connOpen && _scStack.Count == 1)
                {
                    _transaction.Commit();
                    _transConnection.Close();
                    _scStack.Clear();
                }
                else
                {
                    _scStack.Pop();
                }
            }
            catch(SQLiteException sqlEx)
            {
                if(sqlEx.ErrorCode.Equals(SQLiteErrorCode.ReadOnly))
                {
                    MessageBox.Show("Fehler beim Zugriff auf die Datenbank. Die Datenbank ist schreibgeschützt\n\r " +
                                    "Bitte wenden Sie sich an die Systemadministration.", "Zugriffsfehler",
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    Application.Exit();
                    //Environment.FailFast("Fehler beim Zugriff auf die Datenbank. Die Datenbank ist schreibgeschützt");
                }
                else
                {
                    FileManager.LogException(sqlEx);
                }
            }
            catch(Exception ex)
            {
				_scStack.Pop();
                FileManager.LogException(ex);
            }
            return retVal;
        }

        //public void CommitTransaction()
        //{
        //    if (_transaction != null)
        //    {
        //        if (!_rolledBack)
        //        {
        //            if (_transConnection.State == ConnectionState.Open)
        //            {
        //                _transaction.Commit();
        //                _transConnection.Close();
        //            }
        //        }
        //        else
        //        {
        //            _rolledBack = false;
        //        }
        //    }
        //}
        public void CommitTransaction()
        {
            CommitTransaction(false);
        }


        public void CommitTransaction(bool specialCommand)
        {
            try
            {
                lock (_locker2)
                {
                    if (_transaction != null)
                    {
                        if (!_rolledBack)
                        {
                            foreach (var item in _cmdList)
                                item.Dispose();
                            _cmdList.Clear();
                            //BEGIN ENHANCEMENT SPOT
                            //END ENHANCEMENT SPOT
                            if (_transConnection.State == ConnectionState.Open)
                            {
                                if (!specialCommand)
                                    _transaction.Commit();
                                _transConnection.Close();
                            }
                        }
                        else
                        {
                            _rolledBack = false;
                        }
                    }
                    if (_transConnection.State == ConnectionState.Open)
                    {

                        foreach (var item in _cmdList)
                            item.Dispose();
                        _cmdList.Clear();
                        _transConnection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                while (_transConnection.State == ConnectionState.Open)
                {
                    _transConnection.Close();
                }
                Monitor.Enter(Locker);
                _monEnter++;
                for (var i = 0; i < _monEnter; i++)
                {
                    try
                    {
                        Monitor.Exit(Locker);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                _monEnter = 0;

                TransactionActive = true;

                foreach (var item in _cmdList)
                    item.Dispose();
                _cmdList.Clear();
            }
        }

        /// <summary>
        /// returns the names of all tables to be accessed in the provided SQL statement.
        /// </summary>
        /// <param name="SQLStmt">SQL Statmentto be checked for its tablenames</param>
        /// <returns></returns>
        private List<String> GetTableNames(String SQLStmt)
        {
            List<String> tablenames = new List<string>();
            String tablename = "";
            string[] words = SQLStmt.Split(' ');
            if (SQLStmt.IndexOf("INSERT INTO ") != -1)
            {
                tablename = words[2];
            }
            else if (SQLStmt.IndexOf("INSERT OR REPLACE INTO ") != -1)
            {
                tablename = words[4];
            }
            else if (SQLStmt.IndexOf("UPDATE") != -1)
            {
                tablename = words[1];
            }
            else if (SQLStmt.IndexOf("DELETE FROM ") != -1)
            {
                tablename = words[2];
            }
            else if (SQLStmt.IndexOf("SELECT ") != -1)
            {
                if (SQLStmt.IndexOf(" FROM ") != -1)
                    tablenames.Add(Regex.Split(SQLStmt, " FROM ")[1].Split(' ')[0].ToUpper());
                if (SQLStmt.IndexOf(" JOIN ") != -1)
                {
                    String[] sqlJoinParts = Regex.Split(SQLStmt, " JOIN ");
                    for (int i = 1; i < sqlJoinParts.Length; i++)
                        tablenames.Add(sqlJoinParts[i].Split(' ')[0].ToUpper());
                }
            }
            tablename = tablename.TrimStart('[');
            tablename = tablename.TrimEnd(']');
            if (!String.IsNullOrEmpty(tablename))
                tablenames.Add(tablename.ToUpper());
            return tablenames;
        }

        public static ThDataType GetThDataType(String tablename)
        {
            if (TableStructure == null)
            {
                TableStructure = new List<ThDataType>();
            }
            ThDataType th = TableStructure.Where(x => x.TableName == tablename).FirstOrDefault();
            if (th == null)
            {
                //only create the db structure once
                CreateTableStructure(tablename);
                th = TableStructure.Where(x => x.TableName == tablename).FirstOrDefault();
                int index = th.ColumnNames.ToList().FindIndex(w => w.Equals("UPDFLAG"));
                if (th == null) //if still null --> break
                    return null;
            }
            return th;
        }

        /// <summary>
        /// Adds a new tablestructureobject to Tablestructure based on the data in S_HLP_COLUMNORDER
        /// </summary>
        /// <param name="table"></param>
        public static void CreateTableStructure(string table)
        {
            //var conn = GetInstance()._dbConnection;
            try
            {
                if (String.IsNullOrEmpty(table))
                    return;

                //For each field in the table...
                var dataType = new ThDataType(table);
                DataTable schemaTable = GetInstance().GetSchema(table);

                DataColumn[] dt = schemaTable.PrimaryKey; //empty!

                //For each field in the table...
                var columns = new Dictionary<String, ColumnDescriptor>();
                // http://support.microsoft.com/kb/310107/en-us
                foreach (DataRow myField in schemaTable.Rows)
                {
                    string columnname = myField["ColumnName"].ToString();
                    bool flag = bool.Parse(myField[6].ToString());

                    var cd = new ColumnDescriptor
                    {
                        Column = columnname,
                        IsKey = flag
                    };
                    columns.Add(columnname, cd);
                }


                String sqlStmt = @"SELECT column_x FROM S_HLP_COLUMNORDER WHERE table_x = '" + table
                      + "' ORDER BY order_c ASC";
                var records = GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    var col = rdr["column_x"];

                    foreach (var cd in columns.Values)
                    {
                        if (cd.Column == col)
                        {
                            if (!dataType.Columns.ContainsKey(cd.Column))
                                dataType.Columns.Add(cd.Column, cd);
                        }
                    }
                }

                //if not everything is included
                if (dataType.Columns.Count() == 0)
                {
                    //read again..
                    foreach (var cd in columns.Values)
                    {
                        dataType.Columns.Add(cd.Column, cd);
                    }
                }
                //update overall structures
                TableStructure.Add(dataType);
            }
            catch (Exception ex)
            {
                SyncManager.GetInstance()._syncError = true;
                FileManager.LogException(ex);
            }
        }

        /// <summary>
        /// Executes the transaction command for the current sync.
        /// Keep in mind that the sync is to be always executed through a transaction to keep it performant and consistent, 
        /// so the actual database operation won't be finished until the tarnsaction has been commited.
        /// </summary>
        /// <param name="intDbResult"></param>
        private int ExecuteDbSyncCommand(SQLiteCommand cmd)
        {
            var intDbResult = 0;
            try
            {
                intDbResult = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                FileManager.LogMessage(cmd.CommandText);
                FileManager.LogException(ex);
            }
            return intDbResult;
        }

        public List<Dictionary<string, string>> ExecuteDbSpecialCommand(SQLiteCommand cmd)
        {
            var res = new List<Dictionary<string, string>>();
            try
            {
                //if (AppConfig.DebugMode)
                //    LogDebugInfo(cmd, cmd.CommandText);

                if (_transConnection.State != ConnectionState.Open)
                    _transConnection.Open();

                cmd.Connection = _transConnection;
                res = DbExecute(cmd, AccessType.CReader);
                _transConnection.Close();
                return res;
            }
            catch (Exception ex)
            {
                FileManager.LogMessage(SqlTransCommand.CommandText);
                FileManager.LogException(ex);
            }
            return res;
        }

        public static string GetCoolSqlSelect(string aTable, ThDataType thDataType)
        {
            if (_coolSqlSelectList == null)
                _coolSqlSelectList = new Dictionary<string, string>();
            if (_coolSqlSelectList.ContainsKey(aTable) && _lastInitialisedTable != null)
                if (aTable.Equals(_lastInitialisedTable))
                {
                    return _coolSqlSelectList[aTable];
                }
            var sb = new StringBuilder();
            try
            {
                sb.Append("SELECT * FROM [" + aTable + "]");

                sb.Append(" WHERE ");
                //get keys
                //get non key values
                foreach (string column in thDataType.KeyColumns)
                {
                    sb.Append(string.Format("[{0}]=@{0} AND ", column));
                }
                sb.Remove(sb.Length - 4, 4);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            if (!_coolSqlSelectList.ContainsKey(aTable))
                _coolSqlSelectList.Add(aTable, sb.ToString());
            _lastInitialisedTable = aTable;
            return sb.ToString();
        }

        public static string GetCoolSqlUpdate(string aTable, ThDataType thDataType)
        {
            if (_coolSqlUpdateList == null)
                _coolSqlUpdateList = new Dictionary<string, string>();
            if (_coolSqlUpdateList.ContainsKey(aTable) && _lastInitialisedTable != null)
                if (aTable.Equals(_lastInitialisedTable))
                {
                    return _coolSqlUpdateList[aTable];
                }
            var sb = new StringBuilder();
            try
            {
                sb.Append("UPDATE [" + aTable + "] SET ");

                //get non key values
                foreach (string column in thDataType.NonKeyColumns)
                {
                    sb.Append(string.Format("[{0}]=@{0} ,", column));
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append(" WHERE ");
                //get keys
                //get non key values
                foreach (string column in thDataType.KeyColumns)
                {
                    sb.Append(string.Format("[{0}]=@{0} AND ", column));
                }
                sb.Remove(sb.Length - 4, 4);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            if (!_coolSqlUpdateList.ContainsKey(aTable))
                _coolSqlUpdateList.Add(aTable, sb.ToString());
            _lastInitialisedTable = aTable;
            return sb.ToString();
        }

        public static string GetCoolSqlInsert(string table, ThDataType thDataType)
        {
            var sqlInsert = new StringBuilder();
            try
            {
                if (_coolSqlInsertList == null)
                    _coolSqlInsertList = new Dictionary<string, string>();
                if (_coolSqlInsertList.ContainsKey(table) && _lastInitialisedTable != null)
                    if (table.Equals(_lastInitialisedTable))
                        return _coolSqlInsertList[table];
                sqlInsert.Append("INSERT OR REPLACE INTO [" + table + "] (");
                // get all columns
                foreach (string s in thDataType.ColumnNames)
                {
                    sqlInsert.Append(string.Format("{0} ,", s));
                }
                //remove last unnecessary ,
                sqlInsert.Remove(sqlInsert.Length - 1, 1);
                sqlInsert.Append(") VALUES (");


                foreach (string s in thDataType.ColumnNames)
                {
                    sqlInsert.Append(string.Format("@{0} ,", s));
                }
                //remove last unnecessary ,
                sqlInsert.Remove(sqlInsert.Length - 1, 1);
                sqlInsert.Append(")");
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            if (!_coolSqlInsertList.ContainsKey(table))
                _coolSqlInsertList.Add(table, sqlInsert.ToString());
            _lastInitialisedTable = table;
            return sqlInsert.ToString();
        }

        public static string GetCoolSqlDelete(string aTable, ThDataType thDataType)
        {
            if (_coolSqlDeleteList == null)
                _coolSqlDeleteList = new Dictionary<string, string>();
            if (_coolSqlDeleteList.ContainsKey(aTable) && _lastInitialisedTable != null)
                if (aTable.Equals(_lastInitialisedTable))
                {
                    return _coolSqlDeleteList[aTable];
                }
            var sb = new StringBuilder();
            try
            {
                sb.Append("DELETE FROM [" + aTable + "]");

                sb.Append(" WHERE ");
                //get keys
                //get non key values
                foreach (string column in thDataType.KeyColumns)
                {
                    sb.Append(string.Format("[{0}]=@{0} AND ", column));
                }
                sb.Remove(sb.Length - 4, 4);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            if (!_coolSqlDeleteList.ContainsKey(aTable))
                _coolSqlDeleteList.Add(aTable, sb.ToString());
            _lastInitialisedTable = aTable;
            return sb.ToString();
        }

        public static IList<string> GetDeltaUsers(Boolean upload)
        {
            IList<string> userIds = new List<string>();
            try
            {
                String sqlStmt = "";
                if (upload)
                    //sqlStmt = @"SELECT tablename FROM S_SYNCDELTA WHERE upload = 'X' AND mandt = '" + AppConfig.Mandt + "' and userid = '" + AppConfig.UserId + "'";
                    sqlStmt = @"SELECT distinct userid as userid  FROM S_SYNCDELTA WHERE upload = 'X' AND mandt = '" + AppConfig.Mandt + "'";
                else
                    //sqlStmt = @"SELECT tablename FROM S_SYNCDELTA WHERE download = 'X' AND mandt = '" + AppConfig.Mandt + "' and userid = '" + AppConfig.UserId + "'";
                    sqlStmt = @"SELECT distinct userid as userid FROM S_SYNCDELTA WHERE download = 'X' AND mandt = '" + AppConfig.Mandt + "'";

                List<Dictionary<string, string>> records = GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (Dictionary<string, string> rdr in records)
                {
                    userIds.Add((String)rdr["USERID"]);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return userIds;
        }

        public static IList<string> GetDeltaTables(Boolean upload, String userId)
        {
            IList<string> tableNames = new List<string>();
            try
            {
                String sqlStmt = "";
                if (upload)
                    //sqlStmt = @"SELECT tablename FROM S_SYNCDELTA WHERE upload = 'X' AND mandt = '" + AppConfig.Mandt + "' and userid = '" + AppConfig.UserId + "'";
                    sqlStmt = @"SELECT tablename FROM S_SYNCDELTA WHERE upload = 'X' AND mandt = '" + AppConfig.Mandt + "' and userid = '" + userId + "'";
                else
                    //sqlStmt = @"SELECT tablename FROM S_SYNCDELTA WHERE download = 'X' AND mandt = '" + AppConfig.Mandt + "' and userid = '" + AppConfig.UserId + "'";
                    sqlStmt = @"SELECT tablename FROM S_SYNCDELTA WHERE download = 'X' AND mandt = '" + AppConfig.Mandt + "' and userid = '" + userId + "'";

                List<Dictionary<string, string>> records = GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (Dictionary<string, string> rdr in records)
                {
                    tableNames.Add((String)rdr["TABLENAME"]);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return tableNames;
        }

        public static IList<string> GetDatabaseTables(String tablePrefix)
        {
            IList<string> tableNames = new List<string>();
            try
            {
                String sqlStmt = @"SELECT * FROM sqlite_master WHERE type = 'table' AND (name LIKE '" + tablePrefix + "%')";
                List<Dictionary<string, string>> records = GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (Dictionary<string, string> rdr in records)
                {
                    tableNames.Add((String)rdr["tbl_name"]);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return tableNames;
        }

        // Fill table S_SYNCDELTA initially with all d/c table names after user has been logged on
        public static void LoadTableList()
        {
            try
            {
                _tableList = new Hashtable();
                String table = "";
                String sqlStmt = @"SELECT * FROM sqlite_master WHERE type='table'";
                List<Dictionary<string, string>> records = GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (Dictionary<string, string> rdr in records)
                {
                    table = (String) rdr["tbl_name"];
                    if (!table.Equals("C_ZFRAPORT_ENH_CUST001"))
                        _tableList[((String)rdr["tbl_name"]).Substring(2)] = table;
                }
                _tableList["/FRAPORT/ENH_CUST001"] = "C_ZFRAPORT_ENH_CUST001";
            }
            catch (Exception ex)
            {
                SyncManager.GetInstance()._syncError = true;
                FileManager.LogException(ex);
            }
        }

        // Fill table S_SYNCDELTA initially with all d/c table names after user has been logged on
        public void CreateDeltaTableContent()
        {
            var dTables = GetDatabaseTables("D_");
            var allTables = GetDatabaseTables("C_");

            var sqlStmt = "SELECT COUNT(*) FROM [S_SYNCDELTA] WHERE MANDT=@MANDT AND USERID=@USERID";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
            cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
            var list = FeedTransaction(cmd);
            if (list.Count > 0 && list[0]["RetVal"].Equals("0"))
            {
                foreach (var tablename in dTables)
                {
                    allTables.Add(tablename);
                }
                foreach (var tablename in allTables)
                {
                    sqlStmt = "INSERT OR REPLACE INTO [S_SYNCDELTA] " +
                              "(MANDT, USERID, TABLENAME, UPLOAD, DOWNLOAD) " +
                              "Values (@MANDT, @USERID, @TABLENAME, @UPLOAD, @DOWNLOAD)";
                    cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@TABLENAME", tablename);
                    cmd.Parameters.AddWithValue("@UPLOAD", "");
                    cmd.Parameters.AddWithValue("@DOWNLOAD", "");
                    FeedTransaction(cmd);
                }
            }
            CommitTransaction();
        }

        public void UpdateSyncDelta(String table)
        {
            var sqlStmt = "UPDATE [S_SYNCDELTA] SET UPLOAD = 'X' WHERE tablename = '" + table +
                 "' AND mandt = '" + AppConfig.Mandt + "' AND userid = '" + AppConfig.UserId + "'";
            GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
        }

        public static void FindAndReplace(string tablename, string newValue, string valueField, string keyValue, string keyField)
        {
            var sqlStmt = "UPDATE " + tablename + " SET " + valueField + " = @NEWVALUE WHERE " + keyField + " = @KEYVALUE";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@NEWVALUE", newValue);
            cmd.Parameters.AddWithValue("@KEYVALUE", keyValue);
            GetInstance().FeedSyncTransaction(cmd);
        }

        public void CreateDeviceGuid(String mandt, String userid)
        {
            String sqlStmt = "SELECT DISTINCT DEVICE FROM S_DEVICE";
            List<Dictionary<string, string>> result_device = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
            if (result_device.Count == 0)
            {
                // New device -> Generate new GUID
                AppConfig.DeviceId = Guid.NewGuid().ToString();
                sqlStmt = "INSERT INTO [S_DEVICE] (DEVICE, MANDT, USERID, LASTLOGON) Values (@DEVICE, @MANDT, @USERID, @LASTLOGON)";
                SQLiteCommand cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@DEVICE", AppConfig.DeviceId);
                cmd.Parameters.AddWithValue("@MANDT", mandt);
                cmd.Parameters.AddWithValue("@USERID", userid);
                cmd.Parameters.AddWithValue("@LASTLOGON", DateTime.Now.ToString("yyyyMMddhhmmss"));
                List<Dictionary<string, string>> ret = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                return;
            }
            foreach (var rdr in result_device)
            {
                AppConfig.DeviceId = rdr["DEVICE"];
            }

            sqlStmt = "SELECT * FROM S_DEVICE WHERE MANDT = '" + mandt + "' AND USERID = '" + userid + "'";
            List<Dictionary<string, string>> result_user = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
            if (result_user.Count == 0)
            {
                // New user on this device
                sqlStmt = "INSERT OR REPLACE INTO [S_DEVICE] (DEVICE, MANDT, USERID, LASTLOGON) Values (@DEVICE, @MANDT, @USERID, @LASTLOGON)";
            }
            else
            {
                sqlStmt = "INSERT OR REPLACE INTO [S_DEVICE] (DEVICE, MANDT, USERID, LASTLOGON) Values (@DEVICE, @MANDT, @USERID, @LASTLOGON)";
            }

            var cmd2 = new SQLiteCommand(sqlStmt);
            cmd2.Parameters.AddWithValue("@DEVICE", AppConfig.DeviceId);
            cmd2.Parameters.AddWithValue("@MANDT", mandt);
            cmd2.Parameters.AddWithValue("@USERID", userid);
            cmd2.Parameters.AddWithValue("@LASTLOGON", DateTime.Now.ToString("yyyyMMddhhmmss"));
            OxDbManager.GetInstance().ExecuteSingleCommand(cmd2);
            return;
        }
        /* private void LogDebugInfo(SQLiteCommand cmd, String sql)
        {
            if (cmd != null)
            {
                StackFrame[] stackFrames = (new StackTrace(true)).GetFrames();
                String stackTrace = "";
                foreach (StackFrame stackFrame in stackFrames)
                    if (stackFrame.GetFileName() != null)
                        stackTrace += "\r\n" + stackFrame.GetFileName() + "." + stackFrame.GetMethod().Name + ":" +
                                      stackFrame.GetFileLineNumber();
                String logMessage = cmd.CommandText;
                foreach (SQLiteParameter param in cmd.Parameters)
                {
                    logMessage = logMessage.Replace(param.ParameterName, "'" + param.Value.ToString() + "'");
                }
                FileManager.LogMessage(stackTrace + "\r\n" + logMessage);
            }
            if (sql != null)
            {
                StackFrame[] stackFrames = (new StackTrace()).GetFrames();
                String stackTrace = "";
                foreach (StackFrame stackFrame in stackFrames)
                    if (stackFrame.GetFileName() != null)
                        stackTrace += "\r\n" + stackFrame.GetMethod().Name + ":" + stackFrame.GetFileLineNumber();
                FileManager.LogMessage(stackTrace + "\r\n" + sql);
            }
        } */

        public void SweepDepricatedUserData()
        {
            if (!string.IsNullOrEmpty(AppConfig.ResidenceTime))
            {
                try
                {
                    var restime = int.Parse(AppConfig.ResidenceTime);
                    if (restime > 0)
                    {
                        var days = int.Parse(AppConfig.ResidenceTime) * -1;
                        var date = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
                        var sqlStmt = "SELECT MANDT, USERID FROM S_USER WHERE LAST_LOGIN_DATE < @SWEEPDATE";
                        var cmd = new SQLiteCommand(sqlStmt);
                        cmd.Parameters.AddWithValue("@SWEEPDATE", date);
                        var records = ExecuteDbSpecialCommand(cmd);
                        var tables = GetDatabaseTables("D_");
                        tables = tables.Concat(GetDatabaseTables("C_")).ToList();
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@MANDT", DbType.String);
                        cmd.Parameters.Add("@USERID", DbType.String);
                        foreach (var rdr in records)
                        {
                            cmd.Parameters["@MANDT"].Value = rdr["MANDT"];
                            cmd.Parameters["@USERID"].Value = rdr["USERID"];
                            OxStatus.GetInstance().TriggerEventMessage(1, 'W', DateTime.Now, "Sweeping data for user " + rdr["USERID"], null, false);
                            foreach (var table in tables)
                            {
                                sqlStmt = string.Format("DELETE FROM {0} WHERE MANDT = @MANDT AND USERID = @USERID",
                                                        table);
                                cmd.CommandText = sqlStmt;
                                FeedSyncTransaction(cmd);
                            }

                            sqlStmt = "DELETE FROM S_SYNCDELTA WHERE MANDT = @MANDT AND USERID = @USERID";
                            cmd.CommandText = sqlStmt;
                            FeedSyncTransaction(cmd);

                            sqlStmt = "DELETE FROM S_USER WHERE MANDT = @MANDT AND USERID = @USERID";
                            cmd.CommandText = sqlStmt;
                            FeedSyncTransaction(cmd);

                            sqlStmt = "DELETE FROM S_DEVICE WHERE MANDT = @MANDT AND USERID = @USERID";
                            cmd.CommandText = sqlStmt;
                            FeedSyncTransaction(cmd);
                        }
                        CommitTransaction();
                        if (records.Count > 0)
                        {
                            ExecuteDbSpecialCommand(new SQLiteCommand("VACUUM;"));
                            ExecuteDbSpecialCommand(new SQLiteCommand("ANALYZE;"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    FileManager.LogException(ex);
                }
            }
        }

        public List<Dictionary<string, string>> ExecuteDbSpecialUpdateCommand(SQLiteCommand cmd)
        {
            var res = new List<Dictionary<string, string>>();
            try
            {
                //if (AppConfig.DebugMode)
                //    LogDebugInfo(cmd, cmd.CommandText);

                if (_transConnection.State != ConnectionState.Open)
                {
                    _transConnection.Open();
                    _transaction = _transConnection.BeginTransaction();
                }
                cmd.Connection = _transConnection;
                cmd.Transaction = _transaction;

                res = DbExecute(cmd, AccessType.CReader);

                if (_transaction != null)
                {
                    if (!_rolledBack)
                    {
                        if (_transConnection.State == ConnectionState.Open)
                        {
                            _transaction.Commit();
                            _transConnection.Close();
                        }
                    }
                    else
                    {
                        _rolledBack = false;
                    }
                }

                //_transConnection.Close();
                return res;
            }
            catch (Exception ex)
            {
                FileManager.LogMessage(SqlTransCommand.CommandText);
                FileManager.LogException(ex);
            }
            return res;
        }

        private Queue _nextThreadQueue = new Queue();
        protected SQLiteCommand GetConnection(SQLiteCommand cmd, bool specialCommand)
        {
            try
            {
                //BEGIN ENHANCEMENT SPOT
                //Perform only if Connection was not previously open
                //if (_transConnection.State != ConnectionState.Open)
                //{
                //    EnhancementSpots.OxDBManager_BeforeFeedTransaction();
                //}
                //END ENHANCEMENT SPOT
                if (_transConnection.State != ConnectionState.Open)
                {
                    _transConnection.Open();
                    if (!specialCommand)
                        _transaction = _transConnection.BeginTransaction();
                }
                cmd.Connection = _transConnection;
                if (!specialCommand)
                    cmd.Transaction = _transaction;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                if (_nextThreadQueue.Count > 0)
                    _nextThreadQueue.Dequeue();
            }
            return cmd;
        }
    }
}
