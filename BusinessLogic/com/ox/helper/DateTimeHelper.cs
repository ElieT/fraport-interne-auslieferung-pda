﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class DateTimeHelper
    {
        public static readonly DateTime MinDateTimeValue = new DateTime(1753, 1, 1, 00, 00, 00);

        public static string MakeTimeString(double num, string unit, out int aHours, out int aMinutes)
        {
            double hours = 0;
            double minutes = 0;
            if (unit.ToUpper().Equals("H"))
            {
                hours = Math.Floor(num); //take integral part

                double fraction = Math.Round(num - Math.Floor(num), 2);
                minutes = fraction * 60.0; //multiply fractional part with 60

            }
            else
            {
                hours = Math.Floor(num / 60);
                double fraction = Math.Round(num - Math.Floor(num), 2);
                minutes = fraction * 60;
            }

            aHours = (int)Math.Floor(hours);

            aMinutes = (int)Math.Floor(minutes);
            return aHours.ToString() + ":" + aMinutes.ToString(); //+":" + S.ToString(); //add if you want seconds
        }

        public static String GetDateString(DateTime input)
        {
            var minDateTimeValue = DateTimeHelper.MinDateTimeValue;
            String date = "00000000";
            if (input == DateTimeHelper.MinDateTimeValue)
                return date;
            if (input == DateTime.MinValue)
                return date;
            date = input.ToString("yyyyMMdd");
            return date;
        }

        public static String GetTimeString(DateTime input)
        {
            DateTime minDateTimeValue = DateTimeHelper.MinDateTimeValue; 
            String time = "000000";
            if (input == DateTimeHelper.MinDateTimeValue)
                return time;
            if (input == DateTime.MinValue)
                return time;
            time = input.ToString("HHmmss");
            if (time.Equals("235959"))
                time = "240000";
            return time;
        }

		public static String GetTimeString(String input)
        {
            try
            {
                var time = "000000";
                if (input.Length > 8)
                    throw new Exception("Unerlaubtes Zeitformat");
                input = input.Replace(":", "");
                try
                {
                    var outValue = 0;
                    outValue = int.Parse(input);
                    if (outValue == 0)
                        throw new Exception("Unerlaubtes Zeitformat");
                    if (input.Length < 6)
                        input.PadLeft(6, '0'); 
                    outValue = int.Parse(input.Substring(0, 2));
                    if (outValue > 23)
                        throw new Exception("Unerlaubtes Zeitformat");

                    outValue = int.Parse(input.Substring(2, 2));
                    if (outValue > 59)
                        throw new Exception("Unerlaubtes Zeitformat");

                    outValue = int.Parse(input.Substring(4, 2));
                    if (outValue > 59)
                        throw new Exception("Unerlaubtes Zeitformat");

                    time = input;
                    return time;
                }
                catch(FormatException ex)
                {
                    throw new Exception("Unerlaubtes Zeitformat", ex);
                }
                
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
		
         public static DateTime getDate(String input)
         {
             var minDateTimeValue = DateTimeHelper.MinDateTimeValue;
             // gets a formated date string 'dd.mm.yyyy' and returns the datetime object
             try
             {
				 if (String.IsNullOrEmpty(input))
					return minDateTimeValue;
                 int i1 = int.Parse(input.Split('.')[2]);
                 int i2 = int.Parse(input.Split('.')[1]);
                 int i3 = int.Parse(input.Split('.')[0]);
                 return new DateTime(i1,i2,i3);
             } 
             catch (Exception ex)
             {
                 FileManager.LogException(ex);
             }
             return DateTimeHelper.MinDateTimeValue;
         }

        public static Boolean getDate(Object input, out DateTime output)
        {
            DateTime minDateTimeValue = DateTimeHelper.MinDateTimeValue;
            long i;
            String strValue = "";
            if (!(input is DBNull))
            {
                strValue = (String)input;
            }
            if (strValue.Length.Equals(8) && CustomLong.TryParse(strValue, out i) && !strValue.Equals("00000000"))
            {
                output = new DateTime(int.Parse(strValue.Substring(0, 4)), int.Parse(strValue.Substring(4, 2)),
                                      int.Parse(strValue.Substring(6, 2)));
                return true;
            }
            output = DateTimeHelper.MinDateTimeValue;
            return false;
        }

        public static Boolean getTime(Object date, Object time, out DateTime output)
        {
            DateTime minDateTimeValue = DateTimeHelper.MinDateTimeValue;
            long i;
            if (date == null || date.Equals("00000000"))
            {
                output = DateTimeHelper.MinDateTimeValue;
                return false;
            }
            if (time.Equals("240000"))
            {
                time = "235959";
            }
            String strValue = "";
            if (!(date is DBNull))
            {
                strValue = (String)date;
                if (string.IsNullOrEmpty(strValue))
                    strValue = DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
            }
            if (!(time is DBNull))
            {
                strValue += (String)time;
            }
            if (strValue.Length.Equals(14) && CustomLong.TryParse(strValue, out i) && !strValue.Equals("00000000000000"))
            {
                output = new DateTime(int.Parse(strValue.Substring(0, 4)), int.Parse(strValue.Substring(4, 2)),
                                      int.Parse(strValue.Substring(6, 2)),
                                      int.Parse(strValue.Substring(8, 2)), int.Parse(strValue.Substring(10, 2)),
                                      int.Parse(strValue.Substring(12, 2)));
                return true;
            }
            if (strValue.Length.Equals(12) && CustomLong.TryParse(strValue, out i) && !strValue.Equals("000000000000"))
            {
                output = new DateTime(int.Parse(strValue.Substring(0, 4)), int.Parse(strValue.Substring(4, 2)),
                                      int.Parse(strValue.Substring(6, 2)),
                                      int.Parse(strValue.Substring(8, 2)), int.Parse(strValue.Substring(10, 2)), 0);
                return true;
            }
            output = DateTimeHelper.MinDateTimeValue;
            return false;
        }

        public static Boolean getTime(Object timestamp, out DateTime output)
        {
            long i;
            if (timestamp.ToString().Length != 14)
            {
                output = DateTimeHelper.MinDateTimeValue;
                return false;
            }
            String _date = timestamp.ToString().Substring(0, 8);
            String _time = timestamp.ToString().Substring(8, 6);
            if (_time.Equals("240000"))
            {
                _time = "235959";
            }
            String strValue = "";
            if (!(_date is DBNull))
            {
                strValue = (String)_date;
                if (string.IsNullOrEmpty(strValue))
                    strValue = DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
            }
            if (!(_time is DBNull))
            {
                strValue += (String)_time;
            }
            if (strValue.Length.Equals(14) && CustomLong.TryParse(strValue, out i) && !strValue.Equals("00000000000000"))
            {
                output = new DateTime(int.Parse(strValue.Substring(0, 4)), int.Parse(strValue.Substring(4, 2)),
                                      int.Parse(strValue.Substring(6, 2)),
                                      int.Parse(strValue.Substring(8, 2)), int.Parse(strValue.Substring(10, 2)),
                                      int.Parse(strValue.Substring(12, 2)));
                return true;
            }
            if (strValue.Length.Equals(12) && CustomLong.TryParse(strValue, out i) && !strValue.Equals("000000000000"))
            {
                output = new DateTime(int.Parse(strValue.Substring(0, 4)), int.Parse(strValue.Substring(4, 2)),
                                      int.Parse(strValue.Substring(6, 2)),
                                      int.Parse(strValue.Substring(8, 2)), int.Parse(strValue.Substring(10, 2)), 0);
                return true;
            }
            output = DateTimeHelper.MinDateTimeValue;
            return false;
        }

        public static String TimeValue(float input)
        {
            //double real = 3.87;
            double fraction = Math.Round(input - Math.Floor(input), 2);
            double minutes = fraction * 60;
            return Math.Floor(input) + ":" + Math.Floor(minutes).ToString().PadLeft(2, '0');
        }

        public static double TimeValueHours(float input)
        {
            //double real = 3.87;
            return Math.Floor(input);
        }

        public static double TimeValueMinutes(float input)
        {
            //double real = 3.87;
            double fraction = Math.Round(input - Math.Floor(input), 2);
            double minutes = fraction * 60;
            return Math.Floor(minutes);
        }
    }
}