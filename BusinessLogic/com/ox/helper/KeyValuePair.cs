﻿namespace oxmc.UI.uiHelper
{
    public class KeyValuePair
    {
        public object Key;
        public string Value;

        public KeyValuePair(object NewValue, string NewDescription)
        {
            Key = NewValue;
            Value = NewDescription;
        }

        public override string ToString()
        {
            return Value;
        }
    }
}