﻿using System;
using System.Collections.Generic;

using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class EMSHelper
    {
        public static void fillEMSData()
        {
            // Read data from file
            IList<String> codeLines = FileManager.ReadFileLines("EMS\\codes.txt");
            IList<String> equiLines = FileManager.ReadFileLines("EMS\\equi.txt");
            IList<String> beifahrer = FileManager.ReadFileLines("EMS\\beifahrer.txt");
            var connection = new SqlCeConnection(AppConfig.DatabasePath);

            int dbResult = 0;

            try
            {
                connection.Open();
                foreach (String equiLine in equiLines)
                {
                    //System.Console.WriteLine(equiLine);
                    if (equiLine.Split(';').Length > 6)
                    {
                        string sql =
                            @"INSERT INTO EQUI (MANDT, USERID, EQUI, ATWRT_SORT, ATWRT_RICH, ATWRT_EORT, ATWRT_BNVT, ATWRT_LERD) values ('" +
                            AppConfig.Mandt + "', '" +
                            AppConfig.UserId + "', '" +
                            equiLine.Split(';')[0] + "', '" +
                            equiLine.Split(';')[3] + "', '" +
                            equiLine.Split(';')[4] + "', '" +
                            equiLine.Split(';')[5] + "', '" +
                            equiLine.Split(';')[6] + "', '" +
                            equiLine.Split(';')[7] + "'" +
                            ")";
                        var cmd = new SqlCeCommand(sql, connection);
                        dbResult = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogMessage("Error while filling Equipments");
                FileManager.LogException(ex);
            }
            try
            {
                foreach (String codeLine in codeLines)
                {
                    //System.Console.WriteLine(codeLine);
                    if (codeLine.Split(';').Length > 4)
                    {
                        String sql =
                            @"INSERT INTO CODECATALOG (MANDT, USERID, CATALOG, CODEGRP, CODE, CODE_TEXT, CODEGRP_TEXT) values ('" +
                            AppConfig.Mandt + "', '" +
                            AppConfig.UserId + "', '" +
                            codeLine.Split(';')[0] + "', '" +
                            codeLine.Split(';')[1] + "', '" +
                            codeLine.Split(';')[2] + "', '" +
                            codeLine.Split(';')[3] + "', '" +
                            codeLine.Split(';')[4] + "')";
                        var cmd = new SqlCeCommand(sql, connection);
                        dbResult = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogMessage("Error while filling Codecatalogs");
                FileManager.LogException(ex);
            }
            try
            {
                string sql1 = @"INSERT INTO CUST_EMS_ACTYPE (MANDT, USERID, ACTTYP, KTEXT) values ('" +
                              AppConfig.Mandt + "', '" + AppConfig.UserId + "', 'ENT', 'Entstörung')";
                var cmd1 = new SqlCeCommand(sql1, connection);
                dbResult = cmd1.ExecuteNonQuery();
                sql1 = @"INSERT INTO CUST_EMS_ACTYPE (MANDT, USERID, ACTTYP, KTEXT) values ('" +
                       AppConfig.Mandt + "', '" + AppConfig.UserId + "', 'VAN', 'Vandalismus')";
                cmd1 = new SqlCeCommand(sql1, connection);
                dbResult = cmd1.ExecuteNonQuery();
                sql1 = @"INSERT INTO CUST_EMS_ACTYPE (MANDT, USERID, ACTTYP, KTEXT) values ('" +
                       AppConfig.Mandt + "', '" + AppConfig.UserId + "', 'WAR', 'Wartung')";
                cmd1 = new SqlCeCommand(sql1, connection);
                dbResult = cmd1.ExecuteNonQuery();
                sql1 = @"INSERT INTO CUST_EMS_ACTYPE (MANDT, USERID, ACTTYP, KTEXT) values ('" +
                       AppConfig.Mandt + "', '" + AppConfig.UserId + "', 'ENW', 'Entstörung durch Werkstätte')";
                cmd1 = new SqlCeCommand(sql1, connection);
                dbResult = cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                FileManager.LogMessage("Error while filling ActTypes");
                FileManager.LogException(ex);
            }
            try
            {
                string sql1 = @"INSERT INTO CUST_EMS_ZUSTAND (MANDT, USERID, ZUSTAND, KTEXT) values ('" +
                              AppConfig.Mandt + "', '" + AppConfig.UserId +
                              "', 'A', 'Nicht verkaufsbereit, außer Betrieb')";
                var cmd1 = new SqlCeCommand(sql1, connection);
                dbResult = cmd1.ExecuteNonQuery();
                sql1 = @"INSERT INTO CUST_EMS_ZUSTAND (MANDT, USERID, ZUSTAND, KTEXT) values ('" +
                       AppConfig.Mandt + "', '" + AppConfig.UserId + "', 'B', 'Nicht verkaufsbereit, in Betrieb')";
                cmd1 = new SqlCeCommand(sql1, connection);
                dbResult = cmd1.ExecuteNonQuery();
                sql1 = @"INSERT INTO CUST_EMS_ZUSTAND (MANDT, USERID, ZUSTAND, KTEXT) values ('" +
                       AppConfig.Mandt + "', '" + AppConfig.UserId + "', 'C', 'Verkaufsbereit, in Betrieb')";
                cmd1 = new SqlCeCommand(sql1, connection);
                dbResult = cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                FileManager.LogMessage("Error while filling Codecatalogs");
                FileManager.LogException(ex);
            }
            try
            {
                int i = 0;
                foreach (String name in beifahrer)
                {
                    //System.Console.WriteLine(codeLine);
                    if (name.Length > 0)
                    {
                        i++;
                        String sql =
                            @"INSERT INTO CUST_EMS_DRIVER (MANDT, USERID, PERNR, FIRSTNAME, LASTNAME, USER_ID) values ('" +
                            AppConfig.Mandt + "', '" +
                            AppConfig.UserId + "', '" +
                            "i" + i + "', '" +
                            "', '" +
                            name + "', '" +
                            "i" + i + "')";
                        var cmd = new SqlCeCommand(sql, connection);
                        dbResult = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogMessage("Error while filling Fahrer");
                FileManager.LogException(ex);
            }
            finally
            {
                connection.Close();
                connection = null;
            }
        }
    }
}