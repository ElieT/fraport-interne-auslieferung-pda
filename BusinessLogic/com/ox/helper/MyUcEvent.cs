﻿using System;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class MyUcEvent : EventArgs
    {
        public MyUcEvent(Object aObj)
        {
            Obj = aObj;
        }

        public Object Obj { get; set; }
    }
}