﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using iTextSharp.text;
using iTextSharp.text.pdf;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.Common;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class DocumentHelper
    {
        protected static char[] HEXA = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

        //protected static BASE64Encoder encoder = new BASE64Encoder();

        private static Order _order;
        private static FileStream _fs = null;
        private static PdfWriter _writer;



        public static void CreateDeliveryPdf(List<Delivery> items, String persnr, String kostst, String filename, String imageFilename1)
        {
            try
            {
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return;
            }
            var doc = new Document();
            try
            {
                _fs = new FileStream(filename, FileMode.Create, FileAccess.Write,
                                     FileShare.Read);
                _writer = PdfWriter.GetInstance(doc, _fs);
                //var phrase = new Phrase(
                //    DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + " GMT",
                //    new Font(Font.NORMAL, 8)
                //    );
                doc.Open();
                var page = doc.PageSize;
                var head = new PdfPTable(1);
                //var emptyLine = new Phrase("\n");
                var emptyLine = new Phrase(" ");
                var emptyCell = new PdfPCell(new Phrase(" "));
                var cell = new PdfPCell();

                var font_24 = new Font(Font.UNDEFINED, 24);
                var font_10 = new Font(Font.UNDEFINED, 12);

                head.TotalWidth = page.Width;
                var c = new PdfPCell();
                c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                c.VerticalAlignment = Element.ALIGN_TOP;
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                head.AddCell(c);

                head.WriteSelectedRows(
                    // first/last row; -1 writes all rows
                    0, -1,
                    // left offset
                    0,
                    // ** bottom** yPos of the table
                    page.Height - doc.TopMargin + head.TotalHeight,
                    _writer.DirectContent
                    );

                PdfPCell cellHeading = new PdfPCell();
                cellHeading.Border = iTextSharp.text.Rectangle.NO_BORDER;
                var p2 = new Paragraph("Auslieferungsschein / Empfangsbestätigung", font_24);
                p2.Alignment = Element.ALIGN_CENTER;
                doc.Add(p2);

                doc.Add(emptyLine);

                PdfPTable headTable = new PdfPTable(2);
                headTable.WidthPercentage = 50;
                headTable.SpacingBefore = 10;
                headTable.HorizontalAlignment = Element.ALIGN_LEFT;

                var dateStr = "";
                foreach (var deliv in items)
                {
                    //if (deliv.Status.Equals("Z"))
                    if (deliv.Status.Equals("Z") || deliv.Status.Equals("A"))
                    {
                        dateStr = deliv.Lidat.ToString("dd.MM.yyyy");
                        break;
                    }
                    else
                        dateStr = DateTime.Now.ToString("dd.MM.yyyy");
                }

                p2 = new Paragraph("Datum:", font_10);
                p2.Alignment = Element.ALIGN_LEFT;
                cell = new PdfPCell(p2);
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                headTable.AddCell(cell);
                p2 = new Paragraph(dateStr, font_10);
                p2.Alignment = Element.ALIGN_LEFT;
                cell = new PdfPCell(p2);
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                headTable.AddCell(cell);

                p2 = new Paragraph("Gebäude:", font_10);
                p2.Alignment = Element.ALIGN_LEFT;
                cell = new PdfPCell(p2);
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                headTable.AddCell(cell);
                p2 = new Paragraph(items[0].Gebnr, font_10);
                p2.Alignment = Element.ALIGN_LEFT;
                cell = new PdfPCell(p2);
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                headTable.AddCell(cell);

                p2 = new Paragraph("Raum:", font_10);
                p2.Alignment = Element.ALIGN_LEFT;
                cell = new PdfPCell(p2);
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                headTable.AddCell(cell);
                p2 = new Paragraph(items[0].Raum, font_10);
                p2.Alignment = Element.ALIGN_LEFT;
                cell = new PdfPCell(p2);
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                headTable.AddCell(cell);

                doc.Add(headTable);

                doc.Add(emptyLine);

                PdfPTable hTable = new PdfPTable(1);
                hTable.WidthPercentage = 100;

                cellHeading = new PdfPCell();
                cellHeading.Border = iTextSharp.text.Rectangle.NO_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                cellHeading.BorderWidthBottom = 2;
                cellHeading.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellHeading.FixedHeight = 30;
                cellHeading.PaddingTop = -50;
                //cellHeading.PaddingBottom = 4;
                //p2 = new Paragraph("Abholung / Zustellung", font_24);
                var ph2 = new Phrase("Abholung / Zustellung", font_24);
                //p2.Alignment = Element.ALIGN_LEFT | Element.ALIGN_MIDDLE;
                //ph2.Alignment = Element.ALIGN_LEFT | Element.ALIGN_MIDDLE;
                cellHeading.AddElement(ph2);
                hTable.AddCell(cellHeading);
                doc.Add(hTable);

                doc.Add(emptyLine);

                doc.Add(CreateDeliveryDataTable(items));

                doc.Add(emptyLine);

                PdfPTable footTable = GenerateFooterTable(imageFilename1, kostst, persnr);
                doc.Add(footTable);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                if (doc != null)
                {
                    //doc.CloseDocument();
                    CloseDocument(doc);
                    _writer.Close();
                    doc.Close();
                }

                if (_fs != null)
                {
                    _fs.Close();
                }
            }
        }
        public static void CreatePDF(Order order, String filename, String imageFilename1, String imageFilename2)
        {
            try
            {
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return;
            }
            _order = order;
            Document doc = new Document();
            try
            {
               /*
                // DON'T DO THIS
                  using ( MemoryStream ms = new MemoryStream() ) {
                    // the object required to write to a (output) Stream
                    PdfWriter.GetInstance(doc, ms);
                */

                // INSTEAD DO THIS TO SAVE IN-MEMORY COPY
                //PdfWriter _writer = PdfWriter.GetInstance(doc, Response.OutputStream);


                _fs = new FileStream(filename, FileMode.Create, FileAccess.Write,
                           FileShare.Read);

                //_fs.Close();
                //_fs.Flush();
                _writer = PdfWriter.GetInstance(doc, _fs);
                Phrase phrase = new Phrase(
                    DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + " GMT",
                    new Font(Font.NORMAL, 8)
                    );
                doc.Open();
                // create document's header; shows GMT time when PDF created.
                // HeaderFooter class removed in iText 5.0.0;, we must write 
                // content to an **absolute** position on the document
                iTextSharp.text.Rectangle page = doc.PageSize;
                var head = new PdfPTable(1);
                head.TotalWidth = page.Width;
                var c = new PdfPCell(phrase);
                c.Border = iTextSharp.text.Rectangle.NO_BORDER;
                c.VerticalAlignment = Element.ALIGN_TOP;
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                head.AddCell(c);

                head.WriteSelectedRows(
                    // first/last row; -1 writes all rows
                    0, -1,
                    // left offset
                    0,
                    // ** bottom** yPos of the table
                    page.Height - doc.TopMargin + head.TotalHeight,
                    _writer.DirectContent
                    );
                /*
                * 
                * iText versions older than 5.0.0
                * 
                    // create document's header; shows GMT time when PDF created.
                    // set header [1] text [2] font style
                    HeaderFooter  header = new HeaderFooter (phrase, false);
                    // top & bottom borders on by default 
                    header.Border = Rectangle.NO_BORDER;
                    // center header
                    header.Alignment = 1;
                    // add header *before* opening document
                    doc.Header = header;
                    doc.Open();
                */

                    // add image to document
                    /*
                Image gif = Image.GetInstance(Request.MapPath("~/image/kuujinbo2.gif"));
                gif.Alignment = Image.MIDDLE_ALIGN;
                gif.ScalePercent(50f); // change it's size
                doc.Add(gif);
                */
                // report "title"
                Image i = Image.GetInstance(Assembly.GetExecutingAssembly().GetManifestResourceStream("oxmc.BusinessLogic.com.ox.signature.oxando.jpg"));
                i.Alignment = Element.ALIGN_TOP;
                i.Alignment = Element.ALIGN_RIGHT;
                i.ScaleAbsoluteWidth(93);
                i.ScaleAbsoluteHeight(31);

                doc.Add(i);
                Paragraph p2 = new Paragraph("Auftragsreport " + order.aufnr.TrimStart('0') + " " + order.Ktext);
                p2.Alignment = Element.ALIGN_LEFT;
                //p2.Font = new Font(Font.NORMAL,14);
                doc.Add(p2);

                p2 = new Paragraph("Erfasst durch: " + OxStatus.GetInstance().SAPUsername);
                p2.Alignment = Element.ALIGN_LEFT;
                //p2.Font = new Font(Font.NORMAL, 10);
                doc.Add(p2);

                // add tabular data
                p2 = new Paragraph("Zeitübersicht");
                p2.Alignment = Element.ALIGN_LEFT;
                //p2.Font = new Font(Font.NORMAL, 10);
                doc.Add(p2);
                doc.Add(_timeConf_table());

                p2 = new Paragraph("Materialverbrauch");
                p2.Alignment = Element.ALIGN_LEFT;
                //p2.Font = new Font(Font.NORMAL, 10);
                doc.Add(p2);
                doc.Add(_matConf_table());

                


                Paragraph p = new Paragraph(" ");
                p.Alignment = 1;
                doc.Add(p);

                Image signature2 = Image.GetInstance(imageFilename2);
                signature2.Alignment = Element.ALIGN_RIGHT;
                signature2.Alignment = Element.ALIGN_BOTTOM;
                signature2.ScaleAbsoluteWidth(180);
                signature2.ScaleAbsoluteHeight(60);
                doc.Add(signature2);

                Image signature1 = Image.GetInstance(imageFilename1);
                signature1.Alignment = Element.ALIGN_LEFT;
                signature1.Alignment = Element.ALIGN_BOTTOM;
                signature1.ScaleAbsoluteWidth(180);
                signature1.ScaleAbsoluteHeight(60);
                doc.Add(signature1);
                
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                if (doc != null)
                {
                    //doc.CloseDocument();
                    doc.Close();
                }
                    
                if (_fs!= null)
                {
                    _fs.Close();
                }

            }

        }

        private static PdfPTable _timeConf_table()
        {
            PdfPTable table = new PdfPTable(6);

            string[] col = {"Datum", "Vorgang", "Arbeit", "Einheit", "Text", "Leist.Art"};
            table.WidthPercentage = 100;
            table.SetWidths(new Single[] {4, 4, 4, 4, 4, 4});
            table.SpacingBefore = 10;
            var timeConfs = TimeConfManager.GetTimeConfs(_order);
            for (int i = 0; i < col.Length; ++i)
            {
                PdfPCell cell = new PdfPCell(new Phrase(col[i]));
                //cell.BackgroundColor = new BaseColor(204, 204, 204);
                table.AddCell(cell);
            }


            // !! database code omitted !!
            // r.Read is the DbDataReader for whatever flavor 
            // of database you're connecting to; we're iterating
            // over the results returned from the database and 
            // adding rows to the table in the PDF 
            foreach (TimeConf item in timeConfs)
            {
                DateTime dtErsda = DateTime.Now;
                DateTimeHelper.getDate(item.Ersda.ToString(), out dtErsda);
                table.AddCell(dtErsda.ToString("dd.MM.yyyy"));
                table.AddCell(item.Vornr);
                if (item.Ismnw != null)
                    table.AddCell(TimeValue(float.Parse(item.Ismnw.Replace('.', ','))));
                else
                    table.AddCell("");
                table.AddCell(item.Ismne);
                table.AddCell(item.Ltxa1);

                table.AddCell(item.Learr);
            }

            return table;
        }

        private static PdfPTable _matConf_table()
        {
            PdfPTable table = new PdfPTable(3);

            string[] col = {"Datum", "Material", "Menge"};
            table.WidthPercentage = 100;
            table.SetWidths(new Single[] {4, 4, 4});
            table.SpacingBefore = 10;
            var matConfs = MaterialManager.GetMatConfs(_order);
            for (int i = 0; i < col.Length; ++i)
            {
                PdfPCell cell = new PdfPCell(new Phrase(col[i]));
                //cell.BackgroundColor = new BaseColor(204, 204, 204);
                table.AddCell(cell);
            }
            foreach (MatConf item in matConfs)
            {
                table.AddCell(item.Isdd.ToString("dd.MM.yyyy"));
                table.AddCell(item.Matnr + " " + MaterialManager.GetMaterial(item.Matnr, true).Maktx);
                table.AddCell(item.Quantity + " " + item.Unit);
            }
            OxDbManager.GetInstance().CommitTransaction();

            return table;
        }


        public static String TimeValue(float input)
        {
            //double real = 3.87;
            double fraction = input - Math.Floor(input);
            double minutes = fraction * 60;
            return Math.Floor(input) + ":" + Math.Floor(minutes).ToString().PadLeft(2, '0'); ;
            //int abs1 = Math.Abs(input);

        }

        private static PdfPTable CreateDeliveryDataTable(List<Delivery> delivs)
        {
            PdfPTable table = new PdfPTable(6);

            string[] col = { "Beleg", "Name", "Org.kz.", "Menge", " ", "Status / Grund" };
            table.WidthPercentage = 100;
            //table.SetWidths(new Single[] { 4, 4, 4, 4, 4, 4 });
            table.SetWidths(new int[] { 18, 25, 20, 15, 5, 18 });
            table.SpacingBefore = 10;
            String mandt = AppConfig.Mandt;

            PdfPCell cell = new PdfPCell(new Phrase());

            for (int i = 0; i < col.Length; ++i)
            {
                cell = new PdfPCell(new Phrase(col[i]));
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                table.AddCell(cell);
            }

            foreach (Delivery item in delivs)
            {
                cell = new PdfPCell(new Phrase(item.Belnr));
                cell.Colspan = 1;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(item.Empf));
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(item.Shortobj));
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(item.Menge));
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(item.Meins));
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);
                string status = "";
                if (item.Status.Equals("Z"))
                    status = "zugestellt";
                else if (item.Status.Equals("A"))
                    status = "abgeholt";
                else
                    status = "fehlgeschlagen";
                cell = new PdfPCell(new Phrase(status));
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);

                //String barCodeString = "FRA-IA-" + mandt + "-" + item.Belnr;
                String barCodeString = "FRA-IA-001-" + item.Belnr;
                Barcode128 code = new Barcode128();
                code.CodeType = Barcode.CODE128;
                code.ChecksumText = true;
                code.GenerateChecksum = true;
                code.StartStopText = true;
                code.Code = barCodeString;
                PdfContentByte pdfcontentbyte = new PdfContentByte(_writer);
                Image barImage = code.CreateImageWithBarcode(pdfcontentbyte, null, null);

                cell = new PdfPCell(barImage);
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase());
                cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                table.AddCell(cell);
                //cell = new PdfPCell(new Phrase());
                //cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //table.AddCell(cell);
                //cell = new PdfPCell(new Phrase());
                //cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //table.AddCell(cell);
                // *** BEGIN CHANGE 2015-01-23 ***
                //cell = new PdfPCell(new Phrase());
                //cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //table.AddCell(cell);
                //cell = new PdfPCell(new Phrase());
                //cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //table.AddCell(cell);
                if (item.Status.Equals("F"))
                {
                    cell = new PdfPCell(new Phrase(item.Gtext));
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cell.PaddingRight = 10;
                    table.AddCell(cell);
                } else
                {
                    cell = new PdfPCell(new Phrase());
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase());
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase());
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    table.AddCell(cell);
                    cell = new PdfPCell(new Phrase());
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    table.AddCell(cell);
                }
                // *** END CHANGE 2015-01-23 ***
            }

            return table;
        }

        private static PdfPTable GenerateFooterTable(String imageFileName, String kostst, String persnr)
        {
            var emptyCell = new PdfPCell(new Phrase(" "));
            emptyCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            PdfPTable footTable = new PdfPTable(7);
            float[] widths = new float[] { 175, 20, 175, 20, 175, 20, 360 };
            footTable.SetTotalWidth(widths);
            footTable.HorizontalAlignment = Element.ALIGN_LEFT;
            footTable.WidthPercentage = 100;

            PdfPCell cell = new PdfPCell();
            cell = new PdfPCell(new Phrase(DateTime.Now.ToString("dd.MM.yyyy")));
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
            cell.VerticalAlignment = Element.ALIGN_BOTTOM;
            footTable.AddCell(cell);

            footTable.AddCell(emptyCell);

            if (String.IsNullOrEmpty(kostst))
                cell = new PdfPCell(new Phrase(" "));
            else
                cell = new PdfPCell(new Phrase(kostst));
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
            cell.VerticalAlignment = Element.ALIGN_BOTTOM;
            footTable.AddCell(cell);

            footTable.AddCell(emptyCell);

            if (String.IsNullOrEmpty(persnr))
                cell = new PdfPCell(new Phrase(" "));
            else
                cell = new PdfPCell(new Phrase(persnr));
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
            cell.VerticalAlignment = Element.ALIGN_BOTTOM;
            footTable.AddCell(cell);

            footTable.AddCell(emptyCell);

            Image signature1 = Image.GetInstance(imageFileName);
            signature1.Alignment = Element.ALIGN_LEFT | Element.ALIGN_BOTTOM;
            signature1.ScaleAbsoluteWidth(180);
            signature1.ScaleAbsoluteHeight(60);

            cell = new PdfPCell(signature1);
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
            footTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Datum"));
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            footTable.AddCell(cell);

            footTable.AddCell(emptyCell);

            cell = new PdfPCell(new Phrase("Kostenstelle"));
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            footTable.AddCell(cell);

            footTable.AddCell(emptyCell);

            cell = new PdfPCell(new Phrase("Personalnummer"));
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            footTable.AddCell(cell);

            footTable.AddCell(emptyCell);

            cell = new PdfPCell(new Phrase("Unterschrift"));
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            footTable.AddCell(cell);

            return footTable;
        }

        private static void CloseDocument(Document doc)
        {
            doc.Close();

        }
        
    }
}
