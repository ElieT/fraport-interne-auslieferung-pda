﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class FileManager
    {
        private static String BinaryStream = "";
        private static Doch _header;
        private static IA_Doch _IAHeader;
        private static Boolean _processing;
        private static string _path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);

        private static Object _lock = new object();
        public static object Locker { get { return _lock; } set { _lock = value; } }
        private static int _countWriterThreads;
        private static Object _thisLock = new Object();
        private static List<DocUpload> _queryList = new List<DocUpload>();
        public delegate void MyDelegate();
        private static MyDelegate del;
        private static List<DocUpload> _docList;

        private static bool _accessAllowed = true;
        public static bool AccessAllowed { get { return _accessAllowed; } set { _accessAllowed = value; } }

        public static bool Processing
        {
            get { return _processing; }
            set { _processing = value; }
        }


        public static void ReadBinFile(Doch header)
        {
            try
            {
                if (header.DocType.Equals("bmp") || header.DocType.Equals("jpg"))
                {
                    /*var img = Image.FromFile(header.Filename);
                    var newWidth = img.Size.Width;
                    var newHeight = img.Size.Height;
                    if (img.Size.Width > 800)
                        newWidth = 800;
                    newHeight = img.Height * newWidth / img.Width;
                    if (newHeight > 600)
                    {
                        newHeight = 600;
                        newWidth = img.Width * newHeight / img.Height;
                    }
                    var b = new Bitmap(newWidth, newHeight);
                    var g = Graphics.FromImage((Image)b);
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    g.DrawImage(img, 0, 0, newWidth, newHeight);
                    g.Dispose();
                    var filename = header.Filename.Substring(header.Filename.LastIndexOf('\\') + 1);
                    if (!Directory.Exists(@"" + AppConfig.GetAppPath() + "\\images\\"))
                        Directory.CreateDirectory(@"" + AppConfig.GetAppPath() + "\\images\\");
                    var path = @"" + AppConfig.GetAppPath() + "\\images\\" + header.RefObject + "_" + filename.Trim().Replace(" ", "") +"";
                    b.Save(@path);
                    header.Filename = @path;*/
                }
                else
                    Processing = false;
                if (!Processing)
                {

                    OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now, "Datei wird verarbeitet", null, true, 5000);
                    //OxStatus.GetInstance().SetStatusMessage("Datei wird verarbeitet", 100);
                    Processing = true;
                    _header = header;

                    var convStart = new ThreadStart(GetHexStreamValue);
                    var convThread = new Thread(convStart);
                    convThread.Start();
                }
                else
                    Processing = false;
            }
            catch (Exception ex)
            {
                //Console.WriteLine("\"" + AppConfig.GetAppPath() + "\\images\\" + header.RefObject + "_Blaue Berge.jpg\"");
                LogException(ex);
                Processing = false;
            }
        }

        private static void GetHexStreamValue()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(_header.Filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                Docd item;
                var fs2 = "";
                var itemCount = 1;
                var outputStream = "";
                String Linenumber, Line;

                var intDbKey = -1;
                var intDbItemKey = -1;
                var dbResult = -1;
                var sqlStmt = "";
                // Get Key
                sqlStmt = "SELECT MAX(rowid) + 1 FROM D_DOCH";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;

                var id = intDbKey.ToString();
                _header.Id = id;

                for (long i = 0; i < stream.Length; i++)
                {
                    fs2 = fs2 + String.Format("{0:X2}", stream.ReadByte());
                    if (fs2.Length > 255)
                    {
                        String teil = fs2.Substring(0, 255);
                        fs2 = fs2.Substring(255);
                        Line = teil;
                        Linenumber = itemCount.ToString();
                        Linenumber = Linenumber.PadLeft(5, '0');
                        outputStream += "|DOCD;I;" + id + ";" + Linenumber + ";" + Line;
                        itemCount++;
                    }
                }
                item = new Docd();
                Line = fs2;
                Linenumber = itemCount.ToString();
                Linenumber = Linenumber.PadLeft(5, '0');
                outputStream += "|DOCD;I;" + id + ";" + Linenumber + ";" + Line;
                item.Line = outputStream;
                item.Linenumber = "1".PadLeft(5, '0');
                _header.Docds.Add(item);

                var oldFilename = _header.Filename;
                _header.Filename = _header.Filename.Substring(_header.Filename.LastIndexOf("\\") + 1);
                DocManager.SaveDoc(_header);
                _header.Filename = oldFilename;
                OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
                                                             "Datei gespeichert", null, true,
                                                             5000);
                //OxStatus.GetInstance().SetStatusMessage("Datei gespeichert", 100);
                if (AppConfig.FlagShowOrderReportOnSave.Equals("X"))
                    FileManager.ExecuteFile(_header.Filename);
                Processing = false;
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }

        
        public static void WriteFile(String aFilename, String aData)
        {
            try
            {
                string AppPath =
                    Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

                //if (AppPath.Length > 7 && AppPath.IndexOf("file:") == -1)
                //    aFilename = AppPath + "\\" + aFilename;

                if (aFilename.IndexOf("\\") != -1)
                    if (!Directory.Exists(aFilename.Substring(0, aFilename.LastIndexOf("\\"))))
                        Directory.CreateDirectory(aFilename.Substring(0, aFilename.LastIndexOf("\\")));

                var f2 = new FileInfo(aFilename);
                if (f2.Exists && f2.Length > Int64.Parse(AppConfig.Settings["trace.filesize"]))
                {
                    deleteTrace(aFilename);
                }
                using (var writer = new StreamWriter(aFilename, true))
                {
                    writer.WriteLine(aData);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static String FilterPassword(String aInput)
        {
            string output = "";
            String[] replacements = {"&sap-password=", "&pw=", "&pa_pwold=", "&pa_pwnew1", "&pa_pwnew2"};

            try
            {
                for (int i = 0; i < replacements.Length; i++)
                {
                    if (aInput.IndexOf(replacements[i]) != -1)
                    {
                        int start = aInput.IndexOf(replacements[i]) + replacements[i].Length;
                        string temp = aInput.Substring(start, aInput.Length - start);
                        if (temp.IndexOf("&") != -1)
                        {
                            output = aInput.Substring(0, aInput.IndexOf(replacements[i])) +
                                     temp.Substring(temp.IndexOf("&"), temp.Length - temp.IndexOf("&"));
                        }
                        else
                        {
                            output = aInput.Substring(0, aInput.IndexOf(replacements[i]));
                        }
                        aInput = output;
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            return output;
        }

        public static void LogHttp(String message)
        {
            try
            {
                if (message.IndexOf("&sap-password=") != -1 || message.IndexOf("&pw=") != -1 || message.IndexOf("&pa_pwold=") != -1 || message.IndexOf("&pa_pwnew1") != -1 || message.IndexOf("&pa_pwnew2") != -1)
                    WriteFile(Path.Combine(_path, AppConfig.Settings["trace.http.filename"]),
                              DateTime.Now + ": " + FilterPassword(message));
                else
                    WriteFile(Path.Combine(_path, AppConfig.Settings["trace.http.filename"]), DateTime.Now + ": " + message);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }

        public static void LogException(Exception exeption)
        {
            try
            {
                WriteFile(Path.Combine(_path, AppConfig.Settings["trace.filename"]), DateTime.Now + ": Exception at " + exeption);
                OxDbManager.GetInstance().Rollback();
                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, exeption.Message, exeption, true, 5000);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void LogMessage(String message)
        {
            try
            {
                if (message.IndexOf("&sap-password=") != -1 || message.IndexOf("&pw=") != -1 || message.IndexOf("&pa_pwold=") != -1 || message.IndexOf("&pa_pwnew1") != -1 || message.IndexOf("&pa_pwnew2") != -1)
                    WriteFile(Path.Combine(_path, AppConfig.Settings["trace.filename"]),
                              DateTime.Now + ": " + FilterPassword(message));
                else
                    WriteFile(Path.Combine(_path, AppConfig.Settings["trace.filename"]), DateTime.Now + ": " + message);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }

        public static String ReadFileLine(String aFilename)
        {
            String retVal = "";
            try
            {
                if (!File.Exists(aFilename))
                    return retVal;
                // create reader & open file
                TextReader tr = new StreamReader(aFilename);

                // read a line of text
                retVal = tr.ReadLine();

                // close the stream
                tr.Close();
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            return retVal;
        }

        public static byte[] ReadFileBuffer(string filePath)
        {
            byte[] buffer;
            if (!File.Exists(filePath))
                return new byte[0];
            var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            try
            {
                var length = (int) fileStream.Length; // get file length
                buffer = new byte[length]; // create buffer
                int count; // actual number of bytes read
                int sum = 0; // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count; // sum is a buffer offset for next reading
                return buffer;
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            finally
            {
                fileStream.Close();
            }
            return new byte[0];
        }

        public static void deleteTrace(String aFilename)
        {
            try
            {
                String backFilename = aFilename.Substring(0, aFilename.IndexOf(".") + 1) + "bak";
                if (File.Exists(backFilename))
                    File.Delete(backFilename);
                File.Copy(aFilename, backFilename);
                File.Delete(aFilename);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }

        public static IList<String> ReadFileLines(string filePath)
        {
            IList<String> retList = new List<String>();
            try
            {
                string AppPath =
                    Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

                if (AppPath.Length > 7 && AppPath.IndexOf("file:") == -1)
                    filePath = AppPath + "\\" + filePath;

                if (!File.Exists(filePath))
                    return retList;
                var stream = new StreamReader(filePath, Encoding.Default);
                try
                {
                    string strLine = "";
                    while ((strLine = stream.ReadLine()) != null)
                    {
                        retList.Add(strLine);
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex);
                }

                finally
                {
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            return retList;
        }

        public static List<Doch> GetFileList(Order order)
        {
            var docList = new List<Doch>();
            try
            {

                String sqlStmt = "SELECT ID, DOC_TYPE, DESCRIPTION, FILENAME, UPDFLAG FROM D_DOCH WHERE REF_OBJECT = @REF_OBJECT";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@REF_OBJECT", order.Aufnr);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (Dictionary<string, string> rdr in records)
                {
                    var dochTemp = new Doch();
                    dochTemp.Id = rdr["ID"];
                    dochTemp.DocType = rdr["DOC_TYPE"];
                    dochTemp.Description = rdr["DESCRIPTION"];
                    dochTemp.Filename = rdr["FILENAME"];
                    dochTemp.Updflag = rdr["UPDFLAG"];
                    docList.Add(dochTemp);
                }
                return docList;
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            return docList;
        }

        public static void ExecuteFile(String path)
        {
            try
            {
                var p = new Process();
                p.StartInfo.FileName = path;
                p.Start();
            }
            catch(Exception ex)
            {
                LogException(ex);
            }
        }

        public static void DeleteFile(String path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }
        public static void IA_ReadBinFile(IA_Doch header)
        {
            try
            {
                if (!header.DocType.Equals("bmp") && !header.DocType.Equals("jpg"))
                    Processing = false;
                if (!Processing)
                {

                    OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now, "Datei wird verarbeitet", null, true, 5000);
                    //OxStatus.GetInstance().SetStatusMessage("Datei wird verarbeitet", 100);
                    Processing = true;
                    _IAHeader = header;

                    /*var convStart = new ThreadStart(IA_GetHexStreamValue);
                    var convThread = new Thread(convStart);
                    convThread.Start();*/
                    IA_GetHexStreamValue();
                }
                else
                    Processing = false;
            }
            catch (Exception ex)
            {
                LogException(ex);
                Processing = false;
            }
        }
        private static void IA_GetHexStreamValue()
        {
            Stream stream = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                stream = new FileStream(_IAHeader.Filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                IA_Docd item;
                var fs2 = "";
                var itemCount = 1;
                var outputStream = "";
                String Linenumber, Line;

                var intDbKey = -1;
                var intDbItemKey = -1;
                var dbResult = -1;
                var sqlStmt = "";
                // Get Key
                sqlStmt = "SELECT MAX(rowid) + 1 FROM D_ZFRAPORT_IA_DOCH";
                var cmd = new SQLiteCommand(sqlStmt);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;
                OxDbManager.GetInstance().CommitTransaction();

                var id = intDbKey.ToString();
                _IAHeader.Id = id;

                for (long i = 0; i < stream.Length; i++)
                {
                    fs2 = fs2 + String.Format("{0:X2}", stream.ReadByte());
                    if (fs2.Length > 255)
                    {
                        String teil = fs2.Substring(0, 255);
                        fs2 = fs2.Substring(255);
                        Line = teil;
                        //if (teil.TrimEnd().Length < 255)
                        //    Line.PadRight(255, '0');
                        Linenumber = itemCount.ToString();
                        Linenumber = Linenumber.PadLeft(5, '0');
                        outputStream += "|ZFRAPORT_IA_DOCD;I;" + Linenumber + ";" + Line + ";" + _IAHeader.MobileKey;
                        itemCount++;
                    }
                }
                item = new IA_Docd();
                item.MobileKey = _IAHeader.MobileKey;
                item.Dirty = "X";
                Line = fs2;
                Linenumber = itemCount.ToString();
                Linenumber = Linenumber.PadLeft(5, '0');
                //if (Line.TrimEnd(' ').Length < 255)
                //    Line.PadRight(255, '0');
                outputStream += "|ZFRAPORT_IA_DOCD;I;" + Linenumber + ";" + Line + ";" + _IAHeader.MobileKey;
                item.Line = outputStream;
                item.Linenumber = "1".PadLeft(5, '0');

                _IAHeader.Docds.Add(item);

                var oldFilename = _IAHeader.Filename;
                _IAHeader.Filename = _IAHeader.Filename.Substring(_IAHeader.Filename.LastIndexOf("\\") + 1);
                DocManager.IA_SaveDoc(_IAHeader);
                _IAHeader.Filename = oldFilename;
                OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
                                                             "Datei gespeichert", null, true,
                                                             5000);

                Processing = false;
                //Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        //public static void MyCallbackProc()
        //{
        //    lock (_lock)
        //    {
        //        //var oldFilename = _header.Filename;

        //        if (_header.Filename.EndsWith(""))
        //        {
        //            //if (_header.Filename.EndsWith(".bmp") || _header.Filename.EndsWith(".gif") || _header.Filename.EndsWith(".jpg"))
        //            //{

        //            ////    _header.Filename = _header.Filename.Substring(_header.Filename.LastIndexOf("\\") + 1);
        //            //}
        //            //else
        //            //{
        //            //     _header.Filename = _header.RefType + "_" + _header.RefObject + "_" + _header.Filename.Substring(_header.Filename.LastIndexOf("\\") + 1);
        //            //}
        //            //if(_header.Filename.Contains(Localization.Control_labels.Control_BO.OrderReport))
        //            //{
        //            //    string filename = _header.Filename;
        //            //    filename = filename.Remove(0, 16);
        //            //    _header.Filename = filename;
        //            //}
        //            //if(_header.Filename.Contains(Localization.Control_labels.Control_BO.Checklist_Result))
        //            //{
        //            //    string filename = _header.Filename;
        //            //    filename = filename.Remove(0, 16);
        //            //    _header.Filename = filename;
        //            //}
        //            var filename = _header.Filename.Substring(_header.Filename.LastIndexOf("\\") + 1);
        //            _header.Filename = "1_" + filename;

        //        }
        //        _header.DocSize = FileManager.GetFileSize(_header);
        //        DocUploadManager.SaveDoc(_header);
        //        //_header.Filename = oldFilename;
        //        OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now,
        //                                                   "Datei wurde erfolgreich gespeichert.", null, true,
        //                                                   5000);
        //    }

        //    DocSavedEvent(null, EventArgs.Empty);

        //    if (AppConfig.FlagShowOrderReportOnSave.Equals("X"))
        //    {
        //        bool folderExist = System.IO.Directory.Exists(AppConfig.GetAppPath() + "\\temp");
        //        string newPath = AppConfig.GetAppPath() + "\\temp";

        //        // Create the subfolder
        //        if (folderExist == false)
        //            System.IO.Directory.CreateDirectory(newPath);

        //        string source = _header.FileLocation;
        //        //destination = _header.FileLocation;
        //        var destination = AppConfig.GetAppPath() + "\\temp\\" + _header.OrgFilename;
        //        File.Copy(source, destination, true);
        //        //if (_header != null && File.Exists(destination))
        //        //    FileManager.ExecuteFile(destination);
        //        //else
        //        //{
        //        //    File.Copy(source, destination, true);

        //        //    if (_header != null && File.Exists(destination))
        //        //        FileManager.ExecuteFile(destination);
        //        //}
        //    }

        //    Processing = false;
        //    _queryList.Remove(_header);
        //    CountWriter(-1);
        //}


        public static string GetNextDocumentId(bool inTransaction)
        {
            var sqlStmt = "SELECT MAX(rowid) + 1 FROM S_DOCUPLOAD";

            var intDbKey = "0";
            List<Dictionary<string, string>> records;
            var cmd = new SQLiteCommand(sqlStmt);
            records = OxDbManager.GetInstance().FeedTransaction(cmd);
            if (!inTransaction)
                OxDbManager.GetInstance().CommitTransaction();
            var result = records[0]["RetVal"];
            if (result.Length > 0)
                intDbKey = result;
            else
                intDbKey = "1";

            return intDbKey;
        }

        //ermittelt die Dateigröße
        public static string GetFileSize(DocUpload doc)
        {
            long size = 0;
            doc.DocSize = "";

            try
            {
                FileInfo FI = new FileInfo(doc.FileLocation);
                size += FI.Length;
                doc.DocSize = ConvertByte(size);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            return doc.DocSize;

        }

        //Umwandlung von Bytes
        public static string ConvertByte(long bytes)
        {
            {
                double s = bytes;
                string[] format = new string[]
                  {
                      "{0} bytes", "{0} KB",  
                      "{0} MB", "{0} GB", "{0} TB", "{0} PB", "{0} EB"
                  };

                int i = 0;

                while (i < format.Length && s >= 1024)
                {
                    s = (long)(100 * s / 1024) / 100.0;
                    i++;

                }
                return string.Format(format[i], s);
            }

        }
    }
}