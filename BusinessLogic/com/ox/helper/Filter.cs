using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.sync;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class Filter
    {
        public static string[,] GetFilterFields(string objectType)
        {
            var retVal = new String[0, 2];
            try
            {
                String sqlStmt = "SELECT column_x,filterType " +
                                           "FROM S_HLP_COLUMNORDER " +
                                           "WHERE table_x = '" + objectType + "' " +
                                           "AND filterType <> ''";

                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                retVal = new string[records.Count,2];
                var i = 0;
                foreach (Dictionary<string, string> rdr in records)
                {
                    retVal[i, 0] = rdr["column_x"];
                    retVal[i, 1] = rdr["filterType"];
                    i++;
                }

                return retVal;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static String[,] GetFields(String type)
        {
            String[,] fields;
            try
            {
                switch (type)
                {
                    case "Order":
                        fields = GetFilterFields("D_ORDHEAD"); //Array containing the fields that can be filtered
                        break;
                    case "Notif":
                        fields = GetFilterFields("D_NOTIHEAD"); //Array containing the fields that can be filtered
                        break;
                    case "Material":
                        fields = GetFilterFields("D_MATERIAL"); //Array containing the fields that can be filtered
                        break;
                    case "Equi":
                        fields = GetFilterFields("D_EQUI"); //Array containing the fields that can be filtered
                        break;
                    case "FuncLoc":
                        fields = GetFilterFields("D_FLOC"); //Array containing the fields that can be filtered
                        break;
                    default:
                        fields = GetFilterFields("D_ORDHEAD"); //Array containing the fields that can be filtered
                        break;
                }
                return fields;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return null;
        }

        public static Hashtable GetFieldDesc(String type)
        {
            Hashtable fieldDesc;
            try
            {
                switch (type)
                {
                    case "Order":
                        fieldDesc = DBManager.GetFieldDesc("D_ORDHEAD"); //Hashtable containing the fielddescriptions
                        break;
                    case "Notif":
                        fieldDesc = DBManager.GetFieldDesc("D_NOTIHEAD"); //Hashtable containing the fielddescriptions
                        break;
                    case "Material":
                        fieldDesc = DBManager.GetFieldDesc("D_MATERIAL"); //Hashtable containing the fielddescriptions
                        break;
                    case "Equi":
                        fieldDesc = DBManager.GetFieldDesc("D_EQUI"); //Hashtable containing the fielddescriptions
                        break;
                    case "FuncLoc":
                        fieldDesc = DBManager.GetFieldDesc("D_FLOC"); //Hashtable containing the fielddescriptions
                        break;
                    case "MeasPoint":
                        fieldDesc = DBManager.GetFieldDesc("D_MEASPOINT"); //Hashtable containing the fielddescriptions
                        break;
                    default:
                        fieldDesc = DBManager.GetFieldDesc("D_ORDHEAD"); //Hashtable containing the fielddescriptions
                        break;
                }
                return fieldDesc;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return null;
        }

        public static object FilterData(object data, String[,] filterArgs)
        {
            object retVal = null;
            try
            {
                switch (data.GetType().GetGenericArguments()[0].Name)
                {
                    case "Order":
                        retVal = FilterOrder((List<Order>) data, filterArgs);
                        break;
                    case "Notif":
                        retVal = FilterNotif((List<Notif>) data, filterArgs);
                        break;
                    case "Material":
                        retVal = FilterMaterial((List<Material>) data, filterArgs);
                        break;
                    case "Equi":
                        retVal = FilterEqui((List<Equi>) data, filterArgs);
                        break;
                    case "FuncLoc":
                        retVal = FilterFuncLoc((List<FuncLoc>) data, filterArgs);
                        break;
                    case "MeasurementPoint":
                        retVal = FilterMeasPoint((List<MeasurementPoint>)data, filterArgs);
                        break;
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static Dictionary<string, int> GetFieldLengths(string name)
        {
            Dictionary<string, int> ret = new Dictionary<string, int>();
            var tableName = "";
            try
            {
                switch (name)
                {
                    case "Order":
                        tableName = "D_ORDER";
                        break;
                    case "Notif":
                        tableName = "D_NOTIHEAD";
                        break;
                    case "Material":
                        tableName = "C_MATERIAL";
                        break;
                    case "Equi":
                        tableName = "D_EQUI";
                        break;
                    case "FuncLoc":
                        tableName = "D_FLOC";
                        break;
                    case "MeasurementPoint":
                        tableName = "C_MEASPOINT";
                        break;
                }

                var sqlStmt = "SELECT * FROM " + tableName;
                var cmd = new SQLiteCommand(sqlStmt);
                var dt = OxDbManager.GetTableSchema(cmd);
                foreach(DataRow row in dt.Rows)
                {
                    ret.Add((string) row["ColumnName"], (int) row["ColumnSize"]);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return ret;
        }

        private static List<Order> FilterOrder(List<Order> orders, String[,] args)
        {
            var newOrderList = new List<Order>();
            var equals = true;

            try
            {
                foreach (var order in orders)
                {
                    for (var i = 0; i < args.GetLength(0); i++)
                    {
                        if ((args[i, 0].ToUpper()).Equals("AUFNR") && !args[i, 1].Equals(""))
                            equals = CheckString(order.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1]);
                        else if ((args[i, 0].ToUpper()).Equals("EQUIEQFNR") && !args[i, 1].Equals(""))
                        {
                            if (args[i, 1].StartsWith("0"))
                                equals = CheckString(order.GetValueByString(args[i, 0]), args[i, 1]);
                            else
                                equals =
                                    CheckString(order.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1].TrimStart
                                        ('0'));
                        }
                        else if ((args[i, 0].ToUpper()).Equals("EQUNR") && !args[i, 1].Equals(""))
                        {
                            if (args[i, 1].StartsWith("0"))
                                equals = CheckString(order.GetValueByString(args[i, 0]), args[i, 1]);
                            else
                                equals =
                                    CheckString(order.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1].TrimStart
                                        ('0'));
                        }
                        else if ((args[i, 0].ToUpper()).Equals("FSAVD") && !args[i, 1].Equals(""))
                        {
                            if (order.OrderOperations != null)
                            {
                                foreach (var op in order.OrderOperations)
                                {
                                    if (String.IsNullOrEmpty(op.GetValueByString(args[i, 0])))
                                        equals = CheckString(op.GetValueByString("FSAVD"), args[i, 1]);
                                    else
                                        equals = CheckString(op.GetValueByString(args[i, 0]), args[i, 1]);
                                }
                            }
                            else
                                break;
                        }
                        else if ((args[i, 0].ToUpper()).Equals("FSAVZ") && !args[i, 1].Equals(""))
                        {
                            if (order.OrderOperations != null)
                            {
                                var op = order.OrderOperations.OrderBy(o=>o.Fsavd).ThenBy(o=>o.Fsavz).First();
                                if (String.IsNullOrEmpty(op.GetValueByString(args[i, 0])))
                                {
                                    //if (op.GetValueByString("FSAVZ").ToString().Equals(args[i, 1]) || op.GetValueByString("FSAVZ").ToString().StartsWith(args[i, 1]))
                                    //equals = true;
                                    equals = CheckString(op.GetValueByString("FSAVZ"), args[i, 1]);
                                }
                                else
                                    equals = CheckString(op.GetValueByString(args[i, 0]), args[i, 1]);
                                if (equals)
                                    break;
                            }
                            else
                                break;
                        }
                        else
                            equals = CheckString(order.GetValueByString(args[i, 0]), args[i, 1]);
                        if (!equals)
                            break;
                    }
                    if (equals)
                        newOrderList.Add(order);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return newOrderList;
        }

        private static List<bo.Notif> FilterNotif(List<bo.Notif> notifs, String[,] args)
        {
            var newNotifList = new List<bo.Notif>();
            var equals = true;
            try
            {
                foreach (var notif in notifs)
                {
                    equals = true;
                    for (var i = 0; i < args.GetLength(0); i++)
                    {
                        if (args[i, 0] == null)
                            continue;
                        if (args[i, 0].Equals("QMNUM") && !args[i, 1].Equals(""))
                            equals = CheckString(notif.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1]);
                        else if ((args[i, 0].ToUpper()).Equals("EQFNR") && !args[i, 1].Equals(""))
                        {
                            if (args[i, 1].StartsWith("0"))
                                equals = CheckString(notif.GetValueByString(args[i, 0]), args[i, 1]);
                            else
                                equals =
                                    CheckString(notif.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1].TrimStart
                                        ('0'));
                        }
                        //else if (args[i, 0].Equals("EQFNR") && !args[i, 1].Equals(""))
                        //    equals = CheckString(notif.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1]);
                        else if(args[i, 0].Equals("STRMN") && !args[i, 1].Equals(""))
                            equals = CheckString(notif.GetValueByString(args[i, 0]), args[i, 1]);
                        else if (args[i, 0].Equals("EQUNR") && !args[i, 1].Equals(""))
                            equals = CheckString(notif.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1].TrimStart('0'));
                        else if(!args[i, 1].Equals(""))
                            equals = CheckString(notif.GetValueByString(args[i, 0]), args[i, 1]);
                        if (!equals)
                            break;
                    }
                    if (equals)
                        newNotifList.Add(notif);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return newNotifList;
        }

        private static List<Material> FilterMaterial(List<Material> material, String[,] args)
        {
            var newMaterialList = new List<Material>();
            var equals = true;
            try
            {
                foreach (var mat in material)
                {
                    for (var i = 0; i < args.GetLength(0); i++)
                    {
                        if (args[i, 0].Equals("Matnr") && !args[i, 1].Equals(""))
                            equals = CheckString(mat.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1]);
                        else
                        equals = CheckString(mat.GetValueByString(args[i, 0]), args[i, 1]);
                        if (!equals)
                            break;
                    }
                    if (equals)
                        newMaterialList.Add(mat);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return newMaterialList;
        }

        private static List<Equi> FilterEqui(List<Equi> equipment, String[,] args)
        {
            var newEquiList = new List<Equi>();
            var equals = true;
            try
            {
                foreach (var equi in equipment)
                {
                    for (var i = 0; i < args.GetLength(0); i++)
                    {
                        if (args[i, 0].Equals("equnr") && !args[i, 1].Equals(""))
                            equals = CheckString(equi.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1]);
                        else if(args[i, 0].Equals("EQUI") && !args[i, 1].Equals(""))
                            equals = CheckString(equi.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1]);
                        else
                            equals = CheckString(equi.GetValueByString(args[i, 0]), args[i, 1]);
                        if (!equals)
                            break;
                    }
                    if (equals)
                        newEquiList.Add(equi);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return newEquiList;
        }

        private static List<FuncLoc> FilterFuncLoc(List<FuncLoc> funcLocs, String[,] args)
        {
            var newFuncLocList = new List<FuncLoc>();
            var equals = true;
            try
            {
                foreach (var funcLoc in funcLocs)
                {
                    for (var i = 0; i < args.GetLength(0); i++)
                    {
                        equals = CheckString(funcLoc.GetValueByString(args[i, 0]), args[i, 1]);
                        if (!equals)
                            break;
                    }
                    if (equals)
                        newFuncLocList.Add(funcLoc);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return newFuncLocList;
        }

        private static List<MeasurementPoint> FilterMeasPoint(List<MeasurementPoint> measPoints, String[,] args)
        {
            var newMeasPointList = new List<MeasurementPoint>();
            var equals = true;
            try
            {
                foreach (var measPoint in measPoints)
                {
                    for (var i = 0; i < args.GetLength(0); i++)
                    {
                        if (args[i, 0].Equals("point") && !args[i, 1].Equals(""))
                            equals = CheckString(measPoint.GetValueByString(args[i, 0]).TrimStart('0'), args[i, 1]);
                        else
                            equals = CheckString(measPoint.GetValueByString(args[i, 0]), args[i, 1]);
                        if (!equals)
                            break;
                    }
                    if (equals)
                        newMeasPointList.Add(measPoint);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return newMeasPointList;
        }

        private static Boolean CheckString(String data, String compareTo)
        {
            var retVal = true;
            var startString = "";
            var endString = "";
            var start = new List<String>();
            var end = new List<String>();
            var op = "";

            try
            {
                if (compareTo == null)
                    compareTo = "";
                compareTo = compareTo.ToLower();
                if (data == null)
                    data = "";
                data = data.ToLower();

                if (!compareTo.Equals(""))
                {
                    //check if compareTo is a range
                    var posOfDots = compareTo.IndexOf("..");

                    if (posOfDots > 0)
                    {
                        startString = compareTo.Substring(0, posOfDots);
                        endString = compareTo.Substring(posOfDots + 1);
                    }
                    else
                    {
                        startString = compareTo;
                        //find operator
                        switch (startString.Substring(0, 1))
                        {
                            case "<":
                                op = "<";
                                startString = startString.Substring(1);
                                break;
                            case ">":
                                op = ">";
                                startString = startString.Substring(1);
                                break;
                        }
                        if (startString.Substring(0, 1) == "=" && !op.Equals(""))
                        {
                            op += "=";
                            startString = startString.Substring(1);
                        }
                    }

                    var found = true;
                    var pos = 0;
                    do
                    {
                        pos = startString.IndexOf("*");
                        if (pos > 0)
                        {
                            start.Add(startString.Substring(0, pos + 1));
                            startString = startString.Substring(pos + 1);
                        }
                        else
                        {
                            found = false;
                            if (start.Count == 0)
                            {
                                start.Add(startString);
                            }
                        }
                    } while (found);

                    if (endString.Length > 0)
                    {
                        found = true;
                        do
                        {
                            pos = endString.IndexOf("*");
                            if (pos > 0)
                            {
                                end.Add(endString.Substring(0, pos));
                                endString = endString.Substring(pos);
                            }
                            else
                            {
                                found = false;
                                end.Add(endString);
                            }
                        } while (found);
                    }

                    retVal = compareStrings(data, start, op);

                    if (end.Count != 0)
                    {
                        //range
                        retVal = compareStrings(data, end, op);
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        private static Boolean compareStrings(String data, List<String> compareTo, String op)
        {
            var retVal = true;
            try
            {
                //range
                foreach (var s in compareTo)
                {
                    if (s.Length > data.Length)
                    {
                        //searchstring is longer than the data. ==> no match
                        retVal = false;
                    }
                    else if ((op.Equals("") || op.Equals("=")) && s.Length != data.Length && s.IndexOf("*") == s.Length)
                    {
                        //Strings must equal but have different Length ==> no match
                        retVal = false;
                    }
                    else
                    {
                        //transform strings to char-Arrays for comparison
                        var searchChar = s.ToCharArray();
                        var dataToChar = data.ToCharArray();
                        var like = false;
                        //check if wildcard is set
                        if (searchChar[searchChar.GetLength(0) - 1] == '*')
                            like = true;
                        switch (op)
                        {
                            case "<":
                                for (var i = 0; i < searchChar.GetLength(0); i++)
                                {
                                    if (searchChar[i] < dataToChar[i])
                                    {
                                        if (searchChar[i] != '*')
                                        {
                                            //searchstring has lower value than data ==> no match
                                            retVal = false;
                                        }
                                    }
                                    else if (searchChar[i] > dataToChar[i] && !like)
                                        break;
                                    if (i == (searchChar.GetLength(0) - 1))
                                    {
                                        if (searchChar[i - 1] <= dataToChar[i - 1])
                                        {
                                            //searchstring equals or has lower value than data ==> no match
                                            retVal = false;
                                        }
                                        if (searchChar[i] <= dataToChar[i] && searchChar[i] != '*')
                                        {
                                            //searchstring equals or has lower value than data ==> no match
                                            retVal = false;
                                        }
                                    }
                                    if (i == searchChar.GetLength(0) - 1 && searchChar[i] != '*' &&
                                        (searchChar[i] <= dataToChar[i]))
                                    {
                                        retVal = false;
                                    }
                                }
                                break;
                            case ">":
                                for (var i = 0; i < searchChar.GetLength(0); i++)
                                {
                                    if (searchChar[i] > dataToChar[i])
                                    {
                                        if (searchChar[i] != '*')
                                        {
                                            //searchstring has higher value than data ==> no match
                                            retVal = false;
                                        }
                                    }
                                    else if (searchChar[i] < dataToChar[i] && !like)
                                        break;
                                    if (i == (searchChar.GetLength(0) - 1))
                                    {
                                        if (searchChar[i] == '*')
                                        {
                                            if (searchChar[i - 1] >= dataToChar[i - 1])
                                            {
                                                //searchstring equals or has lower value than data ==> no match
                                                retVal = false;
                                            }
                                        }
                                        if (searchChar[i] >= dataToChar[i])
                                        {
                                            //searchstring equals or has lower value than data ==> no match
                                            retVal = false;
                                        }

                                    }
                                    if (i == searchChar.GetLength(0) - 1 && searchChar[i] != '*' &&
                                        (searchChar[i] >= dataToChar[i]))
                                    {
                                        retVal = false;
                                    }
                                }
                                break;
                            case "=":
                                for (var i = 0; i < searchChar.GetLength(0); i++)
                                {
                                    if (searchChar[i] != dataToChar[i])
                                    {
                                        if (searchChar[i] != '*')
                                        {
                                            //searchstring does not equal data ==> no match
                                            retVal = false;
                                        }
                                    }

                                    if (i == searchChar.GetLength(0) - 1 && searchChar[i] != '*' &&
                                        (searchChar[i] != dataToChar[i]))
                                    {
                                        retVal = false;
                                    }
                                }
                                break;
                            case "":
                                for (var i = 0; i < searchChar.GetLength(0); i++)
                                {
                                    if (searchChar[i] != dataToChar[i])
                                    {
                                        if (searchChar[i] != '*')
                                        {
                                            //searchstring does not equal data ==> no match
                                            retVal = false;
                                            break;
                                        }
                                    }
                                    if (i == searchChar.GetLength(0) - 1 && searchChar[i] != '*' &&
                                        (searchChar[i] != dataToChar[i]))
                                    {
                                        retVal = false;
                                    }
                                }
                                break;
                            case "<=":
                                for (var i = 0; i < searchChar.GetLength(0); i++)
                                {
                                    if (searchChar[i] < dataToChar[i])
                                    {
                                        if (searchChar[i] != '*')
                                        {
                                            //searchstring has lower value than data ==> no match
                                            retVal = false;
                                        }
                                    }
                                    else if (searchChar[i] > dataToChar[i] && !like)
                                        break;
                                    if (i == (searchChar.GetLength(0) - 1))
                                    {
                                        if (searchChar[i - 1] < dataToChar[i - 1])
                                        {
                                            //searchstring equals or has lower value than data ==> no match
                                            retVal = false;
                                        }
                                        if (searchChar[i] < dataToChar[i] && searchChar[i] != '*')
                                        {
                                            //searchstring equals or has lower value than data ==> no match
                                            retVal = false;
                                        }
                                    }
                                    if (i == searchChar.GetLength(0) - 1 && searchChar[i] != '*' &&
                                        (searchChar[i] < dataToChar[i]))
                                    {
                                        retVal = false;
                                    }
                                }
                                break;
                            case ">=":
                                for (var i = 0; i < searchChar.GetLength(0); i++)
                                {
                                    if (searchChar[i] > dataToChar[i])
                                    {
                                        if (searchChar[i] != '*')
                                        {
                                            //searchstring has lower value than data ==> no match
                                            retVal = false;
                                        }
                                    }
                                    else if (searchChar[i] < dataToChar[i] && !like)
                                        break;
                                    if (i == (searchChar.GetLength(0) - 1))
                                    {
                                        if (searchChar[i - 1] > dataToChar[i - 1])
                                        {
                                            //searchstring equals or has lower value than data ==> no match
                                            retVal = false;
                                        }
                                        if (searchChar[i] > dataToChar[i] && searchChar[i] != '*')
                                        {
                                            //searchstring equals or has lower value than data ==> no match
                                            retVal = false;
                                        }
                                    }
                                    if (i == searchChar.GetLength(0) - 1 && searchChar[i] != '*' &&
                                        (searchChar[i] > dataToChar[i]))
                                    {
                                        retVal = false;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
    }
}