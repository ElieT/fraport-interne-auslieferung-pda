﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class OxCrypto
    {
        public static String HashPasswordForStoringInConfigFile(String arg)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            Byte[] hashedBytes;
            UTF8Encoding encoder = new UTF8Encoding();
            var hashArray = md5Hasher.ComputeHash(encoder.GetBytes(arg));
            string hash = string.Empty;
            foreach (var item in hashArray)
            {
                hash += item.ToString("X2");
            }
            return hash;
        }
    }
}
