﻿using System;
using System.Collections;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class SyncLogData
    {
        private String DBYTES;
        private String DRECORDS;
        private String EDATE;
        private String ERRORID;
        private String ETIME;
        private String FAILED;
        private Int64 ID;

        private String MESSAGE1,
                       MESSAGE2;

        public string name;

        private String SDATE,
                       STIME;

        public DateTime timestamp;
        private String UBYTES;
        private String URECORDS;
        public string value;
        private Hashtable typeSummary;

        public SyncLogData(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        public SyncLogData(string sdate, string stime, string edate, string etime, string ubytes, string dbytes,
                           string urecords, string drecords, string failed, string errorid, string message1,
                           string message2, Hashtable _TypeSummary)
        {
            SDATE = sdate;
            STIME = stime;
            EDATE = edate;
            ETIME = etime;
            UBYTES = ubytes;
            DBYTES = dbytes;
            URECORDS = urecords;
            DRECORDS = drecords;
            FAILED = failed;
            ERRORID = errorid;
            MESSAGE1 = message1;
            MESSAGE2 = message2;
            typeSummary = _TypeSummary;
        }

        public SyncLogData()
        {
        }

        public Hashtable TypeSummary
        {
            get { return typeSummary; }
            set { typeSummary = value; }
        }

        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public long Id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string Sdate
        {
            get { return SDATE; }
            set { SDATE = value; }
        }

        public string Stime
        {
            get { return STIME; }
            set { STIME = value; }
        }

        public string Edate
        {
            get { return EDATE; }
            set { EDATE = value; }
        }

        public string Etime
        {
            get { return ETIME; }
            set { ETIME = value; }
        }

        public string Ubytes
        {
            get { return UBYTES; }
            set { UBYTES = value; }
        }

        public string Dbytes
        {
            get { return DBYTES; }
            set { DBYTES = value; }
        }

        public string Urecords
        {
            get { return URECORDS; }
            set { URECORDS = value; }
        }

        public string Drecords
        {
            get { return DRECORDS; }
            set { DRECORDS = value; }
        }

        public string Failed
        {
            get { return FAILED; }
            set { FAILED = value; }
        }

        public string Errorid
        {
            get { return ERRORID; }
            set { ERRORID = value; }
        }

        public string Message1
        {
            get { return MESSAGE1; }
            set { MESSAGE1 = value; }
        }

        public string Message2
        {
            get { return MESSAGE2; }
            set { MESSAGE2 = value; }
        }
    }
}