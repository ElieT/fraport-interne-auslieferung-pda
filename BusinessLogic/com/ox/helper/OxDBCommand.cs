﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class OxDBCommand
    {
        private List<Dictionary<string, string>> dbResult = new List<Dictionary<string, string>>();
        private String sqlStmt = "";
        private Dictionary<string, string> parameter = new Dictionary<string, string>();
        public enum AccessType { CReader, CScalar, CUpdate, CInsert, CDelete };
        private int dbIntResult = 0;
        private String dbScalarResult = "";
        private AccessType accessType = AccessType.CReader;

        public OxDBCommand(string sqlStmt)
        {
            this.sqlStmt = sqlStmt;
        }

        public OxDBCommand(string sqlStmt, Dictionary<string, string> parameter)
        {
            this.sqlStmt = sqlStmt;
            this.parameter = parameter;
        }

        public AccessType MyAccessType
        {
            get { return accessType; }
            set { accessType = value; }
        }

        public List<Dictionary<string, string>> DbResult
        {
            get { return dbResult; }
            set { dbResult = value; }
        }

        public string SqlStmt
        {
            get { return sqlStmt; }
            set { sqlStmt = value; }
        }

        public Dictionary<string, string> Parameter
        {
            get { return parameter; }
            set { parameter = value; }
        }

        public int DbIntResult
        {
            get { return dbIntResult; }
            set { dbIntResult = value; }
        }

        public string DbScalarResult
        {
            get { return dbScalarResult; }
            set { dbScalarResult = value; }
        }
    }
}
