﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class FormatHelper
    {
        public const String Charact = "CHAR";
        public const String Numeric = "NUM";
        public const String Date = "DATE";
        public const String Time = "TIME";
        public const String Hour = "HOUR";
        public const String Minute = "MINUTE";

        public static Boolean CheckFormat(String formatType, int maxLength, int decimals, String value)
        {
            switch (formatType)
            {
                case Charact:
                    return CheckCharact(maxLength, value);
                case Numeric:
                    return CheckNumeric(maxLength, decimals, value);
                case Date:
                    return CheckDate(maxLength, value);
                case Time:
                    return CheckTime(maxLength, value);
                case Hour:
                    return CheckHour(value);
                case Minute:
                    return CheckMinute(value);
                default:
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Unbekanntes Datenformat. Formatprüfung nicht möglich.", null, true, 5000);
                    return false;
            }
        }

        private static Boolean CheckCharact(int maxLength, String value)
        {
            return true;
        }

        private static Boolean CheckNumeric(int maxLength, int decimals, String value)
        {
            var last = value.Last();
            var length = value.Length;
            if (last != 46 && last != 44 && (last < 48 || last > 57))
                return false;
            if ((last == 46 || last == 44) && decimals == 0)
                return false;
            if (length > maxLength)
                return false;
            var posOfDot = value.IndexOf('.') + 1;
            var posOfComma = value.IndexOf(',') + 1;
            if (posOfDot != 0 && posOfComma != 0)
                return false;
            var lastPosDot = value.LastIndexOf('.') + 1;
            var lastPosComma = value.LastIndexOf(',') + 1;
            if (((posOfDot > 0) && posOfDot > maxLength - 1) ||
                ((posOfComma > 0) && posOfComma > maxLength - 1))
                return false;
            int posOfSeperator = 0;
            if (posOfDot != 0)
                posOfSeperator = posOfDot;
            if (posOfComma != 0)
                posOfSeperator = posOfComma;
            if (posOfDot != lastPosDot || posOfComma != lastPosComma)
                return false;
            if (posOfSeperator > 0 && (length - (posOfSeperator + 1)) >= decimals)
                return false;

            return true;
        }

        private static Boolean CheckDate(int maxLength, String value)
        {
            var last = value.Last();
            var length = value.Length;
            if (last != 46 && (last < 48 || last > 57))
                return false;
            if (length == 3 || length == 6)
                if (last != 46)
                    return false;

            if (length == maxLength)
            {
                string[] formats = { "dd.MM.yyyy" };
                try
                {
                    DateTime.ParseExact(value, formats, CultureInfo.CurrentCulture, DateTimeStyles.None);
                }
                catch (FormatException ex)
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Ungültige Zeit.", null, true, 5000);
                    return false;
                }
                return true;
            }
            return true;
        }

        private static Boolean CheckTime(int maxLength, String value)
        {
            var last = value.Last();
            var length = value.Length;
            if (last < 48 || last > 58)
                return false;
            if (length == 3 || length == 6)
                if (last != 58)
                    return false;

            if (length == maxLength)
            {
                try
                {
                    DateTime.Parse(value, CultureInfo.CurrentCulture, DateTimeStyles.None);
                }
                catch (FormatException ex)
                {
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Ungültiges Datum.", null, true, 5000);
                    return false;
                }
                return true;
            }
            return true;
        }

        private static Boolean CheckHour(String value)
        {
            if(value.Length == 0)
                return true;
            var last = value.Last();
            var length = value.Length;
            if (last < 48 || last > 58 || length > 2)
                return false;
            try
            {
                var intVal = int.Parse(value);
                if (intVal < 24 && intVal > 0)
                    return true;
                return false;
            }
            catch (FormatException ex)
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Ungültige Zeit.", null, true, 5000);
                return false;
            }
        }


        public static Boolean CheckFormatHours(String value)
        {
            if (value.Length == 0)
                return true;
            var last = value.Last();
            var length = value.Length;
            if (last < 48 || last > 58 || length > 2)
                return false;
            try
            {
                var intVal = int.Parse(value);
                if (intVal < 100 && intVal > 0)
                    return true;
                return false;
            }
            catch (FormatException ex)
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Ungültige Zeit.", null, true, 5000);
                return false;
            }
        }

        private static Boolean CheckMinute(String value)
        {
            var last = value.Last();
            var length = value.Length;
            if (last < 48 || last > 58 || length > 2)
                return false;
            try
            {
                var intVal = int.Parse(value);
                if (intVal < 60 && intVal > -1)
                    return true;
                return false;
            }
            catch (FormatException ex)
            {
                OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Ungültige Zeit.", null, true, 5000);
                return false;
            }
        }

        public static bool IsRFIDRelevant(String eqfnr)
        {
            if ((eqfnr.Length != 12 && eqfnr.Length != 14) || !IsNumeric(eqfnr))
                return false;
            return true;
        }

        public static bool IsNumeric(string value)
        {
            try
            {
                long.Parse(value);
                return (true);
            }
            catch
            {
                return (false);
            }
        }
    }
}
