﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class OxStatus
    {
        private readonly Timer _timerStatusBarRefresh = new Timer();
        private static readonly object Padlock = new object();
        private static OxStatus _instance;
        private static readonly List<Dictionary<string, string>> _messageList = new List<Dictionary<string, string>>();
        /*
        private static String _sapPing = "";
        private static String _sapPwStatus1 = "";
        private static String _sapPwStatus2 = "";
        private static String _sapLockStatus = "";
        private static String _sapLastLogon = "";
        private static String _sapUserid = "";
        private static String _sapSysId = "";
        private static String _sapMessage = "";
        private static String _sapSubRcUser = "";
        private static String _sapSubRcPw = "";
        private static String _sapLocnt = "";
        private static String _sapDeviceID = "";
        private String _product = "oxando Mobile Connector";
        private String _client = "Mandant";
        private String _clientValue = AppConfig.Mandt;
        private String _sapUser = "Benutzer";
        private String _sapUserValue = AppConfig.UserId;
        private String _sapUsername;
        private String _lastSync;
        private String _newData;
        private String _gateway = AppConfig.Gateway + ":" + AppConfig.GatewayPort;
        

        
        private String _sapUserDetails = "";
        
        private String _sm02Message = "";
         */
        private String _lastSync;
        private int _progressBar;
        private Color _lastSyncColor;
        private Color _newDataColor;
        private DateTime _lastSyncTime;
        private Boolean _sapAvailable = false;
        private String _sapUserDetails = "";
        private String _rfidPersIdent = "";
        public EventHandler StatusItemChange;
        public EventHandler EventMessageTriggered;
        public EventHandler PersIdentChanged;
        private NameValueCollection _parameter = new NameValueCollection();

        #region Constructor
        private OxStatus()
        {
            try
            {
                _timerStatusBarRefresh = new Timer();
                _timerStatusBarRefresh.Interval = 10000;
                _timerStatusBarRefresh.Tick += new EventHandler(Timer_StatusBarRefresh_Tick);
                _timerStatusBarRefresh.Enabled = false;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
        public static OxStatus GetInstance()
        {
            lock (Padlock)
            {
                if (_instance == null)
                {
                    _instance = new OxStatus();
                }
                return _instance;
            }
        }

        public static void CloseInstance()
        {
            _instance = null;
        }

        #endregion

        protected virtual void OnStatusItemChange(object sender, EventArgs e)
        {
            try
            {
                if (StatusItemChange != null)
                {
                    StatusItemChange(sender, e);
                }

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void Timer_StatusBarRefresh_Tick(object sender, EventArgs e)
        {
            try
            {
                _timerStatusBarRefresh.Enabled = false;
                ResetUserDetails(this._sapUserDetails);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void ShowSM02Message()
        {
            if (!String.IsNullOrEmpty(SapMessage))
            {
                String[] sm02Message = SapMessage.Substring(1).Split('_');
                DateTime temp = DateTime.Now;
                for (int i = 0; i < sm02Message.Length; i++)
                {
                    String[] messagePart = sm02Message[i].Split('#');
                    if (messagePart.Length == 4)
                    {
                        DateTimeHelper.getTime(messagePart[0], messagePart[1], out temp);
                        TriggerEventMessage(2, 'E', DateTime.Now,
                                            temp.ToString("dd.MM.yyyy HH:mm") + " " + messagePart[2] + ": " +
                                            messagePart[3], null, true, AppConfig.MessageTimeout);
                    }
                }
            }
        }
        public String SM02Message()
        {
            return _parameter["sm02msg"];
        }

        public String ValidateUser(out Boolean pwChange, out Boolean pwReset, out Boolean allowLogin)
        {
            // SapSubRcUser prüfen
            String errorMessage = "";
            String errorLocnt = "";
            pwChange = false;
            pwReset = false;
            allowLogin = true;
            switch (SapLocnt)
            {
                case "0": break;
                default: errorLocnt = " Bisher " + SapLocnt + " Fehlanmeldungen."; break;
            }

            // SapLockStatus prüfen
            switch (SapLockStatus)
            {
                //case "-2": errorMessage = "Password cannot (generally) be changed."; break;
                //case "-1": errorMessage = "Password cannot be changed today (only allowed once a day)."; break;
                case "32": errorMessage = "Global im SAP System gesperrt durch Administrator."; allowLogin = false; break;
                case "64": errorMessage = "Lokal im SAP System gesperrt durch Administrator."; allowLogin = false; break;
                case "128": errorMessage = "Benutzer im SAP System gesperrt wegen Falschanmeldungen (befristet)."; allowLogin = false; break;
                //case "255": errorMessage = "Password cannot be changed today (only allowed once a day)."; break;
            }
            if (!String.IsNullOrEmpty(errorMessage))
            {
                TriggerEventMessage(2, 'E', DateTime.Now, errorMessage + errorLocnt, null, true);
                return errorMessage;
            }

            // SapSubRcPw prüfen
            switch (SapSubRcPw)
            {
                case "1": errorMessage = "Kennwort oder Benutzername stimmt nicht mit den SAP Daten überein."; allowLogin = false; break;
                case "2": errorMessage = "Benutzer gesperrt (durch Admin oder Fehlanmeldungen)."; allowLogin = false; break;
                case "3": errorMessage = "Benutzer außerhalb des Gültigkeitsdatums."; allowLogin = false; break;
                case "4": errorMessage = "Benutzer kann sich nur mittels SNC anmelden."; allowLogin = false; break;
                case "5": errorMessage = "Anzahl fehlerhafter Kennwortanmeldungen überschritten."; allowLogin = false; break;
                case "6": errorMessage = "Benutzer darf sich nicht per Kennwort anmelden (login/disable_password_logon)."; allowLogin = false; break;
                case "7": errorMessage = "Benutzer hat kein Kennwort."; allowLogin = false; break;
                case "8": errorMessage = "Initialkennwort ist abgelaufen (login/password_max_idle_initial)."; allowLogin = false; break;
                case "9": errorMessage = "Produktivkennwort ist abgelaufen (login/password_max_idle_productive)."; allowLogin = false; break;
                case "10": errorMessage = "Authentisierungsdaten wurden ungesichert übertragen (ohne SNC)."; allowLogin = false; break;
                case "11": errorMessage = "Ticketanmeldung ist inaktiv (login/accept_sso2_ticket)."; allowLogin = false; break;
                case "12": errorMessage = "Anmeldeticket syntaktisch falsch bzw. genereller SSF-Fehler."; allowLogin = false; break;
                case "13": errorMessage = "Digitale Signatur des Tickets ungültig / nicht überprüfbar."; allowLogin = false; break;
                case "14": errorMessage = "Ticket-ausstellendes System nicht in ACL (Transaktion SSO2)."; allowLogin = false; break;
                case "15": errorMessage = "Anmeldeticket ist abgelaufen."; allowLogin = false; break;
                case "16": errorMessage = "X.509-Anmeldung inaktiv (Transaktion SNC0)."; allowLogin = false; break;
                case "17": errorMessage = "X.509-Zertifikat nicht im Base64-Format bzw. ASN.1-Fehler."; allowLogin = false; break;
                case "18": errorMessage = "BenutzerID kann Aliasname nicht zugeordnet werden."; allowLogin = false; break;
                case "19": errorMessage = "ExtID-Anmeldung inaktiv (Transaktion SNC0)."; allowLogin = false; break;
                case "20": errorMessage = "Fehlender Mappingeintrag in Tabelle USREXTID (X.509/ExtID)."; allowLogin = false; break;
                case "21": errorMessage = "Mehrdeutige Zuordnung in Tabelle USREXTID (X.509/ExtID)."; allowLogin = false; break;
                case "22": errorMessage = "Sonstiger Fehler (ggf. neue Fehlersituationen)."; allowLogin = false; break;
                case "23": errorMessage = "Authentisierungsmethode wird (hier) nicht unterstützt."; allowLogin = false; break;
                case "24": errorMessage = "Unvollständige Authentisierungsdaten."; allowLogin = false; break;
                case "25": errorMessage = "Allgemeine Fehler."; allowLogin = false; break;
            }
            if (!String.IsNullOrEmpty(errorMessage))
            {
                TriggerEventMessage(2, 'E', DateTime.Now, errorMessage + errorLocnt, null, true);
                return errorMessage;
            }

            // SapSubRcUser prüfen
            switch (SapSubRcUser)
            {
                case "1": errorMessage = "Keine Berechtigung für diese Anfrage (Benutzer/Passwort nicht korrekt)."; allowLogin = false; break;
                case "2": errorMessage = "BenutzerID kann Aliasname nicht zugeordnet werden"; allowLogin = false; break;
                case "3": errorMessage = "Unvollständige Aufrufparameter"; allowLogin = false; break;
                case "4": errorMessage = "Benutzer existiert im aktuellen Mandant nicht"; allowLogin = false; break;
                case "5": errorMessage = "sonstiger Fehler (z.B. beim Datenbankzugriff)"; allowLogin = false; break;
                case "6": errorMessage = "Allgemeine Fehler."; allowLogin = false; break;
            }
            if (!String.IsNullOrEmpty(errorMessage))
            {
                TriggerEventMessage(2, 'E', DateTime.Now, errorMessage + errorLocnt, null, true);
                return errorMessage;
            }

            // SapPwStatus1 prüfen
            switch (SapPwStatus1)
            {
                //case "-2": errorMessage = "Password cannot (generally) be changed."; break;
                //case "-1": errorMessage = "Password cannot be changed today (only allowed once a day)."; break;
                case "1": errorMessage = "Passwort ist initial und muß geändert werden."; allowLogin = false; pwChange = true; break;
                case "2": errorMessage = "Passwort ist abgelaufen und muß geändert werden."; allowLogin = false; pwChange = true; break;
                case "3": errorMessage = "Passwort muss aufgrund neuer Richtlinien geändert werden."; allowLogin = false; pwChange = true; break;
                //case "254": errorMessage = "Password cannot (generally) be changed."; break;
                //case "255": errorMessage = "Password cannot be changed today (only allowed once a day)."; break;
            }
            if (!String.IsNullOrEmpty(errorMessage))
            {
                TriggerEventMessage(2, 'E', DateTime.Now, errorMessage + errorLocnt, null, true);
                
                return errorMessage;
            }

            // SapPwStatus2 prüfen
            switch (SapPwStatus2)
            {
                //case "-2": errorMessage = "Password cannot (generally) be changed."; break;
                //case "-1": errorMessage = "Password cannot be changed today (only allowed once a day)."; break;
                case "1": errorMessage = "Passwort ist initial und muß geändert werden."; allowLogin = false; pwChange = true; break;
                case "2": errorMessage = "Passwort ist abgelaufen und muß geändert werden."; allowLogin = false; pwChange = true; break;
                case "3": errorMessage = "Passwort muss aufgrund neuer Richtlinien geändert werden."; allowLogin = false; pwChange = true; break;
                //case "254": errorMessage = "Password cannot (generally) be changed."; break;
                //case "255": errorMessage = "Password cannot be changed today (only allowed once a day)."; break;
            }
            if (!String.IsNullOrEmpty(errorMessage))
            {
                TriggerEventMessage(2, 'E', DateTime.Now, errorMessage + errorLocnt, null, true);
                return errorMessage;
            }

            return errorMessage;
        }
        #region Getter Setter

        public Boolean SapAvailable
        {
            get { return _sapAvailable; }
            set
            {
                _sapAvailable = value;
                if (!_sapAvailable)
                {
                    String lastSyncDB = SyncLogManager.GetLastSync();
                    if (lastSyncDB.Length > 11)
                    {
                        DateTime newSyncTime = DateTime.Now;
                        DateTimeHelper.getTime(lastSyncDB, out newSyncTime);
                        LastSyncTime = newSyncTime;
                        OnStatusItemChange(this, new EventArgs());
                    }
                }
            }
        }

        public String SapUserDetails
        {
            get { return _sapUserDetails; }
            set
            {
                _sapUserDetails = value;
                ResetUserDetails(value);
            }
        }

        private void ResetUserDetails(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                _sapUserDetails = "";
                return;
            }
            String _newDataBefore = NewData;
            NameValueCollection parameterNew = System.ParseQueryString(value);
            foreach (String key in parameterNew.Keys)
            {
                _parameter.Set(key, parameterNew[key]);
            }

            if (!String.IsNullOrEmpty(_parameter["last_sync"]))
            {
                DateTime _lastSyncTime = DateTime.Now;
                DateTimeHelper.getTime(_parameter["last_sync"], out _lastSyncTime);
                LastSyncTime = _lastSyncTime;
                LastSyncColor = Color.FromArgb(0, 0, 0);
            }

            if (!String.IsNullOrEmpty(SapMessage))
            {
                ShowSM02Message();
            }

            if (_newDataBefore != NewData)
            {
                NewDataColor = Color.FromArgb(255, 0, 0);
            }
            else
            {
                NewDataColor = Color.FromArgb(0, 0, 0);
            }

            _sapUserDetails = value;

            Boolean pwChange, pwReset, allowLogin;
            ValidateUser(out pwChange, out pwReset, out allowLogin);

            OnStatusItemChange(this, new EventArgs());
        }

        public DateTime LastSyncTime
        {
            get { return _lastSyncTime; }
            set
            {
                _lastSyncTime = value;
                if (_lastSyncTime <= DateTimeHelper.MinDateTimeValue)
                    _lastSync = "Nicht Synchronisiert";
                else
                    _lastSync = "Letzte Synchronisation: " + _lastSyncTime.ToString("dd.MM.yyyy HH:mm:ss");
                OnStatusItemChange(this, new EventArgs());
            }
        }

        public void SetStatusMessage(String message, int timeout)
        {
            message = (message.Length > 50) ? message.Substring(0, 50) : message;
            message = message.Replace('\0', ' ');

            // Filter Personal security code
            if (message.IndexOf("91") < message.IndexOf("+") && message.IndexOf("+") < message.LastIndexOf("+"))
            {
                message = message.Substring(0, message.IndexOf("91")) + message.Substring(message.IndexOf("+") + 1, message.LastIndexOf("+") - message.IndexOf("+") - 1);
            }
            _lastSync = message;
            _lastSyncColor = Color.FromArgb(255, 0, 0);
            if (timeout != -1)
            {
                if (_timerStatusBarRefresh.Enabled)
                    _timerStatusBarRefresh.Enabled = false;
                _timerStatusBarRefresh.Enabled = true;
                _timerStatusBarRefresh.Interval = timeout;
            }
            OnStatusItemChange(this, new EventArgs());
        }

        public void TriggerEventMessage(int caller, char type, DateTime time, string message, Exception ex, bool display)
        {
            var displayMessage = false;
            switch (int.Parse(AppConfig.MessageDisplayLevel))
            {
                case 0:
                    break;
                case 1:
                    if (type.Equals('E') && ex != null)
                        displayMessage = true;
                    break;
                case 2:
                    if (ex == null)
                        displayMessage = true;
                    break;
                case 3:
                    displayMessage = true;
                    break;

            }
            if (displayMessage)
            {
                var dict = new Dictionary<string, string>();
                dict.Add("Caller", caller.ToString());
                dict.Add("Type", type + "");
                dict.Add("Message", message);
                dict.Add("Time", time.ToString());
                if (ex != null)
                {
                    dict.Add("Exception", ex.Message);
                    dict.Add("Stack Trace", ex.StackTrace);
                    //dict.Add("Source", ex.Source);
                }
                MessageList.Insert(0, dict);
                if (display)
                    EventMessageTriggered(this, new EventArgs());
            }
        }

        public void TriggerEventMessage(int caller, char type, DateTime time, string message, Exception ex, bool display, int timeout)
        {
            var displayMessage = false;
            switch (int.Parse(AppConfig.MessageDisplayLevel))
            {
                case 0:
                    break;
                case 1:
                    if (type.Equals('E') && ex != null)
                        displayMessage = true;
                    break;
                case 2:
                    if (ex == null)
                        displayMessage = true;
                    break;
                case 3:
                    displayMessage = true;
                    break;
            }
            if (displayMessage)
            {
                var dict = new Dictionary<string, string>();
                dict.Add("Caller", caller.ToString());
                dict.Add("Type", "" + type);
                dict.Add("Message", message);
                dict.Add("Time", time.ToString());
                dict.Add("Timeout", timeout.ToString());
                if (ex != null)
                {
                    dict.Add("Exception", ex.Message);
                    dict.Add("Stack Trace", ex.StackTrace);
                    //dict.Add("Source", ex.Source);
                }

                MessageList.Insert(0, dict);
                if (display)
                    EventMessageTriggered(this, new EventArgs());
            }
        }

        public string GetParameterValue(String key)
        {
            return _parameter[key];
        }

        public string SapPing
        {
            get { return _parameter["ping"]; }
        }

        public string SapPwStatus1
        {
            get { return _parameter["pwdstate1"]; }
        }

        public string SapPwStatus2
        {
            get { return _parameter["pwdstate2"]; }
        }

        public string SapLockStatus
        {
            get { return _parameter["lock_state"]; }
        }

        public string SapSubRcUser
        {
            get { return _parameter["subrc_user"]; }
        }

        public string SapSubRcPw
        {
            get { return _parameter["subrc_pw"]; }
        }

        public string SapLocnt
        {
            get { return _parameter["locnt"]; }
            set { _parameter.Set("locnt", value); }
        }

        public string SapDeviceId
        {
            get { return _parameter["device_id"]; }
        }

        public string SapMessage
        {
            get { return _parameter["sm02msg"]; }
        }

        public string SapSysId
        {
            get { return _parameter["sysid"]; }
        }

        public string Product
        {
            get { return "oxmc"; }
        }

        public string SAPClient
        {
            get { return "Mandant"; }
        }

        public string SAPUser
        {
            get { return "Benutzer"; }
        }

        public string SAPClientValue
        {
            get { return _parameter["symdt"]; }
            set { _parameter.Set("symdt", value); }
        }

        public string SAPUserValue
        {
            get { return _parameter["userid"]; ; }
            set { _parameter.Set("userid", value); }
        }

        public string SAPUsername
        {
            get { return _parameter["username"]; ; }
        }

        public string LastSync
        {
            get
            {
                return _lastSync;
            }
        }

        public string NewData
        {
            get { return _parameter["count_obj"]; }
        }

        public string Gateway
        {
            get { return _parameter["count_obj"]; }
        }
        public string SVMVersion
        {
            get { return _parameter["ver_version"]; }
        }

        public string SVMFilename
        {
            get { return _parameter["ver_filename"]; }
        }

        public int ProgressBar
        {
            get { return _progressBar; }
            set { _progressBar = value; }
        }
        public Color LastSyncColor
        {
            get { return _lastSyncColor; }
            set { _lastSyncColor = value; }
        }

        public Color NewDataColor
        {
            get { return _newDataColor; }
            set { _newDataColor = value; }
        }

        public static List<Dictionary<string, string>> MessageList
        {
            get { return _messageList; }
        }

        public string RfidPersIdent
        {
            get { return _rfidPersIdent; }
            set
            {
                _rfidPersIdent = value;
                PersIdentChanged(this, new EventArgs());
            }
        }

        #endregion

        public static void ClearStatus()
        {
            _instance = null;
        }
    }
}
