﻿using System;

namespace oxmc.BusinessLogic.com.ox.helper
{
    internal class CustomLong
    {
        public static Boolean TryParse(String input, out long output)
        {
            var retVal = false;
            try
            {
                output = long.Parse(input);
                return true;
            }
            catch (Exception ex)
            {
                output = 0;
                return false;
            }
        }

        public Boolean TryParse(String input)
        {
            var retVal = false;
            try
            {
                var output = long.Parse(input);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}