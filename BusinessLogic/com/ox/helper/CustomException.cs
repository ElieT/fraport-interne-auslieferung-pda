﻿using System;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class NoWindowObjectException: Exception
    {
        public string ErrorMessage
        {
            get
            {
                return "Error finding WindowObject. No object found.\n"+base.Message;
            }
        }

        public NoWindowObjectException(string errorMessage) : base(errorMessage) { }
        public NoWindowObjectException(string errorMessage, Exception inner)
            : base(errorMessage, inner) { }

    }

    public class ErrorUpdateingDataException: Exception
    {
        public string ErrorMessage
        {
            get
            {
                return "Error finding WindowObject. No object found.\n"+base.Message;
            }
        }
        public ErrorUpdateingDataException(string errorMessage) : base(errorMessage) { }
        public ErrorUpdateingDataException(string errorMessage, Exception inner)
            : base(errorMessage, inner) { }
    }
}
