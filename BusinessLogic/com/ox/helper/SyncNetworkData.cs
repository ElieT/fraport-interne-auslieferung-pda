﻿using System;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class SyncNetworkData
    {
        public string date;
        public long ID;

        public string ip1,
                      ip2;

        public string network_desc1;

        public string network_desc2;

        public string network_name1;

        public string network_name2,
                      network_speed1,
                      network_speed2;

        public string network_type1;
        public string network_type2;

        public string sap_available;

        public string time;

        public DateTime timestamp;

        public SyncNetworkData()
        {
        }

        public SyncNetworkData(string date, string time, string ip1, string ip2, string sap_available,
                               string networkType1, string networkDesc1, string networkName1, string networkType2,
                               string networkDesc2, string networkName2, string network_speed1, string network_speed2)
        {
            this.date = date;
            this.time = time;
            this.ip1 = ip1;
            this.ip2 = ip2;
            this.sap_available = sap_available;
            this.network_speed1 = network_speed1;
            this.network_speed2 = network_speed2;
            network_type1 = networkType1;
            network_desc1 = networkDesc1;
            network_name1 = networkName1;
            network_type2 = networkType2;
            network_desc2 = networkDesc2;
            network_name2 = networkName2;
        }

        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        public long Id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string NetworkSpeed2
        {
            get { return network_speed2; }
            set { network_speed2 = value; }
        }

        public string NetworkName2
        {
            get { return network_name2; }
            set { network_name2 = value; }
        }

        public string NetworkName1
        {
            get { return network_name1; }
            set { network_name1 = value; }
        }

        public string NetworkSpeed1
        {
            get { return network_speed1; }
            set { network_speed1 = value; }
        }

        public string Sap_available
        {
            get { return sap_available; }
            set { sap_available = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Time
        {
            get { return time; }
            set { time = value; }
        }

        public string Ip1
        {
            get { return ip1; }
            set { ip1 = value; }
        }

        public string Ip2
        {
            get { return ip2; }
            set { ip2 = value; }
        }

        public string NetworkType1
        {
            get { return network_type1; }
            set { network_type1 = value; }
        }

        public string NetworkDesc1
        {
            get { return network_desc1; }
            set { network_desc1 = value; }
        }

        public string NetworkType2
        {
            get { return network_type2; }
            set { network_type2 = value; }
        }

        public string NetworkDesc2
        {
            get { return network_desc2; }
            set { network_desc2 = value; }
        }
    }
}