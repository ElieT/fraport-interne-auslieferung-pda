﻿using System;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class SyncLogItemWrapper : IComparable
    {
        private Object _object;
        private String _objectType;
        private DateTime _TimeStamp;

        public object Obj
        {
            get { return _object; }
            set { _object = value; }
        }

        public string ObjectType
        {
            get { return _objectType; }
            set { _objectType = value; }
        }

        public DateTime TimeStamp
        {
            get { return _TimeStamp; }
            set { _TimeStamp = value; }
        }

        #region IComparable Members

        int IComparable.CompareTo(object obj1)
        {
            var obj2 = (SyncLogItemWrapper) obj1;
            if (TimeStamp > obj2.TimeStamp)
                return (1);
            if (TimeStamp < obj2.TimeStamp)
                return (-1);
            else
                return (0);
        }

        #endregion
    }
}