﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class SyncNetworkManager
    {
        public static List<SyncNetworkData> GetSyncNetworkItems()
        {
            var retVal = new List<SyncNetworkData>();
            try
            {
                String sqlStmt = "SELECT * " +
                                  "FROM D_SYNC_NETWORK_DATA";
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (Dictionary<string, string> rdr in records)
                {
                    var _syncNetworkData = new SyncNetworkData();
                    DateTime test;
                    DateTimeHelper.getDate(rdr["DATE"], out test);
                    _syncNetworkData.Date = test.ToString("dd.MM.yyyy");
                    DateTimeHelper.getTime(rdr["DATE"], rdr["TIME"], out test);
                    _syncNetworkData.Time = test.ToString("HH.mm.sss");
                    _syncNetworkData.Timestamp = test;
                    _syncNetworkData.Ip1 = (String) rdr["IP1"];
                    _syncNetworkData.Ip2 = (String) rdr["IP2"];
                    _syncNetworkData.NetworkType1 = (String) rdr["NETWORK_TYPE1"];
                    _syncNetworkData.NetworkDesc1 = (String) rdr["NETWORK_DESC1"];
                    _syncNetworkData.NetworkName1 = (String) rdr["NETWORK_NAME1"];
                    _syncNetworkData.NetworkType2 = (String) rdr["NETWORK_TYPE2"];
                    _syncNetworkData.NetworkDesc2 = (String) rdr["NETWORK_DESC2"];
                    _syncNetworkData.NetworkName2 = (String) rdr["NETWORK_NAME2"];
                    _syncNetworkData.Sap_available = (String) rdr["SAP_AVAILABLE"];
                    _syncNetworkData.NetworkSpeed1 = (String) rdr["NETWORK_SPEED1"];
                    _syncNetworkData.NetworkSpeed2 = (String) rdr["NETWORK_SPEED2"];

                    retVal.Add(_syncNetworkData);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static void CreateNetworkData(SyncNetworkData _networkData)
        {
            try
            {
                int intDbKey = -1;
                int dbResult = -1;
                String maxid_db = "";

                String sqlStmt = "SELECT MAX(rowid) + 1 FROM D_SYNC_NETWORK_DATA WHERE " +
                                 " MANDT = '" + AppConfig.Mandt + "' AND USERID = '" + AppConfig.UserId +
                                 "'";
                var cmd = new SQLiteCommand(sqlStmt);
                var record = OxDbManager.GetInstance().FeedTransaction(cmd);
                var result = record[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;


                _networkData.Id = intDbKey;

                // Insert Data
                sqlStmt = "INSERT OR REPLACE INTO [D_SYNC_NETWORK_DATA] " +
                          "(MANDT, USERID, ID, DATE, TIME, IP1, IP2, " +
                          "NETWORK_TYPE1, NETWORK_DESC1, NETWORK_NAME1, NETWORK_TYPE2, NETWORK_DESC2, NETWORK_NAME2, SAP_AVAILABLE, " +
                          "NETWORK_SPEED1, NETWORK_SPEED2, UPDFLAG) " +
                          "Values (@MANDT, @USERID, @ID, @DATE, @TIME, @IP1, @IP2, " +
                          "@NETWORK_TYPE1, @NETWORK_DESC1, @NETWORK_NAME1, @NETWORK_TYPE2, @NETWORK_DESC2, @NETWORK_NAME2, @SAP_AVAILABLE, " +
                          "@NETWORK_SPEED1, @NETWORK_SPEED2, @UPDFLAG)";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", _networkData.Id.ToString());
                cmd.Parameters.AddWithValue("@DATE", _networkData.Date);
                cmd.Parameters.AddWithValue("@TIME", _networkData.Time);
                cmd.Parameters.AddWithValue("@IP1", _networkData.Ip1);
                cmd.Parameters.AddWithValue("@IP2", _networkData.Ip2);
                cmd.Parameters.AddWithValue("@NETWORK_TYPE1", _networkData.NetworkType1);
                cmd.Parameters.AddWithValue("@NETWORK_DESC1", _networkData.NetworkDesc1);
                cmd.Parameters.AddWithValue("@NETWORK_NAME1", _networkData.NetworkName1);
                cmd.Parameters.AddWithValue("@NETWORK_TYPE2", _networkData.NetworkType2);
                cmd.Parameters.AddWithValue("@NETWORK_DESC2", _networkData.NetworkDesc2);
                cmd.Parameters.AddWithValue("@NETWORK_NAME2", _networkData.NetworkName2);
                cmd.Parameters.AddWithValue("@SAP_AVAILABLE", _networkData.Sap_available);
                cmd.Parameters.AddWithValue("@NETWORK_SPEED1", _networkData.NetworkSpeed1);
                cmd.Parameters.AddWithValue("@NETWORK_SPEED2", _networkData.NetworkSpeed2);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                OxDbManager.GetInstance().FeedTransaction(cmd);
                OxDbManager.GetInstance().CommitTransaction(); ;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}