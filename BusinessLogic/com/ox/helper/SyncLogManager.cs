﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;

using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class SyncLogManager
    {
        private static readonly object padlock = new object();
        private static SyncLogManager instance;

        private SyncLogManager()
        {
        }

        public static SyncLogManager GetInstance()
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new SyncLogManager();
                }
                return instance;
            }
        }

        public static List<SyncLogData> GetSyncLogItems()
        {
            var retVal = new List<SyncLogData>();
            try
            {
                String sqlStmt = "SELECT * " +
                                 "FROM D_SYNC_LOG_DATA";

                var cmd = new SQLiteCommand(sqlStmt);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().FeedTransaction(cmd);

                sqlStmt =
                    "SELECT * FROM D_SYNC_LOG_DATA_DETAILS WHERE MANDT = @MANDT AND USERID = @USERID AND ID = @ID";
                cmd = new SQLiteCommand(sqlStmt);
                foreach (Dictionary<string, string> rdr in records)
                {
                    var _syncLogData = new SyncLogData();
                    _syncLogData.TypeSummary = new Hashtable();
                    DateTime test;
                    DateTimeHelper.getDate(rdr["SDATE"], out test);
                    _syncLogData.Sdate = test.ToString("dd.MM.yyyy");
                    DateTimeHelper.getTime(rdr["SDATE"], rdr["STIME"], out test);
                    _syncLogData.Stime = test.ToString("HH.mm.sss");
                    _syncLogData.Timestamp = test;
                    DateTimeHelper.getDate(rdr["EDATE"], out test);
                    _syncLogData.Edate = test.ToString("dd.MM.yyyy");
                    DateTimeHelper.getTime(rdr["EDATE"], rdr["ETIME"], out test);
                    _syncLogData.Etime = test.ToString("HH.mm.sss");
                    _syncLogData.Ubytes = (String) rdr["UBYTES"];
                    _syncLogData.Dbytes = (String) rdr["DBYTES"];
                    _syncLogData.Urecords = (String) rdr["URECORDS"];
                    _syncLogData.Drecords = (String) rdr["DRECORDS"];
                    _syncLogData.Failed = (String) rdr["FAILED"];
                    _syncLogData.Message1 = (String) rdr["MESSAGE1"];
                    _syncLogData.Message2 = (String) rdr["MESSAGE2"];
                    _syncLogData.Id = long.Parse((String) rdr["ID"]);

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@ID", _syncLogData.Id.ToString());

                    List<Dictionary<string, string>> itemRecords = OxDbManager.GetInstance().FeedTransaction(cmd);
                    foreach (Dictionary<string, string> itemRdr in itemRecords)
                    {
                        _syncLogData.TypeSummary.Add(itemRdr["OBJTYPE"], itemRdr["OBJCOUNT"]);
                    }
                    retVal.Add(_syncLogData);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static void CreateSyncLog(SyncLogData _syncLogData)
        {
            try
            {
                int intDbKey = -1;
                int dbResult = -1;
                String maxid_db = "";

                String sqlStmt = "SELECT MAX(rowid) + 1 FROM D_SYNC_LOG_DATA WHERE " +
                                 " MANDT = '" + AppConfig.Mandt + "' AND USERID = '" + AppConfig.UserId +
                                 "'";
                var cmd = new SQLiteCommand(sqlStmt);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().FeedTransaction(cmd);
                var result = records[0]["RetVal"];
                if (result.Length > 0)
                    intDbKey = int.Parse(result);
                else
                    intDbKey = 1;

                _syncLogData.Id = intDbKey;

                // Insert Data
                sqlStmt = "INSERT OR REPLACE INTO [D_SYNC_LOG_DATA] " +
                          "(MANDT, USERID, ID, SDATE, STIME, EDATE, ETIME, " +
                          "UBYTES, DBYTES, URECORDS, DRECORDS, FAILED, " +
                          "ERRORID, MESSAGE1, MESSAGE2, UPDFLAG) " +
                          "Values (@MANDT, @USERID, @ID, @SDATE, @STIME, @EDATE, @ETIME, " +
                          "@UBYTES, @DBYTES, @URECORDS, @DRECORDS, @FAILED, " +
                          "@ERRORID, @MESSAGE1, @MESSAGE2, @UPDFLAG)";
                cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@ID", _syncLogData.Id.ToString());
                cmd.Parameters.AddWithValue("@SDATE", _syncLogData.Sdate);
                cmd.Parameters.AddWithValue("@STIME", _syncLogData.Stime);
                cmd.Parameters.AddWithValue("@EDATE", _syncLogData.Edate);
                cmd.Parameters.AddWithValue("@ETIME", _syncLogData.Etime);
                cmd.Parameters.AddWithValue("@UBYTES", _syncLogData.Ubytes);
                cmd.Parameters.AddWithValue("@DBYTES", _syncLogData.Dbytes);
                cmd.Parameters.AddWithValue("@URECORDS", _syncLogData.Urecords);
                cmd.Parameters.AddWithValue("@DRECORDS", _syncLogData.Drecords);
                cmd.Parameters.AddWithValue("@FAILED", _syncLogData.Failed);
                cmd.Parameters.AddWithValue("@ERRORID", _syncLogData.Errorid);
                cmd.Parameters.AddWithValue("@MESSAGE1", _syncLogData.Message1);
                cmd.Parameters.AddWithValue("@MESSAGE2", _syncLogData.Message2);
                cmd.Parameters.AddWithValue("@UPDFLAG", "I");

                OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (DictionaryEntry Item in _syncLogData.TypeSummary)
                {
                    sqlStmt = "INSERT OR REPLACE INTO [D_SYNC_LOG_DATA_DETAILS] " +
                              "(MANDT, USERID, ID, OBJTYPE, OBJCOUNT, UPDFLAG) " +
                              "Values (@MANDT, @USERID, @ID, @OBJTYPE, @OBJCOUNT, @UPDFLAG)";
                    cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@ID", _syncLogData.Id.ToString());
                    cmd.Parameters.AddWithValue("@OBJTYPE", Item.Key.ToString());
                    cmd.Parameters.AddWithValue("@OBJCOUNT", Item.Value.ToString());
                    cmd.Parameters.AddWithValue("@UPDFLAG", "I");
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static String GetLastSync()
        {
            String last_sync_sap = "";
            String last_sync_client = "";
            try
            {
                String sqlStmt = "SELECT * " +
                                 "FROM D_SYNC_LOG WHERE MANDT = @MANDT AND USERID = @USERID";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (Dictionary<string, string> rdr in records)
                {
                    if (!(rdr["LAST_SYNC_R3"].Equals("")))
                    {
                        last_sync_sap = (String) rdr["LAST_SYNC_R3"];
                    }
                    if (!(rdr["LAST_SYNC_CLIENT"].Equals("")))
                    {
                        last_sync_client = (String) rdr["LAST_SYNC_CLIENT"];
                    }
                }
            }

            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return last_sync_sap;
        }

        public void SetLastSync(String last_sync_sap, String last_sync_client)
        {
            try
            {
                int dbResult = -1;
                String maxid_db = "";
                if (last_sync_sap.Length > 14)
                    last_sync_sap = last_sync_sap.Substring(0, 14);

                // Insert Data

                String sqlStmt = "SELECT * " +
                                 "FROM D_SYNC_LOG WHERE MANDT = @MANDT AND USERID = @USERID";
                var cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                var records = OxDbManager.GetInstance().FeedTransaction(cmd);
                foreach (Dictionary<string, string> rdr in records)
                {
                    if (last_sync_sap.Equals("") && !(rdr["LAST_SYNC_R3"].Equals("")))
                    {
                        last_sync_sap = (String) rdr["LAST_SYNC_R3"];
                    }
                    if (last_sync_client.Equals("") && !(rdr["LAST_SYNC_CLIENT"].Equals("")))
                    {
                        last_sync_client = (String) rdr["LAST_SYNC_CLIENT"];
                    }
                }

                sqlStmt = "UPDATE [D_SYNC_LOG] SET LAST_SYNC_R3 = @LAST_SYNC_R3, LAST_SYNC_CLIENT = @LAST_SYNC_CLIENT " +
                          "WHERE MANDT = @MANDT AND USERID = @USERID";
                cmd = new SQLiteCommand(sqlStmt);

                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                cmd.Parameters.AddWithValue("@LAST_SYNC_R3", last_sync_sap);
                cmd.Parameters.AddWithValue("@LAST_SYNC_CLIENT", last_sync_client);
                OxDbManager.GetInstance().FeedTransaction(cmd);

                if (dbResult == 0)
                {
                    sqlStmt = "INSERT INTO [D_SYNC_LOG] " +
                                                      "(MANDT, USERID, LAST_SYNC_R3, LAST_SYNC_CLIENT) " +
                                                      "Values (@MANDT, @USERID, @LAST_SYNC_R3, @LAST_SYNC_CLIENT)";
                    cmd = new SQLiteCommand(sqlStmt);

                    cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
                    cmd.Parameters.AddWithValue("@LAST_SYNC_R3", last_sync_sap);
                    cmd.Parameters.AddWithValue("@LAST_SYNC_CLIENT", last_sync_client);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }
}