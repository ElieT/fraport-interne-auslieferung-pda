﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace oxmc.BusinessLogic.com.ox.helper
{
    public class System
    {
        /// <summary>
        /// Splits query string into NameValueCollection
        /// </summary>
        /// <param name="queryString">Query string</param>
        /// <returns>NameValueCollection</returns>
        public static NameValueCollection ParseQueryString(string queryString)
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            string[] parts = queryString.Split('&');

            foreach (string part in parts)
            {
                string[] nameValue = part.Split('=');
                if (nameValue.Length == 1)
                    nameValueCollection.Add("error" + nameValueCollection.Count, nameValue[0]);
                else
                    nameValueCollection.Add(nameValue[0], URLDecode(nameValue[1]));
            }

            return nameValueCollection;
        }

        /// <summary>
        /// Decodes the URL query string into string
        /// </summary>
        /// <param name="encodedString">Encoded QueryString</param>
        /// <returns>Plain string</returns>
        public static string URLDecode(string encodedString)
        {
            string outStr = string.Empty;

            int i = 0;
            while (i < encodedString.Length)
            {
                switch (encodedString[i])
                {
                    case '+': outStr += " "; break;
                    case '%':
                        string tempStr = encodedString.Substring(i + 1, 2);
                        outStr += Convert.ToChar(int.Parse(tempStr, NumberStyles.AllowHexSpecifier));
                        i = i + 2;
                        break;
                    default:
                        outStr += encodedString[i];
                        break;
                }
                i++;
            }
            return outStr;
        }
    }
}
