﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.sync
{
    public class WebServer
    {
        private static readonly object padlock = new object();
        private static WebServer instance;

        private readonly TcpListener myListener;
        private readonly Thread th;
        public String iLastMessage = "";
        private int port = 5050; // Select any free port you wish
        public EventHandler webServerCallEvent;
        public EventHandler webServerNewOrder;
        //The constructor which make the TcpListener start listening on th
        //given port. It also calls a Thread on the method StartListen().
        private WebServer()
        {
            try
            {
                //start listing on the given port
                myListener = new TcpListener(port);
                myListener.Start();
                Console.WriteLine("Web Server Running... Press ^C to Stop...");
                //start the thread which calls the method 'StartListen'
                th = new Thread(StartListen);
                th.Start();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static WebServer GetInstance()
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new WebServer();
                }
                return instance;
            }
        }


        public void Stop()
        {
            try
            {
                myListener.Server.Close();
                myListener.Stop();
                th.Abort();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public string GetLocalPath(string sMyWebServerRoot, string sDirName)
        {
            String sVirtualDir = "";
            String sRealDir = "";
            try
            {
                StreamReader sr;
                String sLine = "";


                int iStartPos = 0;
                //Remove extra spaces
                sDirName.Trim();
                // Convert to lowercase
                sMyWebServerRoot = sMyWebServerRoot.ToLower();
                // Convert to lowercase
                sDirName = sDirName.ToLower();
                try
                {
                    //Open the Vdirs.dat to find out the list virtual directories
                    sr = new StreamReader("data\\VDirs.Dat");
                    while ((sLine = sr.ReadLine()) != null)
                    {
                        //Remove extra Spaces
                        sLine.Trim();
                        if (sLine.Length > 0)
                        {
                            //find the separator
                            iStartPos = sLine.IndexOf(";");
                            // Convert to lowercase
                            sLine = sLine.ToLower();
                            sVirtualDir = sLine.Substring(0, iStartPos);
                            sRealDir = sLine.Substring(iStartPos + 1);
                            if (sVirtualDir == sDirName)
                            {
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    FileManager.LogException(ex);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            if (sVirtualDir == sDirName)
                return sRealDir;
            else
                return "";
        }

        protected virtual void OnWebServerCallEvent(EventArgs e)
        {
            try
            {
                if (webServerCallEvent != null)
                {
                    webServerCallEvent(this, e);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        protected virtual void OnWebServerNewOrder(EventArgs e)
        {
            try
            {
                if (webServerNewOrder != null)
                {
                    webServerNewOrder(this, e);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public string GetMimeType(string sRequestedFile)
        {
            StreamReader sr;
            String sLine = "";
            String sMimeType = "";
            String sFileExt = "";
            String sMimeExt = "";

            try
            {
                // Convert to lowercase
                sRequestedFile = sRequestedFile.ToLower();
                int iStartPos = sRequestedFile.IndexOf(".");
                sFileExt = sRequestedFile.Substring(iStartPos);
                try
                {
                    //Open the Vdirs.dat to find out the list virtual directories
                    sr = new StreamReader("data\\Mime.Dat");
                    while ((sLine = sr.ReadLine()) != null)
                    {
                        sLine.Trim();
                        if (sLine.Length > 0)
                        {
                            //find the separator
                            iStartPos = sLine.IndexOf(";");
                            // Convert to lower case
                            sLine = sLine.ToLower();
                            sMimeExt = sLine.Substring(0, iStartPos);
                            sMimeType = sLine.Substring(iStartPos + 1);
                            if (sMimeExt == sFileExt)
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    FileManager.LogException(ex);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            if (sMimeExt == sFileExt)
                return sMimeType;
            else
                return "";
        }

        public void SendHeader(string sHttpVersion, string sMIMEHeader, int iTotBytes,
                               string sStatusCode, ref Socket mySocket)
        {
            try
            {
                String sBuffer = "";
                // if Mime type is not provided set default to text/html
                if (sMIMEHeader.Length == 0)
                {
                    sMIMEHeader = "text/html"; // Default Mime Type is text/html
                }
                sBuffer = sBuffer + sHttpVersion + sStatusCode + "\r\n";
                sBuffer = sBuffer + "Server: cx1193719-b\r\n";
                sBuffer = sBuffer + "Content-Type: " + sMIMEHeader + "\r\n";
                sBuffer = sBuffer + "Accept-Ranges: bytes\r\n";
                sBuffer = sBuffer + "Content-Length: " + iTotBytes + "\r\n\r\n";
                Byte[] bSendData = Encoding.ASCII.GetBytes(sBuffer);
                SendToBrowser(bSendData, ref mySocket);
                Console.WriteLine("Total Bytes : " + iTotBytes);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void SendToBrowser(String sData, ref Socket mySocket)
        {
            try
            {
                SendToBrowser(Encoding.ASCII.GetBytes(sData), ref mySocket);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void SendToBrowser(Byte[] bSendData, ref Socket mySocket)
        {
            try
            {
                int numBytes = 0;
                try
                {
                    if (mySocket.Connected)
                    {
                        if ((numBytes = mySocket.Send(bSendData, bSendData.Length, 0)) == -1)
                            Console.WriteLine("Socket Error cannot Send Packet");
                        else
                        {
                            Console.WriteLine("No. of bytes send {0}", numBytes);
                        }
                    }
                    else Console.WriteLine("Connection Dropped....");
                }
                catch (Exception ex)
                {
                    FileManager.LogException(ex);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public void StartListen()
        {
            try
            {
                int iStartPos = 0;
                String sRequest;
                String sDirName;
                String sRequestedFile;
                String sErrorMessage;
                String sLocalDir;
                String sMyWebServerRoot = "C:\\MyWebServerRoot\\";
                String sPhysicalFilePath = "";
                String sFormattedMessage = "";
                String sResponse = "";

                while (true)
                {
                    //Accept a new connection
                    try
                    {
                        Socket mySocket = myListener.AcceptSocket();
                        Console.WriteLine("Socket Type " + mySocket.SocketType);
                        if (mySocket.Connected)
                        {
                            Console.WriteLine("\nClient Connected!!\n==================\nClient IP {0}\n",
                                              mySocket.RemoteEndPoint);
                            //make a byte array and receive data from the client
                            var bReceive = new Byte[1024];
                            int i = mySocket.Receive(bReceive, bReceive.Length, 0);
                            //Convert Byte to String
                            string sBuffer = Encoding.ASCII.GetString(bReceive);
                            //At present we will only deal with GET type
                            if (sBuffer.Substring(0, 3) != "GET")
                            {
                                /*
                                Console.WriteLine("Only Get Method is supported..");
                                mySocket.Close();
                                return;
                                 */
                            }
                            // Look for HTTP request
                            iStartPos = sBuffer.IndexOf("HTTP", 1);
                            // Get the HTTP text and version e.g. it will return "HTTP/1.1"
                            string sHttpVersion = sBuffer.Substring(iStartPos, 8);
                            // Extract the Requested Type and Requested file/directory
                            sRequest = sBuffer.Substring(0, iStartPos - 1);
                            //Replace backslash with Forward Slash, if Any
                            sRequest.Replace("\\", "/");

                            if (false)
                            {
                                //If file name is not supplied add forward slash to indicate
                                //that it is a directory and then we will look for the
                                //default file name..
                                if ((sRequest.IndexOf(".") < 1) && (!sRequest.EndsWith("/")))
                                {
                                    sRequest = sRequest + "/";
                                }
                                //Extract the requested file name
                                iStartPos = sRequest.LastIndexOf("/") + 1;
                                sRequestedFile = sRequest.Substring(iStartPos);
                                //Extract The directory Name
                                sDirName = sRequest.Substring(sRequest.IndexOf("/"), sRequest.LastIndexOf("/") - 3);
                                /////////////////////////////////////////////////////////////////////
                                // Identify the Physical Directory
                                /////////////////////////////////////////////////////////////////////
                                if (sDirName == "/")
                                    sLocalDir = sMyWebServerRoot;
                                else
                                {
                                    //Get the Virtual Directory
                                    sLocalDir = GetLocalPath(sMyWebServerRoot, sDirName);
                                }
                                Console.WriteLine("Directory Requested : " + sLocalDir);
                                //If the physical directory does not exists then
                                // dispaly the error message
                                if (sLocalDir.Length == 0)
                                {
                                    sErrorMessage = "<H2>Error!! Requested Directory does not exists</H2><Br>";
                                    //sErrorMessage = sErrorMessage + "Please check data\\Vdirs.Dat";
                                    //Format The Message
                                    SendHeader(sHttpVersion, "", sErrorMessage.Length, " 404 Not Found", ref mySocket);
                                    //Send to the browser
                                    SendToBrowser(sErrorMessage, ref mySocket);
                                    mySocket.Close();
                                    continue;
                                }
                                /////////////////////////////////////////////////////////////////////
                                // Identify the File Name
                                /////////////////////////////////////////////////////////////////////
                                //If The file name is not supplied then look in the default file list
                                if (sRequestedFile.Length == 0)
                                {
                                    // Get the default filename
                                    sRequestedFile = GetTheDefaultFileName(sLocalDir);
                                    if (sRequestedFile == "")
                                    {
                                        sErrorMessage = "<H2>Error!! No Default File Name Specified</H2>";
                                        SendHeader(sHttpVersion, "", sErrorMessage.Length, " 404 Not Found",
                                                   ref mySocket);
                                        SendToBrowser(sErrorMessage, ref mySocket);
                                        mySocket.Close();
                                        return;
                                    }
                                }
                                /////////////////////////////////////////////////////////////////////
                                // Get TheMime Type
                                /////////////////////////////////////////////////////////////////////
                                String sMimeType = GetMimeType(sRequestedFile);
                                //Build the physical path
                                sPhysicalFilePath = sLocalDir + sRequestedFile;
                                Console.WriteLine("File Requested : " + sPhysicalFilePath);

                                if (File.Exists(sPhysicalFilePath) == false)
                                {
                                    sErrorMessage = "<H2>404 Error! File Does Not Exists...</H2>";
                                    SendHeader(sHttpVersion, "", sErrorMessage.Length, " 404 Not Found", ref
                                                                                                             mySocket);
                                    SendToBrowser(sErrorMessage, ref mySocket);
                                    Console.WriteLine(sFormattedMessage);
                                }
                                else
                                {
                                    int iTotBytes = 0;
                                    sResponse = "";
                                    var fs = new FileStream(sPhysicalFilePath, FileMode.Open, FileAccess.Read,
                                                            FileShare.Read);
                                    // Create a reader that can read bytes from the FileStream.
                                    var reader = new BinaryReader(fs);
                                    var bytes = new byte[fs.Length];
                                    int read;
                                    while ((read = reader.Read(bytes, 0, bytes.Length)) != 0)
                                    {
                                        // Read from the file and write the data to the network
                                        sResponse = sResponse + Encoding.ASCII.GetString(bytes, 0, read);
                                        iTotBytes = iTotBytes + read;
                                    }
                                    reader.Close();
                                    fs.Close();
                                    SendHeader(sHttpVersion, sMimeType, iTotBytes, " 200 OK", ref mySocket);
                                    SendToBrowser(bytes, ref mySocket);
                                    //mySocket.Send(bytes, bytes.Length,0);
                                }
                            }
                            /*SendHeader(sHttpVersion, "", 0, " 200 OK", ref mySocket);
                            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                            SendToBrowser(enc.GetBytes("OK"), ref mySocket);
                             */
                            FileManager.LogHttp("Server request: " + sRequest);
                            FileManager.LogMessage("Server request: " + sRequest);
                            sRequest = Uri.UnescapeDataString(sRequest);
                            if (sRequest.IndexOf("EVENT=PING") != -1)
                            {
                                // In case of a ping: Do Nothing
                            }
                            else if (sRequest.IndexOf("EVENT=NEWOBJECT") != -1)
                            {
                                // In case of new push objects: Refresh InfoBar
                                OnWebServerNewOrder(new EventArgs());
                            }
                            else if (sRequest.IndexOf("EVENT=PUSH|") != -1)
                            {
                                if (sRequest.IndexOf("GET EVENT=PUSH|") != -1 && sRequest.Length > 15)
                                {
                                    sRequest = sRequest.Substring(15, sRequest.Length - 15);
                                }
                                if (sRequest.IndexOf("GET /EVENT=PUSH|") != -1 && sRequest.Length > 16)
                                {
                                    sRequest = sRequest.Substring(16, sRequest.Length - 16);
                                }
                                // In case of any other messages: Display notification


                                //if (iLastMessage != "")
                                //    iLastMessage = iLastMessage + "|";
                                sRequest = sRequest.Replace('+', ' ');
                                iLastMessage = sRequest;
                                OnWebServerCallEvent(new EventArgs());
                            }

                            sResponse = sRequest;
                            FileManager.LogHttp("Client response: " + sResponse);
                            FileManager.LogMessage("Client response: " + sResponse);
                            SendHeader(sHttpVersion, "", sResponse.Length, " 200 OK", ref mySocket);
                            SendToBrowser(sResponse, ref mySocket);
                            //Console.WriteLine(sFormattedMessage);
                            mySocket.Close();
                        }
                    }
                    catch (ThreadAbortException ex)
                    {

                    }
                    catch (SocketException ex)
                    {

                    }
                    catch (Exception ex)
                    {
                        FileManager.LogException(ex);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {

            }
            catch (SocketException ex)
            {

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public string GetTheDefaultFileName(string sLocalDirectory)
        {
            StreamReader sr;
            String sLine = "";
            try
            {
                //Open the default.dat to find out the list
                // of default file
                sr = new StreamReader("data\\Default.Dat");
                while ((sLine = sr.ReadLine()) != null)
                {
                    //Look for the default file in the web server root folder
                    if (File.Exists(sLocalDirectory + sLine))
                        break;
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            if (File.Exists(sLocalDirectory + sLine))
                return sLine;
            else
                return "";
        }
    }
}