﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.sync
{
    public class UploadManager
    {
        public static String GetUploadData(String aTimeStampUp)
        {
            var userLists = OxDbManager.GetDeltaUsers(true);
            var upStream = "";
            foreach (String userId in userLists)
            {
                var upStream4User = GetUploadData4User(aTimeStampUp, userId);
                SyncManager.GetInstance().RecordsUploadCount += upStream4User.Split('|').Length;
                if (upStream4User.StartsWith("|") || upStream4User.StartsWith("%7C"))
                    upStream += "&UPSTREAM=" + Uri.EscapeDataString("USERID;" + userId) + upStream4User;
                else
                    upStream += "&UPSTREAM=" + Uri.EscapeDataString("USERID;" + userId + "|") + upStream4User;
                //upStream += "&UPSTREAM=" + "USERID;" + userId + "|" + upStream4User;
            }
            if (upStream.Length > 0)
            {

            }
            return upStream;
        }

        public static String GetUploadData4User(String aTimeStampUp, String userId)
        {
            //IList<string> tableList = OxDbManager.GetDatabaseTables();
            var tableList = OxDbManager.GetDeltaTables(true, userId);

            var uploadStream = "";
            try
            {
                foreach (String table in tableList)
                {
                    if (table.Equals("D_DOCD"))
                    {
                        uploadStream = AppendDOCDData(uploadStream, userId);
                        continue;
                    } else if (table.Equals("D_ZFRAPORT_IA_DOCD"))
                    {
                        uploadStream = AppendIADOCDData(uploadStream, userId);
                        continue;
                    }
                    else if (table.Equals("D_ZFRAPORT_IA_DOCH"))
                    {
                        uploadStream = AppendIADOCHData(uploadStream, userId);
                        continue;
                    }
                    ThDataType thDataType = OxDbManager.GetThDataType(table);
                    Boolean isRelevant = false;
                    String uploadStreamTable = "";

                    var ColumnNames = GetUploadStructure(table);

                    int idxUpdFlag = ColumnNames.ToList().FindIndex(w => w.Equals("UPDFLAG"));
                    if (idxUpdFlag != -1)
                    {
                        userId = userId.ToLower();
                        var sqlStmt = @"SELECT * FROM " + table + " WHERE (UPDFLAG = 'I' OR UPDFLAG = 'U' OR UPDFLAG = 'D') AND USERID = '" + userId + "'";
                        var cmd = new SQLiteCommand(sqlStmt);
                        var records = OxDbManager.GetInstance().FeedUploadTransaction(cmd);
                        
                        OxDbManager.GetInstance().CommitTransaction();

                        var sqlSelectDirtyBase = "SELECT ";
                        for (int i = 0; i < ColumnNames.Count; i++)
                        {
                            if (i < ColumnNames.Count - 1)
                                sqlSelectDirtyBase += ColumnNames[i] + ", ";
                            else
                                sqlSelectDirtyBase += ColumnNames[i] + " ";
                        }
                        sqlSelectDirtyBase += " FROM " + table;
                        foreach (var rdr in records)
                        {
                            var sqlSelectDirty = sqlSelectDirtyBase;
                            uploadStreamTable +=  Uri.EscapeDataString("|" + table.Substring(2));
                            if (thDataType.ColumnNames.Contains("DIRTY") && (rdr["UPDFLAG"].Equals("U") || rdr["UPDFLAG"].Equals("D")))
                            {
                                // Delta handling
                                foreach (var columName in ColumnNames)
                                {
                                    if (thDataType.KeyColumns.Contains(columName))
                                    {
                                        if (sqlSelectDirty.IndexOf(" WHERE ") < 0)
                                            sqlSelectDirty += " WHERE " + columName + " = '" + rdr[columName] + "'";
                                        else
                                            sqlSelectDirty += " AND " + columName + " = '" + rdr[columName] + "'";
                                    }
                                }
                                sqlSelectDirty += " AND DIRTY = 'X' AND USERID = '" + userId + "'";
                                cmd = new SQLiteCommand(sqlSelectDirty);
                                var recordsDirty = OxDbManager.GetInstance().FeedTransaction(cmd);
                                if (recordsDirty.Count > 0)
                                {
                                    // There is a dirty data set -> Compare and only send changed fields
                                    var recordDirty = recordsDirty[0];
                                    foreach (var columnName in ColumnNames)
                                    {
                                        if (thDataType.KeyColumns.Contains(columnName))
                                        {
                                            if (table.StartsWith("D_DOCD") || table.StartsWith("D_ZFRAPORT_IA_DOCD"))
                                                uploadStreamTable += Uri.EscapeDataString(";") + rdr[columnName];
                                            else
                                                uploadStreamTable += Uri.EscapeDataString(";" + rdr[columnName]);
                                        }
                                        else if (rdr[columnName].Length > 0 &&
                                             !rdr[columnName].Equals(recordDirty[columnName]))
                                        {

                                            if (table.StartsWith("D_DOCD") || table.StartsWith("D_ZFRAPORT_IA_DOCD"))
                                                uploadStreamTable += Uri.EscapeDataString(";") + rdr[columnName];
                                            else
                                                uploadStreamTable += Uri.EscapeDataString(";" + rdr[columnName]);
                                            if (!columnName.Equals("UPDFLAG"))
                                                isRelevant = true;
                                        }
                                        else if (columnName.Equals("MOBILEKEY"))
                                        {
                                            if (table.StartsWith("D_DOCD") || table.StartsWith("D_ZFRAPORT_IA_DOCD"))
                                                uploadStreamTable += Uri.EscapeDataString(";") + rdr[columnName];
                                            else
                                                uploadStreamTable += Uri.EscapeDataString(";" + rdr[columnName]);
                                        }
                                        else
                                            uploadStreamTable += Uri.EscapeDataString(";");
                                    }
                                }
                                else
                                {
                                    // There is no dirty data set -> Send whole data set
                                    foreach (var columName in ColumnNames)
                                    {
                                        isRelevant = true;
                                        if (rdr[columName].Length > 0)
                                        {
                                            if (table.StartsWith("D_DOCD") || table.StartsWith("D_ZFRAPORT_IA_DOCD"))
                                                uploadStreamTable += Uri.EscapeDataString(";") + rdr[columName];
                                            else
                                                uploadStreamTable += Uri.EscapeDataString(";" + rdr[columName]);
                                        }
                                        else
                                            uploadStreamTable += Uri.EscapeDataString(";");
                                    }
                                }
                            }
                            else
                            {

                                //Hook to catch uploadstrings with documents
                                if (table.Substring(2).Equals("DOCD"))
                                {
                                    var index = uploadStreamTable.LastIndexOf("DOCD");
                                    uploadStreamTable = uploadStreamTable.Substring(0, index - 1);
                                    uploadStreamTable += rdr["LINE"];
                                }
                                else if (table.Substring(2).Equals("ZFRAPORT_IA_DOCD"))
                                {
                                    var index = uploadStreamTable.LastIndexOf("ZFRAPORT_IA_DOCD");
                                    uploadStreamTable = uploadStreamTable.Substring(0, index - 1);
                                    uploadStreamTable += rdr["LINE"];
                                }
								else
                                {
                                    // This is either not an update line or table has no dirty field
                                    foreach (String columnName in ColumnNames)
                                    {
                                        isRelevant = true;
                                        if (rdr[columnName].Length > 0)
	                                    {
	                                        if (table.StartsWith("D_DOCD") || table.StartsWith("D_ZFRAPORT_IA_DOCD"))
                                                uploadStreamTable += Uri.EscapeDataString(";") + rdr[columnName];
	                                        else
                                                uploadStreamTable += Uri.EscapeDataString(";" + rdr[columnName]);
	                                    }
	                                    else
	                                        uploadStreamTable +=  Uri.EscapeDataString(";");
	                                    }
                                }
                                //Hook to catch uploadstrings with documents
                                //if (uploadStreamTable.StartsWith("|DOCD"))
                                //{
                                //    uploadStreamTable =
                                //        uploadStreamTable.Substring(uploadStreamTable.IndexOf("|") + 1);
                                //    uploadStreamTable = uploadStreamTable.Substring(uploadStreamTable.IndexOf("|"));
                                //}
                            }
                        }
                    }
                    if (isRelevant)
                        uploadStream += uploadStreamTable;
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            if (uploadStream.Length > 0 && uploadStream.Substring(0, 1).Equals("|"))
                uploadStream = uploadStream.Substring(1, uploadStream.Length - 1);
            return uploadStream;
        }

        private static IList<string> GetUploadStructure(string table)
        {
            IList<string> retList = new List<string>();
            try
            {
                String sqlStmt = @"select column_x from S_HLP_COLUMNORDER where table_x = '" + table
                             + "' and surpressUpload = '' ORDER by order_c asc";
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().FeedTransaction(new SQLiteCommand(sqlStmt));
                foreach (Dictionary<string, string> rdr in records)
                {
                    retList.Add((String)rdr["column_x"]);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retList;
        }

        public static void ResetUploadData(string aUpTimeStamp)
        {
            //IList<string> tableList = OxDbManager.GetDatabaseTables("D_");
            IList<string> userLists = OxDbManager.GetDeltaUsers(true);
            IList<string> tableList = new List<string>();
            foreach (String userId in userLists)
            {
                var tables = OxDbManager.GetDeltaTables(true, userId);
                foreach (String table in tables)
                    if (!tableList.Contains(table))
                        tableList.Add(table);
            }

            try
            {
                foreach (String table in tableList)
                {
                    var ColumnNames = GetUploadStructure(table);

                    var idxUpdFlag = ColumnNames.ToList().FindIndex(w => w.Equals("UPDFLAG"));
                    if (idxUpdFlag != -1)
                    {
                        //var sqlStmt = @"UPDATE " + table + " SET UPDFLAG = 'X' WHERE UPDFLAG = 'I' OR UPDFLAG = 'U' AND USERID = @USER AND MANDT = @MANDT";
                        var sqlStmt = @"UPDATE " + table + " SET UPDFLAG = 'X' WHERE UPDFLAG = 'I' OR UPDFLAG = 'U' AND MANDT = @MANDT";
                        var cmd = new SQLiteCommand(sqlStmt);
                        //cmd.Parameters.AddWithValue("@USER", userId);
                        cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
                        OxDbManager.GetInstance().FeedSyncTransaction(cmd);
                    }
                }

                //String sqlStmt2 = @"UPDATE S_SYNCDELTA SET upload = '' WHERE upload = 'X' AND mandt = '" + AppConfig.Mandt + "' and userid = '" + userId + "'";
                String sqlStmt2 = @"UPDATE S_SYNCDELTA SET upload = '' WHERE upload = 'X' AND mandt = '" + AppConfig.Mandt + "'";
                OxDbManager.GetInstance().FeedSyncTransaction(new SQLiteCommand(sqlStmt2));
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private static String AppendDOCDData(String uploadStream, String userId)
        {
            var sqlStmt = @"SELECT * FROM D_DOCD WHERE (UPDFLAG = 'I' OR UPDFLAG = 'U' OR UPDFLAG = 'D') AND USERID = '" + userId + "'";
            var cmd = new SQLiteCommand(sqlStmt);
            var records = OxDbManager.GetInstance().FeedUploadTransaction(cmd);
            foreach (var record in records)
            {
                uploadStream += record["LINE"];
            }
            return uploadStream;
        }

        private static String AppendIADOCDData(String uploadStream, String userId)
        {
            var sqlStmt = @"SELECT * FROM D_ZFRAPORT_IA_DOCD WHERE (UPDFLAG = 'I' OR UPDFLAG = 'U' OR UPDFLAG = 'D') AND USERID = '" + userId + "'";
            var cmd = new SQLiteCommand(sqlStmt);
            var records = OxDbManager.GetInstance().FeedUploadTransaction(cmd);
            foreach (var record in records)
            {
                uploadStream += record["LINE"];
            }
            return uploadStream;
        }
        private static String AppendIADOCHData(String uploadStream, String userId)
        {
            var sqlStmt = @"SELECT * FROM D_ZFRAPORT_IA_DOCH WHERE (UPDFLAG = 'I' OR UPDFLAG = 'U' OR UPDFLAG = 'D') AND USERID = '" + userId + "'";
            var cmd = new SQLiteCommand(sqlStmt);
            var records = OxDbManager.GetInstance().FeedUploadTransaction(cmd);
            foreach (var record in records)
            {
                uploadStream += record["LINE"];
            }
            return uploadStream;
        }
    }
}