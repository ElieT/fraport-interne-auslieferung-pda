﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;

using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using oxmc.BusinessLogic.com.ox.bo;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.sync
{
    //todo: better sync option
    //todo: connection pooling
    //todo: refactor http request
    // todo: sonderzeichen
    //public delegate void MyEventHandler(object sender, MyEventArgs e);


    public class SyncManager
    {
        #region Constants

        private const int _TDBResult = -1;
        private const char SEMICOLON = ';';
        private const char DIRTYCOLUMNVALUE = '0';
        #endregion

        #region Fields
        private DateTime Sync_EndTimeStamp;
        private DateTime Sync_StartTimeStamp;
        private String timeStampUp;
        private Dictionary<String, String> _syncDeltaTables = new Dictionary<String, String>();
        private List<ThDataType> TableStructure { get; set; }
        public Boolean _reqMD { get; set; }
        public Boolean _reqCust { get; set; }
        public Boolean _reqDoc { get; set; }
        public Boolean _confRQ { get; set; }
        public Boolean _massData { get; set; }
        public static String iSAPAvailable = "Nein";
        private Boolean _anonymousSync = false;
        private int _RecordsDownloadCount;
        private int _RecordsUploadCount;
        private int _streamlengthIn;
        private int _streamlengthOut;
        private Hashtable TypeSummary = new Hashtable();
        public EventHandler newSyncEvent;
        private Hashtable _syncStatus = new Hashtable();
        private String _lastTable = "";
        private ThDataType _th;
        private static SQLiteCommand cmd;
        //private NetworkManager _netWorkManager = NetworkManager.GetInstance();
        private bool _userCustomizingUpdate = false;
        private Char _resetData = ' ';
        private List<string> _customizingtableList;
        private Encoding _encodingUpload = Encoding.Default;
        private Encoding _encodingDownload = Encoding.Default;
        private static Dictionary<string, List<string>> _columnList = new Dictionary<string, List<string>>();
        private int _parsingCounter = 0;
        private Dictionary<string, SQLiteParameter> _paramList = new Dictionary<string, SQLiteParameter>();
        public bool _syncError = false;
        #endregion

        #region Constructors (1)

        private static readonly object padlock = new object();
        private static SyncManager instance;
        private String _datastream_prefix_user = null;
        private String _datastream_prefix = null;

        private SyncManager()
        {

        }

        #endregion Constructors

        #region Getters and setters


        public Boolean AnonymousSync
        {
            get { return _anonymousSync; }
            set { _anonymousSync = value; }
        }

        public int RecordsUploadCount
        {
            get { return _RecordsUploadCount; }
            set { _RecordsUploadCount = value; }
        }

        public int RecordsDownloadCount
        {
            get { return _RecordsDownloadCount; }
            set { _RecordsDownloadCount = value; }
        }

        public int StreamlengthIn
        {
            get { return _streamlengthIn; }
            set { _streamlengthIn = value; }
        }

        public int StreamlengthOut
        {
            get { return _streamlengthOut; }
            set { _streamlengthOut = value; }
        }

        public DateTime SyncStartTimeStamp
        {
            get { return Sync_StartTimeStamp; }
        }

        public DateTime SyncEndTimeStamp
        {
            get { return Sync_EndTimeStamp; }
        }

        public Hashtable getTypeSummary()
        {
            return TypeSummary;
        }
        #endregion

        public static SyncManager GetInstance()
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new SyncManager();
                }
                return instance;
            }
        }

        public static void ClearInstance()
        {
            instance = null;
        }

        //public string DefaultGatewayConfig
        //{
        //    get { return DEFAULT_GATEWAY_CONFIG; }
        //}

        public String ChangePw(String userId, String oldPw, String newPw1, String newPw2, String pingString, Boolean anonymousSync)
        {
            String retVal = "";
            try
            {
                _anonymousSync = anonymousSync;
                retVal = Synchronize("&pa_ping=" + pingString + "&pa_user=" + userId + "&pa_pwold=" + oldPw + "&pa_pwnew1=" + newPw1 + "&pa_pwnew2=" + newPw2 + "&pa_ping=" + pingString, false, false,
                                      "change_pw.htm");
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                _anonymousSync = false;
            }
            return retVal;
        }

        public static String ConstructQueryString(NameValueCollection parameters)
        {
            List<String> items = new List<String>();

            foreach (String name in parameters.Keys)
                items.Add(String.Concat(name, "=", Uri.EscapeDataString(parameters[name])));

            return String.Join("&", items.ToArray());
        }

        public String GetUserName(NameValueCollection parameter, String service, Boolean anonymousSync)
        {
            String retVal = "";
            try
            {
                _anonymousSync = anonymousSync;

                var version = System.Reflection.Assembly.LoadFrom(AppConfig.SVMClientExe).GetName().Version;
                /*
                if (String.IsNullOrEmpty(ip1))
                    ip1 = _netWorkManager.INetworkAdress1;
                if (String.IsNullOrEmpty(ip2))
                    ip2 = _netWorkManager.INetworkAdress2;
                 */
                if (String.IsNullOrEmpty(parameter["ip1"]))
                    parameter.Set("ip1", "PDA");
                if (String.IsNullOrEmpty(parameter["ip2"]))
                    parameter.Set("ip2", "PDA");
                parameter.Set("pa_device_id", AppConfig.DeviceId);
                parameter.Set("pa_appl", AppConfig.SVMApplicationId);
                parameter.Set("pa_client_os", AppConfig.SVMClientOs);
                parameter.Set("pa_version", version.ToString().Split('.')[0].PadLeft(3, '0') + "." + version.ToString().Split('.')[1].PadLeft(3, '0') + "." + version.ToString().Split('.')[2].PadLeft(3, '0'));
                parameter.Set("pa_build", version.ToString().Split('.')[3]);
                retVal = Synchronize("&" + ConstructQueryString(parameter), false, false, service);

                /*
                retVal = Synchronize(
                       "&pa_ip1=" + ip1 + 
                       "&pa_ip2=" + ip2 +
                       "&pa_ping=" + pingString +
                       "&userid=" + userid +
                       "&pw=" + pw +
                       "&pa_device_id=" + AppConfig.DeviceId +
                       "&pa_appl=" + AppConfig.SVMApplicationId +
                       "&pa_client_os=" + AppConfig.SVMClientOs +
                       "&pa_version=" + Application.ProductVersion.Split('.')[0].PadLeft(3, '0') + "." + Application.ProductVersion.Split('.')[1].PadLeft(3, '0') + "." + Application.ProductVersion.Split('.')[2].PadLeft(3, '0') +
                       "&pa_device_version=" + Application.ProductVersion.Split('.')[3]
                        , false, false,
                       "user_details.htm");
                 * */
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                _anonymousSync = false;
            }
            return retVal;
        }

        /// <summary>
        /// Initializes a Sync with the SAP-Backend
        /// Call to OxStatus is initialized to inform the User of the Sync-progress.
        /// Options like Testmode or what data should be requested have to be set prior to calling this method
        /// </summary>
        public void SynchronizeWithBackend()
        {
            try
            {
                //Reset the customizingtable-list
                _syncError = false;
                _customizingtableList = DBManager.GetCustomizingtables();
                var oxStatus = OxStatus.GetInstance();
                _syncDeltaTables = new Dictionary<string, string>();
                // Performance Frage: Wird vor jedem Sync geprüft, ob die Logon-Daten des Benutzers in Ordnung sind oder nur wenn diese vorher nicht in Ordnung waren?
                //if (!_oxStatus.SapPwStatus1.Equals("0") || !_oxStatus.SapPwStatus2.Equals("0") || !_oxStatus.SapLockStatus.Equals("0") || !_oxStatus.SapSubRcPw.Equals("0") || !_oxStatus.SapSubRcUser.Equals("0"))
                NameValueCollection parameter = new NameValueCollection();
                parameter.Add("PDA", "PDA");
                parameter.Add("PDA", "PDA");
                parameter.Add("pa_ping", "");
                parameter.Add("userid", AppConfig.UserId);
                parameter.Add("pw", AppConfig.Pw);
                oxStatus.SapUserDetails = GetUserName(parameter, "user_details.htm", true);
                
                //MessageBox.Show("_syncError " + _syncError);
                //MessageBox.Show("oxStatus.SapPwStatus1 " + oxStatus.SapPwStatus1);
                //MessageBox.Show("oxStatus.SapPwStatus2 " + oxStatus.SapPwStatus2);
                //MessageBox.Show("oxStatus.SapLockStatus " + oxStatus.SapLockStatus);
                //MessageBox.Show("oxStatus.SapSubRcPw " + oxStatus.SapSubRcPw);
                //MessageBox.Show("oxStatus.SapSubRcUser " + oxStatus.SapSubRcUser);

                //if (oxStatus.SapPwStatus1 == null)
                //    MessageBox.Show("Wert Null");
                // *** BEGIN CHANGE 2013-07-04 ***
                //if (oxStatus.SapPwStatus1 != null && oxStatus.SapPwStatus2 != null && oxStatus.SapLockStatus != null && oxStatus.SapSubRcPw != null)
                //{
                    if (_syncError ||
                        !(oxStatus.SapPwStatus1.Equals("0") || oxStatus.SapPwStatus1.Equals("1-") ||
                          oxStatus.SapPwStatus1.Equals("2-")) ||
                        !(oxStatus.SapPwStatus2.Equals("0") || oxStatus.SapPwStatus2.Equals("1-") ||
                          oxStatus.SapPwStatus1.Equals("2-")) || !oxStatus.SapLockStatus.Equals("0") ||
                        !oxStatus.SapSubRcPw.Equals("0") || !oxStatus.SapSubRcUser.Equals("0"))
                    {
                        return;
                    }
                //}


                _userCustomizingUpdate = false;
                Sync_StartTimeStamp = DateTime.Now;
                _RecordsDownloadCount = 0;
                _RecordsUploadCount = 0;
                TypeSummary = new Hashtable();
                if (TableStructure == null)
                {
                    TableStructure = new List<ThDataType>();
                }
                //calls onNewSyncEvent in order to set a statusmessage for OxStatus
                _syncStatus.Clear();
                _syncStatus.Add("percentage", ((double)0.12));
                _syncStatus.Add("message", "Getting upload data");
                OnNewSyncEvent(_syncStatus, new EventArgs());

                //starts the sync using the string returned by GetPostingStream
                Synchronize(GetPostingStream(), false, true, "sync.htm");

                FileManager.LogMessage("Sync finished [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                       (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]");
                //after the Sync is complete, reset the statusmessage
                _syncStatus.Clear();
                _syncStatus.Add("percentage", (double)1);
                OnNewSyncEvent(_syncStatus, new EventArgs());

                Sync_EndTimeStamp = DateTime.Now;
                if (_userCustomizingUpdate)
                {
                    CustomizingManager.GetCust_AM_012();
                    CustomizingManager.GetCust_AM_001();
                    CustomizingManager.GetCust_AM_011();
                    CustomizingManager.GetCust_MC_010();
                }

                oxStatus.SapUserDetails = GetUserName(parameter, "user_details.htm", true);

                // Save table names where changes has been occurred.
                foreach (KeyValuePair<string, string> tablename in _syncDeltaTables)
                {
                    var sqlStmt = "UPDATE [S_SYNCDELTA] SET DOWNLOAD = 'X' WHERE tablename = '" + tablename.Key +
                                     "' AND mandt = '" + AppConfig.Mandt + "' AND userid = '" + AppConfig.UserId + "'";
                    OxDbManager.GetInstance().FeedTransaction(new SQLiteCommand(sqlStmt));
                }
                OxDbManager.GetInstance().CommitTransaction();

                OxDbManager.GetInstance().ExecuteDbSpecialCommand(new SQLiteCommand("ANALYZE;"));

                _syncDeltaTables = new Dictionary<string, string>();

                CachingManager.UpdateOrderList();
                CachingManager.UpdateNotifList();
                CachingManager.UpdateTimeConfList();
                if (!_syncError)
                    OxStatus.GetInstance().TriggerEventMessage(1, 'S', DateTime.Now, "Synchronisation erfolgreich abgeschlossen", null, true, 10000);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            finally
            {
                if (_syncStatus.ContainsKey("percentage"))
                    _syncStatus["percentage"] = (double)0;
                else
                    _syncStatus.Add("percentage", (double)0);
                if (_syncError)
                {
                    if (_syncStatus.ContainsKey("message"))
                        _syncStatus["message"] = "Fehler bei der Synchronisation";
                    else
                        _syncStatus.Add("message", "Fehler bei der Synchronisation");
                    OnNewSyncEvent(_syncStatus, new EventArgs());
                    OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, "Fehler bei der Synchronisation, bitte wiederholen Sie den Vorgang", null, true);
                    FileManager.LogMessage("Fehler bei der Synchronisation, Verarbeitung abgebrochen.");
                }
                else
                {
                    OnNewSyncEvent(_syncStatus, new EventArgs());
                    OxStatus.GetInstance().SapUserDetails = OxStatus.GetInstance().SapUserDetails;
                }

                OxDbManager.GetInstance().CommitTransaction();
                OxDbManager.GetInstance().DbConnection.Close();
            }
        }

        /// <summary>
        /// Sets the provided resetData-Flag and calls SynchronizeWithBackend()
        /// </summary>
        /// <param name="resetData">if true a datareset is triggered</param>
        public void SynchronizeWithBackend(Boolean resetData)
        {
            try
            {
                if (resetData)
                    _resetData = 'X';

                OxStatus.GetInstance().TriggerEventMessage(1, 'I', DateTime.Now, "Letzte Synchronisation: ......... läuft .........", null, true, 5000);
                OxStatus.GetInstance().SetStatusMessage("Letzte Synchronisation: ......... läuft .........", -1);

                if (AppConfig.FlagDocuments.Equals("X"))
                    _reqDoc = true;
                else
                    _reqDoc = false;
                if (AppConfig.FlagCustomizing.Equals("X"))
                    _reqCust = true;
                else
                    _reqCust = false;
                if (AppConfig.FlagMasterdata.Equals("X"))
                    _reqMD = true;
                else
                    _reqMD = false;
                if (AppConfig.FlagConfirmation.Equals("X"))
                    _confRQ = true;
                else
                    _confRQ = false;
                if (AppConfig.FlagMassdata.Equals("X"))
                    _massData = true;
                else
                    _massData = false;
                SynchronizeWithBackend();
                _resetData = ' ';
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void ConfirmStream(string aTimestamp)
        {
            try
            {
                //iStatusLabel.Text = "Sending Confirmation";
                if (_confRQ)
                    Synchronize("&CONFIRMSYNCGUID=" + aTimestamp + "&DEVICE_ID=" + AppConfig.DeviceId, true, true, "sync.htm");
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        /// <summary>
        /// Calls the Backend using the provided posting stream
        /// If aDataSync is set the serverresponse is then passed on to ParseInboundStream
        /// </summary>
        /// <param name="aPostStream">The request stream</param>
        /// <param name="aConfirmMode"></param>
        /// <param name="aDataSync">If true the serverresponse is passed on to ParseInboundStream</param>
        /// <param name="aSyncService"></param>
        /// <returns></returns>
        private String Synchronize(string aPostStream, Boolean aConfirmMode, Boolean aDataSync, String aSyncRessource)
        {
            string strResponse = "";
            string retVal = "";
            try
            {
                if (!String.IsNullOrEmpty(OxStatus.GetInstance().SapLocnt) && !OxStatus.GetInstance().SapLocnt.Equals("0"))
                    return "Kennwort oder Benutzer falsch.";
                //todo: refactor!!
                if (aSyncRessource.Equals("sync.htm") && !aConfirmMode && aDataSync)
                    _streamlengthOut = aPostStream.Length;

                string strNewValue;
                string strFormValues;
                int dbResult;
                System.Net.ServicePointManager.Expect100Continue = false;
                HttpWebRequest httpReq;
                //iStatusLabel.Text = "Create HTTP Request";
                httpReq = BuildUrl(aSyncRessource);

                if (aDataSync && !aConfirmMode)
                    httpReq.Timeout = AppConfig.TimeoutDataSync;
                else
                    httpReq.Timeout = AppConfig.TimeoutUserSync;

                // Set Values for the request back
                httpReq.Method = "POST";
                httpReq.ContentType = "application/x-www-form-urlencoded";
                strNewValue = "OnInputProcessing=post" + aPostStream;
                //String EncodedString = Microsoft.SqlServer.Server.HtmlEncode(TestString);
                httpReq.AllowWriteStreamBuffering = true;

                httpReq.ContentLength = strNewValue.Length;

                // Write the request

                FileManager.LogHttp("Client Request [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + httpReq.Address + "&" +
                                    strNewValue + "\r\n");

                FileManager.LogMessage("Client Request [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                       (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + httpReq.Address +
                                       "&" + strNewValue + "\r\n");

                if (!AppConfig.FlagTestmode.Equals("X"))
                {
                    FileManager.LogMessage("Testmode = false\r\n");

                    try
                    {
                        var stOut = new StreamWriter(httpReq.GetRequestStream(), Encoding.ASCII);
                        stOut.Write(strNewValue);
                        stOut.Close();

                        // Do the request to get the response
                        var stIn = new StreamReader(httpReq.GetResponse().GetResponseStream());
                        strResponse = stIn.ReadToEnd();
                        stIn.Close();
                    }
                    catch (Exception ex2)
                    {
                        FileManager.LogMessage("Sync Exception: " + ex2.Message + "\r\n");
                        _syncError = true;
                    }
                }
                else
                {
                    FileManager.LogMessage("Test mode enabled. Using server response from file");
                    OxStatus.GetInstance().TriggerEventMessage(2, 'W', DateTime.Now, "Testmodus aktiv", null, true, 5000);
                    if (!aConfirmMode && aDataSync)
                    {
                        IList<String> _serverSyncData = FileManager.ReadFileLines("\\IA_ce\\Sync\\ServerSyncData.txt");
                        strResponse = "";
                        foreach (String line in _serverSyncData)
                            strResponse += line;
                    }
                    else if (!aConfirmMode && !aDataSync && aSyncRessource.IndexOf("user_details") != -1)
                    {
                        IList<String> _serverUserData = FileManager.ReadFileLines("\\IA_ce\\Sync\\ServerUserData.txt");
                        strResponse = "";
                        foreach (String line in _serverUserData)
                            strResponse += line;
                    }
                }

                FileManager.LogHttp("Server Response[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + strResponse);
                FileManager.LogMessage("Server Response[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                       (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + strResponse);

                if (aSyncRessource.Equals("sync.htm") && !aConfirmMode && aDataSync)
                    _streamlengthIn = strResponse.Length;
                // Parse the server response
                if (!aDataSync)
                {
                    retVal = strResponse;
                }
                else if (!aConfirmMode && strResponse != null
                         && strResponse.Length > 13
                         && (strResponse.StartsWith("SYNCGUID") || strResponse.StartsWith("RESETCLIENT=X")))
                {
                    _syncStatus.Clear();
                    // if reset client has been triggerd on the device or was sent from the server
                    // -> delete client data before parsing new data
                    if (_resetData == 'X' || strResponse.StartsWith("RESETCLIENT=X"))
                    {
                        DBManager.ClearData();
                        DBManager.ClearColumnOrder();
                        DBManager.CreateColumnOrder();
                        CustomizingManager.GetCust_AM_001();
                        CustomizingManager.GetCust_AM_011();
                        CustomizingManager.GetCust_AM_012();
                        CustomizingManager.GetCust_MC_010();
                    }
                    _syncStatus.Add("percentage", (double)0.5);
                    _syncStatus.Add("message", "Received data, start parsing...");
                    OnNewSyncEvent(_syncStatus, new EventArgs());

                    //if (_datastream_prefix_user == null)
                    //    _datastream_prefix_user = AppConfig.Settings["Mandt"] + ";" + AppConfig.Settings["UserId"] + ";" +
                    //       DIRTYCOLUMNVALUE + ";";
                    ////if (_datastream_prefix == null)
                    ////    _datastream_prefix = AppConfig.Settings["Mandt"] + ";" + DIRTYCOLUMNVALUE + ";";

                    _datastream_prefix_user = AppConfig.Settings["Mandt"] + ";" + AppConfig.Settings["UserId"] + ";" + DIRTYCOLUMNVALUE + ";";
                    _datastream_prefix = AppConfig.Settings["Mandt"] + ";" + DIRTYCOLUMNVALUE + ";";

                    ParseInboundStream(strResponse);
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                _syncError = true;
                retVal = ex.Message;
                //OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, retVal, null, true, 5000);
                OxStatus.GetInstance().SetStatusMessage(retVal, -1);
                FileManager.LogException(ex);
            }
            return retVal;
        }

        protected virtual void OnNewSyncEvent(object sender, EventArgs e)
        {
            if (newSyncEvent != null)
            {
                newSyncEvent(sender, e);
            }
        }

        /// <summary>
        /// Parses the provided Datastream and kicks of the db-transaction
        /// All datasets in the stream are passed on to ParseData. Afterwards the transaction is commited
        /// </summary>
        /// <param name="aInboundStream">The stream returned by the backend containing new data</param>
        private void ParseInboundStream(string aInboundStream)
        {
            var rt = false;
            var timestamp = "";
            try
            {
                string tTimestamp = "";
                String tData = "";
                IList<string> arrInboundStream = aInboundStream.Split('|').ToList();
                FileManager.LogMessage("Starting parsing[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                       (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " +
                                       arrInboundStream.Count() + " items.");

                cmd = new SQLiteCommand();
                _lastTable = "";

                var posOfRt = aInboundStream.IndexOf("RETURNTIMESTAMP;" + timeStampUp);

                if (posOfRt >= 0)
                {
                    FileManager.LogMessage("Confirmed Upload Data: " + timeStampUp);
                    UploadManager.ResetUploadData(timestamp);
                }

                for (int i = 0; i < arrInboundStream.Count(); i++)
                {
                    var denom = Math.Ceiling(arrInboundStream.Count() / 25.0);
                    if (i % 50000 == 0)
                    {
                        FileManager.LogMessage("Parsed [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes +
                                               "/" +
                                               (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + i +
                                               " items.");
                    }
                    if (i % denom == 0)
                    {
                        //System.Console.WriteLine("Trying to throw event " + i);

                        //int size = arrInboundStream.Count();
                        //float progress = i / size;
                        _syncStatus[0] = (0.5 + (i / (float)arrInboundStream.Count()) * 0.5);
                        //onNewSyncEvent(new MyEventArgs(0.5 + (i/(float) arrInboundStream.Count())*0.5));
                        _syncStatus.Clear();
                        _syncStatus.Add("percentage", (double)(0.5 + (i / (float)arrInboundStream.Count()) * 0.5));
                        _syncStatus.Add("message", "Received data, start parsing...");
                        OnNewSyncEvent(_syncStatus, new EventArgs());
                    }
                    var tPartName = SplitDataPart(arrInboundStream[i])[0];
                    var tPartValue = SplitDataPart(arrInboundStream[i])[1];
                    if (_syncError)
                        return;

                    if (tPartName.Length > 0 && tPartValue.Length > 0)
                    {
                        switch (tPartName)
                        {
                            case "SYNCGUID":
                                tTimestamp = tPartValue;
                                break;
                            case "RETURNTIMESTAMP":
                                if (tPartValue == timeStampUp)
                                {
                                    rt = true;
                                    timestamp = timeStampUp;
                                }
                                break;
                            case "RETURNMESSAGE":
                                FileManager.LogMessage(tPartValue);
                                OxStatus.GetInstance().TriggerEventMessage(2, 'W', DateTime.Now, tPartValue.Replace("\n", " ").Replace("\r", ""), null, true, 5000);
                                break;
                            default:
                                //if the current tablename does not equal the last table of the plast parsed dataset, reset the parameters
                                if (!_lastTable.Equals(tPartName))
                                {
                                    GetParameters(tPartName);
                                    _lastTable = tPartName;
                                }

                                ParseData(tPartName, tPartValue);
                                break;
                        }
                        if (TypeSummary.Contains(tPartName))
                            TypeSummary[tPartName] = (int)TypeSummary[tPartName] + 1;
                        else
                            TypeSummary.Add(tPartName, 1);
                    }
                }

                if (rt)
                {
                    UploadManager.ResetUploadData(timestamp);
                }
                _RecordsDownloadCount = arrInboundStream.Count() - 1;
                FileManager.LogMessage("Parsing completed [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                       (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " +
                                       arrInboundStream.Count() + " items.");
                OxStatus.GetInstance().TriggerEventMessage(2, 'S', DateTime.Now, "Syncverarbeitung abgeschlossen: " + arrInboundStream.Count() + " Objekte synchronisiert", null, true, 5000);
                ConfirmStream(tTimestamp);
            }
            catch (Exception ex)
            {
                _syncError = true;
                FileManager.LogException(ex);
            }
        }

        /// <summary>
        /// Adds the parameters to the SqlTransCommand
        /// </summary>
        /// <param name="table"></param>
        private void GetParameters(String table)
        {
            try
            {
                if (OxDbManager.TableList == null)
                    OxDbManager.LoadTableList();
                if (_syncError)
                    return;
                table = (String)OxDbManager.TableList[table];
                if (table != null && !table.Equals(_lastTable))
                {

                    _th = TableStructure.Where(x => x.TableName == table).FirstOrDefault();
                    if (_th == null)
                    {
                        //only create the db structure once
                        OxDbManager.CreateTableStructure(table);

                        TableStructure = OxDbManager.TableStructure;
                        _th = TableStructure.Where(x => x.TableName == table).FirstOrDefault();
                        int index = _th.ColumnNames.ToList().FindIndex(w => w.Equals("UPDFLAG"));
                        if (_th == null) //if still null --> break
                            return;
                    }
                    else
                    {
                        //reset values
                        _th.ResetValues();
                    }
                    foreach (var col in _th.Columns.Values)
                    {
                        SQLiteParameter param = cmd.CreateParameter();
                        param.ParameterName = "@" + col.Column;
                        param.DbType = DbType.String;
                        cmd.Parameters.Add(param);
                    }
                }
            }
            catch (Exception ex)
            {
                _syncError = true;
                FileManager.LogMessage("Tablename: " + table + " Lasttable: " + _lastTable);
                FileManager.LogException(ex);
            }
        }


        private void LoadColumns(string table)
        {
            try
            {
                var columns = new List<string>();
                var sqlStmt = @"SELECT column_x FROM S_HLP_COLUMNORDER WHERE table_x = '" + table
                              + "' ORDER BY order_c ASC";
                var records = OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                foreach (var rdr in records)
                {
                    columns.Add(rdr["column_x"]);
                }
                _columnList.Add(table, columns);
            }
            catch (Exception ex)
            {
                _syncError = true;
                FileManager.LogException(ex);
            }
        }

        private void ParseData(string table, String aDataStream)
        {
            try
            {
                if ((_parsingCounter / 100) >= 1)
                {
                    OxDbManager.GetInstance().CommitTransaction();
                    _parsingCounter = 0;
                }
                _parsingCounter++;

                if (table.Equals("MC_MOBILEKEY"))
                {
                    IList<string> fields = aDataStream.Split(SEMICOLON).ToList();
                    var mk = fields[1];
                    DeleteMobileKeyData(mk);
                    return;
                }
                if (table.Equals("MC_CHILDKEY"))
                {
                    IList<string> fields = aDataStream.Split(SEMICOLON).ToList();
                    var mk = fields[1];
                    DeleteChildkeyData(mk);
                    return;
                }
                if (OxDbManager.TableList == null)
                    OxDbManager.LoadTableList();

                if (_syncError)
                    return;

                table = (String)OxDbManager.TableList[table];
                if (table == null)
                    return;

                if (table.StartsWith("G_"))
                    aDataStream = _datastream_prefix + aDataStream;
                else
                    aDataStream = _datastream_prefix_user + aDataStream;

                if (_customizingtableList.Contains(table))
                    ClearCustomizingData(table);

                if (!_userCustomizingUpdate && (table.Equals("C_AM_CUST_012") || table.Equals("C_AM_CUST_001") || table.Equals("C_AM_CUST_011") || table.Equals("C_MC_CUST_010")))
                    _userCustomizingUpdate = true;

                //if the number of values in th is smaller then the number vo columns, create addidional values)
                _th.ResetValues();
                IList<string> tDataField = aDataStream.Split(SEMICOLON).ToList();
                var i = 0;
                foreach (var col in _th.Columns.Values)
                {
                    if (tDataField.Count() > i)
                        _th.Values.Add(col.Column, tDataField[i++]);
                    else
                        _th.Values.Add(col.Column, "");
                }

                if (_th.getUpdFlag().Equals(""))
                    _th.setUpdFlag("I");

                if (_th.getUpdFlag().Equals("I") || _th.getUpdFlag().Equals("U"))
                {
                    UpdateData(_TDBResult, table, _th);
                    // nur wenn noch nicht vorhanden
                    if (!_syncDeltaTables.ContainsKey(table))
                        _syncDeltaTables.Add(table, "");
                }
                else
                {
                    _th.setUpdFlag("S");
                    DeleteData(_TDBResult, table, _th);
                    if (!_syncDeltaTables.ContainsKey(table))
                        _syncDeltaTables.Add(table, "");
                }
            }
            catch (Exception ex)
            {
                _syncError = true;
                FileManager.LogException(ex);
            }
        }

        private string[] SplitDataPart(string aDataStream)
        {
            String[] tRetVal = { "", "" };
            try
            {
                if (aDataStream.Length > 3 && aDataStream.IndexOf(";") > -1 &&
                    aDataStream.IndexOf(";") < aDataStream.Length)
                {
                    tRetVal[0] = aDataStream.Substring(0, aDataStream.IndexOf(";"));
                    tRetVal[1] = aDataStream.Substring(aDataStream.IndexOf(";") + 1,
                                                       aDataStream.Length - aDataStream.IndexOf(";") - 1);
                }
            }
            catch (Exception ex)
            {
                _syncError = true;
                FileManager.LogException(ex);
            }
            return tRetVal;
        }


        private HttpWebRequest BuildUrl(String resource)
        {
            HttpWebRequest httpReq = null;
            try
            {
                string link = "";
                if (!_anonymousSync)
                    link = AppConfig.Protocol + "://" + AppConfig.Gateway + ":" + AppConfig.GatewayPort + "/sap" +
                           AppConfig.GatewayIdent
                           + AppConfig.GatewayServiceSync +
                           resource + "?sap-client=" + AppConfig.Mandt + "&sap-user=" + AppConfig.UserId +
                           "&sap-password=" + Uri.EscapeDataString(AppConfig.Pw);
                else
                    link = AppConfig.Protocol + "://" + AppConfig.Gateway + ":" + AppConfig.GatewayPort + "/sap" + AppConfig.GatewayIdent
                           + AppConfig.GatewayServiceInfo + resource + "?sap-client=" + AppConfig.Mandt;
                httpReq = (HttpWebRequest)WebRequest.Create(link);
                httpReq.Proxy = GlobalProxySelection.GetEmptyWebProxy();
            }
            catch (Exception ex)
            {
                _syncError = true;
                FileManager.LogException(ex);
            }
            return httpReq;
        }

        /// <summary>
        /// Creates a Posting Stream depending on the settings of 
        /// ReqDoc,
        /// RecMD,
        /// RecCust,
        /// MassData,
        /// Furthermore Uploadmanager.GetUploadData is beeing called in order to transfer all changes to the local data to the backend
        /// </summary>
        /// <returns>String which is to be send to the backend containing new data and requesting a stream of downloaddata</returns>
        private String GetPostingStream()
        {
            String postStream = "";
            String basicPostStream = "";
            try
            {
                if (AppConfig.ResetData)
                    _resetData = 'X';
                DateTime dt = DateTime.Now;
                var dfi = new DateTimeFormatInfo();
                timeStampUp = dt.ToString("yyyyMMddHHmmsss");

                String flagDoc = "", flagMD = "", flagCust = "", flagMassData = "";
                if (_reqDoc) flagDoc = "X";
                if (_reqMD) flagMD = "X";
                if (_reqCust) flagCust = "X";
                if (_massData) flagMassData = "X";
                basicPostStream = "&UPTIMESTAMP=" +
                                  timeStampUp +
                                  "&REQDOC=" + flagDoc +
                                  "&REQMD=" + flagMD +
                                  "&REQCUST=" + flagCust +
                                  "&REQMASSDATA=" + flagMassData +
                                  "&REQMASSDATASIZE=100000" +
                                  "&RESETCLIENT=" + _resetData +
                                  "&DEVICE_ID=" + AppConfig.DeviceId;
                if (postStream.Length > 0 && postStream.Substring(postStream.Length - 1, 1).Equals("|"))
                    postStream = postStream.Substring(0, postStream.Length - 1);

                postStream = UploadManager.GetUploadData(timeStampUp);
                /*
                if (postStream.Length > 0)
                {
                    _RecordsUploadCount = postStream.Split('|').Length;
                }
                 * */
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            _syncStatus.Clear();
            _syncStatus.Add("percentage", ((double)0.25));
            _syncStatus.Add("message", "Waiting for SAP");
            OnNewSyncEvent(_syncStatus, new EventArgs());
            return basicPostStream + postStream;
        }

        private static int UpdateData(int dbResult, string aTable, ThDataType thDataType)
        {
            try
            {
                if (thDataType.getUpdFlag().Equals("D"))
                {
                    cmd.CommandText = OxDbManager.GetCoolSqlDelete(aTable, thDataType);
                }
                else if (thDataType.getUpdFlag().Equals("I") || thDataType.getUpdFlag().Equals("U"))
                {
                    cmd.CommandText = OxDbManager.GetCoolSqlInsert(aTable, thDataType);

                    try
                    {
                        //get non keys
                        if (aTable.Equals("C_MC_CUST_010"))
                        {
                            thDataType.Values["RFID_PORTNAME_CE"] = thDataType.Values["RFID_PORTNAME_CE"].Trim();
                            thDataType.Values["RFID_PORTNAME_W32"] = thDataType.Values["RFID_PORTNAME_W32"].Trim();
                            thDataType.Values["RFID_PORTNAME_CE"] = thDataType.Values["RFID_PORTNAME_CE"].TrimEnd(':');
                            thDataType.Values["RFID_PORTNAME_W32"] = thDataType.Values["RFID_PORTNAME_W32"].TrimEnd(':');
                            thDataType.Values["RFID_PORTNAME_CE"] += ":";
                            thDataType.Values["RFID_PORTNAME_W32"] += ":";
                        }
                        foreach (ColumnDescriptor column in thDataType.Columns.Values)
                        {
                            cmd.Parameters.AddWithValue("@" + column.Column, thDataType.Values[column.Column]);
                        }
                        cmd.Parameters["@UPDFLAG"].Value = "X";
                    }
                    catch (Exception ex)
                    {
                        FileManager.LogException(ex);
                    }
                }
                OxDbManager.GetInstance().FeedSyncTransaction(cmd);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return dbResult;
        }

        private static int DeleteData(int dbResult, string aTable, ThDataType thDataType)
        {
            try
            {
                cmd.CommandText = OxDbManager.GetCoolSqlDelete(aTable, thDataType);
                try
                {
                    //get keys
                    foreach (ColumnDescriptor column in thDataType.Columns.Values)
                    {
                        if (column.IsKey)
                            cmd.Parameters.AddWithValue("@" + column.Column, thDataType.Values[column.Column]);
                    }

                    if (cmd.Parameters.IndexOf("@UPDFLAG") == -1 || cmd.Parameters["@UPDFLAG"].Value == null)
                    {
                        cmd.Parameters.AddWithValue("@UPDFLAG", "S");
                    }
                    OxDbManager.GetInstance().FeedSyncTransaction(cmd);
                }
                catch (Exception ex)
                {
                    FileManager.LogException(ex);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return dbResult;
        }

        private static void DeleteMobileKeyData(String mobilekey)
        {
            NotifManager.DeleteMobileKeyData(mobilekey);
            OrderManager.DeleteMobileKeyData(mobilekey);
            DeliveryManager.DeleteMobileKeyData(mobilekey);
        }

        private static void DeleteChildkeyData(String mobilekey)
        {
            //TimeConfManager.DeleteChildkeyData(mobilekey);
        }

        private void ClearCustomizingData(string table)
        {
            try
            {
                var sqlStmt = "DELETE FROM [" + table + "] WHERE USERID = @USERID AND MANDT = @MANDT";
                if (table.StartsWith("G_"))
                    sqlStmt = "DELETE FROM [" + table + "] WHERE MANDT = @MANDT";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);

                if (!table.StartsWith("G_"))
                    cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);

                OxDbManager.GetInstance().FeedSyncTransaction(cmd);
                _customizingtableList.Remove(table);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private void ResetCompleted()
        {
            AppConfig.ResetData = false;
            _resetData = ' ';
            var sqlStmt = "UPDATE S_DEVICE SET RESETDATA = '' WHERE USERID = @USERID AND MANDT = @MANDT";
            var cmd = new SQLiteCommand(sqlStmt);
            cmd.Parameters.AddWithValue("@USERID", AppConfig.UserId);
            cmd.Parameters.AddWithValue("@MANDT", AppConfig.Mandt);
            OxDbManager.GetInstance().FeedSyncTransaction(cmd);
        }

        public string CallServiceSelfDispo(string postStream)
        {
            //var retVal = "";
            //try
            //{
            //    var httpReq = BuildUrl("online_request.htm");
            //    var strResponse = "";
            //    httpReq.Method = "POST";
            //    httpReq.ContentType = "application/x-www-form-urlencoded";
            //    var strNewValue = "OnInputProcessing=Post" + aPostStream;

            //    //String EncodedString = Microsoft.SqlServer.Server.HtmlEncode(TestString);
            //    httpReq.AllowWriteStreamBuffering = true;

            //    httpReq.ContentLength = strNewValue.Length;

            //    if (!AppConfig.FlagTestmode.Equals("X"))
            //    {
            //        FileManager.LogMessage("Testmode = false\r\n");

            //        try
            //        {
            //            var stOut = new StreamWriter(httpReq.GetRequestStream(), Encoding.ASCII);
            //            stOut.Write(strNewValue);
            //            stOut.Close();

            //            // Do the request to get the response
            //            var stIn = new StreamReader(httpReq.GetResponse().GetResponseStream());
            //            strResponse = stIn.ReadToEnd();
            //            stIn.Close();
            //        }
            //        catch (Exception ex2)
            //        {
            //            FileManager.LogMessage("Sync Exception: " + ex2.Message + "\r\n");
            //            _syncError = true;
            //        }
            //    }

            //    FileManager.LogHttp("Server Response[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
            //                        (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + strResponse);
            //    FileManager.LogMessage("Server Response[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
            //                           (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + strResponse);

            //    if (strResponse != null)
            //    {
            //        retVal = strResponse;
            //    }
            //    OxDbManager.GetInstance().CommitTransaction();
            //}
            //catch (Exception ex)
            //{
            //    _syncError = true;
            //    retVal = ex.Message;
            //    //OxStatus.GetInstance().TriggerEventMessage(1, 'E', DateTime.Now, retVal, null, true, 5000);
            //    OxStatus.GetInstance().SetStatusMessage(retVal, -1);
            //    FileManager.LogException(ex);
            //}
            //return retVal;

            String result = "";
            try
            {
                var syncServiceNode = "online_request.htm";
                var gatewayConfig = "0";

                HttpWebRequest request = BuildUrlSelfDispo(syncServiceNode);
                var path = request.RequestUri.AbsoluteUri;

                request = (HttpWebRequest)WebRequest.Create(path);

                request.Timeout = AppConfig.TimeoutDataSync;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                postStream = "OnInputProcessing=post" + postStream;
                request.AllowWriteStreamBuffering = true;

                request.ContentLength = postStream.Length;


                FileManager.LogHttp("Client Request [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + request.Address + "&" +
                                    postStream + "\r\n");

                if (!AppConfig.FlagTestmode.Equals("X"))
                {
                    var stOut = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
                    stOut.Write(postStream);
                    stOut.Close();

                    // Do the request to get the response
                    // var stIn = new StreamReader(request.GetResponse().GetResponseStream());
                    var stIn = new StreamReader(request.GetResponse().GetResponseStream());
                    result = stIn.ReadToEnd();

                    stIn.Close();
                }
                else
                {

                    FileManager.LogMessage("Test mode enabled. Using server response from file");
                    OxStatus.GetInstance().TriggerEventMessage(2, 'W', DateTime.Now, "Testmodus aktiv", null, true, 5000);
                    IList<String> _serverSyncData = FileManager.ReadFileLines("Sync\\SFMServerSyncData.txt");
                    result = "";
                    foreach (String line in _serverSyncData)
                        result += line;
                }
                FileManager.LogHttp("Server Response[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + result);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return result;
        }

        private static List<string[]> ParseResponseStream(string strResponseStream)
        {
            var retList = new List<string[]>();
            try
            {
                var objList = strResponseStream.Split('|');
                if (objList != null && objList.Count() > 0)
                {
                    List<string> obj = null;
                    foreach (var item in objList)
                    {
                        var atts = item.Split(';');
                        retList.Add(atts);
                    }

                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
                return null;
            }
            return retList;
        }

        public String ProcessOnlineRequest(String postStream)
        {
            String result = "";
            try
            {
                var syncServiceNode = "online_request.htm";
                var gatewayConfig = "0";

                HttpWebRequest request = BuildUrl(syncServiceNode);
                var path = request.RequestUri.AbsoluteUri;

                request = (HttpWebRequest) WebRequest.Create(path);

                request.Timeout = AppConfig.TimeoutDataSync;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                postStream = "OnInputProcessing=post" + postStream;
                request.AllowWriteStreamBuffering = true;

                request.ContentLength = postStream.Length;


                FileManager.LogHttp("Client Request [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + request.Address + "&" +
                                    postStream + "\r\n");

                if (!AppConfig.FlagTestmode.Equals("X"))
                {
                    var stOut = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
                    stOut.Write(postStream);
                    stOut.Close();

                    // Do the request to get the response
                    // var stIn = new StreamReader(request.GetResponse().GetResponseStream());
                    var stIn = new StreamReader(request.GetResponse().GetResponseStream());
                    result = stIn.ReadToEnd();

                    stIn.Close();
                }
                else
                {

                    FileManager.LogMessage("Test mode enabled. Using server response from file");
                    OxStatus.GetInstance().TriggerEventMessage(2, 'W', DateTime.Now, "Testmodus aktiv", null, true, 5000);
                    IList<String> _serverSyncData = FileManager.ReadFileLines("Sync\\SFMServerSyncData.txt");
                    result = "";
                    foreach (String line in _serverSyncData)
                        result += line;
                }
                FileManager.LogHttp("Server Response[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + result);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return result;
        }

        private HttpWebRequest BuildUrlSelfDispo(String resource)
        {
            HttpWebRequest httpReq = null;
            try
            {
                string link = "";
                
                link = AppConfig.Protocol + "://" + AppConfig.Gateway + ":" + AppConfig.GatewayPort + "/sap" + AppConfig.GatewayIdent
                       + AppConfig.GatewayServiceSync +
                       resource + "?sap-client=" + AppConfig.Mandt + "&sap-user=" + AppConfig.UserId + "&sap-password=" + AppConfig.Pw;

                MessageBox.Show("Link: " + link);
                
                httpReq = (HttpWebRequest)WebRequest.Create(link);
                httpReq.Proxy = GlobalProxySelection.GetEmptyWebProxy();
            }
            catch (Exception ex)
            {
                _syncError = true;
                FileManager.LogException(ex);
            }
            return httpReq;
        }

        // *** BEGIN CHANGE 2013-08-29 ***
        public string CallStatusService (string postStream)
        {
            String result = "";
            try
            {
                var syncServiceNode = "online_request.htm";
                var gatewayConfig = "0";

                HttpWebRequest request = BuildUrlSelfDispo(syncServiceNode);
                var path = request.RequestUri.AbsoluteUri;

                request = (HttpWebRequest)WebRequest.Create(path);

                request.Timeout = AppConfig.TimeoutDataSync;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                postStream = "OnInputProcessing=post" + postStream;
                request.AllowWriteStreamBuffering = true;

                request.ContentLength = postStream.Length;


                FileManager.LogHttp("Client Request [" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + request.Address + "&" +
                                    postStream + "\r\n");

                if (!AppConfig.FlagTestmode.Equals("X"))
                {
                    var stOut = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
                    stOut.Write(postStream);
                    stOut.Close();

                    // Do the request to get the response
                    // var stIn = new StreamReader(request.GetResponse().GetResponseStream());
                    var stIn = new StreamReader(request.GetResponse().GetResponseStream());
                    result = stIn.ReadToEnd();

                    stIn.Close();
                }
                else
                {

                    FileManager.LogMessage("Test mode enabled. Using server response from file");
                    OxStatus.GetInstance().TriggerEventMessage(2, 'W', DateTime.Now, "Testmodus aktiv", null, true, 5000);
                    IList<String> _serverSyncData = FileManager.ReadFileLines("Sync\\SFMServerSyncData.txt");
                    result = "";
                    foreach (String line in _serverSyncData)
                        result += line;
                }
                FileManager.LogHttp("Server Response[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                    (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + result);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return result;
  
        }
        // *** END CHANGE 2013-08-29 ***

        //Synchronisiert pro Datei die sich im Ordner Upload befindet und setzt das Datum des Sync_Date und ändert den Updflag auf X um
        public void UploadFiles(String filetype)
        {
            Boolean aConfirmMode = false;
            Boolean aDataSync = true;
            String aSyncRessource = "upload.htm";
            Boolean anonymousSync = false;
            String serverResult = "";
            var uploadlist = DocUploadManager.GetDocUploadList(filetype);

            try
            {
                if (uploadlist != null && uploadlist.Count > 0)
                {
                    foreach (var doc in uploadlist)
                    {
                        if (doc.Updflag.Equals("I") || doc.Updflag.Equals("U"))
                        {
                            serverResult = CallBackend2("", aConfirmMode, aDataSync, aSyncRessource, anonymousSync,
                                                        doc.FileLocation, GetUploadDocFields(doc));

                            if (serverResult.IndexOf("RETURNMESSAGE;E") != -1)
                                _syncError = true;
                            else
                            {
                                doc.Synctimestamp = DateTime.Now;

                                var sqlStmt = @"UPDATE S_DOCUPLOAD" +
                                              " SET UPDFLAG = @UPDFLAG, SYNCTIMESTAMP = @SYNCTIMESTAMP" +
                                              " WHERE ID = @ID";
                                var cmd = new SQLiteCommand(sqlStmt);
                                cmd.Parameters.AddWithValue("@UPDFLAG", "X");
                                cmd.Parameters.AddWithValue("@ID", doc.Id);
                                cmd.Parameters.AddWithValue("@SYNCTIMESTAMP",
                                                            doc.Synctimestamp.ToString("yyyyMMddHHmmss",
                                                                                       CultureInfo.InvariantCulture));
                                OxDbManager.GetInstance().FeedSyncTransaction(cmd);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        private String CallBackend2(string upstream, Boolean confirmMode, Boolean dataSync, String syncRessource, Boolean anonymousSync, string filename, NameValueCollection headerfields)
        {
            var parameterName = "name=\"UPSTREAM\";\r\n";
            var result = "";

            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            // Upload
            var httpReq = BuildUrlNew(syncRessource, anonymousSync, boundary, dataSync, confirmMode);
            var rs = httpReq.GetRequestStream();

            if (!String.IsNullOrEmpty(filename))
                parameterName = "name=\"file\"; filename=\"" + filename + "\"\r\nContent-Type: " + GetMimeTypeInfo(filename) + "\r\n";

            rs.Write(boundarybytes, 0, boundarybytes.Length);
            string header = "Content-Disposition: form-data; " + parameterName + ToHeaderString(headerfields) + "\r\n" + upstream;
            byte[] headerbytes = System.Text.Encoding.ASCII.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileManager.LogHttp("Client Request: " + "[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + httpReq.Address + "&" +
                                ToQueryString(headerfields) + "\r\n");

            if (!String.IsNullOrEmpty(filename) && File.Exists(filename))
            {
                FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[4096];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    rs.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
            }

            rs.Write(boundarybytes, 0, boundarybytes.Length);
            // Ende Upload

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            try
            {
                // Do the request to get the response
                var stIn = new StreamReader(httpReq.GetResponse().GetResponseStream(), _encodingDownload);
                result = stIn.ReadToEnd();
                FileManager.LogHttp("Client Request: " + "[" + (DateTime.Now.Subtract(SyncStartTimeStamp)).Minutes + "/" +
                                   (DateTime.Now.Subtract(SyncStartTimeStamp)).Seconds + "]: " + result);
                stIn.Close();
            }
            catch (Exception ex)
            {
                FileManager.LogMessage("Sync Exception" + ": " + ex.Message + "\r\n");
            }
            finally
            {
                httpReq.Abort();
            }
            return result;
        }

        private HttpWebRequest BuildUrlNew(String resource, Boolean anonymousSync, String boundary, Boolean dataSync, Boolean confirmMode)
        {
            System.Net.ServicePointManager.Expect100Continue = false;
            HttpWebRequest httpReq = null;

            NameValueCollection queryParams = new NameValueCollection();
            queryParams.Add("sap-language", AppConfig.SapLanguage);
            queryParams.Add("sap-client", AppConfig.Mandt);
            String service = AppConfig.GatewayServiceInfo;
            try
            {
                string link = "";
                if (!anonymousSync)
                {
                    queryParams.Add("sap-user", AppConfig.UserId);
                    queryParams.Add("sap-password", AppConfig.Pw);
                    queryParams.Add("foruser", AppConfig.UserId);
                    //service = AppConfig.GatewayServiceSyncld;
                    service = AppConfig.GatewayServiceSync;
                }

                httpReq = (HttpWebRequest)WebRequest.Create(AppConfig.Protocol + "://" + AppConfig.Gateway + ":" + AppConfig.GatewayPort + "/sap" +
                       AppConfig.GatewayIdent + service + resource + "?" + ToQueryString(queryParams));

                httpReq.ContentType = "multipart/form-data; boundary=" + boundary;
                httpReq.Method = "POST";
                httpReq.KeepAlive = true;
                httpReq.Credentials = System.Net.CredentialCache.DefaultCredentials;
                httpReq.AllowWriteStreamBuffering = true;
                if (dataSync && !confirmMode)
                    httpReq.Timeout = AppConfig.TimeoutSync;
                else
                    httpReq.Timeout = AppConfig.TimeoutDetail;

                if (!String.IsNullOrEmpty(AppConfig.ProxyHostname))
                {
                    FileManager.LogMessage("WebProxy myProxy = new System.Net.WebProxy(" + AppConfig.ProxyHostname + ", " + AppConfig.ProxyPort + ")");
                    WebProxy myProxy = new System.Net.WebProxy(AppConfig.ProxyHostname, int.Parse(AppConfig.ProxyPort));
                    myProxy.Credentials = CredentialCache.DefaultCredentials;
                    httpReq.Proxy = myProxy;
                }
                else
                {
                    FileManager.LogMessage("httpReq.Proxy = GlobalProxySelection.GetEmptyWebProxy();");
                    httpReq.Proxy = GlobalProxySelection.GetEmptyWebProxy();
                }
                FileManager.LogMessage("url: " + httpReq.RequestUri);
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }

            return httpReq;
        }

        private string ToQueryString(NameValueCollection nvc)
        {
            return string.Join("&", Convert(nvc, "{0}={1}"));
        }

        public NameValueCollection GetUploadDocFields(DocUpload doc)
        {
            NameValueCollection parameter = new NameValueCollection();

            try
            {
                parameter.Add("FILENAME", doc.OrgFilename);
                parameter.Add("FORUSER", AppConfig.UserId);
                parameter.Add("DOC_SIZE", doc.DocSize);
                parameter.Add("ORG_FILENAME", doc.OrgFilename);
                parameter.Add("ID", doc.Id);
                parameter.Add("DOC_TYPE", doc.DocType);
                parameter.Add("REF_TYPE", doc.RefType);
                parameter.Add("REF_OBJECT", doc.RefObject);
                parameter.Add("DESCRIPTION", doc.Description);
                parameter.Add("SPRAS", doc.Spras);
                parameter.Add("FILE_LOCATION", doc.FileLocation);
                parameter.Add("EMAIL", doc.Email);
                parameter.Add("MOBILEKEY", doc.MobileKey);
                parameter.Add("RECORDNAME", doc.Recordname);
                parameter.Add("RECORDNAME_BIN", doc.RecordnameBin);
                parameter.Add("UPTIMESTAMP", DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));

                // add proxy fields
                parameter.Add("sap_prot", "http");
                parameter.Add("sap_gate", AppConfig.Gateway);
                parameter.Add("sap_port", AppConfig.GatewayPort);
                parameter.Add("sap_serv", AppConfig.GatewayServiceSync + "upload.htm");
                parameter.Add("sap_lang", AppConfig.SapLanguage);
                parameter.Add("sap_mand", AppConfig.Mandt);
                parameter.Add("userid", AppConfig.UserId);
                parameter.Add("pw", AppConfig.Pw);
                //parameter.Add("TIMESTAMP", doc.Timestamp.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
                //parameter.Add("SYNCTIMESTAMP", doc.Synctimestamp.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return parameter;
        }

        private String[] Convert(NameValueCollection nvc, string formatstring)
        {
            var retVal = new String[nvc.Count];
            for (var i = 0; i < nvc.Count; i++)
            {
                var key = nvc.GetKey(i);
                var escapedKey = Uri.EscapeDataString(key);
                var value = nvc.Get(key);
                var escapedValue = Uri.EscapeDataString(value);
                retVal[i] = string.Format(formatstring, escapedKey, escapedValue);
            }
            return retVal;
        }

        public String GetMimeTypeInfo(String aFile)
        {
            try
            {
                FileManager.LogMessage("GetMimeTypeInfo: " + aFile);

                if (!string.IsNullOrEmpty(aFile) && !string.IsNullOrEmpty(Path.GetExtension(aFile)) && Registry.ClassesRoot.OpenSubKey(Path.GetExtension(aFile)) != null)
                {
                    var subKey = Registry.ClassesRoot.OpenSubKey(Path.GetExtension(aFile));
                    var keyNames = subKey.GetValueNames();
                    if (keyNames.Contains("Content Type"))
                        return subKey.GetValue("Content Type").ToString().ToLower();
                    if (keyNames.Contains("PerceivedType"))
                        return subKey.GetValue("PerceivedType").ToString().ToLower();
                    if (keyNames.Contains("Default"))
                        return subKey.GetValue("Default").ToString().ToLower();
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            FileManager.LogMessage("Fehler: MimeType kann nicht ermittelt werden.");
            return "application/pdf";
        }

        private string ToHeaderString(NameValueCollection nvc)
        {
            return string.Join("", Convert(nvc, "{0}: {1}\r\n"));
        }

    }
}