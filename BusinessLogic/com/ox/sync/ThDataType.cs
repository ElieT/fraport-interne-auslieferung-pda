﻿using System;
using System.Collections.Generic;
using System.Linq;
using oxmc.BusinessLogic.com.ox.helper;

namespace oxmc.BusinessLogic.com.ox.sync
{
    public class ThDataType
    {
        public ThDataType(string tableName)
        {
            TableName = tableName;

            Columns = new Dictionary<string, ColumnDescriptor>();
            Values = new Dictionary<string, string>();
        }

        public string TableName { get; private set; }
        public Dictionary<string, ColumnDescriptor> Columns { get; set; }
        public Dictionary<string, string> Values { get; set; }


        public IEnumerable<string> ColumnNames
        {
            get { return Columns.Values.Select(x => x.Column).ToList(); }
        }

        public IEnumerable<string> KeyColumns
        {
            get { return Columns.Values.Where(x => x.IsKey).Select(x => x.Column); }
        }

        public IEnumerable<string> NonKeyColumns
        {
            get { return Columns.Values.Where(x => x.IsKey == false).Select(x => x.Column); }
        }

        public void ResetValues ()
        {
            try
            {
                Values.Clear();
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public string getUpdFlag()
        {
            string retValue = "I";
            try
            {
                retValue = Values["UPDFLAG"];
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retValue;
        }

        public void setUpdFlag(string aUpdFlag)
        {
            try
            {
                Values["UPDFLAG"] = aUpdFlag;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }
    }

    public class ColumnDescriptor
    {
        public string Column;
        public bool IsKey;
    }
}