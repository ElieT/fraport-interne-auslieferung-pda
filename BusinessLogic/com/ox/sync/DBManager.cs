﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;

using System.Linq;
using oxmc.BusinessLogic.com.ox.helper;
using oxmc.Common;

namespace oxmc.BusinessLogic.com.ox.sync
{
    public class DBManager
    {
        public static void ClearData()
        {
            var tableList = (List<string>)OxDbManager.GetDatabaseTables("C");
            tableList.AddRange((List<string>)OxDbManager.GetDatabaseTables("D"));
            var dbCount = -1;
            try
            {
                foreach (var table in tableList)
                {
                    var sqlStmt = @"DELETE FROM [" + table + "] WHERE MANDT = '" + AppConfig.Mandt + "' AND USERID = '" + AppConfig.UserId + "'";
                    OxDbManager.GetInstance().FeedSyncTransaction(new SQLiteCommand(sqlStmt));
                    //OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                }
                OxDbManager.GetInstance().CommitTransaction();
            }
            catch (Exception ex)
            {
                OxDbManager.GetInstance().CommitTransaction();
                FileManager.LogException(ex);
            }
        }

        public static void ChangeUpdateFlag()
        {
            IList<string> tableList = OxDbManager.GetDatabaseTables("");
            int dbCount = -1;
            try
            {
                foreach (String table in tableList)
                {
                    String sqlStmt = @"UPDATE " + table + " SET UPDFLAG = 'X'";
                    OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void ClearColumnOrder()
        {
            try
            {
                String sqlStmt = @"DELETE FROM S_HLP_COLUMNORDER";
                OxDbManager.GetInstance().ExecuteSingleCommand(new SQLiteCommand(sqlStmt));
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        public static void CreateColumnOrder()
        {
            try
            {
                int dbResult = 0;

                var fieldList = new ArrayList();
                var descList = new ArrayList();
                var listDef = new ArrayList();
                var tableList = new List<string>();
                

                fieldList.Add("MANDT,D_SYNC_LOG_DATA,1,X,X");
                fieldList.Add("USERID,D_SYNC_LOG_DATA,2,X,X");
                fieldList.Add("DIRTY,D_SYNC_LOG_DATA,3,X,X");
                fieldList.Add("UPDFLAG,D_SYNC_LOG_DATA,4,");
                fieldList.Add("ID,D_SYNC_LOG_DATA,5,X,X");
                fieldList.Add("SDATE,D_SYNC_LOG_DATA,6,,");
                fieldList.Add("STIME,D_SYNC_LOG_DATA,7,,");
                fieldList.Add("EDATE,D_SYNC_LOG_DATA,8,,");
                fieldList.Add("ETIME,D_SYNC_LOG_DATA,9,,");
                fieldList.Add("UBYTES,D_SYNC_LOG_DATA,10,,");
                fieldList.Add("DBYTES,D_SYNC_LOG_DATA,11,,");
                fieldList.Add("URECORDS,D_SYNC_LOG_DATA,12,,");
                fieldList.Add("DRECORDS,D_SYNC_LOG_DATA,13,,");
                fieldList.Add("FAILED,D_SYNC_LOG_DATA,14,,");
                fieldList.Add("ERRORID,D_SYNC_LOG_DATA,15,,");
                fieldList.Add("MESSAGE1,D_SYNC_LOG_DATA,16,,");
                fieldList.Add("MESSAGE2,D_SYNC_LOG_DATA,17,,");

                fieldList.Add("MANDT,D_SYNC_NETWORK_DATA,1,X,X");
                fieldList.Add("USERID,D_SYNC_NETWORK_DATA,2,X,X");
                fieldList.Add("DIRTY,D_SYNC_NETWORK_DATA,3,X,X");
                fieldList.Add("UPDFLAG,D_SYNC_NETWORK_DATA,4,");
                fieldList.Add("ID,D_SYNC_NETWORK_DATA,5,X,X");
                fieldList.Add("DATE,D_SYNC_NETWORK_DATA,6,,");
                fieldList.Add("TIME,D_SYNC_NETWORK_DATA,7,,");
                fieldList.Add("IP1,D_SYNC_NETWORK_DATA,8,,");
                fieldList.Add("IP2,D_SYNC_NETWORK_DATA,9,,");
                fieldList.Add("NETWORK_TYPE1,D_SYNC_NETWORK_DATA,10,,");
                fieldList.Add("NETWORK_DESC1,D_SYNC_NETWORK_DATA,11,,");
                fieldList.Add("NETWORK_NAME1,D_SYNC_NETWORK_DATA,12,,");
                fieldList.Add("NETWORK_SPEED1,D_SYNC_NETWORK_DATA,13,,");
                fieldList.Add("NETWORK_TYPE2,D_SYNC_NETWORK_DATA,14,,");
                fieldList.Add("NETWORK_DESC2,D_SYNC_NETWORK_DATA,15,,");
                fieldList.Add("NETWORK_NAME2,D_SYNC_NETWORK_DATA,16,,");
                fieldList.Add("NETWORK_SPEED2,D_SYNC_NETWORK_DATA,17,,");
                fieldList.Add("SAP_AVAILABLE,D_SYNC_NETWORK_DATA,18,,");


                fieldList.Add("MANDT,C_MC_CUST_002,1,X,X");
                fieldList.Add("USERID,C_MC_CUST_002,2,X,X");
                fieldList.Add("DIRTY,C_MC_CUST_002,3,X,X");
                fieldList.Add("UPDFLAG,C_MC_CUST_002,4,,");
                fieldList.Add("MOBILEUSER,C_MC_CUST_002,5,X,");
                fieldList.Add("SCENARIO,C_MC_CUST_002,6,,");

                fieldList.Add("MANDT,C_MC_CUST_010,1,X,X");
                fieldList.Add("USERID,C_MC_CUST_010,2,X,X");
                fieldList.Add("DIRTY,C_MC_CUST_010,3,X,X");
                fieldList.Add("UPDFLAG,C_MC_CUST_010,4,,");
                fieldList.Add("SCENARIO,C_MC_CUST_010,5,X,");
                fieldList.Add("TRACE_FILESIZE,C_MC_CUST_010,6,,");
                fieldList.Add("RFID_TIMEOUT,C_MC_CUST_010,7,,");
                fieldList.Add("RFID_PORTNAME_CE,C_MC_CUST_010,8,,");
                fieldList.Add("RFID_PORTNAME_W32,C_MC_CUST_010,9,,");
                fieldList.Add("RFID_PORTTYPE_CE,C_MC_CUST_010,10,,");
                fieldList.Add("RFID_PORTTYPE_W32,C_MC_CUST_010,11,,");
                fieldList.Add("RFID_TIMEOUT_DR_CE,C_MC_CUST_010,12,,");
                fieldList.Add("RFID_TIMEOUT_DR_W32,C_MC_CUST_010,13,,");
                fieldList.Add("RFID_WRITE_VERIFY,C_MC_CUST_010,14,,");
                fieldList.Add("STATUSMESSAGE_TIMEOUT,C_MC_CUST_010,15,,");
                fieldList.Add("FILE_FOLDER_W32,C_MC_CUST_010,16,,");
                fieldList.Add("FILE_FOLDER_CE,C_MC_CUST_010,17,,");
                fieldList.Add("MESSAGE_DISPLAY_MODE,C_MC_CUST_010,18,,");
                fieldList.Add("SVM_TEMP_DIR_CE,C_MC_CUST_010,19,,");
                fieldList.Add("SVM_TEMP_DIR_W32,C_MC_CUST_010,20,,");


                fieldList.Add("MANDT,D_ZFRAPORT_IA_IAUS,1,X,X");
                fieldList.Add("USERID,D_ZFRAPORT_IA_IAUS,2,X,X");
                fieldList.Add("UPDFLAG,D_ZFRAPORT_IA_IAUS,4,,");
                fieldList.Add("DIRTY,D_ZFRAPORT_IA_IAUS,3,X,X");
                //fieldList.Add("UPDFLAG,D_ZFRAPORT_IA_IAUS,4,,");
                fieldList.Add("BELNR,D_ZFRAPORT_IA_IAUS,5,,");
                fieldList.Add("SEGID,D_ZFRAPORT_IA_IAUS,6,,");
                fieldList.Add("ERDAT,D_ZFRAPORT_IA_IAUS,7,,");
                fieldList.Add("ERTIM,D_ZFRAPORT_IA_IAUS,8,,");
                fieldList.Add("MBLNR,D_ZFRAPORT_IA_IAUS,9,,");
                fieldList.Add("MJAHR,D_ZFRAPORT_IA_IAUS,10,,");
                fieldList.Add("MBLPO,D_ZFRAPORT_IA_IAUS,11,,");
                fieldList.Add("EBELN,D_ZFRAPORT_IA_IAUS,12,,");
                fieldList.Add("EBELP,D_ZFRAPORT_IA_IAUS,13,,");
                fieldList.Add("USNAM,D_ZFRAPORT_IA_IAUS,14,,");
                fieldList.Add("TXZ01,D_ZFRAPORT_IA_IAUS,15,,");
                fieldList.Add("MENGE,D_ZFRAPORT_IA_IAUS,16,,");
                fieldList.Add("MEINS,D_ZFRAPORT_IA_IAUS,17,,");
                fieldList.Add("EMPF,D_ZFRAPORT_IA_IAUS,18,,");
                fieldList.Add("GEBNR,D_ZFRAPORT_IA_IAUS,19,,");
                fieldList.Add("RAUM,D_ZFRAPORT_IA_IAUS,20,,");
                fieldList.Add("SHORT,D_ZFRAPORT_IA_IAUS,21,,");
                fieldList.Add("KOSTL,D_ZFRAPORT_IA_IAUS,22,,");
                fieldList.Add("AUFNR,D_ZFRAPORT_IA_IAUS,23,,");
                fieldList.Add("PSPNR,D_ZFRAPORT_IA_IAUS,24,,");
                fieldList.Add("WLIEF,D_ZFRAPORT_IA_IAUS,25,,");
                fieldList.Add("WUZEITV,D_ZFRAPORT_IA_IAUS,26,,");
                fieldList.Add("WUZEITB,D_ZFRAPORT_IA_IAUS,27,,");
                fieldList.Add("SGTXT,D_ZFRAPORT_IA_IAUS,28,,");
                fieldList.Add("BEREICH,D_ZFRAPORT_IA_IAUS,29,,");
                fieldList.Add("IA_STATUS,D_ZFRAPORT_IA_IAUS,30,,");
                fieldList.Add("GRUND,D_ZFRAPORT_IA_IAUS,31,,");
                fieldList.Add("GTEXT,D_ZFRAPORT_IA_IAUS,32,,");
                fieldList.Add("DIENST,D_ZFRAPORT_IA_IAUS,33,,");
                fieldList.Add("NUMMER,D_ZFRAPORT_IA_IAUS,34,,");
                fieldList.Add("LFNAME,D_ZFRAPORT_IA_IAUS,35,,");
                fieldList.Add("TELEFON,D_ZFRAPORT_IA_IAUS,36,,");
                fieldList.Add("GUID,D_ZFRAPORT_IA_IAUS,37,,");
                fieldList.Add("EMAIL,D_ZFRAPORT_IA_IAUS,38,,");
                fieldList.Add("PRIO,D_ZFRAPORT_IA_IAUS,39,,");
                fieldList.Add("TRANS,D_ZFRAPORT_IA_IAUS,40,,");
                fieldList.Add("XBLOCK,D_ZFRAPORT_IA_IAUS,41,,");
                fieldList.Add("VLGORT,D_ZFRAPORT_IA_IAUS,42,,");
                fieldList.Add("EQUNR,D_ZFRAPORT_IA_IAUS,43,,");
                fieldList.Add("LIDAT,D_ZFRAPORT_IA_IAUS,44,,");
                fieldList.Add("SEQU,D_ZFRAPORT_IA_IAUS,45,,");
                fieldList.Add("MATNR,D_ZFRAPORT_IA_IAUS,46,,");
                fieldList.Add("PASSCODE,D_ZFRAPORT_IA_IAUS,47,,");
                fieldList.Add("MOBILEKEY,D_ZFRAPORT_IA_IAUS,48,,");
                fieldList.Add("ISPICKUP,D_ZFRAPORT_IA_IAUS,49,,X");

                fieldList.Add("MANDT,D_ZFRAPORT_IA_DOCH,1,X,X");
                fieldList.Add("USERID,D_ZFRAPORT_IA_DOCH,2,X,X");
                fieldList.Add("DIRTY,D_ZFRAPORT_IA_DOCH,3,X,X");
                fieldList.Add("UPDFLAG,D_ZFRAPORT_IA_DOCH,4,");
                fieldList.Add("ID,D_ZFRAPORT_IA_DOCH,5,X,X");
                fieldList.Add("DOC_TYPE,D_ZFRAPORT_IA_DOCH,6,,X");
                fieldList.Add("REF_TYPE,D_ZFRAPORT_IA_DOCH,7,,X");
                fieldList.Add("REF_OBJECT,D_ZFRAPORT_IA_DOCH,8,,X");
                fieldList.Add("DESCRIPTION,D_ZFRAPORT_IA_DOCH,9,,X");
                fieldList.Add("SPRAS,D_ZFRAPORT_IA_DOCH,10,,X");
                fieldList.Add("FILENAME,D_ZFRAPORT_IA_DOCH,11,,");
                fieldList.Add("EMAIL,D_ZFRAPORT_IA_DOCH,12,,X");
                fieldList.Add("MOBILEKEY,D_ZFRAPORT_IA_DOCH,13,,");

                fieldList.Add("MANDT,D_ZFRAPORT_IA_DOCD,1,X,X");
                fieldList.Add("USERID,D_ZFRAPORT_IA_DOCD,2,X,X");
                fieldList.Add("DIRTY,D_ZFRAPORT_IA_DOCD,3,X,X");
                fieldList.Add("UPDFLAG,D_ZFRAPORT_IA_DOCD,4,");
                fieldList.Add("ID,D_ZFRAPORT_IA_DOCD,5,,X");
                fieldList.Add("LINENUMBER,D_ZFRAPORT_IA_DOCD,6,,");
                fieldList.Add("LINE,D_ZFRAPORT_IA_DOCD,7,,");
                fieldList.Add("MOBILEKEY,D_ZFRAPORT_IA_DOCD,8,,");

                fieldList.Add("MANDT,C_ZFRAPORT_IA_GRND,1,X,X");
                fieldList.Add("USERID,C_ZFRAPORT_IA_GRND,2,X,X");
                fieldList.Add("DIRTY,C_ZFRAPORT_IA_GRND,3,X,X");
                fieldList.Add("UPDFLAG,C_ZFRAPORT_IA_GRND,4,,");
                fieldList.Add("SPRAS,C_ZFRAPORT_IA_GRND,5,X,");
                fieldList.Add("GRUND,C_ZFRAPORT_IA_GRND,6,X,");
                fieldList.Add("TEXT,C_ZFRAPORT_IA_GRND,7,,");

                fieldList.Add("MANDT,C_ZFRAPORT_ENH_MD0001,1,X,X");
                fieldList.Add("USERID,C_ZFRAPORT_ENH_MD0001,2,X,X");
                fieldList.Add("DIRTY,C_ZFRAPORT_ENH_MD0001,3,X,X");
                fieldList.Add("UPDFLAG,C_ZFRAPORT_ENH_MD0001,4,,");
                fieldList.Add("ARBPL,C_ZFRAPORT_ENH_MD0001,5,X,");
                fieldList.Add("WERKS,C_ZFRAPORT_ENH_MD0001,6,X,");
                fieldList.Add("LSTAR_WORK,C_ZFRAPORT_ENH_MD0001,7,X,");
                fieldList.Add("LSTAR_WORK_TXT,C_ZFRAPORT_ENH_MD0001,8,X,");
                fieldList.Add("LSTAR_TRAVEL,C_ZFRAPORT_ENH_MD0001,9,X,");
                fieldList.Add("LSTAR_TRAV_TXT,C_ZFRAPORT_ENH_MD0001,10,X,");

                fieldList.Add("MANDT,C_LTXT_HEADER,1,X,X");
                fieldList.Add("USERID,C_LTXT_HEADER,2,X,X");
                fieldList.Add("DIRTY,C_LTXT_HEADER,3,X,X");
                fieldList.Add("UPDFLAG,C_LTXT_HEADER,4,,");
                fieldList.Add("TDOBJECT,C_LTXT_HEADER,5,X,");
                fieldList.Add("TDNAME,C_LTXT_HEADER,6,X,");
                fieldList.Add("TDID,C_LTXT_HEADER,7,X,");
                fieldList.Add("TDSPRAS,C_LTXT_HEADER,8,X,");

                fieldList.Add("MANDT,C_LTXT_LINE,1,X,X");
                fieldList.Add("USERID,C_LTXT_LINE,2,X,X");
                fieldList.Add("DIRTY,C_LTXT_LINE,3,X,X");
                fieldList.Add("UPDFLAG,C_LTXT_LINE,4,,");
                fieldList.Add("TDOBJECT,C_LTXT_LINE,5,X,");
                fieldList.Add("TDNAME,C_LTXT_LINE,6,X,");
                fieldList.Add("TDID,C_LTXT_LINE,7,X,");
                fieldList.Add("TDSPRAS,C_LTXT_LINE,8,X,");
                fieldList.Add("COUNTER,C_LTXT_LINE,9,X,");
                fieldList.Add("TDFORMAT,C_LTXT_LINE,10,,");
                fieldList.Add("TDLINE,C_LTXT_LINE,11,,");

                /*
                 * *** DESCLIST ***
                 */

                descList.Add("POINT,C_MEASPOINT,DE,Meßpunkt");
                descList.Add("MPOBJ,C_MEASPOINT,DE,Objektnr.");
                descList.Add("PSORT,C_MEASPOINT,DE,Positionsnr.");
                descList.Add("PTTXT,C_MEASPOINT,DE,Meßp. Bez.");
                descList.Add("LOCAS,C_MEASPOINT,DE,Lokalisierungsbaugr.");
                descList.Add("ATINN,C_MEASPOINT,DE,Int. Merkmal");
                descList.Add("MRMIN,C_MEASPOINT,DE,Min. Zählerstand");
                descList.Add("MRMAX,C_MEASPOINT,DE,Max. Zählerstand");
                descList.Add("MRNGU,C_MEASPOINT,DE,Meßbereichseinh.");
                descList.Add("DESIR,C_MEASPOINT,DE,Sollwert");

                descList.Add("MANDT,D_TIMECONF,DE,Auftragsnummer");
                descList.Add("UPDFLAG,D_TIMECONF,DE,Update Kennzeichen");
                descList.Add("VORNR,D_TIMECONF,DE,Vorgangsnummer");
                descList.Add("RMZHL,D_TIMECONF,DE,Zähler Rückm.");
                descList.Add("ERSDA,D_TIMECONF,DE,Erfdat. Rückm.");
                descList.Add("ERNAM,D_TIMECONF,DE,Erfasser");
                descList.Add("SPLIT,D_TIMECONF,DE,Splittnummer");
                descList.Add("ARBPL,D_TIMECONF,DE,Arbeitsplatz");
                descList.Add("ISMNW,D_TIMECONF,DE,Istarbeit");
                descList.Add("ISMNE,D_TIMECONF,DE,Einheit");

                descList.Add("AUFNR,D_NOTIHEAD,DE,Auftragsnummer");
                descList.Add("PRIOK,D_NOTIHEAD,DE,Priorität");
                descList.Add("EQUNR,D_NOTIHEAD,DE,Equipmentnummer");
                descList.Add("BAUTL,D_NOTIHEAD,DE,Baugruppe");
                descList.Add("IWERK,D_NOTIHEAD,DE,Instandhaltungsplanungswerk");
                descList.Add("INGPR,D_NOTIHEAD,DE,Planergruppe");
                descList.Add("QMNUM,D_NOTIHEAD,DE,Meldungsnummer");
                descList.Add("TPLNR,D_NOTIHEAD,DE,Technischer Platz");
                descList.Add("EQFNR,D_NOTIHEAD,DE,Sortierfeld");
                descList.Add("STRMN,D_NOTIHEAD,DE,Gew. Starttermin");
                descList.Add("STATUS,D_NOTIHEAD,DE,Status");
                descList.Add("STORT,D_NOTIHEAD,DE,Standort");
                descList.Add("QMART,D_NOTIHEAD,DE,Meldungsart");

                descList.Add("MANDT,C_MATERIAL,DE,Auftragsnummer");
                descList.Add("USERID,C_MATERIAL,DE,Priorität");
                descList.Add("EQUNR,C_MATERIAL,DE,Equipmentnummer");
                descList.Add("MATNR,C_MATERIAL,DE,Materialnummer");
                descList.Add("MEINS,C_MATERIAL,DE,Einheit");
                descList.Add("MAKTX,C_MATERIAL,DE,Kurztext");

                descList.Add("EQUI,D_EQUI,DE,Equipment");
                descList.Add("EQTYP,D_EQUI,DE,Typ");
                descList.Add("EQART,D_EQUI,DE,Art");
                descList.Add("EQFNR,D_EQUI,DE,Sortierfeld");
                descList.Add("TIDNR,D_EQUI,DE,Tech. Identnr.");
                descList.Add("INVNR,D_EQUI,DE,Inventarnr.");
                descList.Add("SERNR,D_EQUI,DE,Seriennummer");
                descList.Add("MATNR,D_EQUI,DE,Materialnummer");
                descList.Add("EQKTX,D_EQUI,DE,Kurztext");
                descList.Add("HERST,D_EQUI,DE,Hersteller");
                descList.Add("TYPBZ,D_EQUI,DE,Typbezeichnung");
                descList.Add("BAUJJ,D_EQUI,DE,Baujahr");
                descList.Add("MSGRP,D_EQUI,DE,Standort (Raum)");
                descList.Add("MAPAR,D_EQUI,DE,Herst. Teilnr.");

                descList.Add("TPLNR,D_FLOC,DE,Technischer Platz");
                descList.Add("PLTXT,D_FLOC,DE,Kurztext");
                descList.Add("STORT,D_FLOC,DE,Standort");
                descList.Add("EQFNR,D_FLOC,DE,Sortierfeld");
                descList.Add("MSGRP,D_FLOC,DE,Raum");
                descList.Add("TYPBZ,D_FLOC,DE,Typbezeichnung");
                descList.Add("BAUJJ,D_FLOC,DE,Baujahr");

                descList.Add("AUFNR,D_ORDHEAD,DE,Auftragsnummer");
                descList.Add("PRIOK,D_ORDHEAD,DE,Priorität");
                descList.Add("EQUNR,D_ORDHEAD,DE,Equipmentnummer");
                descList.Add("BAUTL,D_ORDHEAD,DE,Baugruppe");
                descList.Add("IWERK,D_ORDHEAD,DE,Instandhaltungsplanungswerk");
                descList.Add("INGPR,D_ORDHEAD,DE,Planergruppe");
                descList.Add("PM_OBJTY,D_ORDHEAD,DE,Objekttyp");
                descList.Add("GEWRK,D_ORDHEAD,DE,Arbeitsplatz");
                descList.Add("KUNUM,D_ORDHEAD,DE,Kundennummer");
                descList.Add("QMNUM,D_ORDHEAD,DE,Meldungsnummer");
                descList.Add("SERIALNR,D_ORDHEAD,DE,Serialnummer");
                descList.Add("SERMAT,D_ORDHEAD,DE,Materialnummer");
                descList.Add("AUART,D_ORDHEAD,DE,Auftragsart");
                descList.Add("KTEXT,D_ORDHEAD,DE,Kurztext");
                descList.Add("VAPLZ,D_ORDHEAD,DE,Verantwortlicher Arbeitsplatz");
                descList.Add("WAWRK,D_ORDHEAD,DE,Werk");
                descList.Add("GLTRP,D_ORDHEAD,DE,Eckendtermin");
                descList.Add("GSTRP,D_ORDHEAD,DE,Eckstarttermin");
                descList.Add("TPLNR,D_ORDHEAD,DE,Technischer Platz");

                /*
                 * *** LISTDEF ***
                 */

                listDef.Add("D_ORDERLIST,AUFNR,Auftragsnummer,1");
                listDef.Add("D_ORDERLIST,PLTXT,Technischer Platz,2");
                listDef.Add("D_ORDERLIST,EQTXT,Equipment,3");
                listDef.Add("D_ORDERLIST,STORT,Standort,4");

                listDef.Add("D_TIMECONF,ERSDA,Erstellungsdatum,1");
                listDef.Add("D_TIMECONF,VORNR,Vorgang,2");
                listDef.Add("D_TIMECONF,ISMNW,Dauer,3");

                /*
                 * *** CUSTOMIZINGTABLES
                 */
                tableList.Add("C_ZFRAPORT_ENH_CUST001");
                //tableList.Add("C_ZFRAPORT_ENH_MD0001");
                tableList.Add("C_ACTIVITY_TYPE");
                tableList.Add("C_AM_CUST_001");
                tableList.Add("C_AM_CUST_010");
                tableList.Add("C_AM_CUST_011");
                tableList.Add("C_AM_CUST_012");
                tableList.Add("C_AM_CUST_013");
                tableList.Add("C_AM_CUST_ILART_ORTYPE");
                tableList.Add("C_CHARACTVALUE");
                tableList.Add("C_CLASS");
                tableList.Add("C_CLASSCHARACT");
                tableList.Add("C_CODECATALOG");
                tableList.Add("C_CUSTAUSWM");
                tableList.Add("G_CUSTBERS");
                tableList.Add("G_CUSTCODES");
                tableList.Add("C_CUSTNOTIFTYPE");
                tableList.Add("C_ENH_CODESTATUS");
                tableList.Add("C_EQUI_CHARACT");
                tableList.Add("C_HRCHAR");
                tableList.Add("C_MC_CUST_002");
                tableList.Add("C_MC_CUST_010");
                tableList.Add("C_OBJCLASSAS");


                for (int i = 0; i < fieldList.Count; i++)
                {
                    String isKey = "";
                    String surpressUpload = "";
                    String filterType = "";
                    //System.Console.WriteLine(fieldList[i]);
                    if (((String)fieldList[i]).Split(',').Length > 3)
                        isKey = ((String)fieldList[i]).Split(',')[3];
                    if (((String)fieldList[i]).Split(',').Length > 4)
                        surpressUpload = ((String)fieldList[i]).Split(',')[4];
                    if (((String)fieldList[i]).Split(',').Length > 5)
                        filterType = ((String)fieldList[i]).Split(',')[5];

                    var sqlStmt =
                        @"INSERT OR REPLACE INTO S_HLP_COLUMNORDER (column_x, order_c, table_x, isKey, surpressUpload, filterType) values ('" +
                        ((String)fieldList[i]).Split(',')[0] + "', " +
                        ((String)fieldList[i]).Split(',')[2] + ", '" +
                        ((String)fieldList[i]).Split(',')[1] + "', '" +
                        isKey + "', '" +
                        surpressUpload + "', '" +
                        filterType + "')";

                    var cmd = new SQLiteCommand(sqlStmt);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }
                for (int i = 0; i < descList.Count; i++)
                {
                    var sqlStmt =
                        @"INSERT OR REPLACE INTO S_HLP_COLUMNDESC (column_x, table_x, langu, description) values (" +
                        "@COLUMN_X, " +
                        "@TABLE_X, " +
                        "@LANGU, " +
                        "@DESC)";

                    var cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@COLUMN_X", ((String)descList[i]).Split(',')[0]);
                    cmd.Parameters.AddWithValue("@TABLE_X", ((String)descList[i]).Split(',')[1]);
                    cmd.Parameters.AddWithValue("@LANGU", ((String)descList[i]).Split(',')[2]);
                    cmd.Parameters.AddWithValue("@DESC", ((String)descList[i]).Split(',')[3]);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }

                foreach (var table in tableList)
                {
                    var sqlStmt = "INSERT OR REPLACE INTO S_CUSTOMIZINGTABLES_DEF (TABLENAME) VALUES (@TABLENAME)";
                    var cmd = new SQLiteCommand(sqlStmt);
                    cmd.Parameters.AddWithValue("@TABLENAME", table);
                    OxDbManager.GetInstance().FeedTransaction(cmd);
                }


                OxDbManager.GetInstance().CommitTransaction();
                OxDbManager.LoadTableList();

            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
        }

        //returns the Fielddescriptions for all Fields
        public static Hashtable GetFieldDesc(String table)
        {
            var retVal = new Hashtable();
            try
            {
                String sqlStmt = "SELECT * " +
                                  "FROM S_HLP_COLUMNDESC " +
                                  "WHERE table_x = @TABLE " +
                                  "AND langu = @LANGU";
                var cmd = new SQLiteCommand(sqlStmt);
                cmd.Parameters.AddWithValue("@TABLE", table);
                cmd.Parameters.AddWithValue("@LANGU", "DE");
                List<Dictionary<string, string>> records = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (Dictionary<string, string> rdr in records)
                {
                    retVal.Add(rdr["column_x"], rdr["description"]);
                }
                return retVal;
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }

        public static List<string> GetCustomizingtables()
        {
            var retVal = new List<string>();
            try
            {
                var sqlStmt = "SELECT * FROM S_CUSTOMIZINGTABLES_DEF";
                var cmd = new SQLiteCommand(sqlStmt);
                var result = OxDbManager.GetInstance().ExecuteSingleCommand(cmd);
                foreach (var res in result)
                {
                    retVal.Add(res["TABLENAME"]);
                }
            }
            catch (Exception ex)
            {
                FileManager.LogException(ex);
            }
            return retVal;
        }
    }
}