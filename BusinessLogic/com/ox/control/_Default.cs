﻿/////////////////FYI: Framework used - .NET 3.5///////////////////

using System;

namespace oxmc.BusinessLogic.com.ox.control
{
    public class _Default
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CreatePDFDocument();
        }

        /// <summary>
        /// Creates the PDF document with a given content at a given location.
        /// </summary>
        /// <param name=”strFilePath”>The file path to write the new PDF to.</param>
        /// <param name=”strContent”>Content in HTML to write to the PDF.</param>
        public void CreatePDFDocument()
        {
            /*
        string pdfTemplate = @"testFinal.pdf";
 
        string imagePath = String.Empty;
 
        imagePath = @"community.jpg";
 
        PdfReader pdfReader = null;
       
        // Create the form filler
        FileStream pdfOutputFile = new FileStream(pdfTemplate, FileMode.Create);
 
        pdfReader = new PdfReader(@"\testTemplate.pdf");
       
        PdfStamper pdfStamper = null;
 
        pdfStamper = new PdfStamper(pdfReader, pdfOutputFile);
 
        // Get the form fields
        AcroFields testForm = pdfStamper.AcroFields;
 
        // Fill the form
        testForm.SetField("textBox1", "This is PDF generation test from coolwebdeveloper.com using iTextSharp PDF library");
        testForm.SetField("textBox2", "This is PDF generation test from coolwebdeveloper.com using iTextSharp PDF library");
 
        iTextSharp.text.Image instanceImg = iTextSharp.text.Image.GetInstance(imagePath);
 
        PdfContentByte overContent = pdfStamper.GetOverContent(1);
 
        //Specifying the name of the field wehere this image will be placed
        float[] imageArea = testForm.GetFieldPositions("testImage1");
 
        iTextSharp.text.Rectangle imageRect = new Rectangle(imageArea[1], imageArea[2], imageArea[3], imageArea[4]);
 
        instanceImg.ScaleToFit(imageRect.Width, imageRect.Height);
 
        instanceImg.SetAbsolutePosition(imageArea[3] - instanceImg.ScaledWidth + (imageRect.Width - instanceImg.ScaledWidth) / 2, imageArea[2] + (imageRect.Height - instanceImg.ScaledHeight) / 2);
 
        overContent.AddImage(instanceImg);
 
        //’Flatten’ (make the text go directly onto the pdf) and close the form
        pdfStamper.FormFlattening = true;
 
        pdfStamper.Close();
 
        pdfReader.Close();
 */
        }
    }
}