﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Updater
{
    public partial class MainForm : Form
    {
        public static string _logfile = "";
        public static string _backupPath;
        public static string _source;
        public static string _target;
        public static Dictionary<string, NameValueCollection> _configDict;
        public static List<string> _newFiles;
        public static string _appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
        public static string _appName = Assembly.GetExecutingAssembly().GetName().CodeBase;
        private static string _callerProcess;




        [DllImport("coredll.dll")]
        private static extern int SetSystemPowerState(string psState, int StateFlags, int Options);
        const int POWER_STATE_ON = 0x00010000;
        const int POWER_STATE_OFF = 0x00020000;
        const int POWER_STATE_SUSPEND = 0x00200000;
        const int POWER_FORCE = 4096;
        const int POWER_STATE_RESET = 0x00800000;
        public int SoftReset()
        {
                // Though I guess this will never really return anything
            return SetSystemPowerState(null, POWER_STATE_RESET, POWER_FORCE);
        }

       
        public MainForm(string[] args)
        {
            InitializeComponent();
            Refresh();
            BringToFront();
            Visible = true;
            Thread.Sleep(2000);
            try
            {
                if (File.Exists(_appPath + "\\updater.config"))
                {
                    var file = new StreamReader(_appPath + "\\updater.config");
                    _source = file.ReadLine();
                    _target = file.ReadLine();
                    _callerProcess = file.ReadLine();
                    _source = _source.Trim();
                    _target = _target.Trim();
                    _callerProcess = _callerProcess.Trim();
                    _logfile = _target + "\\log\\update_log.txt";
                    if (File.Exists(_logfile))
                        File.Delete(_logfile);
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Loading configfile. ");
                    file.Close();
                }
                else
                {
                    if (args.Count() >= 3)
                    {
                        _source = args[0];
                        _target = args[1];
                        _callerProcess = args[2];
                        _source = _source.Trim();
                        _target = _target.Trim();
                        _callerProcess = _callerProcess.Trim();
                        _logfile = _target + "\\log\\update_log.txt";
                        if (File.Exists(_logfile))
                            File.Delete(_logfile);
                        WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Configfile not found, trying parameters. ");
                        WriteFile(_logfile,
                                  DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Configfile: " +
                                  Directory.GetCurrentDirectory() + "\\updater.config");
                    }
                    else
                    {
                        AddText("100010: Fehler beim durchführen des Updates. Bitte wenden Sie sich an die Systemadministration");
                        return;
                    }
                }

                AddText("Startet Update, bitte warten");
                //AddText("Process: " + _callerProcess);
                _backupPath = _source + "\\backup\\";

                //AddText("1");
                //Get a list of all processes and try to kill the exe-process (if its active)
                var procList = new List<ProcEntry>();
                ProcessEnumerator.Enumerate(ref procList);
                //AddText("ProcCount:" + procList.Count);
                foreach (var proc in procList)
                {
                    if (proc.ExeName.Equals(_callerProcess) || proc.ID.Equals(_callerProcess))
                    {
                        var iter = 0;
                        //AddText("Found Process: " + proc.ExeName);
                        var p = Process.GetProcessById((int) proc.ID);
                        ProcessEnumerator.KillProcess(proc.ID);

                        Thread.Sleep(2000);
                        while (!p.HasExited && iter < 10)
                        {
                            WriteFile(_logfile,
                                      DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Waiting to end process " +
                                      proc.ExeName);
                            iter++;
                            Thread.Sleep(500);
                        }
                        if (!p.HasExited)
                        {
                            WriteFile(_logfile,
                                      DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Unable to end Process " +
                                      proc.ExeName);
                            AddText("Anwendung konnte nicht beendet werden. Starten Sie das Update erneut. ");
                            return;
                        }
                        WriteFile(_logfile,
                                  DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": " + proc.ExeName +
                                  " Process has ended");
                        Thread.Sleep(2000);
                    }
                }
                //AddText("2");

                foreach (var arg in args)
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Parameter: " + arg);
                //Check if both paths exist);
                if (!Directory.Exists(_source) || !Directory.Exists(_target))
                    Application.Exit();

                //continue with update
                _newFiles = GetFiles(_source);

                CreateBackup();
                if (!LastOperationSuccessful())
                    Application.Exit();

                //Copy files to the applicationfolder
                var appFileName = _appName.Substring(_appName.LastIndexOf('\\') + 1);

                foreach (var file in _newFiles)
                {
                    var filename = file.Substring(_source.Length);
                    try
                    {
                        if (!filename.Equals(appFileName))
                        {
                            AddText("Aktualisiere Datei: " + file);
                            WriteFile(_logfile,
                                      DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Updating file: " + file + " >> " +
                                      _target + "\\" + filename);
                            var directoryName = Path.GetDirectoryName(_target + "\\" + filename);
                            if (directoryName.Length > 0 && !Directory.Exists(directoryName))
                                Directory.CreateDirectory(directoryName);
                            File.Copy(file, _target + "\\" + filename, true);
                            WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Update successful");
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteFile(_logfile,
                                  DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error Updating file: " + file);
                        WriteFile(_logfile, ": Target: " + _target + "\\" + filename);
                        WriteFile(_logfile, ": Trace: " + ex.StackTrace);
                        Rollback();

                        WriteFile(_logfile, "Failed");
                        return;
                    }
                }

                InitialiseConfigfiles(_target);
                //AddText("AppFileName: " + appFileName);
                try
                {
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Update complete");
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Delete updatefolder");
                    DeleteUpdateFolder();
                    if (!LastOperationSuccessful())
                        Application.Exit();
                    WriteFile(_logfile, "Success");

                    //Resatrt device
                    SoftReset();
                    /*
                    //Delete updater
                    var info = new ProcessStartInfo();
                    info.FileName = _target + "\\oxmc.UI_ce.exe";
                    Process.Start(info);
                    Process.GetCurrentProcess().Kill();
                     * */
                }
                catch (Exception ex)
                {
                    AddText(ex.ToString());
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error while cleaning Updatefolder: " +
                              ex.StackTrace);
                    WriteFile(_logfile, "Failed");
                    Process.GetCurrentProcess().Kill();
                    Application.Exit();
                }
            }
            catch(Exception ex)
            {
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": " + ex.ToString());
            }
        }

        private bool LastOperationSuccessful()
        {
            bool retval;
            try
            {
                var reader = new StreamReader(_logfile, true);
                var returnvalue = reader.ReadToEnd();
                if (returnvalue.Trim().EndsWith("Failed"))
                    retval = false;
                else
                    retval = true;
                reader.Close();
            }
            catch (Exception ex)
            {
                AddText(ex.ToString());
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error while cleaning Updatefolder: " + ex.StackTrace);
                WriteFile(_logfile, "Failed");
                Process.GetCurrentProcess().Kill();
                return false;
            }
            return retval;
        }

        private List<string> GetFiles(string path)
        {
            var retVal = new List<string>();
            foreach (var d in Directory.GetDirectories(path))
            {
                retVal.AddRange(GetFiles(d).ToList());
            }
            var files = Directory.GetFiles(path).ToList();
            retVal.AddRange(files);
            return retVal;
        }

        private void CreateBackup()
        {
            try
            {
                //Backup Configfiles
                BackupConfigfiles(_target);

                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Create Backup folder: " + _backupPath);
                if (!Directory.Exists(_backupPath))
                    Directory.CreateDirectory(_backupPath);
                var directorys = Directory.GetDirectories(_target).ToList();
                foreach (var d in directorys)
                {
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Create subfolder: " + _backupPath + "\\" + d.Substring(_target.Length));
                    Directory.CreateDirectory(_backupPath + "\\" + d.Substring(_target.Length));
                }
                foreach (var file in _newFiles)
                {
                    var filename = file.Substring(_source.Length);
                    var backupTargetName = _backupPath + "\\" + filename;
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Backing up " + _target + "\\" + filename);
                    if (File.Exists(_target))
                        File.Copy(_target + "\\" + filename, backupTargetName, true);
                }

                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Backup Complete");
            }
            catch (Exception ex)
            {
                AddText(ex.ToString());
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error while creating Backup: " + ex.StackTrace);
                WriteFile(_logfile, "Failed");
                Process.GetCurrentProcess().Kill();
            }
        }

        private void Rollback()
        {
            WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Commencing Rollback ");
            var backupFiles = GetFiles(_backupPath);
            foreach (var file in backupFiles)
            {
                var filename = file.Substring(_target.Length);
                try
                {
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Restoring file: " + file + " >> " + _target + "\\" + filename);
                    File.Copy(file, _target + "\\" + filename, true);
                }
                catch (Exception ex)
                {
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Restore failed");
                    WriteFile(_logfile, ": Exception: " + ex);
                }
            }
            WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Rollback ended");
            WriteFile(_logfile, "Failed");
            Process.GetCurrentProcess().Kill();
        }


        public void WriteFile(String aFilename, String aData)
        {
            try
            {
                if (aFilename.IndexOf("\\") != -1)
                    if (!Directory.Exists(aFilename.Substring(0, aFilename.LastIndexOf("\\"))))
                        Directory.CreateDirectory(aFilename.Substring(0, aFilename.LastIndexOf("\\")));

                using (var writer = new StreamWriter(aFilename, true))
                {
                    writer.WriteLine(aData);
                }
            }
            catch (Exception ex)
            {
                AddText(ex.ToString());
                AddText("Error WriteFile :" + ex.StackTrace);
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error WriteFile :" + ex.StackTrace);
                WriteFile(_logfile, "Failed");
            }
        }

        private void DeleteUpdateFolder()
        {
            try
            {
                var files = GetFiles(_source);
                var appFileName = _appName.Substring(_appName.LastIndexOf('\\') + 1);

                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": AppFileName  :" + appFileName);
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Executable  :" + _appPath);
                foreach (var file in files)
                {
                    var name = file.Substring(_source.Length);

                    if (!name.Equals(appFileName))
                    {
                        WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Deleting File: " + name);
                        File.Delete(file);
                    }
                }
                var directories = Directory.GetDirectories(_source);
                foreach (var dir in directories)
                {
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Deleting Dir: " + dir);
                    Directory.Delete(dir, true);
                }
            }
            catch (Exception ex)
            {
                AddText(ex.ToString());
                AddText("Error DeleteUpdateFolder :" + ex.StackTrace);
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error DeleteUpdateFolder :" + ex.StackTrace);
                WriteFile(_logfile, "Failed");
            }
        }

        public void BackupConfigfiles(string source)
        {
            try
            {
                _configDict = new Dictionary<string, NameValueCollection>();
                if (Directory.Exists(source))
                {
                    var settings = new NameValueCollection();
                    var oldFile = new XmlDocument();

                    WriteFile(_logfile, "Create config Dictionary");
                    foreach (var file in Directory.GetFiles(source, "*.config"))
                    {
                        settings = new NameValueCollection();
                        var filename = Path.GetFileName(file);
                        try
                        {
                            oldFile.Load(file);
                        }
                        catch (XmlException ex)
                        {
                            WriteFile(_logfile, "Invalid fileformat File: " + filename);
                            continue;
                        }

                        WriteFile(_logfile, "Create backup of " + filename);
                        var oldNodeList = oldFile.GetElementsByTagName("appSettings");

                        foreach (XmlNode node in oldNodeList)
                        {
                            foreach (XmlNode key in node.ChildNodes)
                            {
                                settings.Set(key.Attributes["key"].Value,
                                             key.Attributes["value"].Value);
                            }
                        }
                        _configDict.Add(filename, settings);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteFile(_logfile, ex.ToString());
            }
        }


        public void InitialiseConfigfiles(string sFilePathToExtractTo)
        {
            try
            {
                var newFile = new XmlDocument();
                NameValueCollection settings;

                WriteFile(_logfile, "InitialiseConfigfiles");
                foreach (var file in Directory.GetFiles(sFilePathToExtractTo, "*.config"))
                {
                    var filename = Path.GetFileName(file);
                    if (string.IsNullOrEmpty(filename) || !_configDict.ContainsKey(filename))
                        continue;

                    settings = _configDict[filename];
                    WriteFile(_logfile, "");
                    WriteFile(_logfile, "Ersetzen des Configfiles " + file);
                    newFile.Load(file);

                    var newNodeList = newFile.GetElementsByTagName("appSettings");

                    foreach (XmlNode node in newNodeList)
                    {
                        foreach (XmlNode key in node.ChildNodes)
                        {
                            if (settings[key.Attributes["key"].Value] != null)
                            {
                                WriteFile(_logfile, key.Attributes["key"].Value + ": " + key.Attributes["value"].Value + " => " + settings[key.Attributes["key"].Value]);
                                key.Attributes["value"].Value = settings[key.Attributes["key"].Value];
                            }
                        }
                    }
                    newFile.Save(file);
                }
            }
            catch (Exception ex)
            {
                WriteFile(_logfile, "Fehler beim Ersetzten der Configfiles: " + ex.ToString());
            }
        }

        public void AddText(string text)
        {
            tbContent.Text += text + Environment.NewLine;
            tbContent.ScrollToCaret();
            WriteFile(_logfile, "Textoutput: " + text);
            Refresh();
        }

        private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Exit();
        }
    }

    public class ProcEntry
    {
        public string ExeName;
        public uint ID;
    }

    public class ProcessEnumerator
    {
        #region Constants
        private const uint TH32CS_SNAPPROCESS = 0x00000002;
        private const int MAX_PATH = 260;
        #endregion

        #region Structs
        public struct PROCESSENTRY
        {
            public uint dwSize;
            public uint cntUsage;
            public uint th32ProcessID;
            public uint th32DefaultHeapID;
            public uint th32ModuleID;
            public uint cntThreads;
            public uint th32ParentProcessID;
            public int pcPriClassBase;
            public uint dwFlags;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_PATH)]
            public string szExeFile;
            uint th32MemoryBase;
            uint th32AccessKey;
        }
        #endregion

        #region P/Invoke
        [DllImport("toolhelp.dll")]
        private static extern IntPtr CreateToolhelp32Snapshot(uint flags, uint processID);

        [DllImport("toolhelp.dll")]
        private static extern int CloseToolhelp32Snapshot(IntPtr snapshot);

        [DllImport("toolhelp.dll")]
        private static extern int Process32First(IntPtr snapshot, ref PROCESSENTRY processEntry);

        [DllImport("toolhelp.dll")]
        private static extern int Process32Next(IntPtr snapshot, ref PROCESSENTRY processEntry);
        #endregion

        #region public Methods

        public static bool Enumerate(ref List<ProcEntry> list)
        {
            if (list == null)
            {
                return false;
            }
            list.Clear();

            IntPtr snapshot_ = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
            if (snapshot_ == IntPtr.Zero)
            {
                return false;
            }

            PROCESSENTRY entry_ = new PROCESSENTRY();
            entry_.dwSize = (uint)Marshal.SizeOf(entry_);
            if (Process32First(snapshot_, ref entry_) == 0)
            {
                CloseToolhelp32Snapshot(snapshot_);
                return false;
            }

            do
            {
                ProcEntry procEntry = new ProcEntry();
                procEntry.ExeName = entry_.szExeFile;
                procEntry.ID = entry_.th32ProcessID;
                list.Add(procEntry);
                entry_.dwSize = (uint)Marshal.SizeOf(entry_);
            }
            while (Process32Next(snapshot_, ref entry_) != 0);

            CloseToolhelp32Snapshot(snapshot_);

            return true;
        }

        public static bool KillProcess(uint procID)
        {
            try
            {
                System.Diagnostics.Process proc_ = System.Diagnostics.Process.GetProcessById((int)procID);
                proc_.Kill();
                proc_.Dispose();
                return true;
            }
            catch (ArgumentException)
            {
                return false; //process does not exist
            }
            catch (Exception)
            {
                return false; //cannot kill process (perhaps its system process)
            }
        }
        #endregion
    }
}