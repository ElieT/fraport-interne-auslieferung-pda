﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace SVM
{
    public class DirectoryCopy
    {
        private static ArrayList CollectedFiles = new ArrayList();

        public static void Copy(string source, string destination)
        {

            if (Directory.Exists(source))
            {

                foreach (string folder in Directory.GetDirectories(source))
                {
                    // Ordnername extrahieren
                    int index = folder.LastIndexOf("\\");
                    string cop = folder.Substring(index + 1);
                    Directory.CreateDirectory(destination + "\\" + cop);
                    // rekursiver aufruf
                    Copy(folder, destination + "\\" + cop);
                }

                string dest = source.Replace(source, destination);
                Directory.CreateDirectory(dest);
                Console.WriteLine("Create Directory: " + dest);

                foreach (string file in Directory.GetFiles(source))
                {
                    string newfile = file.Replace(source, destination);
                    File.Copy(file, newfile);
                    Console.WriteLine("Copy File " + file + " to " + newfile);
                }
            }
        }

        public static void Search(string Dir, string FileTag)
        {

            if (Directory.Exists(Dir))
            {

                foreach (string SubDir in Directory.GetDirectories(Dir))
                {
                    Search(SubDir, FileTag);
                }

                foreach (string File in Directory.GetFiles(Dir))
                {
                    if (File.Substring(File.IndexOf(".")) == FileTag)
                        CollectedFiles.Add(File);
                }
            }
        }


        public static void InitialiseConfigfiles(string sFilePathToExtractTo, string sFolderToBackupTo)
        {
            try
            {
                var oldFile = new XmlDocument();
                var newFile = new XmlDocument();
                var settings = new NameValueCollection();

                foreach (var file in Directory.GetFiles(sFilePathToExtractTo, "*.config"))
                {
                    var filename = Path.GetFileName(file);
                    newFile.Load(file);
                    var backupfile = Directory.GetFiles(sFolderToBackupTo, filename);
                    oldFile.Load(backupfile[0]);

                    var newNodeList = newFile.GetElementsByTagName("appSettings");
                    var oldNodeList = oldFile.GetElementsByTagName("appSettings");

                    foreach (XmlNode node in oldNodeList)
                    {
                        foreach (XmlNode key in node.ChildNodes)
                        {
                            settings.Set(key.Attributes["key"].Value,
                                         key.Attributes["value"].Value);
                        }
                    }

                    foreach (XmlNode node in newNodeList)
                    {
                        foreach (XmlNode key in node.ChildNodes)
                        {
                            if (settings[key.Attributes["key"].Value] != null)
                                key.Attributes["value"].Value = settings[key.Attributes["key"].Value];
                        }
                    }
                    newFile.Save(file);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
