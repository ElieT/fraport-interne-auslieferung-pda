﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Xml;

namespace svm
{
    public class AppConfig
    {
        #region Fields

        private static string AppPath =
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static NameValueCollection Settings;
        private static XmlNodeList _nodeList;
        private static String _configMode;
        private static String _defaultConfigMode = "app_prd";
        private static String _configFile = "\\" + _defaultConfigMode + ".config";
        public static string ConfigFile
        {
            get { return _configMode; }
            set { _configMode = value; }
        }

        private static XmlDocument _xmlDocument;
        #endregion

        #region Constructors

        // Static Ctor
        static AppConfig()
        {
            //LoadSettings(configMode);
        }

        public static void LoadSettings(String configMode)
        {
            if (String.IsNullOrEmpty(configMode))
                configMode = _defaultConfigMode;
            _configMode = configMode;
            //System.Console.WriteLine("load settings: " + _configMode);
            try
            {
                if (AppPath == null)
                    return;
                if (AppPath.Length > 7 && AppPath.IndexOf("file:") != -1)
                    AppPath = AppPath.Substring(6, AppPath.Length - 6);
                _configFile = AppPath + "\\" + configMode + ".config";

                if (!File.Exists(_configFile))
                {
                    throw new FileNotFoundException(string.Format("Application configuration file '{0}' not found.",
                                                                  _configFile));
                }
                _xmlDocument = new XmlDocument();
                _xmlDocument.Load(_configFile);
                _nodeList = _xmlDocument.GetElementsByTagName("appSettings");
                if (Settings == null)
                    Settings = new NameValueCollection();

                foreach (XmlNode node in _nodeList)
                {
                    foreach (XmlNode key in node.ChildNodes)
                    {
                        if ((key.Attributes["key"].Value.Equals("UserId")) && !String.IsNullOrEmpty(Settings["UserId"]))
                        {

                        }
                        else if ((key.Attributes["key"].Value.Equals("Mandt")) && !String.IsNullOrEmpty(Settings["Mandt"]))
                        {

                        }
                        else if ((key.Attributes["key"].Value.Equals("Password")) && !String.IsNullOrEmpty(Settings["Password"]))
                        {

                        }
                        else
                        {
                            Settings.Set(key.Attributes["key"].Value,
                                         key.Attributes["value"].Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public static void SaveSettings(String configMode)
        {
            try
            {
                if (String.IsNullOrEmpty(configMode))
                    configMode = _defaultConfigMode;
                //configMode = _defaultConfigMode;
                _configMode = configMode;
                System.Console.WriteLine("save settings: " + _configMode);
                if (AppPath == null)
                    return;
                if (AppPath.Length > 7 && AppPath.IndexOf("file:") != -1)
                    AppPath = AppPath.Substring(6, AppPath.Length - 6);
                _configFile = AppPath + "\\" + configMode + ".config";

                foreach (XmlNode node in _nodeList)
                {
                    foreach (XmlNode key in node.ChildNodes)
                    {
                        if (Settings[key.Attributes["key"].Value] != null)
                            key.Attributes["value"].Value = Settings[key.Attributes["key"].Value];
                    }
                }
                _xmlDocument.Save(_configFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion Constructors

        #region Public Properties

        public static string DatabasePath
        {
            get
            {
                String retwert = "";
                retwert = @"Data Source = " + Path.Combine(AppPath, "AssetManagementDB_SQLite.sqlite");
                return retwert;
            }
        }

        public static string GetAppPath()
        {
            return AppPath;
        }


        public static int RfidTechObjMaxTagLength
        {
            get { return int.Parse(Settings.Get("rfid.TechObjMaxTagLength")); }
            set { Settings.Set("rfid.TechObjMaxTagLength", value.ToString()); }
        }

        public static int RfidPersIdMaxTagLength
        {
            get { return int.Parse(Settings.Get("rfid.PersIdMaxTagLength")); }
            set { Settings.Set("rfid.PersIdMaxTagLength", value.ToString()); }
        }

        public static int RfidTechObjIdentLength
        {
            get { return int.Parse(Settings.Get("rfid.TechObjIdentLength")); }
            set { Settings.Set("rfid.TechObjIdentLength", value.ToString()); }
        }

        public static Boolean DebugMode
        {
            get { return Settings.Get("System.DebugMode").Equals("X"); }
            set { Settings.Set("System.DebugMode", (value) ? "X" : ""); }
        }

        public static String TraceFileName
        {
            get { return Settings.Get("trace.filename"); }
            set { Settings.Set("trace.filename", value); }
        }

        public static String RFIDTimeout
        {
            get { return Settings.Get("rfid.timeout"); }
            set { Settings.Set("rfid.timeout", value); }
        }

        public static String RFIDPortName
        {
            get { return Settings.Get("rfid.PortName"); }
            set { Settings.Set("rfid.PortName", value); }
        }

        public static String RFIDPortType
        {
            get { return Settings.Get("rfid.PortType"); }
            set { Settings.Set("rfid.PortType", value); }
        }

        public static Boolean RFIDWriteVerify
        {
            get { return Settings.Get("rfid.WriteVerify").Equals("X"); }
            set { Settings.Set("rfid.WriteVerify", (value) ? "X" : ""); }
        }

        public static String RFIDDriverTimeOut
        {
            get { return Settings.Get("rfid.DriverTimeOut"); }
            set { Settings.Set("rfid.DriverTimeOut", value); }
        }

        public static String TraceFileSize
        {
            get { return Settings.Get("trace.filesize"); }
            set { Settings.Set("trace.filesize", value); }
        }

        public static String UserId
        {
            get { return Settings.Get("UserId"); }
            set { Settings.Set("UserId", value); }
        }

        public static String Mandt
        {
            get { return Settings.Get("Mandt"); }
            set { Settings.Set("Mandt", value); }
        }

        public static String Email1
        {
            get { return Settings.Get("user.email1"); }
            set { Settings.Set("user.email1", value); }
        }

        public static String Email2
        {
            get { return Settings.Get("user.email2"); }
            set { Settings.Set("user.email2", value); }
        }

        public static String Pw
        {
            get { return Settings.Get("Password"); }
            set { Settings.Set("Password", value); }
        }

        public static string Protocol
        {
            get { return Settings.Get("Protocol"); }
            set { Settings.Set("Protocol", value); }
        }

        public static string Gateway
        {
            get { return Settings.Get("Gateway"); }
            set { Settings.Set("Gateway", value); }
        }

        public static string GatewayIdent
        {
            get { return Settings.Get("GatewayIdent"); }
            set { Settings.Set("GatewayIdent", value); }
        }

        public static string GatewayPort
        {
            get { return Settings.Get("GatewayPort"); }
            set { Settings.Set("GatewayPort", value); }
        }

        public static string GatewayServiceSync
        {
            get { return Settings.Get("GatewayServiceSync"); }
            set { Settings.Set("GatewayServiceSync", value); }
        }

        public static string GatewayServiceInfo
        {
            get { return Settings.Get("GatewayServiceInfo"); }
            set { Settings.Set("GatewayServiceInfo", value); }
        }


        public static string GatewayServiceSVM
        {
            get { return Settings.Get("GatewayServiceSVM"); }
            set { Settings.Set("GatewayServiceSVM", value); }
        }



        public static string FlagMassdata
        {
            get { return Settings.Get("sync.massData"); }
            set { Settings.Set("sync.massData", value); }
        }

        public static string FlagCustomizing
        {
            get { return Settings.Get("sync.reqCust"); }
            set { Settings.Set("sync.reqCust", value); }
        }

        public static string FlagDocuments
        {
            get { return Settings.Get("sync.reqDoc"); }
            set { Settings.Set("sync.reqDoc", value); }
        }

        public static string FlagMasterdata
        {
            get { return Settings.Get("sync.reqMD"); }
            set { Settings.Set("sync.reqMD", value); }
        }

        public static string FlagAutoLogon
        {
            get { return Settings.Get("user.AutoLogon"); }
            set { Settings.Set("user.AutoLogon", value); }
        }

        public static string FlagTestmode
        {
            get { return Settings.Get("sync.testMode"); }
            set { Settings.Set("sync.testMode", value); }
        }
        public static string FlagConfirmation
        {
            get { return Settings.Get("sync.confirmation"); }
            set { Settings.Set("sync.confirmation", value); }
        }
        public static string FlagBackgroundSync
        {
            get { return Settings.Get("sync.backgroundSync"); }
            set { Settings.Set("sync.backgroundSync", value); }
        }

        public static string TestMode
        {
            get { return Settings.Get("sync.testMode"); }
            set { Settings.Set("sync.testMode", value); }
        }
        public static string FlagSyncAtStartup
        {
            get { return Settings.Get("sync.SyncAtStartup"); }
            set { Settings.Set("sync.SyncAtStartup", value); }
        }

        public static string FlagShowOrderReportOnSave
        {
            get { return Settings.Get("ShowOrderReportOnSave"); }
            set { Settings.Set("ShowOrderReportOnSave", value); }
        }

        public static string PresetNetworkAddress
        {
            get { return Settings.Get("PresetNetworkAddress"); }
            set { Settings.Set("PresetNetworkAddress", value); }
        }

        public static string PresetVPNAddress
        {
            get { return Settings.Get("PresetVPNAddress"); }
            set { Settings.Set("PresetVPNAddress", value); }
        }

        public static string TimePrecision
        {
            get { return Settings.Get("TimePrecision"); }
            set { Settings.Set("TimePrecision", value); }
        }

        public static Boolean RfidEnabled
        {
            get { return Settings.Get("RfidEnabled").Equals("X"); }
            set { Settings.Set("RfidEnabled", (value) ? "X" : ""); }
        }

        public static String DeviceId
        {
            get { return Settings.Get("System.DeviceId"); }
            set { Settings.Set("System.DeviceId", value); }
        }

        public static int MessageTimeout
        {
            get { return int.Parse(Settings.Get("Statusmessage.Timeout")); }
            set { Settings.Set("Statusmessage.Timeout", value.ToString()); }
        }

        /// <summary>
        /// Defines the Level for displaying messages to the user
        /// Messagelevels are:
        ///  - 0: No messages are displayed
        ///  - 1: only informatory messages are displayed (type I)
        ///  - 2: All messages but Exceptions (type E) are displayed 
        ///  - 3: Debugging mode, all Mmssages are displayed
        /// </summary>
        public static string MessageDisplayLevel
        {
            get { return Settings.Get("MessageDisplayLevel"); }
            set { Settings.Set("MessageDisplayLevel", value); }
        }

        public static String DefaultApplication
        {
            get { return Settings.Get("application.default"); }
        }

        public static int[] Color1
        {
            get
            {
                if (Settings == null)
                    return null;

                String strColor = Settings.Get("Color1");
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        public static int[] Color2
        {
            get
            {
                String strColor = Settings.Get("Color2");
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        public static int[] Color3
        {
            get
            {
                String strColor = Settings.Get("Color3");
                int[] color = {
                                  Convert.ToInt32(strColor.Split(',')[0]), Convert.ToInt32(strColor.Split(',')[1]),
                                  Convert.ToInt32(strColor.Split(',')[2])
                              };
                return color;
            }
        }

        #endregion
    }
}