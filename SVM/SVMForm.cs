﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using svm;
using SVM.Properties;

namespace SVM
{
    public partial class SVMForm : Form
    {
        private static String _sUrlToReadFileFrom = "";
        private static String _sFilePathToWriteFileTo = "";
        private static String _sFilePathToExtractTo = "";
        private static String _configMode = "app_prd";
        private static String _appl = "ih";
        private static String _clientOs = "mc75";
        private static String _mobileuser = "i0001";
        private static String _version = "001.001.031"; //"001.001.032";
        private static String _versionOld = "001.001.030"; //"001.001.032";
        private static String _filename = "w32_1_0_31.zip"; //"dlptest10c.zip";
        private static String _downloadFolder = "\\Temp\\";
        private static String _destinationFolder = "F:\\Documents\\dev\\oxmc\\oxmc_fraport\\w32\\oxmc\\UI\\bin\\x86\\Release\\";
        private static String _configFile = "";
        private static String _checksum = "";
        private static String _appPath =
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

        private static String _logfile = "";
        private static string[] _args;
        private static String _sFolderToBackupTo;

        public SVMForm()
        {
            try
            {
                InitializeComponent();
                ApplicationStart();
            }catch (Exception ex)
            {
                
            }
        }

        private void ApplicationStart()
        {
            try
            {
                lblMessage.Text = "";
                Boolean setConfigData = false;
                if (_appPath == null)
                    return;
                if (_appPath.Length > 7 && _appPath.IndexOf("file:") != -1)
                    _appPath = _appPath.Substring(6, _appPath.Length - 6);
                _configFile = _appPath + "\\svm.config";
                _logfile = _appPath + "\\svm_trace.txt";


                if (_args != null && _args.Length > 0)
                    _configMode = _args[0];


                WriteFile(_logfile,
                        DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") +
                        ": Lade Konfiguration");
                //WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Using config file data");
                lblMessage.Text = "Lade Konfiguration";
                if (!LoadSVMConfigFile())
                {
                    //No config file and no arguments -> Cant do anything;
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") +
                              ": Error no arguments or config data found");
                }
                else
                {
                    setConfigData = true;
                    lblMessage.Text = "Konfiguration geladen. Bitte Download starten.";
                    WriteFile(_logfile,
                        DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") +
                        ": Konfiguration geladen. Bitte Download starten.");
                }

                switch (_configMode)
                {
                    case "dev":
                        _configMode = "app_dev";
                        break;
                    case "ox":
                        _configMode = "app_ox";
                        break;
                    case "kon":
                        _configMode = "app_kon";
                        break;
                    default:
                        _configMode = "app_prd";
                        break;
                }
                WriteFile(_logfile,
                          DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _configMode = " + _configMode);

                if (!setConfigData)
                {
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": No Config Data found. Exit.");
                    return;
                }

                WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Start loading settings");
                AppConfig.LoadSettings(_configMode);

                if (AppConfig.Settings == null || AppConfig.Settings.Count == 0)
                    WriteFile(_logfile,
                                DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": No settings found");

                WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Finish loading settings");

                WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Start checking settings");

                //AppConfig.Protocol = "HTTP";

                WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": AppConfig.Protocol " + AppConfig.Protocol);

                if (String.IsNullOrEmpty(AppConfig.Protocol) || String.IsNullOrEmpty(AppConfig.Gateway) || String.IsNullOrEmpty(AppConfig.GatewayPort) || String.IsNullOrEmpty(AppConfig.GatewayIdent) || String.IsNullOrEmpty(AppConfig.GatewayServiceSVM))
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Parameter is empty or null.");

                if (String.IsNullOrEmpty(_appl))
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _appl is empty or null.");
                if (String.IsNullOrEmpty(_clientOs))
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _clientOs is empty or null.");
                if (String.IsNullOrEmpty(_mobileuser))
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _mobileuser is empty or null.");
                if (String.IsNullOrEmpty(_version))
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _version is empty or null.");

                _sUrlToReadFileFrom = AppConfig.Protocol + "://" + AppConfig.Gateway + ":" + AppConfig.GatewayPort +
                                      "/sap" +
                                      AppConfig.GatewayIdent
                                      + AppConfig.GatewayServiceSVM +
                                      "svm_get_file.htm?" + "pa_appl=" + _appl + "&pa_client_os=" + _clientOs +
                                      "&pa_mobileuser=" + _mobileuser + "&pa_version=" + _version;

                _sFolderToBackupTo = _downloadFolder + "\\backup\\";
                _sFilePathToWriteFileTo = _downloadFolder + _filename;
                _sFilePathToExtractTo = _destinationFolder;
                WriteFile(_logfile,
                          DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _sUrlToReadFileFrom: " + _sUrlToReadFileFrom);
                WriteFile(_logfile,
                          DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _sFilePathToWriteFileTo: " +
                          _sFilePathToWriteFileTo);
                WriteFile(_logfile,
                          DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": _sFilePathToExtractTo: " +
                          _sFilePathToExtractTo);

                if (!File.Exists(_sFilePathToWriteFileTo))
                {
                    button2.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error ApplicationStart :" + ex.Message + " " + ex.StackTrace);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                WriteFile(_logfile,DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Pfad zur Appplikation:  " + _appPath + "; Config Mode: " + _configMode);

                //String forecastAdress = "http://www.weather.com/stockholm..html"
                WriteFile(_logfile,
                          DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Trying to connect: " +
                          _sUrlToReadFileFrom);

                lblMessage.Text += "\r\nDatei wird heruntergeladen.... Bitte warten.";
                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(_sUrlToReadFileFrom);
                httpReq.Proxy = GlobalProxySelection.GetEmptyWebProxy();
                WriteFile(_logfile,
                          DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Connection establised: " +
                          _sUrlToReadFileFrom);

                //httpRequest.Credentials = CredentialCache.DefaultCredentials;

                //var reqStream = httpReq.GetRequestStream();

                HttpWebResponse httpResponse = (HttpWebResponse)httpReq.GetResponse();

                // gets the size of the file in bytes
                Int64 iSize = httpResponse.ContentLength;

                // keeps track of the total bytes downloaded so we can update the progress bar
                Int64 iRunningByteTotal = 0;

                //System.IO.Stream dataStream = httpResponse.GetResponseStream();
                using (System.IO.Stream streamRemote = httpResponse.GetResponseStream())
                {
                    // using the FileStream object, we can write the downloaded bytes to the file system
                    using (
                        Stream streamLocal = new FileStream(_sFilePathToWriteFileTo, FileMode.Create,
                                                            FileAccess.Write, FileShare.None))
                    {
                        // loop the stream and get the file into the byte buffer
                        int iByteSize = 0;
                        byte[] byteBuffer = new byte[iSize];
                        WriteFile(_logfile,
                                  DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": byteBuffer: " + byteBuffer.Length);
                        while ((iByteSize = streamRemote.Read(byteBuffer, 0, byteBuffer.Length)) > 0)
                        {
                            // write the bytes to the file system at the file path specified
                            streamLocal.Write(byteBuffer, 0, iByteSize);
                            iRunningByteTotal += iByteSize;

                            // calculate the progress out of a base "100"
                            double dIndex = (double)(iRunningByteTotal);
                            double dTotal = (double)byteBuffer.Length;
                            double dProgressPercentage = (dIndex / dTotal);
                            int iProgressPercentage = (int)(dProgressPercentage * 100);
                            progressBar1.Value = iProgressPercentage;
                            Refresh();
                            // update the progress bar
                            //backgroundWorker1.ReportProgress(iProgressPercentage);
                        }

                        // clean up the file stream
                        streamLocal.Close();
                    }

                    // close the connection to the remote server
                    streamRemote.Close();
                    WriteFile(_logfile,
                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Connection closed");
                }
                lblMessage.Text += "\r\nDatei erfolgreich heruntergeladen. Bitte installieren.";


                //System.IO.StreamReader streamReader = new System.IO.StreamReader(dataStream);

                //String forecastData = streamReader.ReadToEnd();

                //streamReader.Close();
                httpResponse.Close();

                if (File.Exists(_sFilePathToWriteFileTo))
                {
                    progressBar1.Value = 1;
                    button2.Enabled = true;
                    Refresh();
                }
            } //try
            catch (Exception ex)
            {
                lblMessage.Text += "\r\nAbbruch, Fehler beim Herunterladen: " + ex.Message;
                System.Windows.Forms.MessageBox.Show(ex.Message);
            } //catch
        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text += "\r\nNeue Version wird installiert.";

                if (Directory.Exists(_sFilePathToExtractTo))
                {
                    if (Directory.Exists(_sFolderToBackupTo))
                        Directory.Delete(_sFolderToBackupTo, true);
                    DirectoryCopy.Copy(_sFilePathToExtractTo, _sFolderToBackupTo);
                }

                //String _sFilePathToWriteFileTo = "\\Temp\\xx.cab";
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = _sFilePathToWriteFileTo;
                //process.StartInfo.Arguments = strIniFilePath;
                process.Start();
                int time = 0;
                while (!process.HasExited)
                {
                    time = 1000;
                    Thread.Sleep(time);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text += "\r\nAbbruch, Fehler bei der Installation: " + ex.Message;
            }
        }

        private static Boolean LoadSVMConfigFile()
        {
            try
            {
                if (!File.Exists(_configFile))
                    return false;
                var stream = new StreamReader(_configFile, Encoding.Default);
                try
                {
                    string strLine = "";
                    int count = 0;
                    while ((strLine = stream.ReadLine()) != null)
                    {
                        WriteFile(_logfile,
                                DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": strLine: " +
                                strLine);
                        switch (count)
                        {
                            case 0:
                                _configMode = strLine;
                                break;
                            case 1:
                                _appl = strLine;
                                break;
                            case 2:
                                _clientOs = strLine;
                                break;
                            case 3:
                                _mobileuser = strLine;
                                break;
                            case 4:
                                _version = strLine;
                                break;
                            case 5:
                                _versionOld = strLine;
                                break;
                            case 6:
                                _filename = strLine;
                                break;
                            case 7:
                                _downloadFolder = strLine;
                                break;
                            case 8:
                                _destinationFolder = strLine;
                                break;
                            case 9:
                                _checksum = strLine;
                                break;
                        }

                        WriteFile(_logfile,
                                DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Count: " +
                                count);
                        
                        count++;

                    }
                }
                catch (Exception ex)
                {
                    WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error Load Config File: " + ex.StackTrace);
                    System.Console.WriteLine(ex.StackTrace);
                    return false;
                }

                finally
                {
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                WriteFile(_logfile, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": Error Load Config File: " + ex.StackTrace);
                System.Console.WriteLine(ex.StackTrace);
                return false;
            }

            return true;
        }
        public static void WriteFile(String aFilename, String aData)
        {
            try
            {
                if (aFilename.IndexOf("\\") != -1)
                    if (!Directory.Exists(aFilename.Substring(0, aFilename.LastIndexOf("\\"))))
                        Directory.CreateDirectory(aFilename.Substring(0, aFilename.LastIndexOf("\\")));

                using (var writer = new StreamWriter(aFilename, true))
                {
                    writer.WriteLine(aData);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Error WriteFile :" + ex.StackTrace);
            }
        }

        private void SVMForm_Closing(object sender, CancelEventArgs e)
        {
            var res = MessageBox.Show("Möchten Sie die Applikation wirklich beenden?", "Beenden", MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (res == DialogResult.No)
                e.Cancel = true;
            Application.Exit();
        }
    }
}